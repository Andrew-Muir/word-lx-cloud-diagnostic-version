﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace PrinterApis.WinNative
{
    [StructLayout(LayoutKind.Sequential)]
    public struct PRINTER_INFO_9
    {
        public IntPtr pDevMode;

        // Pointer to SECURITY_DESCRIPTOR
        public int pSecurityDescriptor;
    }
}