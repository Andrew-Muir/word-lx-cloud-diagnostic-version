﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PrinterApis.WinNative
{
    public static class DEVMODEfModes
    {
        public const int DM_IN_BUFFER = 0x8;
        public const int DM_IN_PROMPT = 0x4;
        public const int DM_OUT_BUFFER = 0x2;
    }
}