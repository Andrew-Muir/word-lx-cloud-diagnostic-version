﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace PrinterApis.WinNative
{
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public struct PRINTER_INFO_5
    {
        [MarshalAs(UnmanagedType.LPTStr)]
        public string pPrinterName;
        [MarshalAs(UnmanagedType.LPTStr)]
        public string pPortName;
        public uint Attributes;
        public uint DeviceNotSelectedTimeout;
        public uint TransmissionRetryTimeout;
    }
}
