﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PrinterApis.WinNative
{
	internal class WinErrors
	{
		/// <summary>
		/// The data area passed to a system call is too small.
		/// </summary>
		public const int ERROR_INSUFFICIENT_BUFFER = 122;
	}
}