﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PrinterApis.WinNative
{
    public static class PRINTER_DEFAULTS_DesiredAccess
    {
        public const int PRINTER_ACCESS_ADMINISTER = 0x4;
        public const int PRINTER_ACCESS_USE = 0x8;
        public const int STANDARD_RIGHTS_REQUIRED = 0xF0000;
    }
}