﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PrinterApis.WinNative
{
    public struct POINTL
    {
        public Int32 x;
        public Int32 y;
    }
}