﻿using PrinterApis.WinNative;
using System;
using System.Runtime.InteropServices;

namespace PrinterApis
{
    public class PrinterSettingsApis
    {
        private const int DM_DUPLEX = 7;
        private const int DM_COLORDEVICE = 32;
        private const int PRINTER_ENUM_LOCAL = 2;
        private const int PRINTER_ENUM_CONNECTIONS = 4;
        public static bool SetPrinterProperty(string printerName, DM dmProperty, long dmPropertyValue,
            out string errorMessage)
        {
            errorMessage = string.Empty;
            var functionReturnValue = false;
            var pd = default(PRINTER_DEFAULTS);
            var pinfo = new PRINTER_INFO_9();
            var dm = new DEVMODE();
            var nBytesNeeded = 0;
            var nRet = 0;
            var nJunk = default(int);
            var hPrinter = default(IntPtr);
            var ptrPrinterInfo = default(IntPtr);

            pd.DesiredAccess = PRINTER_DEFAULTS_DesiredAccess.PRINTER_ACCESS_USE;
            nRet = WinApis.OpenPrinter(printerName, out hPrinter, ref pd);
            if ((nRet == 0) | (hPrinter.ToInt64() == 0))
            {
                if (WinApis.GetLastError() == 5)
                {
                    errorMessage = "Access denied -- See the article for more info.";
                }
                else
                {
                    errorMessage = "Cannot open the printer specified " + "(make sure the printer name is correct).";
                }
                return false;
            }
            nRet = WinApis.DocumentProperties(IntPtr.Zero, hPrinter, printerName, IntPtr.Zero, IntPtr.Zero, 0);
            if ((nRet < 0))
            {
                errorMessage = "Cannot get the size of the DEVMODE structure.";
                goto cleanup;
            }
            var iparg = Marshal.AllocCoTaskMem(nRet + 100);
            nRet = WinApis.DocumentProperties(IntPtr.Zero, hPrinter, printerName, iparg, IntPtr.Zero,
                DEVMODEfModes.DM_OUT_BUFFER);
            if ((nRet < 0))
            {
                errorMessage = "Cannot get the DEVMODE structure.";
                goto cleanup;
            }
            dm = (DEVMODE)Marshal.PtrToStructure(iparg, dm.GetType());
            if (!Convert.ToBoolean(dm.dmFields & dmProperty))
            {
                errorMessage =
                    "You cannot modify the setting flag for this printer because it does not support the setting or the driver does not support setting it from the Windows API.";
                goto cleanup;
            }

            switch (dmProperty)
            {
                case DM.Orientation:
                    dm.dmOrientation = (short)dmPropertyValue;
                    break;

                case DM.PaperSize:
                    dm.dmPaperSize = (short)dmPropertyValue;
                    break;

                case DM.Copies:
                    dm.dmCopies = (short)dmPropertyValue;
                    break;

                case DM.NUP:
                    dm.dmNup = (short)dmPropertyValue;
                    break;

                case DM.Duplex:
                    dm.dmDuplex = (short)dmPropertyValue;
                    break;

                case DM.Color:
                    dm.dmColor = (short)dmPropertyValue;
                    break;
            }

            Marshal.StructureToPtr(dm, iparg, true);
            nRet = WinApis.DocumentProperties(IntPtr.Zero, hPrinter, printerName, pinfo.pDevMode, pinfo.pDevMode,
                DEVMODEfModes.DM_IN_BUFFER | DEVMODEfModes.DM_OUT_BUFFER);
            if ((nRet < 0))
            {
                errorMessage = "Unable to set setting to this printer.";
                goto cleanup;
            }
            WinApis.GetPrinter(hPrinter, 9, IntPtr.Zero, 0, ref nBytesNeeded);
            if ((nBytesNeeded == 0))
            {
                errorMessage = "GetPrinter failed.";
                goto cleanup;
            }
            ptrPrinterInfo = Marshal.AllocCoTaskMem(nBytesNeeded + 100);
            nRet = WinApis.GetPrinter(hPrinter, 9, ptrPrinterInfo, nBytesNeeded, ref nJunk) ? 1 : 0;
            if ((nRet == 0))
            {
                errorMessage = "Unable to get shared printer settings.";
                goto cleanup;
            }
            pinfo = (PRINTER_INFO_9)Marshal.PtrToStructure(ptrPrinterInfo, pinfo.GetType());
            pinfo.pDevMode = iparg;
            pinfo.pSecurityDescriptor = 0;
            Marshal.StructureToPtr(pinfo, ptrPrinterInfo, true);
            nRet = WinApis.SetPrinter(hPrinter, 9, ptrPrinterInfo, 0) ? 1 : 0;
            if ((nRet == 0))
            {
                errorMessage = "Unable to set shared printer settings.";
            }
            functionReturnValue = Convert.ToBoolean(nRet);
            cleanup:
            if ((hPrinter.ToInt64() != 0))
                WinApis.ClosePrinter(hPrinter);
            return functionReturnValue;
        }

        /// <summary>
        /// Method Name : GetPrinterDuplex 
        /// Programmatically get the Duplex flag for the specified printer 
        /// driver's default properties. 
        /// </summary>
        /// <param name="sPrinterName"> The name of the printer to be used. </param>
        /// <param name="errorMessage"> this will contain error messsage if any. </param>
        /// <returns> 
        /// nDuplexSetting - One of the following standard settings: 
        /// 0 = Error
        /// 1 = None (Simplex)
        /// 2 = Duplex on long edge (book) 
        /// 3 = Duplex on short edge (legal) 
        /// </returns>
        /// <remarks>
        /// </remarks>
        public static short GetPrinterDuplex(string sPrinterName, out string errorMessage)
        {
            errorMessage = string.Empty;
            short functionReturnValue = 0;
            IntPtr hPrinter = default(IntPtr);
            PRINTER_DEFAULTS pd = default(PRINTER_DEFAULTS);
            DEVMODE dm = new DEVMODE();
            int nRet = 0;
            pd.DesiredAccess = PRINTER_DEFAULTS_DesiredAccess.PRINTER_ACCESS_USE;
            nRet = WinApis.OpenPrinter(sPrinterName, out hPrinter, ref pd);
            if ((nRet == 0) | (hPrinter.ToInt64() == 0))
            {
                if (WinApis.GetLastError() == 5)
                {
                    errorMessage = "Access denied -- See the article for more info.";
                }
                else
                {
                    errorMessage = "Cannot open the printer specified " + "(make sure the printer name is correct).";
                }
                return functionReturnValue;
            }
            nRet = WinApis.DocumentProperties(IntPtr.Zero, hPrinter, sPrinterName, IntPtr.Zero, IntPtr.Zero, 0);
            if ((nRet < 0))
            {
                errorMessage = "Cannot get the size of the DEVMODE structure.";
                goto cleanup;
            }
            IntPtr iparg = Marshal.AllocCoTaskMem(nRet + 100);
            nRet = WinApis.DocumentProperties(IntPtr.Zero, hPrinter, sPrinterName, iparg, IntPtr.Zero, DEVMODEfModes.DM_OUT_BUFFER);
            if ((nRet < 0))
            {
                errorMessage = "Cannot get the DEVMODE structure.";
                goto cleanup;
            }
            dm = (DEVMODE)Marshal.PtrToStructure(iparg, dm.GetType());
            if (!Convert.ToBoolean(dm.dmFields & DM.Duplex))
            {
                errorMessage = "You cannot modify the duplex flag for this printer " + "because it does not support duplex or the driver " + "does not support setting it from the Windows API.";
                goto cleanup;
            }
            functionReturnValue = dm.dmDuplex;

        cleanup:
            if ((hPrinter.ToInt64() != 0))
                WinApis.ClosePrinter(hPrinter);
            return functionReturnValue;
        }
        public static short GetPrinterColor(string sPrinterName, out string errorMessage)
        {
            errorMessage = string.Empty;
            short functionReturnValue = 0;
            IntPtr hPrinter = default(IntPtr);
            PRINTER_DEFAULTS pd = default(PRINTER_DEFAULTS);
            DEVMODE dm = new DEVMODE();
            int nRet = 0;
            pd.DesiredAccess = PRINTER_DEFAULTS_DesiredAccess.PRINTER_ACCESS_USE;
            nRet = WinApis.OpenPrinter(sPrinterName, out hPrinter, ref pd);
            if ((nRet == 0) | (hPrinter.ToInt64() == 0))
            {
                if (WinApis.GetLastError() == 5)
                {
                    errorMessage = "Access denied -- See the article for more info.";
                }
                else
                {
                    errorMessage = "Cannot open the printer specified " + "(make sure the printer name is correct).";
                }
                return functionReturnValue;
            }
            nRet = WinApis.DocumentProperties(IntPtr.Zero, hPrinter, sPrinterName, IntPtr.Zero, IntPtr.Zero, 0);
            if ((nRet < 0))
            {
                errorMessage = "Cannot get the size of the DEVMODE structure.";
                goto cleanup;
            }
            IntPtr iparg = Marshal.AllocCoTaskMem(nRet + 100);
            nRet = WinApis.DocumentProperties(IntPtr.Zero, hPrinter, sPrinterName, iparg, IntPtr.Zero, DEVMODEfModes.DM_OUT_BUFFER);
            if ((nRet < 0))
            {
                errorMessage = "Cannot get the DEVMODE structure.";
                goto cleanup;
            }
            dm = (DEVMODE)Marshal.PtrToStructure(iparg, dm.GetType());
            if (!Convert.ToBoolean(dm.dmFields & DM.Color))
            {
                errorMessage = "You cannot modify the duplex flag for this printer " + "because it does not support color or the driver " + "does not support setting it from the Windows API.";
                goto cleanup;
            }
            functionReturnValue = dm.dmColor;

            cleanup:
            if ((hPrinter.ToInt64() != 0))
                WinApis.ClosePrinter(hPrinter);
            return functionReturnValue;
        }

        public static IntPtr GetDevMode(string sPrinterName, out string errorMessage)
        {
            errorMessage = string.Empty;
            IntPtr hPrinter = default(IntPtr);
            PRINTER_DEFAULTS pd = default(PRINTER_DEFAULTS);
            DEVMODE dm = new DEVMODE();
            int nRet = 0;
            pd.DesiredAccess = PRINTER_DEFAULTS_DesiredAccess.PRINTER_ACCESS_USE;
            nRet = WinApis.OpenPrinter(sPrinterName, out hPrinter, ref pd);
            if ((nRet == 0) | (hPrinter.ToInt64() == 0))
            {
                if (WinApis.GetLastError() == 5)
                {
                    errorMessage = "Access denied -- See the article for more info.";
                }
                else
                {
                    errorMessage = "Cannot open the printer specified " + "(make sure the printer name is correct).";
                }
                return default(IntPtr);
            }
            nRet = WinApis.DocumentProperties(IntPtr.Zero, hPrinter, sPrinterName, IntPtr.Zero, IntPtr.Zero, 0);
            if ((nRet < 0))
            {
                errorMessage = "Cannot get the size of the DEVMODE structure.";
                goto cleanup;
            }
            IntPtr iparg = Marshal.AllocCoTaskMem(nRet + 100);
            nRet = WinApis.DocumentProperties(IntPtr.Zero, hPrinter, sPrinterName, iparg, IntPtr.Zero, DEVMODEfModes.DM_OUT_BUFFER);
            if ((nRet < 0))
            {
                errorMessage = "Cannot get the DEVMODE structure.";
                goto cleanup;
            }
            dm = (DEVMODE)Marshal.PtrToStructure(iparg, dm.GetType());
        
            return iparg;

            cleanup:
            if ((hPrinter.ToInt64() != 0))
                WinApis.ClosePrinter(hPrinter);
            return default(IntPtr);
        }


        public static bool GetPrinterHasDuplex(string sPrinterName)
        {
            try
            {
                IntPtr iparg = GetDevMode(sPrinterName, out string error);

                if (string.IsNullOrWhiteSpace(error) && iparg != default)
                {
                    return GetDeviceCapabilities(sPrinterName, DM_DUPLEX) == 1;
                }
            }
            catch(Exception ex)
            {

            }
            return false;
        }

        public static bool GetPrinterHasColor(string sPrinterName)
        {
            try
            {
                IntPtr iparg = GetDevMode(sPrinterName, out string error);

                if (string.IsNullOrWhiteSpace(error) && iparg != default)
                {
                    return GetDeviceCapabilities(sPrinterName, DM_COLORDEVICE) == 1;
                }
            }
            catch (Exception ex)
            {

            }
            return false;
        }

        public static int GetDeviceCapabilities(string printerName, ushort capability)
        {
            int result = 0;
            PRINTER_INFO_5 info5;
            int requiredSize;
            int numPrinters;
            bool foundPrinter = WinApis.EnumPrintersW(PRINTER_ENUM_LOCAL | PRINTER_ENUM_CONNECTIONS, printerName, 5, (IntPtr)null, 0, out requiredSize, out numPrinters);
            Console.WriteLine("Required size is: {0}", requiredSize);
            int info5Size = requiredSize;
            IntPtr info5Ptr = Marshal.AllocHGlobal(info5Size);
            try
            {
                foundPrinter = WinApis.EnumPrintersW(PRINTER_ENUM_LOCAL | PRINTER_ENUM_CONNECTIONS, printerName, 5, info5Ptr, info5Size, out
                requiredSize, out numPrinters);
                Console.WriteLine("Size: {0}, required size: {1}, num printers:{2}", info5Size, requiredSize, numPrinters);
                string port = null;
                for (int i = 0; i < numPrinters; i++)
                {
                    info5 = (PRINTER_INFO_5)Marshal.PtrToStructure((IntPtr)((i * Marshal.SizeOf(typeof(PRINTER_INFO_5))) + (long)info5Ptr), typeof(PRINTER_INFO_5));
                    if (info5.pPrinterName == printerName)
                    {
                        port = info5.pPortName;
                    }
                    Console.WriteLine("Printer: '{0}', Port:'{1}'", info5.pPrinterName,
                    info5.pPortName);
                }
                IntPtr buffer;
                result = WinApis.DeviceCapabilities(printerName, port, capability, out buffer, (IntPtr)null);

            }
            catch (Exception ex)
            {
            }

            return result;
        }
    }
}