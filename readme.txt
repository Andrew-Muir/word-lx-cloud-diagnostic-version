DEPLOY STEPS FOR STAGING:
1.) Change branch to Full VSTO master
2.) Fetch -> Pull all changes
3.) Change the Assembly version -> Project -> Properties -> Application -> Assembly Information
4.) Change Publish versin as well to Assembly version: Project -> Properties -> Publish -> Publish version
4.) Signing -> Select from file -> IDICert.crt -> provide password for file
5.) Verify if app.config has staging urls
6.) Build -> Publish InfowareVsto -> Find the current pub number and increment by one and set the current pub name.
7.) Add to archive -> move to google drive (VSTO Publications)

ADDITIONAL DEPLOY STEPS FOR PRODUCTION:
5.) Comment staging WebApiUrl in App.config: https://infoware-stg.vmtinternal.net:4432/
6.) Uncomment production WebApiUrl https://wordlx.infowaregroup.net and remove port from url
6.) Continue with steps from staging