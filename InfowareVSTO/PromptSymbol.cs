﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfowareVSTO
{
    public class PromptSymbol
    {
        public int Code { get; set; }

        public string Font { get; set; }

        private static List<PromptSymbol> promptSymbols;

        public static List<PromptSymbol> GetPromptSymbols()
        {
            if (promptSymbols == null)
            {
                promptSymbols = new List<PromptSymbol>();
                string codes = SettingsManager.GetSetting("InsertCode", "PromptBullets") ?? "108";

                string fonts = SettingsManager.GetSetting("Font", "PromptBullets") ?? "Wingdings";

                string[] codeArr = SettingsManager.UnescapeCommaCharacter(codes.Split(','));

                string[] fontArr = SettingsManager.UnescapeCommaCharacter(fonts.Split(','));

                for (int i = 0; i < codeArr.Length; i++)
                {
                    if (int.TryParse(codeArr[i], out int codeInt))
                    {
                        string font = fontArr.Length > i ? fontArr[i] : fontArr[fontArr.Length - 1];

                        promptSymbols.Add(new PromptSymbol()
                        {
                            Code = codeInt,
                            Font = font
                        });
                    }
                }
            }

            return promptSymbols;
        }
    }
}
