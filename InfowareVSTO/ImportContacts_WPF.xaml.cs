﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using InfowareVSTO.Common.DTOs;
using System.Collections.ObjectModel;
using InfowareVSTO.Common;
using InfowareVSTO.Common.Models.ViewModels;
using InfowareVSTO.Common.Models;
using InfowareVSTO.Windows.ParagraphNumbering.Util;
using InfowareVSTO.Common.Language;
using DocumentFormat.OpenXml.Bibliography;
using Microsoft.Office.Interop.Outlook;
using InfowareVSTO.Windows;

namespace InfowareVSTO
{
    /// <summary>
    /// Interaction logic for SelectAuthor_WPF.xaml
    /// </summary>
    public partial class ImportContacts_WPF : InfowareWindow
    {
        private const string CHECKBOXES_STATE_PROP = "OutlookSources";
        private static JobTitleVisibility LastJobTitleVisibility { get; set; } = JobTitleVisibility.NoJobTitle;
        private ImportContactsViewModel ImportContactsViewModel { get; set; }

        public bool CompanyNameVisibility { get; set; }
        private static bool LastCompanyNameVisibility { get; set; } = true;

        public bool DepartmentVisibility { get; set; }
        private static bool LastDepartmentVisibility { get; set; } = false;

        public bool UseAttentionLineVisibility { get; set; }
        private static bool LastUseAttentionLineVisibility { get; set; } = true;
        
        private static OutlookContactType SelectedOutlookContact { get; set; } = OutlookContactType.Own;
        public ObservableCollection<LocalContact> LocalContactsList { get; set; }
        public static ObservableCollection<OutlookContact> OutlookContactsList { get; set; }
        private static Dictionary<OutlookSourceType, List<AuthorWithPathsDTO>> OutlookSources = new Dictionary<OutlookSourceType, List<AuthorWithPathsDTO>>();
        private static string outlookSourcesCurrentAuthor;
        private int? authorId;
        public JobTitleVisibility JobTitleVisibility { get; set; }
        public List<SelectedContact> SelectedContacts { get; set; }
        public List<SelectedContact> OldContacts { get; set; }
        public List<SelectedContact> RemovedContacts { get; set; }


        public bool ResultOk { get; set; }
        private bool canUpdate = false;

        public ImportContacts_WPF(RecipientType recipientType, bool showUseAsAttentionLine = false, DataGridSelectionMode dataGridSelectionMode = DataGridSelectionMode.Extended, int? authorId = null, Visibility CompanyNameStackPanelVisibility = Visibility.Visible, List<SelectedContact> oldContacts = null, bool useAddress = true)
        {
            InitializeComponent();

            if (!showUseAsAttentionLine)
            {
                UseAttentionLine.Visibility = Visibility.Hidden;
            }

            this.authorId = authorId;
            if (recipientType == RecipientType.SingleRow)
            {
                this.JobTitleOnSameRow.Visibility = Visibility.Collapsed;
                this.JobTitleOnSameRow.IsChecked = true;
                this.UseDepartment.Visibility = Visibility.Collapsed;
            }

            if (!useAddress)
            {
                this.UseAlternateAddress.Visibility = Visibility.Collapsed;
            }

            if (recipientType == RecipientType.NoJobTitle)
            {
                JobTitleStackPanel.Visibility = Visibility.Collapsed;
            }

            CompanyNameStackPanel.Visibility = CompanyNameStackPanelVisibility;

            LoadJobTitleVisibility();
            LoadCompanyNameVisibility();
            LoadUseAttentionLineVisibility();
            LoadDepartMentVisibility();

            object checkBoxesState = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, CHECKBOXES_STATE_PROP, null);
            if (checkBoxesState is int)
            {
                SetCheckboxes((int)checkBoxesState);
            }
            else
            {
                SetCheckboxes(1);
            }

            OldContacts = oldContacts;
            RemovedContacts = new List<SelectedContact>();
            SelectedContacts = new List<SelectedContact>();
            ResultOk = false;

            ImportContactsViewModel = new ImportContactsViewModel()
            {
                SelectionMode = dataGridSelectionMode
            };
            if (oldContacts != null)
            {
                foreach (SelectedContact oldContact in oldContacts)
                {
                    ImportContactsViewModel.SelectedContactsView.Add(oldContact);
                }
            }

            DataContext = ImportContactsViewModel;
        }

        private void LoadJobTitleVisibility()
        {
            switch (LastJobTitleVisibility)
            {
                case JobTitleVisibility.SameRow:
                    this.JobTitleOnSameRow.IsChecked = true;
                    this.UseJobTitle.IsChecked = true;
                    break;
                case JobTitleVisibility.NextRow:
                    this.UseJobTitle.IsChecked = true;
                    break;
            }
        }

        private void LoadCompanyNameVisibility()
        {
            if (LastCompanyNameVisibility == true)
            {
                UseCompanyName.IsChecked = true;
            }
            else
            {
                UseCompanyName.IsChecked = false;
            }
        }

        private void LoadDepartMentVisibility()
        {
            UseDepartment.IsChecked = LastDepartmentVisibility;
        }

        private void LoadUseAttentionLineVisibility()
        {
            if (LastUseAttentionLineVisibility == true)
            {
                UseAttentionLine.IsChecked = true;
            }
            else
            {
                UseAttentionLine.IsChecked = false;
            }
        }

        private void OnContentRendered(object sender, EventArgs e)
        {
            using (new LoadingWindowWrapper())
            {
                LocalContactsList = ParseLocalContacts(AdminPanelWebApi.GetAuthors());

                //outlookContactTypes.SelectionChanged -= OutlookContactTypes_SelectionChanged;
                //outlookContactTypes.SelectedValue = SelectedOutlookContact;
                //outlookContactTypes.SelectionChanged += OutlookContactTypes_SelectionChanged;

                ImportContactsViewModel.LocalContactsCollection = LocalContactsList;
              
              
            }
            canUpdate = true;
            UpdateOutlookContacts();
        }

        private void SetCheckboxes(int number)
        {
             this.Personal.IsChecked = (number & 0b1) > 0;
             this.CurrentAuthor.IsChecked = (number & 0b10) > 0;
             this.OtherAccounts.IsChecked = (number & 0b100) > 0;
             this.Shared.IsChecked = (number & 0b1000) > 0;
        }

        private int GetCheckboxes()
        {
            int result = 0;
            if (this.Personal.IsChecked == true)
            {
                result++;
            }
            if (this.CurrentAuthor.IsChecked == true)
            {
                result += 2;
            }
            if (this.OtherAccounts.IsChecked == true)
            {
                result += 4;
            }
            if (this.Shared.IsChecked == true)
            {
                result += 8;
            }

            return result;
        }

        private void UpdateOutlookContacts()
        {
            if (canUpdate)
            {
                using (new LoadingWindowWrapper())
                {
                    List<AuthorWithPathsDTO> allContacts = new List<AuthorWithPathsDTO>();
                    if (this.Personal.IsChecked == true)
                    {
                        allContacts.AddRange(GetOutlookContacts(OutlookSourceType.Personal));
                    }
                    if (this.CurrentAuthor.IsChecked == true)
                    {
                        allContacts.AddRange(GetOutlookContacts(OutlookSourceType.CurrentAuthor));
                        allContacts = BaseUtils.DistinctBy(allContacts, x => $"{x.CompanyName};{x.FirstName};{x.MiddleName};{x.LastName};{x.JobTitle}").ToList();
                    }
                    if (this.OtherAccounts.IsChecked == true)
                    {
                        allContacts.AddRange(GetOutlookContacts(OutlookSourceType.OtherAccounts));
                    }
                    if (this.Shared.IsChecked == true)
                    {
                        allContacts.AddRange(GetOutlookContacts(OutlookSourceType.Shared));
                    }

                    OutlookContactsList = ParseOutlookContacts(allContacts);
                    ImportContactsViewModel.OutlookContactsCollection = OutlookContactsList;
                }
            }
        } 

        private List<AuthorWithPathsDTO> GetOutlookContacts(OutlookSourceType sourceType)
        {

            string currentAuthorEmail = null;
            string currentAuthorFirstName = null;
            string currentAuthorLastName = null;
            if (sourceType == OutlookSourceType.CurrentAuthor || sourceType == OutlookSourceType.OtherAccounts)
            {
                int authorId = this.authorId ?? AdminPanelWebApi.GetCurrentAuthorId();
                if (authorId > 0)
                {
                    AuthorWithPathsDTO author = AdminPanelWebApi.GetAuthor(authorId);
                    if (author != null)
                    {
                        currentAuthorEmail = BaseUtils.GetContactFirstInformation(author.ContactInformations, "Email");
                        currentAuthorFirstName = author.FirstName;
                        currentAuthorLastName = author.LastName;
                    }
                }

                if (currentAuthorEmail != outlookSourcesCurrentAuthor)
                {
                    OutlookSources.Remove(OutlookSourceType.CurrentAuthor);
                    OutlookSources.Remove(OutlookSourceType.OtherAccounts);
                    outlookSourcesCurrentAuthor = currentAuthorEmail;
                }
            }

            List<AuthorWithPathsDTO> contacts = null;
            if(OutlookSources.TryGetValue(sourceType, out contacts))
            {
                return contacts;
            }

            contacts = BaseUtils.GetOutlookContactsByType(sourceType, currentAuthorEmail, currentAuthorFirstName, currentAuthorLastName);
            OutlookSources.Add(sourceType, contacts);
            return contacts;
        }

        public ObservableCollection<LocalContact> ParseLocalContacts(List<AuthorDTO> authors)
        {
            if(authors != null && authors.Count > 0)
            {
                var localContacts = new ObservableCollection<LocalContact>();
                foreach(var author in authors)
                {
                    localContacts.Add(new LocalContact
                    {
                        Id = author.Id,
                        CompanyName = author.Company,
                        FullName = BaseUtils.ImplodeNameList(new List<string> { author.FirstName, author.LastName }),
                        FirstName = author.FirstName,
                        LastName = author.LastName,
                        JobTitle = author.JobTitle
                    });
                }
                return new ObservableCollection<LocalContact>(localContacts.OrderBy(x=>x.LastName).ThenBy(x=>x.FirstName).ThenBy(x=>x.CompanyName));
            }
            return new ObservableCollection<LocalContact>();
        }

        public ObservableCollection<OutlookContact> ParseOutlookContacts(List<AuthorWithPathsDTO> authors)
        {
            if (authors != null && authors.Count > 0)
            {
                var outlookContacts = new ObservableCollection<OutlookContact>();
                foreach (var author in authors)
                {
                    outlookContacts.Add(new OutlookContact
                    {
                        Id = author.Id,
                        CompanyName = author.CompanyName,
                        FullName = BaseUtils.ImplodeNameList(new List<string> { author.Prefix, author.FirstName, author.LastName, author.Suffix }),
						Prefix = author.Prefix,
                        Suffix = author.Suffix,
                        FirstName = author.FirstName,
                        LastName = author.LastName,
                        JobTitle = author.JobTitle,
                        Addresses = author.Addresses,
                        ContactInformations = author.ContactInformations,
                        Companies = author.Companies,
                        ContactItem = author.ContactItem,
                        Department = author.Department,
                        AlternateAddresses = author.AlternateAddresses
                    });
                }
                return new ObservableCollection<OutlookContact>(outlookContacts.OrderBy(x => x.LastName).ThenBy(x => x.FirstName).ThenBy(x => x.CompanyName));
            }
            return new ObservableCollection<OutlookContact>();
        }

        private bool FilterLocalContacts(LocalContact localContact, string localSearch)
        {
            return string.IsNullOrEmpty(localSearch)
                || (!string.IsNullOrEmpty(localContact.CompanyName) ? localContact.CompanyName.IndexOf(localSearch, StringComparison.OrdinalIgnoreCase) != -1 : false)
                || (!string.IsNullOrEmpty(localContact.FullName) ? localContact.FullName.IndexOf(localSearch, StringComparison.OrdinalIgnoreCase) != -1 : false)
                || (!string.IsNullOrEmpty(localContact.JobTitle) ? localContact.JobTitle.IndexOf(localSearch, StringComparison.OrdinalIgnoreCase) != -1 : false);
        }

        private bool FilterOutlookContacts(OutlookContact outlookContact, string outlookSearch)
        {
            return string.IsNullOrEmpty(outlookSearch)
                || (!string.IsNullOrEmpty(outlookContact.CompanyName) ? outlookContact.CompanyName.IndexOf(outlookSearch, StringComparison.OrdinalIgnoreCase) != -1 : false)
                || (!string.IsNullOrEmpty(outlookContact.FullName) ? outlookContact.FullName.IndexOf(outlookSearch, StringComparison.OrdinalIgnoreCase) != -1 : false)
                || (!string.IsNullOrEmpty(outlookContact.JobTitle) ? outlookContact.JobTitle.IndexOf(outlookSearch, StringComparison.OrdinalIgnoreCase) != -1 : false);
        }

        private void ButtonInsert_Click(object sender, RoutedEventArgs e)
        {
            var selectedContacts = ImportContactsViewModel.SelectedContactsView;
            if(selectedContacts != null && selectedContacts.Count > 0)
            {
                foreach(var selectedContact in selectedContacts)
                {
                    if (OldContacts?.Contains(selectedContact) != true)
                    {
                        var contact = new SelectedContact
                        {
                            Id = selectedContact.Id,
                            CompanyName = selectedContact.CompanyName?.Trim(),
                            FullName = selectedContact.FullName?.Trim(),
                            Prefix = selectedContact.Prefix?.Trim(),
                            Suffix = selectedContact.Suffix?.Trim(),
                            FirstName = selectedContact.FirstName?.Trim(),
                            LastName = selectedContact.LastName?.Trim(),
                            JobTitle = selectedContact.JobTitle?.Trim(),
                            Addresses = selectedContact.Addresses,
                            Companies = selectedContact.Companies,
                            Department = selectedContact.Department,
                            ContactInformations = selectedContact.ContactInformations,
                            IsSelected = false
                        };

                        if (UseAttentionLine?.IsChecked ?? false)
                        {
                            contact.IsSelected = true;
                        }

                        if (selectedContact.Id != 0)
                        {
                            var author = AdminPanelWebApi.GetAuthor(selectedContact.Id, MultiLanguage.MLanguageUtil.ActiveDocumentLanguage);
                            if (author != null)
                            {
                                contact.Addresses = author.Addresses;
                                contact.Companies = author.Companies;
                                contact.ContactInformations = author.ContactInformations;
                            }
                        }

                        SelectedContacts.Add(contact);
                    }
                }
            }

            foreach(SelectedContact contact in RemovedContacts)
            {
                OldContacts.Remove(contact);
            }

            ResultOk = true;

            JobTitleVisibility = UseJobTitle.IsChecked == true ? (JobTitleOnSameRow.IsChecked == true ? JobTitleVisibility.SameRow : JobTitleVisibility.NextRow) : JobTitleVisibility.NoJobTitle;
            LastJobTitleVisibility = JobTitleVisibility;

            CompanyNameVisibility = UseCompanyName.IsChecked ?? false;
            LastCompanyNameVisibility = CompanyNameVisibility;

            DepartmentVisibility = UseDepartment.IsChecked ?? false;
            LastDepartmentVisibility = DepartmentVisibility;


            UseAttentionLineVisibility = UseAttentionLine?.IsChecked ?? false;
            LastUseAttentionLineVisibility = UseAttentionLineVisibility;

            RegistryManager.SaveToRegistry(CustomConstants.USE_ATTENTION_LINE, UseAttentionLineVisibility ? 1 : 0);

            Microsoft.Win32.Registry.SetValue(ConfigSettings.InfowareKeyFolder, CHECKBOXES_STATE_PROP, GetCheckboxes());

            Close();
	    }

		private void ButtonCancel_Click(object sender, RoutedEventArgs e)
		{
            ResultOk = false;
			Close();
		}	
		
		private void ButtonAdd_Click(object sender, RoutedEventArgs e)
		{
			if (tabOutlook.IsSelected)
			{

                Dictionary<OutlookContact, List<AuthorAddressDTO>> alternateAddresses = new Dictionary<OutlookContact, List<AuthorAddressDTO>>();
                if (UseAlternateAddress.IsChecked == true)
                {
                    foreach (OutlookContact contact in listViewContactsOutlook.SelectedItems)
                    {
                        alternateAddresses[contact] = contact.AlternateAddresses?.Where(x => !string.IsNullOrWhiteSpace(x.Address1)).ToList();
                    }

                    if (alternateAddresses.Values.Where(x=>x.Count > 1).Any())
                    {
                        AlternateAddressesDialog aad = new AlternateAddressesDialog(alternateAddresses);

                        aad.ShowDialog();
                    }
                }
                
                foreach (OutlookContact contact in listViewContactsOutlook.SelectedItems)
                {
                    var foundContact = ImportContactsViewModel.SelectedContactsView.Where(x => x.CompanyName == contact.CompanyName & x.FirstName == contact.FirstName & x.LastName == contact.LastName & x.JobTitle == contact.JobTitle).FirstOrDefault();
                    if (foundContact == null)
                    {
                        if (contact.ContactItem != null)
                        {
                            contact.Companies = new List<ACompanyDTO>() { new ACompanyDTO() { Name = contact.ContactItem.CompanyName } };
                            contact.ContactInformations = new List<AContactInformationDTO>() { new AContactInformationDTO() { Type = BaseUtils.PHONE, Details = contact.ContactItem.BusinessTelephoneNumber ?? contact.ContactItem.CompanyMainTelephoneNumber ?? contact.ContactItem.OtherTelephoneNumber },
                            new AContactInformationDTO(){ Type = BaseUtils.FAX, Details = contact.ContactItem.BusinessFaxNumber ?? contact.ContactItem.OtherFaxNumber} };
                            contact.Addresses = new List<AuthorAddressDTO>() { new AuthorAddressDTO() { Address1 = contact.ContactItem.BusinessAddressStreet?.Trim(), City = contact.ContactItem.BusinessAddressCity?.Trim(), Country = contact.ContactItem.BusinessAddressCountry?.Trim(), PostalCode = contact.ContactItem.BusinessAddressPostalCode?.Trim(), Province = contact.ContactItem.BusinessAddressState?.Trim() } };
                        }

                        if (alternateAddresses != null && alternateAddresses.TryGetValue(contact, out List<AuthorAddressDTO> value) && value.Count > 0)
                        {
                            contact.Addresses = new List<AuthorAddressDTO>() { value.FirstOrDefault() };
                        }

                        ImportContactsViewModel.SelectedContactsView.Add(new SelectedContact {
                            Id = contact.Id,
                            CompanyName = contact.CompanyName,
                            FullName = contact.FullName,
							Prefix = contact.Prefix,
                            Suffix = contact.Suffix,
                            FirstName = contact.FirstName,
                            LastName = contact.LastName,
                            JobTitle = contact.JobTitle,
                            Addresses = contact.Addresses,
                            ContactInformations = contact.ContactInformations,
                            Companies = contact.Companies,
                            Department = contact.Department
                        });
                    }
                }
            }
			else
			{
                foreach (LocalContact contact in listViewContactsLocal.SelectedItems)
                {
                    var foundContact = ImportContactsViewModel.SelectedContactsView.Where(x => x.Id == contact.Id).FirstOrDefault();
                    if (foundContact == null)
                    {
                        ImportContactsViewModel.SelectedContactsView.Add(new SelectedContact
                        {
                            Id = contact.Id,
                            CompanyName = contact.CompanyName,
                            FullName = contact.FullName,
                            FirstName = contact.FirstName,
                            LastName = contact.LastName,
                            JobTitle = contact.JobTitle
                        });
                    }
                }
			}
        }

		private void ButtonRemove_Click(object sender, RoutedEventArgs e)
		{
            var selectedItems = listViewContacts.SelectedItems.Cast<SelectedContact>().ToList();
            if (ImportContactsViewModel.SelectedContactsView.Count > 0 && selectedItems.Count > 0)
            {
                foreach (SelectedContact contact in selectedItems)
                {
                    if (ImportContactsViewModel.SelectedContactsView.Contains(contact))
                    {
                        ImportContactsViewModel.SelectedContactsView.Remove(contact);

                        if (OldContacts?.Contains(contact) == true)
                        {
                            RemovedContacts.Add(contact);
                        }
                    }
                }
            }
		}

		private void ButtonRemoveAll_Click(object sender, RoutedEventArgs e)
		{
            if(ImportContactsViewModel.SelectedContactsView.Count > 0)
            {
                ImportContactsViewModel.SelectedContactsView.Clear();
                RemovedContacts = OldContacts?.ToList();
            }
		}

      
        private void Row_DoubleClick(object sender, MouseButtonEventArgs e)
        {
            ButtonAdd_Click(null, null);
        }

        private void OutlookSource_Checked(object sender, RoutedEventArgs e)
        {
            UpdateOutlookContacts();
        }

        private void UseAsAttention_Checked(object sender, RoutedEventArgs e)
        {
            var uniqueId = (sender as CheckBox)?.Tag;
            var selectedContacts = ImportContactsViewModel.SelectedContactsView;
            if(uniqueId != null && selectedContacts != null)
            {
                foreach(SelectedContact selectedContact in selectedContacts)
                {
                    if(selectedContact != null && uniqueId?.ToString() != selectedContact.UniqueId)
                    {
                        selectedContact.IsSelected = false;
                    }
                }
            }
        }
    }
}
