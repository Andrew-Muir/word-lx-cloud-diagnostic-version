﻿using InfowareVSTO.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfowareVSTO
{
    public class RegistryManager
    {
        public static void SaveToRegistry(string key, int value)
        {
            Microsoft.Win32.Registry.SetValue(ConfigSettings.InfowareKeyFolder, key, value);
        }

        public static int? GetIntRegistryValue(string key)
        {
            return Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, key, null) as int?;
        }
        public static void SaveToRegistry(string key, string value)
        {
            Microsoft.Win32.Registry.SetValue(ConfigSettings.InfowareKeyFolder, key, value);
        }

        public static string GetStringRegistryValue(string key)
        {
            return Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, key, null) as string;
        }

        public static void RemoveKey(string keyName)
        {
            using (Microsoft.Win32.RegistryKey key = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(ConfigSettings.InfowareKeyFolder.Replace("HKEY_CURRENT_USER\\", ""), true))
            {
                if (key == null)
                {
                    // Key doesn't exist. Do whatever you want to handle
                    // this case
                }
                else
                {
                    key.DeleteValue(keyName);
                }
            }
        }
    }
}
