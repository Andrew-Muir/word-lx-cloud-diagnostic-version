﻿namespace InfowareVSTO.Constants
{
    internal static class HeaderFooterControlTags
    {
        public const string WATERMARK = "LX-WATERMARK";

        public const string PAGE_NUMBERING = "LX-PAGENUMBERING";

        public const string DOCUMENT_ID = "LX-DOCUMENTID";

        public const string LETTERHEAD = "LX-LETTERHEAD";

        public const string DRAFT_DATE_STAMP = "LX-DRAFTDATESTAMP";
    }
}
