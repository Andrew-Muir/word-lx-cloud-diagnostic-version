﻿namespace InfowareVSTO.Constants
{
    internal static class MeasurementConstants
    {
        public const float ONE_INCH = 72;
        public const float TWO_INCH = ONE_INCH * 2;
        public const float HALF_INCH = ONE_INCH / 2;
        public const float ONE_AND_A_HALF_INCH = ONE_INCH + HALF_INCH;
    }
}
