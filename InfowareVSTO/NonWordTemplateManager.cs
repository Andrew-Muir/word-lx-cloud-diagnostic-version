﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfowareVSTO
{
    public class NonWordTemplateManager
    {
        public static string TEMP_FOLDER = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\Infoware\\Temporary Files\\";

        public static List<string> WordFileTypes
        {
            get
            {
                return new List<string>()
                {
                    ".doc",
                    ".dot",
                    ".wbk",
                    ".docx",
                    ".docm",
                    ".dotx",
                    ".dotm",
                    ".docb"
                };
            }
        }

        public static void CreateTempFolderAndEmpty()
        {
            if (!Directory.Exists(TEMP_FOLDER))
            {
                Directory.CreateDirectory(TEMP_FOLDER);
            }

            try
            {
                foreach (string path in Directory.GetFiles(TEMP_FOLDER))
                {
                    File.Delete(path);
                }
            }
            catch { }
        }

        public static void SaveAndOpen(string name, byte[] templateBytes)
        {
            Random rand = new Random();
            int nr = rand.Next() % 100000;

            name = Path.GetFileNameWithoutExtension(name) + nr + Path.GetExtension(name);
            try
            {
                File.WriteAllBytes(TEMP_FOLDER + name, templateBytes);
                Process.Start(TEMP_FOLDER + name);
            }
            catch { }
        }
    }
}
