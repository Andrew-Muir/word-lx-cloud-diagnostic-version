﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using InfowareVSTO.Common;
using InfowareVSTO.Common.DTOs;
using InfowareVSTO.Common.General;
using InfowareVSTO.Common.Models;
using InfowareVSTO.Util;
using InfowareVSTO.VSTOAuthor;
using InfowareVSTO.Windows.ParagraphNumbering.Util;
using Microsoft.Office.Interop.Word;
using Microsoft.Office.Tools.Word;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using ContentControl = Microsoft.Office.Interop.Word.ContentControl;
using Document = Microsoft.Office.Interop.Word.Document;

namespace InfowareVSTO
{
    public partial class MultiPrintLabel_WPF : InfowareWindow
    {
        private AuthorDTO Author { get; set; }
        private List<SelectedContact> AddedRecipients { get; set; }
        public KeyValuePair<int, int> Cells { get; set; }
        public Document LabelDocument { get; set; }

        private readonly MultiPrintSettings PrintSettings;

        private readonly string PrinterName;
        public List<TemplateListModel> LabelTemplateList { get; set; }

        static MultiPrintLabel_WPF()
        {

        }

        public MultiPrintLabel_WPF(MultiPrintSettings printSettings, string printerName)
        {
            InitializeComponent();

            AddedRecipients = new List<SelectedContact>();
            PrintSettings = printSettings;
            PrinterName = printerName;
            var currentAuthorSettings = ManageUsers.CurrentAuthorSettings;
            LabelTemplateList = AdminPanelWebApi.GetTemplateList(TemplateType.Labels, currentAuthorSettings.CompanyId, currentAuthorSettings.LanguageId, currentAuthorSettings.LocationId);
            comboBoxOfficeLocation.ItemsSource = Utils.GetLocationsForCombobox();
            int defaultLocationId = -1;
         
            if (currentAuthorSettings.LocationId.HasValue)
            {
                defaultLocationId = currentAuthorSettings.LocationId.Value;
            }
            else
            {
                int? companyId = currentAuthorSettings.CompanyId ?? DocumentPropertyUtil.GetTemplateCompanyID();

                if (companyId.HasValue)
                {
                    defaultLocationId = AdminPanelWebApi.GetCompanyDetails(companyId.Value).Id;
                }
            }

            if (defaultLocationId >= 0)
            {
                comboBoxOfficeLocation.SelectedValue = defaultLocationId;
            }
            DataContext = this;
            Utils.PopulateComboboxWithTemplateOptions(comboBoxTemplateLabelMultiPrint, LabelTemplateList);
       
            SettingsManager.PopulateComboboxWithHandlingOptions(comboBoxHandlingLabelMultiPrint);
            SettingsManager.PopulateComboboxWithDeliveryOptions(comboBoxDeliveryLabelMultiPrint);
            LoadAnswers();

            SetDefaultSelectedTemplate();
        }
                
        public void LoadAnswers(PaneDTO paneDTO = null)
        {
            var labelDTO = TaskPaneUtil.LoadAnswers(paneDTO);
            if (labelDTO != null)
            {
                textBoxFileNumberLabelMultiPrint.Text = labelDTO.FileNumber;
                textBoxClientFileNumberLabelMultiPrint.Text = labelDTO.ClientFileNumber;
                textBoxInitialsLabelMultiPrint.Text = labelDTO.Initials ?? PaneUtils.GetAuthorInitials_Letter(labelDTO.Author);
                textBoxRecipientsLabelMultiPrint.Text = labelDTO.Recipients;
                comboBoxHandlingLabelMultiPrint.Text = labelDTO.Handling;
                comboBoxDeliveryLabelMultiPrint.Text = labelDTO.Delivery;
                textBoxAttentionLabelMultiPrint.Text = labelDTO.Attention;
                textBoxStartAtLabelNumberLabelMultiPrint.Text = labelDTO.StartAtLabelNumber > 0 ? labelDTO.StartAtLabelNumber.ToString() : "1";
                checkBoxUseFirstRecipientLabelMultiPrint.IsChecked = labelDTO.UseFirstRecipient;
                AddedRecipients = labelDTO.AddedRecipients ?? AddedRecipients;
                if (labelDTO.OfficeLocationId.HasValue)
                {
                    this.comboBoxOfficeLocation.SelectedValue = labelDTO.OfficeLocationId.Value;
                }
            }
        }

        public void ButtonOK_Click(object sender, EventArgs e)
        {
            Int32.TryParse(comboBoxTemplateLabelMultiPrint.SelectedValue?.ToString(), out int templateId);
            var templateName = (comboBoxTemplateLabelMultiPrint.SelectedItem as ComboboxItem)?.Text;

            if (templateId != 0 && templateName != null)
            {
                var downloadedTemplate = Utils.DownloadAndGetTemplate(new TemplateListModel { Id = templateId, Name = templateName }, createNew: false);
                if (downloadedTemplate?.Document != null)
                {
                    LabelDocument = downloadedTemplate.Document;
               
                    DocumentPropertyUtil.SaveShortProperty(CustomConstants.TemplateTypeTags, ((int)TemplateType.Labels).ToString(), true);
                }
            }

            if(LabelDocument != null)
            {
                bool isNumber = int.TryParse(textBoxStartAtLabelNumberLabelMultiPrint.Text, out int position);
                if (checkBoxUseFirstRecipientLabelMultiPrint.IsChecked == false && Cells.Key * Cells.Value > 0 && (isNumber == false || position <= 0 || position > Cells.Key * Cells.Value))
                {
                    string message = string.Format("Please enter a \"Start at Label Number\" value between 1 and {0}.", Cells.Key * Cells.Value);
                    var message_WPF = new Message_WPF(message);
                    message_WPF.ShowDialog();
                    return;
                }

                var selectedAuthor = new AuthorWithPathsDTO();
                if (Author != null)
                {
                    selectedAuthor = AdminPanelWebApi.GetAuthor(Author.Id);
                }

                var labelDTO = new PaneDTO
                {
                    FileNumber = textBoxFileNumberLabelMultiPrint.Text,
                    ClientFileNumber = textBoxClientFileNumberLabelMultiPrint.Text,
                    Initials = textBoxInitialsLabelMultiPrint.Text,
                    Handling = comboBoxHandlingLabelMultiPrint.SelectedValue is string ? comboBoxHandlingLabelMultiPrint.SelectedValue as string : comboBoxHandlingLabelMultiPrint.Text,
                    Author = selectedAuthor,
                    Attention = textBoxAttentionLabelMultiPrint.Text,
                    CompanyId = DocumentPropertyUtil.GetTemplateCompanyID(),
                    Recipients = textBoxRecipientsLabelMultiPrint.Text.Replace("\r\n", "\v"),
                    StartAtLabelNumber = position,
                    OfficeLocationId = comboBoxOfficeLocation.SelectedValue as int?,
                    UseFirstRecipient = checkBoxUseFirstRecipientLabelMultiPrint.IsChecked ?? false,
                    Delivery = comboBoxDeliveryLabelMultiPrint.Text,
                    AddedRecipients = AddedRecipients,
                    IncludeContactInformationDeliverySetting = SettingsManager.GetSetting("IncludeContactInformationDelivery", "Letters")
                };

                PaneUtils.InsertLabelTemplateDTO(LabelDocument, labelDTO);
                //TaskPaneUtil.SaveAnswers(labelDTO);
                if (this.IsVisible)
                {
                    this.DialogResult = true;
                }
            }
            this.Close();
        }

        private void ButtonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonFindRecipients_Click(object sender, EventArgs e)
        {
            var importContacts_WPF = new ImportContacts_WPF(RecipientType.MultiRow, true);
            importContacts_WPF.ShowDialog();

            var recipients = new List<string>();
            if (importContacts_WPF.ResultOk)
            {
                foreach (var recipient in importContacts_WPF.SelectedContacts)
                {
                    var grouppedLine = new List<string>();
                    var fullName = new List<string>();
                    if (!recipient.FirstName.IsEmpty())
                    {
                        fullName.Add(recipient.FirstName);
                    }
                    if (!recipient.LastName.IsEmpty())
                    {
                        fullName.Add(recipient.LastName);
                    }

                    string fullNameStr = string.Join(" ", fullName.ToArray());
                    if (importContacts_WPF.JobTitleVisibility == JobTitleVisibility.SameRow && !string.IsNullOrEmpty(recipient.JobTitle))
                    {
                        fullNameStr += ", " + recipient.JobTitle;
                    }

                    if (!recipient.IsSelected && !string.IsNullOrEmpty(fullNameStr))
                    {
                        grouppedLine.Add(fullNameStr);
                    }
                    if (!recipient.IsSelected && !recipient.JobTitle.IsEmpty() && importContacts_WPF.JobTitleVisibility == JobTitleVisibility.NextRow)
                    {
                        grouppedLine.Add(recipient.JobTitle);
                    }

                    var address = BaseUtils.GetAuthorAddress(recipient);
                    if (address != null && !string.IsNullOrWhiteSpace(address.Address1))                          
						grouppedLine.Add(string.Join("\r\n", address.LinesFormat));

                    if (recipient.IsSelected)
                    {
                        if (grouppedLine.Count > 0)
                        {
                            textBoxAttentionLabelMultiPrint.Text += BaseUtils.GetAttentionLine(recipient, importContacts_WPF.JobTitleVisibility) + "\n";
                        }
                        else
                        {
                            if (fullName.Count > 0)
                            {
                                grouppedLine.Add(fullNameStr);
                            }

                            if (!recipient.JobTitle.IsEmpty() && importContacts_WPF.JobTitleVisibility == JobTitleVisibility.NextRow)
                            {
                                grouppedLine.Add(recipient.JobTitle);
                            }
                        }
                    }
                    if (grouppedLine.Count > 0)
                    {
                        recipients.Add(string.Join("\r\n", grouppedLine));
                        AddedRecipients.Add(recipient);
                    }
                }
            }
            var recipientsStr = string.Join("\r\n\r\n", recipients.ToArray());
            if (textBoxRecipientsLabelMultiPrint.Text.Trim().IsEmpty())
            {
                textBoxRecipientsLabelMultiPrint.Text = recipientsStr;
            }
            else
            {
                textBoxRecipientsLabelMultiPrint.Text = textBoxRecipientsLabelMultiPrint.Text + "\r\n\r\n" + recipientsStr;
            }
        }

        private void buttonSelectStartAtLabelNumber_Click(object sender, EventArgs e)
        {
            int position = 1;
            int.TryParse(textBoxStartAtLabelNumberLabelMultiPrint.Text, out position);
            LabelGridStartingPosition_WPF labelGridStartingPosition_WPF = new LabelGridStartingPosition_WPF(Cells.Key, Cells.Value, position);
            labelGridStartingPosition_WPF.ShowDialog();
            if (labelGridStartingPosition_WPF.ResultOk)
            {
                textBoxStartAtLabelNumberLabelMultiPrint.Text = labelGridStartingPosition_WPF.SelectedPosition.ToString();
            }
        }

        private void buttonFindAuthor_Click(object sender, EventArgs e)
        {
            SelectAuthor_WPF selectAuthor_WPF = new SelectAuthor_WPF();
            selectAuthor_WPF.ShowDialog();
            if (selectAuthor_WPF.ResultOk)
            {
                Author = selectAuthor_WPF.SelectedAuthor;
            }
        }

        private void TextBoxRecipients_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (AddedRecipients.Count > 0)
            {
                var firstRecipientsRow = BaseUtils.GetRecipientTextFirstRow(textBoxRecipientsLabelMultiPrint.Text.Replace("\r\n", "\v"));
                if (firstRecipientsRow != null)
                {
                    var firstRecipient = AddedRecipients.FirstOrDefault();
                    if (firstRecipient != null)
                    {
                        var firstRecipientFullName = firstRecipient.FirstName + " " + firstRecipient.LastName;
                        if (firstRecipientsRow != firstRecipientFullName)
                        {
                            AddedRecipients.Clear();
                        }
                    }
                }
                else
                {
                    AddedRecipients.Clear();
                }
            }
        }

        private void ButtonCopyRecipientFirstRow_Click(object sender, RoutedEventArgs e)
        {
            var firstRecipientsRow = BaseUtils.GetRecipientTextFirstRow(textBoxRecipientsLabelMultiPrint.Text.Replace("\r\n", "\v"));
            if (firstRecipientsRow != null)
            {
                textBoxAttentionLabelMultiPrint.Text = firstRecipientsRow;
            }
        }

        private void SetDefaultSelectedTemplate()
        {
            if(!string.IsNullOrEmpty(PrinterName))
            {
                var printerGeneralSettings = PrintSettings.PrinterBinSettings.Where(x => x.Name == PrinterName).FirstOrDefault();
            
                comboBoxTemplateLabelMultiPrint.SelectedValue = Utils.GetAddressLabelComboBoxSelectedValue(printerGeneralSettings, LabelTemplateList, SettingsManager.GetSetting("AddressLabelPrinterTemplate", "MultiPrint"));

                if (comboBoxTemplateLabelMultiPrint.SelectedItem == null && comboBoxTemplateLabelMultiPrint.Items.Count > 0)
                {
                    comboBoxTemplateLabelMultiPrint.SelectedItem = comboBoxTemplateLabelMultiPrint.Items[0];
                }
            }
        }

        private void ComboBoxTemplateLabelMultiPrint_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var selectedTemplate = (sender as ComboBox);

            if (selectedTemplate != null && selectedTemplate.SelectedItem != null) 
            {
                using (new LoadingWindowWrapper())
                {
                    var template = Utils.DownloadAndGetTemplate(new TemplateListModel { Id = (int)(selectedTemplate?.SelectedItem as ComboboxItem).Value, Name = (selectedTemplate?.SelectedItem as ComboboxItem).Text }, false);

                    if (template != null && template?.Document != null)
                    {
                        Cells = DocumentUtils.GetLabelTemplatePositions(template.Document);

                        if (Cells.Key <= 1 && Cells.Value <= 1)
                        {
                            this.textBoxStartAtLabelNumberLabelMultiPrint.IsEnabled = false;
                            this.checkBoxUseFirstRecipientLabelMultiPrint.IsEnabled = false;
                            this.buttonSelectStartAtLabelNumber.IsEnabled = false;
                        }
                        else
                        {
                            this.textBoxStartAtLabelNumberLabelMultiPrint.IsEnabled = true;
                            this.checkBoxUseFirstRecipientLabelMultiPrint.IsEnabled = true;
                            this.buttonSelectStartAtLabelNumber.IsEnabled = true;
                        }
                    }
                }
            }
        }
    }
}
