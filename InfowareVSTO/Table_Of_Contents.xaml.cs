﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Word = Microsoft.Office.Interop.Word;
using Microsoft.Office.Interop.Word;
using Hyperlink = Microsoft.Office.Interop.Word.Hyperlink;
using System.Windows.Controls.Primitives;
using System.Text.RegularExpressions;
using InfowareVSTO.Windows;
using System.Threading;
using InfowareVSTO.Common.Language;
using InfowareVSTO.Windows.ParagraphNumbering.Model;
using InfowareVSTO.MultiLanguage;
using InfowareVSTO.Util;
using Microsoft.Office.Core;
using System.Diagnostics;
using InfowareVSTO.Settings;
using Microsoft.Office.Core;
using DocumentFormat.OpenXml.Wordprocessing;
using PageNumber = Microsoft.Office.Interop.Word.PageNumber;
using InfowareVSTO.Common;
using System.Windows.Xps;
using Style = Microsoft.Office.Interop.Word.Style;
using System.Globalization;

namespace InfowareVSTO
{
    public enum PositionEnum
    {
        InsertAtBeginning = 0,
        InsertAtCursor = 1,
        InsertAtEnd = 2
    }

    public partial class UserControl1 : InfowareWindow
    {
        int i, z, q, f, g, sec, page, NrTOCs;

        private const string FLOAT_REGEX = @"^[0-9]*\.?[0-9]*$";
        private const string NATURAL_NO_REGEX = @"^[0-9]*$";
        private string FLOAT_CONVERSION_ERROR_MESSAGE = LanguageManager.GetTranslation(LanguageConstants.RightTabIncorrectFormat, "Right Tab input is not in the correct format."); //"Right Tab input is not in the correct format.";
        private string FLOAT_CONVERSION_ERROR_TITLE = LanguageManager.GetTranslation(LanguageConstants.IncorrectFormat, "Incorrect format.");  //"Incorrect format.";
        private const string PAGE_NUMBERING_CONTROL_TAG = "LX-PAGENUMBERING";
        private const string SERIALIZER_PREFIX = "TDS";
        private const string TITLE_TAG = "LX-TOCTITLE";
        private const string PAGE_TITLE_TAG = "LX-TOCPAGETITLE";
        private Bookmark currentTocBookmark = null;
        private Range TrimmedCurrentTocBookmarkRange
        {
            get
            {
                if (currentTocBookmark != null)
                {
                    Range range = currentTocBookmark.Range;

                    if (range.Text.Length > 0)
                    {
                        Range last = range.Duplicate;
                        last.Collapse(WdCollapseDirection.wdCollapseEnd);
                        last.MoveStart(WdUnits.wdCharacter, -1);
                        if (last.Text == "\f")
                        {
                            range.MoveEnd(WdUnits.wdCharacter, -1);
                        }

                        Range first = range.Duplicate;
                        first.Collapse(WdCollapseDirection.wdCollapseStart);
                        first.MoveEnd(WdUnits.wdCharacter, 1);
                        if (first.Text == "\f")
                        {
                            range.MoveStart(WdUnits.wdCharacter, 1);
                        }
                    }

                    return range;
                }

                return null;
            }
        }

        private static List<KeyValuePair<PositionEnum, string>> positions = new List<KeyValuePair<PositionEnum, string>>()
        {
            new KeyValuePair<PositionEnum, string>(PositionEnum.InsertAtBeginning, LanguageManager.GetTranslation(LanguageConstants.InsertAtBeginning, "Insert at Beginning")),
            new KeyValuePair<PositionEnum, string>(PositionEnum.InsertAtCursor,LanguageManager.GetTranslation(LanguageConstants.InsertAtCursor, "Insert at Cursor")),
            new KeyValuePair<PositionEnum, string>(PositionEnum.InsertAtEnd, LanguageManager.GetTranslation(LanguageConstants.InsertAtEnd,"Insert at End"))
        };

        float Pos, Spacing;
        object sameTabLeader;
        string level;
        public bool centered = false;
        bool[] bold = new bool[15];
        bool[] allcaps = new bool[15];
        bool[] spacebefore = new bool[15];
        bool[] spaceafter = new bool[15];
        int SelectedPropertyItem = 1;
        int NumericUpDownAfter = 0;
        int NumericUpDownBefore = 0;
        List<string> list = new List<string>();
        bool[] LevelForDelete = new bool[10];
        public Range RangeForDelete;
        Word.Application app = ThisAddIn.Instance.Application;
        public Microsoft.Office.Interop.Word.Document DOC = ThisAddIn.Instance.Application.ActiveDocument;
        string[] tableader = new string[] { "TabLeader1", "TabLeader2", "TabLeader3", "TabLeader4", "TabLeader5", "TabLeader6", "TabLeader7", "TabLeader8", "TabLeader9" };
        string[] tabType = new string[] { LanguageManager.GetTranslation(LanguageConstants.None, "(None)"), ". . . . . . . . . .", "- - - - - - -", "____________" };
        int[] tabTypeValues = new int[] { 0, 1, 2, 4 };
        List<System.Windows.Controls.ComboBox> TabLeaders = new List<System.Windows.Controls.ComboBox>();
        List<System.Windows.Controls.CheckBox> PageNumbers = new List<System.Windows.Controls.CheckBox>();
        InfowareVSTO.ComboboxItem COM = new InfowareVSTO.ComboboxItem();
        private string sectionCaption;
        private string SectionCaption
        {
            get
            {
                if (sectionCaption == null)
                {
                    sectionCaption = SettingsManager.GetSetting("TocHeaderSectionCaption", "Sections", "", ActiveLanguage);
                }

                return sectionCaption;
            }
        }

        private string PageCaption
        {
            get
            {
                return TextPag.Text;
            }
        }

        private MsoLanguageID activeLanguage = MsoLanguageID.msoLanguageIDNone;
        private MsoLanguageID ActiveLanguage
        {
            get
            {
                if (activeLanguage == MsoLanguageID.msoLanguageIDNone)
                {
                    activeLanguage = MLanguageUtil.ActiveDocumentLanguage;
                }

                return activeLanguage;
            }
        }

        public List<List<string>> lists = new List<List<string>>();

        public UserControl1()
        {
            InitializeComponent();
            RightTab.Text = "6.5";

            TitleTB.Text = SettingsManager.GetSetting("TOCDefaultTitle", "Sections", MLanguageUtil.GetResource(MLanguageResourceEnum.TOCTitle), ActiveLanguage);
            TextPag.Text = SettingsManager.GetSetting("TocHeaderPageCaption", "Sections", MLanguageUtil.GetResource(MLanguageResourceEnum.TOCPage), ActiveLanguage);

            bool tocShowExtOpts = SettingsManager.GetSettingAsBool("TOCShowExtOpts", "Sections", true);
            if (!tocShowExtOpts)
            {
                OptionsTabItem.Visibility = Visibility.Collapsed;
            }

            int maxNrOfOutlineLevel = SettingsManager.GetSettingAsInt("TOCDefaultMaxOutlineLevel", "Sections", 2);

            for (int i = 1; i <= 9; i++)
            {
                OutlineLevels.Items.Add(i);
            }
            OutlineLevels.SelectedIndex = maxNrOfOutlineLevel < 10 && maxNrOfOutlineLevel > 0 ? maxNrOfOutlineLevel - 1 : 1;

            this.TabLeaders.Add(TabLeader1);
            this.TabLeaders.Add(TabLeader2);
            this.TabLeaders.Add(TabLeader3);
            this.TabLeaders.Add(TabLeader4);
            this.TabLeaders.Add(TabLeader5);
            this.TabLeaders.Add(TabLeader6);
            this.TabLeaders.Add(TabLeader7);
            this.TabLeaders.Add(TabLeader8);
            this.TabLeaders.Add(TabLeader9);


            this.SameTabLeader.IsChecked = true;
            List<KeyValuePair<int, string>> tabLeaderValues = new List<KeyValuePair<int, string>>();
            for (int i = 0; i < tabType.Length; i++)
            {
                tabLeaderValues.Add(new KeyValuePair<int, string>(tabTypeValues[i], tabType[i]));
            }

            foreach (System.Windows.Controls.ComboBox tab in this.TabLeaders)
            {
                tab.ItemsSource = tabLeaderValues;
                tab.DisplayMemberPath = "Value";
                tab.SelectedValuePath = "Key";

                int defaultTabLeader = SettingsManager.GetSettingAsInt("TOCDefaultTabLeader", "Sections", 1);

                if (tabTypeValues.Contains(defaultTabLeader))
                {
                    tab.SelectedValue = defaultTabLeader;
                }
                else
                {
                    tab.SelectedValue = 1;
                }
            }

            bool includeTCFields = SettingsManager.GetSettingAsBool("TOCDefaultIncludeTCFields", "Sections", false);
            UseRunInHeadings.IsChecked = includeTCFields;

            this.Position.ItemsSource = positions;
            this.Position.SelectedIndex = 0;

            int positionIndex = SettingsManager.GetSettingAsInt("TOCDefaultLocation", "Sections", 0);
            if (positionIndex >= 0 && positionIndex < 3)
            {
                this.Position.SelectedItem = this.Position.Items[positionIndex];
            }
            else
            {
                this.Position.SelectedItem = this.Position.Items[0];
            }

            this.PageNumbers.Add(PageNum1);
            this.PageNumbers.Add(PageNum2);
            this.PageNumbers.Add(PageNum3);
            this.PageNumbers.Add(PageNum4);
            this.PageNumbers.Add(PageNum5);
            this.PageNumbers.Add(PageNum6);
            this.PageNumbers.Add(PageNum7);
            this.PageNumbers.Add(PageNum8);
            this.PageNumbers.Add(PageNum9);

            foreach (System.Windows.Controls.CheckBox page in this.PageNumbers)
            {
                page.IsChecked = true;
            }

            this.lists.Add((new List<string>() { "Heading 1" }));
            this.lists.Add((new List<string>() { "Heading 2" }));
            this.lists.Add((new List<string>() { "Heading 3" }));
            this.lists.Add((new List<string>() { "Heading 4" }));
            this.lists.Add((new List<string>() { "Heading 5" }));
            this.lists.Add((new List<string>() { "Heading 6" }));
            this.lists.Add((new List<string>() { "Heading 7" }));
            this.lists.Add((new List<string>() { "Heading 8" }));
            this.lists.Add((new List<string>() { "Heading 9" }));

            string tocIncludeStyles = SettingsManager.GetSetting("TOCIncludeStyles", "Sections");
            if (tocIncludeStyles != null)
            {
                string[] tocIncludeStylesArr = tocIncludeStyles.Split(';');

                foreach (string style in tocIncludeStylesArr)
                {
                    string[] styleArr = style.Split(',');
                    int levelNr = 0;
                    if (styleArr.Length > 2 && int.TryParse(styleArr[2], out levelNr))
                    {
                        lists[levelNr - 1].Add(styleArr[1].Trim());
                    }
                }
            }

            this.OutlineLevel.IsChecked = true;
            foreach (Field field in DOC.Fields)
            {
                if (field.Type == WdFieldType.wdFieldTOC)
                {
                    field.Code.Text += @" \x";
                    NrTOCs++;
                }
            }
            if (NrTOCs > 0)
            {
                for (int i = 1; i <= NrTOCs; i++)
                {
                    page = (int)DOC.TablesOfContents[i].Range.Information[Word.WdInformation.wdActiveEndAdjustedPageNumber];
                    sec = (int)DOC.TablesOfContents[i].Range.Information[Word.WdInformation.wdActiveEndSectionNumber];
                    string section = $"Section {sec.ToString()} (Page {page.ToString()})";
                    ListBoxItem item = new ListBoxItem();
                    item.Content = section;
                    BrowseList.Items.Add(item);
                }
                BrowseList.SelectedItem = BrowseList.Items[0];
            }

            LineSpacing.ItemsSource = new List<LineSpacingOption>(){ new LineSpacingOption(LanguageManager.GetTranslation(LanguageConstants.Single, "Single"), WdLineSpacing.wdLineSpaceSingle),
                new LineSpacingOption(LanguageManager.GetTranslation(LanguageConstants.OneP5Lines, "1.5 lines"), WdLineSpacing.wdLineSpace1pt5),
                new LineSpacingOption(LanguageManager.GetTranslation(LanguageConstants.Double, "Double"), WdLineSpacing.wdLineSpaceDouble) };

            LineSpacing.SelectedItem = this.LineSpacing.Items[0];
            for (int j = 0; j <= 14; j++)
            {
                bold[j] = false;
                allcaps[j] = false;
                spacebefore[j] = false;
                spaceafter[j] = false;
            }

            bool tocNoSpaceBetween = SettingsManager.GetSettingAsBool("TOCNoSpaceBetween", "Sections", false);
            if (tocNoSpaceBetween)
            {
                this.NoSpaceSameLevel.IsChecked = true;
            }

            int spaceBeforeVal = SettingsManager.GetSettingAsInt("TOCSpaceBefore", "Sections", int.MinValue);
            if (spaceBeforeVal != int.MinValue)
            {
                NumericUpDownBefore = spaceBeforeVal;
                SpacingValue_Before.Text = NumericUpDownBefore.ToString() + " pt";
            }

            int spaceAfterVal = SettingsManager.GetSettingAsInt("TOCSpaceAfter", "Sections", int.MinValue);
            if (spaceAfterVal != int.MinValue)
            {
                NumericUpDownAfter = spaceAfterVal;
                SpacingValue_After.Text = NumericUpDownAfter.ToString() + " pt";
            }

            //find if cursor is in toc.
            var selectionRange = ThisAddIn.Instance.Application.Selection.Range;
            foreach (Bookmark bookmark in DOC.Bookmarks)
            {
                if (bookmark.Name.IndexOf("TOC_") == 0 && selectionRange.InRange(bookmark.Range))
                {
                    currentTocBookmark = bookmark;
                    ReadTitlesFromCC(bookmark);
                    ReadStylesFromTocField(bookmark);
                    this.Position.IsEnabled = false;
                    break;
                }
            }

            var continuousPageNumbering = SettingsManager.GetSettingAsBool("ContinuousPageNumbering", "Tools", false);

            if (continuousPageNumbering == false)
            {
                var firstPageHasPageNumber = SettingsManager.GetSettingAsInt("FirstPageHasPageNumber", "Sections", -1);

                if (firstPageHasPageNumber == -1 || firstPageHasPageNumber == 1)
                {
                    this.FirstPageNumbering.IsChecked = true;
                }
                else
                {
                    this.FirstPageNumbering.IsChecked = false;
                }
            }
        }

        private void ReadStylesFromTocField(Bookmark bookmark)
        {
            if (bookmark != null)
            {
                foreach (Field field in bookmark.Range.Fields)
                {
                    if (field.Type == WdFieldType.wdFieldTOC)
                    {
                        lists = new List<List<string>>();
                        for (int i = 0; i < 9; i++)
                        {
                            lists.Add(new List<string>());
                        }
                        string fieldCode = field.Code.Text;
                        int tIndex = fieldCode.IndexOf("\\t");

                        if (tIndex >= 0)
                        {
                            int firstSlashIndex = fieldCode.Substring(tIndex).IndexOf("\"") + tIndex;
                            if (firstSlashIndex >= 0)
                            {
                                int secondSlashIndex = fieldCode.Substring(firstSlashIndex + 1).IndexOf("\"") + firstSlashIndex + 1;

                                if (secondSlashIndex >= 0)
                                {
                                    string stylesString = fieldCode.Substring(firstSlashIndex + 1, secondSlashIndex - firstSlashIndex - 1);

                                    string[] stylesArr = stylesString.Split(',');

                                    for (int i = 0; i < stylesArr.Length / 2; i++)
                                    {
                                        if (int.TryParse(stylesArr[2 * i + 1], out int result))
                                        {
                                            lists[result - 1].Add(stylesArr[2 * i]);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private void ReadTitlesFromCC(Bookmark bookmark)
        {

            if (bookmark != null)
            {
                foreach (Microsoft.Office.Interop.Word.ContentControl cc in bookmark.Range.ContentControls)
                {
                    if (cc.Title == TITLE_TAG)
                    {
                        this.TitleTB.Text = cc.Range.Text;

                        try
                        {
                            this.ReadSerializedData(cc.Tag);
                        }
                        catch (Exception ex)
                        {
                            Debug.WriteLine(ex);
                        }
                    }

                    if (cc.Tag == PAGE_TITLE_TAG)
                    {
                        this.TextPag.Text = cc.Range.Text;
                    }
                }
            }
        }

        private void ReadSerializedData(string serializedData)
        {
            TOCSerializer tocSerializer = new TOCSerializer();

            tocSerializer.Deserialize(serializedData);
            this.SameTabLeader.IsChecked = tocSerializer.SameTabLeader;

            this.TabLeader1.Text = tabType[tocSerializer.TabLeaders[0]];
            this.TabLeader2.Text = tabType[tocSerializer.TabLeaders[1]];
            this.TabLeader3.Text = tabType[tocSerializer.TabLeaders[2]];
            this.TabLeader4.Text = tabType[tocSerializer.TabLeaders[3]];
            this.TabLeader5.Text = tabType[tocSerializer.TabLeaders[4]];
            this.TabLeader6.Text = tabType[tocSerializer.TabLeaders[5]];
            this.TabLeader7.Text = tabType[tocSerializer.TabLeaders[6]];
            this.TabLeader8.Text = tabType[tocSerializer.TabLeaders[7]];
            this.TabLeader9.Text = tabType[tocSerializer.TabLeaders[8]];

            this.PageNum1.IsChecked = tocSerializer.IncludePageNumber[0];
            this.PageNum2.IsChecked = tocSerializer.IncludePageNumber[1];
            this.PageNum3.IsChecked = tocSerializer.IncludePageNumber[2];
            this.PageNum4.IsChecked = tocSerializer.IncludePageNumber[3];
            this.PageNum5.IsChecked = tocSerializer.IncludePageNumber[4];
            this.PageNum6.IsChecked = tocSerializer.IncludePageNumber[5];
            this.PageNum7.IsChecked = tocSerializer.IncludePageNumber[6];
            this.PageNum8.IsChecked = tocSerializer.IncludePageNumber[7];
            this.PageNum9.IsChecked = tocSerializer.IncludePageNumber[8];

            this.OutlineLevel.IsChecked = tocSerializer.OutlineLevels > 0;
            this.OutlineLevels.Text = tocSerializer.OutlineLevels.ToString();

            this.PreservedLineBreaks.IsChecked = tocSerializer.PreserLineBreaks;
            this.ExcludeSectionBreaks.IsChecked = tocSerializer.ExcludeSectionBreaks;

            this.AddendaTable.IsChecked = tocSerializer.AddendaTable;
            this.FirstPageNumbering.IsChecked = tocSerializer.IncludePageNumberingFirstPage;
            this.UseRunInHeadings.IsChecked = tocSerializer.RunInHeadings;

            switch (tocSerializer.LineSpacing)
            {
                case 0:
                    this.LineSpacing.SelectedValue = WdLineSpacing.wdLineSpaceSingle;
                    break;
                case 1:
                    this.LineSpacing.SelectedValue = WdLineSpacing.wdLineSpace1pt5;
                    break;
                case 2:
                    this.LineSpacing.SelectedValue = WdLineSpacing.wdLineSpaceDouble;
                    break;
            }

            this.TOCAlignmentCenter.IsChecked = tocSerializer.CenterAlignment;
            this.TOCAlignmentLeft.IsChecked = tocSerializer.LeftAlignment;
            this.Two_Columns.IsChecked = tocSerializer.TwoColumn;
            this.Centered.IsChecked = tocSerializer.Toc1Centered;
            this.NoSpaceSameLevel.IsChecked = tocSerializer.NoSpaceBetweenEntriesOfSameLevel;
            for (int i = 0; i < 9; i++)
            {
                bold[i + 1] = tocSerializer.Bold[i];
            }
            for (int i = 0; i < 9; i++)
            {
                allcaps[i + 1] = tocSerializer.AllCaps[i];
            }
            for (int i = 0; i < 9; i++)
            {
                spaceafter[i + 1] = tocSerializer.SpaceAfter12[i];
            }
            for (int i = 0; i < 9; i++)
            {
                spacebefore[i + 1] = tocSerializer.SpaceBefore12[i];
            }
            this.NumericUpDownAfter = tocSerializer.AfterEachLevel - 1;
            this.Spacing_After_Up(null, null);
            this.NumericUpDownBefore = tocSerializer.BeforeEachLevel - 1;
            this.Spacing_Before_Up(null, null);
            this.RightTab.Text = ((double)tocSerializer.RightTab / 10).ToString("F1");
            Refresh_Commands();

        }

        string History(int args)
        {
            string history = "";
            if (args != 1 && args != 2 && args != 3 && args != 4)
            {
                return "";
            }
            bool[] aux = ((args == 1) ? bold : (((args == 2) ? allcaps : (((args == 3) ? spacebefore : spaceafter)))));
            for (int j = 1; j < 10; j++)
            {
                if (aux[j] == true)
                {
                    int pressed = j;
                    do
                    {
                        pressed++;
                    } while ((aux[pressed] != false) && (pressed >= j));
                    int cons_res = pressed - 1;
                    if (cons_res > j + 2)
                    {
                        int d = j;
                        j = cons_res;
                        history += d.ToString() + "-" + cons_res + " ";
                    }
                    else
                    {
                        history += j.ToString() + " ";
                    }
                    if (aux[10] == true) history += "All Levels";
                }
            }
            return history + "\n";
        }

        string Comma(string source)
        {
            for (int g = 0; g < source.Length; g++)
            {
                if ((source[g] == ',') && (g + 1 > source.Length))
                {
                    string copy = source.Substring(0, g - 1);
                    source = copy;
                }
            }
            return source;
        }

        void Restore_Bold_Buttons()
        {
            for (int j = 1; j <= 9; j++)
            {
                switch (bold[j])
                {
                    case true:
                        {
                            switch (j)
                            {
                                case 1: TOCLevel1.IsChecked = true; break;
                                case 2: TOCLevel2.IsChecked = true; break;
                                case 3: TOCLevel3.IsChecked = true; break;
                                case 4: TOCLevel4.IsChecked = true; break;
                                case 5: TOCLevel5.IsChecked = true; break;
                                case 6: TOCLevel6.IsChecked = true; break;
                                case 7: TOCLevel7.IsChecked = true; break;
                                case 8: TOCLevel8.IsChecked = true; break;
                                case 9: TOCLevel9.IsChecked = true; break;
                            }
                        }; break;
                    case false:
                        {
                            switch (j)
                            {
                                case 1: TOCLevel1.IsChecked = false; break;
                                case 2: TOCLevel2.IsChecked = false; break;
                                case 3: TOCLevel3.IsChecked = false; break;
                                case 4: TOCLevel4.IsChecked = false; break;
                                case 5: TOCLevel5.IsChecked = false; break;
                                case 6: TOCLevel6.IsChecked = false; break;
                                case 7: TOCLevel7.IsChecked = false; break;
                                case 8: TOCLevel8.IsChecked = false; break;
                                case 9: TOCLevel9.IsChecked = false; break;
                            }
                        }; break;
                }
            }
        }

        void Restore_SpaceBefore_Buttons()
        {
            for (int j = 1; j <= 9; j++)
            {
                switch (spacebefore[j])
                {
                    case true:
                        {
                            switch (j)
                            {
                                case 1: TOCLevel1.IsChecked = true; break;
                                case 2: TOCLevel2.IsChecked = true; break;
                                case 3: TOCLevel3.IsChecked = true; break;
                                case 4: TOCLevel4.IsChecked = true; break;
                                case 5: TOCLevel5.IsChecked = true; break;
                                case 6: TOCLevel6.IsChecked = true; break;
                                case 7: TOCLevel7.IsChecked = true; break;
                                case 8: TOCLevel8.IsChecked = true; break;
                                case 9: TOCLevel9.IsChecked = true; break;
                            }
                        }; break;
                    case false:
                        {
                            switch (j)
                            {
                                case 1: TOCLevel1.IsChecked = false; break;
                                case 2: TOCLevel2.IsChecked = false; break;
                                case 3: TOCLevel3.IsChecked = false; break;
                                case 4: TOCLevel4.IsChecked = false; break;
                                case 5: TOCLevel5.IsChecked = false; break;
                                case 6: TOCLevel6.IsChecked = false; break;
                                case 7: TOCLevel7.IsChecked = false; break;
                                case 8: TOCLevel8.IsChecked = false; break;
                                case 9: TOCLevel9.IsChecked = false; break;
                            }
                        }; break;
                }
            }
        }

        void Restore_AllCaps_Buttons()
        {
            for (int j = 1; j <= 9; j++)
            {
                switch (allcaps[j])
                {
                    case true:
                        {
                            switch (j)
                            {
                                case 1: TOCLevel1.IsChecked = true; break;
                                case 2: TOCLevel2.IsChecked = true; break;
                                case 3: TOCLevel3.IsChecked = true; break;
                                case 4: TOCLevel4.IsChecked = true; break;
                                case 5: TOCLevel5.IsChecked = true; break;
                                case 6: TOCLevel6.IsChecked = true; break;
                                case 7: TOCLevel7.IsChecked = true; break;
                                case 8: TOCLevel8.IsChecked = true; break;
                                case 9: TOCLevel9.IsChecked = true; break;
                            }
                        }; break;
                    case false:
                        {
                            switch (j)
                            {
                                case 1: TOCLevel1.IsChecked = false; break;
                                case 2: TOCLevel2.IsChecked = false; break;
                                case 3: TOCLevel3.IsChecked = false; break;
                                case 4: TOCLevel4.IsChecked = false; break;
                                case 5: TOCLevel5.IsChecked = false; break;
                                case 6: TOCLevel6.IsChecked = false; break;
                                case 7: TOCLevel7.IsChecked = false; break;
                                case 8: TOCLevel8.IsChecked = false; break;
                                case 9: TOCLevel9.IsChecked = false; break;
                            }
                        }; break;
                }
            }
        }

        void Restore_SpaceAfter_Buttons()
        {
            for (int j = 1; j <= 9; j++)
            {
                switch (spaceafter[j])
                {
                    case true:
                        {
                            switch (j)
                            {
                                case 1: TOCLevel1.IsChecked = true; break;
                                case 2: TOCLevel2.IsChecked = true; break;
                                case 3: TOCLevel3.IsChecked = true; break;
                                case 4: TOCLevel4.IsChecked = true; break;
                                case 5: TOCLevel5.IsChecked = true; break;
                                case 6: TOCLevel6.IsChecked = true; break;
                                case 7: TOCLevel7.IsChecked = true; break;
                                case 8: TOCLevel8.IsChecked = true; break;
                                case 9: TOCLevel9.IsChecked = true; break;
                            }
                        }; break;
                    case false:
                        {
                            switch (j)
                            {
                                case 1: TOCLevel1.IsChecked = false; break;
                                case 2: TOCLevel2.IsChecked = false; break;
                                case 3: TOCLevel3.IsChecked = false; break;
                                case 4: TOCLevel4.IsChecked = false; break;
                                case 5: TOCLevel5.IsChecked = false; break;
                                case 6: TOCLevel6.IsChecked = false; break;
                                case 7: TOCLevel7.IsChecked = false; break;
                                case 8: TOCLevel8.IsChecked = false; break;
                                case 9: TOCLevel9.IsChecked = false; break;
                            }
                        };
                        break;
                }
            }
        }

        private void Refresh_Commands()
        {
            string histBold = History(1);
            string histAllCaps = History(2);
            string histSpaceBefore = History(3);
            string histSpaceAfter = History(4);
            Commands.Content = "";
            if (histBold != "\n")
            {
                Commands.Content += "Bold" + "\n" + "TOC " + histBold;
            }
            if (histAllCaps != "\n")
            {
                Commands.Content += "All Caps" + "\n" + "TOC " + histAllCaps;
            }
            if (histSpaceBefore != "\n")
            {
                Commands.Content += "Space Before" + "\n" + "TOC " + histSpaceBefore;
            }
            if (histSpaceAfter != "\n")
            {
                Commands.Content += "Space After" + "\n" + "TOC " + histSpaceAfter;
            }
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string s = TitleTB.Text;
        }

        private void Level_Click(object sender, RoutedEventArgs e)
        {
            var button = (sender as Button);
            if (button != null)
            {
                var level = button.TabIndex;

                int? outlineLevel = null;
                if (this.OutlineLevel.IsChecked == true && int.Parse(this.OutlineLevels.Text) >= level)
                {
                    outlineLevel = level;
                }

                var tableOfContentStyles = new TableOfContentsStyles(this.lists[(level - 1)], outlineLevel);
                tableOfContentStyles.ShowDialog();

                this.lists[(level - 1)] = tableOfContentStyles.GetSelectedStyles();
            }
        }

        void RefreshLevelIndex(int k, TableOfContents table)
        {
            // level k was already deleted from table of contents ==> needs update
            for (int j = k; j < table.Range.Paragraphs.Count - 1; j++)
            {
                this.LevelForDelete[j] = this.LevelForDelete[j + 1];
            }

        }

        private void OK_Button(object sender, RoutedEventArgs e)
        {
            //dynamic dialog = ThisAddIn.Instance.Application.Dialogs[WdWordDialog.wdDialogFormatStyle];
            //dialog.Name = "Heading 1";
            //dialog.New();
            //dialog.Show();

            Range currentTocRange = TrimmedCurrentTocBookmarkRange;
            Range startingPointBeginning = null;
            List<Bookmark> bookmarksStartingAt0 = null;
            int? startCurrentToc = currentTocRange?.Start;
            if (currentTocRange != null)
            {
                currentTocRange.Delete();
            }

            Range startRange, endRange;
            int end;

            string TabelName = TitleTB.Text;
            Microsoft.Office.Interop.Word.Document doc = ThisAddIn.Instance.Application.ActiveDocument;
            Microsoft.Office.Interop.Word.Range tocRange = doc.Range();
            string nr = RightTab.Text;
            float Nr = (float)6.5;
            try
            {
                Nr = float.Parse(nr, CultureInfo.InvariantCulture);
            }
            catch (Exception ex)
            {
                new InfowareInformationWindow(FLOAT_CONVERSION_ERROR_MESSAGE, FLOAT_CONVERSION_ERROR_TITLE).ShowDialog();
                return;
            }
            this.Close();

            Pos = (doc.PageSetup.PageWidth - (doc.PageSetup.PageWidth - Nr)) * 70 + 5;
            if (LineSpacing.SelectedValue is WdLineSpacing && ((WdLineSpacing)LineSpacing.SelectedValue) == WdLineSpacing.wdLineSpaceSingle)
            {
                Spacing = 12;
            }
            if (LineSpacing.SelectedValue is WdLineSpacing && ((WdLineSpacing)LineSpacing.SelectedValue) == WdLineSpacing.wdLineSpace1pt5)
            {
                Spacing = (float)18;
            }
            if (LineSpacing.SelectedValue is WdLineSpacing && ((WdLineSpacing)LineSpacing.SelectedValue) == WdLineSpacing.wdLineSpaceDouble)
            {
                Spacing = (float)24;
            }

            List<float> tabStopsSettings = null;

            foreach (Microsoft.Office.Interop.Word.Style currentStyle in doc.Styles)
            {
                object tabLeader;
                tabLeader = Microsoft.Office.Interop.Word.WdTabLeader.wdTabLeaderSpaces;
                if (currentStyle.Type == Word.WdStyleType.wdStyleTypeParagraph || currentStyle.Type == Word.WdStyleType.wdStyleTypeParagraphOnly)
                {
                    string name = currentStyle.NameLocal;
                    string toc_string_index = name.Replace("TOC ", "");
                    int toc_index = 0;
                    if (Int32.TryParse(toc_string_index, out toc_index) && toc_index <= 9 && toc_index >= 1)
                    {
                        if (tabStopsSettings == null)
                        {
                            string tabStopsString = SettingsManager.GetSetting("TOCTabStops", "Sections");
                            if (tabStopsString != null)
                            {
                                try
                                {
                                    tabStopsSettings = SettingsManager.UnescapeCommaCharacter(tabStopsString.Split(',')).Select(x => float.Parse(x)).ToList();
                                }
                                catch (Exception ex)
                                {
                                    tabStopsSettings = new List<float>();
                                }
                            }
                            else
                            {
                                tabStopsSettings = new List<float>();
                            }
                        }

                        int? settingsPos = tabStopsSettings.Count >= toc_index ? (int?)(tabStopsSettings[toc_index - 1] * 72) : null;

                        currentStyle.Font.AllCaps = 0;
                        currentStyle.Font.Bold = 0;

                        int aux_toc = (toc_index - 1) % 9;

                        if (SameTabLeader.IsChecked == true)
                        {
                            tabLeader = GetTabLeaderBasedOnSelection(0);
                        }
                        else
                        {
                            tabLeader = GetTabLeaderBasedOnSelection(aux_toc);
                        }

                        currentStyle.ParagraphFormat.TabStops.ClearAll();
                        currentStyle.ParagraphFormat.TabStops.Add(settingsPos ?? Pos, Word.WdTabAlignment.wdAlignTabRight, tabLeader);

                        float tocRightIndent = SettingsManager.GetSettingAsFloat("TOCRightIndent", "Sections", float.MinValue);

                        if (tocRightIndent != float.MinValue)
                        {
                            currentStyle.ParagraphFormat.RightIndent = (int)(tocRightIndent * 72);
                        }

                        if (toc_index == 1)
                        {
                            int tocLevel1Align = SettingsManager.GetSettingAsInt("TOCLevel1Align", "Sections", -1);
                            string tocLevel1Format = SettingsManager.GetSetting("TOCLevel1Format", "Sections");
                            int tocLevel1SpaceBefore = SettingsManager.GetSettingAsInt("TOCLevel1SpaceBefore", "Sections", -1);
                            int tocLevel1SpaceAfter = SettingsManager.GetSettingAsInt("TOCLevel1SpaceAfter", "Sections", -1);

                            if (tocLevel1Format != null && !string.IsNullOrEmpty(tocLevel1Format.Trim()))
                            {
                                tocLevel1Format = tocLevel1Format.ToLower();
                                if (tocLevel1Format.IndexOf('c') >= 0)
                                {
                                    currentStyle.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphCenter;
                                    currentStyle.ParagraphFormat.TabStops.ClearAll();
                                    currentStyle.ParagraphFormat.TabStops.Add(36, Word.WdTabAlignment.wdAlignTabLeft, Word.WdTabLeader.wdTabLeaderSpaces);
                                }
                                if (tocLevel1Format.IndexOf('a') >= 0)
                                {
                                    currentStyle.Font.AllCaps = 1;
                                }
                                if (tocLevel1Format.IndexOf('b') >= 0)
                                {
                                    currentStyle.Font.Bold = 1;
                                }
                            }
                            else
                            {
                                if (tocLevel1Align == 1)
                                {
                                    currentStyle.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphLeft;
                                }
                                if (tocLevel1Align == 2)
                                {
                                    currentStyle.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphCenter;
                                }
                            }

                            if (tocLevel1SpaceBefore >= 0)
                            {
                                currentStyle.ParagraphFormat.SpaceBefore = tocLevel1SpaceBefore;
                            }

                            if (tocLevel1SpaceAfter >= 0)
                            {
                                currentStyle.ParagraphFormat.SpaceAfter = tocLevel1SpaceAfter;
                            }
                        }
                        if (SpacingValue_After.Text == "") SpacingValue_After.Text = ((float)0).ToString();
                        if (SpacingValue_Before.Text == "") SpacingValue_Before.Text = ((float)0).ToString();
                        float SpacingAfter = (float)NumericUpDownAfter;
                        float SpacingBefore = (float)NumericUpDownBefore;

                        currentStyle.ParagraphFormat.LineSpacing = Spacing;
                        currentStyle.ParagraphFormat.SpaceBefore = SpacingBefore;
                        currentStyle.ParagraphFormat.SpaceAfter = SpacingAfter;
                        if (bold[toc_index] == true)
                        {
                            currentStyle.Font.Bold = 1;
                        }
                        if (allcaps[toc_index] == true)
                        {
                            currentStyle.Font.AllCaps = 1;
                        }
                        if (spacebefore[toc_index] == true)
                        {
                            currentStyle.ParagraphFormat.SpaceBefore = 12;
                        }
                        if (spaceafter[toc_index] == true)
                        {
                            currentStyle.ParagraphFormat.SpaceAfter = 12;
                        }
                        if (allcaps[toc_index] == true)
                        {
                            (currentStyle.ParagraphFormat.ToString()).ToUpper();
                        }
                    }
                }
            }
            int start = doc.Content.End - 1;
            object oUpperHeadingLevel = "1";
            object oLowerHeadingLevel = "9";
            object UseOutlineLevels;
            if (OutlineLevel.IsChecked == true)
            {
                UseOutlineLevels = true;
                if (OutlineLevels.SelectedValue == null) oLowerHeadingLevel = "9";
                else
                    oLowerHeadingLevel = OutlineLevels.SelectedValue;
            }
            else
            {
                UseOutlineLevels = false;
            }

            if (startCurrentToc != null)
            {
                tocRange = doc.Range(startCurrentToc.Value, startCurrentToc.Value);
            }
            else
            {
                if (((int)Position.SelectedValue == (int)PositionEnum.InsertAtBeginning) || (Position.SelectedValue == null))
                {
                    bookmarksStartingAt0 = GetBookmarksStartingAt0();
                    if (bookmarksStartingAt0 != null)
                    {
                        foreach (Bookmark bm in bookmarksStartingAt0)
                        {
                            if (bm.Start == bm.End)
                            {
                                bm.End++;
                            }
                            bm.Start++;
                        }

                        if (bookmarksStartingAt0.Count > 0)
                        {
                            startingPointBeginning = doc.Range(0, 0);
                            startingPointBeginning.InsertBefore("\u200B");
                            startingPointBeginning = doc.Range(0, 1);
                        }
                    }

                    tocRange = doc.Range(0, 0);
                    start = 0;
                }
                if ((int)Position.SelectedValue == (int)PositionEnum.InsertAtEnd)
                {

                    Microsoft.Office.Interop.Word.Paragraph para = ThisAddIn.Instance.Application.ActiveDocument.Paragraphs.Add();
                    Microsoft.Office.Interop.Word.Paragraph para1 = ThisAddIn.Instance.Application.ActiveDocument.Paragraphs.Add();
                    object SectionBreak = ExcludeSectionBreaks.IsChecked == false ? Word.WdBreakType.wdSectionBreakNextPage : WdBreakType.wdSectionBreakContinuous;
                    Range r = para1.Range;
                    r.InsertBreak(ref SectionBreak);
                    tocRange = para1.Range;
                    DOC.Paragraphs.Last.Range.Select();
                    start++;
                }
                if ((int)Position.SelectedValue == (int)PositionEnum.InsertAtCursor)
                {
                    Microsoft.Office.Interop.Word.Range rng = ThisAddIn.Instance.Application.Selection.Range;
                    start = rng.End;
                    var breakRange = rng.Duplicate;
                    breakRange.Collapse(WdCollapseDirection.wdCollapseEnd);
                    object sectionBreak = ExcludeSectionBreaks.IsChecked == false ? Word.WdBreakType.wdSectionBreakNextPage : WdBreakType.wdSectionBreakContinuous;
                    breakRange.InsertBreak(sectionBreak);

                    tocRange = doc.Range(rng.End + 1, rng.End + 1);
                }
            }
            bool UseFields = UseRunInHeadings.IsChecked == true;
            TableOfContents toc = doc.TablesOfContents.Add(tocRange, true, oUpperHeadingLevel, oLowerHeadingLevel, UseFields, null, true, true, null, true, true, UseOutlineLevels);
            if (PreservedLineBreaks.IsChecked == true)
            {
                foreach (Field field in doc.Fields)
                {
                    if (field.Type == WdFieldType.wdFieldTOC)
                    {
                        field.Code.Text += @" \x";
                        NrTOCs++;
                    }
                }
            }
            var tocRange2 = toc.Range.Duplicate;
            tocRange2.Collapse(WdCollapseDirection.wdCollapseEnd);

            if (currentTocRange == null)
            {
                if (ExcludeSectionBreaks.IsChecked == false && (((int)Position.SelectedValue == (int)PositionEnum.InsertAtBeginning) || ((int)Position.SelectedValue == (int)PositionEnum.InsertAtCursor)))
                {
                    tocRange2.InsertBreak(WdBreakType.wdSectionBreakNextPage);
                    tocRange2.Previous(WdUnits.wdSection).Paragraphs.Last.Range.set_Style(WdBuiltinStyle.wdStyleNormal);
                }
                else
                {
                    tocRange2.InsertBreak(WdBreakType.wdSectionBreakContinuous);
                }
            }

            short aux_index_lista = 1;
            foreach (List<string> aux_list in lists)
            {
                foreach (string aux_str in aux_list)
                {
                    try
                    {
                        if (!aux_str.Contains(","))
                        {
                            toc.HeadingStyles.Add(aux_str, aux_index_lista);
                        }
                        else
                        {
                            foreach (Field field in toc.Range.Fields)
                            {
                                if (field.Type == WdFieldType.wdFieldTOC)
                                {
                                    if (field.Code.Text.Contains("\\t"))
                                    {
                                        string fieldCode = field.Code.Text;
                                        int first = fieldCode.IndexOf("\"", fieldCode.IndexOf("\\t"));
                                        int second = first;
                                        bool found = false;
                                        while (second != -1 && !found)
                                        {
                                            second = fieldCode.IndexOf("\"", second + 1);
                                            if (second != -1 && fieldCode[second] != '\\')
                                            {
                                                found = true;
                                            }
                                        }
                                        if (found)
                                        {
                                            field.Code.Text = fieldCode.Substring(0, second) + $"{EscapeComma(aux_str)},{aux_index_lista}" + fieldCode.Substring(second);
                                        }
                                    }
                                    else
                                    {
                                        field.Code.Text += $" \\t \"{EscapeComma(aux_str)},{aux_index_lista}\"";
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex) { }
                }

                aux_index_lista++;

            }

            Paragraphs pars = toc.Range.Paragraphs;
            doc.Activate();
            object mis = System.Reflection.Missing.Value;
            toc.Update();
            if (Centered.IsChecked == true)
            {
                foreach (Word.Paragraph p in pars)
                {
                    if ((p.get_Style() as Word.Style).NameLocal == "TOC 1")
                    {
                        p.Alignment = Word.WdParagraphAlignment.wdAlignParagraphCenter;
                        string s = p.Range.Text;
                        string s1 = "";
                        string s2 = '\t'.ToString();
                        int post = s.LastIndexOf("\t");
                        if (post < 0) continue;
                        string Heading = s.Substring(0, post);
                        s1 = s.Replace("\t1", "");
                        Range rg = p.Range.Duplicate;
                        List<Hyperlink> list = new List<Hyperlink>(rg.Hyperlinks.Cast<Hyperlink>());
                        list[0].TextToDisplay = Heading;
                    }
                }

                try
                {
                    Word.Style toc1Style = Utils.GetStyleByName("TOC 1");
                    if (toc1Style != null)
                    {
                        toc1Style.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphCenter;
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex);
                }
            }

            int page_index = 1;
            foreach (System.Windows.Controls.CheckBox page in this.PageNumbers)
            {
                if (page.IsChecked == false)
                {
                    string toc_index = String.Concat("TOC ", page_index);
                    foreach (Word.Paragraph par in pars)
                    {
                        if ((par.get_Style() as Word.Style).NameLocal == toc_index)
                        {
                            string s = par.Range.Text;
                            string s1 = "";
                            string s2 = '\t'.ToString();
                            int post = s.LastIndexOf("\t");
                            if (post < 0) continue;
                            string Heading = s.Substring(0, post);
                            s1 = s.Replace("\t1", "");
                            Range rg = par.Range.Duplicate;
                            List<Hyperlink> list = new List<Hyperlink>(rg.Hyperlinks.Cast<Hyperlink>());
                            list[0].TextToDisplay = Heading;
                        }
                    }
                }
                page_index++;
            }

            float NrFirstPagTOC = 0;
            Paragraphs parg = toc.Range.Paragraphs;
            if (NoSpaceSameLevel.IsChecked == true)
            {
                for (int j = 2; j <= parg.Count; j++)
                {
                    string st1 = (parg[j].get_Style() as Word.Style).NameLocal;
                    string st2 = (parg[j - 1].get_Style() as Word.Style).NameLocal;
                    if (st1 == st2)
                    {
                        parg[j].SpaceBefore = 0;
                        NrFirstPagTOC = (float)parg[j].Range.Information[Word.WdInformation.wdActiveEndPageNumber];
                    }
                }
            }
            int jj = 0;
            foreach (Field f in DOC.Fields)
            {
                if (f.Type == WdFieldType.wdFieldTOC)
                {
                    jj++;
                    string gh = f.ToString();
                }
            }
            if (jj > 0)
            {
                if (toc.Range.Previous() != null && !string.IsNullOrWhiteSpace(toc.Range.Previous().Text))
                {
                    toc.Range.InsertParagraphBefore();
                }

                toc.Range.InsertParagraphBefore();
                toc.Range.Paragraphs[1].Previous().Range.Text = TitleTB.Text + Environment.NewLine;

                var titleCC = doc.ContentControls.Add(WdContentControlType.wdContentControlRichText, toc.Range.Paragraphs[1].Previous().Range);
                titleCC.Title = TITLE_TAG;
                titleCC.Tag = SerializeData();

                try
                {
                    toc.Range.Paragraphs[1].Previous().set_Style(SettingsManager.GetSetting(CustomConstants.TOC_TITLE_STYLE_NAME, "Sections"));
                }
                catch (Exception ex)
                {
                    toc.Range.Paragraphs[1].Previous().set_Style(GetOrCreateTocTitleStyle());
                }

                if (TOCAlignmentLeft.IsChecked == true)
                {
                    toc.Range.Paragraphs[1].Previous().Alignment = WdParagraphAlignment.wdAlignParagraphLeft;
                }

                if (TOCAlignmentCenter.IsChecked == true)
                {
                    toc.Range.Paragraphs[1].Previous().Alignment = WdParagraphAlignment.wdAlignParagraphCenter;
                }
                if (!string.IsNullOrWhiteSpace(SectionCaption) || !string.IsNullOrWhiteSpace(PageCaption))
                {
                    toc.Range.InsertParagraphBefore();

                    string tocHeaderStyle = SettingsManager.GetSetting("TocHeaderStyle", "Styles", null);
                    if (tocHeaderStyle != null)
                    {
                        try
                        {
                            toc.Range.Paragraphs[1].Previous().set_Style(tocHeaderStyle);
                        }
                        catch { }
                    }
                    else
                    {
                        toc.Range.Paragraphs[1].Previous().set_Style(Utils.GetLocalizedStyleNameFromBuiltInStyle(WdBuiltinStyle.wdStyleNormal));
                        toc.Range.Paragraphs[1].Previous().Range.Font.Bold = 1;
                        toc.Range.Paragraphs[1].Previous().Range.Font.Size = 10;
                    }

                    if (!string.IsNullOrWhiteSpace(PageCaption))
                    {
                        toc.Range.Paragraphs[1].Previous().Range.Text = "\t" + TextPag.Text + Environment.NewLine;
                        toc.Range.Paragraphs[1].Previous().Range.ParagraphFormat.TabStops.Add(Pos, Word.WdAlignmentTabAlignment.wdRight, WdTabLeader.wdTabLeaderSpaces);
                        var pageTitleCC = doc.ContentControls.Add(WdContentControlType.wdContentControlRichText, TrimTabCharacter(toc.Range.Paragraphs[1].Previous().Range));
                        pageTitleCC.Tag = PAGE_TITLE_TAG;
                    }

                    if (!string.IsNullOrWhiteSpace(SectionCaption))
                    {
                        Range sectionRange = toc.Range.Paragraphs[1].Previous().Range;
                        sectionRange.Collapse(WdCollapseDirection.wdCollapseStart);
                        var sectionCC = doc.ContentControls.Add(WdContentControlType.wdContentControlRichText, sectionRange);
                        sectionCC.Range.Text = SectionCaption;
                    }
                }

                if (Two_Columns.IsChecked == true)
                {
                    Word.Range rg = toc.Range.Paragraphs[1].Previous().Previous().Range;
                    rg.Collapse(WdCollapseDirection.wdCollapseEnd);
                    rg.InsertBreak(WdBreakType.wdSectionBreakContinuous);
                    toc.Range.PageSetup.TextColumns.SetCount(2);
                }
            }
            var tocTitleRange = DOC.Range(0, 0);
            //-----------------------------------------------------------------------
            int i = 1;
            foreach (bool DelLevel in LevelForDelete)
            {
                if (DelLevel == true)
                {
                    string test_i = String.Concat("TOC ", i);
                    for (int j = 1; j <= toc.Range.Paragraphs.Count; j++)
                    {
                        var h = toc.Range.Paragraphs[j];
                        string ForDelete = h.Range.Text; // desters
                        string TOCType = ((toc.Range.Paragraphs[j].get_Style() as Word.Style)?.NameLocal);
                        string test = (toc.Range.Paragraphs[j].get_Style() as Word.Style)?.NameLocal;
                        if (test == test_i)
                        {
                            toc.Range.Paragraphs[j].Range.Delete();
                            toc.Range.Paragraphs[j].Range.Collapse();
                            j--;
                        }
                    }
                }
                i++;
            }

            endRange = toc.Range;
            endRange.Collapse(WdCollapseDirection.wdCollapseEnd);
            int trueEnd = 0;
            if (AddendaTable.IsChecked == true)
            {
                try
                {
                    endRange.InsertParagraphAfter();
                    endRange.InsertParagraphAfter();
                    endRange.InsertParagraphAfter();
                    endRange.Collapse(WdCollapseDirection.wdCollapseStart);
                    Word.Paragraph addendaTocTitlePar = endRange.Paragraphs.Last.Next();
                    addendaTocTitlePar.set_Style(Utils.GetLocalizedStyleNameFromBuiltInStyle(WdBuiltinStyle.wdStyleNormal));
                    addendaTocTitlePar.Range.Text = "ADDENDA" + "\r\n";
                    addendaTocTitlePar.Previous().Alignment = WdParagraphAlignment.wdAlignParagraphCenter;
                    Range addendaTableRange = endRange.Paragraphs.Last.Next().Next().Range;
                    TableOfContents addendatoc = doc.TablesOfContents.Add(addendaTableRange, false, 1, 1, false, null, null, false, "Addenda,5", true, true, false);

                    //addendatoc.HeadingStyles.Add("Addenda", 5);
                    end = endRange.End;
                    addendaTableRange.MoveEndUntil('\b');
                    foreach (Field field in addendaTableRange.Fields)
                    {
                        if (field.Type == WdFieldType.wdFieldTOC)
                        {
                            field.Code.Text = RemoveOutlineLevelsCode(field.Code.Text);
                        }
                    }
                    doc.Bookmarks.Add(GenerateBookmarkName(), doc.Range(endRange.End, addendaTableRange.End + 1));
                    trueEnd = addendaTableRange.End;
                    addendatoc.Update();
                    Thread.Sleep(100);
                }
                catch (Exception ex)
                {
                    endRange.MoveEndUntil('\b');
                    end = endRange.End + 1;
                    trueEnd = end;
                }
            }
            else
            {
                endRange.MoveEndUntil('\b');
                end = endRange.End + 1;
                trueEnd = end;
            }

            if (currentTocRange == null)
            {
                doc.Bookmarks.Add(GenerateBookmarkName(), doc.Range(start, end));
            }
            else
            {
                ReplaceCurrentBookMark(GenerateBookmarkName());
            }

            Utils.ReplaceTcFieldsInDocument(ThisAddIn.Instance.Application.ActiveDocument);

            try
            {
                if (ExcludeSectionBreaks.IsChecked == false && (int)Position.SelectedValue != (int)PositionEnum.InsertAtCursor)
                {
                    AddRomanPageNumbering(doc.Range(start + 2, trueEnd).Sections, this.FirstPageNumbering.IsChecked == true);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }

            toc.UpdatePageNumbers();
            if (bookmarksStartingAt0 != null)
            {
                foreach (Bookmark bm in bookmarksStartingAt0)
                {
                    bm.Start--;
                    if (bm.Start + 1 == bm.End)
                    {
                        bm.End--;
                    }
                }
                if (startingPointBeginning != null)
                {
                    startingPointBeginning.Collapse(WdCollapseDirection.wdCollapseEnd);
                    startingPointBeginning.MoveStart(WdUnits.wdCharacter, -1);
                    startingPointBeginning.Delete();
                }
            }
        }

        private object EscapeComma(string aux_str)
        {
            return aux_str.Replace(",", "\\,");
        }

        private List<Bookmark> GetBookmarksStartingAt0()
        {
            var vstoDoc = Globals.Factory.GetVstoObject(DOC);
            List<Bookmark> bookmarks = new List<Bookmark>();

            foreach (Bookmark bm in DOC.Bookmarks)
            {
                if (bm.Range.Start == 0)
                {
                    bookmarks.Add(bm);
                }
            }
            return bookmarks;
        }

        private Range TrimTabCharacter(Range range)
        {
            if (range.Text.Length > 0)
            {
                Range last = range.Duplicate;
                last.Collapse(WdCollapseDirection.wdCollapseEnd);
                last.MoveStart(WdUnits.wdCharacter, -1);
                if (last.Text == "\t")
                {
                    range.MoveEnd(WdUnits.wdCharacter, -1);
                }

                Range first = range.Duplicate;
                first.Collapse(WdCollapseDirection.wdCollapseStart);
                first.MoveEnd(WdUnits.wdCharacter, 1);
                if (first.Text == "\t")
                {
                    range.MoveStart(WdUnits.wdCharacter, 1);
                }
            }

            return range;
        }

        private void ReplaceCurrentBookMark(string bookMarkName)
        {
            if (currentTocBookmark != null)
            {
                int start = currentTocBookmark.Range.Start;
                int end = currentTocBookmark.Range.End;
                currentTocBookmark.Delete();
                var doc = ThisAddIn.Instance.Application.ActiveDocument;
                doc.Bookmarks.Add(bookMarkName, doc.Range(start, end));
            }
        }

        private string RemoveOutlineLevelsCode(string code)
        {
            int start = code.IndexOf("\\o");
            if (start == -1)
            {
                start = code.IndexOf("\\O");
            }
            int end = code.IndexOf("\"", code.IndexOf("\"", start) + 1);
            return code.Substring(0, start) + code.Substring(end + 1);
        }

        private void AddRomanPageNumbering(Sections sections, bool includeFirstPageNumbering)
        {
            bool first = true;

            Range nextSectionRange = sections.Last.Range.Next(WdUnits.wdSection);
            if (nextSectionRange != null)
            {
                nextSectionRange.Sections[1].Footers[WdHeaderFooterIndex.wdHeaderFooterPrimary].LinkToPrevious = false;
                nextSectionRange.Sections[1].Footers[WdHeaderFooterIndex.wdHeaderFooterPrimary].PageNumbers.RestartNumberingAtSection = true;
                nextSectionRange.Sections[1].Footers[WdHeaderFooterIndex.wdHeaderFooterPrimary].PageNumbers.StartingNumber = 1;
                nextSectionRange.Sections[1].Footers[WdHeaderFooterIndex.wdHeaderFooterFirstPage].LinkToPrevious = false;
                nextSectionRange.Sections[1].Footers[WdHeaderFooterIndex.wdHeaderFooterFirstPage].PageNumbers.RestartNumberingAtSection = true;
                nextSectionRange.Sections[1].Footers[WdHeaderFooterIndex.wdHeaderFooterFirstPage].PageNumbers.StartingNumber = 1;
                nextSectionRange.Sections[1].Footers[WdHeaderFooterIndex.wdHeaderFooterEvenPages].LinkToPrevious = false;
                nextSectionRange.Sections[1].Footers[WdHeaderFooterIndex.wdHeaderFooterEvenPages].PageNumbers.RestartNumberingAtSection = true;
                nextSectionRange.Sections[1].Footers[WdHeaderFooterIndex.wdHeaderFooterEvenPages].PageNumbers.StartingNumber = 1;

                nextSectionRange.Sections[1].Headers[WdHeaderFooterIndex.wdHeaderFooterPrimary].LinkToPrevious = false;
                nextSectionRange.Sections[1].Headers[WdHeaderFooterIndex.wdHeaderFooterPrimary].PageNumbers.RestartNumberingAtSection = true;
                nextSectionRange.Sections[1].Headers[WdHeaderFooterIndex.wdHeaderFooterPrimary].PageNumbers.StartingNumber = 1;
                nextSectionRange.Sections[1].Headers[WdHeaderFooterIndex.wdHeaderFooterFirstPage].LinkToPrevious = false;
                nextSectionRange.Sections[1].Headers[WdHeaderFooterIndex.wdHeaderFooterFirstPage].PageNumbers.RestartNumberingAtSection = true;
                nextSectionRange.Sections[1].Headers[WdHeaderFooterIndex.wdHeaderFooterFirstPage].PageNumbers.StartingNumber = 1;
                nextSectionRange.Sections[1].Headers[WdHeaderFooterIndex.wdHeaderFooterEvenPages].LinkToPrevious = false;
                nextSectionRange.Sections[1].Headers[WdHeaderFooterIndex.wdHeaderFooterEvenPages].PageNumbers.RestartNumberingAtSection = true;
                nextSectionRange.Sections[1].Headers[WdHeaderFooterIndex.wdHeaderFooterEvenPages].PageNumbers.StartingNumber = 1;
            }
            ThisAddIn.Instance.Application.ScreenUpdating = false;
            if (ThisAddIn.Instance.IsFunctionForLogging("ScreenUpdating"))
                Logger.Log("Disabled Screen Updating (Table_Of_Contents.xaml.cs > AddRomanPageNumbering)");
            foreach (Word.Section section in sections)
            {
                AddRomanPageNumbering(section, includeFirstPageNumbering, first);
                first = false;
            }
            ThisAddIn.Instance.Application.ScreenUpdating = true;
            if (ThisAddIn.Instance.IsFunctionForLogging("ScreenUpdating"))
                Logger.Log("Enabled Screen Updating (Table_Of_Contents.xaml.cs > AddRomanPageNumbering)");
        }

        private void AddRomanPageNumbering(Word.Section section, bool includeFirstPage = true, bool restartNumbering = false)
        {
            if (restartNumbering)
            {
                section.Footers[WdHeaderFooterIndex.wdHeaderFooterPrimary].LinkToPrevious = false;
                section.Footers[WdHeaderFooterIndex.wdHeaderFooterPrimary].PageNumbers.RestartNumberingAtSection = true;
                section.Footers[WdHeaderFooterIndex.wdHeaderFooterPrimary].PageNumbers.StartingNumber = 1;
                section.Footers[WdHeaderFooterIndex.wdHeaderFooterPrimary].Range.Text = "";
                section.Footers[WdHeaderFooterIndex.wdHeaderFooterFirstPage].LinkToPrevious = false;
                section.Footers[WdHeaderFooterIndex.wdHeaderFooterFirstPage].PageNumbers.RestartNumberingAtSection = true;
                section.Footers[WdHeaderFooterIndex.wdHeaderFooterFirstPage].PageNumbers.StartingNumber = 1;
                section.Footers[WdHeaderFooterIndex.wdHeaderFooterFirstPage].Range.Text = "";

                section.Headers[WdHeaderFooterIndex.wdHeaderFooterPrimary].LinkToPrevious = false;
                section.Headers[WdHeaderFooterIndex.wdHeaderFooterPrimary].PageNumbers.RestartNumberingAtSection = true;
                section.Headers[WdHeaderFooterIndex.wdHeaderFooterPrimary].PageNumbers.StartingNumber = 1;
                section.Headers[WdHeaderFooterIndex.wdHeaderFooterPrimary].Range.Text = "";
                section.Headers[WdHeaderFooterIndex.wdHeaderFooterFirstPage].LinkToPrevious = false;
                section.Headers[WdHeaderFooterIndex.wdHeaderFooterFirstPage].PageNumbers.RestartNumberingAtSection = true;
                section.Headers[WdHeaderFooterIndex.wdHeaderFooterFirstPage].PageNumbers.StartingNumber = 1;
                section.Headers[WdHeaderFooterIndex.wdHeaderFooterFirstPage].Range.Text = "";
            }

            var footer = section.Footers[WdHeaderFooterIndex.wdHeaderFooterPrimary];
            var range = footer.Range;
            range.Collapse(WdCollapseDirection.wdCollapseStart);

            PageNumber pn = footer.PageNumbers.Add(WdPageNumberAlignment.wdAlignPageNumberCenter);

            //TODO add real setting names

            string TOCPageNumberSetting = SettingsManager.GetSetting("TOCPageNumber", "Sections");
            if (TOCPageNumberSetting == null || TOCPageNumberSetting.Split(',').Length < 3)
            {
                TOCPageNumberSetting = "(,),2";
            }

            var tocPageNumberSettingArr = SettingsManager.UnescapeCommaCharacter(TOCPageNumberSetting.Split(','));

            string startChar = tocPageNumberSettingArr[0];
            string endChar = tocPageNumberSettingArr[1];

            int numberingStyleInt = -1;
            if (!int.TryParse(tocPageNumberSettingArr[2], out numberingStyleInt))
            {
                numberingStyleInt = 2;
            }

            WdPageNumberStyle pageNumberStyle = (WdPageNumberStyle)numberingStyleInt;


            //adding brackets
            Range pageNoRange = footer.Range;
            pageNoRange.Collapse(WdCollapseDirection.wdCollapseStart);
            pageNoRange.Text = startChar != null ? startChar : "(";

            pageNoRange = footer.Range.Paragraphs[1].Range;
            //surrounding with content control for delete / replace
            var contentControl = ThisAddIn.Instance.Application.ActiveDocument.ContentControls.Add(WdContentControlType.wdContentControlRichText, pageNoRange);
            contentControl.Tag = PAGE_NUMBERING_CONTROL_TAG;

            pageNoRange.MoveEnd(WdUnits.wdCharacter, -1);
            pageNoRange.Collapse(WdCollapseDirection.wdCollapseEnd);
            pageNoRange.Text = endChar != null ? endChar : ")";

            section.PageSetup.DifferentFirstPageHeaderFooter = includeFirstPage ? 0 : -1;
            footer.PageNumbers.NumberStyle = pageNumberStyle;
        }
        private string SerializeData()
        {
            TOCSerializer tocSerializer = new TOCSerializer();

            tocSerializer.SameTabLeader = this.SameTabLeader.IsChecked == true;
            tocSerializer.TabLeaders.Add(tabType.ToList().IndexOf(this.TabLeader1.Text));
            tocSerializer.TabLeaders.Add(tabType.ToList().IndexOf(this.TabLeader2.Text));
            tocSerializer.TabLeaders.Add(tabType.ToList().IndexOf(this.TabLeader3.Text));
            tocSerializer.TabLeaders.Add(tabType.ToList().IndexOf(this.TabLeader4.Text));
            tocSerializer.TabLeaders.Add(tabType.ToList().IndexOf(this.TabLeader5.Text));
            tocSerializer.TabLeaders.Add(tabType.ToList().IndexOf(this.TabLeader6.Text));
            tocSerializer.TabLeaders.Add(tabType.ToList().IndexOf(this.TabLeader7.Text));
            tocSerializer.TabLeaders.Add(tabType.ToList().IndexOf(this.TabLeader8.Text));
            tocSerializer.TabLeaders.Add(tabType.ToList().IndexOf(this.TabLeader9.Text));

            tocSerializer.IncludePageNumber.Add(this.PageNum1.IsChecked == true);
            tocSerializer.IncludePageNumber.Add(this.PageNum2.IsChecked == true);
            tocSerializer.IncludePageNumber.Add(this.PageNum3.IsChecked == true);
            tocSerializer.IncludePageNumber.Add(this.PageNum4.IsChecked == true);
            tocSerializer.IncludePageNumber.Add(this.PageNum5.IsChecked == true);
            tocSerializer.IncludePageNumber.Add(this.PageNum6.IsChecked == true);
            tocSerializer.IncludePageNumber.Add(this.PageNum7.IsChecked == true);
            tocSerializer.IncludePageNumber.Add(this.PageNum8.IsChecked == true);
            tocSerializer.IncludePageNumber.Add(this.PageNum9.IsChecked == true);

            if (this.OutlineLevel.IsChecked == false)
            {
                tocSerializer.OutlineLevels = 0;
            }
            else
            {
                tocSerializer.OutlineLevels = int.Parse(this.OutlineLevels.Text);
            }

            tocSerializer.PreserLineBreaks = this.PreservedLineBreaks.IsChecked == true;
            tocSerializer.ExcludeSectionBreaks = this.ExcludeSectionBreaks.IsChecked == true;
            tocSerializer.AddendaTable = this.AddendaTable.IsChecked == true;
            tocSerializer.IncludePageNumberingFirstPage = this.FirstPageNumbering.IsChecked == true;
            tocSerializer.RunInHeadings = this.UseRunInHeadings.IsChecked == true;

            switch (this.LineSpacing.SelectedValue)
            {
                case WdLineSpacing.wdLineSpaceSingle:
                    tocSerializer.LineSpacing = 0;
                    break;
                case WdLineSpacing.wdLineSpace1pt5:
                    tocSerializer.LineSpacing = 1;
                    break;
                case WdLineSpacing.wdLineSpaceDouble:
                    tocSerializer.LineSpacing = 2;
                    break;
            }

            tocSerializer.LeftAlignment = this.TOCAlignmentLeft.IsChecked == true;
            tocSerializer.CenterAlignment = this.TOCAlignmentCenter.IsChecked == true;
            tocSerializer.TwoColumn = this.Two_Columns.IsChecked == true;
            tocSerializer.Toc1Centered = this.Centered.IsChecked == true;
            tocSerializer.NoSpaceBetweenEntriesOfSameLevel = this.NoSpaceSameLevel.IsChecked == true;

            for (int i = 0; i < 9; i++)
            {
                tocSerializer.Bold.Add(bold[i + 1]);
                tocSerializer.AllCaps.Add(allcaps[i + 1]);
                tocSerializer.SpaceAfter12.Add(spaceafter[i + 1]);
                tocSerializer.SpaceBefore12.Add(spacebefore[i + 1]);
            }
            tocSerializer.AfterEachLevel = this.NumericUpDownAfter;
            tocSerializer.BeforeEachLevel = this.NumericUpDownBefore;
            try
            {
                tocSerializer.RightTab = (int)(double.Parse(this.RightTab.Text) * 10);
            }
            catch { }
            return tocSerializer.Serialize();
        }



        private string GenerateBookmarkName()
        {
            Random rand = new Random();
            return "TOC_" + rand.Next() % int.MaxValue;
        }

        private void Cancel(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void SameTabLeader_Checked(object sender, RoutedEventArgs e)
        {
            this.TabLeaders[1].IsEnabled = false;
            this.TabLeaders[2].IsEnabled = false;
            this.TabLeaders[3].IsEnabled = false;
            this.TabLeaders[4].IsEnabled = false;
            this.TabLeaders[5].IsEnabled = false;
            this.TabLeaders[6].IsEnabled = false;
            this.TabLeaders[7].IsEnabled = false;
            this.TabLeaders[8].IsEnabled = false;
        }

        private void SameTabLeader_UnChecked(object sender, RoutedEventArgs e)
        {
            this.TabLeaders[1].IsEnabled = true;
            this.TabLeaders[2].IsEnabled = true;
            this.TabLeaders[3].IsEnabled = true;
            this.TabLeaders[4].IsEnabled = true;
            this.TabLeaders[5].IsEnabled = true;
            this.TabLeaders[6].IsEnabled = true;
            this.TabLeaders[7].IsEnabled = true;
            this.TabLeaders[8].IsEnabled = true;
        }

        private void Spacing_Before_Down(object sender, RoutedEventArgs e)
        {
            NumericUpDownBefore--;
            if (NumericUpDownBefore < 0) NumericUpDownBefore = 0;
            SpacingValue_Before.Text = NumericUpDownBefore.ToString() + " pt";
        }

        private void Spacing_Before_Up(object sender, RoutedEventArgs e)
        {
            NumericUpDownBefore++;
            SpacingValue_Before.Text = NumericUpDownBefore.ToString() + " pt";
        }

        private void Spacing_After_Down(object sender, RoutedEventArgs e)
        {
            NumericUpDownAfter--;
            if (NumericUpDownAfter < 0) NumericUpDownAfter = 0;
            SpacingValue_After.Text = NumericUpDownAfter.ToString() + " pt";
        }

        private void Spacing_After_Up(object sender, RoutedEventArgs e)
        {
            NumericUpDownAfter++;
            SpacingValue_After.Text = NumericUpDownAfter.ToString() + " pt";
        }

        private void Update(object sender, RoutedEventArgs e)
        {
            Utils.ReplaceTcFieldsInDocument(ThisAddIn.Instance.Application.ActiveDocument);

            int j = 0;
            foreach (Field f in DOC.Fields)
            {
                if (f.Type == WdFieldType.wdFieldTOC)
                {
                    j++;
                }
            }
            if (j > 0)
            {
                int Nr = BrowseList.SelectedIndex;
                if (Nr > -1)
                {
                    DOC.TablesOfContents[Nr + 1].Update();
                }
                Word.TableOfContents toc = DOC.TablesOfContents[Nr + 1];
                Paragraphs pars = toc.Range.Paragraphs;

                int page_index = 1;
                foreach (System.Windows.Controls.CheckBox page in this.PageNumbers)
                {
                    if (page.IsChecked == false)
                    {
                        string toc_index = String.Concat("TOC ", page_index);
                        foreach (Word.Paragraph par in pars)
                        {
                            if ((par.get_Style() as Word.Style).NameLocal == toc_index)
                            {
                                string s = par.Range.Text;
                                string s1 = "";
                                string s2 = '\t'.ToString();
                                int post = s.LastIndexOf("\t");
                                if (post < 0) continue;
                                string Heading = s.Substring(0, post);
                                s1 = s.Replace("\t1", "");
                                Range rg = par.Range.Duplicate;
                                List<Hyperlink> list = new List<Hyperlink>(rg.Hyperlinks.Cast<Hyperlink>());
                                list[0].TextToDisplay = Heading;
                            }
                        }
                    }
                    page_index++;
                }
                WFP_TableOfContent.Close();
            }
            else
            {
                new InfowareInformationWindow("There is no Table Of Contents in this document.").ShowDialog();
                //WFP_TableOfContent.Close();
            }
        }

        private void Delete(object sender, RoutedEventArgs e)
        {
            int Nr = BrowseList.SelectedIndex;
            if (Nr >= 0)
            {
                Nr++;
                if (DOC.TablesOfContents[Nr] != null)
                {
                    foreach (Bookmark bookmark in DOC.Bookmarks)
                    {
                        if (bookmark.Name.IndexOf("TOC_") == 0 && DOC.TablesOfContents[Nr].Range.InRange(bookmark.Range))
                        {
                            bookmark.Range.Delete();
                            //bookmark.Delete();
                            break;
                        }
                    }

                    //Range range = DOC.TablesOfContents[Nr].Range.Duplicate;
                    //range.Collapse(WdCollapseDirection.wdCollapseStart);
                    //range.Paragraphs[1].Previous().Range.Delete();
                    //range.Paragraphs[1].Previous().Range.Delete();
                    //range.Paragraphs[1].Previous().Range.Delete();

                    //Microsoft.Office.Interop.Word.Section current_section = DOC.TablesOfContents[Nr].Range.Sections[1];
                    //DOC.TablesOfContents[Nr].Delete();
                    //Range sectionEnd = current_section.Range;
                    //sectionEnd.Collapse(WdCollapseDirection.wdCollapseEnd);
                    //range.MoveEndUntil('\b');
                    //range.Delete();
                }
            }
            this.Close();
        }

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0 && e.AddedItems[0] is TabItem)
            {
                if ((e.AddedItems[0] as TabItem).Name == "BrowseTabItem")
                {
                    this.UpdateButton.Visibility = Visibility.Visible;
                    this.DeleteButton.Visibility = Visibility.Visible;
                    this.GotoButton.Visibility = Visibility.Visible;
                    this.OkButton.Visibility = Visibility.Collapsed;
                    this.CancelButton.Visibility = Visibility.Collapsed;
                    this.OkButton.IsDefault = false;
                }
                else
                {
                    this.UpdateButton.Visibility = Visibility.Collapsed;
                    this.DeleteButton.Visibility = Visibility.Collapsed;
                    this.GotoButton.Visibility = Visibility.Collapsed;
                    this.OkButton.Visibility = Visibility.Visible;
                    this.CancelButton.Visibility = Visibility.Visible;
                    this.OkButton.IsDefault = true;
                }

            }

        }

        private void RightTab_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            bool hasPoint = false;
            TextBox senderTB = sender as TextBox;

            if (senderTB != null && e != null)
            {
                hasPoint = senderTB.Text.Contains('.');
                Regex regex = new Regex(hasPoint ? NATURAL_NO_REGEX : FLOAT_REGEX);
                e.Handled = !regex.IsMatch(e.Text);
            }
        }

        private void GoTo(object sender, RoutedEventArgs e)
        {
            foreach (Field f in DOC.Fields)
            {
                if (f.Type == WdFieldType.wdFieldTOC)
                {
                    i++;
                }
            }
            if (i == 0)
            {
                new InfowareInformationWindow("There is no Table Of Contents in this document.").ShowDialog();
            }
            else
            {
                DOC.ActiveWindow.ScrollIntoView(DOC.TablesOfContents[NrTOCs].Range, true);
                DOC.TablesOfContents[NrTOCs].Range.Select();
                DOC.GoTo(WdGoToItem.wdGoToTable, WdGoToDirection.wdGoToAbsolute, WdGoToDirection.wdGoToPrevious, WdGoToItem.wdGoToTable);
                WFP_TableOfContent.Close();
            }
        }

        void TOC4Buttons()
        {
            if (TOCLevel1.IsChecked == true) TOCLevelTOC1.IsChecked = true;
            if (TOCLevelTOC2_9.IsChecked == true)
            {
                TOCLevel2.IsChecked = true;
                TOCLevel3.IsChecked = true;
                TOCLevel4.IsChecked = true;
                TOCLevel5.IsChecked = true;
                TOCLevel6.IsChecked = true;
                TOCLevel7.IsChecked = true;
                TOCLevel8.IsChecked = true;
                TOCLevel9.IsChecked = true;
                //switch (SelectedPropertyItem)
                //{
                //    case 1:
                //        {
                //            for (int j = 2; j <= 9; j++)
                //            {
                //                bold[j] = true;
                //            }
                //        }
                //        break;
                //}
            }
            if (TOCLevelTOC2_9.IsChecked == false)
            {
                TOCLevel2.IsChecked = false;
                TOCLevel3.IsChecked = false;
                TOCLevel4.IsChecked = false;
                TOCLevel5.IsChecked = false;
                TOCLevel6.IsChecked = false;
                TOCLevel7.IsChecked = false;
                TOCLevel8.IsChecked = false;
                TOCLevel9.IsChecked = false;
                //switch (SelectedPropertyItem)
                //{
                //    case 1:
                //        {
                //            for (int j = 2; j <= 9; j++)
                //            {
                //                bold[j] = false;
                //            }
                //        }
                //        break;
                //}
            }
            Refresh_Commands();
        }

        private void TOC_Centered(object sender, RoutedEventArgs e)
        {
            centered = true;
        }

        private void PropertyBold_Selected(object sender, RoutedEventArgs e)
        {
            TOCLevelTOC1.IsChecked = false;
            TOCLevelTOC2_9.IsChecked = false;
            TOCLevelAll.IsChecked = false;
            TOCLevelNone.IsChecked = false;
            TOC4Buttons();
            SelectedPropertyItem = 1;
            Refresh_Commands();
            Restore_Bold_Buttons();
            z = 0;
            for (int x = 1; x <= 9; x++)
            {
                if (bold[x] == false) z++;
            }
            if (z == 9) TOCLevelNone.IsChecked = true;
        }

        private void PropertyAllCaps_Selected(object sender, RoutedEventArgs e)
        {
            TOCLevelTOC1.IsChecked = false;
            TOCLevelTOC2_9.IsChecked = false;
            TOCLevelAll.IsChecked = false;
            TOCLevelNone.IsChecked = false;
            SelectedPropertyItem = 2;
            Refresh_Commands();
            Restore_AllCaps_Buttons();
            z = 0;
            for (int x = 1; x <= 9; x++)
            {
                if (allcaps[x] == false) z++;
            }
            if (z == 9) TOCLevelNone.IsChecked = true;
        }

        private void PropertySpaceBefore_Selected(object sender, RoutedEventArgs e)
        {
            TOCLevelTOC1.IsChecked = false;
            TOCLevelTOC2_9.IsChecked = false;
            TOCLevelAll.IsChecked = false;
            TOCLevelNone.IsChecked = false;
            SelectedPropertyItem = 3;
            Refresh_Commands();
            Restore_SpaceBefore_Buttons();
            z = 0;
            for (int x = 1; x <= 9; x++)
            {
                if (spacebefore[x] == false) z++;
            }
            if (z == 9) TOCLevelNone.IsChecked = true;
        }

        private void PropertySpaceAfter_Selected(object sender, RoutedEventArgs e)
        {
            TOCLevelTOC1.IsChecked = false;
            TOCLevelTOC2_9.IsChecked = false;
            TOCLevelAll.IsChecked = false;
            TOCLevelNone.IsChecked = false;
            SelectedPropertyItem = 4;
            Commands.Content = "";
            Refresh_Commands();
            Restore_SpaceAfter_Buttons();
            z = 0;
            for (int x = 1; x <= 9; x++)
            {
                if (spaceafter[x] == false) z++;
            }
            if (z == 9) TOCLevelNone.IsChecked = true;
        }

        void Set_TOCLevel(object sender, RoutedEventArgs e)
        {
            g = 0;
            var button = (ToggleButton)sender;
            int index = Int32.Parse(button.Content.ToString());
            if (index != 1) TOCLevelTOC1.IsChecked = false;
            if (index == 1)
            {
                TOCLevelTOC2_9.IsChecked = false;
            }
            switch (SelectedPropertyItem)
            {
                case 1:
                    {
                        bold[index] = (bool)button.IsChecked;
                        if (bold[index] == true) TOCLevelNone.IsChecked = false;
                        if ((index >= 2) && (index <= 9))
                        {
                            if (bold[index] == false) TOCLevelTOC2_9.IsChecked = false;
                        }
                        Refresh_Commands();
                        for (int j = 1; j <= 9; j++)
                        {
                            if (bold[j] == true) g++;
                        }
                        TOCLevelAll.IsChecked = g == 9;
                        if (bold[1] == false) TOCLevelTOC1.IsChecked = false;
                        int q = 0;
                        if (bold[1] == true)
                        {
                            for (int x = 2; x <= 9; x++)
                            {
                                if (bold[x] == true) q++;
                            }
                            if (q == 0) TOCLevelTOC1.IsChecked = true;
                            else
                                TOCLevelTOC1.IsChecked = false;
                        }
                        int f = 0;
                        if (bold[1] == false)
                        {
                            for (int x = 2; x <= 9; x++)
                            {
                                if (bold[x] == false) f++;
                            }
                            if (f == 0) TOCLevelTOC2_9.IsChecked = true;
                        }
                        int z = 0;
                        for (int x = 1; x <= 9; x++)
                        {
                            if (bold[x] == false) z++;
                        }
                        if (z == 9) TOCLevelNone.IsChecked = true;
                        break;
                    }
                    g = 0;
                case 2:
                    {
                        allcaps[index] = (bool)button.IsChecked;
                        if (allcaps[index] == true) TOCLevelNone.IsChecked = false;
                        if ((index >= 2) && (index <= 9))
                        {
                            if (bold[index] == false) TOCLevelTOC2_9.IsChecked = false;
                        }
                        Refresh_Commands();
                        for (int j = 1; j <= 9; j++)
                        {
                            if (allcaps[j] == true) g++;
                        }
                        TOCLevelAll.IsChecked = g == 9;
                        if (allcaps[1] == false) TOCLevelTOC1.IsChecked = false;
                        q = 0;
                        if (allcaps[1] == true)
                        {
                            for (int x = 2; x <= 9; x++)
                            {
                                if (allcaps[x] == true) q++;
                            }
                            if (q == 0) TOCLevelTOC1.IsChecked = true;
                            else
                                TOCLevelTOC1.IsChecked = false;
                        }
                        f = 0;
                        if (allcaps[1] == false)
                        {
                            for (int x = 2; x <= 9; x++)
                            {
                                if (allcaps[x] == false) f++;
                            }
                            if (f == 0) TOCLevelTOC2_9.IsChecked = true;
                        }
                        z = 0;
                        for (int x = 1; x <= 9; x++)
                        {
                            if (allcaps[x] == false) z++;
                        }
                        if (z == 9) TOCLevelNone.IsChecked = true;
                        break;
                    }
                case 3:
                    {
                        spacebefore[index] = (bool)button.IsChecked;
                        if (spacebefore[index] == true) TOCLevelNone.IsChecked = false;
                        if ((index >= 2) && (index <= 9))
                        {
                            if (spacebefore[index] == false) TOCLevelTOC2_9.IsChecked = false;
                        }
                        Refresh_Commands();
                        for (int j = 1; j <= 9; j++)
                        {
                            if (spacebefore[j] == true) g++;
                        }
                        TOCLevelAll.IsChecked = g == 9;
                        if (spacebefore[1] == false) TOCLevelTOC1.IsChecked = false;
                        q = 0;
                        if (spacebefore[1] == true)
                        {
                            for (int x = 2; x <= 9; x++)
                            {
                                if (spacebefore[x] == true) q++;
                            }
                            if (q == 0) TOCLevelTOC1.IsChecked = true;
                            else
                                TOCLevelTOC1.IsChecked = false;
                        }
                        f = 0;
                        if (spacebefore[1] == false)
                        {
                            for (int x = 2; x <= 9; x++)
                            {
                                if (spacebefore[x] == false) f++;
                            }
                            if (f == 0) TOCLevelTOC2_9.IsChecked = true;
                        }
                        int z = 0;
                        for (int x = 1; x <= 9; x++)
                        {
                            if (spacebefore[x] == false) z++;
                        }
                        if (z == 9) TOCLevelNone.IsChecked = true;
                    }
                    break;
                case 4:
                    {
                        spaceafter[index] = (bool)button.IsChecked;
                        if (spaceafter[index] == true) TOCLevelNone.IsChecked = false;
                        if ((index >= 2) && (index <= 9))
                        {
                            if (spaceafter[index] == false) TOCLevelTOC2_9.IsChecked = false;
                        }
                        Refresh_Commands();
                        for (int j = 1; j <= 9; j++)
                        {
                            if (spaceafter[j] == true) g++;
                        }
                        TOCLevelAll.IsChecked = g == 9;
                        if (spaceafter[1] == false) TOCLevelTOC1.IsChecked = false;
                        q = 0;
                        if (spaceafter[1] == true)
                        {
                            for (int x = 2; x <= 9; x++)
                            {
                                if (spaceafter[x] == true) q++;
                            }
                            if (q == 0) TOCLevelTOC1.IsChecked = true;
                            else
                                TOCLevelTOC1.IsChecked = false;
                        }
                        f = 0;
                        if (spaceafter[1] == false)
                        {
                            for (int x = 2; x <= 9; x++)
                            {
                                if (spaceafter[x] == false) f++;
                            }
                            if (f == 0) TOCLevelTOC2_9.IsChecked = true;
                        }
                        z = 0;
                        for (int x = 1; x <= 9; x++)
                        {
                            if (spaceafter[x] == false) z++;
                        }
                        if (z == 9) TOCLevelNone.IsChecked = true;
                    }
                    break;
            }
        }

        private void Set_TOCButton_Checked(object sender, RoutedEventArgs e)
        {
            var button = (ToggleButton)sender;
            TocLevelType tocLevelType = (TocLevelType)Enum.Parse(typeof(TocLevelType), button.Tag.ToString());

            switch (tocLevelType)
            {
                case TocLevelType.Toc1:
                    {
                        switch (SelectedPropertyItem)
                        {
                            case 1:
                                {
                                    bold[1] = (bool)button.IsChecked;
                                    for (int j = 2; j <= 9; j++) bold[j] = !(bool)button.IsChecked;
                                    Refresh_Commands();
                                    Restore_Bold_Buttons();
                                }
                                break;
                            case 2:
                                {
                                    allcaps[1] = (bool)button.IsChecked;
                                    for (int j = 2; j <= 9; j++) allcaps[j] = !(bool)button.IsChecked;
                                    Refresh_Commands();
                                    Restore_AllCaps_Buttons();
                                }
                                break;
                            case 3:
                                {
                                    spacebefore[1] = (bool)button.IsChecked;
                                    for (int j = 2; j <= 9; j++) spacebefore[j] = !(bool)button.IsChecked;
                                    Refresh_Commands();
                                    Restore_SpaceBefore_Buttons();
                                }
                                break;
                            case 4:
                                {
                                    spaceafter[1] = (bool)button.IsChecked;
                                    for (int j = 2; j <= 9; j++) spaceafter[j] = !(bool)button.IsChecked;
                                    Refresh_Commands();
                                    Restore_SpaceAfter_Buttons();
                                }
                                break;
                        }
                        TOCLevel1.IsChecked = true;
                    }
                    break;
                case TocLevelType.Toc2to9:
                    switch (SelectedPropertyItem)
                    {
                        case 1:
                            {
                                bold[1] = false;
                                for (int j = 2; j <= 9; j++) bold[j] = true;
                                Refresh_Commands();
                                Restore_Bold_Buttons();
                                break;
                            }
                        case 2:
                            {
                                allcaps[1] = false;
                                for (int j = 2; j <= 9; j++) allcaps[j] = true;
                                Refresh_Commands();
                                Restore_AllCaps_Buttons();
                                break;
                            }
                        case 3:
                            {
                                spacebefore[1] = false;
                                for (int j = 2; j <= 9; j++) spacebefore[j] = true;
                                Refresh_Commands();
                                Restore_SpaceBefore_Buttons();
                                break;
                            }
                        case 4:
                            {
                                spaceafter[1] = false;
                                for (int j = 2; j <= 9; j++) spaceafter[j] = true;
                                Refresh_Commands();
                                Restore_SpaceAfter_Buttons();
                                break;
                            }
                    }
                    break;
                case TocLevelType.All:
                    switch (SelectedPropertyItem)
                    {
                        case 1:
                            {
                                for (int j = 1; j <= 9; j++) bold[j] = true;
                                Refresh_Commands();
                                Restore_Bold_Buttons();
                                break;
                            }
                        case 2:
                            {
                                for (int j = 1; j <= 9; j++) allcaps[j] = true;
                                Refresh_Commands();
                                Restore_AllCaps_Buttons();
                                break;
                            }
                        case 3:
                            {
                                for (int j = 1; j <= 9; j++) spacebefore[j] = true;
                                Refresh_Commands();
                                Restore_SpaceBefore_Buttons();
                                break;
                            }
                        case 4:
                            {
                                for (int j = 1; j <= 9; j++) spaceafter[j] = true;
                                Refresh_Commands();
                                Restore_SpaceAfter_Buttons();
                                break;
                            }
                    }
                    break;
                case TocLevelType.None:
                    switch (SelectedPropertyItem)
                    {
                        case 1:
                            {
                                for (int j = 1; j <= 9; j++) bold[j] = false;
                                Refresh_Commands();
                                Restore_Bold_Buttons();
                                break;
                            }
                        case 2:
                            {
                                for (int j = 1; j <= 9; j++) allcaps[j] = false;
                                Refresh_Commands();
                                Restore_AllCaps_Buttons();
                                break;
                            }
                        case 3:
                            {
                                for (int j = 1; j <= 9; j++) spacebefore[j] = false;
                                Refresh_Commands();
                                Restore_SpaceBefore_Buttons();
                                break;
                            }
                        case 4:
                            {
                                for (int j = 1; j <= 9; j++) spaceafter[j] = false;
                                Refresh_Commands();
                                Restore_SpaceAfter_Buttons();
                                break;
                            }
                    }
                    break;
            }
        }

        private void Set_TOCButton_UnChecked(object sender, RoutedEventArgs e)
        {
            var button = (ToggleButton)sender;
            string content = button.Content.ToString();
            switch (content)
            {
                case "TOC 1":
                    {
                        switch (SelectedPropertyItem)
                        {
                            case 1:
                                {
                                    bold[1] = false;
                                    Refresh_Commands();
                                    Restore_Bold_Buttons();
                                }
                                break;
                            case 2:
                                {
                                    allcaps[1] = false;
                                    Refresh_Commands();
                                    Restore_AllCaps_Buttons();
                                }
                                break;
                            case 3:
                                {
                                    spacebefore[1] = false;
                                    Refresh_Commands();
                                    Restore_SpaceBefore_Buttons();
                                }
                                break;
                            case 4:
                                {
                                    spaceafter[1] = false;
                                    Refresh_Commands();
                                    Restore_SpaceAfter_Buttons();
                                }
                                break;
                        }
                    }
                    break;
                case "TOC2 to 9":
                    switch (SelectedPropertyItem)
                    {
                        case 1:
                            {
                                for (int j = 2; j <= 9; j++) bold[j] = false;
                                Refresh_Commands();
                                Restore_Bold_Buttons();
                                break;
                            }
                        case 2:
                            {
                                for (int j = 2; j <= 9; j++) allcaps[j] = false;
                                Refresh_Commands();
                                Restore_AllCaps_Buttons();
                                break;
                            }
                        case 3:
                            {
                                for (int j = 2; j <= 9; j++) spacebefore[j] = false;
                                Refresh_Commands();
                                Restore_SpaceBefore_Buttons();
                                break;
                            }
                        case 4:
                            {
                                for (int j = 2; j <= 9; j++) spaceafter[j] = false;
                                Refresh_Commands();
                                Restore_SpaceAfter_Buttons();
                                break;
                            }
                    }
                    break;
                case "All":
                    switch (SelectedPropertyItem)
                    {
                        case 1:
                            {
                                for (int j = 1; j <= 9; j++) bold[j] = false;
                                Refresh_Commands();
                                Restore_Bold_Buttons();
                                break;
                            }
                        case 2:
                            {
                                for (int j = 1; j <= 9; j++) allcaps[j] = false;
                                Refresh_Commands();
                                Restore_AllCaps_Buttons();
                                break;
                            }
                        case 3:
                            {
                                for (int j = 1; j <= 9; j++) spacebefore[j] = false;
                                Refresh_Commands();
                                Restore_SpaceBefore_Buttons();
                                break;
                            }
                        case 4:
                            {
                                for (int j = 1; j <= 9; j++) spaceafter[j] = false;
                                Refresh_Commands();
                                Restore_SpaceAfter_Buttons();
                                break;
                            }
                    }
                    break;
                case "None":
                    switch (SelectedPropertyItem)
                    {
                        case 1:
                            {
                                for (int j = 1; j <= 9; j++) bold[j] = false;
                                Refresh_Commands();
                                Restore_Bold_Buttons();
                                break;
                            }
                        case 2:
                            {
                                for (int j = 1; j <= 9; j++) allcaps[j] = false;
                                Refresh_Commands();
                                Restore_AllCaps_Buttons();
                                break;
                            }
                        case 3:
                            {
                                for (int j = 1; j <= 9; j++) spacebefore[j] = false;
                                Refresh_Commands();
                                Restore_SpaceBefore_Buttons();
                                break;
                            }
                        case 4:
                            {
                                for (int j = 1; j <= 9; j++) spaceafter[j] = false;
                                Refresh_Commands();
                                Restore_SpaceAfter_Buttons();
                                break;
                            }
                    }
                    break;
            }
        }

        private object GetTabLeaderBasedOnSelection(int tocLevel)
        {
            object tabLeader = WdTabLeader.wdTabLeaderSpaces;

            if (TabLeaders != null && TabLeaders.Count > 0 && tabType != null && tabType.Length >= 4)
            {
                var selectedTabLeaders = TabLeaders[tocLevel]?.SelectedValue as int?;
                if (selectedTabLeaders.HasValue)
                {
                    tabLeader = (WdTabLeader)selectedTabLeaders.Value;
                }
            }

            return tabLeader;
        }

        private Style GetOrCreateTocTitleStyle()
        {
            try
            {
                return ThisAddIn.Instance.Application.ActiveDocument.Styles[CustomConstants.TOC_TITLE_STYLE_NAME];
            }
            catch (Exception)
            {
                Style style = ThisAddIn.Instance.Application.ActiveDocument.Styles.Add(CustomConstants.TOC_TITLE_STYLE_NAME, WdStyleType.wdStyleTypeParagraph);

                style.set_BaseStyle(Utils.GetLocalizedStyleNameFromBuiltInStyle(WdBuiltinStyle.wdStyleNormal));
                style.Font.Bold = 1;
                style.Font.Size = 18;
                style.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphCenter;

                return style;
            }
        }
    }
}