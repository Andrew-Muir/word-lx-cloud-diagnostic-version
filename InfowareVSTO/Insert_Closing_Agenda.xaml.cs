﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.IO.Ports;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Word = Microsoft.Office.Interop.Word;
using InfowareVSTO.Common.Language;
using DocumentFormat.OpenXml.Spreadsheet;
using InfowareVSTO.Windows;
using Microsoft.Office.Interop.Word;

namespace InfowareVSTO
{
    /// <summary>
    /// Interaction logic for Insert_Closing_Agenda.xaml
    /// </summary>
    public partial class Insert_Closing_Agenda : InfowareWindow
    {
        public Insert_Closing_Agenda()
        {
            InitializeComponent();
        }

        private void InsertClosingAgenda(object sender, RoutedEventArgs e)
        {
            this.Close();
            Word.Application app = ThisAddIn.Instance.Application;
            if (app.Documents.Count >= 1)
            {
                Word.Document DOC = ThisAddIn.Instance.Application.ActiveDocument;
                if (DOC.Application.Selection.Tables.Count > 0)
                {
                    Word.Table tableSelected = DOC.Application.Selection.Tables[1];

                    if (FileLabels.IsChecked == true)
                    {
                        byte[] template = AdminPanelWebApi.GetClosingAgendaFileLabels();
                        if (template?.Length > 0)
                        {
                            InsertCustomFileLabels(template, tableSelected);
                        }
                        else
                        {
                            ((ApplicationEvents4_Event)ThisAddIn.Instance.Application).NewDocument -= ThisAddIn.Instance.OnDocumentNew;
                            Word.Document NewDOC = ThisAddIn.Instance.Application.Documents.Add();
                            ((ApplicationEvents4_Event)ThisAddIn.Instance.Application).NewDocument += ThisAddIn.Instance.OnDocumentNew;
                            Word.Range NewTableRange = NewDOC.Range(0, 0);
                            int NrRows = tableSelected.Rows.Count;
                            int NrColumns = tableSelected.Columns.Count;
                            if ((NrRows % 2) != 0)   //-----------------  INSERT CLOSING AGENDA---------------------------- 
                            {
                                NrRows++;
                            }
                            Word.Table AddendaTable = NewDOC.Tables.Add(NewTableRange, NrRows / 2, 2);
                            AddendaTable.Borders.Enable = 0;
                            NewDOC.PageSetup.RightMargin = 5;
                            NewDOC.PageSetup.LeftMargin = 5;
                            AddendaTable.Range.set_Style(Word.WdBuiltinStyle.wdStylePlainText);
                            AddendaTable.Range.Font.Size = 14;
                            AddendaTable.Range.Cells.VerticalAlignment = Word.WdCellVerticalAlignment.wdCellAlignVerticalTop;
                            AddendaTable.Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphLeft;
                            AddendaTable.Range.Cells.Width = (NewDOC.PageSetup.PageWidth / 2) - 5;
                            AddendaTable.Range.Cells.Width = (NewDOC.PageSetup.PageWidth / 2) - 5;
                            AddendaTable.Range.Cells.Height = 144;
                            for (int h = 1; h <= NrRows; h++)
                            {
                                AddendaTable.Range.Cells[h].Range.Text = tableSelected.Cell(h, 2).Range.Text.Replace("\r\a", "");
                            }  //----------------------------------------------------------------------
                            Word.Application ap = NewDOC.Application;
                            NewDOC.Activate();  // brings the second document on top
                            ap.Activate();
                        }
                    }
                    if (RecordBookIndex.IsChecked == true)
                    {
                        byte[] template = AdminPanelWebApi.GetClosingAgendaRecordBookIndex();
                        if (template?.Length > 0)
                        {
                            InsertCustomRecordBookIndex(template, tableSelected);
                        }
                        else
                        {
                            ((ApplicationEvents4_Event)ThisAddIn.Instance.Application).NewDocument -= ThisAddIn.Instance.OnDocumentNew;
                            Word.Document NewDOC = ThisAddIn.Instance.Application.Documents.Add();
                            ((ApplicationEvents4_Event)ThisAddIn.Instance.Application).NewDocument += ThisAddIn.Instance.OnDocumentNew;
                            Word.Range NewTableRange = NewDOC.Range(0, 0);
                            int NrRows = tableSelected.Rows.Count;
                            for (int i = 1; i <= NrRows; i++)  //------------------ADDING THE DATA-----------------------------
                            {
                                var p = NewDOC.Paragraphs.Add();
                                Word.Range range = p.Range;
                                range.set_Style(Word.WdBuiltinStyle.wdStylePlainText);
                                range.Font.Size = 14;
                                range.Text = tableSelected.Cell(i, 2).Range.Text.Replace("\r\a", "");
                                var p1 = NewDOC.Paragraphs.Add();
                                p1.set_Style(Word.WdBuiltinStyle.wdStylePlainText);
                                p1.Range.Font.Size = 16;
                                range.Next();
                            }
                            NewDOC.Paragraphs.Last.Range.Delete();
                            //------------------------------TITLE AND TEXT BELOW-------------------------------------------                            
                            Word.Section section = NewDOC.Sections[1];
                            try
                            {
                                Word.HeaderFooter header = section.Headers[Word.WdHeaderFooterIndex.wdHeaderFooterPrimary];
                                Word.Range headerRange = header.Range;
                                float Pos = NewDOC.PageSetup.PageWidth - NewDOC.PageSetup.RightMargin - NewDOC.PageSetup.LeftMargin;
                                Word.Paragraph paraTITLE = headerRange.Paragraphs.Add();
                                Word.Paragraph paraAFTERTITLE = paraTITLE.Range.Paragraphs.Add(paraTITLE.Range);
                                paraTITLE.TabStops.Add(Pos, Word.WdAlignmentTabAlignment.wdRight, Word.WdTabLeader.wdTabLeaderSpaces);
                                paraTITLE.set_Style(Word.WdBuiltinStyle.wdStylePlainText);
                                paraTITLE.Range.Font.Size = 20;
                                paraTITLE.Alignment = Word.WdParagraphAlignment.wdAlignParagraphLeft;
                                paraTITLE.Range.Text = "DOCUMENT\t\tTAB\n";
                                paraAFTERTITLE.set_Style(Word.WdBuiltinStyle.wdStylePlainText);
                                paraAFTERTITLE.Range.Font.Size = 20;
                                paraAFTERTITLE.Alignment = Word.WdParagraphAlignment.wdAlignParagraphLeft;
                                paraAFTERTITLE.Range.Text = "\tINDEX\n";
                            }
                            catch
                            {
                            }
                        }
                    }
                }
                else
                {
                    new InfowareInformationWindow(LanguageManager.GetTranslation(LanguageConstants.ToInsertClosingAgendaTheCursorNeeds, "To insert Closing Agenda, the cursor needs to be inside of a table")).ShowDialog();
                }
            }
        }

        private void InsertCustomRecordBookIndex(byte[] template, Word.Table tableSelected)
        {
            var newDoc = OpenTemplate(template);

            int nrRows = tableSelected.Rows.Count;
            for (int i = 1; i <= nrRows; i++)  //------------------ADDING THE DATA-----------------------------
            {
                var p = newDoc.Paragraphs.Add();
                Word.Range range = p.Range;
                range.Text = tableSelected.Cell(i, 2).Range.Text.Replace("\r\a", "");
                var p1 = newDoc.Paragraphs.Add();
                range.Next();
            }
            newDoc.Paragraphs.Last.Range.Delete();
        }

        private void InsertCustomFileLabels(byte[] template, Word.Table tableSelected)
        {
            var doc = OpenTemplate(template);
            int nrRows = tableSelected.Rows.Count;
            if (doc?.Tables?.Count > 0)
            {
                var table = doc.Tables[1];
                int cellCount = table.Rows[1].Cells.Count;
                int rowsNeeded = (int)Math.Ceiling(nrRows / (double)cellCount);
                int oldRowCount = table.Rows.Count;
                if (oldRowCount < rowsNeeded)
                {
                    for (int i = 0; i < rowsNeeded - oldRowCount; i++)
                    {
                        table.Rows.Add();
                    }
                }
                else if (oldRowCount > rowsNeeded)
                {
                    for (int i = oldRowCount; i > rowsNeeded; i--)
                    {
                        table.Rows[i].Delete();
                    }
                }

                for (var i = 1; i<= nrRows; i++)
                {
                    int rowNr = (int)Math.Ceiling(i / (double)cellCount);
                    int cellNr = i % cellCount;
                    if(cellNr == 0)
                    {
                        cellNr = cellCount;
                    }
                    try
                    {
                        table.Rows[rowNr].Cells[cellNr].Range.Text = tableSelected.Cell(i, 2).Range.Text.Replace("\r\a", "");
                    }
                    catch(Exception ex)
                    {
                    }
                }
            }
        }

        private Word.Document OpenTemplate(byte[] template)
        {
            var extension = "docx";

            var tmpFile = System.IO.Path.ChangeExtension(System.IO.Path.GetTempFileName(), extension);
            File.WriteAllBytes(tmpFile, template);
            ((ApplicationEvents4_Event)ThisAddIn.Instance.Application).NewDocument -= ThisAddIn.Instance.OnDocumentNew;
            var doc = Globals.ThisAddIn.Application.Documents.Add(tmpFile);
            ((ApplicationEvents4_Event)ThisAddIn.Instance.Application).NewDocument += ThisAddIn.Instance.OnDocumentNew;

            if (ThisAddIn.Instance.TmpTemplatesToDelete != null)
            {
                ThisAddIn.Instance.TmpTemplatesToDelete.Add(tmpFile);
            }

            return doc;
        }

        private void Cancel(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
