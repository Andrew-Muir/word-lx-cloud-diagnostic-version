﻿using InfowareVSTO.Common;
using InfowareVSTO.Common.Language;
using InfowareVSTO.Common.Models;
using InfowareVSTO.MultiLanguage;
using InfowareVSTO.Util;
using Microsoft.Office.Interop.Word;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace InfowareVSTO.Managers
{
    public class InsertContactsEntitiesBaseManager
    {
        protected const string ATTNLINE_STYLE = "AttnLine";
        protected const string ADDRESS_STYLE = "Address";
        protected string attentionText = string.Empty;
        protected List<SelectedContact> AddedRecipients;

        protected void CreateAddressStyleIfNotExists()
        {
            try
            {
                Style addressStyle = Globals.ThisAddIn.Application.ActiveDocument.Styles[ADDRESS_STYLE];
            }
            catch (Exception ex)
            {
                Style addressStyle = Globals.ThisAddIn.Application.ActiveDocument.Styles.Add(ADDRESS_STYLE, WdStyleType.wdStyleTypeParagraph);

                try
                {
                    addressStyle.set_BaseStyle("Normal Single");
                }
                catch (Exception ex2)
                {
                    addressStyle.set_BaseStyle(Utils.GetLocalizedStyleNameFromBuiltInStyle(WdBuiltinStyle.wdStyleNormal));
                }

                addressStyle.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphLeft;
            }
        }

        protected void CreateAttnLineStyleIfNotExists()
        {
            try
            {
                Style attnLineStyle = Globals.ThisAddIn.Application.ActiveDocument.Styles[ATTNLINE_STYLE];
            }
            catch (Exception ex)
            {
                Style attnLineStyle = Globals.ThisAddIn.Application.ActiveDocument.Styles.Add(ATTNLINE_STYLE, WdStyleType.wdStyleTypeParagraph);

                try
                {
                    attnLineStyle.set_BaseStyle("Normal Single");
                }
                catch (Exception ex2)
                {
                    attnLineStyle.set_BaseStyle(Utils.GetLocalizedStyleNameFromBuiltInStyle(WdBuiltinStyle.wdStyleNormal));
                }

                attnLineStyle.Font.Bold = -1;

                var language = MLanguageUtil.ActiveDocumentLanguage;

                if (language == Microsoft.Office.Core.MsoLanguageID.msoLanguageIDEnglishCanadian)
                {
                    attnLineStyle.ParagraphFormat.LeftIndent = 72;
                    attnLineStyle.ParagraphFormat.FirstLineIndent = -72;
                    attnLineStyle.ParagraphFormat.TabStops.Add(72, WdTabAlignment.wdAlignTabLeft);
                }

                if (language == Microsoft.Office.Core.MsoLanguageID.msoLanguageIDFrenchCanadian)
                {
                    attnLineStyle.ParagraphFormat.LeftIndent = 108;
                    attnLineStyle.ParagraphFormat.FirstLineIndent = -108;
                    attnLineStyle.ParagraphFormat.TabStops.Add(108, WdTabAlignment.wdAlignTabLeft);
                }
            }
        }

        protected void InsertNewParagraphIfNeeded()
        {
            Range selectionRange = Globals.ThisAddIn.Application.Selection.Range;

            Paragraph last = selectionRange.Paragraphs.Last;
            bool insertNewPar = last.Next() == null;

            if (!insertNewPar)
            {
                foreach (Table table in last.Next().Range.Tables)
                {
                    if (table.Range.Start > last.Range.Start)
                    {
                        insertNewPar = true;
                        break;
                    }
                }
            }

            if (insertNewPar)
            {
                selectionRange.Paragraphs.Add();
            }
        }

        public void CreateRecipientText(List<SelectedContact> selectedContacts, List<string> recipients, JobTitleVisibility jobTitleVisibility = JobTitleVisibility.NoJobTitle, bool companyNameVisibility = true)
        {
            foreach (var recipient in selectedContacts)
            {
                var grouppedLine = new List<string>();
                var fullName = new List<string>();

                if (!recipient.Prefix.IsEmpty())
                {
                    fullName.Add(recipient.Prefix);
                }

                if (!recipient.FirstName.IsEmpty())
                {
                    fullName.Add(recipient.FirstName);
                }

                if (!recipient.LastName.IsEmpty())
                {
                    fullName.Add(recipient.LastName);
                }
                if (!recipient.Suffix.IsEmpty())
                {
                    fullName.Add(recipient.Suffix);
                }

                string fullNameStr = string.Join(" ", fullName.ToArray());
                if (jobTitleVisibility == JobTitleVisibility.SameRow && !string.IsNullOrEmpty(recipient.JobTitle))
                {
                    fullNameStr += ", " + recipient.JobTitle;
                }

                if (!recipient.IsSelected && fullName.Count > 0)
                {
                    grouppedLine.Add(fullNameStr);
                }

                if (!recipient.IsSelected && !recipient.JobTitle.IsEmpty() && jobTitleVisibility == JobTitleVisibility.NextRow)
                {
                    grouppedLine.Add(recipient.JobTitle);
                }

                if (companyNameVisibility == true)
                {
                    var companies = recipient.Companies;
                    if (companies != null && companies.Count > 0)
                    {
                        var company = companies.OrderBy(x => x.IsDefault).FirstOrDefault();
                        if (company != null && !string.IsNullOrEmpty(company.Name))
                        {
                            grouppedLine.Add(company.Name);
                        }
                    }
                    else if (!string.IsNullOrWhiteSpace(recipient.CompanyName))
                    {
                        grouppedLine.Add(recipient.CompanyName);
                    }
                }

                var address = BaseUtils.GetAuthorAddress(recipient);
                if (address != null && !string.IsNullOrWhiteSpace(address.Address1))
                {
                    grouppedLine.Add(string.Join("\r\n", address.LinesFormat));
                }

                if (recipient.IsSelected)
                {
                    if (grouppedLine.Count > 0)
                    {
                        attentionText += BaseUtils.GetAttentionLine(recipient, jobTitleVisibility) + "\n";
                    }
                    else
                    {
                        if (fullName.Count > 0)
                        {
                            grouppedLine.Add(fullNameStr);
                        }

                        if (!recipient.JobTitle.IsEmpty() && jobTitleVisibility == JobTitleVisibility.NextRow)
                        {
                            grouppedLine.Add(recipient.JobTitle);
                        }
                    }
                }

                if (grouppedLine.Count > 0)
                {
                    recipients.Add(string.Join("\r\n", grouppedLine));
                    AddedRecipients.Add(recipient);
                }
            }
        }
    }
}
