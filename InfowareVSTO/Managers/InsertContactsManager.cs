﻿using InfowareVSTO.Common;
using InfowareVSTO.Common.DTOs;
using InfowareVSTO.Common.General;
using InfowareVSTO.Common.Language;
using InfowareVSTO.Common.Models;
using InfowareVSTO.MultiLanguage;
using InfowareVSTO.Util;
using Microsoft.Office.Interop.Word;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfowareVSTO.Managers
{
    public class InsertContactsManager :InsertContactsEntitiesBaseManager
    {
        private const string CONTACTS_TAG = "LX-CONTACTS";
        private ContentControl contentControl;

        public void InsertContacts()
        {
            ImportContacts_WPF importContacts_WPF = new ImportContacts_WPF(Common.RecipientType.MultiRow, true, System.Windows.Controls.DataGridSelectionMode.Extended);
            importContacts_WPF.ShowDialog();
            attentionText = string.Empty;
            AddedRecipients = new List<SelectedContact>();

            var recipients = new List<string>();
            if (importContacts_WPF.ResultOk)
            {
                CreateRecipientText(importContacts_WPF.SelectedContacts, recipients, importContacts_WPF.JobTitleVisibility, importContacts_WPF.CompanyNameVisibility);

                ContentControl cc = Utils.GetContainingCCWithTag(Globals.ThisAddIn.Application.ActiveDocument, CONTACTS_TAG, Globals.ThisAddIn.Application.Selection.Range);

                if (cc != null )
                {
                    contentControl = cc;
                }
                else
                {
                    InsertNewParagraphIfNeeded();
                    InsertContentControl();
                }

                if (contentControl != null)
                {
                    PaneDTO paneDTO = new PaneDTO();//todo
                    paneDTO.Recipients = string.Join("\v\v", recipients).Replace("\r\n", "\v");
                    paneDTO.AddedRecipients = AddedRecipients;
                    paneDTO.AttentionTranslation = MLanguageUtil.GetResource(MLanguageResourceEnum.Attention);

                    PaneSettings paneSettings = new PaneSettings();
                    paneSettings.HorizontalRecipientsColumnCount = SettingsManager.GetSettingAsInt("HorizontalRecipientsColumnCount", "Letters", 3);
                    paneSettings.MinRecipientCountForHorizontal = SettingsManager.GetSettingAsInt("MinRecipientCountForHorizontal", "Letters", 3);

                    string[] attentions = attentionText?.Trim()?.Split(new string[] { "\r\n\r\n", "\r\r", "\n\n", "\v\v", "\n" }, StringSplitOptions.RemoveEmptyEntries);

                    CreateAttnLineStyleIfNotExists();
                    CreateAddressStyleIfNotExists();


                    PaneUtils.InsertLetterRecipients(Globals.ThisAddIn.Application.ActiveDocument, paneDTO, paneSettings, false, attentions, contentControl);
                }
            }
        }

        private void InsertContentControl()
        {
            try
            {
                Range selectionRange = Globals.ThisAddIn.Application.Selection.Range;
                contentControl = selectionRange.ContentControls.Add(WdContentControlType.wdContentControlRichText);
                contentControl.Title = LanguageManager.GetTranslation(LanguageConstants.Contacts, "Contacts");
                contentControl.SetPlaceholderText(Text: LanguageManager.GetTranslation(LanguageConstants.Contacts, "Contacts"));
                contentControl.Tag = CONTACTS_TAG;
            }
            catch (System.NotImplementedException)
            {
                DocumentUtil.ShowCompatibilityModeError(new System.Threading.Tasks.Task(() => { InsertContentControl(); }));
            }
        }
    }
}
