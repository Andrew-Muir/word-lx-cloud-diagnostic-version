﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfowareVSTO
{
	[Serializable] 
	public class MultiPrintSettings : IMultiPrintSettings
	{
		public List<PrinterGeneralSetting> PrinterBinSettings { get; set; }
		public PrinterSpecialPrintersSetting SpecialPrinterSettings { get; set; }
		public Dictionary<string, SpecialPaperSettings> SpecialPaperSettingsDict { get; set; }
		
		//This is not used but needs to be here because of te interface
		public bool InEditMode { get; set; } 
		public MultiPrintSettings RestoreToDefaultWhereSame()
		{
			return this;
		}

		public MultiPrintSettings GetMultiPrintSettingsObject()
		{
			return this;
		}

		public MultiPrintSettings()
		{
			PrinterBinSettings = new List<PrinterGeneralSetting>();
			SpecialPaperSettingsDict = new Dictionary<string, SpecialPaperSettings>();
			SpecialPrinterSettings = new PrinterSpecialPrintersSetting();
		}
	}

	[Serializable]
	public class PrinterGeneralSetting
	{
		public string Name { get; set; }
		public string AddressLabelBin { get; set; }		
		public string A4Bin { get; set; }
		public bool? A4IsDuplex { get; set; } = false;
		public string EnvelopesBin { get; set; }
		public string FileCopyBin { get; set; }
		public bool? FileCopyIsDuplex { get; set; } = false;
		public string LegalBin { get; set; }
		public bool? LegalIsDuplex { get; set; } = false;
		public string Letterhead1Bin { get; set; }
		public string Letterhead2Bin { get; set; }
		public bool? Letterhead2IsDuplex { get; set; } = false;
		public string PlainPaperBin { get; set; }
		public bool? PlainPaperIsDuplex { get; set; } = false;
		public bool? LetterHeadOnPlain { get; set; } = false;
		public int? AddressLabelTemplateId { get; set; } = 0;
		public string AddressLabelTemplateName { get; set; }
		public bool? LetterHeadPrePrinted { get; set; }
		public bool? ForcePromptsBlackAndWhite { get; set; } = false;
	}

	[Serializable]
	public class PrinterSpecialPrintersSetting
	{
		public string A4Printer { get; set; }
		public string A4Bin { get; set; }
		public string AddressLabelPrinter { get; set; }
		public string AddressLabelBin { get; set; }
		public string EnvelopePrinter { get; set; }
		public string EnvelopeBin { get; set; }
		public string PdfPrinter { get; set; }
		public string PdfBin { get; set; }
		public string FileCopyPrinter { get; set; }
		public string FileCopyBin { get; set; }
		public string LegalPrinter { get; set; }
		public string LegalBin { get; set; }
		public string LetterheadPrinter { get; set; }
		public string Letterhead1Bin { get; set; }
		public string Letterhead2Bin { get; set; }
	}

	[Serializable]
	public class SpecialPaperSettings
	{
		public string Printer { get; set; }
		public string Bin1 { get; set; }
		public string Bin2 { get; set; }
		public int? Duplex { get; set; } = 0;
		public bool? Color { get; set; } = false;

		public SpecialPaperSettings()
		{
			Duplex = -1;
			Color = true;
		}
	}
}
