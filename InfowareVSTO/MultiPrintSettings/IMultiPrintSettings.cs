﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfowareVSTO
{
    public interface IMultiPrintSettings
    {
        List<PrinterGeneralSetting> PrinterBinSettings { get; set; }
        PrinterSpecialPrintersSetting SpecialPrinterSettings { get; set; }
        Dictionary<string, SpecialPaperSettings> SpecialPaperSettingsDict { get; set; }
        bool InEditMode { get; set; }
        MultiPrintSettings RestoreToDefaultWhereSame();
        MultiPrintSettings GetMultiPrintSettingsObject();
    }
}
