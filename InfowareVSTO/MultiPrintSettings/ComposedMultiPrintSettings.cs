﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfowareVSTO
{
    public class ComposedMultiPrintSettings : IMultiPrintSettings
    {
        public bool inEditMode;
        public bool InEditMode
        {
            get
            {
                return inEditMode;
            }
            set
            {
                inEditMode = value;
                if (value)
                {
                    printerBinSettings = null;
                    specialPrinterSettings = null;
                    specialPaperSettingsDict = null;
                }
            }
        }

        public List<PrinterGeneralSetting> printerBinSettings;
        public List<PrinterGeneralSetting> PrinterBinSettings
        {
            get
            {
                if (InEditMode)
                {
                    return personalMultiPrintSettings.PrinterBinSettings;
                }

                if (printerBinSettings == null)
                {
                    printerBinSettings = ComposePrinterBinSettings();
                }

                return printerBinSettings;
            }

            set
            {
                personalMultiPrintSettings.PrinterBinSettings = value;
            }
        }

        public PrinterSpecialPrintersSetting specialPrinterSettings;
        public PrinterSpecialPrintersSetting SpecialPrinterSettings
        {
            get
            {
                if (InEditMode)
                {
                    return personalMultiPrintSettings.SpecialPrinterSettings;
                }

                if (specialPrinterSettings == null)
                {
                    specialPrinterSettings = ComposeSpecialPrinterSettings();
                }

                return specialPrinterSettings;
            }

            set
            {
                personalMultiPrintSettings.SpecialPrinterSettings = value;
            }
        }
        public Dictionary<string, SpecialPaperSettings> specialPaperSettingsDict;
        public Dictionary<string, SpecialPaperSettings> SpecialPaperSettingsDict
        {
            get
            {
                if (InEditMode)
                {
                    return personalMultiPrintSettings.SpecialPaperSettingsDict;
                }

                if (specialPaperSettingsDict == null)
                {
                    specialPaperSettingsDict = ComposeSpecialPaperSettingsDict();
                }

                return specialPaperSettingsDict;
            }

            set
            {
                personalMultiPrintSettings.SpecialPaperSettingsDict = value;
            }
        }

        private MultiPrintSettings companyMultiPrintSettings;
        private MultiPrintSettings personalMultiPrintSettings;

        public ComposedMultiPrintSettings(MultiPrintSettings companySettings, MultiPrintSettings personalSettings)
        {
            companyMultiPrintSettings = companySettings;
            personalMultiPrintSettings = personalSettings;
        }

        private PrinterSpecialPrintersSetting ComposeSpecialPrinterSettings()
        {
            if (companyMultiPrintSettings.SpecialPrinterSettings == null)
            {
                companyMultiPrintSettings.SpecialPrinterSettings = new PrinterSpecialPrintersSetting();
            }

            if (personalMultiPrintSettings.SpecialPrinterSettings == null)
            {
                personalMultiPrintSettings.SpecialPrinterSettings = new PrinterSpecialPrintersSetting();
            }

            PrinterSpecialPrintersSetting result = new PrinterSpecialPrintersSetting()
            {
                A4Bin = personalMultiPrintSettings.SpecialPrinterSettings.A4Bin ?? companyMultiPrintSettings.SpecialPrinterSettings.A4Bin,
                A4Printer = personalMultiPrintSettings.SpecialPrinterSettings.A4Printer ?? companyMultiPrintSettings.SpecialPrinterSettings.A4Printer,
                AddressLabelBin = personalMultiPrintSettings.SpecialPrinterSettings.AddressLabelBin ?? companyMultiPrintSettings.SpecialPrinterSettings.AddressLabelBin,
                AddressLabelPrinter = personalMultiPrintSettings.SpecialPrinterSettings.AddressLabelPrinter ?? companyMultiPrintSettings.SpecialPrinterSettings.AddressLabelPrinter,
                EnvelopeBin = personalMultiPrintSettings.SpecialPrinterSettings.EnvelopeBin ?? companyMultiPrintSettings.SpecialPrinterSettings.EnvelopeBin,
                EnvelopePrinter = personalMultiPrintSettings.SpecialPrinterSettings.EnvelopePrinter ?? companyMultiPrintSettings.SpecialPrinterSettings.EnvelopePrinter,
                FileCopyBin = personalMultiPrintSettings.SpecialPrinterSettings.FileCopyBin ?? companyMultiPrintSettings.SpecialPrinterSettings.FileCopyBin,
                FileCopyPrinter = personalMultiPrintSettings.SpecialPrinterSettings.FileCopyPrinter ?? companyMultiPrintSettings.SpecialPrinterSettings.FileCopyPrinter,
                LegalBin = personalMultiPrintSettings.SpecialPrinterSettings.LegalBin ?? companyMultiPrintSettings.SpecialPrinterSettings.LegalBin,
                LegalPrinter = personalMultiPrintSettings.SpecialPrinterSettings.LegalPrinter ?? companyMultiPrintSettings.SpecialPrinterSettings.LegalPrinter,
                Letterhead1Bin = personalMultiPrintSettings.SpecialPrinterSettings.Letterhead1Bin ?? companyMultiPrintSettings.SpecialPrinterSettings.Letterhead1Bin,
                Letterhead2Bin = personalMultiPrintSettings.SpecialPrinterSettings.Letterhead2Bin ?? companyMultiPrintSettings.SpecialPrinterSettings.Letterhead2Bin,
                LetterheadPrinter = personalMultiPrintSettings.SpecialPrinterSettings.LetterheadPrinter ?? companyMultiPrintSettings.SpecialPrinterSettings.LetterheadPrinter,
                PdfBin = personalMultiPrintSettings.SpecialPrinterSettings.PdfBin ?? companyMultiPrintSettings.SpecialPrinterSettings.PdfBin,
                PdfPrinter = personalMultiPrintSettings.SpecialPrinterSettings.PdfPrinter ?? companyMultiPrintSettings.SpecialPrinterSettings.PdfPrinter,
            };

            return result;
        }

        private Dictionary<string, SpecialPaperSettings> ComposeSpecialPaperSettingsDict()
        {
            if (companyMultiPrintSettings.SpecialPaperSettingsDict == null)
            {
                companyMultiPrintSettings.SpecialPaperSettingsDict = new Dictionary<string, SpecialPaperSettings>();
            }

            if (personalMultiPrintSettings.SpecialPaperSettingsDict == null)
            {
                personalMultiPrintSettings.SpecialPaperSettingsDict = new Dictionary<string, SpecialPaperSettings>();
            }

            Dictionary<string, SpecialPaperSettings> result = new Dictionary<string, SpecialPaperSettings>();

            List<string> added = new List<string>();
            foreach (var keyValuePair in companyMultiPrintSettings.SpecialPaperSettingsDict)
            {
                var personalSetting = personalMultiPrintSettings.SpecialPaperSettingsDict.Where(x => x.Key == keyValuePair.Key).FirstOrDefault();
                if (personalSetting.Key != null)
                {
                    added.Add(keyValuePair.Key);

                    var setting = keyValuePair.Value;
                    SpecialPaperSettings tempSetting = new SpecialPaperSettings()
                    {
                        Bin1 = personalSetting.Value.Bin1 ?? setting.Bin1,
                        Bin2 = personalSetting.Value.Bin2 ?? setting.Bin2,
                        Color = personalSetting.Value.Color ?? setting.Color,
                        Duplex = personalSetting.Value.Duplex ?? setting.Duplex,
                        Printer = personalSetting.Value.Printer ?? setting.Printer,
                    };

                    result.Add(keyValuePair.Key, tempSetting);
                }
                else
                {
                    result.Add(keyValuePair.Key, keyValuePair.Value);
                }
            }

            foreach (var keyValuePair in personalMultiPrintSettings.SpecialPaperSettingsDict.Where(x => !added.Contains(x.Key)))
            {
                result.Add(keyValuePair.Key, keyValuePair.Value);
            }

            return result;
        }

        private List<PrinterGeneralSetting> ComposePrinterBinSettings()
        {
            if (companyMultiPrintSettings.PrinterBinSettings == null)
            {
                companyMultiPrintSettings.PrinterBinSettings = new List<PrinterGeneralSetting>();
            }

            if (personalMultiPrintSettings.PrinterBinSettings == null)
            {
                personalMultiPrintSettings.PrinterBinSettings = new List<PrinterGeneralSetting>();
            }

            List<PrinterGeneralSetting> result = new List<PrinterGeneralSetting>();

            List<string> added = new List<string>();
            foreach (var setting in companyMultiPrintSettings.PrinterBinSettings)
            {
                var personalSetting = personalMultiPrintSettings.PrinterBinSettings.Where(x => x.Name == setting.Name).FirstOrDefault();
                if (personalSetting != null)
                {
                    added.Add(setting.Name);

                    PrinterGeneralSetting tempSetting = new PrinterGeneralSetting()
                    {
                        A4Bin = personalSetting.A4Bin ?? setting.A4Bin,
                        A4IsDuplex = personalSetting.A4IsDuplex ?? setting.A4IsDuplex,
                        AddressLabelBin = personalSetting.AddressLabelBin ?? setting.AddressLabelBin,
                        AddressLabelTemplateId = personalSetting.AddressLabelTemplateId ?? setting.AddressLabelTemplateId,
                        AddressLabelTemplateName = personalSetting.AddressLabelTemplateName ?? setting.AddressLabelTemplateName,
                        EnvelopesBin = personalSetting.EnvelopesBin ?? setting.EnvelopesBin,
                        FileCopyBin = personalSetting.FileCopyBin ?? setting.FileCopyBin,
                        FileCopyIsDuplex = personalSetting.FileCopyIsDuplex ?? setting.FileCopyIsDuplex,
                        ForcePromptsBlackAndWhite = personalSetting.ForcePromptsBlackAndWhite ?? setting.ForcePromptsBlackAndWhite,
                        LegalBin = personalSetting.LegalBin ?? setting.LegalBin,
                        LegalIsDuplex = personalSetting.LegalIsDuplex ?? setting.LegalIsDuplex,
                        Letterhead1Bin = personalSetting.Letterhead1Bin ?? setting.Letterhead1Bin,
                        Letterhead2Bin = personalSetting.Letterhead2Bin ?? setting.Letterhead2Bin,
                        Letterhead2IsDuplex = personalSetting.Letterhead2IsDuplex ?? setting.Letterhead2IsDuplex,
                        LetterHeadOnPlain = personalSetting.LetterHeadOnPlain ?? setting.LetterHeadOnPlain,
                        LetterHeadPrePrinted = personalSetting.LetterHeadPrePrinted ?? setting.LetterHeadPrePrinted,
                        Name = personalSetting.Name ?? setting.Name,
                        PlainPaperBin = personalSetting.PlainPaperBin ?? setting.PlainPaperBin,
                        PlainPaperIsDuplex = personalSetting.PlainPaperIsDuplex ?? setting.PlainPaperIsDuplex,
                    };

                    result.Add(tempSetting);
                }
                else
                {
                    result.Add(setting);
                }
            }

            result.AddRange(personalMultiPrintSettings.PrinterBinSettings.Where(x => !added.Contains(x.Name)));

            return result;
        }

        public MultiPrintSettings RestoreToDefaultWhereSame()
        {
            if (personalMultiPrintSettings.SpecialPrinterSettings != null && personalMultiPrintSettings.SpecialPrinterSettings != null)
            {
                if (companyMultiPrintSettings.SpecialPrinterSettings.A4Bin == personalMultiPrintSettings.SpecialPrinterSettings.A4Bin)
                {
                    personalMultiPrintSettings.SpecialPrinterSettings.A4Bin = null;
                }
                if (companyMultiPrintSettings.SpecialPrinterSettings.A4Printer == personalMultiPrintSettings.SpecialPrinterSettings.A4Printer)
                {
                    personalMultiPrintSettings.SpecialPrinterSettings.A4Printer = null;
                }
                if (companyMultiPrintSettings.SpecialPrinterSettings.AddressLabelBin == personalMultiPrintSettings.SpecialPrinterSettings.AddressLabelBin)
                {
                    personalMultiPrintSettings.SpecialPrinterSettings.AddressLabelBin = null;
                }
                if (companyMultiPrintSettings.SpecialPrinterSettings.AddressLabelPrinter == personalMultiPrintSettings.SpecialPrinterSettings.AddressLabelPrinter)
                {
                    personalMultiPrintSettings.SpecialPrinterSettings.AddressLabelPrinter = null;
                }
                if (companyMultiPrintSettings.SpecialPrinterSettings.EnvelopeBin == personalMultiPrintSettings.SpecialPrinterSettings.EnvelopeBin)
                {
                    personalMultiPrintSettings.SpecialPrinterSettings.EnvelopeBin = null;
                }
                if (companyMultiPrintSettings.SpecialPrinterSettings.EnvelopePrinter == personalMultiPrintSettings.SpecialPrinterSettings.EnvelopePrinter)
                {
                    personalMultiPrintSettings.SpecialPrinterSettings.EnvelopePrinter = null;
                }
                if (companyMultiPrintSettings.SpecialPrinterSettings.FileCopyBin == personalMultiPrintSettings.SpecialPrinterSettings.FileCopyBin)
                {
                    personalMultiPrintSettings.SpecialPrinterSettings.FileCopyBin = null;
                }
                if (companyMultiPrintSettings.SpecialPrinterSettings.FileCopyPrinter == personalMultiPrintSettings.SpecialPrinterSettings.FileCopyPrinter)
                {
                    personalMultiPrintSettings.SpecialPrinterSettings.FileCopyPrinter = null;
                }
                if (companyMultiPrintSettings.SpecialPrinterSettings.LegalBin == personalMultiPrintSettings.SpecialPrinterSettings.LegalBin)
                {
                    personalMultiPrintSettings.SpecialPrinterSettings.LegalBin = null;
                }
                if (companyMultiPrintSettings.SpecialPrinterSettings.LegalPrinter == personalMultiPrintSettings.SpecialPrinterSettings.LegalPrinter)
                {
                    personalMultiPrintSettings.SpecialPrinterSettings.LegalPrinter = null;
                }
                if (companyMultiPrintSettings.SpecialPrinterSettings.Letterhead1Bin == personalMultiPrintSettings.SpecialPrinterSettings.Letterhead1Bin)
                {
                    personalMultiPrintSettings.SpecialPrinterSettings.Letterhead1Bin = null;
                }
                if (companyMultiPrintSettings.SpecialPrinterSettings.Letterhead2Bin == personalMultiPrintSettings.SpecialPrinterSettings.Letterhead2Bin)
                {
                    personalMultiPrintSettings.SpecialPrinterSettings.Letterhead2Bin = null;
                }
                if (companyMultiPrintSettings.SpecialPrinterSettings.LetterheadPrinter == personalMultiPrintSettings.SpecialPrinterSettings.LetterheadPrinter)
                {
                    personalMultiPrintSettings.SpecialPrinterSettings.LetterheadPrinter = null;
                }
                if (companyMultiPrintSettings.SpecialPrinterSettings.PdfBin == personalMultiPrintSettings.SpecialPrinterSettings.PdfBin)
                {
                    personalMultiPrintSettings.SpecialPrinterSettings.PdfBin = null;
                }
                if (companyMultiPrintSettings.SpecialPrinterSettings.PdfPrinter == personalMultiPrintSettings.SpecialPrinterSettings.PdfPrinter)
                {
                    personalMultiPrintSettings.SpecialPrinterSettings.PdfPrinter = null;
                }
            }

            if (personalMultiPrintSettings.PrinterBinSettings != null && companyMultiPrintSettings.PrinterBinSettings != null)
                foreach (var setting in personalMultiPrintSettings.PrinterBinSettings)
                {
                    var companySetting = companyMultiPrintSettings.PrinterBinSettings.Where(x => x.Name == setting.Name).FirstOrDefault();

                    if (companySetting != null)
                    {
                        if (companySetting.A4Bin == setting.A4Bin)
                        {
                            setting.A4Bin = null;
                        }
                        if (companySetting.A4IsDuplex == setting.A4IsDuplex)
                        {
                            setting.A4IsDuplex = null;
                        }
                        if (companySetting.AddressLabelBin == setting.AddressLabelBin)
                        {
                            setting.AddressLabelBin = null;
                        }
                        if (companySetting.AddressLabelTemplateId == setting.AddressLabelTemplateId)
                        {
                            setting.AddressLabelTemplateId = null;
                        }
                        if (companySetting.AddressLabelTemplateName == setting.AddressLabelTemplateName)
                        {
                            setting.AddressLabelTemplateName = null;
                        }
                        if (companySetting.EnvelopesBin == setting.EnvelopesBin)
                        {
                            setting.EnvelopesBin = null;
                        }
                        if (companySetting.FileCopyBin == setting.FileCopyBin)
                        {
                            setting.FileCopyBin = null;
                        }
                        if (companySetting.FileCopyIsDuplex == setting.FileCopyIsDuplex)
                        {
                            setting.FileCopyIsDuplex = null;
                        }
                        if (companySetting.ForcePromptsBlackAndWhite == setting.ForcePromptsBlackAndWhite)
                        {
                            setting.ForcePromptsBlackAndWhite = null;
                        }
                        if (companySetting.LegalBin == setting.LegalBin)
                        {
                            setting.LegalBin = null;
                        }
                        if (companySetting.LegalIsDuplex == setting.LegalIsDuplex)
                        {
                            setting.LegalIsDuplex = null;
                        }
                        if (companySetting.Letterhead1Bin == setting.Letterhead1Bin)
                        {
                            setting.Letterhead1Bin = null;
                        }
                        if (companySetting.Letterhead2Bin == setting.Letterhead2Bin)
                        {
                            setting.Letterhead2Bin = null;
                        }
                        if (companySetting.Letterhead2IsDuplex == setting.Letterhead2IsDuplex)
                        {
                            setting.Letterhead2IsDuplex = null;
                        }
                        if (companySetting.LetterHeadOnPlain == setting.LetterHeadOnPlain)
                        {
                            setting.LetterHeadOnPlain = null;
                        }
                        if (companySetting.LetterHeadPrePrinted == setting.LetterHeadPrePrinted)
                        {
                            setting.LetterHeadPrePrinted = null;
                        }
                        if (companySetting.PlainPaperBin == setting.PlainPaperBin)
                        {
                            setting.PlainPaperBin = null;
                        }
                        if (companySetting.PlainPaperIsDuplex == setting.PlainPaperIsDuplex)
                        {
                            setting.PlainPaperIsDuplex = null;
                        }
                    }
                }

            if (personalMultiPrintSettings.SpecialPaperSettingsDict != null && companyMultiPrintSettings.SpecialPaperSettingsDict != null)
                foreach (var keyValuePair in personalMultiPrintSettings.SpecialPaperSettingsDict)
                {
                    if (companyMultiPrintSettings.SpecialPaperSettingsDict.TryGetValue(keyValuePair.Key, out var companySetting))
                    {
                        var setting = keyValuePair.Value;

                        if (setting.Bin1 == companySetting.Bin1)
                        {
                            setting.Bin1 = null;
                        }
                        if (setting.Bin2 == companySetting.Bin2)
                        {
                            setting.Bin2 = null;
                        }
                        if (setting.Color == companySetting.Color)
                        {
                            setting.Color = null;
                        }
                        if (setting.Duplex == companySetting.Duplex)
                        {
                            setting.Duplex = null;
                        }
                    }
                }

            return personalMultiPrintSettings;
        }

        public void RestoreToCompanyDefault()
        {
            this.personalMultiPrintSettings = new MultiPrintSettings();
            printerBinSettings = null;
            specialPrinterSettings = null;
            specialPaperSettingsDict = null;
        }

       public MultiPrintSettings GetMultiPrintSettingsObject()
        {
            return new MultiPrintSettings()
            {
                InEditMode = false,
                SpecialPrinterSettings = this.SpecialPrinterSettings,
                PrinterBinSettings = this.PrinterBinSettings,
                SpecialPaperSettingsDict = this.SpecialPaperSettingsDict
            };
        }
    }
}
