﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace InfowareVSTO
{
	/// <summary>
	/// Interaction logic for Message_WPF.xaml
	/// </summary>
	public partial class Message_WPF : InfowareWindow
	{	
		public Message_WPF(string message)
		{
			InitializeComponent();
			textBlockMessage.Text = message;
		}

		private void ButtonOK_Click(object sender, RoutedEventArgs e)
		{			
			this.Close();
		}
	}
}
