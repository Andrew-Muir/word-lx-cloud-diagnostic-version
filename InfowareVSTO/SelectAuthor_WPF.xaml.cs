﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Word = Microsoft.Office.Interop.Word;
using Newtonsoft.Json.Linq;
using InfowareVSTO.Common.DTOs;
using System.Collections.ObjectModel;
using System.Collections;
using InfowareVSTO.Common.Language;

namespace InfowareVSTO
{
    /// <summary>
    /// Interaction logic for SelectAuthor_WPF.xaml
    /// </summary>
    public partial class SelectAuthor_WPF : InfowareWindow
    {
		public bool ResultOk { get; set; }
		public AuthorDTO SelectedAuthor { get; set; }

        public ObservableCollection<AuthorDTO> Authors { get; set; }
		public ICollectionView AuthorsView    
		{    
			get { return CollectionViewSource.GetDefaultView(Authors); }    
		} 

		private string search;    
     
		public string Search    
		{    
			get { return search; }    
			set    
			{    
				search = value;    
				NotifyPropertyChanged("Search");    
				AuthorsView.Refresh();
			}    
		}  

		// Property Change Logic  
		public event PropertyChangedEventHandler PropertyChanged;    
		private void NotifyPropertyChanged(string propertyName)  
		{  
			if (PropertyChanged != null)  
			{  
				PropertyChanged(this, new PropertyChangedEventArgs(propertyName));  
			}  
		} 

		private bool Filter(AuthorDTO author)    
		{    
			return Search == null    
				|| author.FirstName.IndexOf(Search, StringComparison.OrdinalIgnoreCase) != -1    
				|| author.LastName.IndexOf(Search, StringComparison.OrdinalIgnoreCase) != -1    
				|| author.JobTitle.IndexOf(Search, StringComparison.OrdinalIgnoreCase) != -1
			    || author.FirstName.RemoveDiacritics().IndexOf(Search, StringComparison.OrdinalIgnoreCase) != -1
				|| author.LastName.RemoveDiacritics().IndexOf(Search, StringComparison.OrdinalIgnoreCase) != -1
				|| author.JobTitle.RemoveDiacritics().IndexOf(Search, StringComparison.OrdinalIgnoreCase) != -1;
		} 

		public class NotifiableCollectionView : ListCollectionView    
		{    
			public NotifiableCollectionView(IList sourceCollection, object model)    
				: base(sourceCollection)    
			{    
				if (model is INotifyPropertyChanged)    
					(model as INotifyPropertyChanged).PropertyChanged += NotifiableCollectionView_PropertyChanged;    
			}    
     
			void NotifiableCollectionView_PropertyChanged(object sender, PropertyChangedEventArgs e)    
			{    
				if (e.PropertyName == "Search")    
					this.Refresh();    
			}    
		} 

        public SelectAuthor_WPF()
        {
            InitializeComponent();

			var authors = AdminPanelWebApi.GetAuthors((int)LanguageManager.GetLanguage());
			authors = authors?.OrderBy(x => x.LastName).ThenBy(x => x.FirstName).ToList();
            var favouriteAuthors = AdminPanelWebApi.GetFavouriteAuthors();
            if (favouriteAuthors != null)
            {
                List<int> favouriteAuthorsIds = favouriteAuthors.Select(x => x.Id).ToList();

                var tempAuthors = authors.Where(x => favouriteAuthorsIds.Contains(x.Id)).ToList();
                tempAuthors.AddRange(authors.Where(x => !favouriteAuthorsIds.Contains(x.Id)));
                authors = tempAuthors;
            }

			Authors = new ObservableCollection<AuthorDTO>(authors);			
			
			AuthorsView.Filter = new Predicate<object>(o => Filter(o as AuthorDTO)); 

			DataContext = this;
        }
        private void ButtonOK_Click(object sender, RoutedEventArgs e)
        {
			if (listViewAuthors.SelectedIndex != -1)
			{
				this.SelectedAuthor = (AuthorDTO)listViewAuthors.SelectedItem;
				this.ResultOk = true;
				this.Close();
			}
        }

		private void ButtonCancel_Click(object sender, RoutedEventArgs e)
		{
			this.ResultOk = false;
			this.Close();
		}

        private void ListViewItem_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ButtonOK_Click(null, null);
        }
    }
}
