﻿using InfowareVSTO.Windows;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace InfowareVSTO
{
    /// <summary>
    /// Interaction logic for LoginDialog.xaml
    /// </summary>
    public partial class LoginDialog : InfowareWindow
    {
        public LoginDialog()
        {
            InitializeComponent();
            this.Ok = false;
        }

        public bool Ok { get; set; }

        private void SubmitBtn_Click(object sender, RoutedEventArgs e)
        {
            RestClient client = new RestClient(Login.WebserviceURL);
            RestRequest request;
            IRestResponse response;
            JObject content;
            string email = this.EmailTextBox.Text;

            this.EmailTBlk.Text = "Email";
            this.EmailTBlk.Foreground = new SolidColorBrush(Color.FromRgb(18, 62, 111));
            this.LicenseKeyTBlk.Text = "License Key";
            this.LicenseKeyTBlk.Foreground = new SolidColorBrush(Color.FromRgb(18, 62, 111));

            if (!IsValidEmail(email))
            {
                this.EmailTBlk.Text = "Email is not valid";
                this.EmailTBlk.Foreground = Brushes.Red;
                return;
            }

            string licenseKey = "";
          
            licenseKey = this.LicenseKeyTextBox.Text;

            request = new RestRequest("api/WebApi/GetActivateLicense?email=" + email + "&licenseKey=" + licenseKey + "&browserId=" + Utils.GetProcessorId(), Method.GET);
            response = client.Execute(request);

            content = JsonConvert.DeserializeObject<JObject>(response.Content);

            int statusCode2 = content["StatusCode"].ToObject<int>();

            if (statusCode2 == 300)
            {
                this.EmailTBlk.Text = "No user exists with this email + license key exists";
                this.EmailTBlk.Foreground = Brushes.Red;
            }
            else if (statusCode2 == 200 || statusCode2 == 201 || statusCode2 == 202)
            {
                Microsoft.Win32.Registry.SetValue(Login.InfowareKeyFolder, "AuthKey", content["Data"].ToObject<string>());
                Microsoft.Win32.Registry.SetValue(Login.InfowareKeyFolder, "Email", email);
                Microsoft.Win32.Registry.SetValue(Login.InfowareKeyFolder, "LicenseKey", licenseKey);

                if (this.LicenseKeyTextBox.Visibility == Visibility.Visible)
                {
                    this.Ok = true;
                    ThisAddIn.Instance.Ribbon.Invalidate();
                    this.Close();
                }
            }
            else if (statusCode2 == 500 || statusCode2 == 302)
            {
                this.LicenseKeyTBlk.Text = "Please enter a valid License key";
                this.LicenseKeyTBlk.Foreground = Brushes.Red;
            }
            else if (statusCode2 == 301)
            {
                this.EmailTBlk.Text = "Max number of devices was reached, cannot use more devices";
                this.EmailTBlk.Foreground = Brushes.Red;
            }
        }

        private void ShowKeyInput()
        {
            this.LicenseKeyTBlk.Visibility = Visibility.Visible;
            this.LicenseKeyTextBox.Visibility = Visibility.Visible;
            this.LicenseKeyTextBox.Focus();
        }

        private void SwitchToLicenseKeyMessage()
        {
            this.EmailTBlk.Visibility = Visibility.Collapsed;
            this.EmailTextBox.Visibility = Visibility.Collapsed;
            this.LicenseKeyTBlk.Visibility = Visibility.Collapsed;
            this.LicenseKeyTextBox.Visibility = Visibility.Collapsed;
            this.SubmitBtn.Visibility = Visibility.Collapsed;

            this.YourLicenseKeyIs.Visibility = Visibility.Visible;
            this.LicenseKeyValue.Visibility = Visibility.Visible;
            this.OKBtn.Visibility = Visibility.Visible;
        }

        private bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }

        private void OKBtn_Click(object sender, RoutedEventArgs e)
        {
            this.Ok = true;
            this.Close();
        }
    }
}
