﻿#pragma checksum "..\..\..\Windows\InfowareDraftDateStampWindow.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "45971033EB73A20754F1B538F09D6533CEA6343AB14255E7ECDC115A4E43205C"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using InfowareVSTO;
using InfowareVSTO.Behaviors;
using InfowareVSTO.Common.Language;
using InfowareVSTO.Language;
using InfowareVSTO.Windows;
using Microsoft.Xaml.Behaviors;
using Microsoft.Xaml.Behaviors.Core;
using Microsoft.Xaml.Behaviors.Input;
using Microsoft.Xaml.Behaviors.Layout;
using Microsoft.Xaml.Behaviors.Media;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace InfowareVSTO.Windows {
    
    
    /// <summary>
    /// InfowareDraftDateStampWindow
    /// </summary>
    public partial class InfowareDraftDateStampWindow : InfowareVSTO.InfowareWindow, System.Windows.Markup.IComponentConnector {
        
        
        #line 62 "..\..\..\Windows\InfowareDraftDateStampWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TbDraftNumber;
        
        #line default
        #line hidden
        
        
        #line 64 "..\..\..\Windows\InfowareDraftDateStampWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton RbTopMargin;
        
        #line default
        #line hidden
        
        
        #line 72 "..\..\..\Windows\InfowareDraftDateStampWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton RbLeftMargin;
        
        #line default
        #line hidden
        
        
        #line 80 "..\..\..\Windows\InfowareDraftDateStampWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton RbRightMargin;
        
        #line default
        #line hidden
        
        
        #line 89 "..\..\..\Windows\InfowareDraftDateStampWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox ComboDateOptions;
        
        #line default
        #line hidden
        
        
        #line 90 "..\..\..\Windows\InfowareDraftDateStampWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox CbIncludeTime;
        
        #line default
        #line hidden
        
        
        #line 98 "..\..\..\Windows\InfowareDraftDateStampWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BtnRemove;
        
        #line default
        #line hidden
        
        
        #line 113 "..\..\..\Windows\InfowareDraftDateStampWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BtnOk;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/InfowareVSTO;component/windows/infowaredraftdatestampwindow.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\Windows\InfowareDraftDateStampWindow.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal System.Delegate _CreateDelegate(System.Type delegateType, string handler) {
            return System.Delegate.CreateDelegate(delegateType, this, handler);
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.TbDraftNumber = ((System.Windows.Controls.TextBox)(target));
            
            #line 62 "..\..\..\Windows\InfowareDraftDateStampWindow.xaml"
            this.TbDraftNumber.PreviewTextInput += new System.Windows.Input.TextCompositionEventHandler(this.NumericTextBoxValidator);
            
            #line default
            #line hidden
            return;
            case 2:
            this.RbTopMargin = ((System.Windows.Controls.RadioButton)(target));
            return;
            case 3:
            this.RbLeftMargin = ((System.Windows.Controls.RadioButton)(target));
            return;
            case 4:
            this.RbRightMargin = ((System.Windows.Controls.RadioButton)(target));
            return;
            case 5:
            this.ComboDateOptions = ((System.Windows.Controls.ComboBox)(target));
            
            #line 89 "..\..\..\Windows\InfowareDraftDateStampWindow.xaml"
            this.ComboDateOptions.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.OnComboDateOptionsSelectionChanged);
            
            #line default
            #line hidden
            return;
            case 6:
            this.CbIncludeTime = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 7:
            this.BtnRemove = ((System.Windows.Controls.Button)(target));
            
            #line 98 "..\..\..\Windows\InfowareDraftDateStampWindow.xaml"
            this.BtnRemove.Click += new System.Windows.RoutedEventHandler(this.OnRemoveButtonClick);
            
            #line default
            #line hidden
            return;
            case 8:
            this.BtnOk = ((System.Windows.Controls.Button)(target));
            
            #line 113 "..\..\..\Windows\InfowareDraftDateStampWindow.xaml"
            this.BtnOk.Click += new System.Windows.RoutedEventHandler(this.OnOkButtonClick);
            
            #line default
            #line hidden
            return;
            case 9:
            
            #line 121 "..\..\..\Windows\InfowareDraftDateStampWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.OnCancelButtonClick);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

