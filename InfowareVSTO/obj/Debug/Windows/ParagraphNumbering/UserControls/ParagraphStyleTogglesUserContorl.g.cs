﻿#pragma checksum "..\..\..\..\..\Windows\ParagraphNumbering\UserControls\ParagraphStyleTogglesUserContorl.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "FBE463D1CCF74A9C4EBA7475111673E94CB3AC1A4D6E584AB117C06EFD503F06"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using InfowareVSTO.Common.Language;
using InfowareVSTO.Language;
using InfowareVSTO.Windows.ParagraphNumbering.UserControls;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace InfowareVSTO.Windows.ParagraphNumbering.UserControls {
    
    
    /// <summary>
    /// ParagraphStyleTogglesUserContorl
    /// </summary>
    public partial class ParagraphStyleTogglesUserContorl : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 64 "..\..\..\..\..\Windows\ParagraphNumbering\UserControls\ParagraphStyleTogglesUserContorl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Primitives.ToggleButton HangButton;
        
        #line default
        #line hidden
        
        
        #line 74 "..\..\..\..\..\Windows\ParagraphNumbering\UserControls\ParagraphStyleTogglesUserContorl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Primitives.ToggleButton WrapToNumberButton;
        
        #line default
        #line hidden
        
        
        #line 84 "..\..\..\..\..\Windows\ParagraphNumbering\UserControls\ParagraphStyleTogglesUserContorl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Primitives.ToggleButton WrapToMarginButton;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/InfowareVSTO;component/windows/paragraphnumbering/usercontrols/paragraphstyletog" +
                    "glesusercontorl.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\..\Windows\ParagraphNumbering\UserControls\ParagraphStyleTogglesUserContorl.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.HangButton = ((System.Windows.Controls.Primitives.ToggleButton)(target));
            
            #line 64 "..\..\..\..\..\Windows\ParagraphNumbering\UserControls\ParagraphStyleTogglesUserContorl.xaml"
            this.HangButton.Click += new System.Windows.RoutedEventHandler(this.HangButton_Click);
            
            #line default
            #line hidden
            return;
            case 2:
            this.WrapToNumberButton = ((System.Windows.Controls.Primitives.ToggleButton)(target));
            
            #line 74 "..\..\..\..\..\Windows\ParagraphNumbering\UserControls\ParagraphStyleTogglesUserContorl.xaml"
            this.WrapToNumberButton.Click += new System.Windows.RoutedEventHandler(this.WrapToNumberButton_Click);
            
            #line default
            #line hidden
            return;
            case 3:
            this.WrapToMarginButton = ((System.Windows.Controls.Primitives.ToggleButton)(target));
            
            #line 84 "..\..\..\..\..\Windows\ParagraphNumbering\UserControls\ParagraphStyleTogglesUserContorl.xaml"
            this.WrapToMarginButton.Click += new System.Windows.RoutedEventHandler(this.WrapToMarginButton_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

