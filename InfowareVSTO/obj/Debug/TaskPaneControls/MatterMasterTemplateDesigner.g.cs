﻿#pragma checksum "..\..\..\TaskPaneControls\MatterMasterTemplateDesigner.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "EEE2AF24ADD38C0C954F530AEC8F8E40C4B228F211088CB79C22E5089BAFBD54"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using InfowareVSTO.Common.Language;
using InfowareVSTO.Language;
using InfowareVSTO.TaskPaneControls;
using InfowareVSTO.Util;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace InfowareVSTO.TaskPaneControls {
    
    
    /// <summary>
    /// MatterMasterTemplateDesigner
    /// </summary>
    public partial class MatterMasterTemplateDesigner : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 26 "..\..\..\TaskPaneControls\MatterMasterTemplateDesigner.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ScrollViewer MainScrollViewer;
        
        #line default
        #line hidden
        
        
        #line 27 "..\..\..\TaskPaneControls\MatterMasterTemplateDesigner.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel MainStackPanel;
        
        #line default
        #line hidden
        
        
        #line 44 "..\..\..\TaskPaneControls\MatterMasterTemplateDesigner.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox MatterMasterFields;
        
        #line default
        #line hidden
        
        
        #line 45 "..\..\..\TaskPaneControls\MatterMasterTemplateDesigner.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel MatterMasterBoolean;
        
        #line default
        #line hidden
        
        
        #line 46 "..\..\..\TaskPaneControls\MatterMasterTemplateDesigner.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal InfowareVSTO.Util.PlaceholderTextBox MatterMasterFalse;
        
        #line default
        #line hidden
        
        
        #line 54 "..\..\..\TaskPaneControls\MatterMasterTemplateDesigner.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal InfowareVSTO.Util.PlaceholderTextBox MatterMasterTrue;
        
        #line default
        #line hidden
        
        
        #line 64 "..\..\..\TaskPaneControls\MatterMasterTemplateDesigner.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button InsertMatterMasterField;
        
        #line default
        #line hidden
        
        
        #line 80 "..\..\..\TaskPaneControls\MatterMasterTemplateDesigner.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox Role;
        
        #line default
        #line hidden
        
        
        #line 81 "..\..\..\TaskPaneControls\MatterMasterTemplateDesigner.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox EntityFields;
        
        #line default
        #line hidden
        
        
        #line 82 "..\..\..\TaskPaneControls\MatterMasterTemplateDesigner.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel EntityBoolean;
        
        #line default
        #line hidden
        
        
        #line 83 "..\..\..\TaskPaneControls\MatterMasterTemplateDesigner.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal InfowareVSTO.Util.PlaceholderTextBox EntityFalse;
        
        #line default
        #line hidden
        
        
        #line 91 "..\..\..\TaskPaneControls\MatterMasterTemplateDesigner.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal InfowareVSTO.Util.PlaceholderTextBox EntityTrue;
        
        #line default
        #line hidden
        
        
        #line 101 "..\..\..\TaskPaneControls\MatterMasterTemplateDesigner.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button InsertEntityField;
        
        #line default
        #line hidden
        
        
        #line 117 "..\..\..\TaskPaneControls\MatterMasterTemplateDesigner.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox CustomFieldsGroups;
        
        #line default
        #line hidden
        
        
        #line 118 "..\..\..\TaskPaneControls\MatterMasterTemplateDesigner.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox CustomFields;
        
        #line default
        #line hidden
        
        
        #line 119 "..\..\..\TaskPaneControls\MatterMasterTemplateDesigner.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button InsertCustomField;
        
        #line default
        #line hidden
        
        
        #line 135 "..\..\..\TaskPaneControls\MatterMasterTemplateDesigner.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox Swithces;
        
        #line default
        #line hidden
        
        
        #line 144 "..\..\..\TaskPaneControls\MatterMasterTemplateDesigner.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox ERBs;
        
        #line default
        #line hidden
        
        
        #line 145 "..\..\..\TaskPaneControls\MatterMasterTemplateDesigner.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock BookmarkInserted;
        
        #line default
        #line hidden
        
        
        #line 153 "..\..\..\TaskPaneControls\MatterMasterTemplateDesigner.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button InsertERB;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/InfowareVSTO;component/taskpanecontrols/mattermastertemplatedesigner.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\TaskPaneControls\MatterMasterTemplateDesigner.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal System.Delegate _CreateDelegate(System.Type delegateType, string handler) {
            return System.Delegate.CreateDelegate(delegateType, this, handler);
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.MainScrollViewer = ((System.Windows.Controls.ScrollViewer)(target));
            return;
            case 2:
            this.MainStackPanel = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 3:
            this.MatterMasterFields = ((System.Windows.Controls.ComboBox)(target));
            
            #line 44 "..\..\..\TaskPaneControls\MatterMasterTemplateDesigner.xaml"
            this.MatterMasterFields.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.MatterMasterFields_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 4:
            this.MatterMasterBoolean = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 5:
            this.MatterMasterFalse = ((InfowareVSTO.Util.PlaceholderTextBox)(target));
            return;
            case 6:
            this.MatterMasterTrue = ((InfowareVSTO.Util.PlaceholderTextBox)(target));
            return;
            case 7:
            this.InsertMatterMasterField = ((System.Windows.Controls.Button)(target));
            
            #line 64 "..\..\..\TaskPaneControls\MatterMasterTemplateDesigner.xaml"
            this.InsertMatterMasterField.Click += new System.Windows.RoutedEventHandler(this.InsertMatterMasterField_Click);
            
            #line default
            #line hidden
            return;
            case 8:
            this.Role = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 9:
            this.EntityFields = ((System.Windows.Controls.ComboBox)(target));
            
            #line 81 "..\..\..\TaskPaneControls\MatterMasterTemplateDesigner.xaml"
            this.EntityFields.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.EntityFields_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 10:
            this.EntityBoolean = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 11:
            this.EntityFalse = ((InfowareVSTO.Util.PlaceholderTextBox)(target));
            return;
            case 12:
            this.EntityTrue = ((InfowareVSTO.Util.PlaceholderTextBox)(target));
            return;
            case 13:
            this.InsertEntityField = ((System.Windows.Controls.Button)(target));
            
            #line 101 "..\..\..\TaskPaneControls\MatterMasterTemplateDesigner.xaml"
            this.InsertEntityField.Click += new System.Windows.RoutedEventHandler(this.InsertEntityField_Click);
            
            #line default
            #line hidden
            return;
            case 14:
            this.CustomFieldsGroups = ((System.Windows.Controls.ComboBox)(target));
            
            #line 117 "..\..\..\TaskPaneControls\MatterMasterTemplateDesigner.xaml"
            this.CustomFieldsGroups.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.CustomFieldsGroups_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 15:
            this.CustomFields = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 16:
            this.InsertCustomField = ((System.Windows.Controls.Button)(target));
            
            #line 119 "..\..\..\TaskPaneControls\MatterMasterTemplateDesigner.xaml"
            this.InsertCustomField.Click += new System.Windows.RoutedEventHandler(this.InsertCustomField_Click);
            
            #line default
            #line hidden
            return;
            case 17:
            this.Swithces = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 18:
            this.ERBs = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 19:
            this.BookmarkInserted = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 20:
            this.InsertERB = ((System.Windows.Controls.Button)(target));
            
            #line 153 "..\..\..\TaskPaneControls\MatterMasterTemplateDesigner.xaml"
            this.InsertERB.Click += new System.Windows.RoutedEventHandler(this.InsertERB_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

