﻿#pragma checksum "..\..\..\VSTOAuthor\AuthorInfo.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "92481CB0D13B6149979793ACAE52D321AC7CE90E2D5D63EBCC0E1238CA901C5A"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using InfowareVSTO;
using InfowareVSTO.VSTOAuthor;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace InfowareVSTO.VSTOAuthor {
    
    
    /// <summary>
    /// AuthorInfo
    /// </summary>
    public partial class AuthorInfo : InfowareVSTO.InfowareWindow, System.Windows.Markup.IComponentConnector {
        
        
        #line 33 "..\..\..\VSTOAuthor\AuthorInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox AuthorsList;
        
        #line default
        #line hidden
        
        
        #line 145 "..\..\..\VSTOAuthor\AuthorInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Select;
        
        #line default
        #line hidden
        
        
        #line 146 "..\..\..\VSTOAuthor\AuthorInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Edit;
        
        #line default
        #line hidden
        
        
        #line 147 "..\..\..\VSTOAuthor\AuthorInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button New;
        
        #line default
        #line hidden
        
        
        #line 149 "..\..\..\VSTOAuthor\AuthorInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock YouHaveSelectedAuthor;
        
        #line default
        #line hidden
        
        
        #line 151 "..\..\..\VSTOAuthor\AuthorInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock selectedAuthorName;
        
        #line default
        #line hidden
        
        
        #line 155 "..\..\..\VSTOAuthor\AuthorInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock CurrentAuthorIs;
        
        #line default
        #line hidden
        
        
        #line 157 "..\..\..\VSTOAuthor\AuthorInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock currentAuthorName;
        
        #line default
        #line hidden
        
        
        #line 160 "..\..\..\VSTOAuthor\AuthorInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid RightSide;
        
        #line default
        #line hidden
        
        
        #line 186 "..\..\..\VSTOAuthor\AuthorInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock LabelSave;
        
        #line default
        #line hidden
        
        
        #line 187 "..\..\..\VSTOAuthor\AuthorInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button SaveBtn;
        
        #line default
        #line hidden
        
        
        #line 194 "..\..\..\VSTOAuthor\AuthorInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox Prefix;
        
        #line default
        #line hidden
        
        
        #line 197 "..\..\..\VSTOAuthor\AuthorInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox FirstName;
        
        #line default
        #line hidden
        
        
        #line 199 "..\..\..\VSTOAuthor\AuthorInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox MiddleName;
        
        #line default
        #line hidden
        
        
        #line 202 "..\..\..\VSTOAuthor\AuthorInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox LastName;
        
        #line default
        #line hidden
        
        
        #line 204 "..\..\..\VSTOAuthor\AuthorInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Initials;
        
        #line default
        #line hidden
        
        
        #line 207 "..\..\..\VSTOAuthor\AuthorInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox Sufix;
        
        #line default
        #line hidden
        
        
        #line 209 "..\..\..\VSTOAuthor\AuthorInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox JobTitle;
        
        #line default
        #line hidden
        
        
        #line 210 "..\..\..\VSTOAuthor\AuthorInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox ComboJobTitles;
        
        #line default
        #line hidden
        
        
        #line 211 "..\..\..\VSTOAuthor\AuthorInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox EditId;
        
        #line default
        #line hidden
        
        
        #line 212 "..\..\..\VSTOAuthor\AuthorInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox AddressID;
        
        #line default
        #line hidden
        
        
        #line 213 "..\..\..\VSTOAuthor\AuthorInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox PhoneID;
        
        #line default
        #line hidden
        
        
        #line 214 "..\..\..\VSTOAuthor\AuthorInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox MobileCellID;
        
        #line default
        #line hidden
        
        
        #line 215 "..\..\..\VSTOAuthor\AuthorInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox EmailID;
        
        #line default
        #line hidden
        
        
        #line 216 "..\..\..\VSTOAuthor\AuthorInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox FaxID;
        
        #line default
        #line hidden
        
        
        #line 220 "..\..\..\VSTOAuthor\AuthorInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox EmployeeID;
        
        #line default
        #line hidden
        
        
        #line 222 "..\..\..\VSTOAuthor\AuthorInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Lawyer;
        
        #line default
        #line hidden
        
        
        #line 225 "..\..\..\VSTOAuthor\AuthorInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Credentials;
        
        #line default
        #line hidden
        
        
        #line 227 "..\..\..\VSTOAuthor\AuthorInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Certification;
        
        #line default
        #line hidden
        
        
        #line 230 "..\..\..\VSTOAuthor\AuthorInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Address1;
        
        #line default
        #line hidden
        
        
        #line 232 "..\..\..\VSTOAuthor\AuthorInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Address2;
        
        #line default
        #line hidden
        
        
        #line 235 "..\..\..\VSTOAuthor\AuthorInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox City;
        
        #line default
        #line hidden
        
        
        #line 237 "..\..\..\VSTOAuthor\AuthorInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox ProvState;
        
        #line default
        #line hidden
        
        
        #line 240 "..\..\..\VSTOAuthor\AuthorInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Postal_ZipCode;
        
        #line default
        #line hidden
        
        
        #line 242 "..\..\..\VSTOAuthor\AuthorInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Country;
        
        #line default
        #line hidden
        
        
        #line 245 "..\..\..\VSTOAuthor\AuthorInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Phone;
        
        #line default
        #line hidden
        
        
        #line 247 "..\..\..\VSTOAuthor\AuthorInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox MobileCell;
        
        #line default
        #line hidden
        
        
        #line 250 "..\..\..\VSTOAuthor\AuthorInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Email;
        
        #line default
        #line hidden
        
        
        #line 252 "..\..\..\VSTOAuthor\AuthorInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Fax;
        
        #line default
        #line hidden
        
        
        #line 258 "..\..\..\VSTOAuthor\AuthorInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button OK;
        
        #line default
        #line hidden
        
        
        #line 259 "..\..\..\VSTOAuthor\AuthorInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Cancel;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/InfowareVSTO;component/vstoauthor/authorinfo.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\VSTOAuthor\AuthorInfo.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal System.Delegate _CreateDelegate(System.Type delegateType, string handler) {
            return System.Delegate.CreateDelegate(delegateType, this, handler);
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.AuthorsList = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 2:
            this.Select = ((System.Windows.Controls.Button)(target));
            
            #line 145 "..\..\..\VSTOAuthor\AuthorInfo.xaml"
            this.Select.Click += new System.Windows.RoutedEventHandler(this.Select_Click);
            
            #line default
            #line hidden
            return;
            case 3:
            this.Edit = ((System.Windows.Controls.Button)(target));
            
            #line 146 "..\..\..\VSTOAuthor\AuthorInfo.xaml"
            this.Edit.Click += new System.Windows.RoutedEventHandler(this.Edit_Click);
            
            #line default
            #line hidden
            return;
            case 4:
            this.New = ((System.Windows.Controls.Button)(target));
            
            #line 147 "..\..\..\VSTOAuthor\AuthorInfo.xaml"
            this.New.Click += new System.Windows.RoutedEventHandler(this.New_Click);
            
            #line default
            #line hidden
            return;
            case 5:
            this.YouHaveSelectedAuthor = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 6:
            this.selectedAuthorName = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 7:
            this.CurrentAuthorIs = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 8:
            this.currentAuthorName = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 9:
            this.RightSide = ((System.Windows.Controls.Grid)(target));
            return;
            case 10:
            this.LabelSave = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 11:
            this.SaveBtn = ((System.Windows.Controls.Button)(target));
            
            #line 187 "..\..\..\VSTOAuthor\AuthorInfo.xaml"
            this.SaveBtn.Click += new System.Windows.RoutedEventHandler(this.SaveBtn_Click);
            
            #line default
            #line hidden
            return;
            case 12:
            this.Prefix = ((System.Windows.Controls.ComboBox)(target));
            
            #line 194 "..\..\..\VSTOAuthor\AuthorInfo.xaml"
            this.Prefix.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 13:
            this.FirstName = ((System.Windows.Controls.TextBox)(target));
            
            #line 197 "..\..\..\VSTOAuthor\AuthorInfo.xaml"
            this.FirstName.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.TextChanged);
            
            #line default
            #line hidden
            return;
            case 14:
            this.MiddleName = ((System.Windows.Controls.TextBox)(target));
            
            #line 199 "..\..\..\VSTOAuthor\AuthorInfo.xaml"
            this.MiddleName.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.TextChanged);
            
            #line default
            #line hidden
            return;
            case 15:
            this.LastName = ((System.Windows.Controls.TextBox)(target));
            
            #line 202 "..\..\..\VSTOAuthor\AuthorInfo.xaml"
            this.LastName.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.TextChanged);
            
            #line default
            #line hidden
            return;
            case 16:
            this.Initials = ((System.Windows.Controls.TextBox)(target));
            
            #line 204 "..\..\..\VSTOAuthor\AuthorInfo.xaml"
            this.Initials.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.TextChanged);
            
            #line default
            #line hidden
            return;
            case 17:
            this.Sufix = ((System.Windows.Controls.ComboBox)(target));
            
            #line 207 "..\..\..\VSTOAuthor\AuthorInfo.xaml"
            this.Sufix.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 18:
            this.JobTitle = ((System.Windows.Controls.TextBox)(target));
            
            #line 209 "..\..\..\VSTOAuthor\AuthorInfo.xaml"
            this.JobTitle.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.TextChanged);
            
            #line default
            #line hidden
            return;
            case 19:
            this.ComboJobTitles = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 20:
            this.EditId = ((System.Windows.Controls.TextBox)(target));
            return;
            case 21:
            this.AddressID = ((System.Windows.Controls.TextBox)(target));
            return;
            case 22:
            this.PhoneID = ((System.Windows.Controls.TextBox)(target));
            return;
            case 23:
            this.MobileCellID = ((System.Windows.Controls.TextBox)(target));
            return;
            case 24:
            this.EmailID = ((System.Windows.Controls.TextBox)(target));
            return;
            case 25:
            this.FaxID = ((System.Windows.Controls.TextBox)(target));
            return;
            case 26:
            this.EmployeeID = ((System.Windows.Controls.TextBox)(target));
            
            #line 220 "..\..\..\VSTOAuthor\AuthorInfo.xaml"
            this.EmployeeID.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.TextChanged);
            
            #line default
            #line hidden
            return;
            case 27:
            this.Lawyer = ((System.Windows.Controls.TextBox)(target));
            
            #line 222 "..\..\..\VSTOAuthor\AuthorInfo.xaml"
            this.Lawyer.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.TextChanged);
            
            #line default
            #line hidden
            return;
            case 28:
            this.Credentials = ((System.Windows.Controls.TextBox)(target));
            
            #line 225 "..\..\..\VSTOAuthor\AuthorInfo.xaml"
            this.Credentials.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.TextChanged);
            
            #line default
            #line hidden
            return;
            case 29:
            this.Certification = ((System.Windows.Controls.TextBox)(target));
            
            #line 227 "..\..\..\VSTOAuthor\AuthorInfo.xaml"
            this.Certification.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.TextChanged);
            
            #line default
            #line hidden
            return;
            case 30:
            this.Address1 = ((System.Windows.Controls.TextBox)(target));
            
            #line 230 "..\..\..\VSTOAuthor\AuthorInfo.xaml"
            this.Address1.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.TextChanged);
            
            #line default
            #line hidden
            return;
            case 31:
            this.Address2 = ((System.Windows.Controls.TextBox)(target));
            
            #line 232 "..\..\..\VSTOAuthor\AuthorInfo.xaml"
            this.Address2.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.TextChanged);
            
            #line default
            #line hidden
            return;
            case 32:
            this.City = ((System.Windows.Controls.TextBox)(target));
            
            #line 235 "..\..\..\VSTOAuthor\AuthorInfo.xaml"
            this.City.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.TextChanged);
            
            #line default
            #line hidden
            return;
            case 33:
            this.ProvState = ((System.Windows.Controls.TextBox)(target));
            
            #line 237 "..\..\..\VSTOAuthor\AuthorInfo.xaml"
            this.ProvState.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.TextChanged);
            
            #line default
            #line hidden
            return;
            case 34:
            this.Postal_ZipCode = ((System.Windows.Controls.TextBox)(target));
            
            #line 240 "..\..\..\VSTOAuthor\AuthorInfo.xaml"
            this.Postal_ZipCode.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.TextChanged);
            
            #line default
            #line hidden
            return;
            case 35:
            this.Country = ((System.Windows.Controls.TextBox)(target));
            
            #line 242 "..\..\..\VSTOAuthor\AuthorInfo.xaml"
            this.Country.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.TextChanged);
            
            #line default
            #line hidden
            return;
            case 36:
            this.Phone = ((System.Windows.Controls.TextBox)(target));
            
            #line 245 "..\..\..\VSTOAuthor\AuthorInfo.xaml"
            this.Phone.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.TextChanged);
            
            #line default
            #line hidden
            return;
            case 37:
            this.MobileCell = ((System.Windows.Controls.TextBox)(target));
            
            #line 247 "..\..\..\VSTOAuthor\AuthorInfo.xaml"
            this.MobileCell.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.TextChanged);
            
            #line default
            #line hidden
            return;
            case 38:
            this.Email = ((System.Windows.Controls.TextBox)(target));
            
            #line 250 "..\..\..\VSTOAuthor\AuthorInfo.xaml"
            this.Email.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.TextChanged);
            
            #line default
            #line hidden
            return;
            case 39:
            this.Fax = ((System.Windows.Controls.TextBox)(target));
            
            #line 252 "..\..\..\VSTOAuthor\AuthorInfo.xaml"
            this.Fax.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.TextChanged);
            
            #line default
            #line hidden
            return;
            case 40:
            this.OK = ((System.Windows.Controls.Button)(target));
            
            #line 258 "..\..\..\VSTOAuthor\AuthorInfo.xaml"
            this.OK.Click += new System.Windows.RoutedEventHandler(this.OK_Click);
            
            #line default
            #line hidden
            return;
            case 41:
            this.Cancel = ((System.Windows.Controls.Button)(target));
            
            #line 259 "..\..\..\VSTOAuthor\AuthorInfo.xaml"
            this.Cancel.Click += new System.Windows.RoutedEventHandler(this.Cancel_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

