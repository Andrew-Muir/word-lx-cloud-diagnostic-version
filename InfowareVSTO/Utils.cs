﻿using InfowareVSTO.Addenda;
using InfowareVSTO.Addenda.Model;
using InfowareVSTO.Common;
using InfowareVSTO.Common.DTOs;
using InfowareVSTO.Windows.ParagraphNumbering;
using InfowareVSTO.Common.DTOs;
using InfowareVSTO.Common.General;
using InfowareVSTO.TaskPaneControls.WordDa;
using InfowareVSTO.Util;
using InfowareVSTO.Windows;
using Microsoft.Office.Core;
using Microsoft.Office.Interop.Word;
using Microsoft.Office.Tools.Word;
using Newtonsoft.Json.Linq;
using Microsoft.Office.Interop.Word;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using Word = Microsoft.Office.Interop.Word;
using InfowareVSTO.Common.Models;
using System.Windows.Controls;
using Bookmark = Microsoft.Office.Interop.Word.Bookmark;
using ContentControl = Microsoft.Office.Interop.Word.ContentControl;
using InfowareVSTO.VSTOAuthor;
using InfowareVSTO.MultiLanguage;
using InfowareVSTO.Common.Language;
using System.Globalization;
using System.Threading;
using DocumentFormat.OpenXml.Math;
using InfowareVSTO.Windows.WordDa.Library;
using Microsoft.VisualBasic;
using DocumentFormat.OpenXml.Bibliography;

namespace InfowareVSTO
{
    public class Utils
    {
        public const string STATIC_PROCESSOR_ID = "7777777777";

        private const string TEMPLATE_TYPE_PROP = "LX-TEMPLATE-TYPE";
        public delegate void TaskBeforeOpen();
        private const string INFOWARE = "Infoware";

        public static Word.WdPaperTray GetPaperTray(string printerName, string binName)
        {
            PrinterSettings ps = new PrinterSettings();
            ps.PrinterName = printerName;
            if (ps.IsValid)
            {
                var paperSources = ps.PaperSources;
                for (int i = 0; i < paperSources.Count; i++)
                {
                    if (paperSources[i].SourceName == binName)
                        return (Word.WdPaperTray)paperSources[i].RawKind;
                }
            }

            return Word.WdPaperTray.wdPrinterDefaultBin;
        }

        public static string GetDocumentVariable(Word.Document oDoc, string varName)
        {
            foreach (Word.Variable v in oDoc.Variables)
            {
                if (v.Name == varName)
                {
                    return v.Value;
                }
            }
            return null;
        }

        public static bool SetDocumentVariable(Word.Document oDoc, string varName, string varValue)
        {
            try
            {
                var foundVariableIndex = GetDocumentVariableIndex(oDoc, varName);

                if (foundVariableIndex == -1)
                {
                    oDoc.Variables.Add(varName, varValue);
                }
                else
                {
                    oDoc.Variables[foundVariableIndex].Value = varValue;
                }

                return true;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
                return false;
            }
        }

        public static int GetDocumentVariableIndex(Word.Document oDoc, string varName)
        {
            if (oDoc != null)
            {
                foreach (Variable v in oDoc.Variables)
                {
                    if (v.Name == varName)
                    {
                        return v.Index;
                    }
                }
            }

            return -1;
        }

        public static void RemoveDocumentVariable(Word.Document oDoc, string varName)
        {
            foreach (Word.Variable v in oDoc.Variables)
            {
                if (v.Name == varName)
                {
                    v.Delete();
                    return;
                }
            }
        }

        public static void SetPrinterAndBin(Word.Document document, string printerName, string printerBinName, string secondPlusPrinterBinName = null)
        {
            if (document != null && !string.IsNullOrEmpty(printerName) && !string.IsNullOrEmpty(printerBinName))
            {
                var firstPageTray = GetPaperTray(printerName, printerBinName);

                var otherPagesTray = firstPageTray;
                if (!string.IsNullOrEmpty(secondPlusPrinterBinName))
                {
                    otherPagesTray = GetPaperTray(printerName, secondPlusPrinterBinName);
                }

                document.Application.ActivePrinter = printerName;

                foreach (Word.Section section in document.Sections)
                {
                    try
                    {
                        section.PageSetup.FirstPageTray = firstPageTray;
                        section.PageSetup.OtherPagesTray = otherPagesTray;
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex);
                    }
                }
            }
        }

        public static ContentControl GetContainingCCWithTag(Word.Document document, string tag, Range range)
        {
            var contentControls = GetAllContentControls(document, tag);

            foreach (ContentControl cc in contentControls)
            {
                if (cc.Range.Start <= range.Start && cc.Range.End >= range.End)
                {
                    return cc;
                }
            }

            return null;
        }

        public static List<Word.ContentControl> GetAllContentControls(Word.Document oDoc, string onlyWithTagName = null)
        {
            List<Word.ContentControl> ccList = new List<Word.ContentControl>();
            // The code below search content controls in all
            // word document stories see http://word.mvps.org/faqs/customization/ReplaceAnywhere.htm
            Word.Range rangeStory;
            foreach (Word.Range range1 in oDoc.StoryRanges)
            {
                rangeStory = range1;
                do
                {
                    try
                    {
                        foreach (Word.ContentControl cc in rangeStory.ContentControls)
                        {
                            ccList.Add(cc);
                        }
                        foreach (Word.Shape shapeRange in rangeStory.ShapeRange)
                        {
                            foreach (Word.ContentControl cc in shapeRange.TextFrame.TextRange.ContentControls)
                            {
                                ccList.Add(cc);
                            }
                        }
                    }
                    catch (COMException) { }
                    rangeStory = rangeStory.NextStoryRange;

                }
                while (rangeStory != null);
            }

            if (!string.IsNullOrEmpty(onlyWithTagName))
                ccList = ccList.Where(x => x.Tag == onlyWithTagName).ToList();

            return ccList;
        }

        public static List<Word.Shape> GetBodyShapes(Word.Document oDoc)
        {
            List<Word.Shape> shapeList = new List<Word.Shape>();
            Word.Range rangeStory;
            foreach (Word.Range range1 in oDoc.StoryRanges)
            {
                rangeStory = range1;
                do
                {
                    try
                    {
                        foreach (Word.Shape shapeRange in rangeStory.ShapeRange)
                        {
                            shapeList.Add(shapeRange);
                        }
                    }
                    catch (COMException) { }
                    rangeStory = rangeStory.NextStoryRange;

                }
                while (rangeStory != null);
            }

            return shapeList;
        }

        public static Range TrimRange(Range range2)
        {
            Range range3 = range2.Duplicate;
            Range range4;
            bool cont = true;
            while (cont && range3.Text?.Length > 0)
            {
                range4 = range3.Duplicate;
                range4.Collapse(WdCollapseDirection.wdCollapseStart);
                range4.MoveEnd(WdUnits.wdCharacter, 1);

                cont = string.IsNullOrWhiteSpace(range4.Text);
                if (cont)
                {
                    int start = range3.Start;
                    range3.MoveStart(WdUnits.wdCharacter, 1);
                    if (range3.Start == start)
                    {
                        cont = false;
                    }
                }
            }
            cont = true;
            while (cont && range3.Text?.Length > 0)
            {
                range4 = range3.Duplicate;
                range4.Collapse(WdCollapseDirection.wdCollapseEnd);
                range4.MoveStart(WdUnits.wdCharacter, -1);

                cont = string.IsNullOrWhiteSpace(range4.Text);
                if (cont)
                {
                    range3.MoveEnd(WdUnits.wdCharacter, -1);
                }
            }
            return range3;
        }

        internal static bool ValidateField(string functionName, string value)
        {
            try
            {
                bool? result = ThisAddIn.Instance.Application.Run(functionName, value) as bool?;
                return result != false;
            }
            catch (Exception ex)
            {
            }
            return true;
        }

        public static void CreateDocument(Word.Document oDoc, Word.Application oWord)
        {
            object oMissing = System.Reflection.Missing.Value;
            object oEndOfDoc = "\\endofdoc"; /* \endofdoc is a predefined bookmark */

            //Start Word and create a new document.
            //Word._Application oWord;
            //Word._Document oDoc;
            //oWord = new Word.Application();
            //oWord.Visible = true;
            //oDoc = oWord.Documents.Add(ref oMissing, ref oMissing,
            //ref oMissing, ref oMissing);

            //Insert a paragraph at the beginning of the document.
            Word.Paragraph oPara1;
            oPara1 = oDoc.Content.Paragraphs.Add(ref oMissing);
            oPara1.Range.Text = "Heading 1";
            oPara1.Range.Font.Bold = 1;
            oPara1.Format.SpaceAfter = 24;    //24 pt spacing after paragraph.
            oPara1.Range.InsertParagraphAfter();

            //Insert a paragraph at the end of the document.
            Word.Paragraph oPara2;
            object oRng = oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range;
            oPara2 = oDoc.Content.Paragraphs.Add(ref oRng);
            oPara2.Range.Text = "Heading 2";
            oPara2.Format.SpaceAfter = 6;
            oPara2.Range.InsertParagraphAfter();

            //Insert another paragraph.
            Word.Paragraph oPara3;
            oRng = oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range;
            oPara3 = oDoc.Content.Paragraphs.Add(ref oRng);
            oPara3.Range.Text = "This is a sentence of normal text. Now here is a table:";
            oPara3.Range.Font.Bold = 0;
            oPara3.Format.SpaceAfter = 24;
            oPara3.Range.InsertParagraphAfter();

            //Insert a 3 x 5 table, fill it with data, and make the first row
            //bold and italic.
            Word.Table oTable;
            Word.Range wrdRng = oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range;
            oTable = oDoc.Tables.Add(wrdRng, 3, 5, ref oMissing, ref oMissing);
            oTable.Range.ParagraphFormat.SpaceAfter = 6;
            int r, c;
            string strText;
            for (r = 1; r <= 3; r++)
                for (c = 1; c <= 5; c++)
                {
                    strText = "r" + r + "c" + c;
                    oTable.Cell(r, c).Range.Text = strText;
                }
            oTable.Rows[1].Range.Font.Bold = 1;
            oTable.Rows[1].Range.Font.Italic = 1;

            //Add some text after the table.
            Word.Paragraph oPara4;
            oRng = oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range;
            oPara4 = oDoc.Content.Paragraphs.Add(ref oRng);
            oPara4.Range.InsertParagraphBefore();
            oPara4.Range.Text = "And here's another table:";
            oPara4.Format.SpaceAfter = 24;
            oPara4.Range.InsertParagraphAfter();

            //Insert a 5 x 2 table, fill it with data, and change the column widths.
            wrdRng = oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range;
            oTable = oDoc.Tables.Add(wrdRng, 5, 2, ref oMissing, ref oMissing);
            oTable.Range.ParagraphFormat.SpaceAfter = 6;
            for (r = 1; r <= 5; r++)
                for (c = 1; c <= 2; c++)
                {
                    strText = "r" + r + "c" + c;
                    oTable.Cell(r, c).Range.Text = strText;
                }
            oTable.Columns[1].Width = oWord.InchesToPoints(2); //Change width of columns 1 & 2
            oTable.Columns[2].Width = oWord.InchesToPoints(3);

            //Keep inserting text. When you get to 7 inches from top of the
            //document, insert a hard page break.
            object oPos;
            double dPos = oWord.InchesToPoints(7);
            oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range.InsertParagraphAfter();
            do
            {
                wrdRng = oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range;
                wrdRng.ParagraphFormat.SpaceAfter = 6;
                wrdRng.InsertAfter("A line of text");
                wrdRng.InsertParagraphAfter();
                oPos = wrdRng.get_Information
                                       (Word.WdInformation.wdVerticalPositionRelativeToPage);
            }
            while (dPos >= Convert.ToDouble(oPos));
            object oCollapseEnd = Word.WdCollapseDirection.wdCollapseEnd;
            object oPageBreak = Word.WdBreakType.wdPageBreak;
            wrdRng.Collapse(ref oCollapseEnd);
            wrdRng.InsertBreak(ref oPageBreak);
            wrdRng.Collapse(ref oCollapseEnd);
            wrdRng.InsertAfter("We're now on page 2. Here's my chart:");
            wrdRng.InsertParagraphAfter();

            //Insert a chart.
            Word.InlineShape oShape;
            object oClassType = "MSGraph.Chart.8";
            wrdRng = oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range;
            oShape = wrdRng.InlineShapes.AddOLEObject(ref oClassType, ref oMissing,
            ref oMissing, ref oMissing, ref oMissing,
            ref oMissing, ref oMissing, ref oMissing);

            //Demonstrate use of late bound oChart and oChartApp objects to
            //manipulate the chart object with MSGraph.
            object oChart;
            object oChartApp;
            oChart = oShape.OLEFormat.Object;
            oChartApp = oChart.GetType().InvokeMember("Application",
            BindingFlags.GetProperty, null, oChart, null);

            //Change the chart type to Line.
            object[] Parameters = new Object[1];
            Parameters[0] = 4; //xlLine = 4
            oChart.GetType().InvokeMember("ChartType", BindingFlags.SetProperty,
            null, oChart, Parameters);

            //Update the chart image and quit MSGraph.
            oChartApp.GetType().InvokeMember("Update",
            BindingFlags.InvokeMethod, null, oChartApp, null);
            oChartApp.GetType().InvokeMember("Quit",
            BindingFlags.InvokeMethod, null, oChartApp, null);
            //... If desired, you can proceed from here using the Microsoft Graph 
            //Object model on the oChart and oChartApp objects to make additional
            //changes to the chart.

            //Set the width of the chart.
            oShape.Width = oWord.InchesToPoints(6.25f);
            oShape.Height = oWord.InchesToPoints(3.57f);

            //Add text after the chart.
            wrdRng = oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range;
            wrdRng.InsertParagraphAfter();
            wrdRng.InsertAfter("THE END.");

        }

        public static string GetProcessorId()
        {
            return BaseUtils.GetProcessorId();
        }

        public static void SetParagraphTextByStleName(Word.Document oDoc, string styleName, string value, int skip = 0)
        {
            int skipped = 0;
            for (int i = 1; i < oDoc.Paragraphs.Count; i++)
            {
                string curStyleName = ((Word.Style)oDoc.Paragraphs[i].get_Style()).NameLocal; //get paragraph style name
                if (styleName == curStyleName)
                {
                    if (skipped < skip)
                    {
                        skipped++;
                        continue;
                    }
                    oDoc.Paragraphs[i].Range.Text = value.Trim();
                    break;
                }
            }
        }

        public static void CreateEnvelope(Word.Document oDoc, Envelope_WPF envelope)
        {
            object oMissing = System.Reflection.Missing.Value;
            object oEndOfDoc = "\\endofdoc"; /* \endofdoc is a predefined bookmark */

            Word.Table oTable;
            Word.Range wrdRng = oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range;
            oTable = oDoc.Tables.Add(wrdRng, 1, 2, ref oMissing, ref oMissing);
            oTable.Range.ParagraphFormat.SpaceAfter = 6;

            object oRng = oTable.Cell(1, 1).Range;
            Word.Paragraph oPara1 = oDoc.Content.Paragraphs.Add(ref oRng);
            oPara1.Range.Text = envelope.Handling + "\v" + envelope.Attention;
            oPara1.Format.SpaceAfter = 6;
            oPara1.Range.Bold = 1;
            //oPara1.Range.InsertParagraphAfter();

            //Word.Paragraph oPara2 = oDoc.Content.Paragraphs.Add(ref oRng);
            //oPara2.Range.Text = "\v \v \v" + envelope.Attention;			
            //oPara2.Range.Bold = 1;
            //oPara2.Range.InsertParagraphAfter();

            object oRng2 = oTable.Cell(1, 2).Range;
            Word.Paragraph oPara3 = oDoc.Content.Paragraphs.Add(ref oRng2);
            oPara3.Range.Text = envelope.Address;
            oPara3.Format.SpaceAfter = 6;
            //oPara3.Range.InsertParagraphAfter();
        }

        //public static void CreateTestDoc(Word.Document oDoc, Word.Application oWord, Envelope_WPF envelope)
        //{
        //	object oMissing = System.Reflection.Missing.Value;
        //	object oEndOfDoc = "\\endofdoc"; /* \endofdoc is a predefined bookmark */			

        //	//Insert a paragraph at the beginning of the document.
        //	Word.Paragraph oPara1;
        //	oPara1 = oDoc.Content.Paragraphs.Add(ref oMissing);
        //	oPara1.Range.Text = envelope.Handling;
        //	oPara1.Range.Font.Bold = 1;
        //	oPara1.Format.SpaceAfter = 24;    //24 pt spacing after paragraph.
        //	oPara1.Range.InsertParagraphAfter();

        //	//Insert a paragraph at the end of the document.
        //	Word.Paragraph oPara2;
        //	object oRng = oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range;
        //	oPara2 = oDoc.Content.Paragraphs.Add(ref oRng);
        //	oPara2.Range.Text = "Heading 2";
        //	oPara2.Format.SpaceAfter = 6;
        //	oPara2.Range.InsertParagraphAfter();

        //	//Insert another paragraph.
        //	Word.Paragraph oPara3;
        //	oRng = oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range;
        //	oPara3 = oDoc.Content.Paragraphs.Add(ref oRng);
        //	oPara3.Range.Text = "This is a sentence of normal text. Now here is a table:";
        //	oPara3.Range.Font.Bold = 0;
        //	oPara3.Format.SpaceAfter = 24;
        //	oPara3.Range.InsertParagraphAfter();

        //	//Insert a 3 x 5 table, fill it with data, and make the first row
        //	//bold and italic.
        //	Word.Table oTable;
        //	Word.Range wrdRng = oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range;
        //	oTable = oDoc.Tables.Add(wrdRng, 3, 5, ref oMissing, ref oMissing);
        //	oTable.Range.ParagraphFormat.SpaceAfter = 6;
        //	int r, c;
        //	string strText;
        //	for (r = 1; r <= 3; r++)
        //		for (c = 1; c <= 5; c++)
        //		{
        //			strText = "r" + r + "c" + c;
        //			oTable.Cell(r, c).Range.Text = strText;
        //		}
        //	oTable.Rows[1].Range.Font.Bold = 1;
        //	oTable.Rows[1].Range.Font.Italic = 1;

        //	//Add some text after the table.
        //	Word.Paragraph oPara4;
        //	oRng = oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range;
        //	oPara4 = oDoc.Content.Paragraphs.Add(ref oRng);
        //	oPara4.Range.InsertParagraphBefore();
        //	oPara4.Range.Text = "And here's another table:";
        //	oPara4.Format.SpaceAfter = 24;
        //	oPara4.Range.InsertParagraphAfter();

        //	//Insert a 5 x 2 table, fill it with data, and change the column widths.
        //	wrdRng = oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range;
        //	oTable = oDoc.Tables.Add(wrdRng, 5, 2, ref oMissing, ref oMissing);
        //	oTable.Range.ParagraphFormat.SpaceAfter = 6;
        //	for (r = 1; r <= 5; r++)
        //		for (c = 1; c <= 2; c++)
        //		{
        //			strText = "r" + r + "c" + c;
        //			oTable.Cell(r, c).Range.Text = strText;
        //		}
        //	oTable.Columns[1].Width = oWord.InchesToPoints(2); //Change width of columns 1 & 2
        //	oTable.Columns[2].Width = oWord.InchesToPoints(3);

        //	//Keep inserting text. When you get to 7 inches from top of the
        //	//document, insert a hard page break.
        //	object oPos;
        //	double dPos = oWord.InchesToPoints(7);
        //	oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range.InsertParagraphAfter();
        //	do
        //	{
        //		wrdRng = oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range;
        //		wrdRng.ParagraphFormat.SpaceAfter = 6;
        //		wrdRng.InsertAfter("A line of text");
        //		wrdRng.InsertParagraphAfter();
        //		oPos = wrdRng.get_Information
        //							   (Word.WdInformation.wdVerticalPositionRelativeToPage);
        //	}
        //	while (dPos >= Convert.ToDouble(oPos));
        //	object oCollapseEnd = Word.WdCollapseDirection.wdCollapseEnd;
        //	object oPageBreak = Word.WdBreakType.wdPageBreak;
        //	wrdRng.Collapse(ref oCollapseEnd);
        //	wrdRng.InsertBreak(ref oPageBreak);
        //	wrdRng.Collapse(ref oCollapseEnd);
        //	wrdRng.InsertAfter("We're now on page 2. Here's my chart:");
        //	wrdRng.InsertParagraphAfter();

        //	//Insert a chart.
        //	Word.InlineShape oShape;
        //	object oClassType = "MSGraph.Chart.8";
        //	wrdRng = oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range;
        //	oShape = wrdRng.InlineShapes.AddOLEObject(ref oClassType, ref oMissing,
        //	ref oMissing, ref oMissing, ref oMissing,
        //	ref oMissing, ref oMissing, ref oMissing);

        //	//Demonstrate use of late bound oChart and oChartApp objects to
        //	//manipulate the chart object with MSGraph.
        //	object oChart;
        //	object oChartApp;
        //	oChart = oShape.OLEFormat.Object;
        //	oChartApp = oChart.GetType().InvokeMember("Application",
        //	BindingFlags.GetProperty, null, oChart, null);

        //	//Change the chart type to Line.
        //	object[] Parameters = new Object[1];
        //	Parameters[0] = 4; //xlLine = 4
        //	oChart.GetType().InvokeMember("ChartType", BindingFlags.SetProperty,
        //	null, oChart, Parameters);

        //	//Update the chart image and quit MSGraph.
        //	oChartApp.GetType().InvokeMember("Update",
        //	BindingFlags.InvokeMethod, null, oChartApp, null);
        //	oChartApp.GetType().InvokeMember("Quit",
        //	BindingFlags.InvokeMethod, null, oChartApp, null);
        //	//... If desired, you can proceed from here using the Microsoft Graph 
        //	//Object model on the oChart and oChartApp objects to make additional
        //	//changes to the chart.

        //	//Set the width of the chart.
        //	oShape.Width = oWord.InchesToPoints(6.25f);
        //	oShape.Height = oWord.InchesToPoints(3.57f);

        //	//Add text after the chart.
        //	wrdRng = oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range;
        //	wrdRng.InsertParagraphAfter();
        //	wrdRng.InsertAfter("THE END.");

        //}

        public static string GetDefaultDateFormat()
        {
            var dateFormat = SettingsManager.GetSetting("StandardDateFormat", "General", null, MLanguageUtil.ActiveDocumentLanguage);
            if (string.IsNullOrEmpty(dateFormat))
            {
                dateFormat = "MMMM d, yyyy";
            }
            return dateFormat;
        }

        public static void OpenTemplate(TemplateType templateType, bool closeAllDocumentsBefore = false, bool openFirst = false)
        {
            Windows.ParagraphNumbering.ParagraphNumberingToolbarManager.Instance.EnsureWindowIsHidden();


            Word.Document originalDocument = null;

            try
            {
                originalDocument = ThisAddIn.Instance.Application.ActiveDocument;
            }
            catch { }
            TemplateListModel template = null;
            TemplateDownloadModel downloadedTemplate = null;
            if (templateType == TemplateType.Custom)
            {
                TemplateFinder templateFinder = new TemplateFinder();
                templateFinder.ShowDialog();
                template = templateFinder.Result;
                if (templateFinder.Result != null)
                {
                    templateType = templateFinder.Result.Type;
                    downloadedTemplate = templateFinder.DownloadedTemplate;
                }
            }
            else
            {
                var currentAuthorSettings = ManageUsers.CurrentAuthorSettings;
                var templateList = AdminPanelWebApi.GetTemplateList(templateType, currentAuthorSettings.CompanyId, currentAuthorSettings.LanguageId, currentAuthorSettings.LocationId);

                if (templateList.Count > 0)
                {
                    if (templateList.Count == 1 || openFirst)
                    {
                        template = templateList.FirstOrDefault();
                    }
                    else
                    {
                        var templateChooser = new TemplateChooser(templateType);
                        templateChooser.PopulateTemplates(templateList);
                        templateChooser.ShowDialog();
                        if (templateChooser.ResultOk)
                        {
                            template = templateList.Where(x => x.Id == templateChooser.ResultId).FirstOrDefault();
                        }
                    }
                }
                else
                {
                    new InfowareInformationWindow(LanguageManager.GetTranslation(LanguageConstants.NoTemplatesOfThisTypeExist, "No templates of this type exist.")).ShowDialog();
                }

                if (template != null)
                {
                    try
                    {
                        downloadedTemplate = DownloadAndGetTemplate(template, true);
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex.Message);
                    }
                }
            }

            try
            {
                if (downloadedTemplate?.Document != null)
                {
                    PaneDTO paneDTO = null;
                    if (originalDocument != null && (templateType == TemplateType.Fax || templateType == TemplateType.Envelope || templateType == TemplateType.Labels) && ActiveDocumentIsLetter(originalDocument) && ConfirmUseAnswersFromLetter())
                    {
                        paneDTO = TaskPaneUtil.LoadAnswers(document: originalDocument);
                    }

                    TaskPaneUtil2.CloseTaskpanesOfDocument(downloadedTemplate.Document);

                    if (templateType != TemplateType.BlankDocument)
                    {
                        SelectBodyBookmark(downloadedTemplate.Document);
                        bool hasfixedClauses = FillFixedClauses(downloadedTemplate.Document);
                        hasfixedClauses = FillFixedClauseGroups(downloadedTemplate.Document) || hasfixedClauses;
                        InfowareVSTO.DataSource.MatterMasterPaneUtils.FillMatterMasterPlaceholders(downloadedTemplate.Document);
                        if (hasfixedClauses)
                        {
                            downloadedTemplate.Document.Fields.Update();
                        }

                        ShowTaskPane(templateType, paneDTO);
                    }

                    DocumentPropertyUtil.SaveShortProperty(TEMPLATE_TYPE_PROP, ((int)templateType).ToString(), true);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                Logger.LogException(ex);
            }

            Windows.ParagraphNumbering.ParagraphNumberingToolbarManager.Instance.RestoreWindowIfNecessary();
        }

        private static bool FillFixedClauseGroups(Word.Document document)
        {
            bool found = false;
            var contentControls = DocumentUtils.GetAllContentControls(document, CustomConstants.FIXED_CLAUSE_GROUP_TAG).ToList();
            if (contentControls.Count() > 0)
            {
                found = true;
                FixedClauseGroupHelper fch = new FixedClauseGroupHelper();
                contentControls = Utils.ReplaceInlineContentControlsWithMultiLine(contentControls, document);

                foreach (var cc in contentControls)
                {
                    cc.Range.Select();
                    fch.FillFixedClauseContentControl(cc);
                }

                fch.InsertQuestions();
            }
            return found;
        }

        private static bool FillFixedClauses(Word.Document document)
        {
            bool found = false;
            var contentControls = DocumentUtils.GetAllContentControls(document, CustomConstants.FIXED_CLAUSE_TAG).ToList();
            if (contentControls.Count() > 0)
            {
                found = true;
                FixedClauseHelper fch = new FixedClauseHelper();
                contentControls = Utils.ReplaceInlineContentControlsWithMultiLine(contentControls, document);
                foreach (var cc in contentControls)
                {
                    fch.FillFixedClauseContentControl(cc);
                }

                fch.InsertQuestions();
            }
            return found;
        }

        private static List<ContentControl> ReplaceInlineContentControlsWithMultiLine(List<ContentControl> contentControls, Word.Document document)
        {
            List<ContentControl> result = new List<ContentControl>();

            foreach(var cc in contentControls)
            {
                if (IsContentControlMultiLine(cc, document))
                {
                    result.Add(cc);
                }
                else
                {
                    Word.Paragraph paragraph = cc.Range.Paragraphs.First;

                    string title = cc.Title;
                    string tag = cc.Tag;
                    cc.Delete(false);
                    var newCC = InsertContentControl(document, paragraph.Range, title, tag);
                    if (newCC != null)
                    {
                        result.Add(newCC);
                    }
                }
            }

            return result;
        }

        public static bool IsContentControlMultiLine(ContentControl cc, Word.Document document)
        {
            if (cc != null && document != null)
            {
                try
                {
                    string xml = cc.Range.Paragraphs.First.Range.WordOpenXML;
                    return cc.Range.Paragraphs.Count > 1 || ClauseLibraryHelper.HasDocumentContentControlDirectlyInBody(xml);
                }
                catch (System.Exception ex)
                {
                    return false;
                }
            }
            return false;
        }

        private static Word.ContentControl InsertContentControl(Word.Document document, Range insertionRange, string title, string tag)
        {
            try
            {
                Word.ContentControl control = document.ContentControls.Add(WdContentControlType.wdContentControlRichText, insertionRange);
                control.Tag = tag;
                control.Title = title;
                return control;
            }
            catch (System.NotImplementedException)
            {
                return null;
            }
        }

        private static void SelectBodyBookmark(Word.Document document)
        {
            if (document != null)
            {
                foreach (Bookmark bookmark in document.Bookmarks)
                {
                    if (bookmark.Name.ToLower() == "body")
                    {
                        Range range = bookmark.Range;
                        range.Collapse(WdCollapseDirection.wdCollapseStart);
                        range.Select();
                    }
                }
            }
        }

        public static void ShowTaskPane(TemplateType templateType, PaneDTO letterDTO = null)
        {
            Microsoft.Office.Tools.CustomTaskPane taskPane = null;
            IRefreshData userControl = null;

            Range selection = ThisAddIn.Instance.Application.Selection.Range;

            switch (templateType)
            {
                case TemplateType.Letter:
                    userControl = new TemplateLetterUserControl();
                    break;
                case TemplateType.Memo:
                    userControl = new TemplateMemoUserControl();
                    break;
                case TemplateType.Envelope:
                    var envelopeTaskpaneControl = new TemplateEnvelopeUserControl();
                    userControl = envelopeTaskpaneControl;
                    if (letterDTO != null)
                    {
                        envelopeTaskpaneControl.LoadAnswers(letterDTO);
                    }
                    break;
                case TemplateType.Fax:

                    var faxTaskpaneControl = new TemplateFaxUserControl();
                    if (letterDTO != null)
                    {
                        faxTaskpaneControl.LoadAnswers(letterDTO);
                    }

                    userControl = faxTaskpaneControl;
                    break;
                case TemplateType.Labels:
                    var labelTaskpaneControl = new TemplateLabelUserControl();
                    if (letterDTO != null)
                    {
                        labelTaskpaneControl.LoadAnswers(letterDTO);
                    }
                    userControl = labelTaskpaneControl;
                    break;
                case TemplateType.Custom:
                    var dataFill = new DataFillUserControl(true);
                    //This is done for the AnyFieldFound to be determined
                    TaskPaneUtil2.HideUnusedFields(dataFill);
                    if (!dataFill.AnyFieldFound && dataFill.ControlsFound == 0)
                    {
                        if (dataFill.OfficeLocationFound)
                        {
                            dataFill.OKHandler();
                        }
                        return;
                    }

                    userControl = dataFill;
                    break;
            }

            if (userControl is UserControl)
            {
                userControl.Selection = selection;
                taskPane = TaskPaneUtil2.CreateCustomTaskPane(INFOWARE, userControl as UserControl);
                if (templateType != TemplateType.BlankDocument && userControl is IRefreshData)
                {
                    TaskPaneUtil2.HideUnusedFields(userControl as IRefreshData);
                    TaskPaneUtil2.AutoPopulateIfNeeded(userControl as IRefreshData);
                }
            }

            taskPane.Width = 375;
            taskPane.Visible = true;
        }

        private static bool ConfirmUseAnswersFromLetter()
        {
            InfowareConfirmationWindow confirmationWindow = new InfowareConfirmationWindow(LanguageManager.GetTranslation(LanguageConstants.DoYouWantToUseAnswersFromLetter, "Do you want to use answers from the letter to fill the new template?"), LanguageManager.GetTranslation(LanguageConstants.UseLetterAnswers, "Use Letter Answers?"), System.Windows.MessageBoxButton.YesNo);
            confirmationWindow.ShowDialog();
            return confirmationWindow.Result == System.Windows.MessageBoxResult.Yes;
        }

        public static bool ActiveDocumentIsLetter(Word.Document fromDocument = null)
        {
            return DocumentPropertyUtil.ReadProperty(TEMPLATE_TYPE_PROP, fromDocument) == "1";
        }

        public static Word.Document AddNewDocument(object template = null, object newTemplate = null, WdNewDocumentType documentType = WdNewDocumentType.wdNewBlankDocument, bool isVisible = true)
        {
            if (template == null)
            {
                template = Missing.Value;
            }

            if (newTemplate == null)
            {
                newTemplate = Missing.Value;
            }

            Word.Document document = new Word.Document();

            try
            {
                ((ApplicationEvents4_Event)ThisAddIn.Instance.Application).NewDocument -= ThisAddIn.Instance.OnDocumentNew;
                document = Globals.ThisAddIn.Application.Documents.Add(template, newTemplate, documentType, isVisible);
                ThisAddIn.Instance.Ribbon.Invalidate();
                ((ApplicationEvents4_Event)ThisAddIn.Instance.Application).NewDocument += ThisAddIn.Instance.OnDocumentNew;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            return document;
        }

        public static void ReplaceTcFieldsInDocument(Word.Document document)
        {
            if (document != null && document.Fields != null)
            {
                var originalTextMath = new Regex("\\[(.*?)\\]");
                var textMatch = new Regex("\\\"(.*?)\\\"");

                foreach (Word.Field field in document.Fields)
                {
                    if (field.Type == WdFieldType.wdFieldTOCEntry)
                    {
                        var match = originalTextMath.Match(field.Code.Text);
                        if (match.Success)
                        {
                            var fieldStart = field.Code.GoToPrevious(WdGoToItem.wdGoToObject);
                            if (fieldStart != null && fieldStart.Paragraphs != null)
                            {
                                var paragraphNumbering = string.Empty;
                                if (!string.IsNullOrEmpty(fieldStart.Paragraphs.Last.Range.ListFormat.ListString))
                                {
                                    var listString = fieldStart.Paragraphs.Last.Range.ListFormat.ListString;
                                    if (listString.Length == 1)
                                    {
                                        if (fieldStart.Paragraphs.Last.Range.ListFormat.ListString != "")
                                        {
                                            paragraphNumbering = listString + " ";
                                        }
                                    }
                                    else
                                    {
                                        paragraphNumbering = listString + " ";
                                    }
                                }
                                field.Code.Text = textMatch.Replace(field.Code.Text, '"' + paragraphNumbering + match.Value.TrimStart('[').TrimEnd(']') + '"', 1);
                            }
                        }
                    }
                }
            }
        }

        public static List<Word.Shape> GetPageNoTextboxes(HeaderFooter headerFooter)
        {
            List<Word.Shape> textBoxes = new List<Word.Shape>();
            if (headerFooter.Exists)
            {
                if (headerFooter.Range.ShapeRange.Count > 0)
                {
                    foreach (Word.Shape shape in headerFooter.Range.ShapeRange)
                    {
                        if (shape.TextFrame.HasText == -1)
                        {
                            bool numberFound = false;

                            foreach (Word.Field field in shape.TextFrame.TextRange.Fields)
                            {
                                if (field.Type == WdFieldType.wdFieldPage || field.Type == WdFieldType.wdFieldNumPages)
                                {
                                    numberFound = true;
                                }
                            }

                            if (numberFound)
                            {
                                textBoxes.Add(shape);
                            }
                        }
                    }
                }
            }

            return textBoxes;
        }

        public static void PopulateComboboxWithTemplateOptions(ComboBox comboBox, List<TemplateListModel> templates)
        {
            if (templates != null && templates.Any())
            {
                var comboBoxItemList = new List<ComboboxItem>();
                foreach (var template in templates)
                {
                    comboBoxItemList.Add(new ComboboxItem { Text = template.Name, Value = template.Id });
                }

                comboBox.Items.Clear();
                comboBox.ItemsSource = comboBoxItemList;
                comboBox.SelectedValuePath = "Value";
                comboBox.DisplayMemberPath = "Text";
            }
        }

        public static WdUnderline GetValidUnderlineValue(WdUnderline wdUnderline)
        {
            var availableUnderlineTypes = Enum.GetValues(typeof(WdUnderline)).Cast<WdUnderline>();
            if (availableUnderlineTypes.Contains(wdUnderline))
            {
                return wdUnderline;
            }

            return WdUnderline.wdUnderlineNone;
        }

        public static string GetLocalizedStyleNameFromBuiltInStyle(WdBuiltinStyle wdBuiltinStyle)
        {
            string localizedStyleName = string.Empty;

            try
            {
                localizedStyleName = Globals.ThisAddIn.Application.ActiveDocument.Styles[wdBuiltinStyle].NameLocal;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }

            return localizedStyleName;
        }

        public static float LimitMaxListLevelValues(float listLevelValueInDp, float limitTo)
        {
            if ((listLevelValueInDp > 0 && listLevelValueInDp >= limitTo) || (listLevelValueInDp < 0 && listLevelValueInDp <= -limitTo))
            {
                listLevelValueInDp = limitTo;
            }

            return listLevelValueInDp;
        }

        public static void ActivateApplication()
        {
            try
            {
                if (Globals.ThisAddIn?.Application != null)
                {
                    Globals.ThisAddIn.Application.Activate();
                    Globals.ThisAddIn.Application.ActiveDocument.Activate();
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
        }

        public static int GetAddressLabelComboBoxSelectedValue(PrinterGeneralSetting printerGeneralSettings, List<TemplateListModel> templates, string settingSelectedTemplateName)
        {
            int templateSelectedValue = 0;

            if (templates != null && templates.Any())
            {
                int? settingSelectedTemplateId = null;
                if (!string.IsNullOrEmpty(settingSelectedTemplateName))
                {
                    settingSelectedTemplateId = templates.FirstOrDefault(x => x.Name == settingSelectedTemplateName)?.Id;
                }

                if (printerGeneralSettings != null && printerGeneralSettings?.AddressLabelTemplateId != null && templates != null && templates.Any(x => x.Id == printerGeneralSettings.AddressLabelTemplateId))
                {
                    templateSelectedValue = printerGeneralSettings.AddressLabelTemplateId ?? 0;
                }
                else if (settingSelectedTemplateId != null && settingSelectedTemplateId != 0)
                {
                    templateSelectedValue = settingSelectedTemplateId ?? 0;
                }
            }

            return templateSelectedValue;
        }

        public static TemplateDownloadModel DownloadAndGetTemplate(TemplateListModel template, bool isVisible = true, bool inform = false, TaskBeforeOpen taskBeforeOpen = null, bool createNew = true)
        {
            TemplateDownloadModel downloadedTemplate = null;

            if (template != null)
            {
                byte[] templateBytes = inform ? AdminPanelWebApi.GetInformTemplate(template.Id) : AdminPanelWebApi.GetTemplate(template.Id);
                if (templateBytes != null && templateBytes.Length > 0)
                {
                    string filenameExtension = Path.GetExtension(template.Name)?.ToLower();
                    var extension = "docx";
                    if (NonWordTemplateManager.WordFileTypes.Contains(filenameExtension))
                    {
                        extension = filenameExtension;
                    }
                    else if (!filenameExtension.IsEmpty() && !NonWordTemplateManager.WordFileTypes.Contains(filenameExtension))
                    {
                        NonWordTemplateManager.SaveAndOpen(template.Name, templateBytes);
                        return null;
                    }

                    var tmpFile = Path.ChangeExtension(Path.GetTempFileName(), extension);
                    File.WriteAllBytes(tmpFile, templateBytes);

                    if (ThisAddIn.Instance.TmpTemplatesToDelete != null)
                    {
                        ThisAddIn.Instance.TmpTemplatesToDelete.Add(tmpFile);
                    }

                    if (taskBeforeOpen != null)
                    {
                        taskBeforeOpen();
                    }

                    if (createNew)
                    {
                        downloadedTemplate = new TemplateDownloadModel
                        {
                            Document = AddNewDocument(tmpFile, isVisible: isVisible),
                            TemplatePath = tmpFile
                        };
                    }
                    else
                    {
                        var document = ThisAddIn.Instance.Application.Documents.Open(tmpFile, AddToRecentFiles:false, Visible: isVisible);
                        downloadedTemplate = new TemplateDownloadModel
                        {
                            Document = document,
                            TemplatePath = tmpFile
                        };
                    }
                }
            }

            return downloadedTemplate;
        }

        public static void SetComboBoxValue(ComboBox comboBox, string value)
        {
            if (comboBox?.Items != null && !string.IsNullOrEmpty(value))
            {
                int foundIndex = -1;

                for (int i = 0; i < comboBox.Items.Count; ++i)
                {
                    try
                    {
                        string comboBoxItemValue = null;
                        var item = comboBox.Items[i];

                        if (item is KeyValuePair<string, string>)
                        {
                            comboBoxItemValue = ((KeyValuePair<string, string>)item).Key;
                        }
                        else if (item is ComboboxItem)
                        {
                            comboBoxItemValue = ((ComboboxItem)item).Value?.ToString();
                        }
                        else if (item is ComboBoxItem)
                        {
                            comboBoxItemValue = ((ComboBoxItem)item).Content?.ToString();
                        }
                        else if (item is string)
                        {
                            comboBoxItemValue = item.ToString();
                        }

                        if (comboBoxItemValue != null && comboBoxItemValue.Trim().ToLower() == value.Trim().ToLower())
                        {
                            foundIndex = i;
                            break;
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex);
                    }
                }

                if (foundIndex != -1)
                {
                    comboBox.SelectedIndex = foundIndex;
                }
                else
                {
                    comboBox.Text = value;
                }
            }
        }

        public static string ToRoman(int nr)
        {
            if (nr >= 1000) return "M" + ToRoman((nr - 1000));
            if (nr >= 900) return "CM" + ToRoman((nr - 900));
            if (nr >= 500) return "D" + ToRoman((nr - 500));
            if (nr >= 400) return "CD" + ToRoman((nr - 400));
            if (nr >= 100) return "C" + ToRoman((nr - 100));
            if (nr >= 90) return "XC" + ToRoman((nr - 90));
            if (nr >= 50) return "L" + ToRoman((nr - 50));
            if (nr >= 40) return "XL" + ToRoman((nr - 40));
            if (nr >= 10) return "X" + ToRoman((nr - 10));
            if (nr >= 9) return "IX" + ToRoman((nr - 9));
            if (nr >= 5) return "V" + ToRoman((nr - 5));
            if (nr >= 4) return "IV" + ToRoman((nr - 4));
            if (nr >= 1) return "I" + ToRoman((nr - 1));

            return string.Empty;
        }

        public static void UpdateAddendaCrossReferences()
        {
            var document = ThisAddIn.Instance.Application.ActiveDocument;
            if (document != null && document.Bookmarks != null && document.Hyperlinks != null)
            {
                foreach (Bookmark bookmark in document.Bookmarks)
                {
                    try
                    {
                        if (bookmark.Range.Sections[1].Range.ContentControls.Count > 0)
                        {
                            var contentControl = bookmark.Range.Sections[1].Range.ContentControls[1];
                            if (contentControl != null && contentControl.Title == CustomConstants.ADDENDA_CONTENT_CONTROL_TITLE)
                            {
                                foreach (Microsoft.Office.Interop.Word.Hyperlink hyperlink in document.Hyperlinks)
                                {
                                    if (contentControl.Range.ContentControls.Count > 0)
                                    {
                                        var innerContentControl = contentControl?.Range?.ContentControls[1];
                                        if (innerContentControl != null && bookmark.Name == hyperlink.Name)
                                        {
                                            var title = innerContentControl.Title;


                                            switch (title)
                                            {
                                                case CustomConstants.ADDENDA_TYPE_APPENDIX_TITLE:
                                                    title = MultiLanguage.MLanguageUtil.GetResource(MultiLanguage.MLanguageResourceEnum.ADDAppendix);
                                                    break;
                                                case CustomConstants.ADDENDA_TYPE_EXHIBIT_TITLE:
                                                    title = MultiLanguage.MLanguageUtil.GetResource(MultiLanguage.MLanguageResourceEnum.ADDExhibit);
                                                    break;
                                                case CustomConstants.ADDENDA_TYPE_NUMBERING_SCHEDULE_TITLE:
                                                case CustomConstants.ADDENDA_TYPE_SCHEDULE_TITLE:
                                                    title = MultiLanguage.MLanguageUtil.GetResource(MultiLanguage.MLanguageResourceEnum.ADDSchedule);
                                                    break;
                                            }

                                            hyperlink.TextToDisplay = title + ' ' + innerContentControl.Range.Text;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex);
                    }
                }
            }
        }

        public static string GetAddendaBookmarkFromBookmarks(Bookmarks bookmarks)
        {
            if (bookmarks != null)
            {
                foreach (Bookmark bookmark in bookmarks)
                {
                    if (bookmark.Name.IndexOf(CustomConstants.ADDENDA_BOOKMARK_PREFIX, StringComparison.OrdinalIgnoreCase) >= 0)
                    {
                        return bookmark.Name;
                    }
                }
            }

            return null;
        }

        public static bool CheckIfRangeHasAddendaHyperlink(Range range)
        {
            if (range.Hyperlinks.Count > 0)
            {
                foreach (Word.Hyperlink hyperlink in range.Hyperlinks)
                {
                    if (hyperlink.Range.Text.IndexOf(CustomConstants.ADDENDA_TYPE_APPENDIX_TITLE, StringComparison.OrdinalIgnoreCase) >= 0 || hyperlink.Range.Text.IndexOf(CustomConstants.ADDENDA_TYPE_SCHEDULE_TITLE, StringComparison.OrdinalIgnoreCase) >= 0 || hyperlink.Range.Text.IndexOf(CustomConstants.ADDENDA_TYPE_EXHIBIT_TITLE, StringComparison.OrdinalIgnoreCase) >= 0)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public static string TrimEndPunctuationMarks(string inputString)
        {
            return inputString != null ? inputString.TrimEnd(new char[] { '.', ':', ',', ';' }) : string.Empty;
        }

        public static void UpdateNumeberedAddenda()
        {
            List<Bookmark> sources = new List<Bookmark>();
            List<Bookmark> addendas = new List<Bookmark>();

            foreach (Bookmark bm in ThisAddIn.Instance.Application.ActiveDocument.Bookmarks)
            {
                if (bm.Name.StartsWith(CustomConstants.ADDENDA_BOOKMARK_PREFIX))
                {
                    addendas.Add(bm);
                }

                if (bm.Name.StartsWith(CustomConstants.ADDENDA_BOOKMARK_SOURCE_PARAGRAPH + CustomConstants.ADDENDA_BOOKMARK_PREFIX))
                {
                    sources.Add(bm);
                }
            }

            foreach (Bookmark addenda in addendas)
            {
                Bookmark source = sources.FirstOrDefault(x => x.Name == CustomConstants.ADDENDA_BOOKMARK_SOURCE_PARAGRAPH + addenda.Name);
                if (source != null)
                {
                    var model = ListTemplateUtil.GetSelectionParagraphNumberAndRange(source.Range.Paragraphs.First, 0);
                    if (model?.LevelNumber != null)
                    {
                        string numbering = Utils.TrimEndPunctuationMarks(model.LevelNumber);

                        foreach (ContentControl cc in addenda.Range.ContentControls)
                        {
                            if (cc.Title == CustomConstants.ADDENDA_TYPE_NUMBERING_SCHEDULE_TITLE)
                            {
                                cc.Range.Text = numbering;
                            }
                        }
                    }
                }
            }
        }
        public static void SetSectionHeaderFooterProperties(HeadersFooters headersFooters, bool linkToPrevious = false, bool restartPageNumbering = true, bool clearRange = false)
        {
            foreach (HeaderFooter headerFooter in headersFooters)
            {
                if (headerFooter.Exists)
                {
                    headerFooter.LinkToPrevious = linkToPrevious;
                    if (restartPageNumbering)
                    {
                        headerFooter.PageNumbers.RestartNumberingAtSection = true;
                        headerFooter.PageNumbers.StartingNumber = 1;
                    }
                    else
                    {
                        headerFooter.PageNumbers.RestartNumberingAtSection = false;
                    }
                    if (clearRange)
                    {
                        headerFooter.Range.Text = "";
                    }
                }
            }
        }

        public static void UpdateAddendaIndexes()
        {
            var document = ThisAddIn.Instance.Application.ActiveDocument;
            var documentContentControls = document.ContentControls;
            var index = new AutoIndex();
            var addendaOptions = new AddendaOptions();

            foreach (ContentControl contentControl in documentContentControls)
            {
                if ((contentControl.Title == CustomConstants.ADDENDA_TYPE_APPENDIX_TITLE || contentControl.Title == CustomConstants.ADDENDA_TYPE_EXHIBIT_TITLE || contentControl.Title == CustomConstants.ADDENDA_TYPE_SCHEDULE_TITLE) && contentControl.Range.Words[1] != null)
                {
                    string ch = "";

                    switch (ReadDocumentProperty(contentControl.Title))
                    {
                        case "A, B, C, ...":
                            {
                                ch += (char)(index[contentControl.Title] + 64);
                                break;
                            }
                        case "a, b, c, ...":
                            {
                                ch += (char)(index[contentControl.Title] + 96);
                                break;
                            }
                        case "I, II, III, ...":
                            {
                                ch += Utils.ToRoman(index[contentControl.Title]);
                                break;
                            }
                        case "i, ii, iii, ...":
                            {
                                ch += Utils.ToRoman(index[contentControl.Title]).ToLower();
                                break;
                            }
                        case "1, 2, 3, ...":
                        default:
                            {
                                ch += index[contentControl.Title];
                                break;
                            }
                    }

                    index[contentControl.Title] += 1;

                    List<ContentControl> addendaNumbersInPageNumber = GetAddendaNumberInPageNumberContentControls(contentControl.Range.Sections.First);

                    foreach (var addendaNumber in addendaNumbersInPageNumber)
                    {
                        addendaNumber.Range.Text = ch;
                    }

                    if (addendaOptions.QuotationIsChecked)
                    {
                        ch.TrimStart('“');
                        ch.TrimEnd('”');

                        ch = "“" + ch + "”";
                    }

                    contentControl.Range.Text = ch;
                }
            }
        }

        public static Word.Style GetStandardStyle()
        {
            if (ThisAddIn.Instance.Application.Documents.Count > 0)
            {
                try
                {
                    return ThisAddIn.Instance.Application.ActiveDocument.Styles[GetStandardStyleName()];
                }
                catch
                {
                    return ThisAddIn.Instance.Application.ActiveDocument.Styles[WdBuiltinStyle.wdStyleNormal];
                }
            }

            return null;
        }

        public static string GetStandardStyleName()
        {
            string styleName = SettingsManager.GetSetting("StandardParaStyle", "Styles", null);
            if (styleName != null)
            {
                int builtInStyle;
                if (int.TryParse(styleName, out builtInStyle))
                {
                    if (ThisAddIn.Instance.Application.Documents.Count > 0)
                    {
                        try
                        {
                            return ThisAddIn.Instance.Application.ActiveDocument.Styles[(WdBuiltinStyle)builtInStyle].NameLocal;
                        }
                        catch
                        {
                            return ThisAddIn.Instance.Application.ActiveDocument.Styles[WdBuiltinStyle.wdStyleNormal].NameLocal;
                        }
                    }
                    else
                    {
                        return "Normal";
                    }
                }
                else
                {
                    return styleName;
                }
            }
            else
            {
                if (ThisAddIn.Instance.Application.Documents.Count > 0)
                {
                    return ThisAddIn.Instance.Application.ActiveDocument.Styles[WdBuiltinStyle.wdStyleNormal].NameLocal;
                }
                else
                {
                    return "Normal";
                }
            }
        }

        public static void CopySectionPageSetup(Section from, Section to)
        {
            to.PageSetup.BookFoldPrinting = from.PageSetup.BookFoldPrinting;
            to.PageSetup.BookFoldPrintingSheets = from.PageSetup.BookFoldPrintingSheets;
            to.PageSetup.BookFoldRevPrinting = from.PageSetup.BookFoldRevPrinting;
            to.PageSetup.BottomMargin = from.PageSetup.BottomMargin;
            to.PageSetup.DifferentFirstPageHeaderFooter = from.PageSetup.DifferentFirstPageHeaderFooter;
            to.PageSetup.FirstPageTray = from.PageSetup.FirstPageTray;
            to.PageSetup.FooterDistance = from.PageSetup.FooterDistance;
            to.PageSetup.Gutter = from.PageSetup.Gutter;
            to.PageSetup.GutterOnTop = from.PageSetup.GutterOnTop;
            to.PageSetup.GutterPos = from.PageSetup.GutterPos;
            to.PageSetup.GutterStyle = from.PageSetup.GutterStyle;
            to.PageSetup.HeaderDistance = from.PageSetup.HeaderDistance;
            to.PageSetup.LayoutMode = from.PageSetup.LayoutMode;
            to.PageSetup.LeftMargin = from.PageSetup.LeftMargin;
            to.PageSetup.LineNumbering = from.PageSetup.LineNumbering;
            to.PageSetup.MirrorMargins = from.PageSetup.MirrorMargins;
            to.PageSetup.OddAndEvenPagesHeaderFooter = from.PageSetup.OddAndEvenPagesHeaderFooter;
            to.PageSetup.PaperSize = from.PageSetup.PaperSize;
            to.PageSetup.Orientation = from.PageSetup.Orientation;
            to.PageSetup.OtherPagesTray = from.PageSetup.OtherPagesTray;
            to.PageSetup.PageHeight = from.PageSetup.PageHeight;
            to.PageSetup.PageWidth = from.PageSetup.PageWidth;
            to.PageSetup.RightMargin = from.PageSetup.RightMargin;
            to.PageSetup.SectionDirection = from.PageSetup.SectionDirection;
            to.PageSetup.SectionStart = from.PageSetup.SectionStart;
            to.PageSetup.SuppressEndnotes = from.PageSetup.SuppressEndnotes;
            to.PageSetup.TextColumns = from.PageSetup.TextColumns;
            to.PageSetup.TopMargin = from.PageSetup.TopMargin;
            to.PageSetup.TwoPagesOnOne = from.PageSetup.TwoPagesOnOne;
            to.PageSetup.VerticalAlignment = from.PageSetup.VerticalAlignment;
        }

        //deprecated
        public static void SectionSetLinkToPrevious(Section section)
        {
            foreach (HeaderFooter header in section.Headers)
            {
                if (header.Exists)
                {
                    header.LinkToPrevious = true;
                }
            }

            foreach (HeaderFooter footer in section.Footers)
            {
                if (footer.Exists)
                {
                    footer.LinkToPrevious = true;
                }
            }
        }

        public static List<ContentControl> GetAddendaNumberInPageNumberContentControls(Section section)
        {
            List<ContentControl> result = new List<ContentControl>();
            var footersList = new List<HeaderFooter>() {
                section.Footers[WdHeaderFooterIndex.wdHeaderFooterFirstPage],
                section.Footers[WdHeaderFooterIndex.wdHeaderFooterPrimary],
                section.Footers[WdHeaderFooterIndex.wdHeaderFooterEvenPages]
            };

            if (footersList.Count > 0)
            {
                foreach (HeaderFooter footer in footersList)
                {
                    if (footer.Exists)
                    {
                        foreach (ContentControl cc in footer.Range.ContentControls)
                        {
                            if (cc.Tag == CustomConstants.ADDENDA_NUMBER_IN_PAGE_NUMBER_TAG)
                            {
                                result.Add(cc);
                            }
                        }
                    }
                }
            }

            return result;
        }

        public static string ReadDocumentProperty(string propertyName)
        {
            Microsoft.Office.Core.DocumentProperties properties;
            properties = (Microsoft.Office.Core.DocumentProperties)ThisAddIn.Instance.Application.ActiveDocument.CustomDocumentProperties;

            foreach (Microsoft.Office.Core.DocumentProperty prop in properties)
            {
                if (prop.Name == propertyName)
                {
                    return prop.Value.ToString();
                }
            }
            return null;
        }

        public static Word.Style GetOrCreateAddendaCrossReferenceCharacterStyle()
        {
            Word.Style addendaCharacterStyle;
            var addendaOptions = new AddendaOptions();

            if (!addendaOptions.ColorIsChecked)
            {
                try
                {
                    addendaCharacterStyle = ThisAddIn.Instance.Application.ActiveDocument.Styles[CustomConstants.ADDENDA_CHARACTER_STYLE];
                }
                catch (Exception)
                {
                    addendaCharacterStyle = ThisAddIn.Instance.Application.ActiveDocument.Styles.Add(CustomConstants.ADDENDA_CHARACTER_STYLE, WdStyleType.wdStyleTypeCharacter);
                    addendaCharacterStyle.Font.Color = WdColor.wdColorBlack;
                    addendaCharacterStyle.Font.Underline = WdUnderline.wdUnderlineSingle;
                }
            }
            else
            {
                addendaCharacterStyle = ThisAddIn.Instance.Application.ActiveDocument.Styles[CustomConstants.ADDENDA_HYPERLINK_CHARACTER_STYLE];
            }

            return addendaCharacterStyle;
        }

        public static Word.Style GetOrCreatePromptStyle()
        {
            Word.Style promptStyle = GetStyleByName("Prompt");

            if (promptStyle == null)
            {
                promptStyle = ThisAddIn.Instance.Application.ActiveDocument.Styles.Add("Prompt", WdStyleType.wdStyleTypeCharacter);
                promptStyle.Font.Color = SettingsManager.GetPromptColor();
            }

            return promptStyle;
        }

        public static Word.Style GetStyleByName(string styleName)
        {
            Word.Style style = null;

            if (!string.IsNullOrEmpty(styleName))
            {
                try
                {
                    style = ThisAddIn.Instance.Application.ActiveDocument.Styles[styleName];
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex);
                }
            }

            return style;
        }

        public static TemplateListByType GetTemplateFromTemplateListByType(TemplateType templateType)
        {
            TemplateListByType templateListByType = null;

            var currentAuthorSettings = ManageUsers.CurrentAuthorSettings;
            var templateList = AdminPanelWebApi.GetTemplateList(templateType, currentAuthorSettings.CompanyId, currentAuthorSettings.LanguageId);

            if (templateList.Count > 0)
            {
                templateListByType = new TemplateListByType();

                if (templateList.Count == 1)
                {
                    templateListByType.Template = templateList.FirstOrDefault();
                }
                else
                {
                    var templateChooser = new TemplateChooser(templateType);
                    templateChooser.PopulateTemplates(templateList);
                    templateChooser.ShowDialog();
                    if (templateChooser.ResultOk)
                    {
                        templateListByType.Template = templateList.Where(x => x.Id == templateChooser.ResultId).FirstOrDefault();
                    }

                    templateListByType.ResultOk = templateChooser.ResultOk;
                }
            }

            return templateListByType;
        }

        public static void UpdateCurrentCultureShortDateFormat()
        {
            try
            {
                CultureInfo cultureInfo = CultureInfo.CreateSpecificCulture(MLanguageUtil.GetResource(MLanguageResourceEnum.DateCultureID));
                cultureInfo.DateTimeFormat.ShortDatePattern = Utils.GetDefaultDateFormat();
                Thread.CurrentThread.CurrentCulture = cultureInfo;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
        }

        public static void SetBackDefaultPrinter()
        {
            if (!string.IsNullOrEmpty(ThisAddIn.Instance.PreMultiPrintActivePrinterName))
            {
                try
                {
                    ThisAddIn.Instance.Application.ActiveDocument.Application.ActivePrinter = ThisAddIn.Instance.PreMultiPrintActivePrinterName;
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex);
                }
            }
        }

        public static List<KeyValuePair<int, string>> GetLocationsForCombobox()
        {
            var addresses = AdminPanelWebApi.GetAddresses(1);
            var addressList = new List<KeyValuePair<int, string>>();
            if (addresses != null)
            {
                foreach (var address in addresses)
                {
                    addressList.Add(new KeyValuePair<int, string>(address.Id, ParseAuthorOfficeLocation(address)));
                }
            }
            return addressList;
        }

        public static string ParseAuthorOfficeLocation(AuthorAddressDTO officeLocation)
        {
            var authorOfficeLocation = new List<string>();
            if (!string.IsNullOrEmpty(officeLocation.Name))
            {
                authorOfficeLocation.Add(officeLocation.Name);
            }
            if (!string.IsNullOrEmpty(officeLocation.City))
            {
                authorOfficeLocation.Add(officeLocation.City);
            }
            return string.Join(" - ", authorOfficeLocation.ToArray());
        }

        public static bool IsSectionContinous(Section section, Word.Document document)
        {
            int index = section.Index;

            if (index <= 1)
            {
                return false;
            }

            Section previous = document.Sections[index - 1];

            return (int)section.Range.Information[WdInformation.wdActiveEndPageNumber] == (int)previous.Range.Information[WdInformation.wdActiveEndPageNumber];
        }


        public static bool GetIsAnyProcessRunning(string[] applications)
        {
            applications = applications.Select(x => x.ToLower()).ToArray();

            foreach (var process in Process.GetProcesses())
            {
                if (applications.Contains(process.ProcessName?.ToLower()))
                {
                    return true;
                }
            }

            return false;
        }
    }
}
