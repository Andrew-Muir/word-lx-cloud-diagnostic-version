﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace InfowareVSTO
{
	/// <summary>
	/// Interaction logic for ConfirmDeleteContacts_WPF.xaml
	/// </summary>
	public partial class ConfirmDeleteContacts_WPF : InfowareWindow
	{
		public bool ResultOk { get; set; }

		public ConfirmDeleteContacts_WPF()
		{
			InitializeComponent();
		}

		private void ButtonNo_Click(object sender, RoutedEventArgs e)
		{
			this.ResultOk = false;
			this.Close();
		}

		private void ButtonYes_Click(object sender, RoutedEventArgs e)
		{
			this.ResultOk = true;
			this.Close();
		}
	}
}
