﻿using System;
using System.Windows;
using InfowareVSTO.Common;
using Microsoft.Office.Interop.Word;
using Application = Microsoft.Office.Interop.Word.Application;

namespace InfowareVSTO.ToolsForStyles
{
    /// <summary>
    /// Interaction logic for QuotationMarks.xaml
    /// </summary>
    public partial class QuotationMarks : InfowareWindow
    {
        public QuotationMarks()
        {
            InitializeComponent();
            UpdateRadioButtonStatuses();
        }

        private void UpdateRadioButtonStatuses()
        {
            bool allowOnlySmartQuotes = SettingsManager.GetSettingAsBool("AllowOnlySmartQuotes", "Tools", false);
            bool allowOnlyStraightQuotes = SettingsManager.GetSettingAsBool("AllowOnlyStraightQuotes", "Tools", false);

            if (allowOnlySmartQuotes == true)
            {
                StraightToSmart.IsEnabled = false;
                ChevronToStraight.IsEnabled = false;
            }

            if (allowOnlyStraightQuotes == true)
            {
                SmartToStraight.IsEnabled = false;
                ChevronToSmart.IsEnabled = false;
            }
        }

        private void OnOkButtonClick(object sender, RoutedEventArgs e)
        {
            Application application = Globals.ThisAddIn.Application;

            if (SmartToStraight.IsChecked != null && SmartToStraight.IsChecked == true)
            {
                ConvertSmartQuotesToStraightQuotes(application);
            }
            else if (StraightToSmart.IsChecked != null && StraightToSmart.IsChecked == true)
            {
                ConvertStraightToSmartQuotes(application);
            }
            else if (AllToChevron.IsChecked != null && AllToChevron.IsChecked == true)
            {
                ConvertStraightToSmartQuotes(application);
                ConvertSmartQuotesToChevronQuotes(application);
            }
            else if (ChevronToSmart.IsChecked != null && ChevronToSmart.IsChecked == true)
            {
                ConvertChevronToSmartQuotes(application);
            }
            else if (ChevronToStraight.IsChecked != null && ChevronToStraight.IsChecked == true)
            {
                ConvertChevronToStraightQuotes(application);
            }

            Close();
        }

        private void ConvertSmartQuotesToStraightQuotes(Application application)
        {
            var previousQuoteAutoFormatValue = application.Options.AutoFormatAsYouTypeReplaceQuotes;

            application.ScreenUpdating = false;
            if (ThisAddIn.Instance.IsFunctionForLogging("ScreenUpdating"))
                Logger.Log("Disabled Screen Updating (QuotationMarks.xaml.cs > ConvertSmartQuotesToStraightQuotes)");
            application.Options.AutoFormatAsYouTypeReplaceQuotes = false;

            Range rng = application.ActiveDocument.Range();

            rng.Find.ClearFormatting();
            rng.Find.Replacement.ClearFormatting();
            rng.Find.Wrap = WdFindWrap.wdFindContinue;
            rng.Find.Forward = true;
            rng.Find.Format = false;
            rng.Find.MatchCase = false;
            rng.Find.MatchWholeWord = false;
            rng.Find.MatchWildcards = true;

            rng.Find.Text = "[“”]";
            rng.Find.Replacement.Text = "" + (char)34;
            rng.Find.Execute(Replace: WdReplace.wdReplaceAll);

            rng.Find.Text = "[‘’]";
            rng.Find.Replacement.Text = "" + (char)39;
            rng.Find.Execute(Replace: WdReplace.wdReplaceAll);

            application.Options.AutoFormatAsYouTypeReplaceQuotes = previousQuoteAutoFormatValue;
            application.ScreenUpdating = true;
            if (ThisAddIn.Instance.IsFunctionForLogging("ScreenUpdating"))
                Logger.Log("Enabled Screen Updating (QuotationMarks.xaml.cs > ConvertSmartQuotesToStraightQuotes)");
        }

        private void ConvertStraightToSmartQuotes(Application application)
        {
            var previousQuoteAutoFormatValue = application.Options.AutoFormatAsYouTypeReplaceQuotes;

            application.ScreenUpdating = false;
            if (ThisAddIn.Instance.IsFunctionForLogging("ScreenUpdating"))
                Logger.Log("Disabled Screen Updating (QuotationMarks.xaml.cs > ConvertStraightToSmartQuotes)");
            application.Options.AutoFormatAsYouTypeReplaceQuotes = true;

            Range rng = application.ActiveDocument.Range();

            rng.Find.ClearFormatting();
            rng.Find.Replacement.ClearFormatting();
            rng.Find.Wrap = WdFindWrap.wdFindContinue;
            rng.Find.Forward = true;
            rng.Find.Format = false;
            rng.Find.MatchCase = false;
            rng.Find.MatchWholeWord = false;
            rng.Find.MatchWildcards = true;

            rng.Find.Text = "[\"\"]";
            rng.Find.Replacement.Text = "\"";
            rng.Find.Execute(Replace: WdReplace.wdReplaceAll);

            rng.Find.Text = "['']";
            rng.Find.Replacement.Text = "'";
            rng.Find.Execute(Replace: WdReplace.wdReplaceAll);

            application.Options.AutoFormatAsYouTypeReplaceQuotes = previousQuoteAutoFormatValue;
            application.ScreenUpdating = true;
            if (ThisAddIn.Instance.IsFunctionForLogging("ScreenUpdating"))
                Logger.Log("Enabled Screen Updating (QuotationMarks.xaml.cs > ConvertStraightToSmartQuotes)");
        }

        private void ConvertSmartQuotesToChevronQuotes(Application application)
        {
            var previousQuoteAutoFormatValue = application.Options.AutoFormatAsYouTypeReplaceQuotes;

            application.ScreenUpdating = false;
            if (ThisAddIn.Instance.IsFunctionForLogging("ScreenUpdating"))
                Logger.Log("Disabled Screen Updating (QuotationMarks.xaml.cs > ConvertSmartQuotesToChevronQuotes)");
            application.Options.AutoFormatAsYouTypeReplaceQuotes = false;

            Range rng = application.ActiveDocument.Range();

            rng.Find.ClearFormatting();
            rng.Find.Replacement.ClearFormatting();
            rng.Find.Wrap = WdFindWrap.wdFindContinue;
            rng.Find.Forward = true;
            rng.Find.Format = false;
            rng.Find.MatchCase = false;
            rng.Find.MatchWholeWord = false;
            rng.Find.MatchWildcards = true;

            rng.Find.Text = "[“]";
            rng.Find.Replacement.Text = "" + (char)171 + (char)160;
            rng.Find.Execute(Replace: WdReplace.wdReplaceAll);

            rng.Find.Text = "[”]";
            rng.Find.Replacement.Text = "" + (char)160 + (char)187;
            rng.Find.Execute(Replace: WdReplace.wdReplaceAll);

            application.Options.AutoFormatAsYouTypeReplaceQuotes = previousQuoteAutoFormatValue;
            application.ScreenUpdating = true;
            if (ThisAddIn.Instance.IsFunctionForLogging("ScreenUpdating"))
                Logger.Log("Enabled Screen Updating (QuotationMarks.xaml.cs > ConvertSmartQuotesToChevronQuotes)");
        }

        private void ConvertChevronToSmartQuotes(Application application)
        {
            var previousQuoteAutoFormatValue = application.Options.AutoFormatAsYouTypeReplaceQuotes;

            application.ScreenUpdating = false;
            if (ThisAddIn.Instance.IsFunctionForLogging("ScreenUpdating"))
                Logger.Log("Disabled Screen Updating (QuotationMarks.xaml.cs > ConvertChevronToSmartQuotes)");
            application.Options.AutoFormatAsYouTypeReplaceQuotes = false;

            Range rng = application.ActiveDocument.Range();

            rng.Find.ClearFormatting();
            rng.Find.Replacement.ClearFormatting();
            rng.Find.Wrap = WdFindWrap.wdFindContinue;
            rng.Find.Forward = true;
            rng.Find.Format = false;
            rng.Find.MatchCase = false;
            rng.Find.MatchWholeWord = false;
            rng.Find.MatchWildcards = true;

            rng.Find.Text = "" + (char)171 + (char)160;
            rng.Find.Replacement.Text = "“";
            rng.Find.Execute(Replace: WdReplace.wdReplaceAll);

            rng.Find.Text = "" + (char)160 + (char)187;
            rng.Find.Replacement.Text = "”";
            rng.Find.Execute(Replace: WdReplace.wdReplaceAll);

            rng.Find.Text = "" + (char)171;
            rng.Find.Replacement.Text = "“";
            rng.Find.Execute(Replace: WdReplace.wdReplaceAll);

            rng.Find.Text = "" + (char)187;
            rng.Find.Replacement.Text = "”";
            rng.Find.Execute(Replace: WdReplace.wdReplaceAll);

            application.Options.AutoFormatAsYouTypeReplaceQuotes = previousQuoteAutoFormatValue;
            application.ScreenUpdating = true;
            if (ThisAddIn.Instance.IsFunctionForLogging("ScreenUpdating"))
                Logger.Log("Enabled Screen Updating (QuotationMarks.xaml.cs > ConvertChevronToSmartQuotes)");
        }

        private void ConvertChevronToStraightQuotes(Application application)
        {
            var previousQuoteAutoFormatValue = application.Options.AutoFormatAsYouTypeReplaceQuotes;

            application.ScreenUpdating = false;
            if (ThisAddIn.Instance.IsFunctionForLogging("ScreenUpdating"))
                Logger.Log("Disabled Screen Updating (QuotationMarks.xaml.cs > ConvertChevronToStraightQuotes)");
            application.Options.AutoFormatAsYouTypeReplaceQuotes = false;

            Range rng = application.ActiveDocument.Range();

            rng.Find.ClearFormatting();
            rng.Find.Replacement.ClearFormatting();
            rng.Find.Wrap = WdFindWrap.wdFindContinue;
            rng.Find.Forward = true;
            rng.Find.Format = false;
            rng.Find.MatchCase = false;
            rng.Find.MatchWholeWord = false;
            rng.Find.MatchWildcards = true;

            rng.Find.Text = "" + (char)171 + (char)160;
            rng.Find.Replacement.Text = "\"";
            rng.Find.Execute(Replace: WdReplace.wdReplaceAll);

            rng.Find.Text = "" + (char)160 + (char)187;
            rng.Find.Replacement.Text = "\"";
            rng.Find.Execute(Replace: WdReplace.wdReplaceAll);

            rng.Find.Text = "" + (char)171;
            rng.Find.Replacement.Text = "\"";
            rng.Find.Execute(Replace: WdReplace.wdReplaceAll);

            rng.Find.Text = "" + (char)187;
            rng.Find.Replacement.Text = "\"";
            rng.Find.Execute(Replace: WdReplace.wdReplaceAll);

            application.Options.AutoFormatAsYouTypeReplaceQuotes = previousQuoteAutoFormatValue;
            application.ScreenUpdating = true;
            if (ThisAddIn.Instance.IsFunctionForLogging("ScreenUpdating"))
                Logger.Log("Enabled Screen Updating (QuotationMarks.xaml.cs > ConvertChevronToStraightQuotes)");
        }

        private void OnCancelButtonClick(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
