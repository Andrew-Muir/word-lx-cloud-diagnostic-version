﻿using Microsoft.Office.Interop.Word;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Text.RegularExpressions;


namespace InfowareVSTO.ToolsForStyles
{
    /// <summary>
    /// Interaction logic for KeepTogether.xaml
    /// </summary>
    public partial class KeepTogether : InfowareWindow
    {
        public KeepTogether()
        {
            InitializeComponent();
            Range selectionRange = Globals.ThisAddIn.Application.Selection.Range;
            KeepLinesTogether.IsChecked = (selectionRange.ParagraphFormat.KeepTogether == -1)? true: false;
            KeepWithNext.IsChecked = (selectionRange.ParagraphFormat.KeepWithNext == -1)? true : false;
        }
        private void OK_Click(object sender, RoutedEventArgs e)
        {
            Range selectionRange = Globals.ThisAddIn.Application.Selection.Range;
            selectionRange.ParagraphFormat.KeepTogether = ((KeepLinesTogether.IsChecked == true) ? -1 : 0);
            selectionRange.ParagraphFormat.KeepWithNext = ((KeepWithNext.IsChecked == true) ? -1 : 0);
            this.Close();
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Both_Click(object sender, RoutedEventArgs e)
        {
            KeepLinesTogether.IsChecked = true;
            KeepWithNext.IsChecked = true;
            this.OK_Click(sender, e);
        }

        private void None_Click(object sender, RoutedEventArgs e)
        {
            KeepLinesTogether.IsChecked = false;
            KeepWithNext.IsChecked = false;
            this.OK_Click(sender, e);
        }
    }
}
