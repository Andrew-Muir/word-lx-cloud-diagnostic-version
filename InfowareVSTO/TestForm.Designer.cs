﻿namespace InfowareVSTO
{
	partial class TestForm
	{
		/// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.cbPrinters = new System.Windows.Forms.ComboBox();
			this.btnInvoke = new System.Windows.Forms.Button();
			this.button1 = new System.Windows.Forms.Button();
			this.progressBarCustom = new InfowareVSTO.ProgressBarCustom();
			this.btnStartProgress = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// cbPrinters
			// 
			this.cbPrinters.FormattingEnabled = true;
			this.cbPrinters.Location = new System.Drawing.Point(12, 12);
			this.cbPrinters.Name = "cbPrinters";
			this.cbPrinters.Size = new System.Drawing.Size(281, 21);
			this.cbPrinters.TabIndex = 0;
			// 
			// btnInvoke
			// 
			this.btnInvoke.Location = new System.Drawing.Point(12, 39);
			this.btnInvoke.Name = "btnInvoke";
			this.btnInvoke.Size = new System.Drawing.Size(75, 33);
			this.btnInvoke.TabIndex = 1;
			this.btnInvoke.Text = "Invoke";
			this.btnInvoke.UseVisualStyleBackColor = true;
			this.btnInvoke.Click += new System.EventHandler(this.btnInvoke_Click);
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(102, 39);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(75, 33);
			this.button1.TabIndex = 2;
			this.button1.Text = "Set Settings";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// progressBarCustom
			// 
			this.progressBarCustom.Location = new System.Drawing.Point(12, 90);
			this.progressBarCustom.Name = "progressBarCustom";
			this.progressBarCustom.Size = new System.Drawing.Size(281, 23);
			this.progressBarCustom.TabIndex = 3;
			// 
			// btnStartProgress
			// 
			this.btnStartProgress.Location = new System.Drawing.Point(114, 119);
			this.btnStartProgress.Name = "btnStartProgress";
			this.btnStartProgress.Size = new System.Drawing.Size(75, 23);
			this.btnStartProgress.TabIndex = 4;
			this.btnStartProgress.Text = "&Start";
			this.btnStartProgress.UseVisualStyleBackColor = true;
			this.btnStartProgress.Click += new System.EventHandler(this.btnStartProgress_Click);
			// 
			// TestForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(319, 161);
			this.Controls.Add(this.btnStartProgress);
			this.Controls.Add(this.progressBarCustom);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.btnInvoke);
			this.Controls.Add(this.cbPrinters);
			this.Name = "TestForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Printer Properties Dialog Invoke";
			this.Load += new System.EventHandler(this.TestForm_Load);
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox cbPrinters;
        private System.Windows.Forms.Button btnInvoke;
        private System.Windows.Forms.Button button1;
		public ProgressBarCustom progressBarCustom;
		private System.Windows.Forms.Button btnStartProgress;
	}
}