﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using InfowareVSTO.Util;
using Microsoft.Office.Interop.Word;
using Newtonsoft.Json.Linq;
using Word = Microsoft.Office.Interop.Word;

namespace InfowareVSTO.TaskPaneControls
{
    /// <summary>
    /// Interaction logic for OtherTemplateTaskpane.xaml
    /// </summary>
    public partial class OtherTemplateTaskpane : UserControl
    {
        private Document document;
        private const string TEMPLATE_TAG = "LX-TEMPLATE";
        private Dictionary<string, List<Word.ContentControl>> contentControlsGroups;
        private const string SAVE_PROPERTY = "LX-SAVEDATA";
 
        public OtherTemplateTaskpane()
        {
            InitializeComponent();

            document = ThisAddIn.Instance.Application.ActiveDocument;
            PopulateFields();
        }

        private void PopulateFields()
        {
            List<Word.ContentControl> contentControls = DocumentUtil.GetAllContentControls();
            var filteredCCs = contentControls.Where(x => x.Tag == TEMPLATE_TAG).ToList();

            SetCCGroups(filteredCCs);

            if (contentControlsGroups != null)
            {
                List<string> uniqueNames = contentControlsGroups.Keys.ToList();

                List<SimpleDataField> itemsSource = new List<SimpleDataField>();

                Dictionary<string, string> values = LoadValues();

                foreach (string name in uniqueNames)
                {
                    string savedValue;
                    if (values != null && values.TryGetValue(name, out savedValue))
                    {
                        itemsSource.Add(new SimpleDataField(name, savedValue));
                    }
                    else
                    {
                        itemsSource.Add(new SimpleDataField(name, string.Empty));
                    }
                }

                MainItemsControl.ItemsSource = itemsSource;
            }
        }

        private Dictionary<string, string> LoadValues()
        {
            string savedText = DocumentPropertyUtil.ReadLongProperty(SAVE_PROPERTY);
            if (savedText != null)
            {
                return JObject.Parse(savedText)?.ToObject<Dictionary<string, string>>();
            }

            return null;
        }

        private void SetCCGroups(List<Word.ContentControl> filteredCCs)
        {
            contentControlsGroups = filteredCCs.GroupBy(x => x.Title).ToDictionary(g => g.Key, g => g.ToList());
        }

        public List<string> GetUniqueNames(List<Word.ContentControl> contentControls)
        {
            return contentControls.Select(x => x.Title).Distinct().ToList();
        }

        private void UpdateBtn_Click(object sender, RoutedEventArgs e)
        {
           List<SimpleDataField> values =  MainItemsControl.ItemsSource as List<SimpleDataField>;

            if (values != null)
            {
                foreach(SimpleDataField field in values)
                {
                    List<Word.ContentControl> list;
                    if (contentControlsGroups.TryGetValue(field.Name, out list))
                    {
                        foreach(Word.ContentControl contentControl in list)
                        {
                            contentControl.Range.Text = field.Value;
                        }
                    }
                }

                SaveValues(values);

            }
        }

        private void SaveValues(List<SimpleDataField> values)
        {
            Dictionary <string, string> toSave = values.ToDictionary(x => x.Name, y => y.Value);

            string textToSave = JObject.FromObject(toSave).ToString();

            DocumentPropertyUtil.SaveLongProperty(SAVE_PROPERTY, textToSave);
        }

        private void Expander_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            MainScrollViewer.ScrollToVerticalOffset(MainScrollViewer.VerticalOffset - e.Delta / 2);
        }

        public class SimpleDataField
        {
            public string Name { get; set; }

            public string Value { get; set; }

            public SimpleDataField(string name, string value)
            {
                Name = name;
                Value = value;
            }
        }
    }
}
