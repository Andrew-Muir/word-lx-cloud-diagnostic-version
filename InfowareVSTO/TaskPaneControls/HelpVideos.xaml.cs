﻿using InfowareVSTO.Common.Models;
using InfowareVSTO.Windows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace InfowareVSTO.TaskPaneControls
{
    /// <summary>
    /// Interaction logic for HelpVideos.xaml
    /// </summary>
    public partial class HelpVideos : UserControl
    {
        public HelpVideos()
        {
            InitializeComponent();
            PopulateVideos();
        }

        public void PopulateVideos()
        {
            HelpVideoList list = AdminPanelWebApi.GetHelpVideos();
            if (list != null && list.Categories != null)
            {
                this.MainItemsControl.ItemsSource = list.Categories;
            }
        }

        private void OpenVideo(object sender, MouseButtonEventArgs e)
        {
            FrameworkElement element = sender as FrameworkElement;

            if (element != null)
            {
                HelpVideo video = element.DataContext as HelpVideo;
                if (video != null)
                {
                    HelpVideoPlayer player = new HelpVideoPlayer();
                    player.OpenVideo(video.Folder);
                    player.ShowDialog();
                }
            }
        }
    }
}
