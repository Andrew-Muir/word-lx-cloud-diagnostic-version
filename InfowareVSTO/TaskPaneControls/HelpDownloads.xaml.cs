﻿using InfowareVSTO.Common.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace InfowareVSTO.TaskPaneControls
{
    /// <summary>
    /// Interaction logic for HelpDownloads.xaml
    /// </summary>
    public partial class HelpDownloads : System.Windows.Controls.UserControl
    {
        public HelpDownloads()
        {
            InitializeComponent();
            PopulateLinks();
        }

        private void PopulateLinks()
        {
            HelpFileList files = AdminPanelWebApi.GetHelpDownloadList();
            if (files != null && files.List != null)
            {
                this.MainItemsControl.ItemsSource = files.List;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            HelpFile file = (sender as System.Windows.Controls.Button)?.DataContext as HelpFile;
            if (file != null)
            {
                byte[] arr = AdminPanelWebApi.DownloadHelpFile(file.FileName);

                if (arr != null)
                {
                    SaveFileDialog sfd = new SaveFileDialog();
                    sfd.FileName = file.FileName;
                    sfd.ShowDialog();
                    if (sfd.FileName != null || sfd.FileName == file.FileName)
                    {
                        File.WriteAllBytes(sfd.FileName, arr);
                        if (System.IO.Path.GetExtension(file.FileName).ToLower() == ".pdf")
                        {
                            Process.Start(sfd.FileName);
                        }
                    }
                }
            }
        }
    }
}
