﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using DocumentFormat.OpenXml.Wordprocessing;
using InfowareVSTO.Common;
using InfowareVSTO.Common.DTOs;
using InfowareVSTO.Common.General;
using InfowareVSTO.Common.Language;
using InfowareVSTO.Common.Models;
using InfowareVSTO.DataSource;
using InfowareVSTO.Letterhead;
using InfowareVSTO.MultiLanguage;
using InfowareVSTO.Util;
using InfowareVSTO.VSTOAuthor;
using InfowareVSTO.Windows;
using Microsoft.Office.Core;
using Microsoft.Office.Interop.Word;
using Microsoft.Office.Tools.Word;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using ContentControl = Microsoft.Office.Interop.Word.ContentControl;

namespace InfowareVSTO.TaskPaneControls.WordDa
{
    /// <summary>
    /// Interaction logic for TemplateLabelUserControl.xaml
    /// </summary>
    public partial class TemplateLetterUserControl : UserControl, IRefreshData
    {
        private const string INCLUDE_FIRM_NAME_CLOSING_KEY = "IncludeFirmNameInClosing";
        private const string INCLUDE_PER_IN_CLOSING_KEY = "IncludePerInClosing";
        private const string INCLUDE_ASSISTANT_BLOCK_KEY = "IncludeAssistantBlock";
        private AuthorDTO Author { get; set; }
        public Range Selection { get; set; }

        private AuthorDTO Assistant { get; set; }
        private List<SelectedContact> AddedRecipients { get; set; }
        private List<SelectedContact> AddedCCRecipients { get; set; }
        private List<SelectedContact> AddedBCCRecipients { get; set; }
        private List<string> DefaultEnclosureSettings { get; set; }

        private List<string> DefaultSalutations { get; set; }
        private List<string> DefaultExcludedSalutations { get; set; }
        private List<string> Salutations { get; set; }
        private int? previousOfficeLocationId = null;
        public List<string> ClosingStatements { get; private set; }
        private bool suppressRemoveAddedRecipient;

        public int DefaultClosingIndex { get; private set; }
        public List<KeyValuePair<System.Windows.Controls.Control, string>> FieldFunctionList { get; private set; }

        private string closingEndChar;
        private DataSourceValues dataSourceValues;



        static TemplateLetterUserControl()
        {

        }

        public TemplateLetterUserControl()
        {
            InitializeComponent();

            this.MainContentControl.Content = new DataFillInnerUserControl(false);

            UpdateClosingStatements();

            UpdateDefaultClosingIndex();

            UpdateClosingEndChar();

            InitFieldValidation();

            UpdateIncludeFirmNameInClosing();
            PopulateDiaryDateComboBox();

            Utils.UpdateCurrentCultureShortDateFormat();
            dateTimePickerDate.SelectedDate = DateTime.Today;

            //get current author and assistant
            var author = AdminPanelWebApi.GetCurrentAuthor();
            if (author != null)
            {
                Author = author.ToAuthorDTO();
                textBoxAuthor.Text = Author?.FirstName + " " + Author?.LastName;
            }

            int assistantId = AdminPanelWebApi.GetCurrentAsistantId();
            if (assistantId > 0)
            {
                var assistant = AdminPanelWebApi.GetAuthor(assistantId);
                if (assistant != null)
                {
                    Assistant = assistant.ToAuthorDTO();
                    textBoxAssistant.Text = Assistant?.FirstName + " " + Assistant?.LastName;
                }
            }

            comboBoxOfficeLocation.ItemsSource = Utils.GetLocationsForCombobox();

            int defaultLocationId = -1;
            var currentAuthorSettings = ManageUsers.CurrentAuthorSettings;
            if (currentAuthorSettings.LocationId.HasValue)
            {
                defaultLocationId = currentAuthorSettings.LocationId.Value;
            }
            else
            {
                int? companyId = currentAuthorSettings.CompanyId ?? DocumentPropertyUtil.GetTemplateCompanyID();

                if (companyId.HasValue)
                {
                    defaultLocationId = AdminPanelWebApi.GetCompanyDetails(companyId.Value).Id;
                }
            }

            if (defaultLocationId >= 0)
            {
                comboBoxOfficeLocation.SelectedValue = defaultLocationId;
            }

            DefaultEnclosureSettings = new List<string> { "Encl,Encls", "Attch,Attchs" };

            var enclosures = GetEnclosureList();
            if (enclosures != null)
            {
                var enclosureOptions = BaseUtils.GetEnclosureOptions(enclosures);
                if (enclosureOptions != null && enclosureOptions.Count > 0)
                {
                    comboBoxEnclosures.ItemsSource = enclosureOptions;
                    comboBoxEnclosures.DisplayMemberPath = "Key";
                    comboBoxEnclosures.SelectedValuePath = "Value";
                    comboBoxEnclosures.SelectedIndex = 0;
                }
            }
            AddedRecipients = new List<SelectedContact>();
            AddedCCRecipients = new List<SelectedContact>();
            AddedBCCRecipients = new List<SelectedContact>();

            DefaultSalutations = new List<string> { "Counsel", "Mr.", "Ms.", "Mrs.", "Mr/Ms.", "Mr/Mrs.", "Mx.", "Madame", "Mesdames", "Sir", "Sirs", "Sir/Madame", "Sirs/Mesdames" };
            DefaultExcludedSalutations = new List<string> { "Counsel", "Sir", "Madame", "Sir/Madame", "Sirs/Mesdames", "Sirs", "Mesdames" };

            Salutations = GetSalutations();
            comboBoxSalutation.ItemsSource = Salutations;

            DataContext = this;
            SettingsManager.PopulateComboboxWithHandlingOptions(this.comboBoxHandling);
            SettingsManager.PopulateComboboxWithDeliveryOptions(this.comboBoxDelivery);
            UpdateDiaryDateVisibility();

            try
            {
                var apiResponse = AdminPanelWebApi.GetCustomSetting(Utils.GetProcessorId(), "LetterIncludePer");
                if (apiResponse.StatusCode == 200)
                    checkBoxIncludePer.IsChecked = Convert.ToBoolean((string)apiResponse.Data);

                apiResponse = AdminPanelWebApi.GetCustomSetting(Utils.GetProcessorId(), "LetterIncludeFirmName");
                if (apiResponse.StatusCode == 200)
                    checkBoxIncludeFirmName.IsChecked = Convert.ToBoolean((string)apiResponse.Data);
            }
            catch
            {
                //do nothing
            }

            OrderFields();
            LoadAnswers();

        }

        private void OrderFields()
        {
            string json = SettingsManager.GetSetting("TaskPaneFieldOrder", "Letters", null);
            TaskpaneFieldOrderer tfo = new TaskpaneFieldOrderer();
            tfo.OrderFields(json, this.FieldContainer);
        }

        private void InitFieldValidation()
        {
            string settingStr = SettingsManager.GetSetting("FieldsRequiringValidation", "Letters");
            List<KeyValuePair<System.Windows.Controls.Control, string>> result = new List<KeyValuePair<System.Windows.Controls.Control, string>>();
            if (settingStr != null)
            {
                List<string> pairs = settingStr.Split(';').Select(x => x.Trim()).ToList();

                foreach (string pair in pairs)
                {
                    List<string> pairList = pair.Split(',').Select(x=>x.Trim()).ToList();

                    if (pairList.Count > 1)
                    {
                        System.Windows.Controls.Control control = TranslateFieldToControl(pairList[0]);   

                        if (control != null)
                        {
                            result.Add(new KeyValuePair<System.Windows.Controls.Control, string>(control, pairList[1]));

                            control.LostKeyboardFocus += Control_LostKeyboardFocus;
                        }
                    }
                }
            }

            this.FieldFunctionList = result;
        }

        private void Control_LostKeyboardFocus(object sender, System.Windows.Input.KeyboardFocusChangedEventArgs e)
        {
            string functionName = this.FieldFunctionList.Where(x => x.Key == sender as System.Windows.Controls.Control).FirstOrDefault().Value;

            if (functionName != null)
            {
                string value = null;
                if (sender is TextBox)
                {
                    value = (sender as TextBox).Text;
                }
                else if(sender is ComboBox)
                {
                    value = (sender as ComboBox).Text;
                }

                if (!string.IsNullOrEmpty(value))
                {
                    bool fieldValid = Utils.ValidateField(functionName, value);

                    if (!fieldValid)
                    {
                        InfowareInformationWindow iiw = new InfowareInformationWindow(LanguageManager.GetTranslation(LanguageConstants.InputIsNotValidPleaseReEnter, "Input is invalid, please re-enter."),
                           LanguageManager.GetTranslation(LanguageConstants.InputIsNotValid, "Invalid Input"), WordDefaultIconType.Warning);
                        iiw.ShowDialog();
                        if (sender is TextBox)
                        {
                           (sender as TextBox).Text = string.Empty;
                        }
                        else if (sender is ComboBox)
                        { 
                            (sender as ComboBox).SelectedIndex = -1;
                            (sender as ComboBox).Text = string.Empty;
                        }
                    }
                }
            }
        }

        private System.Windows.Controls.Control TranslateFieldToControl(string fieldName)
        {
            switch (fieldName)
            {
                case "Recipient(s)":
                    return textBoxRecipients;
                case "Salutation":
                    return comboBoxSalutation;
                case "CC":
                    return textBoxCC;
                case "BCC":
                    return textBoxBCC;
                case "Our File Number":
                    return textBoxFileNumber;
                case "Their File Number":
                    return textBoxClientFileNumber;
                case "Delivery":
                    return comboBoxDelivery;
                case "Handling":
                    return comboBoxHandling;
                case "Attention":
                    return textBoxAttention;
                case "Office Location":
                    return comboBoxOfficeLocation;
                case "Re Line":
                    return textBoxReLine;
                case "Closing":
                    return comboBoxClosing;
                case "Enclosure(s)":
                    return comboBoxEnclosures;
                case "EnclosureNo":
                    return numericUpDownNumber;
            }
            return null;
        }

        private void PopulateDiaryDateComboBox()
        {
            List<KeyValuePair<DiaryDateOptions?, string>> itemSource = new List<KeyValuePair<DiaryDateOptions?, string>>()
            {
                new KeyValuePair<DiaryDateOptions?, string>(null, LanguageManager.GetTranslation(LanguageConstants.NoDiaryDate, "No Diary Date")),
                new KeyValuePair<DiaryDateOptions?, string>(DiaryDateOptions.LetterOnly, LanguageManager.GetTranslation(LanguageConstants.InsertInLetterOnly, "Insert in letter only")),
                new KeyValuePair<DiaryDateOptions?, string>(DiaryDateOptions.OutlookOnly, LanguageManager.GetTranslation(LanguageConstants.CreateOutlookTaskOnly, "Create Outlook Task only")),
                new KeyValuePair<DiaryDateOptions?, string>(DiaryDateOptions.LetterAndOutlook, LanguageManager.GetTranslation(LanguageConstants.InsertInLetterAndCreateOutlookTask, "Insert in letter and create Outlook Task")),
            };

            this.DiaryDateCB.ItemsSource = itemSource;
            this.DiaryDateCB.DisplayMemberPath = "Value";
            this.DiaryDateCB.SelectedValuePath = "Key";
            this.DiaryDateCB.SelectedIndex = 0;
        }

        private void UpdateDiaryDateVisibility()
        {
            if (SettingsManager.GetSettingAsBool("DiaryDateEnabled", "Letters", false))
            {
                this.diaryDateStackPanel.Visibility = Visibility.Visible;
            }
        }

        private void UpdateClosingStatements()
        {
            const char pipeSeparator = '|';

            string rawClosingStatements = SettingsManager.GetSetting("Closings", "Glossary", null, MLanguageUtil.ActiveDocumentLanguage);

            if (rawClosingStatements == null)
            {
                ClosingStatements = new List<string>()
                {
                    "Yours truly",
                    "Yours very truly",
                    "Sincerely",
                    "Regards",
                    "Best regards",
                    "Kindest regards"
                };

                comboBoxClosing.ItemsSource = ClosingStatements;
                return;
            }

            char separator;

            if (rawClosingStatements.Contains(pipeSeparator))
            {
                separator = pipeSeparator;
            }
            else
            {
                separator = ',';
            }

            ClosingStatements = SettingsManager.UnescapeCommaCharacter(rawClosingStatements.Split(separator)).Select(it => it.Trim()).ToList();
            comboBoxClosing.ItemsSource = ClosingStatements;
        }

        private void UpdateDefaultClosingIndex()
        {
            //try to get it from current author
            var author = AdminPanelWebApi.GetCurrentAuthor();
            if (!string.IsNullOrEmpty(author?.DefaultClosing))
                DefaultClosingIndex = ClosingStatements.IndexOf(author.DefaultClosing);
            else
                DefaultClosingIndex = -1;

            if (DefaultClosingIndex < 0)
                DefaultClosingIndex = SettingsManager.GetSettingAsInt("ClosingsDefaultIdx", "Glossary", languageId: MLanguageUtil.ActiveDocumentLanguage);

            if (DefaultClosingIndex > ClosingStatements.Count - 1)
            {
                DefaultClosingIndex = -1;
            }

            comboBoxClosing.SelectedIndex = DefaultClosingIndex;
        }

        private void UpdateClosingEndChar()
        {
            closingEndChar = SettingsManager.GetSetting("ClosingEndChar", "Glossary", null, MLanguageUtil.ActiveDocumentLanguage);

            if (closingEndChar == null)
            {
                closingEndChar = ",";
            }
        }

        private void UpdateIncludeFirmNameInClosing()
        {
            bool includeFirmNameInClosing = SettingsManager.GetSettingAsBool("ClosingIncludeFirmName", "Glossary", true);

            checkBoxIncludeFirmName.IsChecked = includeFirmNameInClosing;
        }

        private void LoadAnswers(bool fromDS = false)
        {
            PaneDTO letterDTO;
            if (fromDS)
            {
                letterDTO = TaskPaneUtil.GetDataSourceAnsweresDTO(TaskPaneUtil.LoadAnswers()).PaneDTO;
                (this.MainContentControl.Content as DataFillInnerUserControl)?.ApplyDataSourceValues(dataSourceValues?.CustomFields);
            }
            else
            {
                dataSourceValues = TaskPaneUtil.GetDataSourceAnsweresDTO();
                letterDTO = TaskPaneUtil.GetNewPaneDTO(dataSourceValues);
                letterDTO.IncludeFirmName = RegistryManager.GetIntRegistryValue(INCLUDE_FIRM_NAME_CLOSING_KEY) == 1;
                letterDTO.IncludePer = RegistryManager.GetIntRegistryValue(INCLUDE_PER_IN_CLOSING_KEY) == 1;
                letterDTO = TaskPaneUtil.LoadAnswers(letterDTO);
                if (dataSourceValues?.PaneDTO != null && TaskPaneUtil.AnswersFound)
                {
                    this.LoadDS.Visibility = Visibility.Visible;
                }
                else
                {
                    (this.MainContentControl.Content as DataFillInnerUserControl)?.ApplyDataSourceValues(dataSourceValues?.CustomFields);
                }
            }


            letterDTO.IncludeFirmName = RegistryManager.GetIntRegistryValue(INCLUDE_FIRM_NAME_CLOSING_KEY) == 1;
            letterDTO.IncludePer = RegistryManager.GetIntRegistryValue(INCLUDE_PER_IN_CLOSING_KEY) == 1;
            letterDTO.IncludeAssistantBlock = RegistryManager.GetIntRegistryValue(INCLUDE_ASSISTANT_BLOCK_KEY) != 0;
            letterDTO = TaskPaneUtil.LoadAnswers(letterDTO);
            if (letterDTO != null)
            {
                var letterAuthor = letterDTO.Author?.ToAuthorDTO();
                if (letterAuthor != null)
                {
                    Author = letterAuthor;

                    var letterAuthorFullName = letterAuthor?.FirstName + " " + letterAuthor?.LastName;
                    if (!string.IsNullOrWhiteSpace(letterAuthorFullName))
                    {
                        textBoxAuthor.Text = letterAuthorFullName;
                    }
                }

                var letterAssistant = letterDTO.Assistant?.ToAuthorDTO();
                if (letterAssistant != null)
                {
                    Assistant = letterAssistant;

                    var letterAssistantFullName = letterAssistant?.FirstName + " " + letterAssistant?.LastName;
                    if (!string.IsNullOrWhiteSpace(letterAssistantFullName))
                    {
                        textBoxAssistant.Text = letterAssistantFullName;
                    }
                }

                dateTimePickerDate.SelectedDate = letterDTO.Date;
                if (letterDTO.Recipients == null && letterDTO.AddedRecipients != null)
                {
                    List<string> recipientsArr = new List<string>();
                    CreateRecipientText(letterDTO.AddedRecipients, recipientsArr);
                    textBoxRecipients.Text = string.Join("\r\n\r\n", recipientsArr);
                }
                else
                {
                    textBoxRecipients.Text = letterDTO.Recipients;
                }
                AddedRecipients = letterDTO.AddedRecipients;
                textBoxCC.Text = letterDTO.CC;
                textBoxBCC.Text = letterDTO.BCC;
                textBoxFileNumber.Text = letterDTO.FileNumber;
                textBoxClientFileNumber.Text = letterDTO.ClientFileNumber;
                textBoxAttention.Text = letterDTO.Attention;
                textBoxReLine.Text = letterDTO.ReLine;
                numericUpDownNumber.Text = letterDTO.Number.ToString();
                checkBoxIncludeFirmName.IsChecked = letterDTO.IncludeFirmName;

                if (dateTimePickerDiaryDate.Visibility == Visibility.Visible)
                {
                    dateTimePickerDiaryDate.SelectedDate = letterDTO.DiaryDate;
                }

                checkBoxIncludePer.IsChecked = letterDTO.IncludePer;
                IncludeAssistantBlockCheckbox.IsChecked = letterDTO.IncludeAssistantBlock;
                Utils.SetComboBoxValue(comboBoxOfficeLocation, letterDTO.OfficeLocation);
                if (letterDTO.OfficeLocationId.HasValue)
                {
                    this.comboBoxOfficeLocation.SelectedValue = letterDTO.OfficeLocationId.Value;
                    this.previousOfficeLocationId = letterDTO.OfficeLocationId;
                }

                Utils.SetComboBoxValue(comboBoxSalutation, letterDTO.Salutation);
                Utils.SetComboBoxValue(comboBoxDelivery, letterDTO.Delivery);
                Utils.SetComboBoxValue(comboBoxHandling, letterDTO.Handling);
      
                Utils.SetComboBoxValue(comboBoxEnclosures, letterDTO.Enclosures);
                Utils.SetComboBoxValue(comboBoxClosing, letterDTO.Closing);
            }
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {

            MsoLanguageID languageID = MLanguageUtil.ActiveDocumentLanguage;
            var selectedAuthor = new AuthorWithPathsDTO();
            if (Author != null)
            {
                selectedAuthor = AdminPanelWebApi.GetAuthor(Author.Id, languageID);
            }

            var selectedAssistant = new AuthorWithPathsDTO();
            if (Assistant != null)
            {
                selectedAssistant = AdminPanelWebApi.GetAuthor(Assistant.Id, languageID);
            }

            int.TryParse(numericUpDownNumber.Text, out int number);
            ChangeEnclosureComboBox(number, comboBoxEnclosures.Text);
            bool insertDiaryDateInLetter = (this.DiaryDateCB.SelectedValue as DiaryDateOptions?) == DiaryDateOptions.LetterOnly || (this.DiaryDateCB.SelectedValue as DiaryDateOptions?) == DiaryDateOptions.LetterAndOutlook;

            var letterDTO = new PaneDTO
            {
                Author = selectedAuthor,
                Assistant = selectedAssistant,
                Date = dateTimePickerDate.SelectedDate ?? DateTime.Today,
                Recipients = textBoxRecipients.Text.Replace("\r\n", "\v"),
                Tos = AddedRecipients.ToArray(),
                Salutation = comboBoxSalutation.Text,
                CC = textBoxCC.Text.Replace("\r\n", "\v"),
                BCC = textBoxBCC.Text.Replace("\r\n", "\v"),
                FileNumber = textBoxFileNumber.Text,
                ClientFileNumber = textBoxClientFileNumber.Text,
                Delivery = comboBoxDelivery.SelectedValue is string ? comboBoxDelivery.SelectedValue as string : comboBoxDelivery.Text,
                Handling = comboBoxHandling.SelectedValue is string ? comboBoxHandling.SelectedValue as string : comboBoxHandling.Text,
                Attention = textBoxAttention.Text,
                //OfficeLocation = comboBoxOfficeLocation.Text,
                OfficeLocationId = comboBoxOfficeLocation.SelectedValue as int?,
                ReLine = textBoxReLine.Text,
                Closing = comboBoxClosing.Text,
                Enclosures = comboBoxEnclosures.SelectedValue is string ? comboBoxEnclosures.SelectedValue as string : comboBoxEnclosures.Text,
                Number = number,
                CompanyId = DocumentPropertyUtil.GetTemplateCompanyID(),
                AddedRecipients = AddedRecipients,
                IncludeContactInformationDeliverySetting = SettingsManager.GetSetting("IncludeContactInformationDelivery", "Letters", null, MLanguageUtil.ActiveDocumentLanguage),
                IncludeFirmName = checkBoxIncludeFirmName.IsChecked ?? false,
                IncludePer = checkBoxIncludePer.IsChecked ?? false,
                ClosingEndChar = closingEndChar,
                DiaryDate = insertDiaryDateInLetter ? dateTimePickerDiaryDate.SelectedDate : null,
                FileNumberTagSetting = SettingsManager.GetSetting("FileNoTag", "Letters", null, MLanguageUtil.ActiveDocumentLanguage),
                ClosingPerText = SettingsManager.GetSetting("ClosingPerText", "Letters", "Per:", MLanguageUtil.ActiveDocumentLanguage),
                HandlingAllCaps = SettingsManager.GetSettingAsBool("HandlingAllCaps", "Letters"),
                DeliveryAllCaps = SettingsManager.GetSettingAsBool("DeliveryAllCaps", "Letters"),
                AttentionTranslation = MLanguageUtil.GetResource(MLanguageResourceEnum.Attention),
                IncludeAssistantBlock = this.IncludeAssistantBlockCheckbox.Visibility == Visibility.Visible ? this.IncludeAssistantBlockCheckbox.IsChecked == true : (bool?)null
            };

            letterDTO = MLanguageUtil.ApplyDateSettings(letterDTO);

            if ((this.DiaryDateCB.SelectedValue as DiaryDateOptions?) == DiaryDateOptions.OutlookOnly || (this.DiaryDateCB.SelectedValue as DiaryDateOptions?) == DiaryDateOptions.LetterAndOutlook)
            {
                BaseUtils.CreateDiaryDateOutlookTask(letterDTO, dateTimePickerDiaryDate.SelectedDate);
            }

            PaneUtils.InsertGeneralTemplateDTO(ThisAddIn.Instance.Application.ActiveDocument, letterDTO, TemplateType.Letter, new PaneSettings()
            {
                HorizontalRecipientsColumnCount = SettingsManager.GetSettingAsInt("HorizontalRecipientsColumnCount", "Letters", 3),
                MinRecipientCountForHorizontal = SettingsManager.GetSettingAsInt("MinRecipientCountForHorizontal", "Letters", 3)
            });

            if (this.comboBoxOfficeLocation.SelectedValue as int? != null && previousOfficeLocationId != null && this.comboBoxOfficeLocation.SelectedValue as int? != previousOfficeLocationId)
            {
                new ExistingLetterhead().ChangeLetterhead((comboBoxOfficeLocation.SelectedValue as int?).Value);
            }
            previousOfficeLocationId = this.comboBoxOfficeLocation.SelectedValue as int?;

            TaskPaneUtil2.TryRestoreSelection(this);
            (MainContentControl.Content as DataFillInnerUserControl)?.OnUpdateButtonClick();
            TaskPaneUtil.SaveAnswers(letterDTO);
            RegistryManager.SaveToRegistry(INCLUDE_FIRM_NAME_CLOSING_KEY, letterDTO.IncludeFirmName ? 1 : 0);

            if (letterDTO.IncludeAssistantBlock.HasValue)
            {
                RegistryManager.SaveToRegistry(INCLUDE_ASSISTANT_BLOCK_KEY, letterDTO.IncludeAssistantBlock.Value ? 1 : 0);
            }

            RegistryManager.SaveToRegistry(INCLUDE_PER_IN_CLOSING_KEY, letterDTO.IncludePer ? 1 : 0);

            //save checkbox states to webapi
            try
            {
                AdminPanelWebApi.UpdateCustomSetting(Utils.GetProcessorId(), "LetterIncludePer", checkBoxIncludePer.IsChecked.ToString());
                AdminPanelWebApi.UpdateCustomSetting(Utils.GetProcessorId(), "LetterIncludeFirmName", checkBoxIncludeFirmName.IsChecked.ToString());
            }
            catch
            {
                //do nothing
            }
        }

        private void buttonFindAuthor_Click(object sender, EventArgs e)
        {
            SelectAuthor_WPF selectAuthor_WPF = new SelectAuthor_WPF();
            selectAuthor_WPF.ShowDialog();
            if (selectAuthor_WPF.ResultOk)
            {
                Author = selectAuthor_WPF.SelectedAuthor;
                textBoxAuthor.Text = Author.FirstName + " " + Author.LastName;
            }
        }

        private void buttonFindAssistant_Click(object sender, EventArgs e)
        {
            SelectAuthor_WPF selectAuthor_WPF = new SelectAuthor_WPF();
            selectAuthor_WPF.ShowDialog();
            if (selectAuthor_WPF.ResultOk)
            {
                Assistant = selectAuthor_WPF.SelectedAuthor;
                textBoxAssistant.Text = Assistant.FirstName + " " + Assistant.LastName;
            }
        }

        private void buttonFindRecipients_Click(object sender, EventArgs e)
        {
            suppressRemoveAddedRecipient = true;
            var importContacts_WPF = new ImportContacts_WPF(RecipientType.MultiRow, true, authorId: this.Author?.Id, oldContacts:AddedRecipients);
            importContacts_WPF.ShowDialog();

            var recipients = new List<string>();
            if (importContacts_WPF.ResultOk)
            {
                BaseUtils.RemoveContactsFromTextBoxes(importContacts_WPF.RemovedContacts, textBoxRecipients, textBoxAttention, AddedRecipients);
                CreateRecipientText(importContacts_WPF.SelectedContacts, recipients, importContacts_WPF.JobTitleVisibility, importContacts_WPF.CompanyNameVisibility, importContacts_WPF.DepartmentVisibility);
            }

            var recipientsStr = string.Join("\r\n\r\n", recipients);
            if (!string.IsNullOrEmpty(recipientsStr))
            {
                if (textBoxRecipients.Text.Trim().IsEmpty())
                {
                    textBoxRecipients.Text = recipientsStr;
                }
                else
                {
                    textBoxRecipients.Text = textBoxRecipients.Text + "\r\n\r\n" + recipientsStr;
                }
            }
        }

        public void CreateRecipientText(List<SelectedContact> selectedContacts, List<string> recipients, JobTitleVisibility jobTitleVisibility = JobTitleVisibility.NoJobTitle, bool companyNameVisibility = true, bool departmentVisibility = false)
        {
            foreach (var recipient in selectedContacts)
            {
                var grouppedLine = new List<string>();
                var fullName = new List<string>();

                bool departmentAfterCompany = SettingsManager.GetSettingAsBool("RecipientDepartmentAfterCompany", "Letters", false);

                if (!recipient.Prefix.IsEmpty())
                {
                    fullName.Add(recipient.Prefix);
                }

                if (!recipient.FirstName.IsEmpty())
                {
                    fullName.Add(recipient.FirstName);
                }

                if (!recipient.LastName.IsEmpty())
                {
                    fullName.Add(recipient.LastName);
                }
                if (!recipient.Suffix.IsEmpty())
                {
                    fullName.Add(recipient.Suffix);
                }

                string fullNameStr = string.Join(" ", fullName.ToArray());
                if (jobTitleVisibility == JobTitleVisibility.SameRow && !string.IsNullOrEmpty(recipient.JobTitle))
                {
                    fullNameStr += ", " + recipient.JobTitle;
                }

                if (!recipient.IsSelected && fullName.Count > 0)
                {
                    grouppedLine.Add(fullNameStr);
                }

                if (!recipient.IsSelected && !recipient.JobTitle.IsEmpty() && jobTitleVisibility == JobTitleVisibility.NextRow)
                {
                    grouppedLine.Add(recipient.JobTitle);
                }

                if (!recipient.IsSelected && !recipient.Department.IsEmpty() && departmentVisibility && !departmentAfterCompany)
                {
                    grouppedLine.Add(recipient.Department);
                }

                if (companyNameVisibility == true)
                {
                    var companies = recipient.Companies;
                    if (companies != null && companies.Count > 0)
                    {
                        var company = companies.OrderBy(x => x.IsDefault).FirstOrDefault();
                        if (company != null && !string.IsNullOrEmpty(company.Name))
                        {
                            grouppedLine.Add(company.Name);
                        }
                    }
                    else if (!string.IsNullOrWhiteSpace(recipient.CompanyName))
                    {
                        grouppedLine.Add(recipient.CompanyName);
                    }
                }

                if (!recipient.Department.IsEmpty() && departmentVisibility && departmentAfterCompany)
                {
                    grouppedLine.Add(recipient.Department);
                }

                var address = BaseUtils.GetAuthorAddress(recipient);
                if (address != null && !string.IsNullOrWhiteSpace(address.Address1))
                {
                    grouppedLine.Add(string.Join("\r\n", address.LinesFormat));
                }
                


                if (recipient.IsSelected)
                {
                    if (grouppedLine.Count > 0)
                    {
                        textBoxAttention.Text += BaseUtils.GetAttentionLine(recipient, jobTitleVisibility, departmentVisibility && !departmentAfterCompany) + "\n";
                    }
                    else
                    {
                        if (fullName.Count > 0)
                        {
                            grouppedLine.Add(fullNameStr);
                        }

                        if (!recipient.JobTitle.IsEmpty() && jobTitleVisibility == JobTitleVisibility.NextRow)
                        {
                            grouppedLine.Add(recipient.JobTitle);
                        }

                        if (!recipient.Department.IsEmpty() && departmentVisibility)
                        {
                            grouppedLine.Add(recipient.Department);
                        }
                    }
                }

                if (grouppedLine.Count > 0)
                {
                    recipients.Add(string.Join("\r\n", grouppedLine));
                    AddedRecipients.Add(recipient);
                }
            }
        }

        private void buttonFindCC_Click(object sender, EventArgs e)
        {
            ImportContacts_WPF importContacts_WPF = new ImportContacts_WPF(RecipientType.SingleRow, authorId: this.Author?.Id, oldContacts:AddedCCRecipients, useAddress: false);
            importContacts_WPF.ShowDialog();
            string ccs = "";
            string newLine = "";
            if (importContacts_WPF.ResultOk)
            {
                BaseUtils.RemoveContactsFromTextBoxes(importContacts_WPF.RemovedContacts, textBoxCC, includesAddress: false);
                foreach (var cc in importContacts_WPF.SelectedContacts)
                {
                    ccs += newLine + cc.FirstName + " " + cc.LastName + (importContacts_WPF.JobTitleVisibility == JobTitleVisibility.SameRow && !string.IsNullOrEmpty(cc.JobTitle) ? ", " + cc.JobTitle : "") +
                        (importContacts_WPF.CompanyNameVisibility == true && !string.IsNullOrEmpty(cc.CompanyName) ? ", " + cc.CompanyName : "");
                    newLine = "\r\n";
                    AddedCCRecipients.Add(cc);
                }
            }
            if (string.IsNullOrWhiteSpace(textBoxCC.Text))
            {
                textBoxCC.Text = ccs;
            }
            else
            {
                textBoxCC.Text = textBoxCC.Text.Trim() + "\r\n" + ccs;
            }
        }

        private void buttonFindBCC_Click(object sender, EventArgs e)
        {
            ImportContacts_WPF importContacts_WPF = new ImportContacts_WPF(RecipientType.SingleRow, authorId: this.Author?.Id, oldContacts:AddedBCCRecipients, useAddress: false);
            importContacts_WPF.ShowDialog();
            string ccs = "";
            string newLine = "";
            if (importContacts_WPF.ResultOk)
            {
                BaseUtils.RemoveContactsFromTextBoxes(importContacts_WPF.RemovedContacts, textBoxBCC, includesAddress:false);
                foreach (var cc in importContacts_WPF.SelectedContacts)
                {
                    ccs += newLine + cc.FirstName + " " + cc.LastName + (importContacts_WPF.JobTitleVisibility == JobTitleVisibility.SameRow && !string.IsNullOrEmpty(cc.JobTitle) ? ", " + cc.JobTitle : "") +
                        (importContacts_WPF.CompanyNameVisibility == true && !string.IsNullOrEmpty(cc.CompanyName) ? ", " + cc.CompanyName : "");
                    newLine = "\r\n";
                    AddedBCCRecipients.Add(cc);
                }
            }
            if (string.IsNullOrWhiteSpace(textBoxBCC.Text))
            {
                textBoxBCC.Text = ccs;
            }
            else
            {
                textBoxBCC.Text = textBoxBCC.Text.Trim() + "\r\n" + ccs;
            }
        }

        private void buttonToday_Click(object sender, EventArgs e)
        {
            dateTimePickerDate.SelectedDate = DateTime.Today;
        }

        private void numericUpDownNumber_PreviewTextInput(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            Regex regex = new Regex("^[0-9]*$");
            if (!regex.IsMatch(e.Text))
            {
                e.Handled = true;
            }
        }

        private void ChangeEnclosureComboBox(int number, string enclosureOption)
        {
            var enclosures = GetEnclosureList();
            if ((number > 0 && !string.IsNullOrEmpty(enclosureOption)) & (enclosures != null && enclosures.Count > 0))
            {
                foreach (var enclosure in enclosures)
                {
                    if (number == 1 && enclosure.PluralValue == enclosureOption)
                    {
                        comboBoxEnclosures.SelectedValue = enclosure.SingularValue;
                        return;
                    }
                    else if (number > 1 && enclosure.SingularValue == enclosureOption)
                    {
                        comboBoxEnclosures.SelectedValue = enclosure.PluralValue;
                        return;
                    }
                }
            }
        }

        private void TextBoxRecipients_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!suppressRemoveAddedRecipient)
            {
                BaseUtils.RemoveRecipientsNotInTextBox(AddedRecipients, textBoxRecipients.Text, textBoxAttention.Text);
            }

            var firstRecipientsRow = BaseUtils.GetRecipientTextFirstRow(textBoxAttention.Text.Trim().Length > 0 ? textBoxAttention.Text.Replace("\r\n", "\v") : textBoxRecipients.Text.Replace("\r\n", "\v"));
            if (!string.IsNullOrEmpty(firstRecipientsRow))
            {
                if (Salutations != null && Salutations.Count > 0)
                {
                    var firstRecipientFirstAndLastName = BaseUtils.GetFirstRecipientFirstAndLastName(firstRecipientsRow);
                    if (firstRecipientFirstAndLastName != null)
                    {
                        var salutationsList = new List<string>();
                        if (!string.IsNullOrEmpty(firstRecipientFirstAndLastName.FirstName))
                        {
                            salutationsList.Add(firstRecipientFirstAndLastName.FirstName);
                        }

                        var salutationsToExclude = GetSalutationsToExclude();
						List<string> salutations;
                        if (!firstRecipientFirstAndLastName.Title.IsEmpty())
                        {
                            salutations = new List<string>() { firstRecipientFirstAndLastName.Title };
                        }
                        else
                        {
                            salutations = Salutations;
                        }

                        foreach (var defaultSalutation in salutations)
                        {
                            if (salutationsToExclude.Count > 0 && salutationsToExclude.Contains(defaultSalutation))
                            {
                                salutationsList.Add(defaultSalutation);
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(firstRecipientFirstAndLastName.LastName))
                                {
                                    var lastNameParts = firstRecipientFirstAndLastName.LastName.Split(new char[] { ' ', '\t' }, StringSplitOptions.RemoveEmptyEntries);

                                    if (lastNameParts != null && lastNameParts.Length > 0)
                                    {
                                        salutationsList.Add(defaultSalutation + " " + lastNameParts.LastOrDefault());

                                        if (lastNameParts.Length > 1)
                                        {
                                            salutationsList.Add(defaultSalutation + " " + firstRecipientFirstAndLastName.LastName);
                                        }
                                    }
                                }

                                var fullName = (firstRecipientFirstAndLastName.FirstName + " " + firstRecipientFirstAndLastName.LastName).Trim();
                                if (!string.IsNullOrEmpty(fullName))
                                {
                                    salutationsList.Add(defaultSalutation + " " + fullName);
                                }
                            }
                        }
                        comboBoxSalutation.ItemsSource = salutationsList;
                    }
                }
            }
            else
            {
                comboBoxSalutation.ItemsSource = Salutations;
            }
        }

        private List<Enclosure> GetEnclosureList()
        {
            var enclosureSettings = SettingsManager.GetSetting("Enclosures", "Letters", null, MLanguageUtil.ActiveDocumentLanguage);
            if (!string.IsNullOrEmpty(enclosureSettings))
            {
                return BaseUtils.ParseEnclosureSettings(enclosureSettings);
            }
            return BaseUtils.ParseEnclosureSettings(string.Join(";", DefaultEnclosureSettings));
        }

        private List<string> GetSalutations()
        {
            var salutations = new List<string>();
            var salutationSetting = SettingsManager.GetSetting("Salutations", "Letters", null, MLanguageUtil.ActiveDocumentLanguage);
            if (!string.IsNullOrEmpty(salutationSetting))
            {
                salutations.AddRange(SettingsManager.UnescapeCommaCharacter(salutationSetting.Split(',')).Select(x => x.Trim()).ToList());
            }
            else
            {
                salutations.AddRange(DefaultSalutations);
            }
            return salutations;
        }

        private List<string> GetSalutationsToExclude()
        {
            var excludedSalutations = new List<string>();
            var salutationToExcludeSetting = SettingsManager.GetSetting("SalutationsToExclude", "Letters", null, MLanguageUtil.ActiveDocumentLanguage);
            if (!string.IsNullOrEmpty(salutationToExcludeSetting))
            {
                excludedSalutations.AddRange(SettingsManager.UnescapeCommaCharacter(salutationToExcludeSetting.Split(',')).Select(x => x.Trim()).ToList());
            }
            else
            {
                excludedSalutations.AddRange(DefaultExcludedSalutations);
            }
            return excludedSalutations;
        }

        private void buttonRemoveDiaryDate_Click(object sender, RoutedEventArgs e)
        {
            this.dateTimePickerDiaryDate.SelectedDate = null;
        }

        private void LoadDS_Click(object sender, RoutedEventArgs e)
        {
            LoadAnswers(true);
        }

        public void OKHandler()
        {
            buttonOK_Click(null, null);
        }

        private void textBoxBCC_TextChanged(object sender, TextChangedEventArgs e)
        {
            BaseUtils.RemoveRecipientsNotInTextBox(AddedBCCRecipients, textBoxBCC.Text);
        }

        private void textBoxCC_TextChanged(object sender, TextChangedEventArgs e)
        {
            BaseUtils.RemoveRecipientsNotInTextBox(AddedCCRecipients, textBoxCC.Text);
        }
    }
    public enum DiaryDateOptions
    {
        LetterOnly = 0,
        OutlookOnly = 1,
        LetterAndOutlook = 2
    }
}