﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using InfowareVSTO.Common;
using InfowareVSTO.Common.DTOs;
using InfowareVSTO.Common.General;
using InfowareVSTO.Common.Language;
using InfowareVSTO.Common.Models;
using InfowareVSTO.Common.Models.ViewModels;
using InfowareVSTO.Letterhead;
using InfowareVSTO.MultiLanguage;
using InfowareVSTO.Util;
using InfowareVSTO.VSTOAuthor;
using InfowareVSTO.Windows;
using InfowareVSTO.Windows.WordDa;
using Microsoft.Office.Core;
using Microsoft.Office.Interop.Word;
using Microsoft.Office.Tools.Word;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ContentControl = Microsoft.Office.Interop.Word.ContentControl;

namespace InfowareVSTO.TaskPaneControls.WordDa
{
    /// <summary>
    /// Interaction logic for DataFillUserControl.xaml
    /// </summary>
    public partial class DataFillUserControl : UserControl, IRefreshData
    {
        public DataSource.DataSourceValues dataSourceValues;
        private List<SelectedContact> AddedRecipients { get; set; }
        private List<SelectedContact> AddedCCRecipients { get; set; }
        private List<SelectedContact> AddedBCCRecipients { get; set; }
        private AuthorDTO Author { get; set; }
        private AuthorDTO Assistant { get; set; }
        private bool isCustomTemplate;
        private int? previousOfficeLocationId;
        private bool suppressRemoveAddedRecipient;
        private DataFillAnswers LoadedAnswers { get; set; }
        private static DataFillAnswers RetainedAnswers { get; set; }
        private List<string> DefaultSalutations { get; set; }
        private List<string> DefaultExcludedSalutations { get; set; }
        private List<string> Salutations { get; set; }
        public List<string> ClosingStatements { get; private set; }
        public int DefaultClosingIndex { get; private set; }
        private string closingEndChar;
        private List<string> DefaultEnclosureSettings { get; set; }
        public List<KeyValuePair<System.Windows.Controls.Control, string>> FieldFunctionList { get; private set; }

        public int ControlsFound
        {
            get
            {
                return (MainContentControl.Content as DataFillInnerUserControl)?.ControlsFound ?? 0;
            }
        }

        public bool AnyFieldFound { get; set; }

        public DataFillUserControl(bool isCustomTemplate = false, bool includeInformLawyerTag = false)
        {
            InitializeComponent();
            var optionsDialog = new DataFillOptionsDialog();
            this.MainContentControl.Content = new DataFillInnerUserControl(true, optionsDialog.UsePrompt, optionsDialog.UseChevrons, optionsDialog.UseSquareBrackets, includeInformLawyerTag);
            this.isCustomTemplate = isCustomTemplate;
            if (isCustomTemplate)
            {
                this.CustomRefreshData.Visibility = Visibility.Visible;
                InitRefreshData();
            }

            dataSourceValues = TaskPaneUtil.GetDataSourceAnsweresDTO();
            if (dataSourceValues?.CustomFields != null)
            {
                this.LoadDS.Visibility = Visibility.Visible;
            }

            if (RetainedAnswers != null)
            {
                DropRetainedAnswers.Visibility = Visibility.Visible;
                (MainContentControl.Content as DataFillInnerUserControl)?.ApplyValues(RetainedAnswers);
            }
        }

        private void InitRefreshData()
        {
            Utils.UpdateCurrentCultureShortDateFormat();
            dateTimePickerDate.SelectedDate = DateTime.Today;
            var author = AdminPanelWebApi.GetCurrentAuthor();
            if (author != null)
            {
                Author = author.ToAuthorDTO();
                textBoxAuthor.Text = Author?.FirstName + " " + Author?.LastName;
            }

            int assistantId = AdminPanelWebApi.GetCurrentAsistantId();
            if (assistantId > 0)
            {
                var assistant = AdminPanelWebApi.GetAuthor(assistantId);
                if (assistant != null)
                {
                    Assistant = assistant.ToAuthorDTO();
                    textBoxAssistant.Text = Assistant?.FirstName + " " + Assistant?.LastName;
                }
            }
            comboBoxOfficeLocation.ItemsSource = Utils.GetLocationsForCombobox();

            int defaultLocationId = -1;
            var currentAuthorSettings = ManageUsers.CurrentAuthorSettings;
            if (currentAuthorSettings.LocationId.HasValue)
            {
                defaultLocationId = currentAuthorSettings.LocationId.Value;
            }
            else
            {
                int? companyId = currentAuthorSettings.CompanyId ?? DocumentPropertyUtil.GetTemplateCompanyID();

                if (companyId.HasValue)
                {
                    defaultLocationId = AdminPanelWebApi.GetCompanyDetails(companyId.Value).Id;
                }
            }

            if (defaultLocationId >= 0)
            {
                comboBoxOfficeLocation.SelectedValue = defaultLocationId;
            }
            AddedRecipients = new List<SelectedContact>();
            AddedCCRecipients = new List<SelectedContact>();
            AddedBCCRecipients = new List<SelectedContact>();

            SettingsManager.PopulateComboboxWithDeliveryOptions(this.comboBoxDelivery);
            SettingsManager.PopulateComboboxWithHandlingOptions(this.comboBoxHandling);

            UpdateClosingStatements();
            UpdateDefaultClosingIndex();
            UpdateClosingEndChar();

            DefaultSalutations = new List<string> { "Counsel", "Mr.", "Ms.", "Mrs.", "Mr/Ms.", "Mr/Mrs.", "Mx.", "Madame", "Mesdames", "Sir", "Sirs", "Sir/Madame", "Sirs/Mesdames" };
            DefaultExcludedSalutations = new List<string> { "Counsel", "Sir", "Madame", "Sir/Madame", "Sirs/Mesdames", "Sirs", "Mesdames" };

            Salutations = GetSalutations();
            comboBoxSalutation.ItemsSource = Salutations;

            DefaultEnclosureSettings = new List<string> { "Encl,Encls", "Attch,Attchs" };

            var enclosures = GetEnclosureList();
            if (enclosures != null)
            {
                var enclosureOptions = BaseUtils.GetEnclosureOptions(enclosures);
                if (enclosureOptions != null && enclosureOptions.Count > 0)
                {
                    comboBoxEnclosures.ItemsSource = enclosureOptions;
                    comboBoxEnclosures.DisplayMemberPath = "Key";
                    comboBoxEnclosures.SelectedValuePath = "Value";
                    comboBoxEnclosures.SelectedIndex = 0;
                }
            }

            OrderFields();
            LoadAnswers();
        }

        private void OrderFields()
        {
            string json = SettingsManager.GetSetting("TaskPaneFieldOrder", "Letters", null);
            TaskpaneFieldOrderer tfo = new TaskpaneFieldOrderer();
            tfo.OrderFields(json, this.CustomRefreshData);
        }

        private void InitFieldValidation()
        {
            string settingStr = SettingsManager.GetSetting("FieldsRequiringValidation", "Letters");
            List<KeyValuePair<System.Windows.Controls.Control, string>> result = new List<KeyValuePair<System.Windows.Controls.Control, string>>();
            if (settingStr != null)
            {
                List<string> pairs = settingStr.Split(';').Select(x => x.Trim()).ToList();

                foreach (string pair in pairs)
                {
                    List<string> pairList = pair.Split(',').Select(x => x.Trim()).ToList();

                    if (pairList.Count > 1)
                    {
                        System.Windows.Controls.Control control = TranslateFieldToControl(pairList[0]);

                        if (control != null)
                        {
                            result.Add(new KeyValuePair<System.Windows.Controls.Control, string>(control, pairList[1]));

                            control.LostKeyboardFocus += Control_LostKeyboardFocus;
                        }
                    }
                }
            }

            this.FieldFunctionList = result;
        }

        private void Control_LostKeyboardFocus(object sender, System.Windows.Input.KeyboardFocusChangedEventArgs e)
        {
            string functionName = this.FieldFunctionList.Where(x => x.Key == sender as System.Windows.Controls.Control).FirstOrDefault().Value;

            if (functionName != null)
            {
                string value = null;
                if (sender is TextBox)
                {
                    value = (sender as TextBox).Text;
                }
                else if (sender is ComboBox)
                {
                    value = (sender as ComboBox).Text;
                }

                if (!string.IsNullOrEmpty(value))
                {
                    bool fieldValid = Utils.ValidateField(functionName, value);

                    if (!fieldValid)
                    {
                        InfowareInformationWindow iiw = new InfowareInformationWindow(LanguageManager.GetTranslation(LanguageConstants.InputIsNotValidPleaseReEnter, "Input is invalid, please re-enter."),
                           LanguageManager.GetTranslation(LanguageConstants.InputIsNotValid, "Invalid Input"), WordDefaultIconType.Warning);
                        iiw.ShowDialog();
                        if (sender is TextBox)
                        {
                            (sender as TextBox).Text = string.Empty;
                        }
                        else if (sender is ComboBox)
                        {
                            (sender as ComboBox).SelectedIndex = -1;
                            (sender as ComboBox).Text = string.Empty;
                        }
                    }
                }
            }
        }

        private System.Windows.Controls.Control TranslateFieldToControl(string fieldName)
        {
            switch (fieldName)
            {
                case "Recipient(s)":
                    return textBoxRecipients;
                case "Salutation":
                    return comboBoxSalutation;
                case "CC":
                    return textBoxCC;
                case "BCC":
                    return textBoxBCC;
                case "Our File Number":
                    return textBoxFileNumber;
                case "Their File Number":
                    return textBoxClientFileNumber;
                case "Delivery":
                    return comboBoxDelivery;
                case "Handling":
                    return comboBoxHandling;
                case "Office Location":
                    return comboBoxOfficeLocation;
                case "Re Line":
                    return textBoxReLine;
                case "Closing":
                    return comboBoxClosing;
                case "Enclosure(s)":
                    return comboBoxEnclosures;
                case "EnclosureNo":
                    return numericUpDownNumber;
            }
            return null;
        }
        internal void PopulateInformLawyerFields()
        {
            throw new NotImplementedException();
        }

        internal void FillTitleOfDocument(string formName)
        {
            (MainContentControl.Content as DataFillInnerUserControl)?.FillTitleOfDocument(formName);
        }

        private void UpdateClosingStatements()
        {
            const char pipeSeparator = '|';

            string rawClosingStatements = SettingsManager.GetSetting("Closings", "Glossary", null, MLanguageUtil.ActiveDocumentLanguage);

            if (rawClosingStatements == null)
            {
                ClosingStatements = new List<string>()
                {
                    "Yours truly",
                    "Yours very truly",
                    "Sincerely",
                    "Regards",
                    "Best regards",
                    "Kindest regards"
                };

                comboBoxClosing.ItemsSource = ClosingStatements;
                return;
            }

            char separator;

            if (rawClosingStatements.Contains(pipeSeparator))
            {
                separator = pipeSeparator;
            }
            else
            {
                separator = ',';
            }

            ClosingStatements = SettingsManager.UnescapeCommaCharacter(rawClosingStatements.Split(separator)).Select(it => it.Trim()).ToList();
            comboBoxClosing.ItemsSource = ClosingStatements;
        }

        private void UpdateDefaultClosingIndex()
        {
            //try to get it from current author
            var author = AdminPanelWebApi.GetCurrentAuthor();
            if (!string.IsNullOrEmpty(author?.DefaultClosing))
                DefaultClosingIndex = ClosingStatements.IndexOf(author.DefaultClosing);
            else
                DefaultClosingIndex = -1;

            if (DefaultClosingIndex < 0)
                DefaultClosingIndex = SettingsManager.GetSettingAsInt("ClosingsDefaultIdx", "Glossary", languageId: MLanguageUtil.ActiveDocumentLanguage);

            if (DefaultClosingIndex > ClosingStatements.Count - 1)
            {
                DefaultClosingIndex = -1;
            }

            comboBoxClosing.SelectedIndex = DefaultClosingIndex;
        }

        private void UpdateClosingEndChar()
        {
            closingEndChar = SettingsManager.GetSetting("ClosingEndChar", "Glossary", null, MLanguageUtil.ActiveDocumentLanguage);

            if (closingEndChar == null)
            {
                closingEndChar = ",";
            }
        }


        public void LoadAnswers(PaneDTO paneDTO = null, bool fromDS = false)
        {
            if (paneDTO == null)
            {
                if (fromDS)
                {
                    paneDTO = TaskPaneUtil.GetDataSourceAnsweresDTO(TaskPaneUtil.LoadAnswers()).PaneDTO;
                }
                else
                {
                    dataSourceValues = TaskPaneUtil.GetDataSourceAnsweresDTO();
                    paneDTO = TaskPaneUtil.GetNewPaneDTO(dataSourceValues);
                    paneDTO = TaskPaneUtil.LoadAnswers(paneDTO);
                    if (dataSourceValues?.PaneDTO != null && TaskPaneUtil.AnswersFound)
                    {
                        this.LoadDS.Visibility = System.Windows.Visibility.Visible;
                    }
                }
            }

            if (paneDTO != null)
            {
                dateTimePickerDate.SelectedDate = paneDTO.Date;
                if (paneDTO.Recipients == null && paneDTO.AddedRecipients != null)
                {
                    List<string> recipientsArr = new List<string>();
                    CreateRecipientText(paneDTO.AddedRecipients, recipientsArr);
                    textBoxRecipients.Text = string.Join("\r\n\r\n", recipientsArr);
                }
                else
                {
                    textBoxRecipients.Text = paneDTO.Recipients;
                }
                AddedRecipients = paneDTO.AddedRecipients;
                textBoxCC.Text = paneDTO.CC;
                textBoxFileNumber.Text = paneDTO.FileNumber;
                textBoxClientFileNumber.Text = paneDTO.ClientFileNumber;
                textBoxBCC.Text = paneDTO.BCC;
                textBoxReLine.Text = paneDTO.ReLine;
                numericUpDownNumber.Text = paneDTO.Number.ToString();
                var paneAuthor = paneDTO.Author?.ToAuthorDTO();
                if (paneAuthor != null)
                {
                    Author = paneAuthor;

                    var paneAuthorFullName = paneAuthor?.FirstName + " " + paneAuthor?.LastName;
                    if (!string.IsNullOrWhiteSpace(paneAuthorFullName))
                    {
                        textBoxAuthor.Text = paneAuthorFullName;
                    }
                }

                var paneAssistant = paneDTO.Assistant?.ToAuthorDTO();
                if (paneAssistant != null)
                {
                    Assistant = paneAssistant;

                    var paneAssistantFullName = paneAssistant?.FirstName + " " + paneAssistant?.LastName;
                    if (!string.IsNullOrWhiteSpace(paneAssistantFullName))
                    {
                        textBoxAssistant.Text = paneAssistantFullName;
                    }
                }
                Utils.SetComboBoxValue(comboBoxOfficeLocation, paneDTO.OfficeLocation);
                if (paneDTO.OfficeLocationId.HasValue)
                {
                    this.comboBoxOfficeLocation.SelectedValue = paneDTO.OfficeLocationId.Value;
                    this.previousOfficeLocationId = paneDTO.OfficeLocationId;
                }
         
                Utils.SetComboBoxValue(comboBoxDelivery, paneDTO.Delivery);
                Utils.SetComboBoxValue(comboBoxHandling, paneDTO.Handling);
                Utils.SetComboBoxValue(comboBoxSalutation, paneDTO.Salutation);
                Utils.SetComboBoxValue(comboBoxClosing, paneDTO.Closing);
                Utils.SetComboBoxValue(comboBoxEnclosures, paneDTO.Enclosures);
            }
        }

        private void RefreshDataOK()
        {
            int.TryParse(numericUpDownNumber.Text, out int number);
            ChangeEnclosureComboBox(number, comboBoxEnclosures.Text);
            var paneDTO = new PaneDTO
            {
                Type = TemplateType.Custom,
                CompanyId = DocumentPropertyUtil.GetTemplateCompanyID(),
                Date = dateTimePickerDate.SelectedDate ?? DateTime.Today,
                OfficeLocationId = comboBoxOfficeLocation.SelectedValue as int?,
                Recipients = textBoxRecipients.Text.Replace("\r\n", "\v"),
                Tos = AddedRecipients.ToArray(),
                FileNumber = textBoxFileNumber.Text,
                Salutation = comboBoxSalutation.Text,
                Closing = comboBoxClosing.Text,
                ClosingEndChar = closingEndChar,
                Delivery = comboBoxDelivery.SelectedValue is string ? comboBoxDelivery.SelectedValue as string : comboBoxDelivery.Text,
                Handling = comboBoxHandling.SelectedValue is string ? comboBoxHandling.SelectedValue as string : comboBoxHandling.Text,
                ClientFileNumber = textBoxClientFileNumber.Text,
                FileNumberTagSetting = SettingsManager.GetSetting("FileNoTag", "Letters", null, MLanguageUtil.ActiveDocumentLanguage),
                ReLine = textBoxReLine.Text,
                Enclosures = comboBoxEnclosures.SelectedValue is string ? comboBoxEnclosures.SelectedValue as string : comboBoxEnclosures.Text,
                Number = number,
                CC = textBoxCC.Text.Replace("\r\n", "\v"),
                BCC = textBoxBCC.Text.Replace("\r\n", "\v"),
                AddedRecipients = AddedRecipients
            };
            MsoLanguageID languageID = MLanguageUtil.ActiveDocumentLanguage;
            AuthorWithPathsDTO selectedAuthor;
            AuthorWithPathsDTO selectedAssistant;
            if (Author == null)
                selectedAuthor = new AuthorWithPathsDTO();
            else
                selectedAuthor = AdminPanelWebApi.GetAuthor(Author.Id, languageID);
            if (Assistant == null)
                selectedAssistant = new AuthorWithPathsDTO();
            else
                selectedAssistant = AdminPanelWebApi.GetAuthor(Assistant.Id, languageID);

            paneDTO.Author = selectedAuthor;
            //letterDTO.Author.FirstName = textBoxAuthor.Text;
            paneDTO.Assistant = selectedAssistant;


            paneDTO = MLanguageUtil.ApplyDateSettings(paneDTO);
            PaneUtils.InsertGeneralTemplateDTO(ThisAddIn.Instance.Application.ActiveDocument, paneDTO, TemplateType.Custom, new PaneSettings()
            {
                HorizontalRecipientsColumnCount = SettingsManager.GetSettingAsInt("HorizontalRecipientsColumnCount", "Letters", 3),
                MinRecipientCountForHorizontal = SettingsManager.GetSettingAsInt("MinRecipientCountForHorizontal", "Letters", 3)
            });
            TaskPaneUtil.SaveAnswers(paneDTO);
            if (this.comboBoxOfficeLocation.SelectedValue as int? != null && previousOfficeLocationId != null && this.comboBoxOfficeLocation.SelectedValue as int? != previousOfficeLocationId)
            {
                new ExistingLetterhead().ChangeLetterhead((comboBoxOfficeLocation.SelectedValue as int?).Value);
            }
        }

        private void buttonFindRecipients_Click(object sender, EventArgs e)
        {
            suppressRemoveAddedRecipient = true;
            var importContacts_WPF = new ImportContacts_WPF(RecipientType.MultiRow, false, authorId: this.Author?.Id, oldContacts: AddedRecipients);
            importContacts_WPF.ShowDialog();

            var recipients = new List<string>();
            if (importContacts_WPF.ResultOk)
            {
                BaseUtils.RemoveContactsFromTextBoxes(importContacts_WPF.RemovedContacts, textBoxRecipients);
                CreateRecipientText(importContacts_WPF.SelectedContacts, recipients, importContacts_WPF.JobTitleVisibility, importContacts_WPF.CompanyNameVisibility, importContacts_WPF.DepartmentVisibility);
            }

            var recipientsStr = string.Join("\r\n\r\n", recipients);
            if (!string.IsNullOrEmpty(recipientsStr))
            {
                if (textBoxRecipients.Text.Trim().IsEmpty())
                {
                    textBoxRecipients.Text = recipientsStr;
                }
                else
                {
                    textBoxRecipients.Text = textBoxRecipients.Text + "\r\n\r\n" + recipientsStr;
                }
            }
        }
        private void buttonToday_Click(object sender, EventArgs e)
        {
            dateTimePickerDate.SelectedDate = DateTime.Today;
        }

        private void buttonFindAuthor_Click(object sender, EventArgs e)
        {
            SelectAuthor_WPF selectAuthor_WPF = new SelectAuthor_WPF();
            selectAuthor_WPF.ShowDialog();
            if (selectAuthor_WPF.ResultOk)
            {
                Author = selectAuthor_WPF.SelectedAuthor;
                textBoxAuthor.Text = Author.FirstName + " " + Author.LastName;
            }
        }

        private void buttonFindAssistant_Click(object sender, EventArgs e)
        {
            SelectAuthor_WPF selectAuthor_WPF = new SelectAuthor_WPF();
            selectAuthor_WPF.ShowDialog();
            if (selectAuthor_WPF.ResultOk)
            {
                Assistant = selectAuthor_WPF.SelectedAuthor;
                textBoxAssistant.Text = Assistant.FirstName + " " + Assistant.LastName;
            }
        }
        private void CreateRecipientText(List<SelectedContact> selectedContacts, List<string> recipients, JobTitleVisibility jobTitleVisibility = JobTitleVisibility.NoJobTitle, bool companyNameVisibility = true, bool departmentVisibility = false)
        {
            foreach (var recipient in selectedContacts)
            {
                var grouppedLine = new List<string>();
                var fullName = new List<string>();

                bool departmentAfterCompany = SettingsManager.GetSettingAsBool("RecipientDepartmentAfterCompany", "Letters", false);

                if (!recipient.Prefix.IsEmpty())
                {
                    fullName.Add(recipient.Prefix);
                }

                if (!recipient.FirstName.IsEmpty())
                {
                    fullName.Add(recipient.FirstName);
                }

                if (!recipient.LastName.IsEmpty())
                {
                    fullName.Add(recipient.LastName);
                }
                if (!recipient.Suffix.IsEmpty())
                {
                    fullName.Add(recipient.Suffix);
                }

                string fullNameStr = string.Join(" ", fullName.ToArray());
                if (jobTitleVisibility == JobTitleVisibility.SameRow && !string.IsNullOrEmpty(recipient.JobTitle))
                {
                    fullNameStr += ", " + recipient.JobTitle;
                }

                if (fullName.Count > 0)
                {
                    grouppedLine.Add(fullNameStr);
                }

                if (!recipient.JobTitle.IsEmpty() && jobTitleVisibility == JobTitleVisibility.NextRow)
                {
                    grouppedLine.Add(recipient.JobTitle);
                }

                if (!recipient.Department.IsEmpty() && departmentVisibility && !departmentAfterCompany)
                {
                    grouppedLine.Add(recipient.Department);
                }

                if (companyNameVisibility == true)
                {
                    var companies = recipient.Companies;
                    if (companies != null && companies.Count > 0)
                    {
                        var company = companies.OrderBy(x => x.IsDefault).FirstOrDefault();
                        if (company != null && !string.IsNullOrEmpty(company.Name))
                        {
                            grouppedLine.Add(company.Name);
                        }
                    }
                    else if (!string.IsNullOrWhiteSpace(recipient.CompanyName))
                    {
                        grouppedLine.Add(recipient.CompanyName);
                    }
                }

                if (!recipient.Department.IsEmpty() && departmentVisibility && departmentAfterCompany)
                {
                    grouppedLine.Add(recipient.Department);
                }

                var address = BaseUtils.GetAuthorAddress(recipient);
                if (address != null && !string.IsNullOrWhiteSpace(address.Address1))
                {
                    grouppedLine.Add(string.Join("\r\n", address.LinesFormat));
                }

                if (grouppedLine.Count > 0)
                {
                    recipients.Add(string.Join("\r\n", grouppedLine));
                    AddedRecipients.Add(recipient);
                }
            }
        }

        private void buttonFindCC_Click(object sender, EventArgs e)
        {
            ImportContacts_WPF importContacts_WPF = new ImportContacts_WPF(RecipientType.SingleRow, authorId: this.Author?.Id, oldContacts: AddedCCRecipients, useAddress: false);
            importContacts_WPF.ShowDialog();
            string ccs = "";
            string newLine = "";
            if (importContacts_WPF.ResultOk)
            {
                BaseUtils.RemoveContactsFromTextBoxes(importContacts_WPF.RemovedContacts, textBoxCC, includesAddress: false);
                foreach (var cc in importContacts_WPF.SelectedContacts)
                {
                    ccs += newLine + cc.FirstName + " " + cc.LastName + (importContacts_WPF.JobTitleVisibility == JobTitleVisibility.SameRow && !string.IsNullOrEmpty(cc.JobTitle) ? ", " + cc.JobTitle : "") +
                    (importContacts_WPF.CompanyNameVisibility == true && !string.IsNullOrEmpty(cc.CompanyName) ? ", " + cc.CompanyName : ""); newLine = "\r\n";
                    AddedCCRecipients.Add(cc);
                }
            }
            if (string.IsNullOrWhiteSpace(textBoxCC.Text))
            {
                textBoxCC.Text = ccs;
            }
            else
            {
                textBoxCC.Text = textBoxCC.Text.Trim() + "\r\n" + ccs;
            }
        }

        private void buttonFindBCC_Click(object sender, EventArgs e)
        {
            ImportContacts_WPF importContacts_WPF = new ImportContacts_WPF(RecipientType.SingleRow, authorId: this.Author?.Id, oldContacts: AddedBCCRecipients, useAddress: false);
            importContacts_WPF.ShowDialog();
            string ccs = "";
            string newLine = "";
            if (importContacts_WPF.ResultOk)
            {
                BaseUtils.RemoveContactsFromTextBoxes(importContacts_WPF.RemovedContacts, textBoxBCC, includesAddress: false);
                foreach (var cc in importContacts_WPF.SelectedContacts)
                {
                    ccs += newLine + cc.FirstName + " " + cc.LastName + (importContacts_WPF.JobTitleVisibility == JobTitleVisibility.SameRow && !string.IsNullOrEmpty(cc.JobTitle) ? ", " + cc.JobTitle : "") +
                    (importContacts_WPF.CompanyNameVisibility == true && !string.IsNullOrEmpty(cc.CompanyName) ? ", " + cc.CompanyName : ""); newLine = "\r\n";
                    AddedBCCRecipients.Add(cc);
                }
            }
            if (string.IsNullOrWhiteSpace(textBoxBCC.Text))
            {
                textBoxBCC.Text = ccs;
            }
            else
            {
                textBoxBCC.Text = textBoxBCC.Text.Trim() + "\r\n" + ccs;
            }
        }

        private void TextBoxRecipients_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!suppressRemoveAddedRecipient)
            {
                BaseUtils.RemoveRecipientsNotInTextBox(AddedRecipients, textBoxRecipients.Text);
            }

            var firstRecipientsRow = BaseUtils.GetRecipientTextFirstRow(textBoxRecipients.Text.Replace("\r\n", "\v"));
            if (!string.IsNullOrEmpty(firstRecipientsRow))
            {
                if (Salutations != null && Salutations.Count > 0)
                {
                    var firstRecipientFirstAndLastName = BaseUtils.GetFirstRecipientFirstAndLastName(firstRecipientsRow);
                    if (firstRecipientFirstAndLastName != null)
                    {
                        var salutationsList = new List<string>();
                        if (!string.IsNullOrEmpty(firstRecipientFirstAndLastName.FirstName))
                        {
                            salutationsList.Add(firstRecipientFirstAndLastName.FirstName);
                        }

                        var salutationsToExclude = GetSalutationsToExclude();
                        List<string> salutations;
                        if (!firstRecipientFirstAndLastName.Title.IsEmpty())
                        {
                            salutations = new List<string>() { firstRecipientFirstAndLastName.Title };
                        }
                        else
                        {
                            salutations = Salutations;
                        }

                        foreach (var defaultSalutation in salutations)
                        {
                            if (salutationsToExclude.Count > 0 && salutationsToExclude.Contains(defaultSalutation))
                            {
                                salutationsList.Add(defaultSalutation);
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(firstRecipientFirstAndLastName.LastName))
                                {
                                    var lastNameParts = firstRecipientFirstAndLastName.LastName.Split(new char[] { ' ', '\t' }, StringSplitOptions.RemoveEmptyEntries);

                                    if (lastNameParts != null && lastNameParts.Length > 0)
                                    {
                                        salutationsList.Add(defaultSalutation + " " + lastNameParts.LastOrDefault());

                                        if (lastNameParts.Length > 1)
                                        {
                                            salutationsList.Add(defaultSalutation + " " + firstRecipientFirstAndLastName.LastName);
                                        }
                                    }
                                }

                                var fullName = (firstRecipientFirstAndLastName.FirstName + " " + firstRecipientFirstAndLastName.LastName).Trim();
                                if (!string.IsNullOrEmpty(fullName))
                                {
                                    salutationsList.Add(defaultSalutation + " " + fullName);
                                }
                            }
                        }
                        comboBoxSalutation.ItemsSource = salutationsList;
                    }
                }
            }
            else
            {
                comboBoxSalutation.ItemsSource = Salutations;
            }
        }

        private void numericUpDownNumber_PreviewTextInput(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            Regex regex = new Regex("^[0-9]*$");
            if (!regex.IsMatch(e.Text))
            {
                e.Handled = true;
            }
        }

        private void ChangeEnclosureComboBox(int number, string enclosureOption)
        {
            var enclosures = GetEnclosureList();
            if ((number > 0 && !string.IsNullOrEmpty(enclosureOption)) & (enclosures != null && enclosures.Count > 0))
            {
                foreach (var enclosure in enclosures)
                {
                    if (number == 1 && enclosure.PluralValue == enclosureOption)
                    {
                        comboBoxEnclosures.SelectedValue = enclosure.SingularValue;
                        return;
                    }
                    else if (number > 1 && enclosure.SingularValue == enclosureOption)
                    {
                        comboBoxEnclosures.SelectedValue = enclosure.PluralValue;
                        return;
                    }
                }
            }
        }

        private List<Enclosure> GetEnclosureList()
        {
            var enclosureSettings = SettingsManager.GetSetting("Enclosures", "Letters", null, MLanguageUtil.ActiveDocumentLanguage);
            if (!string.IsNullOrEmpty(enclosureSettings))
            {
                return BaseUtils.ParseEnclosureSettings(enclosureSettings);
            }
            return BaseUtils.ParseEnclosureSettings(string.Join(";", DefaultEnclosureSettings));
        }

        private List<string> GetSalutations()
        {
            var salutations = new List<string>();
            var salutationSetting = SettingsManager.GetSetting("Salutations", "Letters", null, MLanguageUtil.ActiveDocumentLanguage);
            if (!string.IsNullOrEmpty(salutationSetting))
            {
                salutations.AddRange(SettingsManager.UnescapeCommaCharacter(salutationSetting.Split(',')).Select(x => x.Trim()).ToList());
            }
            else
            {
                salutations.AddRange(DefaultSalutations);
            }
            return salutations;
        }

        private List<string> GetSalutationsToExclude()
        {
            var excludedSalutations = new List<string>();
            var salutationToExcludeSetting = SettingsManager.GetSetting("SalutationsToExclude", "Letters", null, MLanguageUtil.ActiveDocumentLanguage);
            if (!string.IsNullOrEmpty(salutationToExcludeSetting))
            {
                excludedSalutations.AddRange(SettingsManager.UnescapeCommaCharacter(salutationToExcludeSetting.Split(',')).Select(x => x.Trim()).ToList());
            }
            else
            {
                excludedSalutations.AddRange(DefaultExcludedSalutations);
            }
            return excludedSalutations;
        }

        public Range Selection { get; set; }
        public bool OfficeLocationFound { get; internal set; }

        private void OnUpdateButtonClick(object sender, RoutedEventArgs e)
        {
            (this.MainContentControl.Content as DataFillInnerUserControl)?.OnUpdateButtonClick();
            if (isCustomTemplate)
            {
                RefreshDataOK();
            }
            TaskPaneUtil2.TryRestoreSelection(this);
        }

        private void LoadDS_Click(object sender, RoutedEventArgs e)
        {
            (this.MainContentControl.Content as DataFillInnerUserControl)?.ApplyDataSourceValues(dataSourceValues.CustomFields);
            if (isCustomTemplate)
            {
                LoadAnswers(fromDS: true);
            }
        }

        public void OKHandler()
        {
            OnUpdateButtonClick(null, null);
        }

        private void textBoxCC_TextChanged(object sender, TextChangedEventArgs e)
        {
            BaseUtils.RemoveRecipientsNotInTextBox(AddedCCRecipients, textBoxCC.Text);
        }

        private void textBoxBCC_TextChanged(object sender, TextChangedEventArgs e)
        {
            BaseUtils.RemoveRecipientsNotInTextBox(AddedBCCRecipients, textBoxBCC.Text);
        }

        private void SaveAnswersFile_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.SaveFileDialog sfd = new System.Windows.Forms.SaveFileDialog();
            sfd.Filter = "JSON files (*.json)|*.json|XML files (*.xml)|*.xml";
            sfd.AddExtension = true;
            sfd.OverwritePrompt = false;

            sfd.ShowDialog();

            if (!string.IsNullOrWhiteSpace(sfd.FileName))
            {
                bool add = false;

                if (File.Exists(sfd.FileName))
                {
                    DataFillFileExistsDialog dataFillFileExists = new DataFillFileExistsDialog();

                    dataFillFileExists.ShowDialog();

                    switch (dataFillFileExists.Result)
                    {
                        case DataFillFileExistsResult.Overwrite:
                            break;
                        case DataFillFileExistsResult.Add:
                            add = true;
                            break;
                        case DataFillFileExistsResult.Rename:
                            SaveAnswersFile_Click(null, null);
                            return;
                        case DataFillFileExistsResult.Cancel:
                            return;
                    }
                }

                DataFillAnswers answers = new DataFillAnswers();

                if (add)
                {
                    try
                    {
                        if (Path.GetExtension(sfd.FileName).ToLower().Contains("json"))
                        {
                            answers = JsonConvert.DeserializeObject<DataFillAnswers>(File.ReadAllText(sfd.FileName));
                        }
                        else
                        {
                            answers = DataFillAnswers.LoadFromXMLString(File.ReadAllText(sfd.FileName));
                        }
                    }
                    catch (Exception ex)
                    {
                    }
                }

                var fieldValues = (MainContentControl.Content as DataFillInnerUserControl).FieldValues;
                foreach (var fieldValue in fieldValues)
                {
                    var existingField = answers.Answers.Where(x => x.Key == fieldValue.Key).FirstOrDefault();

                    if (existingField == null)
                    {
                        answers.Answers.Add(new DataFillAnswer()
                        {
                            Key = fieldValue.Key,
                            Value = fieldValue.Value
                        });
                    }
                    else
                    {
                        existingField.Value = fieldValue.Value;
                    }
                }

                if (Path.GetExtension(sfd.FileName).ToLower().Contains("json"))
                {
                    string str = JsonConvert.SerializeObject(answers);

                    File.WriteAllText(sfd.FileName, str);
                }
                else
                {
                    string str = answers.ToXML();

                    File.WriteAllText(sfd.FileName, str);
                }
            }
        }

        private void LoadAnswersFile_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.OpenFileDialog ofd = new System.Windows.Forms.OpenFileDialog();
            ofd.Filter = "JSON files (*.json)|*.json|XML files (*.xml)|*.xml";
            ofd.ShowDialog();
            ofd.Multiselect = false;

            if (!string.IsNullOrWhiteSpace(ofd.FileName))
            {
                DataFillAnswers answers = new DataFillAnswers();

                try
                {
                    if (Path.GetExtension(ofd.FileName).ToLower().Contains("json"))
                    {
                        answers = JsonConvert.DeserializeObject<DataFillAnswers>(File.ReadAllText(ofd.FileName));
                    }
                    else
                    {
                        answers = DataFillAnswers.LoadFromXMLString(File.ReadAllText(ofd.FileName));
                    }

                    if (answers?.Answers != null)
                    {
                        LoadedAnswers = answers;

                        (MainContentControl.Content as DataFillInnerUserControl).ApplyValues(answers);

                        this.RetainAnswers.Visibility = Visibility.Visible;
                    }
                }
                catch (Exception ex)
                {
                }
            }
        }

        private void RetainAnswers_Click(object sender, RoutedEventArgs e)
        {
            RetainedAnswers = this.LoadedAnswers;

            RetainAnswers.Visibility = Visibility.Collapsed;
            DropRetainedAnswers.Visibility = Visibility.Visible;
        }

        private void DropRetainedAnswers_Click(object sender, RoutedEventArgs e)
        {
            DropRetainedAnswers.Visibility = Visibility.Collapsed;
            if (LoadedAnswers != null)
            {
                RetainAnswers.Visibility = Visibility.Visible;
            }

            RetainedAnswers = null;
        }

        private void DataFillOptions_Click(object sender, RoutedEventArgs e)
        {
            new DataFillOptionsDialog().ShowDialog();
        }
    }
}
