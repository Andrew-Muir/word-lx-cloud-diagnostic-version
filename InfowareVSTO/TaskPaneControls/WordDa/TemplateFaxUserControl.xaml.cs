﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using InfowareVSTO.Common;
using InfowareVSTO.Common.DTOs;
using InfowareVSTO.Common.General;
using InfowareVSTO.Common.Language;
using InfowareVSTO.Common.Models;
using InfowareVSTO.DataSource;
using InfowareVSTO.Letterhead;
using InfowareVSTO.MultiLanguage;
using InfowareVSTO.Util;
using InfowareVSTO.VSTOAuthor;
using InfowareVSTO.Windows;
using Microsoft.Office.Core;
using Microsoft.Office.Interop.Word;
using Microsoft.Office.Tools.Word;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using ContentControl = Microsoft.Office.Interop.Word.ContentControl;

namespace InfowareVSTO.TaskPaneControls.WordDa
{
	/// <summary>
	/// Interaction logic for TemplateLabelUserControl.xaml
	/// </summary>
	public partial class TemplateFaxUserControl : UserControl, IRefreshData
	{
        private AuthorDTO Author { get; set; }
        private AuthorDTO Assistant { get; set; }
        private List<SelectedContact> Tos { get; set; }
        public Range Selection { get; set; }
        private DataSourceValues dataSourceValues;
        private int? previousOfficeLocationId;
        private List<string> DefaultSalutations { get; set; }
        private List<string> DefaultExcludedSalutations { get; set; }
        private List<string> Salutations { get; set; }
        public List<string> ClosingStatements { get; private set; }
        public int DefaultClosingIndex { get; private set; }
        private string closingEndChar;
        private List<string> DefaultEnclosureSettings { get; set; }
        public List<KeyValuePair<System.Windows.Controls.Control, string>> FieldFunctionList { get; private set; }

        static TemplateFaxUserControl()
		{

		}

		public TemplateFaxUserControl()
		{
			InitializeComponent();
            this.MainContentControl.Content = new DataFillInnerUserControl(false);
            
            //get current author and assistant
            var author = AdminPanelWebApi.GetCurrentAuthor();
            if (author != null)
            {
                Author = author.ToAuthorDTO();
                textBoxAuthor.Text = Author?.FirstName + " " + Author?.LastName;
            }

            int assistantId = AdminPanelWebApi.GetCurrentAsistantId();
            if (assistantId > 0)
            {
                var assistant = AdminPanelWebApi.GetAuthor(assistantId);
                if(assistant != null)
                {
                    Assistant = assistant.ToAuthorDTO();
                    textBoxAssistant.Text = Assistant?.FirstName + " " + Assistant?.LastName;
                }
            }


            comboBoxOfficeLocation.ItemsSource = Utils.GetLocationsForCombobox();

            int defaultLocationId = -1;
            var currentAuthorSettings = ManageUsers.CurrentAuthorSettings;
            if (currentAuthorSettings.LocationId.HasValue)
            {
                defaultLocationId = currentAuthorSettings.LocationId.Value;
            }
            else
            {
                int? companyId = currentAuthorSettings.CompanyId ?? DocumentPropertyUtil.GetTemplateCompanyID();

                if (companyId.HasValue)
                {
                    defaultLocationId = AdminPanelWebApi.GetCompanyDetails(companyId.Value).Id;
                }
            }

            if (defaultLocationId >= 0)
            {
                comboBoxOfficeLocation.SelectedValue = defaultLocationId;
            }

            Tos = new List<SelectedContact>();

            UpdateClosingStatements();
            UpdateDefaultClosingIndex();
            UpdateClosingEndChar();

            Utils.UpdateCurrentCultureShortDateFormat();
            dateTimePickerDate.SelectedDate = DateTime.Today;

            SettingsManager.PopulateComboboxWithDeliveryOptions(this.comboBoxDelivery);
            SettingsManager.PopulateComboboxWithHandlingOptions(this.comboBoxHandling);

            DefaultSalutations = new List<string> { "Counsel", "Mr.", "Ms.", "Mrs.", "Mr/Ms.", "Mr/Mrs.", "Mx.", "Madame", "Mesdames", "Sir", "Sirs", "Sir/Madame", "Sirs/Mesdames" };
            DefaultExcludedSalutations = new List<string> { "Counsel", "Sir", "Madame", "Sir/Madame", "Sirs/Mesdames", "Sirs", "Mesdames" };

            Salutations = GetSalutations();
            comboBoxSalutation.ItemsSource = Salutations;


            var enclosures = GetEnclosureList();
            if (enclosures != null)
            {
                var enclosureOptions = BaseUtils.GetEnclosureOptions(enclosures);
                if (enclosureOptions != null && enclosureOptions.Count > 0)
                {
                    comboBoxEnclosures.ItemsSource = enclosureOptions;
                    comboBoxEnclosures.DisplayMemberPath = "Key";
                    comboBoxEnclosures.SelectedValuePath = "Value";
                    comboBoxEnclosures.SelectedIndex = 0;
                }
            }

            DataContext = this;
            OrderFields();
            LoadAnswers();
		}

        private void OrderFields()
        {
            string json = SettingsManager.GetSetting("TaskPaneFieldOrder", "Faxes", null);
            TaskpaneFieldOrderer tfo = new TaskpaneFieldOrderer();
            tfo.OrderFields(json, this.FieldContainer);
        }

        private void InitFieldValidation()
        {
            string settingStr = SettingsManager.GetSetting("FieldsRequiringValidation", "Letters");
            List<KeyValuePair<System.Windows.Controls.Control, string>> result = new List<KeyValuePair<System.Windows.Controls.Control, string>>();
            if (settingStr != null)
            {
                List<string> pairs = settingStr.Split(';').Select(x => x.Trim()).ToList();

                foreach (string pair in pairs)
                {
                    List<string> pairList = pair.Split(',').Select(x => x.Trim()).ToList();

                    if (pairList.Count > 1)
                    {
                        System.Windows.Controls.Control control = TranslateFieldToControl(pairList[0]);

                        if (control != null)
                        {
                            result.Add(new KeyValuePair<System.Windows.Controls.Control, string>(control, pairList[1]));

                            control.LostKeyboardFocus += Control_LostKeyboardFocus;
                        }
                    }
                }
            }

            this.FieldFunctionList = result;
        }

        private void Control_LostKeyboardFocus(object sender, System.Windows.Input.KeyboardFocusChangedEventArgs e)
        {
            string functionName = this.FieldFunctionList.Where(x => x.Key == sender as System.Windows.Controls.Control).FirstOrDefault().Value;

            if (functionName != null)
            {
                string value = null;
                if (sender is TextBox)
                {
                    value = (sender as TextBox).Text;
                }
                else if (sender is ComboBox)
                {
                    value = (sender as ComboBox).Text;
                }

                if (!string.IsNullOrEmpty(value))
                {
                    bool fieldValid = Utils.ValidateField(functionName, value);

                    if (!fieldValid)
                    {
                        InfowareInformationWindow iiw = new InfowareInformationWindow(LanguageManager.GetTranslation(LanguageConstants.InputIsNotValidPleaseReEnter, "Input is invalid, please re-enter."),
                           LanguageManager.GetTranslation(LanguageConstants.InputIsNotValid, "Invalid Input"), WordDefaultIconType.Warning);
                        iiw.ShowDialog();
                        if (sender is TextBox)
                        {
                            (sender as TextBox).Text = string.Empty;
                        }
                        else if (sender is ComboBox)
                        {
                            (sender as ComboBox).SelectedIndex = -1;
                            (sender as ComboBox).Text = string.Empty;
                        }
                    }
                }
            }
        }

        private System.Windows.Controls.Control TranslateFieldToControl(string fieldName)
        {
            switch (fieldName)
            {
                case "Salutation":
                    return comboBoxSalutation;
                case "Our File Number":
                    return textBoxFileNumber;
                case "Their File Number":
                    return textBoxClientFileNumber;
                case "Delivery":
                    return comboBoxDelivery;
                case "Handling":
                    return comboBoxHandling;
                case "Office Location":
                    return comboBoxOfficeLocation;
                case "Re Line":
                    return textBoxReLine;
                case "Closing":
                    return comboBoxClosing;
                case "Enclosure(s)":
                    return comboBoxEnclosures;
                case "EnclosureNo":
                    return numericUpDownNumber;
                case "To":
                    return textBoxTos;
                case "Firm":
                    return textBoxFirm;
                case "Fax Number":
                    return textBoxFax;
                case "Phone Number":
                    return textBoxPhone;
                case "No. of pages":
                    return textBoxNrOfPages;
            }
            return null;
        }

        private void UpdateClosingStatements()
        {
            const char pipeSeparator = '|';

            string rawClosingStatements = SettingsManager.GetSetting("Closings", "Glossary", null, MLanguageUtil.ActiveDocumentLanguage);

            if (rawClosingStatements == null)
            {
                ClosingStatements = new List<string>()
                {
                    "Yours truly",
                    "Yours very truly",
                    "Sincerely",
                    "Regards",
                    "Best regards",
                    "Kindest regards"
                };

                comboBoxClosing.ItemsSource = ClosingStatements;
                return;
            }

            char separator;

            if (rawClosingStatements.Contains(pipeSeparator))
            {
                separator = pipeSeparator;
            }
            else
            {
                separator = ',';
            }

            ClosingStatements = SettingsManager.UnescapeCommaCharacter(rawClosingStatements.Split(separator)).Select(it => it.Trim()).ToList();
            comboBoxClosing.ItemsSource = ClosingStatements;
        }

        private void UpdateDefaultClosingIndex()
        {
            //try to get it from current author
            var author = AdminPanelWebApi.GetCurrentAuthor();
            if (!string.IsNullOrEmpty(author?.DefaultClosing))
                DefaultClosingIndex = ClosingStatements.IndexOf(author.DefaultClosing);
            else
                DefaultClosingIndex = -1;

            if (DefaultClosingIndex < 0)
                DefaultClosingIndex = SettingsManager.GetSettingAsInt("ClosingsDefaultIdx", "Glossary", languageId: MLanguageUtil.ActiveDocumentLanguage);

            if (DefaultClosingIndex > ClosingStatements.Count - 1)
            {
                DefaultClosingIndex = -1;
            }

            comboBoxClosing.SelectedIndex = DefaultClosingIndex;
        }

        private void UpdateClosingEndChar()
        {
            closingEndChar = SettingsManager.GetSetting("ClosingEndChar", "Glossary", null, MLanguageUtil.ActiveDocumentLanguage);

            if (closingEndChar == null)
            {
                closingEndChar = ",";
            }
        }

        public void LoadAnswers(PaneDTO faxDTO = null, bool fromDS = false)
        {
            if (faxDTO == null)
            {
                if (fromDS)
                {
                    faxDTO = TaskPaneUtil.GetDataSourceAnsweresDTO(TaskPaneUtil.LoadAnswers()).PaneDTO;
                    (this.MainContentControl.Content as DataFillInnerUserControl)?.ApplyDataSourceValues(dataSourceValues?.CustomFields);
                }
                else
                {
                    dataSourceValues = TaskPaneUtil.GetDataSourceAnsweresDTO();
                    faxDTO = TaskPaneUtil.GetNewPaneDTO(dataSourceValues);
                    faxDTO = TaskPaneUtil.LoadAnswers(faxDTO);
                    if (dataSourceValues?.PaneDTO != null && TaskPaneUtil.AnswersFound)
                    {
                        this.LoadDS.Visibility = System.Windows.Visibility.Visible;
                    }
                    else
                    {
                        (this.MainContentControl.Content as DataFillInnerUserControl)?.ApplyDataSourceValues(dataSourceValues?.CustomFields);
                    }
                }
            }
            
            if (faxDTO != null)
            {
                dateTimePickerDate.SelectedDate = faxDTO.Date;
                textBoxFax.Text = faxDTO.FaxNumber;
                textBoxPhone.Text = faxDTO.PhoneNumber;
                textBoxFileNumber.Text = faxDTO.FileNumber;
                textBoxClientFileNumber.Text = faxDTO.ClientFileNumber;
                textBoxNrOfPages.Text = faxDTO.NoOfPages;
                textBoxReLine.Text = faxDTO.ReLine;
                numericUpDownNumber.Text = faxDTO.Number.ToString();
                Tos = faxDTO.Tos?.ToList() ?? new List<SelectedContact>();
                if (Tos.Count == 0)
                {
                    if (faxDTO.Recipients!= null)
                    {
                        string to = string.Empty;
                        var recipients = faxDTO.Recipients.Split(new string[] { "\v\v" }, StringSplitOptions.None);
                        foreach (string recipient in recipients)
                        {
                            var lines = recipient.Split('\v');
                            to += lines[0] + '\v';
                        }

                        to.Trim();
                        textBoxTos.Text = to;
                    }
                }
                else
                {
                    AddContacts(Tos);
                }

                var faxAuthor = faxDTO.Author?.ToAuthorDTO();
                if (faxAuthor != null)
                {
                    Author = faxAuthor;

                    var faxAuthorFullName = faxAuthor?.FirstName + " " + faxAuthor?.LastName;
                    if (!string.IsNullOrWhiteSpace(faxAuthorFullName))
                    {
                        textBoxAuthor.Text = faxAuthorFullName;
                    }
                }

                var faxAssistant = faxDTO.Assistant?.ToAuthorDTO();
                if (faxAssistant != null)
                {
                    Assistant = faxAssistant;

                    var faxAssistantFullName = faxAssistant?.FirstName + " " + faxAssistant?.LastName;
                    if (!string.IsNullOrWhiteSpace(faxAssistantFullName))
                    {
                        textBoxAssistant.Text = faxAssistantFullName;
                    }
                }
                Utils.SetComboBoxValue(comboBoxOfficeLocation, faxDTO.OfficeLocation);
                if (faxDTO.OfficeLocationId.HasValue)
                {
                    this.comboBoxOfficeLocation.SelectedValue = faxDTO.OfficeLocationId.Value;
                    this.previousOfficeLocationId = faxDTO.OfficeLocationId;
                }

                Utils.SetComboBoxValue(comboBoxDelivery, faxDTO.Delivery);
                Utils.SetComboBoxValue(comboBoxHandling, faxDTO.Handling);
                Utils.SetComboBoxValue(comboBoxSalutation, faxDTO.Salutation);
                Utils.SetComboBoxValue(comboBoxClosing, faxDTO.Closing);
                Utils.SetComboBoxValue(comboBoxEnclosures, faxDTO.Enclosures);
            }
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            buttonOK.IsEnabled = false;
            MsoLanguageID languageID = MLanguageUtil.ActiveDocumentLanguage;
            AuthorWithPathsDTO selectedAuthor;
            AuthorWithPathsDTO selectedAssistant;
            if (Author == null)
                selectedAuthor = new AuthorWithPathsDTO();
            else
                selectedAuthor = AdminPanelWebApi.GetAuthor(Author.Id, languageID);
            if (Assistant == null)
                selectedAssistant = new AuthorWithPathsDTO();
            else
                selectedAssistant = AdminPanelWebApi.GetAuthor(Assistant.Id, languageID);
            PaneDTO letterDTO = new PaneDTO();
            letterDTO.Author = selectedAuthor;
            //letterDTO.Author.FirstName = textBoxAuthor.Text;
            letterDTO.Assistant = selectedAssistant;
            //letterDTO.Assistant.FirstName = textBoxAssistant.Text;
            letterDTO.Date = dateTimePickerDate.SelectedDate ?? DateTime.Today;

            int.TryParse(numericUpDownNumber.Text, out int number);
            ChangeEnclosureComboBox(number, comboBoxEnclosures.Text);

            //tos
            List<AuthorWithPathsDTO> tosWithPath = new List<AuthorWithPathsDTO>();
          
            letterDTO.Tos = Tos.ToArray();
            letterDTO.FaxNumber = textBoxFax.Text;
            letterDTO.PhoneNumber = textBoxPhone.Text;
            letterDTO.Firm = textBoxFirm.Text;
            letterDTO.Recipients = textBoxTos.Text.Replace("\r\n", "\v");
            letterDTO.FileNumber = textBoxFileNumber.Text;
            letterDTO.CompanyId = DocumentPropertyUtil.GetTemplateCompanyID();
            letterDTO.ClientFileNumber = textBoxClientFileNumber.Text;
            letterDTO.NoOfPages = textBoxNrOfPages.Text;
            letterDTO.ReLine = textBoxReLine.Text;
            letterDTO.Salutation = comboBoxSalutation.Text;
            letterDTO.Closing = comboBoxClosing.Text;
            letterDTO.ClosingEndChar = closingEndChar;
            letterDTO.Enclosures = comboBoxEnclosures.SelectedValue is string ? comboBoxEnclosures.SelectedValue as string : comboBoxEnclosures.Text;
            letterDTO.Number = number;
            letterDTO.Delivery = comboBoxDelivery.SelectedValue is string ? comboBoxDelivery.SelectedValue as string : comboBoxDelivery.Text;
            letterDTO.Handling = comboBoxHandling.SelectedValue is string ? comboBoxHandling.SelectedValue as string : comboBoxHandling.Text;
            letterDTO.OfficeLocationId = comboBoxOfficeLocation.SelectedValue as int?;
            letterDTO.FileNumberTagSetting = SettingsManager.GetSetting("FileNoTag", "Letters", null, MLanguageUtil.ActiveDocumentLanguage);
            letterDTO = MLanguageUtil.ApplyDateSettings(letterDTO);
            PaneUtils.InsertGeneralTemplateDTO(ThisAddIn.Instance.Application.ActiveDocument, letterDTO, TemplateType.Fax);
           TaskPaneUtil2.TryRestoreSelection(this);
            (MainContentControl.Content as DataFillInnerUserControl)?.OnUpdateButtonClick();
            TaskPaneUtil.SaveAnswers(letterDTO);
            buttonOK.IsEnabled = true;
            if (this.comboBoxOfficeLocation.SelectedValue as int? != null && previousOfficeLocationId != null && this.comboBoxOfficeLocation.SelectedValue as int? != previousOfficeLocationId)
            {
                new ExistingLetterhead().ChangeLetterhead((comboBoxOfficeLocation.SelectedValue as int?).Value);
            }
        }

        private void buttonFindAuthor_Click(object sender, EventArgs e)
        {
            SelectAuthor_WPF selectAuthor_WPF = new SelectAuthor_WPF();
            selectAuthor_WPF.ShowDialog();
            if (selectAuthor_WPF.ResultOk)
            {
                Author = selectAuthor_WPF.SelectedAuthor;
                textBoxAuthor.Text = Author.FirstName + " " + Author.LastName;
            }
        }

        private void buttonFindAssistant_Click(object sender, EventArgs e)
        {
            SelectAuthor_WPF selectAuthor_WPF = new SelectAuthor_WPF();
            selectAuthor_WPF.ShowDialog();
            if (selectAuthor_WPF.ResultOk)
            {
                Assistant = selectAuthor_WPF.SelectedAuthor;
                textBoxAssistant.Text = Assistant.FirstName + " " + Assistant.LastName;
            }
        }

        private void buttonFindRecipients_Click(object sender, EventArgs e)
        {
            ImportContacts_WPF importContacts_WPF = new ImportContacts_WPF(RecipientType.MultiRow, authorId:this.Author?.Id, CompanyNameStackPanelVisibility: Visibility.Collapsed, oldContacts:Tos, useAddress: false);
            importContacts_WPF.ShowDialog();
            if (importContacts_WPF.ResultOk)
            {
                BaseUtils.RemoveContactsFromTextBoxes(importContacts_WPF.RemovedContacts, textBoxTos, includesAddress:false);
                
                var contactsToAdd = new List<SelectedContact>();
                foreach (var selectedContact in importContacts_WPF.SelectedContacts)
                {
                    if (Tos.FirstOrDefault(x => x.FirstName == selectedContact.FirstName & x.LastName == selectedContact.LastName) == null)
                    {
                        Tos.Add(AddJobDescriptionToLastName(selectedContact, importContacts_WPF.JobTitleVisibility));
                        contactsToAdd.Add(selectedContact);
                    }
                }

                AddContacts(contactsToAdd, importContacts_WPF);
            }
        }

        private void numericUpDownNumber_PreviewTextInput(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            Regex regex = new Regex("^[0-9]*$");
            if (!regex.IsMatch(e.Text))
            {
                e.Handled = true;
            }
        }

        private void ChangeEnclosureComboBox(int number, string enclosureOption)
        {
            var enclosures = GetEnclosureList();
            if ((number > 0 && !string.IsNullOrEmpty(enclosureOption)) & (enclosures != null && enclosures.Count > 0))
            {
                foreach (var enclosure in enclosures)
                {
                    if (number == 1 && enclosure.PluralValue == enclosureOption)
                    {
                        comboBoxEnclosures.SelectedValue = enclosure.SingularValue;
                        return;
                    }
                    else if (number > 1 && enclosure.SingularValue == enclosureOption)
                    {
                        comboBoxEnclosures.SelectedValue = enclosure.PluralValue;
                        return;
                    }
                }
            }
        }

        private List<Enclosure> GetEnclosureList()
        {
            var enclosureSettings = SettingsManager.GetSetting("Enclosures", "Letters", null, MLanguageUtil.ActiveDocumentLanguage);
            if (!string.IsNullOrEmpty(enclosureSettings))
            {
                return BaseUtils.ParseEnclosureSettings(enclosureSettings);
            }
            return BaseUtils.ParseEnclosureSettings(string.Join(";", DefaultEnclosureSettings));
        }

        private SelectedContact AddJobDescriptionToLastName(SelectedContact input, JobTitleVisibility jobTitleVisibility)
        {
            if (jobTitleVisibility != JobTitleVisibility.NoJobTitle)
            {
                string jobTitle = input.JobTitle;

                string additionalText = null;

                if (jobTitleVisibility == JobTitleVisibility.SameRow)
                {
                    additionalText = ", " + jobTitle;
                }
                else
                {
                    additionalText = "\v" + jobTitle;
                }

                if (input.LastName.IsEmpty())
                {
                    if (input.FirstName != null)
                    {
                        input.DispayName = input.FirstName += additionalText;
                    }
                }
                else
                {
                    input.DispayName = input.FirstName + " " + input.LastName + additionalText;
                }
            }

            return input;
        }

        private void AddContacts(List<SelectedContact> contacts, ImportContacts_WPF importContacts_WPF = null)
        {
            if (contacts != null && contacts.Count > 0)
            {
                if (textBoxTos.IsReadOnly == false)
                {
                    textBoxTos.Text = "";
                    textBoxTos.IsReadOnly = true;
                    textBoxFirm.Text = "";
                    textBoxFirm.IsReadOnly = true;
                    textBoxFax.Text = "";
                    textBoxFax.IsReadOnly = true;
                    textBoxPhone.Text = "";
                    textBoxPhone.IsReadOnly = true;
                }

                var recipients = new List<string>();
                foreach (var to in contacts)
                {
                    var fullName = new List<string>();
                    if (!to.FirstName.IsEmpty())
                    {
                        fullName.Add(to.FirstName);
                    }

                    if (!to.LastName.IsEmpty())
                    {
                        fullName.Add(to.LastName);
                    }

                    var fullNameStr = "";
                    if (fullName.Count > 0)
                    {
                        fullNameStr = string.Join(" ", fullName);
                    }

                    if (importContacts_WPF != null && !to.JobTitle.IsEmpty())
                    {
                        if (importContacts_WPF.JobTitleVisibility == JobTitleVisibility.SameRow)
                        {
                            var fullNameWithJobTitle = new List<string>();
                            if (!string.IsNullOrEmpty(fullNameStr))
                            {
                                fullNameWithJobTitle.Add(fullNameStr);
                            }
                            fullNameWithJobTitle.Add(to.JobTitle);

                            recipients.Add(string.Join(", ", fullNameWithJobTitle));
                        }
                        else if (importContacts_WPF.JobTitleVisibility == JobTitleVisibility.NextRow)
                        {
                            if (!string.IsNullOrEmpty(fullNameStr))
                            {
                                recipients.Add(fullNameStr);
                            }
                            recipients.Add(to.JobTitle);
                        }
                        else
                        {
                            recipients.Add(fullNameStr);
                        }
                    }
                    else
                    {
                        recipients.Add(fullNameStr);
                    }
                }

                var recipientsStr = string.Join("\r\n", recipients);
                if (textBoxTos.Text.Trim().IsEmpty())
                {
                    textBoxTos.Text = recipientsStr;
                }
                else
                {
                    textBoxTos.Text = textBoxTos.Text + "\r\n" + recipientsStr;
                }
            }
        }


        private void buttonRemoveRecipients_Click(object sender, EventArgs e)
        {
            ConfirmDeleteContacts_WPF confirmDeleteContacts_WPF = new ConfirmDeleteContacts_WPF();
            confirmDeleteContacts_WPF.ShowDialog();
            if (confirmDeleteContacts_WPF.ResultOk)
            {
                Tos = new List<SelectedContact>();
                textBoxTos.Text = "";
                textBoxTos.IsReadOnly = false;
                textBoxFirm.IsReadOnly = false;
                textBoxFax.IsReadOnly = false;
                textBoxPhone.IsReadOnly = false;
            }
        }

		private void buttonToday_Click(object sender, EventArgs e)
		{
			dateTimePickerDate.SelectedDate = DateTime.Today;
		}

        private void LoadDS_Click(object sender, RoutedEventArgs e)
        {
            LoadAnswers(fromDS: true);
        }
        public void OKHandler()
        {
            buttonOK_Click(null, null);
        }

        private void textBoxTos_TextChanged(object sender, TextChangedEventArgs e)
        {
            var firstRecipientsRow = BaseUtils.GetRecipientTextFirstRow(textBoxTos.Text.Replace("\r\n", "\v"));
            if (!string.IsNullOrEmpty(firstRecipientsRow))
            {
                if (Salutations != null && Salutations.Count > 0)
                {
                    var firstRecipientFirstAndLastName = BaseUtils.GetFirstRecipientFirstAndLastName(firstRecipientsRow);
                    if (firstRecipientFirstAndLastName != null)
                    {
                        var salutationsList = new List<string>();
                        if (!string.IsNullOrEmpty(firstRecipientFirstAndLastName.FirstName))
                        {
                            salutationsList.Add(firstRecipientFirstAndLastName.FirstName);
                        }

                        var salutationsToExclude = GetSalutationsToExclude();
                        List<string> salutations;
                        if (!firstRecipientFirstAndLastName.Title.IsEmpty())
                        {
                            salutations = new List<string>() { firstRecipientFirstAndLastName.Title };
                        }
                        else
                        {
                            salutations = Salutations;
                        }

                        foreach (var defaultSalutation in salutations)
                        {
                            if (salutationsToExclude.Count > 0 && salutationsToExclude.Contains(defaultSalutation))
                            {
                                salutationsList.Add(defaultSalutation);
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(firstRecipientFirstAndLastName.LastName))
                                {
                                    var lastNameParts = firstRecipientFirstAndLastName.LastName.Split(new char[] { ' ', '\t' }, StringSplitOptions.RemoveEmptyEntries);

                                    if (lastNameParts != null && lastNameParts.Length > 0)
                                    {
                                        salutationsList.Add(defaultSalutation + " " + lastNameParts.LastOrDefault());

                                        if (lastNameParts.Length > 1)
                                        {
                                            salutationsList.Add(defaultSalutation + " " + firstRecipientFirstAndLastName.LastName);
                                        }
                                    }
                                }

                                var fullName = (firstRecipientFirstAndLastName.FirstName + " " + firstRecipientFirstAndLastName.LastName).Trim();
                                if (!string.IsNullOrEmpty(fullName))
                                {
                                    salutationsList.Add(defaultSalutation + " " + fullName);
                                }
                            }
                        }
                        comboBoxSalutation.ItemsSource = salutationsList;
                    }
                }
            }
            else
            {
                comboBoxSalutation.ItemsSource = Salutations;
            }
        }
        private List<string> GetSalutations()
        {
            var salutations = new List<string>();
            var salutationSetting = SettingsManager.GetSetting("Salutations", "Letters", null, MLanguageUtil.ActiveDocumentLanguage);
            if (!string.IsNullOrEmpty(salutationSetting))
            {
                salutations.AddRange(SettingsManager.UnescapeCommaCharacter(salutationSetting.Split(',')).Select(x => x.Trim()).ToList());
            }
            else
            {
                salutations.AddRange(DefaultSalutations);
            }
            return salutations;
        }

        private List<string> GetSalutationsToExclude()
        {
            var excludedSalutations = new List<string>();
            var salutationToExcludeSetting = SettingsManager.GetSetting("SalutationsToExclude", "Letters", null, MLanguageUtil.ActiveDocumentLanguage);
            if (!string.IsNullOrEmpty(salutationToExcludeSetting))
            {
                excludedSalutations.AddRange(SettingsManager.UnescapeCommaCharacter(salutationToExcludeSetting.Split(',')).Select(x => x.Trim()).ToList());
            }
            else
            {
                excludedSalutations.AddRange(DefaultExcludedSalutations);
            }
            return excludedSalutations;
        }
    }
}
