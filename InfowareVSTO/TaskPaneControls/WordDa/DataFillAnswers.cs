﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace InfowareVSTO.TaskPaneControls.WordDa
{
    public class DataFillAnswers
    {
        public DataFillAnswers()
        {
            Answers = new List<DataFillAnswer>();
        }

        public List<DataFillAnswer> Answers { get; set; }

        public string ToXML()
        {
            using (var stringwriter = new System.IO.StringWriter())
            {
                var serializer = new XmlSerializer(this.GetType());
                serializer.Serialize(stringwriter, this);
                return stringwriter.ToString();
            }
        }

        public static DataFillAnswers LoadFromXMLString(string xmlText)
        {
            using (var stringReader = new System.IO.StringReader(xmlText))
            {
                var serializer = new XmlSerializer(typeof(DataFillAnswers));
                return serializer.Deserialize(stringReader) as DataFillAnswers;
            }
        }
    }

    public class DataFillAnswer
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
