﻿namespace InfowareVSTO.TaskPaneControls.WordDa
{
    partial class WordDaTaskPaneContainer
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MainElementHost = new System.Windows.Forms.Integration.ElementHost();
            this.SuspendLayout();
            // 
            // MainElementHost
            // 
            this.MainElementHost.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainElementHost.Location = new System.Drawing.Point(0, 0);
            this.MainElementHost.Name = "MainElementHost";
            this.MainElementHost.Size = new System.Drawing.Size(150, 150);
            this.MainElementHost.TabIndex = 0;
            this.MainElementHost.Text = "elementHost1";
            this.MainElementHost.Child = null;
            // 
            // WordDaTaskPaneContainer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.MainElementHost);
            this.Name = "WordDaTaskPaneContainer";
            this.ResumeLayout(false);

        }

        #endregion
        private ClauseBuilderUserControl clauseBuilderUserControl1;
        public System.Windows.Forms.Integration.ElementHost MainElementHost;
    }
}
