﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using InfowareVSTO.Common;
using InfowareVSTO.Common.DTOs;
using InfowareVSTO.Common.General;
using InfowareVSTO.Util;
using Microsoft.Office.Interop.Word;
using Microsoft.Office.Tools.Word;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using ContentControl = Microsoft.Office.Interop.Word.ContentControl;

namespace InfowareVSTO.TaskPaneControls.WordDa
{
	/// <summary>
	/// Interaction logic for TemplateLabelUserControl.xaml
	/// </summary>
	public partial class TemplateDesignerUserControl : UserControl
	{		
		public ObservableCollection<string> DataFields { get; set; }
		public ICollectionView DataFieldsView    
		{    
			get { return CollectionViewSource.GetDefaultView(DataFields); }    
		}
		private StandardFieldsDTO StandardFields { get; set; }
		private Group Group { get; set; }

		static TemplateDesignerUserControl()
		{

		}

		public TemplateDesignerUserControl()
		{
			InitializeComponent();
						
			//using(StreamReader reader = new StreamReader(Directory.GetCurrentDirectory() + @"/Resources/standardFields.json"))			
			var assembly = Assembly.GetExecutingAssembly();
			string resourceName = assembly.GetManifestResourceNames().Single(str => str.EndsWith("standardFields.json"));
			using (Stream stream = assembly.GetManifestResourceStream(resourceName))
			using (StreamReader reader = new StreamReader(stream))
			{
				string jsonString = reader.ReadToEnd();
				StandardFields = JsonConvert.DeserializeObject<StandardFieldsDTO>(jsonString);				
			}

			DataFields = new ObservableCollection<string>();

			PopulateFields();

			DataContext = this;
		}

		private void ButtonSave_Click(object sender, RoutedEventArgs e)
		{

		}

		private void TextBlock1Plus_PreviewMouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			if (TextBlockPlus1.Text == "-")
			{
				TextBlockPlus1.Text = "+";	
				ListViewDataFields1.Visibility = Visibility.Collapsed;
				PanelCustomField.Visibility = Visibility.Collapsed;
			}
			else
			{
				CollapsLists();
				TextBlockPlus1.Text = "-";
				DataFields.Clear();
				if (Group.Fields[0].Fields.Count == 1 && Group.Fields[0].Fields[0] == "CUSTOMFIELD")
				{
					PanelCustomField.Visibility = Visibility.Visible;
				}
				else
				{
					foreach (var field in Group.Fields[0].Fields)
						DataFields.Add(field);
					ListViewDataFields1.Visibility = Visibility.Visible;
				}
			}
		}		

		private void TextBlock2Plus_PreviewMouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			if (TextBlockPlus2.Text == "-")
			{
				TextBlockPlus2.Text = "+";	
				ListViewDataFields2.Visibility = Visibility.Collapsed;
			}
			else
			{
				CollapsLists();
				TextBlockPlus2.Text = "-";
				DataFields.Clear();
				foreach (var field in Group.Fields[1].Fields)
					DataFields.Add(field);				
				ListViewDataFields2.Visibility = Visibility.Visible;
			}
		}

		private void TextBlock3Plus_PreviewMouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			if (TextBlockPlus3.Text == "-")
			{
				TextBlockPlus3.Text = "+";	
				ListViewDataFields3.Visibility = Visibility.Collapsed;
			}
			else
			{
				CollapsLists();
				TextBlockPlus3.Text = "-";
				DataFields.Clear();
				foreach (var field in Group.Fields[2].Fields)
					DataFields.Add(field);				
				ListViewDataFields3.Visibility = Visibility.Visible;
			}
		}

		private void TextBlock4Plus_PreviewMouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			if (TextBlockPlus4.Text == "-")
			{
				TextBlockPlus4.Text = "+";	
				ListViewDataFields4.Visibility = Visibility.Collapsed;
			}
			else
			{
				CollapsLists();
				TextBlockPlus4.Text = "-";
				DataFields.Clear();
				foreach (var field in Group.Fields[3].Fields)
					DataFields.Add(field);				
				ListViewDataFields4.Visibility = Visibility.Visible;
			}
		}

		private void ButtonAddDataField_Click(object sender, RoutedEventArgs e)
		{
			Button button = sender as Button;
			string fieldName = button.DataContext as string;			
		}

		private void ButtonRemoveDataField_Click(object sender, RoutedEventArgs e)
		{
			Button button = sender as Button;
			string fieldName = button.DataContext as string;
		}

		private void ComboBoxTemplateType_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			if(DataFields != null)
				PopulateFields();
		}


		private void PopulateFields()
		{
			DataFields.Clear();
			CollapseAll();

			Group = StandardFields.Groups.Where(x => x.Name.ToLower() == ((ComboBoxItem)comboBoxTemplateType.SelectedValue).Tag.ToString().ToLower()).FirstOrDefault();
			if (Group == null)
			{
				TextBlockInsertInfo.Visibility = Visibility.Collapsed;
				TextBlockBlankWarning.Visibility = Visibility.Visible;
				Group = new Group();
				Group.Fields = new List<Common.DTOs.Field>();
			}
			else
			{
				TextBlockInsertInfo.Visibility = Visibility.Visible;
				TextBlockBlankWarning.Visibility = Visibility.Collapsed;				
			}

			//1
			if (Group.Fields.Count > 0)
			{
				TextBlockSubgroup1.Text = Group.Fields[0].SubGroup;				
				PanelFirmFieldsTitle1.Visibility = Visibility.Visible;						
			}
			
			//2
			if (Group.Fields.Count > 1)
			{
				TextBlockSubgroup2.Text = Group.Fields[1].SubGroup;
				PanelFirmFieldsTitle2.Visibility = Visibility.Visible;
			}

			//2
			if (Group.Fields.Count > 2)
			{
				TextBlockSubgroup3.Text = Group.Fields[2].SubGroup;
				PanelFirmFieldsTitle3.Visibility = Visibility.Visible;
			}

			//2
			if (Group.Fields.Count > 3)
			{
				TextBlockSubgroup4.Text = Group.Fields[3].SubGroup;
				PanelFirmFieldsTitle4.Visibility = Visibility.Visible;
			}
		}

		private void CollapseAll()
		{
			PanelFirmFieldsTitle1.Visibility = Visibility.Collapsed;
			PanelFirmFieldsTitle2.Visibility = Visibility.Collapsed;
			PanelFirmFieldsTitle3.Visibility = Visibility.Collapsed;
			PanelFirmFieldsTitle4.Visibility = Visibility.Collapsed;
			
			CollapsLists();			
		}

		private void CollapsLists()
		{
			ListViewDataFields1.Visibility = Visibility.Collapsed;
			PanelCustomField.Visibility = Visibility.Collapsed;
			ListViewDataFields2.Visibility = Visibility.Collapsed;
			ListViewDataFields3.Visibility = Visibility.Collapsed;
			ListViewDataFields4.Visibility = Visibility.Collapsed;
			TextBlockPlus1.Text = "+";
			TextBlockPlus2.Text = "+";
			TextBlockPlus3.Text = "+";
			TextBlockPlus4.Text = "+";
		}

		private void ButtonAddCustomField_Click(object sender, RoutedEventArgs e)
		{

		}
	}
}
