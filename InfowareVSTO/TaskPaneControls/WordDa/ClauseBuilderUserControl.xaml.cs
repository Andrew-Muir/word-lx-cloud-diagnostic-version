﻿using InfowareVSTO.Common;
using InfowareVSTO.Common.Language;
using InfowareVSTO.Util;
using InfowareVSTO.Windows;
using InfowareVSTO.Windows.WordDa.Library;
using Microsoft.Office.Interop.Word;
using Microsoft.Office.Tools.Word;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using ContentControl = Microsoft.Office.Interop.Word.ContentControl;

namespace InfowareVSTO.TaskPaneControls.WordDa
{
    /// <summary>
    /// Interaction logic for ClauseBuilderUserControl.xaml
    /// </summary>
    public partial class ClauseBuilderUserControl : UserControl, INotifyPropertyChanged
    {
        /// <summary>
        /// As defined in the "Webservice parameters" document
        /// </summary>
        private const int TEMPLATE_TYPE = 7;
        public const string ASK_BOOLEAN_TAG = "LX-WORDDA-BOOLEAN";
        public const string ASK_RTF_RESULT_TAG = "LX-WORDDA-RTF-RESULT";
        public const string ASK_FALSE_TITLE = "False Value";
        public const string ASK_TRUE_TITLE = "True Value";
        public const string ASK_RTF_FIELD_TAG = "LX-WORDDA-ASK-RTF";

        private const string TEMPLATE_FILE_FORM_DATA_NAME = "myFile";

        private const string CITATION_CONTROL_TAG = "LX-WORDDA-CITATION";

        private const string CITATION_CONTROL_NAME_TEMPLATE = CITATION_CONTROL_TAG + "_{0}";

        private const string DATA_FIELD_CONTROL_TAG = "WordDA";

        private const string DATA_FIELD_CODE_CONTROL_TAG = "WordDAQuestion";

        private const string QUESTION_FILE_PROPERTY_NAME = "LX-WORDDA-QUESTIONFILE";

        private static readonly string GROUP_TITLE_TEMPLATE = "Group: {0}";

        private static readonly string CITATION_TITLE_TEMPLATE = "Citation: {0}";

        private readonly Microsoft.Office.Interop.Word.Document nativeDocument;

        private readonly Microsoft.Office.Tools.Word.Document vstoDocument;

        private bool overwriteTemplate = false;

        public static ObservableCollection<ClauseBuilderQuestion> Questions { get; }

        public static ObservableCollection<string> DataFieldTitles { get; }
        public ObservableCollection<QuestionAdditionalAnswer> QuestionAdditionalAnswers { get; set; }
        public ObservableCollection<QuestionAdditionalAnswer> QuestionAdditionalResults { get; set; }
        public int richTextState = 1;
        public int RichTextState
        {
            get
            {
                return richTextState;
            }
            set
            {
                richTextState = value;
                OnPropertyChanged();
            }
        }
        private long RichTextID;

        private ContentControl richTextFalseContentControl;
        private ContentControl richTextTrueContentControl;
        private List<ContentControl> richTextAdditionalContentControls = new List<ContentControl>();

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        static ClauseBuilderUserControl()
        {
            Questions = new ObservableCollection<ClauseBuilderQuestion>();

            DataFieldTitles = new ObservableCollection<string>();
        }

        public ClauseBuilderUserControl()
        {
            InitializeComponent();

            LoadDataFieldsAndQuestions();

            QuestionAdditionalAnswers = new ObservableCollection<QuestionAdditionalAnswer>();
            QuestionAdditionalResults = new ObservableCollection<QuestionAdditionalAnswer>();

            nativeDocument = Globals.ThisAddIn.Application.ActiveDocument;

            vstoDocument = Globals.Factory.GetVstoObject(nativeDocument);

            DataContext = this;
        }

        public static void OnDocumentOpen(Microsoft.Office.Interop.Word.Document document)
        {
            LoadDataFieldsAndQuestions();
        }

        private static void LoadDataFieldsAndQuestions()
        {
            LoadDataFields();
            LoadSerializedQuestions();
        }

        public static void LoadDataFields()
        {
            DataFieldTitles.Clear();

            foreach (ContentControl control in Globals.ThisAddIn.Application.ActiveDocument.ContentControls)
            {
                if (control.Tag == DATA_FIELD_CONTROL_TAG)
                {
                    AddDataField(control.Title);
                }
            }
        }

        public static void LoadSerializedQuestions()
        {
            Questions.Clear();

            string serializedQuestions = DocumentPropertyUtil.ReadLongProperty(QUESTION_FILE_PROPERTY_NAME);

            if (serializedQuestions == null)
            {
                // There are no questions saved as document properties
                return;
            }

            var rawQuestions = JObject.Parse(serializedQuestions);

            foreach (KeyValuePair<string, JToken> question in rawQuestions)
            {
                Questions.Add(question.Value.ToObject<ClauseBuilderQuestion>());
            }
        }

        private void OnBtnCreateClauseGroupClick(object sender, System.Windows.RoutedEventArgs e)
        {
            string clauseGroupName = TbClauseGroupName.Text;

            string xml = ThisAddIn.Instance.Application.ActiveDocument.Content.WordOpenXML;
            ClauseLibraryHelper clh = new ClauseLibraryHelper(xml);
            var clauseGroups = clh.GetAllClauseGroups();

            if (clauseGroups.Where(x => x.Id == clauseGroupName).Count() == 0)
            {
                try
                {
                    RichTextContentControl control = vstoDocument.Controls.AddRichTextContentControl(GetSelection().Range, GetNewControlName());
                    control.Tag = CITATION_CONTROL_TAG;

                    control.Title = string.Format(GROUP_TITLE_TEMPLATE, clauseGroupName);

                    WdBuiltinStyle style;
                    if (int.TryParse(ClauseGroupLevel.Text, out int intResult))
                    {
                        style = (WdBuiltinStyle)(-1 - intResult);
                    }
                    else
                    {
                        style = WdBuiltinStyle.wdStyleHeading1;
                    }

                    control.Range.set_Style(style);
                    control.Range.Text = clauseGroupName;

                    control.Range.Paragraphs.Last.Range.InsertParagraphAfter();

                    Paragraph newParagraph = control.Range.Paragraphs.Last.Next();
                    newParagraph.Range.set_Style(WdBuiltinStyle.wdStyleNormal);

                    Range newParagraphStartRange = newParagraph.Range;
                    newParagraphStartRange.Collapse(WdCollapseDirection.wdCollapseStart);
                    newParagraphStartRange.Select();
                }
                catch (System.NotImplementedException)
                {
                    DocumentUtil.ShowCompatibilityModeError(new System.Threading.Tasks.Task(() => { OnBtnCreateClauseGroupClick(sender, e); }));
                }
            }
            else
            {
                InfowareConfirmationWindow icw = new InfowareConfirmationWindow(LanguageManager.GetTranslation(LanguageConstants.DocumentAlreadyContainsClauseGroup, "Document already contains a Clause Group with this name"),
                    LanguageManager.GetTranslation(LanguageConstants.DuplicateClauseGroup, "Duplicate Clause Group"), MessageBoxButton.OK);
                icw.ShowDialog();
            }
        }

        private void OnBtnCreateClauseClick(object sender, System.Windows.RoutedEventArgs e)
        {
            string clauseName = TbClauseName.Text;

            Range tempRange = GetSelection().Range.Duplicate;
            tempRange.Collapse(WdCollapseDirection.wdCollapseStart);
            tempRange.Start = 0;
            string xml = tempRange.WordOpenXML;
            ClauseLibraryHelper clh = new ClauseLibraryHelper(xml);
            var clauseGroups = clh.GetAllClauseGroups();
            if (clauseGroups.LastOrDefault() == null || clauseGroups.Last().Clauses.Where(x => x.Name == clauseName).Count() == 0)
            {
                try
                {
                    RichTextContentControl control = vstoDocument.Controls.AddRichTextContentControl(GetSelection().Range, GetNewControlName());
                    control.Tag = CITATION_CONTROL_TAG;

                    control.Title = string.Format(CITATION_TITLE_TEMPLATE, clauseName);

                    Range lastParagraphPange = control.Range.Paragraphs.Last.Range;
                    lastParagraphPange.Collapse(WdCollapseDirection.wdCollapseEnd);
                    lastParagraphPange.Select();
                }
                catch (System.NotImplementedException)
                {
                    DocumentUtil.ShowCompatibilityModeError(new System.Threading.Tasks.Task(() => { OnBtnCreateClauseClick(sender, e); }));
                }
            }
            else
            {
                InfowareConfirmationWindow icw = new InfowareConfirmationWindow(LanguageManager.GetTranslation(LanguageConstants.ClauseGroupAlreadyContainsClause, "Clause Group already contains a Clause with this name"),
                    LanguageManager.GetTranslation(LanguageConstants.DuplicateClause, "Duplicate Clause"), MessageBoxButton.OK);
                icw.ShowDialog();
            }
        }

        private void OnBtnInsertDataFieldClick(object sender, System.Windows.RoutedEventArgs e)
        {
            string dataFieldName = TbDataFieldName.Text;

            InsertDataField(dataFieldName);

            TbDataFieldName.Text = "";

            AddDataField(dataFieldName);
        }

        private void InsertDataField(string dataFieldName)
        {
            Range insertionRange = GetSelection().Range;
            insertionRange.Collapse(WdCollapseDirection.wdCollapseEnd);
            try
            {
                RichTextContentControl control = vstoDocument.Controls.AddRichTextContentControl(insertionRange, GetNewControlName());
                control.Tag = DATA_FIELD_CONTROL_TAG;

                control.PlaceholderText = dataFieldName;
                control.Title = dataFieldName;

                Range controlStartRange = control.Range;
                controlStartRange.Collapse();
                controlStartRange.Move(WdUnits.wdCharacter, -1);
                controlStartRange.Select();
            }
            catch (System.NotImplementedException)
            {
                DocumentUtil.ShowCompatibilityModeError(new System.Threading.Tasks.Task(() => { InsertDataField(dataFieldName); }));
            }
        }

        private static void AddDataField(string dataFieldTitle)
        {
            if (DataFieldTitles.Any(it => it == dataFieldTitle))
            {
                return;
            }

            DataFieldTitles.Add(dataFieldTitle);
        }

        private Selection GetSelection()
        {
            return Globals.ThisAddIn.Application.Selection;
        }

        private string GetNewControlName()
        {
            return string.Format(CITATION_CONTROL_NAME_TEMPLATE, Guid.NewGuid());
        }

        private void OnBtnReInsertDataFieldClick(object sender, System.Windows.RoutedEventArgs e)
        {
            string dataFieldName = GetSelectedDataFieldNameFromClickedButton(sender as Button);
            InsertDataField(dataFieldName);
        }

        private void OnBtnRemoveDataFieldClick(object sender, System.Windows.RoutedEventArgs e)
        {
            string dataFieldName = GetSelectedDataFieldNameFromClickedButton(sender as Button);

            DataFieldTitles.Remove(dataFieldName);

            var controlsToRemove = new List<ContentControl>();

            foreach (ContentControl control in nativeDocument.ContentControls)
            {
                if (control.Tag == DATA_FIELD_CONTROL_TAG && control.Title == dataFieldName)
                {
                    controlsToRemove.Add(control);
                }
            }

            controlsToRemove.ForEach(it => it.Delete(true));
        }

        private void OnBtnAddQuestionClick(object sender, System.Windows.RoutedEventArgs e)
        {
            var question = new ClauseBuilderQuestion(TbQuestionId.Text, TbQuestion.Text, TbAnswer1.Text, TbAnswer2.Text, this.QuestionAdditionalAnswers.Select(x => x.Answer).ToList());

            Questions.Add(question);
            this.QuestionAdditionalAnswers.Clear();

            SaveQuestionsAsDocumentProperty();
        }

        private void OnBtnDeleteQuestionClick(object sender, System.Windows.RoutedEventArgs e)
        {
            var selectedItem = (ContentPresenter)ListQuestions.ItemContainerGenerator.ContainerFromItem(((Button)sender).DataContext);

            string questionId = ((ClauseBuilderQuestion)selectedItem.Content).Variable;

            Questions.Remove(Questions.First(it => it.Variable == questionId));

            if (Questions.Count == 0)
            {
                DocumentPropertyUtil.RemovePropertiesByNameSuffix(QUESTION_FILE_PROPERTY_NAME);
            }
            else
            {
                SaveQuestionsAsDocumentProperty();
            }
        }

        private void OnBtnCreateAskDataFieldCodeClick(object sender, RoutedEventArgs e)
        {
            string selectedQuestionId = ((ClauseBuilderQuestion)ComboQuestionId.SelectedItem).Variable;

            string result1 = TbResult1.Text;
            string result2 = TbResult2.Text;

            string additionalResults = string.Join("|", this.QuestionAdditionalResults.Select(x=>x.Answer));

            if (!string.IsNullOrEmpty(additionalResults))
            {
                additionalResults = "|" + additionalResults;
            }

            string code = $"[{selectedQuestionId}~AskRef;{result1}|{result2}{additionalResults}]";

            int cursorPosition = TbAskDataFieldCode.CaretIndex;

            TbAskDataFieldCode.Text = TbAskDataFieldCode.Text.Insert(cursorPosition, code);

            TbResult1.Text = TbResult2.Text = "";
        }

        private void OnBtnAddAskDataFieldToDocumentClick(object sender, RoutedEventArgs e)
        {
            string code = TbAskDataFieldCode.Text;

            Range selectionRange = GetSelection().Range;
            selectionRange.Collapse(WdCollapseDirection.wdCollapseEnd);
            try
            {
                var control = vstoDocument.Controls.AddRichTextContentControl(selectionRange, GetNewControlName());
                control.Tag = DATA_FIELD_CODE_CONTROL_TAG;
                control.PlaceholderText = code;
                control.Title = code;

                Range controlStartRange = control.Range;
                controlStartRange.Collapse();
                controlStartRange.Move(WdUnits.wdCharacter, -1);
                controlStartRange.Select();

                TbAskDataFieldCode.Text = "";
            }
            catch (System.NotImplementedException)
            {
                DocumentUtil.ShowCompatibilityModeError(new System.Threading.Tasks.Task(() => { OnBtnAddAskDataFieldToDocumentClick(sender, e); }));
            }
        }

        private void OnBtnUploadFileClick(object sender, RoutedEventArgs e)
        {
            overwriteTemplate = false;
            string fullDocumentPath = SaveDocument();

            if (fullDocumentPath != null)
            {
                UploadDocument(fullDocumentPath, overwriteTemplate);
            }
        }

        private void SaveQuestionsAsDocumentProperty()
        {
            var root = new JObject();

            foreach (ClauseBuilderQuestion question in Questions)
            {
                root.Add(question.Variable, JObject.FromObject(question));
            }

            string serializedQustions = JsonConvert.SerializeObject(root);

            DocumentPropertyUtil.SaveLongProperty(QUESTION_FILE_PROPERTY_NAME, serializedQustions);
        }

        private string GetSelectedDataFieldNameFromClickedButton(Button button)
        {
            var selectedItem = (ListBoxItem)ListDataFields.ItemContainerGenerator.ContainerFromItem(button.DataContext);

            return (string)selectedItem.Content;
        }

        private string SaveDocument()
        {
            string newDocumentName = TbFileName.Text;

            if (!newDocumentName.EndsWith(".docx"))
            {
                newDocumentName += ".docx";
            }

            bool templateAlreadyExists = BaseUtils.CheckIfTemplateAlreadyExists(newDocumentName, (TemplateType)TEMPLATE_TYPE);

            if (templateAlreadyExists)
            {
                var confirmationWindow = new InfowareConfirmationWindow(LanguageManager.GetTranslation(LanguageConstants.ClauseLibraryAlreadyExistsOverwrite, "A Clause Library with the same name already exists.\r\nDo you want to overwrite it?"), LanguageManager.GetTranslation(LanguageConstants.ClauseLibraryAlreadyExists, "Clause Library already exists"), MessageBoxButton.YesNo);
                confirmationWindow.ShowDialog();

                if (confirmationWindow.Result == MessageBoxResult.Yes)
                {
                    overwriteTemplate = true;
                }
            }

            if (!templateAlreadyExists || overwriteTemplate)
            {
                string tempDirectoryPath = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
                Directory.CreateDirectory(tempDirectoryPath);

                string fullDocumentPath = Path.Combine(tempDirectoryPath, newDocumentName);
                return DocumentUtil.SaveCopyAs(fullDocumentPath);
            }
            else
            {
                return null;
            }
        }

        private void UploadDocument(string fullDocumentPath, bool overwriteTemplate = false)
        {
            object authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);

            if (authKey != null)
            {
                var client = new RestClient(ConfigSettings.WebApiUrl);

                string relativeUrl = $"api/WebApi/UploadTemplate?authKey={authKey}&type={TEMPLATE_TYPE}";
                if (overwriteTemplate)
                {
                    relativeUrl = $"{relativeUrl}&overwrite=true";
                }

                var request = new RestRequest(relativeUrl, Method.POST);
                request.AddFile(TEMPLATE_FILE_FORM_DATA_NAME, fullDocumentPath);
                IRestResponse response = client.Execute(request);

                File.Delete(fullDocumentPath);
                Directory.Delete(Path.GetDirectoryName(fullDocumentPath));

                if (!response.IsSuccessful)
                {
                    ShowTemplateUploadMessage(false);
                    return;
                }

                JObject content;

                try
                {
                    content = JsonConvert.DeserializeObject<JObject>(response.Content);
                }
                catch (JsonReaderException)
                {
                    // The response didn't contain valid JSON
                    ShowTemplateUploadMessage(false);
                    return;
                }

                int statusCode = content["StatusCode"].ToObject<int>();

                if (statusCode == 200)
                {
                    ShowTemplateUploadMessage(true);
                    return;
                }
                else
                {
                    ShowTemplateUploadMessage(false);
                }
            }
        }

        private void ShowTemplateUploadMessage(bool uploadWasSuccessful)
        {
            if (uploadWasSuccessful)
            {
                TbSuccessfulUpload.Visibility = Visibility.Visible;
                TbUnsuccessfulUpload.Visibility = Visibility.Collapsed;
            }
            else
            {
                TbUnsuccessfulUpload.Visibility = Visibility.Visible;
                TbSuccessfulUpload.Visibility = Visibility.Collapsed;
            }
        }

        private void MakeSelectionFalse_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ContentControl control = nativeDocument.ContentControls.Add(WdContentControlType.wdContentControlRichText, GetSelection().Range);
                control.Tag = ASK_RTF_RESULT_TAG;
                Random rand = new Random();

                RichTextID = rand.Next();
                RichTextID = (RichTextID << 32);
                RichTextID = RichTextID | (long)rand.Next();

                control.Title = string.Format("{0}#{1}", "Result" + RichTextState, RichTextID);

                richTextFalseContentControl = control;
                RichTextState = 2;
            }
            catch (System.NotImplementedException)
            {
                DocumentUtil.ShowCompatibilityModeError(new System.Threading.Tasks.Task(() => { MakeSelectionFalse_Click(sender, e); }));
            }
        }

        private void MakeSelectionTrue_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ContentControl control = nativeDocument.ContentControls.Add(WdContentControlType.wdContentControlRichText, GetSelection().Range);
                control.Tag = ASK_RTF_RESULT_TAG;

                control.Title = string.Format("{0}#{1}", "Result" + RichTextState, RichTextID);

                richTextTrueContentControl = control;

                if((ComboQuestionId.SelectedItem as ClauseBuilderQuestion).AdditionalAnswers?.Count > 0)
                {
                    RichTextState = 3;
                }
                else
                {
                    RichTextState = 0;
                }
            }
            catch (System.NotImplementedException)
            {
                DocumentUtil.ShowCompatibilityModeError(new System.Threading.Tasks.Task(() => { MakeSelectionTrue_Click(sender, e); }));
            }
        }

        private void CreateAskField_Click(object sender, RoutedEventArgs e)
        {
            List<ContentControl> allContentControls = new List<ContentControl>(richTextAdditionalContentControls);
            allContentControls.Add(richTextFalseContentControl);
            allContentControls.Add(richTextTrueContentControl);

            int min = allContentControls.Min(x => x.Range.Start);
            min--;

            int max = allContentControls.Max(x => x.Range.End);
            max++;

            Range range = nativeDocument.Range(min, max);
            try
            {
                string selectedQuestionId = (ComboQuestionId.SelectedItem as ClauseBuilderQuestion)?.Variable;

                ContentControl control = nativeDocument.ContentControls.Add(WdContentControlType.wdContentControlRichText, range);
                control.Tag = ASK_RTF_FIELD_TAG;

                control.Title = string.Format("{0}#{1}", selectedQuestionId, RichTextID);

                RichTextState = 1;
                richTextTrueContentControl = null;
                richTextFalseContentControl = null;
                richTextAdditionalContentControls.Clear();
            }
            catch (System.NotImplementedException)
            {
                DocumentUtil.ShowCompatibilityModeError(new System.Threading.Tasks.Task(() => { MakeSelectionTrue_Click(sender, e); }));
            }
        }

        private void AskCancel_Click(object sender, RoutedEventArgs e)
        {
            if (richTextFalseContentControl != null)
            {
                richTextFalseContentControl.Delete();
            }

            if(richTextTrueContentControl != null)
            {
                richTextTrueContentControl.Delete();
            }

            foreach (ContentControl contentControl in richTextAdditionalContentControls)
            {
                contentControl.Delete();
            }

            richTextAdditionalContentControls.Clear();

            RichTextState = 1;
        }

        private void AddQuestionAnswer_Click(object sender, RoutedEventArgs e)
        {
            this.QuestionAdditionalAnswers.Add(new QuestionAdditionalAnswer() {Number =( QuestionAdditionalAnswers.LastOrDefault()?.Number ?? 2) + 1 });
        }

        private void RemoveAdditionalAnswer_Click(object sender, RoutedEventArgs e)
        {
            var answer = (sender as Button)?.DataContext as QuestionAdditionalAnswer;

            if (answer != null)
            {
                int index = this.QuestionAdditionalAnswers.IndexOf(answer);

                for(int i = index + 1; i < QuestionAdditionalAnswers.Count; i++)
                {
                    QuestionAdditionalAnswers[i].Number--;
                }

                QuestionAdditionalAnswers.Remove(answer);
            }
        }

        private void ComboQuestionId_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var question = this.ComboQuestionId.SelectedItem as ClauseBuilderQuestion;

            if (question != null)
            {
                this.QuestionAdditionalResults.Clear();

                for (int i = 0; i <  question.AdditionalAnswers?.Count; i++)
                {
                    this.QuestionAdditionalResults.Add(new QuestionAdditionalAnswer()
                        {
                            Number = i + 3
                        }
                    );
                }
            }
        }

        private void MakeSelectionOther_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ContentControl control = nativeDocument.ContentControls.Add(WdContentControlType.wdContentControlRichText, GetSelection().Range);
                control.Tag = ASK_RTF_RESULT_TAG;

                control.Title = string.Format("{0}#{1}", "Result" + RichTextState, RichTextID);

                this.richTextAdditionalContentControls.Add(control);

                if ((ComboQuestionId.SelectedItem as ClauseBuilderQuestion).AdditionalAnswers?.Count > RichTextState - 2)
                {
                    RichTextState++;
                }
                else
                {
                    RichTextState = 0;
                }
            }
            catch (System.NotImplementedException)
            {
                DocumentUtil.ShowCompatibilityModeError(new System.Threading.Tasks.Task(() => { MakeSelectionTrue_Click(sender, e); }));
            }
        }
    }

    public class QuestionAdditionalAnswer : INotifyPropertyChanged
    {
        public int number;
        public int Number
        {
            get
            {
                return number;
            }
            set
            {
                number = value;
                OnPropertyChanged("NumberText");
            }
        }

        public string NumberText
        {
            get
            {
                return Number.ToString() + ":";
            }
        }

        public string Answer { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

    }
}
