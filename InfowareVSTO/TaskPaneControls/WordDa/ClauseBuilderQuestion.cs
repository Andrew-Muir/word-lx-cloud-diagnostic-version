﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace InfowareVSTO.TaskPaneControls.WordDa
{
    public class ClauseBuilderQuestion
    {
        public string Variable { get; set; }

        public string Question { get; set; }

        public string Answer1 { get; set; }

        public string Answer2 { get; set; }

        public List<string> AdditionalAnswers { get; set; }

        [IgnoreDataMember]
        [JsonIgnore]
        public List<QuestionAdditionalAnswer> AdditionalAnswersWithNr
        {
            get
            {
                List<QuestionAdditionalAnswer> result = new List<QuestionAdditionalAnswer>();
                if (AdditionalAnswers != null)
                {
                    for (int i = 0; i < AdditionalAnswers.Count; i++)
                    {
                        result.Add(new QuestionAdditionalAnswer() { Answer = AdditionalAnswers[i], Number = i + 3 });
                    }
                }

                return result;
            }
        }

        public ClauseBuilderQuestion(string id, string question, string answer1, string answer2, List<string> additionalAnswers)
        {
            Variable = id;
            Question = question;
            Answer1 = answer1;
            Answer2 = answer2;
            AdditionalAnswers = additionalAnswers;
        }
    }
}
