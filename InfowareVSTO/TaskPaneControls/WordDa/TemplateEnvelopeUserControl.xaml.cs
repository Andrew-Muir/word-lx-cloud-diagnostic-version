﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Controls;
using InfowareVSTO.Common;
using InfowareVSTO.Common.DTOs;
using InfowareVSTO.Common.General;
using InfowareVSTO.Common.Language;
using InfowareVSTO.Common.Models;
using InfowareVSTO.DataSource;
using InfowareVSTO.Letterhead;
using InfowareVSTO.MultiLanguage;
using InfowareVSTO.Util;
using InfowareVSTO.VSTOAuthor;
using InfowareVSTO.Windows;
using Microsoft.Office.Core;
using Microsoft.Office.Interop.Word;

namespace InfowareVSTO.TaskPaneControls.WordDa
{
    /// <summary>
    /// Interaction logic for TemplateLabelUserControl.xaml
    /// </summary>
    public partial class TemplateEnvelopeUserControl : UserControl, IRefreshData
    {
        private List<SelectedContact> AddedRecipients { get; set; }
        private AuthorDTO Author { get; set; }
        private AuthorDTO Assistant { get; set; }
        public Range Selection { get; set; }
        private DataSourceValues dataSourceValues;
        private int? previousOfficeLocationId;
        private List<string> DefaultSalutations { get; set; }
        private List<string> DefaultExcludedSalutations { get; set; }
        private List<string> Salutations { get; set; }
        public List<string> ClosingStatements { get; private set; }
        public int DefaultClosingIndex { get; private set; }
        private string closingEndChar;
        private List<string> DefaultEnclosureSettings { get; set; }
        public List<KeyValuePair<System.Windows.Controls.Control, string>> FieldFunctionList { get; private set; }

        public TemplateEnvelopeUserControl()
        {
            InitializeComponent();
            this.MainContentControl.Content = new DataFillInnerUserControl(false);
            AddedRecipients = new List<SelectedContact>();

            DataContext = this;
            SettingsManager.PopulateComboboxWithHandlingOptions(this.comboBoxHandling);
            SettingsManager.PopulateComboboxWithDeliveryOptions(this.comboBoxDelivery);
            Utils.UpdateCurrentCultureShortDateFormat();
            dateTimePickerDate.SelectedDate = DateTime.Today;
            var author = AdminPanelWebApi.GetCurrentAuthor();
            if (author != null)
            {
                Author = author.ToAuthorDTO();
                textBoxAuthor.Text = Author?.FirstName + " " + Author?.LastName;
            }

            int assistantId = AdminPanelWebApi.GetCurrentAsistantId();
            if (assistantId > 0)
            {
                var assistant = AdminPanelWebApi.GetAuthor(assistantId);
                if (assistant != null)
                {
                    Assistant = assistant.ToAuthorDTO();
                    textBoxAssistant.Text = Assistant?.FirstName + " " + Assistant?.LastName;
                }
            }
            comboBoxOfficeLocation.ItemsSource = Utils.GetLocationsForCombobox();

            UpdateClosingStatements();
            UpdateDefaultClosingIndex();
            UpdateClosingEndChar();

            DefaultEnclosureSettings = new List<string> { "Encl,Encls", "Attch,Attchs" };

            var enclosures = GetEnclosureList();
            if (enclosures != null)
            {
                var enclosureOptions = BaseUtils.GetEnclosureOptions(enclosures);
                if (enclosureOptions != null && enclosureOptions.Count > 0)
                {
                    comboBoxEnclosures.ItemsSource = enclosureOptions;
                    comboBoxEnclosures.DisplayMemberPath = "Key";
                    comboBoxEnclosures.SelectedValuePath = "Value";
                    comboBoxEnclosures.SelectedIndex = 0;
                }
            }

            int defaultLocationId = -1;
            var currentAuthorSettings = ManageUsers.CurrentAuthorSettings;
            if (currentAuthorSettings.LocationId.HasValue)
            {
                defaultLocationId = currentAuthorSettings.LocationId.Value;
            }
            else
            {
                int? companyId = currentAuthorSettings.CompanyId ?? DocumentPropertyUtil.GetTemplateCompanyID();

                if (companyId.HasValue)
                {
                    defaultLocationId = AdminPanelWebApi.GetCompanyDetails(companyId.Value).Id;
                }
            }

            if (defaultLocationId >= 0)
            {
                comboBoxOfficeLocation.SelectedValue = defaultLocationId;
            }

            DefaultSalutations = new List<string> { "Counsel", "Mr.", "Ms.", "Mrs.", "Mr/Ms.", "Mr/Mrs.", "Mx.", "Madame", "Mesdames", "Sir", "Sirs", "Sir/Madame", "Sirs/Mesdames" };
            DefaultExcludedSalutations = new List<string> { "Counsel", "Sir", "Madame", "Sir/Madame", "Sirs/Mesdames", "Sirs", "Mesdames" };

            Salutations = GetSalutations();
            comboBoxSalutation.ItemsSource = Salutations;
            OrderFields();
            LoadAnswers();
        }

        private void OrderFields()
        {
            string json = SettingsManager.GetSetting("TaskPaneFieldOrder", "Letters", null);
            TaskpaneFieldOrderer tfo = new TaskpaneFieldOrderer();
            tfo.OrderFields(json, this.FieldContainer);
        }

        private void InitFieldValidation()
        {
            string settingStr = SettingsManager.GetSetting("FieldsRequiringValidation", "Letters");
            List<KeyValuePair<System.Windows.Controls.Control, string>> result = new List<KeyValuePair<System.Windows.Controls.Control, string>>();
            if (settingStr != null)
            {
                List<string> pairs = settingStr.Split(';').Select(x => x.Trim()).ToList();

                foreach (string pair in pairs)
                {
                    List<string> pairList = pair.Split(',').Select(x => x.Trim()).ToList();

                    if (pairList.Count > 1)
                    {
                        System.Windows.Controls.Control control = TranslateFieldToControl(pairList[0]);

                        if (control != null)
                        {
                            result.Add(new KeyValuePair<System.Windows.Controls.Control, string>(control, pairList[1]));

                            control.LostKeyboardFocus += Control_LostKeyboardFocus;
                        }
                    }
                }
            }

            this.FieldFunctionList = result;
        }

        private void Control_LostKeyboardFocus(object sender, System.Windows.Input.KeyboardFocusChangedEventArgs e)
        {
            string functionName = this.FieldFunctionList.Where(x => x.Key == sender as System.Windows.Controls.Control).FirstOrDefault().Value;

            if (functionName != null)
            {
                string value = null;
                if (sender is TextBox)
                {
                    value = (sender as TextBox).Text;
                }
                else if (sender is ComboBox)
                {
                    value = (sender as ComboBox).Text;
                }

                if (!string.IsNullOrEmpty(value))
                {
                    bool fieldValid = Utils.ValidateField(functionName, value);

                    if (!fieldValid)
                    {
                        InfowareInformationWindow iiw = new InfowareInformationWindow(LanguageManager.GetTranslation(LanguageConstants.InputIsNotValidPleaseReEnter, "Input is invalid, please re-enter."),
                           LanguageManager.GetTranslation(LanguageConstants.InputIsNotValid, "Invalid Input"), WordDefaultIconType.Warning);
                        iiw.ShowDialog();
                        if (sender is TextBox)
                        {
                            (sender as TextBox).Text = string.Empty;
                        }
                        else if (sender is ComboBox)
                        {
                            (sender as ComboBox).SelectedIndex = -1;
                            (sender as ComboBox).Text = string.Empty;
                        }
                    }
                }
            }
        }

        private System.Windows.Controls.Control TranslateFieldToControl(string fieldName)
        {
            switch (fieldName)
            {
                case "Recipient(s)":
                    return textBoxRecipients;
                case "Salutation":
                    return comboBoxSalutation;
                case "Our File Number":
                    return textBoxFileNumber;
                case "Their File Number":
                    return textBoxClientFileNumber;
                case "Delivery":
                    return comboBoxDelivery;
                case "Handling":
                    return comboBoxHandling;
                case "Attention":
                    return textBoxAttention;
                case "Office Location":
                    return comboBoxOfficeLocation;
                case "Re Line":
                    return textBoxReLine;
                case "Closing":
                    return comboBoxClosing;
                case "Enclosure(s)":
                    return comboBoxEnclosures;
                case "EnclosureNo":
                    return numericUpDownNumber;
            }
            return null;
        }

        private void UpdateClosingStatements()
        {
            const char pipeSeparator = '|';

            string rawClosingStatements = SettingsManager.GetSetting("Closings", "Glossary", null, MLanguageUtil.ActiveDocumentLanguage);

            if (rawClosingStatements == null)
            {
                ClosingStatements = new List<string>()
                {
                    "Yours truly",
                    "Yours very truly",
                    "Sincerely",
                    "Regards",
                    "Best regards",
                    "Kindest regards"
                };

                comboBoxClosing.ItemsSource = ClosingStatements;
                return;
            }

            char separator;

            if (rawClosingStatements.Contains(pipeSeparator))
            {
                separator = pipeSeparator;
            }
            else
            {
                separator = ',';
            }

            ClosingStatements = SettingsManager.UnescapeCommaCharacter(rawClosingStatements.Split(separator)).Select(it => it.Trim()).ToList();
            comboBoxClosing.ItemsSource = ClosingStatements;
        }

        private void UpdateDefaultClosingIndex()
        {
            //try to get it from current author
            var author = AdminPanelWebApi.GetCurrentAuthor();
            if (!string.IsNullOrEmpty(author?.DefaultClosing))
                DefaultClosingIndex = ClosingStatements.IndexOf(author.DefaultClosing);
            else
                DefaultClosingIndex = -1;

            if (DefaultClosingIndex < 0)
                DefaultClosingIndex = SettingsManager.GetSettingAsInt("ClosingsDefaultIdx", "Glossary", languageId: MLanguageUtil.ActiveDocumentLanguage);

            if (DefaultClosingIndex > ClosingStatements.Count - 1)
            {
                DefaultClosingIndex = -1;
            }

            comboBoxClosing.SelectedIndex = DefaultClosingIndex;
        }

        private void UpdateClosingEndChar()
        {
            closingEndChar = SettingsManager.GetSetting("ClosingEndChar", "Glossary", null, MLanguageUtil.ActiveDocumentLanguage);

            if (closingEndChar == null)
            {
                closingEndChar = ",";
            }
        }


        public void LoadAnswers(PaneDTO envelopeDTO = null, bool fromDS = false)
        {
            if (envelopeDTO == null)
            {
                if (fromDS)
                {
                    envelopeDTO = TaskPaneUtil.GetDataSourceAnsweresDTO(TaskPaneUtil.LoadAnswers()).PaneDTO;
                    (this.MainContentControl.Content as DataFillInnerUserControl)?.ApplyDataSourceValues(dataSourceValues?.CustomFields);
                }
                else
                {
                    dataSourceValues = TaskPaneUtil.GetDataSourceAnsweresDTO();
                    envelopeDTO = TaskPaneUtil.GetNewPaneDTO(dataSourceValues);
                    envelopeDTO = TaskPaneUtil.LoadAnswers(envelopeDTO);
                    if (dataSourceValues?.PaneDTO != null && TaskPaneUtil.AnswersFound)
                    {
                        this.LoadDS.Visibility = System.Windows.Visibility.Visible;
                    }
                    else
                    {
                        (this.MainContentControl.Content as DataFillInnerUserControl)?.ApplyDataSourceValues(dataSourceValues?.CustomFields);
                    }
                }
            }

            if (envelopeDTO != null)
            {
                textBoxFileNumber.Text = envelopeDTO.FileNumber;
                textBoxClientFileNumber.Text = envelopeDTO.ClientFileNumber;
                textBoxAttention.Text = envelopeDTO.Attention;
                textBoxReLine.Text = envelopeDTO.ReLine;
                numericUpDownNumber.Text = envelopeDTO.Number.ToString();
                if (envelopeDTO.Recipients == null && envelopeDTO.AddedRecipients != null)
                {
                    List<string> recipientsArr = new List<string>();
                    CreateRecipientText(envelopeDTO.AddedRecipients, recipientsArr);
                    textBoxRecipients.Text = string.Join("\r\n\r\n", recipientsArr);
                }
                else
                {
                    textBoxRecipients.Text = envelopeDTO.Recipients;
                }
                AddedRecipients = envelopeDTO.AddedRecipients;
                dateTimePickerDate.SelectedDate = envelopeDTO.Date;
                Utils.SetComboBoxValue(comboBoxDelivery, envelopeDTO.Delivery);
                Utils.SetComboBoxValue(comboBoxHandling, envelopeDTO.Handling);
                Utils.SetComboBoxValue(comboBoxSalutation, envelopeDTO.Salutation);
                Utils.SetComboBoxValue(comboBoxClosing, envelopeDTO.Closing);
                Utils.SetComboBoxValue(comboBoxEnclosures, envelopeDTO.Enclosures);
                var envelopeAuthor = envelopeDTO.Author?.ToAuthorDTO();
                if (envelopeAuthor != null)
                {
                    Author = envelopeAuthor;

                    var envelopeAuthorFullName = envelopeAuthor?.FirstName + " " + envelopeAuthor?.LastName;
                    if (!string.IsNullOrWhiteSpace(envelopeAuthorFullName))
                    {
                        textBoxAuthor.Text = envelopeAuthorFullName;
                    }
                }

                var envelopeAssistant = envelopeDTO.Assistant?.ToAuthorDTO();
                if (envelopeAssistant != null)
                {
                    Assistant = envelopeAssistant;

                    var envelopeAssistantFullName = envelopeAssistant?.FirstName + " " + envelopeAssistant?.LastName;
                    if (!string.IsNullOrWhiteSpace(envelopeAssistantFullName))
                    {
                        textBoxAssistant.Text = envelopeAssistantFullName;
                    }
                }
                Utils.SetComboBoxValue(comboBoxOfficeLocation, envelopeDTO.OfficeLocation);
                if (envelopeDTO.OfficeLocationId.HasValue)
                {
                    this.comboBoxOfficeLocation.SelectedValue = envelopeDTO.OfficeLocationId.Value;
                    this.previousOfficeLocationId = envelopeDTO.OfficeLocationId;
                }
      
            }
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            int.TryParse(numericUpDownNumber.Text, out int number);
            ChangeEnclosureComboBox(number, comboBoxEnclosures.Text);
            var envelopeDTO = new PaneDTO
            {
                Type = TemplateType.Envelope,
                FileNumber = textBoxFileNumber.Text,
                ClientFileNumber = textBoxClientFileNumber.Text,
                CompanyId = DocumentPropertyUtil.GetTemplateCompanyID(),
                Delivery = comboBoxDelivery.SelectedValue is string ? comboBoxDelivery.SelectedValue as string : comboBoxDelivery.Text,
                Handling = comboBoxHandling.SelectedValue is string ? comboBoxHandling.SelectedValue as string : comboBoxHandling.Text,
                Attention = textBoxAttention.Text,
                Enclosures = comboBoxEnclosures.SelectedValue is string ? comboBoxEnclosures.SelectedValue as string : comboBoxEnclosures.Text,
                Number = number,
                Recipients = textBoxRecipients.Text.Replace("\r\n", "\v").Replace("\f", ""),
                AddedRecipients = AddedRecipients,
                ReLine = textBoxReLine.Text,
                Salutation = comboBoxSalutation.Text,
                FileNumberTagSetting = SettingsManager.GetSetting("FileNoTag", "Letters", null, MLanguageUtil.ActiveDocumentLanguage),
                Date = dateTimePickerDate.SelectedDate ?? DateTime.Today,
                Closing = comboBoxClosing.Text,
                ClosingEndChar = closingEndChar,
                OfficeLocationId = comboBoxOfficeLocation.SelectedValue as int?,
                IncludeContactInformationDeliverySetting = SettingsManager.GetSetting("IncludeContactInformationDelivery", "Letters", null, MLanguageUtil.ActiveDocumentLanguage),
                AttentionTranslation = MLanguageUtil.GetResource(MLanguageResourceEnum.Attention)
            };
            MsoLanguageID languageID = MLanguageUtil.ActiveDocumentLanguage;
            AuthorWithPathsDTO selectedAuthor;
            AuthorWithPathsDTO selectedAssistant;
            if (Author == null)
                selectedAuthor = new AuthorWithPathsDTO();
            else
                selectedAuthor = AdminPanelWebApi.GetAuthor(Author.Id, languageID);
            if (Assistant == null)
                selectedAssistant = new AuthorWithPathsDTO();
            else
                selectedAssistant = AdminPanelWebApi.GetAuthor(Assistant.Id, languageID);

            envelopeDTO.Author = selectedAuthor;
            //letterDTO.Author.FirstName = textBoxAuthor.Text;
            envelopeDTO.Assistant = selectedAssistant;


            envelopeDTO = MLanguageUtil.ApplyDateSettings(envelopeDTO);
            PaneUtils.InsertGeneralTemplateDTO(ThisAddIn.Instance.Application.ActiveDocument, envelopeDTO, TemplateType.Envelope);
            (MainContentControl.Content as DataFillInnerUserControl)?.OnUpdateButtonClick();
            TaskPaneUtil2.TryRestoreSelection(this);
            TaskPaneUtil.SaveAnswers(envelopeDTO);
            if (this.comboBoxOfficeLocation.SelectedValue as int? != null && previousOfficeLocationId != null && this.comboBoxOfficeLocation.SelectedValue as int? != previousOfficeLocationId)
            {
                new ExistingLetterhead().ChangeLetterhead((comboBoxOfficeLocation.SelectedValue as int?).Value);
            }
        }

        private void numericUpDownNumber_PreviewTextInput(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            Regex regex = new Regex("^[0-9]*$");
            if (!regex.IsMatch(e.Text))
            {
                e.Handled = true;
            }
        }

        private void ChangeEnclosureComboBox(int number, string enclosureOption)
        {
            var enclosures = GetEnclosureList();
            if ((number > 0 && !string.IsNullOrEmpty(enclosureOption)) & (enclosures != null && enclosures.Count > 0))
            {
                foreach (var enclosure in enclosures)
                {
                    if (number == 1 && enclosure.PluralValue == enclosureOption)
                    {
                        comboBoxEnclosures.SelectedValue = enclosure.SingularValue;
                        return;
                    }
                    else if (number > 1 && enclosure.SingularValue == enclosureOption)
                    {
                        comboBoxEnclosures.SelectedValue = enclosure.PluralValue;
                        return;
                    }
                }
            }
        }

        private List<Enclosure> GetEnclosureList()
        {
            var enclosureSettings = SettingsManager.GetSetting("Enclosures", "Letters", null, MLanguageUtil.ActiveDocumentLanguage);
            if (!string.IsNullOrEmpty(enclosureSettings))
            {
                return BaseUtils.ParseEnclosureSettings(enclosureSettings);
            }
            return BaseUtils.ParseEnclosureSettings(string.Join(";", DefaultEnclosureSettings));
        }


        private void buttonToday_Click(object sender, EventArgs e)
        {
            dateTimePickerDate.SelectedDate = DateTime.Today;
        }

        private void buttonFindAuthor_Click(object sender, EventArgs e)
        {
            SelectAuthor_WPF selectAuthor_WPF = new SelectAuthor_WPF();
            selectAuthor_WPF.ShowDialog();
            if (selectAuthor_WPF.ResultOk)
            {
                Author = selectAuthor_WPF.SelectedAuthor;
                textBoxAuthor.Text = Author.FirstName + " " + Author.LastName;
            }
        }

        private void buttonFindAssistant_Click(object sender, EventArgs e)
        {
            SelectAuthor_WPF selectAuthor_WPF = new SelectAuthor_WPF();
            selectAuthor_WPF.ShowDialog();
            if (selectAuthor_WPF.ResultOk)
            {
                Assistant = selectAuthor_WPF.SelectedAuthor;
                textBoxAssistant.Text = Assistant.FirstName + " " + Assistant.LastName;
            }
        }

        private void buttonFindRecipients_Click(object sender, EventArgs e)
        {
            var importContacts_WPF = new ImportContacts_WPF(RecipientType.MultiRow, true, oldContacts: AddedRecipients);
            importContacts_WPF.ShowDialog();

            var recipients = new List<string>();
            if (importContacts_WPF.ResultOk)
            {
                BaseUtils.RemoveContactsFromTextBoxes(importContacts_WPF.RemovedContacts, textBoxRecipients, textBoxAttention, AddedRecipients);
                CreateRecipientText(importContacts_WPF.SelectedContacts, recipients, importContacts_WPF.JobTitleVisibility, importContacts_WPF.CompanyNameVisibility, importContacts_WPF.DepartmentVisibility);
            }
            var recipientsStr = string.Join("\r\n\r\n", recipients);
            if (!string.IsNullOrEmpty(recipientsStr))
            {
                if (textBoxRecipients.Text.Trim().IsEmpty())
                {
                    textBoxRecipients.Text = recipientsStr;
                }
                else
                {
                    textBoxRecipients.Text = textBoxRecipients.Text + "\r\n\r\n" + recipientsStr;
                }
            }
        }

        private void CreateRecipientText(List<SelectedContact> selectedContacts, List<string> recipients, JobTitleVisibility jobTitleVisibility = JobTitleVisibility.NoJobTitle, bool companyNameVisibility = true, bool departmentVisibility = false)
        {
            foreach (var recipient in selectedContacts)
            {
                var grouppedLine = new List<string>();
                var fullName = new List<string>();

                bool departmentAfterCompany = SettingsManager.GetSettingAsBool("RecipientDepartmentAfterCompany", "Letters", false);

                if (!recipient.Prefix.IsEmpty())
                {
                    fullName.Add(recipient.Prefix);
                }

                if (!recipient.FirstName.IsEmpty())
                {
                    fullName.Add(recipient.FirstName);
                }

                if (!recipient.LastName.IsEmpty())
                {
                    fullName.Add(recipient.LastName);
                }
                if (!recipient.Suffix.IsEmpty())
                {
                    fullName.Add(recipient.Suffix);
                }

                string fullNameStr = string.Join(" ", fullName.ToArray());
                if (jobTitleVisibility == JobTitleVisibility.SameRow && !string.IsNullOrEmpty(recipient.JobTitle))
                {
                    fullNameStr += ", " + recipient.JobTitle;
                }

                if (!recipient.IsSelected && fullName.Count > 0)
                {
                    grouppedLine.Add(fullNameStr);
                }
                if (!recipient.IsSelected && !recipient.JobTitle.IsEmpty() && jobTitleVisibility == JobTitleVisibility.NextRow)
                {
                    grouppedLine.Add(recipient.JobTitle);
                }

                if (!recipient.IsSelected && !recipient.Department.IsEmpty() && departmentVisibility && !departmentAfterCompany)
                {
                    grouppedLine.Add(recipient.Department);
                }

                if (companyNameVisibility == true)
                {
                    var companies = recipient.Companies;
                    if (companies != null && companies.Count > 0)
                    {
                        var company = companies.OrderBy(x => x.IsDefault).FirstOrDefault();
                        if (company != null && !string.IsNullOrEmpty(company.Name))
                        {
                            grouppedLine.Add(company.Name);
                        }
                    }
                    else if (!string.IsNullOrWhiteSpace(recipient.CompanyName))
                    {
                        grouppedLine.Add(recipient.CompanyName);
                    }
                }

                if ( !recipient.Department.IsEmpty() && departmentVisibility && departmentAfterCompany)
                {
                    grouppedLine.Add(recipient.Department);
                }

                var address = BaseUtils.GetAuthorAddress(recipient);
                if (address != null && !string.IsNullOrWhiteSpace(address.Address1))
                    grouppedLine.Add(string.Join("\r\n", address.LinesFormat));

                if (recipient.IsSelected)
                {
                    if (grouppedLine.Count > 0)
                    {
                        textBoxAttention.Text += BaseUtils.GetAttentionLine(recipient, jobTitleVisibility, departmentVisibility && !departmentAfterCompany) + "\n";
                    }
                    else
                    {
                        if (fullName.Count > 0)
                        {
                            grouppedLine.Add(fullNameStr);
                        }

                        if (!recipient.JobTitle.IsEmpty() && jobTitleVisibility == JobTitleVisibility.NextRow)
                        {
                            grouppedLine.Add(recipient.JobTitle);
                        }

                        if (!recipient.Department.IsEmpty() && departmentVisibility)
                        {
                            grouppedLine.Add(recipient.Department);
                        }
                    }
                }
                if (grouppedLine.Count > 0)
                {
                    recipients.Add(string.Join("\r\n", grouppedLine));
                    AddedRecipients.Add(recipient);
                }
            }
        }

        private void TextBoxRecipients_TextChanged(object sender, TextChangedEventArgs e)
        {
            BaseUtils.RemoveRecipientsNotInTextBox(AddedRecipients, textBoxRecipients.Text, textBoxAttention.Text);

            var firstRecipientsRow = BaseUtils.GetRecipientTextFirstRow(textBoxAttention.Text.Trim().Length > 0 ? textBoxAttention.Text.Replace("\r\n", "\v") : textBoxRecipients.Text.Replace("\r\n", "\v"));
            if (!string.IsNullOrEmpty(firstRecipientsRow))
            {
                if (Salutations != null && Salutations.Count > 0)
                {
                    var firstRecipientFirstAndLastName = BaseUtils.GetFirstRecipientFirstAndLastName(firstRecipientsRow);
                    if (firstRecipientFirstAndLastName != null)
                    {
                        var salutationsList = new List<string>();
                        if (!string.IsNullOrEmpty(firstRecipientFirstAndLastName.FirstName))
                        {
                            salutationsList.Add(firstRecipientFirstAndLastName.FirstName);
                        }

                        var salutationsToExclude = GetSalutationsToExclude();
                        List<string> salutations;
                        if (!firstRecipientFirstAndLastName.Title.IsEmpty())
                        {
                            salutations = new List<string>() { firstRecipientFirstAndLastName.Title };
                        }
                        else
                        {
                            salutations = Salutations;
                        }

                        foreach (var defaultSalutation in salutations)
                        {
                            if (salutationsToExclude.Count > 0 && salutationsToExclude.Contains(defaultSalutation))
                            {
                                salutationsList.Add(defaultSalutation);
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(firstRecipientFirstAndLastName.LastName))
                                {
                                    var lastNameParts = firstRecipientFirstAndLastName.LastName.Split(new char[] { ' ', '\t' }, StringSplitOptions.RemoveEmptyEntries);

                                    if (lastNameParts != null && lastNameParts.Length > 0)
                                    {
                                        salutationsList.Add(defaultSalutation + " " + lastNameParts.LastOrDefault());

                                        if (lastNameParts.Length > 1)
                                        {
                                            salutationsList.Add(defaultSalutation + " " + firstRecipientFirstAndLastName.LastName);
                                        }
                                    }
                                }

                                var fullName = (firstRecipientFirstAndLastName.FirstName + " " + firstRecipientFirstAndLastName.LastName).Trim();
                                if (!string.IsNullOrEmpty(fullName))
                                {
                                    salutationsList.Add(defaultSalutation + " " + fullName);
                                }
                            }
                        }
                        comboBoxSalutation.ItemsSource = salutationsList;
                    }
                }
            }
            else
            {
                comboBoxSalutation.ItemsSource = Salutations;
            }
        }

        private List<string> GetSalutations()
        {
            var salutations = new List<string>();
            var salutationSetting = SettingsManager.GetSetting("Salutations", "Letters", null, MLanguageUtil.ActiveDocumentLanguage);
            if (!string.IsNullOrEmpty(salutationSetting))
            {
                salutations.AddRange(SettingsManager.UnescapeCommaCharacter(salutationSetting.Split(',')).Select(x => x.Trim()).ToList());
            }
            else
            {
                salutations.AddRange(DefaultSalutations);
            }
            return salutations;
        }

        private List<string> GetSalutationsToExclude()
        {
            var excludedSalutations = new List<string>();
            var salutationToExcludeSetting = SettingsManager.GetSetting("SalutationsToExclude", "Letters", null, MLanguageUtil.ActiveDocumentLanguage);
            if (!string.IsNullOrEmpty(salutationToExcludeSetting))
            {
                excludedSalutations.AddRange(SettingsManager.UnescapeCommaCharacter(salutationToExcludeSetting.Split(',')).Select(x => x.Trim()).ToList());
            }
            else
            {
                excludedSalutations.AddRange(DefaultExcludedSalutations);
            }
            return excludedSalutations;
        }


        private void LoadDS_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            LoadAnswers(fromDS: true);
        }

        public void OKHandler()
        {
            buttonOK_Click(null, null);
        }
    }
}
