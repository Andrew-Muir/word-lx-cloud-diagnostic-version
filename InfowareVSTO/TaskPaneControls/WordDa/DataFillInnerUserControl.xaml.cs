﻿using DocumentFormat.OpenXml.Drawing;
using InfowareVSTO.Common;
using InfowareVSTO.Common.Models;
using InfowareVSTO.Common.Models.ViewModels;
using InfowareVSTO.Util;
using InfowareVSTO.Windows.WordDa.Library;
using Microsoft.Office.Interop.Word;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using InfowareVSTO.Common.General;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml.Linq;
using ContentControl = Microsoft.Office.Interop.Word.ContentControl;
using DocumentFormat.OpenXml.Vml.Spreadsheet;
using DocumentFormat.OpenXml.Bibliography;
using Microsoft.Office.Interop.Outlook;
using MySqlX.XDevAPI.Common;
using InfowareVSTO.Windows;
using InfowareVSTO.DataSource;
using InfowareVSTO.Common.DTOs;
using InfowareVSTO.VSTOAuthor;

namespace InfowareVSTO.TaskPaneControls.WordDa
{
    /// <summary>
    /// Interaction logic for DataFillInnerUserControl.xaml
    /// </summary>
    public partial class DataFillInnerUserControl : UserControl
    {
        public static readonly XNamespace wordNamespace = "http://schemas.microsoft.com/office/word/2003/wordml";
        private const string DATA_FIELD_CONTROL_TAG = "WordDA";
        private const string DATA_FIELD_CODE_CONTROL_TAG = "WordDAQuestion";
        private const string QUESTION_FILE_PROPERTY_NAME = "LX-WORDDA-QUESTIONFILE";
        private const string DATA_FILL_ANSWERS_PROPERTY_NAME = "LX-DATAFILL-ANSWERS";
        private const string DATA_FIELD_CODE_ANSWER_PREFIX = "~AskRef;";
        private const string DATA_FIELD_CODE_ANSWER_SEPARATOR = "|";
        private const string SIMPLE_DATA_FIELD_KEY = "Simple";
        private const string CONDITIONAL_DATA_FIELD_KEY = "Conditional";
        private const string BODY_TAG = "body";
        private const string TEXT_TAG = "t";
        private bool UsesConditionalFields { get; set; }
        private bool UsesPromptFields { get; set; }
        public bool UsesChevronFields { get; private set; }
        public bool UsesSquareBracketsFields { get; private set; }
        public int ControlsFound { get; set; }
        private Dictionary<string, List<Range>> promptRanges;
        private Dictionary<string, List<Range>> specialCharacterRanges;
        private bool includeInformLawyerTags;
        private bool hasMatterMasterLoaded;
        private List<string> MatterLawyerFields = new List<string>()
        {
            "matterlawyername",
            "matterlawyerphone",
            "matterlawyerfax",
            "matterlawyeremail",
            "matterlawyerlsn"
        };

        public Dictionary<string, string> FieldValues
        {
            get
            {
                Dictionary<string, string> result = new Dictionary<string, string>();

                foreach (var field in DataFillViewModel.DataFields)
                {
                    if (!string.IsNullOrWhiteSpace(field.Value))
                    {
                        result[field.Name] = field.Value;
                    }
                }

                foreach (var field in DataFillViewModel.PromptAndSpecialCharacterFields)
                {
                    if (!string.IsNullOrWhiteSpace(field.Value))
                    {
                        result[field.Name] = field.Value;
                    }
                }

                return result;
            }
        }


        private DataFillViewModel DataFillViewModel { get; set; }

        public DataFillInnerUserControl(bool usesConditionalFileds = true, bool usePromptFields = true, bool useChevronFields = false, bool useSquareBracketsFields = false, bool includeInformLawyerTags = false)
        {
            InitializeComponent();
            UsesConditionalFields = usesConditionalFileds;
            UsesPromptFields = usePromptFields;
            UsesChevronFields = useChevronFields;
            UsesSquareBracketsFields = useSquareBracketsFields;
            this.includeInformLawyerTags = includeInformLawyerTags;
            if (!usesConditionalFileds)
            {
                ListQuestions.Visibility = Visibility.Collapsed;
            }
            if (!usePromptFields && !useChevronFields && !useSquareBracketsFields)
            {
                ListPromptAndSpecialCharacterFields.Visibility = Visibility.Collapsed;
            }

            ProtectionManager pm = new ProtectionManager(ThisAddIn.Instance.Application.ActiveDocument);

            pm.UnprotectIfNeeded();

            var promptFields = GetPromptFields();
            ObservableCollection<PromptOrSpecialCharacterDataField> specialCharacterFields = GetSpecialCharacterFields();

            var promptAndSpecialCharacterFields = RemoveDuplicateFields(specialCharacterFields, promptFields);

            DataFillViewModel = new DataFillViewModel
            {
                DataFields = GetDataFieldTitles(),
                Questions = GetQuestions(),
                PromptAndSpecialCharacterFields = promptAndSpecialCharacterFields
            };
            pm.ReProtectIfNeeded();

            LoadConditionalDataFieldAnswers(DataFillViewModel.Questions);

            DataContext = DataFillViewModel;
        }

        private ObservableCollection<PromptOrSpecialCharacterDataField> RemoveDuplicateFields(ObservableCollection<PromptOrSpecialCharacterDataField> specialCharacterFields, ObservableCollection<PromptOrSpecialCharacterDataField> promptFields)
        {
            List<string> keysToDelete = new List<string>();
            foreach(var spKeyValue in promptRanges)
            {
                for (int i = spKeyValue.Value.Count -1;i>=0; i--)
                {
                    if (specialCharacterRanges.Where(x=>x.Value.Where(y=>y.Start == spKeyValue.Value[i].Start && y.End == spKeyValue.Value[i].End).Count() > 0).Count() > 0)
                    {
                        spKeyValue.Value.Remove(spKeyValue.Value[i]);
                    }
                }
                
                if(spKeyValue.Value.Count == 0)
                {
                    keysToDelete.Add(spKeyValue.Key);
                }
            }

            foreach(var key in keysToDelete)
            {
                promptRanges.Remove(key);
            }

            for (int i = promptFields.Count - 1; i >= 0; i--)
            {
                var promptField = promptFields[i];

                if (!promptRanges.TryGetValue(promptField.Name, out var list))
                {
                    promptFields.Remove(promptField);
                }
            }
            var joined = specialCharacterFields.ToList();
            joined.AddRange(promptFields);
            return new ObservableCollection<PromptOrSpecialCharacterDataField>(joined.OrderBy(x => GetFirstRangeStart(x.Name)));
        }

        private int GetFirstRangeStart(string name)
        {
            int minPrompt = int.MaxValue, minSpecial = int.MaxValue;

            if (promptRanges.TryGetValue(name, out var listPrompt)) {
                minPrompt = listPrompt.Min(y => y.Start);
            }

            if (specialCharacterRanges.TryGetValue(name, out var listSpecial))
            {
                minSpecial = listSpecial.Min(y => y.Start);
            }

            return Math.Min(minPrompt, minSpecial);
        }

        private ObservableCollection<PromptOrSpecialCharacterDataField> GetSpecialCharacterFields()
        {
            List<PromptOrSpecialCharacterDataField> result = new List<PromptOrSpecialCharacterDataField>();
            this.specialCharacterRanges = new Dictionary<string, List<Range>>();

            if (UsesChevronFields)
            {
                GetSpecialCharacterFields("«", "»");
            }

            if (UsesSquareBracketsFields)
            {
                GetSpecialCharacterFields("[", "]");
            }

            foreach (string text in specialCharacterRanges.Keys)
            {
                result.Add(new PromptOrSpecialCharacterDataField(text));
            }

            return new ObservableCollection<PromptOrSpecialCharacterDataField>(result);
        }

        private void GetSpecialCharacterFields(string startChar, string endChar)
        {
            Range range2 = ThisAddIn.Instance.Application.ActiveDocument.Content;
            Find find = range2.Find;
            find.Text = startChar;
            find.Forward = true;

            find.Execute();
            int previousStart = -1;
            while (find.Found && range2.Start > previousStart)
            {
                previousStart = range2.Start;

                Range paragraphRange = range2.Paragraphs.First.Range;
                paragraphRange.Start = range2.End;
                Find find2 = paragraphRange.Find;
                find2.Text = endChar;
                find2.Forward = true;

                find2.Execute();

                if (find2.Found && range2.Start < paragraphRange.End)
                {
                    Range range3 = Utils.TrimRange(ThisAddIn.Instance.Application.ActiveDocument.Range(range2.End, paragraphRange.Start));
                    Range range4 = Utils.TrimRange(ThisAddIn.Instance.Application.ActiveDocument.Range(range2.Start, paragraphRange.End));
                    string text = GetTextText(range3.XML)?.Trim();

                    if (!string.IsNullOrWhiteSpace(text))
                    {
                        text = GetExistingOrThis(specialCharacterRanges, text);
                        if (!specialCharacterRanges.ContainsKey(text))
                        {
                            specialCharacterRanges[text] = new List<Range>();
                        }

                        var paragraph = range3.Paragraphs.First;

                        dynamic style = paragraph.get_Style();

                        specialCharacterRanges[text].Add(range4);
                    }
                }
                find.Execute();
            }
        }

        internal void FillTitleOfDocument(string formName)
        {
            ObservableCollection<PromptOrSpecialCharacterDataField> promptFields = null;
            if (!UsesPromptFields)
            {
                promptFields = GetPromptFields(true);
            }

            if (promptRanges != null && promptRanges != null && promptRanges.TryGetValue("TITLE OF DOCUMENT", out List<Range> ranges) == true)
            {
                foreach (Range range in ranges)
                {
                    range.Text = formName;
                }
            }
        }

        public void ApplyDataSourceValues(Dictionary<string, string> values)
        {
            if (values != null && DataFillViewModel?.DataFields != null)
            {
                foreach (SimpleDataField simpleDataField in DataFillViewModel.DataFields)
                {
                    if (values.TryGetValue(simpleDataField.Name, out string value))
                    {
                        simpleDataField.Value = value;
                    }
                }
            }
        }

        private ObservableCollection<SimpleDataField> GetDataFieldTitles()
        {
            var dataFields = new ObservableCollection<SimpleDataField>();
            var controlsWithTag = GetControlsWithTag(DATA_FIELD_CONTROL_TAG);
            if (includeInformLawyerTags)
            {
                controlsWithTag.AddRange(GetControlsWithTag(InformDialog.INFORM_LAWYER_TAG));
            }

            if (!MatterMasterPaneUtils.HasMatterMasterMatterLoaded)
            {
                controlsWithTag.AddRange(GetControlsWithTag(MatterMasterPaneUtils.MATTER_MASTER_TAG));
                hasMatterMasterLoaded = true;
            }

            this.ControlsFound = controlsWithTag.Count + GetControlsWithTag(DATA_FIELD_CODE_CONTROL_TAG).Count;
            if (controlsWithTag.Count() > 0)
            {
                bool currentAuthorGot = false;
                AuthorWithPathsDTO currentAuthor = null;
                bool add = true;
                foreach (ContentControl control in controlsWithTag)
                {
                    try
                    {
                        if (dataFields.Any(dataField => dataField.Name == control.Title))
                        {
                            continue;
                        }

                        var dataFieldType = DataFieldType.Text;
                        if (control.Title.IndexOf("Date", StringComparison.OrdinalIgnoreCase) >= 0)
                        {
                            dataFieldType = DataFieldType.Date;
                        }
                        else if (control.Title.Equals("Company", StringComparison.OrdinalIgnoreCase))
                        {
                            dataFieldType = DataFieldType.Company;
                        }
                        else if (control.Title.Equals("Firm", StringComparison.OrdinalIgnoreCase))
                        {
                            dataFieldType = DataFieldType.Firm;
                        }
                        else if (control.Title.Equals("Client", StringComparison.OrdinalIgnoreCase))
                        {
                            dataFieldType = DataFieldType.Client;
                        }
                        else if (control.Title.Equals("Contact", StringComparison.OrdinalIgnoreCase))
                        {
                            dataFieldType = DataFieldType.Contact;
                        }
                        else if (control.Title.Equals("CourtName", StringComparison.OrdinalIgnoreCase))
                        {
                            dataFieldType = DataFieldType.CourtName;
                        }

                        var controlText = control.Range.Text;
                        if (control.Title == control.Range.Text)
                        {
                            controlText = string.Empty;
                        }
                        if (control.Tag == CustomConstants.MATTER_MASTER_TAG)
                        {
                            if (string.IsNullOrWhiteSpace(controlText))
                            {
                                var matterLawyerfield = MatterLawyerFields.Where(x => control.Title?.ToLower()?.StartsWith(x) == true).FirstOrDefault();
                                if (matterLawyerfield != null)
                                {
                                    if (!currentAuthorGot)
                                    {
                                        currentAuthor = AdminPanelWebApi.GetCurrentAuthor();
                                    }
                                    if (currentAuthor != null)
                                    {
                                        switch (matterLawyerfield)
                                        {

                                            case "matterlawyername":
                                                controlText = string.Join(" ", new List<string>() { currentAuthor.Prefix, currentAuthor.FirstName, currentAuthor.MiddleName, currentAuthor.LastName, currentAuthor.Suffix }.Where(x => !string.IsNullOrWhiteSpace(x)));
                                                break;
                                            case "matterlawyerphone":
                                                controlText = BaseUtils.GetContactFirstInformation(currentAuthor.ContactInformations, "Phone");
                                                break;
                                            case "matterlawyerfax":
                                                controlText = BaseUtils.GetContactFirstInformation(currentAuthor.ContactInformations, "Fax");
                                                break;
                                            case "matterlawyeremail":
                                                controlText = BaseUtils.GetContactFirstInformation(currentAuthor.ContactInformations, "Email");
                                                break;
                                            case "matterlawyerlsn":
                                                controlText = currentAuthor.LawyerNumber;
                                                break;
                                        }
                                    }
                                }

                                if (control.Title?.StartsWith("@") == true && control.Title?.ToLower()?.Contains("_company") == true)
                                {
                                    var currentAuthorSettings = ManageUsers.CurrentAuthorSettings;

                                    if (currentAuthorSettings?.CompanyId != null)
                                    {
                                        var company = AdminPanelWebApi.GetCompanyDetails(currentAuthorSettings.CompanyId.Value);

                                        if (company != null)
                                        {
                                            controlText = company.Name;
                                        }
                                    }
                                }
                            }

                            if (control.Title?.ToLower()?.StartsWith("longstyleofcause") == true)
                            {
                                add = false;
                            }
                        }

                        if (add)
                        {
                            dataFields.Add(new SimpleDataField(control.Title, controlText, dataFieldType));
                        }

                        add = true;

                    }
                    catch (System.Exception ex)
                    {
                        Debug.WriteLine(ex);
                    }
                }
            }
            return dataFields;
        }

        private ObservableCollection<PromptOrSpecialCharacterDataField> GetPromptFields(bool alwaysSearch = false)
        {
            var promptStyle = Utils.GetStyleByName("Prompt");
            this.promptRanges = new Dictionary<string, List<Range>>();
            ObservableCollection<PromptOrSpecialCharacterDataField> result = new ObservableCollection<PromptOrSpecialCharacterDataField>();

            List<string> multiLineFields = new List<string>();
            if ((alwaysSearch || UsesPromptFields) && promptStyle != null)
            {
                Range range2 = ThisAddIn.Instance.Application.ActiveDocument.Content;
                range2.Collapse(WdCollapseDirection.wdCollapseStart);
                Find find = range2.Find;
                find.ClearFormatting();
                find.set_Style(promptStyle);
                find.Format = true;
                find.Forward = true;

                find.Execute();
                int previousStart = -1;
                while (find.Found && range2.Start > previousStart)
                {
                    previousStart = range2.Start;
                    Range range3 = Utils.TrimRange(range2);
                    string text = BaseUtils.TrimNonAlphaNumeric(GetTextText(range3.XML))?.Trim().TrimEnd(new char[] { ']', '»' });

                    if (!string.IsNullOrWhiteSpace(text))
                    {
                        text = GetExistingOrThis(promptRanges, text);
                        if (!promptRanges.ContainsKey(text))
                        {
                            promptRanges[text] = new List<Range>();
                        }

                        var paragraph = range3.Paragraphs.First;

                        dynamic style = paragraph.get_Style();

                        if (style != null && style.NameLocal == "ToAndTo")
                        {
                            multiLineFields.Add(text);
                        }

                        promptRanges[text].Add(range3);

                    }
                    find.Execute();
                }

                foreach (string text in promptRanges.Keys)
                {
                    result.Add(new PromptOrSpecialCharacterDataField(text, multiLineFields.Contains(text)));
                }
            }

            return result;
        }

        private string GetExistingOrThis(Dictionary<string, List<Range>> promptRanges, string text)
        {
            if (promptRanges == null)
            {
                return text;
            }

            foreach (var keyValue in promptRanges)
            {
                if (keyValue.Key.ToLower() == text.ToLower())
                {
                    return keyValue.Key;
                }
            }

            return text;
        }

        private string GetTextText(string xml)
        {
            string result = string.Empty;
            if (!string.IsNullOrWhiteSpace(xml))
            {
                var root = XDocument.Parse(xml);

                if (root != null)
                {
                    foreach (XElement bodyElement in root.Descendants(wordNamespace + BODY_TAG))
                    {
                        foreach (XElement textElement in bodyElement.Descendants(wordNamespace + TEXT_TAG))
                        {
                            if ((textElement.PreviousNode as XElement)?.Name == wordNamespace + "br")
                            {
                                result += "\v";
                            }

                            result += textElement.Value;
                        }
                    }
                }
            }

            return result;
        }

        private ObservableCollection<ConditionalDataField> GetQuestions()
        {
            var questions = new ObservableCollection<ConditionalDataField>();
            var serializedQuestions = DocumentPropertyUtil.ReadLongProperty(QUESTION_FILE_PROPERTY_NAME);
            if (serializedQuestions != null)
            {
                var usedQuestionIds = GetUsedQuestionIds();
                var rawQuestions = JObject.Parse(serializedQuestions);
                foreach (KeyValuePair<string, JToken> rawQuestion in rawQuestions)
                {
                    if (usedQuestionIds.Contains(rawQuestion.Key))
                    {
                        ClauseBuilderQuestion question = rawQuestion.Value.ToObject<ClauseBuilderQuestion>();
                        questions.Add(new ConditionalDataField(question.Variable, question.Question, question.Answer1, question.Answer2, question.AdditionalAnswers));
                    }
                }
            }
            return questions;
        }

        private void LoadConditionalDataFieldAnswers(ObservableCollection<ConditionalDataField> questions)
        {
            var serializedDataFillAnswers = DocumentPropertyUtil.ReadLongProperty(DATA_FILL_ANSWERS_PROPERTY_NAME);
            if (serializedDataFillAnswers != null)
            {
                var rawDataFillAnswers = JObject.Parse(serializedDataFillAnswers);
                JToken conditionalDataFieldAnswers = rawDataFillAnswers[CONDITIONAL_DATA_FIELD_KEY];

                foreach (JToken answer in conditionalDataFieldAnswers)
                {
                    var conditionalDataField = answer.ToObject<ConditionalDataField>();
                    var unconfiguredConditionalDataField = questions?.FirstOrDefault(question => question.Name == conditionalDataField.Name);
                    if (unconfiguredConditionalDataField == null)
                    {
                        continue;
                    }
                    unconfiguredConditionalDataField.Value = conditionalDataField.Value;
                }
            }
        }

        private List<string> GetUsedQuestionIds()
        {
            var dataFieldCodes = new List<string>();
            foreach (ContentControl control in GetControlsWithTag(DATA_FIELD_CODE_CONTROL_TAG))
            {
                dataFieldCodes.Add(control.Title);
            }

            var questionIds = new List<string>();
            foreach (string dataFieldCode in dataFieldCodes)
            {
                int startIndex = 1;
                int prefixIndex = dataFieldCode.IndexOf(DATA_FIELD_CODE_ANSWER_PREFIX);

                while (prefixIndex >= 0)
                {
                    string questionId = dataFieldCode.Substring(startIndex, prefixIndex - startIndex);

                    startIndex = dataFieldCode.IndexOf("[", prefixIndex) + 1;
                    prefixIndex = dataFieldCode.IndexOf(DATA_FIELD_CODE_ANSWER_PREFIX, prefixIndex + 1);

                    if (questionIds.Contains(questionId))
                    {
                        continue;
                    }

                    questionIds.Add(questionId);
                }
            }

            //RTF Ask Fields
            var rtfFieldCodes = new List<string>();
            foreach (ContentControl control in GetControlsWithTag(ClauseBuilderUserControl.ASK_RTF_FIELD_TAG))
            {
                rtfFieldCodes.Add(control.Title);
            }

            foreach (string dataFieldCode in rtfFieldCodes)
            {
                int index = dataFieldCode.LastIndexOf("#");
                if (index > 0)
                {
                    string questionId = dataFieldCode.Substring(0, index);

                    if (questionIds.Contains(questionId))
                    {
                        continue;
                    }

                    questionIds.Add(questionId);
                }

            }

            return questionIds;
        }

        private List<ContentControl> GetControlsWithTag(string tag)
        {
            return DocumentUtils.GetAllContentControls(Globals.ThisAddIn.Application.ActiveDocument, tag).ToList();
        }

        public void OnUpdateButtonClick()
        {
            SaveDataFillStateAsDocumentProperty();
            InsertSimpleDataFields();
            if (UsesConditionalFields)
            {
                InsertConditionalDataFields();
            }
            if (UsesPromptFields)
            {
                InsertPromptFields();
            }
            if (UsesChevronFields || UsesSquareBracketsFields)
            {
                InsertSpecialCharacterFields();
            }
        }

        private void InsertPromptFields()
        {
            if (promptRanges != null)
            {
                foreach (PromptOrSpecialCharacterDataField promptField in DataFillViewModel.PromptAndSpecialCharacterFields)
                {
                    var dataValue = promptField.Value;
                    if (!string.IsNullOrWhiteSpace(dataValue) && promptRanges.TryGetValue(promptField.Name, out List<Range> ranges))
                    {
                        foreach (var range in ranges)
                        {
                            range.Text = ConvertNewLinesToManualReturn(dataValue);
                        }
                    }
                }
            }
        }

        private void InsertSpecialCharacterFields()
        {
            if (specialCharacterRanges != null)
            {
                foreach (PromptOrSpecialCharacterDataField specialCharachterField in DataFillViewModel.PromptAndSpecialCharacterFields)
                {
                    var dataValue = specialCharachterField.Value;
                    if (!string.IsNullOrWhiteSpace(dataValue) && specialCharacterRanges.TryGetValue(specialCharachterField.Name, out List<Range> ranges))
                    {
                        foreach (var range in ranges)
                        {
                            range.Text = dataValue;
                        }
                    }
                }
            }
        }

        private string ConvertNewLinesToManualReturn(string dataValue)
        {
            if (dataValue == null)
            {
                return null;
            }

            return dataValue.Replace("\r\n", "\v").Replace("\r", "\v").Replace("\n", "\v");
        }

        private void InsertSimpleDataFields()
        {
            var dataFieldContentDictionary = new Dictionary<string, string>();
            foreach (SimpleDataField dataField in DataFillViewModel.DataFields)
            {
                var dataValue = dataField.Value;
                if (dataField.Type == DataFieldType.Date)
                {
                    DateTime.TryParse(dataField.Value, out var dateField);
                    if (dateField != null && dateField != DateTime.MinValue)
                    {
                        dataValue = dateField.ToString(Utils.GetDefaultDateFormat(), CultureInfo.CreateSpecificCulture(MultiLanguage.MLanguageUtil.GetResource(MultiLanguage.MLanguageResourceEnum.DateCultureID)));
                    }
                }
                dataFieldContentDictionary.Add(dataField.Name, dataValue);
            }

            var controlsWithTag = GetControlsWithTag(DATA_FIELD_CONTROL_TAG);

            if (includeInformLawyerTags)
            {
                controlsWithTag.AddRange(GetControlsWithTag(InformDialog.INFORM_LAWYER_TAG));
            }

            if (!MatterMasterPaneUtils.HasMatterMasterMatterLoaded)
            {
                controlsWithTag.AddRange(GetControlsWithTag(MatterMasterPaneUtils.MATTER_MASTER_TAG));
                hasMatterMasterLoaded = true;
            }

            foreach (ContentControl control in controlsWithTag)
            {
                if (control.Title != null && dataFieldContentDictionary.ContainsKey(control.Title))
                {
                    if (control.Title.Equals("CourtName", StringComparison.OrdinalIgnoreCase))
                    {
                        MatterMasterPaneUtils.InsertCourtName(control, dataFieldContentDictionary[control.Title]);
                    }
                    else
                    {
                        control.Range.Text = dataFieldContentDictionary[control.Title];
                    }
                }
            }
        }

        private void InsertConditionalDataFields()
        {
            var conditionalDataFieldContentDictionary = new Dictionary<string, int>();

            foreach (ConditionalDataField conditionalDataField in DataFillViewModel.Questions)
            {
                int? tag = GetConditionalFieldResultPosition(conditionalDataField.Name);
                if (tag != null)
                {
                    conditionalDataFieldContentDictionary.Add(conditionalDataField.Name, tag.Value);
                }
            }

            foreach (ContentControl control in GetControlsWithTag(DATA_FIELD_CODE_CONTROL_TAG))
            {
                string conditionalDataField = control.Title;

                if (!conditionalDataFieldContentDictionary.ContainsKey(GetConditionalDataFieldQuestionId(conditionalDataField)))
                {
                    /*
                     *This is a new "simple" conditional field (no inner condition). We cannot handle it because
                     * the UserControl contains no radiobuttons for it
                     */
                    continue;
                }

                List<int> askRefIndices = GetConditionalDataFieldAskRefIndices(control.Title);

                while (askRefIndices.Count > 0)
                {
                    foreach (int askRefIndex in askRefIndices)
                    {
                        if (IsConditionalDataFieldAskRefALeaf(askRefIndex, conditionalDataField))
                        {
                            conditionalDataField = ReplaceConditionalDataField(askRefIndex, conditionalDataField,
                                conditionalDataFieldContentDictionary);
                            break;
                        }
                    }

                    askRefIndices = GetConditionalDataFieldAskRefIndices(conditionalDataField);
                }

                control.Range.Text = conditionalDataField;
            }

            InsertRTFAskQuestions(conditionalDataFieldContentDictionary);
        }

        private int? GetConditionalFieldResultPosition(string name)
        {
            var radioButton = DocumentUtils.Descendants<RadioButton>(this).FirstOrDefault(r => r.IsChecked == true && r.GroupName == name);

            if (radioButton.Tag is string && int.TryParse(radioButton.Tag as string, out int intValue))
            {
                return intValue;
            }

            if (radioButton.Tag is int)
            {
                return (int)radioButton.Tag;
            }

            return null;
        }

        private void InsertRTFAskQuestions(Dictionary<string, int> conditionalDataFieldContentDictionary)
        {
            List<ContentControl> contentcontrols = GetControlsWithTag(ClauseBuilderUserControl.ASK_RTF_FIELD_TAG);
            List<ContentControl> toDelete = new List<ContentControl>();

            foreach (var cc in contentcontrols)
            {
                string title = cc.Title;
                if (title != null)
                {
                    int index = title.LastIndexOf("#");
                    if (index > 0)
                    {
                        string questionId = title.Substring(0, index);
                        string id = title.Substring(index + 1);

                        if (conditionalDataFieldContentDictionary.TryGetValue(questionId, out int value))
                        {
                            List<ContentControl> innerContentControls = cc.Range.ContentControls.Cast<ContentControl>().Where(x => (x.Tag == ClauseBuilderUserControl.ASK_BOOLEAN_TAG || x.Tag == ClauseBuilderUserControl.ASK_RTF_RESULT_TAG) && x.Title?.EndsWith(id) == true).ToList();
                            var falseCC = innerContentControls.Where(x => x.Title?.StartsWith(ClauseBuilderUserControl.ASK_FALSE_TITLE) == true).FirstOrDefault();
                            var trueCC = innerContentControls.Where(x => x.Title?.StartsWith(ClauseBuilderUserControl.ASK_TRUE_TITLE) == true).FirstOrDefault();
                            List<ContentControl> resultCCs = innerContentControls.Where(x => x.Title?.StartsWith("Result") == true).ToList();

                            List<ContentControl> ccToDelete = new List<ContentControl>();
                            ContentControl goodCC = null;

                            if (resultCCs.Count == 0)
                            {
                                goodCC = value == 2 ? trueCC : falseCC;
                                ccToDelete.Add(value == 2 ? falseCC : trueCC);
                            }
                            else
                            {
                                goodCC = resultCCs.Where(x => x.Title.StartsWith("Result" + value + "#")).FirstOrDefault();
                                ccToDelete.AddRange(resultCCs.Where(x => !x.Title.StartsWith("Result" + value + "#")));
                            }

                            try
                            {
                                foreach (var tempCC in ccToDelete)
                                {
                                    tempCC?.Delete(true);
                                }

                                goodCC?.Delete(false);
                                toDelete.Add(cc);
                            }
                            catch (System.Exception ex)
                            {
                            }
                        }
                    }
                }
            }

            for (int i = toDelete.Count - 1; i >= 0; i--)
            {
                try
                {
                    toDelete[i].Delete(false);
                }
                catch (System.Exception ex)
                {
                }
            }
        }

        private List<int> GetConditionalDataFieldAskRefIndices(string conditionalDataField)
        {
            var indices = new List<int>();

            int separatorIndex = conditionalDataField.IndexOf(DATA_FIELD_CODE_ANSWER_PREFIX);

            while (separatorIndex >= 0)
            {
                indices.Add(separatorIndex);

                separatorIndex = conditionalDataField.IndexOf(DATA_FIELD_CODE_ANSWER_PREFIX, separatorIndex + 1);
            }

            return indices;
        }

        private string ReplaceConditionalDataField(int askRefIndex, string conditionalDataField,
            Dictionary<string, int> conditionalDataFieldContentDictionary)
        {
            string beforeString = conditionalDataField.Substring(0, askRefIndex);
            int endIndex = conditionalDataField.IndexOf("]", askRefIndex);
            string afterString = conditionalDataField.Substring(endIndex + 1);
            string innerString = conditionalDataField.Substring(askRefIndex + DATA_FIELD_CODE_ANSWER_PREFIX.Length, endIndex - (askRefIndex + DATA_FIELD_CODE_ANSWER_PREFIX.Length));

            string[] results = innerString.Split(new string[] { "|" }, StringSplitOptions.None);

            int startBracketIndex = beforeString.LastIndexOf("[");

            string questionId = conditionalDataField.Substring(startBracketIndex + 1, askRefIndex - startBracketIndex - 1);

            if (conditionalDataFieldContentDictionary.TryGetValue(questionId, out int resultNr) && resultNr <= results.Length)
            {
                string result = results[resultNr - 1];

                return conditionalDataField.Substring(0, startBracketIndex) + result + afterString;
            }

            return conditionalDataField;
        }

        private string GetConditionalDataFieldQuestionId(int separatorIndex, string conditionalDataField)
        {
            string beforeSeparator = conditionalDataField.Substring(0, separatorIndex);

            int answerPrefixIndex = beforeSeparator.LastIndexOf(DATA_FIELD_CODE_ANSWER_PREFIX);
            int startBracketIndex = beforeSeparator.LastIndexOf("[");

            return conditionalDataField.Substring(startBracketIndex + 1, answerPrefixIndex - startBracketIndex - 1);
        }

        private string GetConditionalDataFieldQuestionId(string conditionalDataFiled)
        {
            return GetConditionalDataFieldQuestionId(conditionalDataFiled.IndexOf(DATA_FIELD_CODE_ANSWER_SEPARATOR), conditionalDataFiled);
        }

        private bool IsConditionalDataFieldAskRefALeaf(int askRefIndex, string conditionalDataField)
        {
            string afteraskRef = conditionalDataField.Substring(askRefIndex + DATA_FIELD_CODE_ANSWER_PREFIX.Length);

            int nextAskRefIndex = afteraskRef.IndexOf(DATA_FIELD_CODE_ANSWER_PREFIX);

            return nextAskRefIndex == -1 || nextAskRefIndex > afteraskRef.IndexOf("]");
        }

        //private bool IsSimpleConditionalDataField(string conditionalDataField)
        //{
        //    return Regex.Matches(conditionalDataField, DATA_FIELD_CODE_ANSWER_PREFIX).Count == 1;
        //}

        private void SaveDataFillStateAsDocumentProperty()
        {
            var root = new JObject
            {
                { SIMPLE_DATA_FIELD_KEY, JToken.FromObject(DataFillViewModel.DataFields) },
                { CONDITIONAL_DATA_FIELD_KEY, JToken.FromObject(DataFillViewModel.Questions) }
            };
            DocumentPropertyUtil.SaveLongProperty(DATA_FILL_ANSWERS_PROPERTY_NAME, JsonConvert.SerializeObject(root));
        }

        private string GetNewControlName()
        {
            return Guid.NewGuid().ToString();
        }

        private void ButtonFindCompanyRecipients_Click(object sender, RoutedEventArgs e)
        {
            ImportContacts_WPF importContacts_WPF = new ImportContacts_WPF(RecipientType.NoJobTitle, false, DataGridSelectionMode.Single);
            importContacts_WPF.ShowDialog();

            if (importContacts_WPF.ResultOk)
            {
                var companyTextField = BaseUtils.FindChild<TextBox>(this, "CompanyTextField");
                var selectedContacts = importContacts_WPF.SelectedContacts.FirstOrDefault();
                if (companyTextField != null && selectedContacts != null)
                {
                    var selectedContactsCompany = selectedContacts.Companies.OrderBy(x => x.IsDefault).FirstOrDefault();
                    if (selectedContactsCompany != null)
                    {
                        companyTextField.Text = selectedContactsCompany.Name;
                    }
                }
            }
        }

        private void ButtonFindFirmRecipients_Click(object sender, RoutedEventArgs e)
        {
            ImportContacts_WPF importContacts_WPF = new ImportContacts_WPF(RecipientType.NoJobTitle, false, DataGridSelectionMode.Single);
            importContacts_WPF.ShowDialog();

            if (importContacts_WPF.ResultOk)
            {
                var firmTextField = BaseUtils.FindChild<TextBox>(this, "FirmTextField");
                var selectedContacts = importContacts_WPF.SelectedContacts.FirstOrDefault();
                if (firmTextField != null && selectedContacts != null)
                {
                    var selectedContactsCompany = selectedContacts.Companies.OrderBy(x => x.IsDefault).FirstOrDefault();
                    if (selectedContactsCompany != null)
                    {
                        firmTextField.Text = selectedContactsCompany.Name;
                    }
                }
            }
        }

        public void ApplyValues(DataFillAnswers answers)
        {
            if (answers?.Answers != null)
            {
                foreach (var fieldValue in answers.Answers)
                {
                    foreach (var field in DataFillViewModel.DataFields)
                    {
                        if (field.Name == fieldValue.Key)
                        {
                            field.Value = fieldValue.Value;
                        }
                    }

                    foreach (var field in DataFillViewModel.PromptAndSpecialCharacterFields)
                    {
                        if (field.Name == fieldValue.Key)
                        {
                            field.Value = fieldValue.Value;
                        }
                    }
                }
            }
        }

        private void ButtonFindClientRecipients_Click(object sender, RoutedEventArgs e)
        {
            ImportContacts_WPF importContacts_WPF = new ImportContacts_WPF(RecipientType.SingleRow, false, DataGridSelectionMode.Single);
            importContacts_WPF.ShowDialog();

            if (importContacts_WPF.ResultOk)
            {
                var clientTextField = BaseUtils.FindChild<TextBox>(this, "ClientTextField");
                var selectedContacts = importContacts_WPF.SelectedContacts.FirstOrDefault();
                if (clientTextField != null && selectedContacts != null)
                {
                    clientTextField.Text = (selectedContacts.FirstName + " " + selectedContacts.LastName + (importContacts_WPF.JobTitleVisibility == JobTitleVisibility.SameRow && !string.IsNullOrEmpty(selectedContacts.JobTitle) ? ", " + selectedContacts.JobTitle : "")).Trim();
                }
            }
        }

        private void ButtonFindContactRecipients_Click(object sender, RoutedEventArgs e)
        {
            ImportContacts_WPF importContacts_WPF = new ImportContacts_WPF(RecipientType.SingleRow, false, DataGridSelectionMode.Single);
            importContacts_WPF.ShowDialog();

            if (importContacts_WPF.ResultOk)
            {
                var contactTextField = BaseUtils.FindChild<TextBox>(this, "ContactTextField");
                var selectedContacts = importContacts_WPF.SelectedContacts.FirstOrDefault();
                if (contactTextField != null && selectedContacts != null)
                {
                    contactTextField.Text = (selectedContacts.FirstName + " " + selectedContacts.LastName + (importContacts_WPF.JobTitleVisibility == JobTitleVisibility.SameRow && !string.IsNullOrEmpty(selectedContacts.JobTitle) ? ", " + selectedContacts.JobTitle : "")).Trim();
                }
            }
        }

        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {

        }
    }

}
