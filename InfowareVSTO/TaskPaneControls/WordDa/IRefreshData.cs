﻿using Microsoft.Office.Interop.Word;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfowareVSTO.TaskPaneControls.WordDa
{
    public interface IRefreshData
    {
        Range Selection { get; set; }
        void OKHandler();
    }
}
