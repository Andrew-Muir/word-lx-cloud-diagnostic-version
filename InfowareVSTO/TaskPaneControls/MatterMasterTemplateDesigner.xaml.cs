﻿using InfowareVSTO.Common.Language;
using InfowareVSTO.DataSource;
using Word = Microsoft.Office.Interop.Word;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using InfowareVSTO.Util;
using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml.Wordprocessing;
using System.Timers;
using System.Windows.Threading;
using InfowareVSTO.Common;

namespace InfowareVSTO.TaskPaneControls
{
    /// <summary>
    /// Interaction logic for MatterMasterTemplateDesigner.xaml
    /// </summary>
    public partial class MatterMasterTemplateDesigner : UserControl
    {
        private List<string> booleanFields = new List<string>()
        {
            "ClientIsCompany",
            "IsMinor",
            "IsCompany"
        };

        private Timer messageTimer;
        private Dispatcher dispatcher;

        public MatterMasterTemplateDesigner()
        {
            InitializeComponent();

            PopulateMMFields();
            PopulateRoles();
            PopulateEntityFields();
            PopulateGroups();
            PopulateSwitches();
            PopulateERB();
        }

        private void PopulateERB()
        {
            List<KeyValuePair<string, string>> itemSource = new List<KeyValuePair<string, string>>()
            {
                new KeyValuePair<string, string>("", LanguageManager.GetTranslation(LanguageConstants.Recipients, "Recipients")),
                new KeyValuePair<string, string>("_CC", LanguageManager.GetTranslation(LanguageConstants.CC, "CC")),
                new KeyValuePair<string, string>("_BCC", LanguageManager.GetTranslation(LanguageConstants.BCC, "BCC"))
            };

            this.ERBs.ItemsSource = itemSource;
            this.ERBs.DisplayMemberPath = "Value";
            this.ERBs.SelectedValuePath = "Key";
            this.ERBs.SelectedIndex = 0;
        }

        private void PopulateSwitches()
        {
            List<KeyValuePair<string, string>> switches = new List<KeyValuePair<string, string>>()
            {
                new KeyValuePair<string, string>("", LanguageManager.GetTranslation(LanguageConstants.None, "None")),
                new KeyValuePair<string, string>(MatterMasterPaneUtils.DELIFEMPTY_SWITCH,MatterMasterPaneUtils.DELIFEMPTY_SWITCH),
                new KeyValuePair<string, string>(MatterMasterPaneUtils.DELLINEIFNOVALUE_SWITCH, MatterMasterPaneUtils.DELLINEIFNOVALUE_SWITCH)
            };

            this.Swithces.ItemsSource = switches;
            this.Swithces.SelectedValuePath = "Key";
            this.Swithces.DisplayMemberPath = "Value";
            this.Swithces.SelectedIndex = 0;
        }

        private void PopulateGroups()
        {
            DataTable dataTable = DataSourceManager.GetData("GroupCodes", "1 = 1", "GC_GroupCode, GC_Description");

            List<KeyValuePair<string, string>> itemSource = new List<KeyValuePair<string, string>>();

            itemSource.Add(new KeyValuePair<string, string>("*", LanguageManager.GetTranslation(LanguageConstants.AllGroups, "All Groups")));

            foreach (DataRow row in dataTable.Rows)
            {
                itemSource.Add(new KeyValuePair<string, string>(row.ItemArray[0]?.ToString(), row.ItemArray[1]?.ToString()));
            }

            this.CustomFieldsGroups.ItemsSource = itemSource;
            this.CustomFieldsGroups.SelectedValuePath = "Key";
            this.CustomFieldsGroups.DisplayMemberPath = "Value";
            this.CustomFieldsGroups.SelectedIndex = 0;
        }

        private void PopulateEntityFields()
        {
            List<string> entityFields = new List<string>()
            {
                "Role",
                "FullName",
                "Title",
                "FirstName",
                "MiddleName",
                "LastName",
                "Suffix",
                "Alias",
                "Salutation",
                "JobTitle",
                "Company",
                "IsCompany",
                "Division",
                "Phone",
                "DirectPhone",
                "Fax",
                "DirectFax",
                "MobilePhone",
                "HomePhone",
                "Email",
                "StreetAddress",
                "City",
                "Prov",
                "Postal",
                "Country",
                "FullAddress",
                "LawSocietyNumber",
                "ReLine",
                "Notes",
                "User1",
                "User2",
                "User3",
                "User4",
                "User5",
                "Ref1",
                "Ref2",
                "PartyNumber",
                "IsMinor",
                "LawyerName",
                "LawyerLSN",
                "LawyerFirm",
                "LawyerPhone",
                "LawyerFax",
                "LawyerFullAddress",
                "LawyerClientsAll",
                "FileNo",
                "RepForNames"
            };

            this.EntityFields.ItemsSource = entityFields;
            this.EntityFields.SelectedIndex = 0;
        }

        private void PopulateRoles()
        {
            DataTable dataTable = DataSourceManager.GetData("EntityRoles", "1 = 1", "ER_Code, ER_Description");

            List<KeyValuePair<string, string>> itemSource = new List<KeyValuePair<string, string>>();

            itemSource.Add(new KeyValuePair<string, string>("*", LanguageManager.GetTranslation(LanguageConstants.AllRoles, "All Roles")));

            foreach (DataRow row in dataTable.Rows)
            {
                itemSource.Add(new KeyValuePair<string, string>(row.ItemArray[0]?.ToString(), row.ItemArray[1]?.ToString()));
            }

            this.Role.ItemsSource = itemSource;
            this.Role.SelectedValuePath = "Key";
            this.Role.DisplayMemberPath = "Value";
            this.Role.SelectedIndex = 0;
        }

        private void PopulateMMFields()
        {
            List<string> mmFields = new List<string>()
            {
                "FileNo",
                "MatterName",
                "ReLine",
                "DateOpened",
                "Client_Name",
                "Client_FileNo",
                "ClientIsCompany",
                "ClientNamesAll",
                "Party1",
                "Party2",
                "Party3",
                "Party4",
                "Party5",
                "Party6",
                "Party7",
                "Party8",
                "Party9",
                "MatterLawyerName",
                "MatterLawyerPhone",
                "MatterLawyerEmail",
                "MatterLawyerFax",
                "MatterLawyerLsn",
                "ShortStyleOfCause",
                "LongStyleOfCause",
                "CourtFileNumber",
                "CourtName",
                "ActForParty",
                "ActForNames",
                "ActForPartyAndNames",
                "PlaceCommenced"
            };

            this.MatterMasterFields.ItemsSource = mmFields;
            this.MatterMasterFields.SelectedIndex = 0;
        }

        private void CustomFieldsGroups_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string value = this.CustomFieldsGroups.SelectedValue as string;

            DataTable dataTable = null;
            if (value == "*")
            {
                dataTable = DataSourceManager.GetData("CustomFieldDefs", "1 = 1", "FD_FieldName, FD_Description");
            }
            else
            {
                dataTable = DataSourceManager.GetData("CustomFieldDefs", $"GX_GC_Key = @value", "FD_FieldName, FD_Description",
                    "GroupCodeXref", "GX_FD_Key = FD_Key", parameters: new Dictionary<string, string>() { { "value", value } });
            }

            List<KeyValuePair<string, string>> itemSource = new List<KeyValuePair<string, string>>();

            bool enable = false;
            foreach (DataRow row in dataTable.Rows)
            {
                itemSource.Add(new KeyValuePair<string, string>(row.ItemArray[0]?.ToString(), row.ItemArray[1]?.ToString()));
                enable = true;
            }

            this.CustomFields.ItemsSource = itemSource;
            this.CustomFields.SelectedIndex = 0;
            this.InsertCustomField.IsEnabled = enable;
        }

        private void InsertCustomField_Click(object sender, RoutedEventArgs e)
        {
            string title = this.CustomFields.SelectedValue as string + this.Swithces.SelectedValue as string;

            InsertContentControl(title);
        }

        private void InsertContentControl(string title)
        {
            Word.Range selectionRange = Globals.ThisAddIn.Application.Selection.Range;

            try
            {
                Microsoft.Office.Interop.Word.ContentControl contentControl = selectionRange.ContentControls.Add();
                contentControl.Title = title;
                contentControl.SetPlaceholderText(Text: title);
                contentControl.Tag = MatterMasterPaneUtils.MATTER_MASTER_TAG;
            }
            catch (System.NotImplementedException)
            {
                DocumentUtil.ShowCompatibilityModeError(new System.Threading.Tasks.Task(() => { InsertContentControl(title); }));
            }
        }

        private void MatterMasterFields_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (booleanFields.Contains((MatterMasterFields.SelectedItem as string)))
            {
                this.MatterMasterBoolean.Visibility = Visibility.Visible;
                MatterMasterFalse.Text = "";
                MatterMasterFalse.ShowPlaceholderIfNeeded();
                MatterMasterTrue.Text = "";
                MatterMasterTrue.ShowPlaceholderIfNeeded();
            }
            else
            {
                this.MatterMasterBoolean.Visibility = Visibility.Collapsed;
            }
        }

        private void EntityFields_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (booleanFields.Contains((EntityFields.SelectedItem as string)))
            {
                this.EntityBoolean.Visibility = Visibility.Visible;
                EntityTrue.Text = "";
                EntityTrue.ShowPlaceholderIfNeeded();
                EntityFalse.Text = "";
                EntityFalse.ShowPlaceholderIfNeeded();
            }
            else
            {
                this.EntityBoolean.Visibility = Visibility.Collapsed;
            }
        }

        private void InsertMatterMasterField_Click(object sender, RoutedEventArgs e)
        {
            string title = MatterMasterFields.SelectedValue as string + this.Swithces.SelectedValue as string;

            if (MatterMasterBoolean.Visibility == Visibility.Visible)
            {
                string booleanSwitch = $"<\\Boolean:{ MatterMasterPaneUtils.EscapeBooleanSwitch(MatterMasterFalse.Text)}|{MatterMasterPaneUtils.EscapeBooleanSwitch(MatterMasterTrue.Text)}>";
                title += booleanSwitch;
            }

            InsertContentControl(title);
        }

        private void InsertEntityField_Click(object sender, RoutedEventArgs e)
        {
            string title = "@" + Role.SelectedValue as string + "_" + EntityFields.SelectedValue as string + this.Swithces.SelectedValue as string;

            if (EntityBoolean.Visibility == Visibility.Visible)
            {
                string booleanSwitch = $"<\\Boolean:{ MatterMasterPaneUtils.EscapeBooleanSwitch(EntityFalse.Text)}|{MatterMasterPaneUtils.EscapeBooleanSwitch(EntityTrue.Text)}>";
                title += booleanSwitch;
            }

            InsertContentControl(title);
        }

        private void InsertERB_Click(object sender, RoutedEventArgs e)
        {
            string title = CustomConstants.ERB_NAME + this.ERBs.SelectedValue + "_";
            Word.Document document = ThisAddIn.Instance.Application.ActiveDocument;
            bool found = true;

            int i = 0;
            while (found)
            {
                found = document.Bookmarks.Exists(title + i);
                if (found)
                {
                    i++;
                }
            }

            document.Bookmarks.Add(title + i, document.Application.Selection.Range);

            BookmarkInserted.Visibility = Visibility.Visible;
            messageTimer = new Timer();
            messageTimer.Elapsed += MessageTimer_Elapsed;
            messageTimer.Interval = 3000;
            messageTimer.Enabled = true;
            dispatcher = Dispatcher.CurrentDispatcher;
            messageTimer.Start();
        }

        private void MessageTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            Dispatcher.Invoke(() =>
            {
                BookmarkInserted.Visibility = Visibility.Collapsed;
            }, DispatcherPriority.Normal);
            messageTimer.Stop();
            messageTimer = null;
        }
    }
}
