﻿using InfowareVSTO.Common;
using InfowareVSTO.Common.OfflineMode;
using Microsoft.Office.Core;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfowareVSTO.TaskPaneControls.SigningLines
{
    public class SigningLinesCacheManager
    {
        bool useDefault = false;
        bool? retry = null;

        public string GetImage(string name, MsoLanguageID languageId = MsoLanguageID.msoLanguageIDNone)
        {
            object authkey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
            if (authkey is string)
            {
                WebserviceResponse response = null;
                List<WSParameter> parameters = new List<WSParameter>() { new WSParameter("authKey", authkey.ToString()) };
                if (!useDefault && languageId != MsoLanguageID.msoLanguageIDNone)
                {
                    parameters.Add(new WSParameter("languageId", ((int)languageId).ToString()));
                }
                
                response = OfflineModeManager.GetWebserviceResponse("GetSigningLinePreviews", parameters);
                Dictionary<string, string> responseData = null;
                
                if (response?.Content is ApiResponseDTO)
                {
                    responseData = ((response?.Content as ApiResponseDTO).Data as JToken)?.ToObject<Dictionary<string, string>>();
                }

                if (responseData != null && responseData.TryGetValue(name, out string base64))
                {
                    return base64;
                }
                else if (responseData == null || responseData.Count == 0)
                {
                    if (retry == null)
                    {
                        retry = true;
                        CheckAndDownloadImages(languageId);
                        return GetImage(name, languageId);
                    }
                    else if(retry == true)
                    {
                        retry = false;
                    }
                }
            }

            return null;
        }

        public void CheckAndDownloadImages(MsoLanguageID languageId = MsoLanguageID.msoLanguageIDNone)
        {
            object authkey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
            if (authkey is string)
            {
                List<WSParameter> parameters = new List<WSParameter>() { new WSParameter("authKey", authkey.ToString()) };
                var defaultResponse = OfflineModeManager.GetWebserviceResponse("GetSigningLineHash", parameters.ToList());
                if (languageId != MsoLanguageID.msoLanguageIDNone)
                {
                    parameters.Add(new WSParameter("languageId", ((int)languageId).ToString()));
                }

                var languageResponse = OfflineModeManager.GetWebserviceResponse("GetSigningLineHash", parameters);

                string defaultHash = AdminPanelWebApi.GetSigningLineHash();
                string languageHash = string.Empty;
                if (languageId != MsoLanguageID.msoLanguageIDNone)
                {
                    languageHash = AdminPanelWebApi.GetSigningLineHash(languageId);
                    if (defaultHash == languageHash || string.IsNullOrWhiteSpace(languageHash))
                    {
                        useDefault = true;
                    }
                }
                else
                {
                    useDefault = true;
                }

                bool needsRedownload = false;

                if (useDefault)
                {
                    needsRedownload = defaultHash != (defaultResponse?.Content as ApiResponseDTO)?.Data?.ToString();
                }
                else
                {
                    needsRedownload = languageHash != (languageResponse?.Content as ApiResponseDTO)?.Data?.ToString();
                }

                if (needsRedownload || retry == true)
                {
                    if (useDefault)
                    {
                        AdminPanelWebApi.GetSigningLinePreviews();
                    }
                    else
                    {
                        AdminPanelWebApi.GetSigningLinePreviews(languageId);
                    }
                }
            }
        }
    }
}
