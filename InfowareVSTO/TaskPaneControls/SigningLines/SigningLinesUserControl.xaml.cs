﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using InfowareVSTO.Util;
using InfowareVSTO.Windows;
using Microsoft.Office.Interop.Word;
using WD = Microsoft.Office.Interop.Word;
using ListBox = System.Windows.Controls.ListBox;
using MessageBox = System.Windows.MessageBox;
using InfowareVSTO.Windows.WordDa.Library;
using static InfowareVSTO.Windows.WordDa.Library.InfowareWordDaLibraryClauseInsertionWindow;
using System.Threading;
using HtmlToImage;
using System.Windows.Media.Imaging;
using System.ComponentModel;
using System.Net;
using System.Security;
using InfowareVSTO.Common.General;
using System.Windows.Media;
using InfowareVSTO.TaskPaneControls.SigningLines;
using InfowareVSTO.Common.DTOs;

namespace InfowareVSTO.TaskPaneControls
{
    /// <summary>
    /// Interaction logic for SigningLinesUserControl.xaml
    /// </summary>
    public partial class SigningLinesUserControl : System.Windows.Controls.UserControl
    {
        public static List<SigningLine> GeneralSigningLines { get; set; }

        public static List<SigningLine> CompanyCorporationSigningLines { get; set; }

        public static List<SigningLine> SignedSealedDeliverySigningLines { get; set; }

        public static List<SigningLine> PleadingSigningLines { get; set; }

        public static List<ClauseGroup> CustomSigningLines { get; set; }

        private static Dictionary<string, StringBuilder> templateCache = new Dictionary<string, StringBuilder>();
        private static ClauseLibraryHelper clauseLibraryHelper;

        private static Assembly executingAssembly = Assembly.GetExecutingAssembly();

        private const string DATE_PLACEHOLDER = "##SL-DATE##";

        private const string YEAR_PLACEHOLDER = "##SL-YEAR##";

        private const string MESSAGEBOX_TITLE = "Infoware signing lines";
        private SigningLinesCacheManager signingLinesCacheManager;

        static SigningLinesUserControl()
        {
          //  InitSigningLines();
        }

        private void InitSigningLines()
        {
            string customSigningLines = AdminPanelWebApi.GetCustomSigningLine(MultiLanguage.MLanguageUtil.ActiveDocumentLanguage);
            if (string.IsNullOrEmpty(customSigningLines))
            {
                GeneralSigningLines = new List<SigningLine>()
                {
                    new SigningLine("1 Signature", "OneSignature.png", "1 Signature.xml"),
                    new SigningLine("1 Signature-Date & Witness", "OneSignatureDateAndWitness.png", "1 Signature-Date & Witness.xml"),
                    new SigningLine("1 Signature-Dated", "OneSignatureDated.png", "1 Signature-Dated.xml"),
                    new SigningLine("1 Signature-Witness", "OneSignatureWitness.png", "1 Signature-Witness.xml"),
                    new SigningLine("2 Signatures", "TwoSignatures.png", "2 Signatures.xml"),
                    new SigningLine("2 Signatures-Date & Witness", "TwoSignaturesDateAndWitness.png", "2 Signatures-Date & Witness.xml"),
                    new SigningLine("2 Signatures-Dated", "TwoSignaturesDated.png", "2 Signatures-Dated.xml"),
                    new SigningLine("2 Signatures-Witness", "TwoSignaturesWitness.png", "2 Signatures-Witness.xml"),
                    new SigningLine("Jurat", "Jurat.png", "Jurat.xml")
                };

                CompanyCorporationSigningLines = new List<SigningLine>()
                {
                    new SigningLine("1 Company", "OneCompany.png", "1 Company.xml"),
                    new SigningLine("1 Company-Authority to Bind", "OneCompanyAuthorityToBind.png", "1 Company-Authority to Bind.xml"),
                    new SigningLine("1 Company-Authority to Bind c/s", "OneCompanyAuthorityToBindCs.png", "1 Company-Authority to Bind cs.xml"),
                    new SigningLine("1 Company-c/s", "OneCompanyCs.png", "1 Company-cs.xml"),
                    new SigningLine("1 Company-Dated", "OneCompanyDated.png", "1 Company-Dated.xml"),
                    new SigningLine("1 Company-w/Witness", "OneCompanyWitness.png", "1 Company-wWitness.xml"),
                    new SigningLine("2 Companies", "TwoCompanies.png", "2 Companies.xml"),
                    new SigningLine("2 Companies-2 Sign", "TwoCompaniesTwoSign.png", "2 Companies-2 Sign.xml"),
                    new SigningLine("2 Companies-2 Sign-Authority", "TwoCompaniesTwoSignAuthority.png", "2 Companies-2 Sign-Authority.xml")
                };

                SignedSealedDeliverySigningLines = new List<SigningLine>()
                {
                    new SigningLine("SSD-1 Signature", "Ssd1Signature.png", "SSD-1 Signature.xml"),
                    new SigningLine("SSD-2 Signatures", "Ssd2Signatures.png", "SSD-2 Signatures.xml"),
                    new SigningLine("SSD-Company", "SsdCompany.png", "SSD-Company.xml")
                };

                PleadingSigningLines = new List<SigningLine>()
                {
                    new SigningLine("Address", "Address.png", "Address.xml"),
                    new SigningLine("Jurat-Affidavit", "JuratAffidavit.png", "Jurat-Affidavit.xml"),
                    new SigningLine("Name & Address", "NameAndAddress.png", "Name & Address.xml")
                };
            }
            else
            {
                clauseLibraryHelper = new ClauseLibraryHelper(customSigningLines);
                CustomSigningLines = clauseLibraryHelper.GetAllClauseGroups();
                signingLinesCacheManager = new SigningLinesCacheManager();
                signingLinesCacheManager.CheckAndDownloadImages(MultiLanguage.MLanguageUtil.ActiveDocumentLanguage);
            }
        }

        public SigningLinesUserControl()
        {
            InitSigningLines();
            InitializeComponent();

            if (CustomSigningLines != null)
            {
                this.CustomSigningLinesItemsControl.ItemsSource = CustomSigningLines;
                this.DefaultSigningLines.Visibility = Visibility.Collapsed;
                this.CustomSigningLinesItemsControl.Visibility = Visibility.Visible;
            }

            DataContext = this;
        }

        public class SigningLine
        {
            public string Name { get; set; }

            public string ImagePath { get; set; }

            public string TemplatePath { get; set; }

            public SigningLine(string name, string imageName, string templateName)
            {
                Name = name;
                ImagePath = @"..\..\Images\SigningLines\" + imageName;
                TemplatePath = @"Templates\" + templateName;
            }

            public override string ToString()
            {
                return $"{Name}: {ImagePath}";
            }
        }

        private void OnCustomListBoxMouseClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            Clause clause = (sender as FrameworkElement)?.DataContext as Clause;
            if (clause != null && string.IsNullOrEmpty((sender as FrameworkElement).Tag as string) && !string.IsNullOrEmpty(clause.XmlContent))
            {
                InsertXmlContentInSelection(clause.XmlContent);
            }
        }

        private void BackgoundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            if(e.Argument != null)
            {
                var clausePreview = e.Argument as ClausePreview;
                int i = GetClauseOrder(clausePreview.Clause);
                if (i >= 0)
                {
                    clausePreview.Base64 = signingLinesCacheManager.GetImage($"{i}.png", MultiLanguage.MLanguageUtil.ActiveDocumentLanguage);
                }
                e.Result = clausePreview;
            }
        }

        private void BackgoundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if(e.Result != null)
            {
                var clausePreview = e.Result as ClausePreview;
                AddImage(clausePreview.Base64, clausePreview.StackPanel);
            }
        }

        private void AddImage(string base64, object stackPanel)
        {
            if (!string.IsNullOrWhiteSpace(base64))
            {
                try
                {
                    byte[] image = Convert.FromBase64String(base64);
                    BitmapImage bitmapImage = LoadImage(image);
                    (((stackPanel as StackPanel).Children[1] as Grid).Children[0] as Image).Source = bitmapImage;
                    (((stackPanel as StackPanel).Children[1] as Grid).Children[0] as Image).Visibility = Visibility.Visible;
                    (((stackPanel as StackPanel).Children[1] as Grid).Children[1] as Image).Visibility = Visibility.Collapsed;
                }
                catch(Exception ex)
                {
                }
            }
        }

        private static BitmapImage LoadImage(byte[] imageData)
        {
            if (imageData == null || imageData.Length == 0) return null;
            var image = new BitmapImage();
            using (var mem = new MemoryStream(imageData))
            {
                mem.Position = 0;
                image.BeginInit();
                image.CreateOptions = BitmapCreateOptions.PreservePixelFormat;
                image.CacheOption = BitmapCacheOption.OnLoad;
                image.UriSource = null;
                image.StreamSource = mem;
                image.EndInit();
            }
            image.Freeze();
            return image;
        }

        private int GetClauseOrder(Clause clause)
        {
            if (clause != null)
            {
                int i = 0;
                foreach (ClauseGroup group in CustomSigningLines)
                {
                    foreach (var tempClause in group.Clauses)
                    {
                        if (tempClause.Equals(clause))
                        {
                            return i;
                        }
                        i++;
                    }
                }
            }

            return -1;
        }

        private string GetClauseHtml(Clause clause)
        {
            var clauseHtml = string.Empty;
            if(clause != null && !string.IsNullOrEmpty(clause.XmlContent))
            {
                var validXml = clauseLibraryHelper.GetValidXmlDocument(clause.XmlContent);
                if(!string.IsNullOrEmpty(validXml))
                {
                    clauseHtml = DocumentUtils.ConvertWordXmlDocumentToHtml(validXml);
                }
            }
            return clauseHtml;
        }

        private void OnListBoxMouseClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            // https://stackoverflow.com/questions/6938752/wpf-how-do-i-handle-a-click-on-a-listbox-item
            var selectedItem = ItemsControl.ContainerFromElement((ListBox)sender, (DependencyObject)e.OriginalSource) as ListBoxItem;

            if (selectedItem == null)
            {
                return;
            }

            if (DocumentUtil.IsCursorInsideTable())
            {
                const string warningMessage = "The cursor is currently positioned inside a table or signing line.\n" + "Press OK to insert the selected signing line in a new paragraph, or Cancel to cancel the insertion operation.";

                var confirmationWindow = new InfowareConfirmationWindow(warningMessage, MESSAGEBOX_TITLE);
                confirmationWindow.ShowDialog();

                if (confirmationWindow.Result == MessageBoxResult.Cancel)
                {
                    return;
                }

                Selection selection = DocumentUtil.GetSelection();

                if (selection.Range.Tables.Count > 0)
                {
                    Range firstTableRange = selection.Range.Tables[1].Range.Duplicate;
                    firstTableRange.Collapse(WdCollapseDirection.wdCollapseEnd);
                    firstTableRange.Select();
                }
            }

            string templatePath = ((SigningLine)selectedItem.Content).TemplatePath;

            StringBuilder rawTemplate;

            if (templateCache.ContainsKey(templatePath))
            {
                rawTemplate = templateCache[templatePath];
            }
            else
            {
                string fullTemplatePath = executingAssembly.GetManifestResourceNames()
                    .Where(it => it.EndsWith(templatePath.Replace(@"\", "."))).First();

                using (var streamReader = new StreamReader(executingAssembly.GetManifestResourceStream(fullTemplatePath)))
                {
                    rawTemplate = new StringBuilder(streamReader.ReadToEnd());
                    templateCache.Add(templatePath, rawTemplate);
                }
            }

            DateTime date = DateTime.Now;

            string template = rawTemplate.Replace(DATE_PLACEHOLDER, date.ToString("MMMM d, yyyy"))
                .Replace(YEAR_PLACEHOLDER, date.Year.ToString())
                .ToString();

            WD.Style oldSignLine = GetSignLineStyle();
            DocumentUtil.GetSelection().Range.InsertParagraphAfter();
            DocumentUtil.GetSelection().Range.InsertXML(template);

            WD.Style signLine = GetSignLineStyle();
            WD.Style normal = GetNormalStyle();
            if (oldSignLine == null && signLine != null)
            {
                signLine.set_BaseStyle("Normal");
                signLine.Font = normal.Font;
            }
        }

        private void OnSigningLineItemMouseWheel(object sender, System.Windows.Input.MouseWheelEventArgs e)
        {
            double currentVerticalOffset = MainScrollViewer.VerticalOffset;

            if (currentVerticalOffset == 0 && e.Delta > 0)
            {
                // There's no need to scroll up
                return;
            }

            MainScrollViewer.ScrollToVerticalOffset(currentVerticalOffset - (e.Delta));
        }

        private WD.Style GetSignLineStyle()
        {
            try
            {
                return DocumentUtil.GetActiveDocument().Styles["SignLine"];
            }
            catch (Exception)
            {
                return null;
            }
        }

        private WD.Style GetNormalStyle()
        {
            return DocumentUtil.GetActiveDocument().Styles["Normal"];
        }

        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ListBox listBox = (sender as ListBox);
            ListBoxItem listBoxItem = (ListBoxItem)(listBox.ItemContainerGenerator.ContainerFromIndex(listBox.SelectedIndex));
            ContentPresenter contentPresenter = DocumentUtils.FindVisualChild<ContentPresenter>(listBoxItem);
            DataTemplate myDataTemplate = contentPresenter.ContentTemplate;
            StackPanel stackPanel = (StackPanel)myDataTemplate.FindName("StackPanel", contentPresenter);
            
            Clause clause = listBox.SelectedItem as Clause;
            if (clause != null && clause.HtmlContent == null)
            {
                BackgroundWorker backgoundWorker = new BackgroundWorker();
                backgoundWorker.WorkerSupportsCancellation = true;
                backgoundWorker.CancelAsync();
                backgoundWorker.DoWork += BackgoundWorker_DoWork;
                backgoundWorker.RunWorkerCompleted += BackgoundWorker_RunWorkerCompleted;
                backgoundWorker.RunWorkerAsync(new ClausePreview { Clause = clause, StackPanel = stackPanel });
                return;
            }
        }

        private void InsertXmlContentInSelection(string xmlContent)
        {
            if (!string.IsNullOrEmpty(xmlContent))
            {
                DocumentUtil.GetSelection().Range.InsertParagraphAfter();
                Range range = DocumentUtil.GetSelection().Range;
                range.InsertXML(clauseLibraryHelper.GetValidXmlDocument(xmlContent));
                PopulateAuthorAndCompany(range);
            }
        }

        public void PopulateAuthorAndCompany(Range range)
        {
            if (range != null)
            {
                List<WD.ContentControl> contentControls = range.ContentControls.Cast<WD.ContentControl>().ToList();
                var author = AdminPanelWebApi.GetCurrentAuthor();

                PaneDTO paneDTO = new PaneDTO()
                {
                    Author = author,
                };

                PaneUtils.UsedContentControls = contentControls;
                PaneUtils.InsertGeneralTemplateDTO(ThisAddIn.Instance.Application.ActiveDocument, paneDTO);
                PaneUtils.UsedContentControls = null;
            }

        }
    }
}
