﻿using InfowareVSTO.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace InfowareVSTO.TaskPaneControls
{
    /// <summary>
    /// Interaction logic for OnlineHelpGuide.xaml
    /// </summary>
    public partial class OnlineHelpGuide : UserControl
    {
        public OnlineHelpGuide()
        {
            InitializeComponent();
            string url = $"{ConfigSettings.WebApiUrl}VSTO/Help/guide/index.html";
            Browser.Navigate(url);
        }
    }
}
