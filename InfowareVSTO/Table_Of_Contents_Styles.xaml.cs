﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Word = Microsoft.Office.Interop.Word;

namespace InfowareVSTO
{
    /// <summary>
    /// Interaction logic for TableOfContentStyles.xaml
    /// </summary>
    public partial class TableOfContentsStyles : InfowareWindow
    {
        private List<string> added { get; set; } = new List<string>();
        private List<string> outline = new List<string>();
        private List<string> Displayed
        {
            get
            {
                List<string> list = new List<string>();
                list.AddRange(added);
                list.AddRange(outline);
                return list.Distinct().ToList();
            }
        }
        public TableOfContentsStyles(List<string> selectedStyleNames, int? displayOutline = null )
        {
            InitializeComponent();

            var activeDocument = ThisAddIn.Instance.Application.ActiveDocument;
            if (activeDocument != null)
            {
                foreach (Word.Style currentStyle in activeDocument.Styles)
                {
                    if (currentStyle.Type == Word.WdStyleType.wdStyleTypeParagraph || currentStyle.Type == Word.WdStyleType.wdStyleTypeParagraphOnly)
                    {
                        var currentStyleValue = false;
                        bool enabled = true;
                        if(selectedStyleNames != null && selectedStyleNames.Contains(currentStyle.NameLocal))
                        {
                            added.Add(currentStyle.NameLocal);
                            currentStyleValue = true;
                        }

                        if (displayOutline != null && currentStyle.ParagraphFormat.OutlineLevel == (Word.WdOutlineLevel)displayOutline.Value)
                        {
                            outline.Add(currentStyle.NameLocal);
                            currentStyleValue = true;
                            enabled = false;
                        }

                        AllStyle.Items.Add(new StyleCheckbox() { Name = currentStyle.NameLocal, Value = currentStyleValue, Enabled = enabled });
                    }
                }

                Refresh();
            }
        }

        private void OK_Style(object sender, RoutedEventArgs e)
        {   
            Close();
        }

        private void Cancel_Style(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Refresh()
        {
            LevelStyle.ItemsSource = Displayed;
        }

        public List<string> GetSelectedStyles()
        {
            return added;
        }

        private void Style_Checked(object sender, RoutedEventArgs e)
        {
            if (sender is CheckBox checkbox)
            {
                added.Add((string)checkbox.Tag);
                Refresh();
            }
        }

        private void Style_Unchecked(object sender, RoutedEventArgs e)
        {
            if (sender is CheckBox checkbox)
            {
                added.Remove((string)checkbox.Tag);
                Refresh();
            }
        }
    }
}
