﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace InfowareVSTO
{
	/// <summary>
	/// Interaction logic for Envelope_WPF.xaml
	/// </summary>
	public partial class Envelope_WPF : InfowareWindow
	{
		public string Handling
		{
			get
			{
				return this.txtHandling.Text;
			}
			set {
				this.txtHandling.Text = value;
			}
		}
		public string Attention
		{
			get
			{
				return this.txtAttention.Text;
			}
			set {
				this.txtAttention.Text = value;
			}
		}
		public string Address
		{
			get
			{
				return this.txtAddress.Text;
			}
			set {
				this.txtAddress.Text = value;
			}
		}
		public string FileNo
		{
			get
			{
				return this.txtFileNo.Text;
			}
			set {
				this.txtFileNo.Text = value;
			}
		}
		public string Initials { get; set; }
		public string Author { get; set; }
        public string EnvTemplate
		{
			get
			{
				return ((ComboBoxItem)this.cmbTemplate.SelectedItem).Content.ToString();
			}
			set {
				this.cmbTemplate.SelectedValue = value;
			}
		}

		public Envelope_WPF()
		{
			InitializeComponent();
		}

		private void OkButton_Click(object sender, RoutedEventArgs e)
		{	
			this.DialogResult = true;
			this.Close();
		}

		private void CancelButton_Click(object sender, RoutedEventArgs e)
        {			
            this.Close();
        }
       
        private void chkAllAddr_Checked(object sender, RoutedEventArgs e)
        {
            txtAddress.IsEnabled = ((CheckBox)sender).IsChecked != true;
        }
    }
}
