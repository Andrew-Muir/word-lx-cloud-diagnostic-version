﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using InfowareVSTO.Common.Language;

namespace InfowareVSTO.Language
{
    public class LanguageConverter : IMultiValueConverter
    {
        // parameters are 
        // id
        // default Value
        object IMultiValueConverter.Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values.Length < 2)
            {
                return null;
            }

            if (values[0] is int && values[1] is string)
            {
                return LanguageManager.GetTranslation((int)values[0], values[1] as string);
            }

            return null;
        }

        object[] IMultiValueConverter.ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
