﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using InfowareVSTO.Common.Language;

namespace InfowareVSTO.Language
{
    /// <summary>
    /// Interaction logic for LanguageChooser.xaml
    /// </summary>
    public partial class LanguageChooser : InfowareWindow
    {
        public LanguageChooser()
        {
            InitializeComponent();

            PopulateComboBox();
            SetSelectedLanguage();
        }

        private void SetSelectedLanguage()
        {
            LanguageEnum selectedLanguage = LanguageManager.GetLanguage();

            MainComboBox.Text = selectedLanguage.ToString();
        }

        private void PopulateComboBox()
        {
           MainComboBox.ItemsSource  = Enum.GetValues(typeof(LanguageEnum)).Cast<LanguageEnum>().Select(x=>x.ToString());
        }

        private void OkButton_Click(object sender, RoutedEventArgs e)
        {
            string languageStr = MainComboBox.Text;

            Enum.TryParse(languageStr, out LanguageEnum language);

            LanguageManager.SetLanguage(language);
            ThisAddIn.Instance.Ribbon.Invalidate();
            this.Close();
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void MainComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string languageStr = e.AddedItems[0] as string;
            Flag.Source = new BitmapImage(new Uri(@"Flags\" + languageStr + ".png", UriKind.Relative));
        }
    }
}
