﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace InfowareVSTO
{
	/// <summary>
	/// Interaction logic for LabelGrid_WPF.xaml
	/// </summary>
	public partial class LabelGridStartingPosition_WPF : InfowareWindow
	{
		public int SelectedPosition { get; set; }
		public int NrRows { get; set; }
		public int NrColumns { get; set; }
		public bool ResultOk { get; set; }

		public LabelGridStartingPosition_WPF(int nrRows, int nrColumns, int selectedPosition)
		{
			InitializeComponent();

			NrRows = nrRows;
			NrColumns = nrColumns;
			SelectedPosition = selectedPosition;

			InitGrid();
			InitPanels();
		}

		private void InitGrid()
		{
			gridPosition.ColumnDefinitions.Clear();
			for (int i = 0; i < NrColumns; i++)
			{
				var column = new ColumnDefinition();
				column.Width = new GridLength(50);
				gridPosition.ColumnDefinitions.Add(column);
			}

			gridPosition.RowDefinitions.Clear();
			for (int i = 0; i < NrRows; i++)
			{
				var row = new RowDefinition();
				row.Height = new GridLength(50);
				gridPosition.RowDefinitions.Add(new RowDefinition());
			}
		}
				
		private void InitPanels()
		{
			int iRow = -1;
			foreach (RowDefinition row in gridPosition.RowDefinitions)
			{
				iRow++;
				int iCol = -1;
				foreach(ColumnDefinition col in gridPosition.ColumnDefinitions)
				{
					iCol++;
					Border panel = new Border();
					Grid.SetColumn(panel, iCol);
					Grid.SetRow(panel, iRow);

					Label label = new Label();
					label.Content = "";
					label.HorizontalContentAlignment = HorizontalAlignment.Center;
					label.VerticalContentAlignment = VerticalAlignment.Center;
					panel.Child = label;
					panel.MouseEnter += Panel_MouseEnter;
					panel.MouseLeave += Panel_MouseLeave;
					panel.MouseDown += Panel_MouseDown;
					panel.Margin = new Thickness(1);
					panel.Padding = new Thickness(0, 5, 0, 0);
					int position = iRow * NrColumns + iCol + 1;
					if(position == SelectedPosition)
						panel.Background = new SolidColorBrush(Color.FromRgb(0, 0, 139));
					else
						panel.Background = new SolidColorBrush(Color.FromRgb(211, 211, 211));
					gridPosition.Children.Add(panel);
				}
			}
		}

		private void Panel_MouseDown(object sender, MouseButtonEventArgs e)
		{
			Border panel = sender as Border;
			int iCol = Grid.GetColumn(panel);
			int iRow = Grid.GetRow(panel);
			int position = iRow * NrColumns + iCol + 1;			
			panel.BorderThickness = new Thickness(1);

			if (SelectedPosition != position)
			{
				foreach (var child in gridPosition.Children)
				{
					if(child.GetType() == typeof(Border))
						(child as Border).Background = new SolidColorBrush(Color.FromRgb(211, 211, 211));
				}					
				panel.Background = new SolidColorBrush(Color.FromRgb(0, 0, 139));
			}
			
			SelectedPosition = iRow * NrColumns + iCol + 1;
		}

		private void Panel_MouseLeave(object sender, MouseEventArgs e)
		{
			Border panel = sender as Border;
			panel.BorderThickness = new Thickness(1);
			int iCol = Grid.GetColumn(panel);
			int iRow = Grid.GetRow(panel);			
			int position = iRow * NrColumns + iCol + 1;
			if(SelectedPosition != position)
				panel.Background = new SolidColorBrush(Color.FromRgb(211, 211, 211));
		}

		private void Panel_MouseEnter(object sender, MouseEventArgs e)
		{
			Border panel = sender as Border;
			panel.BorderThickness = new Thickness(1);
			int iCol = Grid.GetColumn(panel);
			int iRow = Grid.GetRow(panel);			
			int position = iRow * NrColumns + iCol + 1;
			if(SelectedPosition != position)
				panel.Background = new SolidColorBrush(Color.FromRgb(173, 216, 230));
		}

		private void Button_Click(object sender, RoutedEventArgs e)
		{
			SelectedPosition = 3;
		}

		private void OkButton_Click(object sender, RoutedEventArgs e)
		{
			ResultOk = true;
			this.Close();
		}

		private void CancelButton_Click(object sender, RoutedEventArgs e)
		{
			ResultOk = false;
			this.Close();
		}
	}
}
