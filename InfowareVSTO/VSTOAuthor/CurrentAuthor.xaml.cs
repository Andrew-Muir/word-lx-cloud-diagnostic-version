﻿using InfowareVSTO.Common;
using Microsoft.Office.Interop.Word;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace InfowareVSTO.VSTOAuthor
{
    /// <summary>
    /// Interaction logic for CurrentAuthor.xaml
    /// </summary>
    public partial class CurrentAuthor : InfowareWindow
    {
        public class AuthorData
        {
            public int    Id        { get; set; }
            public string FirstName { get; set; }
            public string LastName  { get; set; }
            public string JobTitle  { get; set; }
            public string FullName  { get; set; }
            public bool IsFavorite  { get; set; }
            public bool IsPublic    { get; set; }
            public bool IsEditable  { get; set; }
        }
        public class ComboBoxItemClass
        {
            public string name = null;
            public ComboBoxItem cbi = null;
            public AuthorData authorData = null;
        }
        public class ComboBoxItemByNameClass
        {
            public List<ComboBoxItemClass> all_sections = new List<ComboBoxItemClass>();

            public ComboBoxItemClass this[string i]
            {
                get
                {
                    foreach (ComboBoxItemClass aux in all_sections)
                    {
                        if (i == aux.name)
                        {
                            return aux;
                        }
                    }
                    return null;
                }
                set
                {
                    value.name = i;
                    all_sections.Add(value);
                }
            }
        }
        private AuthorData currentAuthorData;

        ComboBoxItemByNameClass AllComboBoxItemByName = new ComboBoxItemByNameClass();

        public CurrentAuthor()
        {
            InitializeComponent();
            this.SetListOfAuthors();

            object authkey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
            if (authkey is string)
            {
                RestClient client = new RestClient(ConfigSettings.WebApiUrl);
                RestRequest request;
                IRestResponse response;
                JObject content;

                request = new RestRequest("api/WebApi/GetCurrentAuthor?authKey=" + (authkey as string), Method.GET);
                response = client.Execute(request);

                content = JsonConvert.DeserializeObject<JObject>(response.Content);
                int statusCode = content["StatusCode"].ToObject<int>();

                if (statusCode == 200)
                {
                    JToken author = content["Data"];
                    if (author.Count() > 0)
                    {
                        int authorId = Convert.ToInt32(author["Id"].ToString());
                        foreach (ComboBoxItemClass aux2 in AllComboBoxItemByName.all_sections)
                        {
                            if (authorId == aux2.authorData.Id)
                            {
                                currentAuthorFavorite.IsChecked = aux2.authorData.IsFavorite;
                                currentAuthorName.Text = aux2.authorData.FullName;
                            }
                        }
                    }
                }
            }
        }

        private void SetListOfAuthors()
        {
            AuthorsList.Items.Clear();
            AuthorsList.SelectedItem = null;
            AllComboBoxItemByName = new ComboBoxItemByNameClass();

            object authkey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
            if (authkey is string)
            {
                RestClient client = new RestClient(ConfigSettings.WebApiUrl);
                RestRequest request;
                IRestResponse response;
                JObject content;

                request = new RestRequest("api/WebApi/GetAuthors?authKey=" + (authkey as string) + "&type=0", Method.GET);
                response = client.Execute(request);

                content = JsonConvert.DeserializeObject<JObject>(response.Content);
                int statusCode = content["StatusCode"].ToObject<int>();

                if (statusCode == 200)
                {
                    JToken data = content["Data"];
                    if (data.Count() > 0)
                    {
                        List<JToken> dataArray = data.Value<JArray>().ToList();
                        List<AuthorData> listFavoriteAuthors = new List<AuthorData>();

                        foreach (JToken dataObj in dataArray)
                        {
                            ComboBoxItemClass boxItem = new ComboBoxItemClass();
                            boxItem.authorData = new AuthorData();
                            boxItem.cbi = new ComboBoxItem();

                            boxItem.authorData.Id           = Convert.ToInt32(dataObj["Id"].ToString());
                            boxItem.authorData.FirstName    = dataObj["FirstName"].ToString();
                            boxItem.authorData.LastName     = dataObj["LastName"].ToString();
                            boxItem.authorData.JobTitle     = dataObj["JobTitle"].ToString();
                            boxItem.authorData.IsFavorite   = Convert.ToBoolean(dataObj["IsFavorite"].ToString());
                            boxItem.authorData.IsEditable   = Convert.ToBoolean(dataObj["IsEditable"].ToString());
                            boxItem.authorData.IsPublic     = Convert.ToBoolean(dataObj["IsPublic"].ToString());
                            boxItem.authorData.FullName     = boxItem.authorData.FirstName + " " + boxItem.authorData.LastName + ", " + boxItem.authorData.JobTitle;

                            boxItem.cbi.Content = boxItem.authorData.FullName;

                            AuthorsList.Items.Add(boxItem.cbi);
                            if (boxItem.authorData.IsFavorite)
                            {
                                listFavoriteAuthors.Add(boxItem.authorData);
                            }
                            AllComboBoxItemByName[boxItem.authorData.FullName] = boxItem;
                        }
                        FavoriteAuthorsDataGrid.ItemsSource = listFavoriteAuthors;
                    }
                }
            }
        }

        private void OK_Click(object sender, RoutedEventArgs e)
        {
            object authkey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
            if (authkey is string)
            {
                RestClient client = new RestClient(ConfigSettings.WebApiUrl);
                RestRequest request;
                IRestResponse response;


                foreach (ComboBoxItemClass cbi in AllComboBoxItemByName.all_sections)
                {
                    request = new RestRequest("api/WebApi/GetSetIsFavorite?authKey=" + (authkey as string) + "&authorId=" + cbi.authorData.Id + "&IsFav=" + cbi.authorData.IsFavorite.ToString(), Method.GET);
                    response = client.Execute(request);
                }

                if (currentAuthorData != null)
                {
                    request = new RestRequest("api/WebApi/GetSetCurrentAuthorId?authKey=" + (authkey as string) + "&authorId=" + currentAuthorData.Id, Method.GET);
                    response = client.Execute(request);
                }
            }

            this.Close();
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void AuthorsListSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string aux = (AuthorsList.SelectedValue as ComboBoxItem).Content.ToString();
            if ( AllComboBoxItemByName[aux] != null )
            {
                currentAuthorName.Text = aux;
                currentAuthorData = AllComboBoxItemByName[aux].authorData;
                currentAuthorFavorite.IsChecked = AllComboBoxItemByName[aux].authorData.IsFavorite;
            }
        }

        private void RemoveClick(object sender, RoutedEventArgs e)
        {
            AuthorData aux = ((sender as System.Windows.Controls.CheckBox).DataContext as AuthorData);
            if (aux!=null)
            {
                ComboBoxItemClass aux2 = AllComboBoxItemByName[aux.FullName];
                if (aux2 != null && aux2.authorData != null)
                {
                    List<AuthorData> listOfAuthorsData = (FavoriteAuthorsDataGrid.ItemsSource as List<AuthorData>);
                    aux2.authorData.IsFavorite = false;
                    listOfAuthorsData.Remove(aux2.authorData);
                    if (currentAuthorName.Text == aux.FullName)
                    {
                        currentAuthorFavorite.IsChecked = false;
                    }
                    FavoriteAuthorsDataGrid.ItemsSource = null;
                    FavoriteAuthorsDataGrid.ItemsSource = listOfAuthorsData;
                }
            }
        }

        private void FavoriteAuthorSelected(object sender, SelectionChangedEventArgs e)
        {
            if (FavoriteAuthorsDataGrid.CurrentColumn!= null && FavoriteAuthorsDataGrid.CurrentColumn.DisplayIndex > 0)
            {
                AuthorData aux = ((sender as System.Windows.Controls.DataGrid).SelectedValue as AuthorData);
                if (aux!=null)
                {
                    currentAuthorName.Text = aux.FullName;
                    currentAuthorFavorite.IsChecked = aux.IsFavorite;
                    currentAuthorData = aux;
                }
            }
        }

        private void currentAuthorFavorite_Click(object sender, RoutedEventArgs e)
        {
            ComboBoxItemClass aux2 = AllComboBoxItemByName[currentAuthorName.Text];
            List<AuthorData> listOfAuthorsData =(FavoriteAuthorsDataGrid.ItemsSource as List<AuthorData>);
            if (aux2!= null && aux2.authorData!=null)
            {
                aux2.authorData.IsFavorite = !aux2.authorData.IsFavorite;
                if (aux2.authorData.IsFavorite)
                {
                    listOfAuthorsData.Add(aux2.authorData);
                }
                else
                {
                    listOfAuthorsData.Remove(aux2.authorData);
                }
                FavoriteAuthorsDataGrid.ItemsSource = null;
                FavoriteAuthorsDataGrid.ItemsSource = listOfAuthorsData;
            }
        }
    }
}
