﻿using Microsoft.Office.Interop.Word;
using InfowareVSTO.Windows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using InfowareVSTO.Common;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;

namespace InfowareVSTO.VSTOAuthor
{
    /// <summary>
    /// Interaction logic for AuthorInfo.xaml
    /// </summary>
    public partial class AuthorInfo : InfowareWindow
    {
        public List<string> JobTitles { get; private set; }

        public List<string> SalutationPrefixes { get; private set; }

        public List<string> SalutationSuffixes { get; private set; }

        private bool useJobTitleComboBox;

        public bool UseJobTitleComboBox
        {
            get => useJobTitleComboBox;

            set
            {
                useJobTitleComboBox = value;
                useJobTitleTextBox = !value;
            }
        }

        private bool useJobTitleTextBox;

        public bool UseJobTitleTextBox
        {
            get => useJobTitleTextBox;

            set
            {
                useJobTitleTextBox = value;
                useJobTitleComboBox = !value;
            }
        }

        private int defaultJobTitleIndex;

        public class ComboBoxItemClass
        {
            public string name = null;
            public ComboBoxItem cbi = null;
            public JToken jToken = null;
        }

        public class ComboBoxItemByNameClass
        {
            private List<ComboBoxItemClass> all_sections = new List<ComboBoxItemClass>();

            public ComboBoxItemClass this[string i]
            {
                get
                {
                    foreach (ComboBoxItemClass aux in all_sections)
                    {
                        if (i == aux.name)
                        {
                            return aux;
                        }
                    }
                    return null;
                }
                set
                {
                    value.name = i;
                    all_sections.Add(value);
                }
            }
        }

        private ComboBoxItemByNameClass AllComboBoxItemByName = new ComboBoxItemByNameClass();
        private ComboBoxItemClass selectedAuthor = null;
        private bool textHasChanged = false;

        public AuthorInfo()
        {
            InitializeComponent();
            SetListOfAuthors();
            refreshSelected();
            textHasChanged = false;

            RightSide.Visibility = Visibility.Hidden;

            UpdateJobTitles();
            UpdateDefaultJobTitleIndex();

            UpdateSalutationPrefixes();
            UpdateSalutationSuffixes();

            DataContext = this;

            Loaded += OnWindowLoaded;
        }

        private void OnWindowLoaded(object sender, RoutedEventArgs e)
        {
            /**
             * Required, because the ComboBox's SelectionChange hander is called when the binding is made, which sets 
             * this to true. Otherwise, an unwanted MessageBox will show up when pressing the New button
             */
            textHasChanged = false;
        }

        private void UpdateJobTitles()
        {
            string titles = SettingsManager.GetSetting("Titles", "Glossary");

            if (string.IsNullOrEmpty(titles))
            {
                JobTitles = null;
                UseJobTitleTextBox = true;
            }
            else
            {
                JobTitles = SettingsManager.UnescapeCommaCharacter(titles.Split(',')).Select(it => it.Trim()).ToList();
                UseJobTitleComboBox = true;
            }
        }

        private void UpdateDefaultJobTitleIndex()
        {
            const int defaultInvalidIndex = -1;

            if (UseJobTitleTextBox)
            {
                defaultJobTitleIndex = defaultInvalidIndex;
            }
            else
            {
                int rawDefaultJobTitleIndex = SettingsManager.GetSettingAsInt("TitlesDefaultIdx", "Glossary");

                if (rawDefaultJobTitleIndex < 0 || rawDefaultJobTitleIndex > JobTitles.Count - 1)
                {
                    defaultJobTitleIndex = defaultInvalidIndex;
                }
                else
                {
                    defaultJobTitleIndex = rawDefaultJobTitleIndex;
                }
            }
        }

        private void UpdateSalutationPrefixes()
        {
            string rawSalutationPrefixes = SettingsManager.GetGeneralSetting("SalFilter");

            if (rawSalutationPrefixes == null)
            {
                SalutationPrefixes = new List<string>()
                {
                    "None",
                    "Miss",
                    "Mrs",
                    "Mr",
					"Mx",
					"Dr",
                    "Atty",
                    "Prof",
                    "Hon"
                };

                return;
            }

            SalutationPrefixes = SettingsManager.UnescapeCommaCharacter(rawSalutationPrefixes.Split(',')).Select(it => it.Trim()).ToList();
        }

        private void UpdateSalutationSuffixes()
        {
            string rawSalutationSuffixes = SettingsManager.GetGeneralSetting("SalPostFix");

            if (rawSalutationSuffixes == null)
            {
                SalutationSuffixes = new List<string>()
                {
                    "None",
                    "Jr",
                    "Sr",
                    "2nd",
                    "II",
                    "III",
                    "IV",
                    "Esq",
                    "CPA",
                    "MD",
                    "PhD"
                };

                return;
            }

            SalutationSuffixes = SettingsManager.UnescapeCommaCharacter(rawSalutationSuffixes.Split(',')).Select(it => it.Trim()).ToList();
        }

        private void SetListOfAuthors()
        {
            AuthorsList.Items.Clear();
            AuthorsList.SelectedItem = null;
            AllComboBoxItemByName = new ComboBoxItemByNameClass();

            object authkey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
            if (authkey is string)
            {
                var client = new RestClient(ConfigSettings.WebApiUrl);
                RestRequest request;
                IRestResponse response;
                JObject content;

                request = new RestRequest("api/WebApi/GetAuthors?authKey=" + (authkey as string) + "&type=0", Method.GET);
                response = client.Execute(request);

                content = JsonConvert.DeserializeObject<JObject>(response.Content);
                int statusCode = content["StatusCode"].ToObject<int>();

                if (statusCode == 200)
                {
                    JToken data = content["Data"];
                    if (data.Count() > 0)
                    {
                        var dataArray = data.Value<JArray>().ToList();

                        foreach (JToken dataObj in dataArray)
                        {
                            var boxItem = new ComboBoxItemClass
                            {
                                jToken = dataObj,
                                cbi = new ComboBoxItem()
                            };
                            string aux_t = dataObj["FirstName"].ToString() + " " + dataObj["LastName"].ToString() + ", " + dataObj["JobTitle"].ToString();
                            boxItem.cbi.Content = aux_t;
                            AuthorsList.Items.Add(boxItem.cbi);
                            if (dataObj["IsFavorite"].ToString() == "true")
                            {
                                boxItem.cbi.IsSelected = true;
                            }
                            AllComboBoxItemByName[aux_t] = boxItem;
                        }

                    }
                }
            }
        }

        private void Edit_Click(object sender, RoutedEventArgs e)
        {
            var val = (AuthorsList.SelectedValue as ComboBoxItem);
            if (val != null)
            {
                if (textHasChanged)
                {
                    // Display message box
                    var confirmationWindow = new InfowareConfirmationWindow("You made changes to the author and didn't save.\n" + "If you switch to a different author now your changes will be lost.", "Discard Changes?");
                    confirmationWindow.ShowDialog();

                    // Process message box results 
                    if (confirmationWindow.Result == MessageBoxResult.Cancel)
                    {
                        return;
                    }
                }

                ComboBoxItemClass cbi = AllComboBoxItemByName[val.Content.ToString()];

                object authkey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
                if (authkey is string && cbi != null && cbi.jToken != null && cbi.jToken["Id"] != null)
                {
                    var client = new RestClient(ConfigSettings.WebApiUrl);
                    RestRequest request;
                    IRestResponse response;
                    JObject content;

                    request = new RestRequest("api/WebApi/GetAuthor?authKey=" + (authkey as string) + "&authorId=" + cbi.jToken["Id"].ToString(), Method.GET);
                    response = client.Execute(request);

                    content = JsonConvert.DeserializeObject<JObject>(response.Content);
                    int statusCode = content["StatusCode"].ToObject<int>();

                    if (statusCode == 200)
                    {
                        JToken data = content["Data"];
                        if (data.Count() > 0)
                        {
                            RightSide.Visibility = Visibility.Visible;
                            SaveBtn.Content = "Update";
                            LabelSave.Text = "Edit Author";

                            EditId.Text = ((data["Id"] != null) ? data["Id"].ToString() : "");
                            Prefix.Text = ((data["Prefix"] != null) ? data["Prefix"].ToString() : "");
                            Sufix.Text = ((data["Suffix"] != null) ? data["Suffix"].ToString() : "");
                            FirstName.Text = ((data["FirstName"] != null) ? data["FirstName"].ToString() : "");
                            MiddleName.Text = ((data["MiddleName"] != null) ? data["MiddleName"].ToString() : "");
                            LastName.Text = ((data["LastName"] != null) ? data["LastName"].ToString() : "");
                            Initials.Text = ((data["Initials"] != null) ? data["Initials"].ToString() : "");

                            string jobTitle = data["JobTitle"]?.ToString() ?? "";

                            if (UseJobTitleTextBox)
                            {
                                JobTitle.Text = jobTitle;
                            }
                            else
                            {
                                ComboJobTitles.SelectedIndex = JobTitles.IndexOf(jobTitle);
                            }

                            EmployeeID.Text = ((data["EmployeeId"] != null) ? data["EmployeeId"].ToString() : "");
                            Lawyer.Text = ((data["LawyerNumber"] != null) ? data["LawyerNumber"].ToString() : "");
                            Credentials.Text = ((data["Credentials"] != null) ? data["Credentials"].ToString() : "");
                            Certification.Text = ((data["Certification"] != null) ? data["Certification"].ToString() : "");
                            AddressID.Text = "";
                            Address1.Text = "";
                            Address2.Text = "";
                            City.Text = "";
                            ProvState.Text = "";
                            Postal_ZipCode.Text = "";
                            Country.Text = "";
                            if (data["Addresses"] != null)
                            {
                                var addrs = data["Addresses"].Value<JArray>().ToList();

                                foreach (JToken address in addrs)
                                {
                                    AddressID.Text = ((address["Id"] != null) ? address["Id"].ToString() : "");
                                    Address1.Text = ((address["Address1"] != null) ? address["Address1"].ToString() : "");
                                    Address2.Text = ((address["Address2"] != null) ? address["Address2"].ToString() : "");
                                    City.Text = ((address["City"] != null) ? address["City"].ToString() : "");
                                    ProvState.Text = ((address["Province"] != null) ? address["Province"].ToString() : "");
                                    Postal_ZipCode.Text = ((address["PostalCode"] != null) ? address["PostalCode"].ToString() : "");
                                    Country.Text = ((address["Country"] != null) ? address["Country"].ToString() : "");
                                }
                            }

                            PhoneID.Text = "";
                            MobileCellID.Text = "";
                            EmailID.Text = "";
                            FaxID.Text = "";
                            Phone.Text = "";
                            MobileCell.Text = "";
                            Email.Text = "";
                            Fax.Text = "";
                            if (data["ContactInformations"] != null)
                            {
                                var addrs = data["ContactInformations"].Value<JArray>().ToList();

                                foreach (JToken address in addrs)
                                {
                                    if (address["Type"] != null)
                                    {
                                        if (address["Type"].ToString() == "Phone")
                                        {
                                            PhoneID.Text = ((address["Id"] != null) ? address["Id"].ToString() : "");
                                            Phone.Text = ((address["Details"] != null) ? address["Details"].ToString() : "");
                                        }
                                        else if (address["Type"].ToString() == "Mobile")
                                        {
                                            PhoneID.Text = ((address["Id"] != null) ? address["Id"].ToString() : "");
                                            MobileCell.Text = ((address["Details"] != null) ? address["Details"].ToString() : "");
                                        }
                                        else if (address["Type"].ToString() == "Email")
                                        {
                                            EmailID.Text = ((address["Id"] != null) ? address["Id"].ToString() : "");
                                            Email.Text = ((address["Details"] != null) ? address["Details"].ToString() : "");
                                        }
                                        else if (address["Type"].ToString() == "Fax")
                                        {
                                            FaxID.Text = ((address["Id"] != null) ? address["Id"].ToString() : "");
                                            Fax.Text = ((address["Details"] != null) ? address["Details"].ToString() : "");
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                textHasChanged = false;
            }
        }

        private void Select_Click(object sender, RoutedEventArgs e)
        {
            var val = (AuthorsList.SelectedValue as ComboBoxItem);
            if (!string.IsNullOrWhiteSpace(val?.Content?.ToString()))
            {
                selectedAuthor = AllComboBoxItemByName[val.Content.ToString()];
            }
            refreshSelected();
        }


        private void refreshSelected()
        {
            YouHaveSelectedAuthor.Visibility = Visibility.Hidden;
            CurrentAuthorIs.Visibility = Visibility.Hidden;
            if (selectedAuthor != null && !string.IsNullOrWhiteSpace(selectedAuthor.cbi.Content.ToString()))
            {
                YouHaveSelectedAuthor.Visibility = Visibility.Visible;
                selectedAuthorName.Text = selectedAuthor.cbi.Content.ToString() + ".";
            }
            else
            {
                object authkey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
                if (authkey is string)
                {
                    var client = new RestClient(ConfigSettings.WebApiUrl);
                    RestRequest request;
                    IRestResponse response;
                    JObject content;

                    request = new RestRequest("api/WebApi/GetCurrentAuthor?authKey=" + (authkey as string), Method.GET);
                    response = client.Execute(request);

                    content = JsonConvert.DeserializeObject<JObject>(response.Content);
                    int statusCode = content["StatusCode"].ToObject<int>();

                    if (statusCode == 200)
                    {
                        JToken author = content["Data"];
                        if (author.Count() > 0)
                        {
                            CurrentAuthorIs.Visibility = Visibility.Visible;
                            currentAuthorName.Text = author["FirstName"].ToString() + " " + author["LastName"].ToString() + ", " + author["JobTitle"].ToString();
                        }
                    }
                }
            }
        }


        private void New_Click(object sender, RoutedEventArgs e)
        {
            if (textHasChanged)
            {
                // Display message box
                var confirmationWindow = new InfowareConfirmationWindow("You made changes to the author and didn't save.\n" + "If you create a different author now your changes will be lost.", "Discard Changes?");
                confirmationWindow.ShowDialog();

                // Process message box results 
                if (confirmationWindow.Result == MessageBoxResult.Cancel)
                {
                    return;
                }
            }
            RightSide.Visibility = Visibility.Visible;
            SaveBtn.Content = "Add";
            LabelSave.Text = "Add New Author";

            EditId.Text = "";
            //Prefix.Text = "";
            //Sufix.Text = "";
            FirstName.Text = "";
            MiddleName.Text = "";
            LastName.Text = "";
            Initials.Text = "";

            if (UseJobTitleTextBox)
            {
                JobTitle.Text = "";
            }
            else
            {
                ComboJobTitles.SelectedIndex = defaultJobTitleIndex;
            }

            EmployeeID.Text = "";
            Lawyer.Text = "";
            Credentials.Text = "";
            Certification.Text = "";
            Address1.Text = "";
            Address2.Text = "";
            City.Text = "";
            ProvState.Text = "";
            Postal_ZipCode.Text = "";
            Country.Text = "";
            Phone.Text = "";
            MobileCell.Text = "";
            Email.Text = "";
            Fax.Text = "";
            AddressID.Text = "";
            PhoneID.Text = "";
            MobileCellID.Text = "";
            EmailID.Text = "";
            FaxID.Text = "";
            textHasChanged = false;
        }

        private void SaveBtn_Click(object sender, RoutedEventArgs e)
        {
            string jobTitle;
            Control jobTitleWidget;

            if (UseJobTitleTextBox)
            {
                jobTitleWidget = JobTitle;
                jobTitle = JobTitle.Text;
            }
            else
            {
                jobTitleWidget = ComboJobTitles;
                jobTitle = ComboJobTitles.SelectedItem as string;
            }

            if (string.IsNullOrWhiteSpace(FirstName.Text) || string.IsNullOrWhiteSpace(LastName.Text) || string.IsNullOrWhiteSpace(jobTitle))
            {
                if (string.IsNullOrWhiteSpace(FirstName.Text))
                {
                    FirstName.Background = new SolidColorBrush(Color.FromArgb(255, 218, 165, 32));
                }
                else
                {
                    FirstName.Background = Brushes.White;
                }

                if (string.IsNullOrWhiteSpace(LastName.Text))
                {
                    LastName.Background = new SolidColorBrush(Color.FromArgb(255, 218, 165, 32));
                }
                else
                {
                    LastName.Background = Brushes.White;
                }

                if (string.IsNullOrWhiteSpace(jobTitle))
                {
                    jobTitleWidget.Background = new SolidColorBrush(Color.FromArgb(255, 218, 165, 32));
                }
                else
                {
                    jobTitleWidget.Background = Brushes.White;
                }
            }
            else
            {
                FirstName.Background = Brushes.White;
                LastName.Background = Brushes.White;
                jobTitleWidget.Background = Brushes.White;

                object authkey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
                if (authkey is string)
                {
                    var client = new RestClient(ConfigSettings.WebApiUrl);
                    RestRequest request;
                    IRestResponse response;
                    JObject content;

                    if (string.IsNullOrWhiteSpace(EditId.Text))
                    {
                        request = new RestRequest("api/WebApi/CreateAuthor?authKey=" + (authkey as string), Method.POST);
                    }
                    else
                    {
                        request = new RestRequest("api/WebApi/UpdateAuthor?authKey=" + (authkey as string), Method.POST);
                    }
                    var requestAll = new Dictionary<string, object>
                    {
                        { "Id", EditId.Text },
                        { "FirstName", FirstName.Text },
                        { "LastName", LastName.Text },
                        { "MiddleName", MiddleName.Text }
                    };
                    if (Prefix.SelectedValue != null)
                    {
                        requestAll.Add("Prefix", (Prefix.SelectedValue as string) ?? string.Empty);
                    }
                    else
                    {
                        requestAll.Add("Prefix", "");
                    }
                    if (Sufix.SelectedValue != null)
                    {
                        requestAll.Add("Suffix", (Sufix.SelectedValue as string) ?? string.Empty);
                    }
                    else
                    {
                        requestAll.Add("Suffix", "");
                    }

                    requestAll.Add("JobTitle", GetJobTitle());
                    requestAll.Add("Credentials", Credentials.Text);
                    requestAll.Add("LawyerNumber", Lawyer.Text);
                    requestAll.Add("EmployeeId", EmployeeID.Text);
                    requestAll.Add("Initials", Initials.Text);
                    requestAll.Add("DisplayName", "");
                    requestAll.Add("Companies", (new JsonArray()).ToArray());
                    requestAll.Add("Certification", Certification.Text);
                    requestAll.Add("IsFavorite", false);

                    var adr = new Dictionary<string, object>
                    {
                        { "Id", ((string.IsNullOrWhiteSpace(AddressID.Text)) ? "0" : AddressID.Text) },
                        { "Name", "" },
                        { "Address1", Address1.Text },
                        { "Address2", Address2.Text },
                        { "City", City.Text },
                        { "Province", ProvState.Text },
                        { "PostalCode", Postal_ZipCode.Text },
                        { "Country", Country.Text }
                    };

                    var o = new JsonObject();

                    foreach (KeyValuePair<string, object> kvp in adr)
                    {
                        o.Add(kvp);
                    }

                    var Addresses = new JsonArray
                    {
                        o
                    };
                    requestAll.Add("Addresses", Addresses.ToArray());

                    var ContactInformations = new JsonArray();

                    var pho = new Dictionary<string, object>
                    {
                        { "Id", ((string.IsNullOrWhiteSpace(PhoneID.Text)) ? "0" : PhoneID.Text) },
                        { "Type", "Phone" },
                        { "Details", Phone.Text }
                    };
                    var oPho = new JsonObject();

                    foreach (KeyValuePair<string, object> kvp in pho)
                    {
                        oPho.Add(kvp);
                    }
                    ContactInformations.Add(oPho);

                    var Em = new Dictionary<string, object>
                    {
                        { "Id", ((string.IsNullOrWhiteSpace(EmailID.Text)) ? "0" : EmailID.Text) },
                        { "Type", "Email" },
                        { "Details", Email.Text }
                    };
                    var oEm = new JsonObject();

                    foreach (KeyValuePair<string, object> kvp in Em)
                    {
                        oEm.Add(kvp);
                    }
                    ContactInformations.Add(oEm);

                    var Fa = new Dictionary<string, object>
                    {
                        { "Id", ((string.IsNullOrWhiteSpace(FaxID.Text)) ? "0" : FaxID.Text) },
                        { "Type", "Fax" },
                        { "Details", Fax.Text }
                    };
                    var oFa = new JsonObject();

                    foreach (KeyValuePair<string, object> kvp in Fa)
                    {
                        oFa.Add(kvp);
                    }
                    ContactInformations.Add(oFa);

                    var Mobil = new Dictionary<string, object>
                    {
                        { "Id", ((string.IsNullOrWhiteSpace(MobileCellID.Text)) ? "0" : MobileCellID.Text) },
                        { "Type", "Mobile" },
                        { "Details", MobileCell.Text }
                    };
                    var oMobil = new JsonObject();

                    foreach (KeyValuePair<string, object> kvp in Mobil)
                    {
                        oMobil.Add(kvp);
                    }
                    ContactInformations.Add(oMobil);

                    requestAll.Add("ContactInformations", ContactInformations.ToArray());

                    var oRAll = new JsonObject();

                    foreach (KeyValuePair<string, object> kvp in requestAll)
                    {
                        oRAll.Add(kvp);
                    }
                    request.AddHeader("Content-Type", "application/json");
                    request.AddJsonBody(oRAll);
                    response = client.Execute(request);

                    content = JsonConvert.DeserializeObject<JObject>(response.Content);
                    int statusCode = content["StatusCode"].ToObject<int>();

                    if (statusCode == 200)
                    {
                        SetListOfAuthors();
                        textHasChanged = false;

                        new InfowareInformationWindow("Author has been saved.").ShowDialog();
                        //if (AuthorsList?.SelectedValue is ComboBoxItem)
                        //{
                        //     = FirstName.Text + " " + LastName.Text + ", " + GetJobTitle();
                        //}
                    }
                }
            }
        }

        private void OK_Click(object sender, RoutedEventArgs e)
        {

            if (selectedAuthor != null && !string.IsNullOrWhiteSpace(selectedAuthor.cbi.Content.ToString()) && !string.IsNullOrWhiteSpace(selectedAuthor.jToken["Id"].ToString()))
            {
                //setCurrentAuthor(selectedAuthor.Id);
                object authkey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
                if (authkey is string)
                {
                    var client = new RestClient(ConfigSettings.WebApiUrl);
                    RestRequest request;
                    IRestResponse response;
                    JObject content;

                    request = new RestRequest("api/WebApi/GetSetCurrentAuthorId?authKey=" + (authkey as string) + "&authorId=" + selectedAuthor.jToken["Id"].ToString(), Method.GET);
                    response = client.Execute(request);

                    content = JsonConvert.DeserializeObject<JObject>(response.Content);
                    int statusCode = content["StatusCode"].ToObject<int>();

                    if (statusCode == 200)
                    {
                    }
                }
            }

            SaveBtn_Click(sender, e);
            Close();
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void TextChanged(object sender, TextChangedEventArgs e)
        {
            textHasChanged = true;
        }

        private void SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            textHasChanged = true;
        }

        private string GetJobTitle()
        {
            if (UseJobTitleTextBox)
            {
                return JobTitle.Text;
            }

            return ComboJobTitles.SelectedItem as string;
        }
    }
}
