﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfowareVSTO.VSTOAuthor
{
    public class CurrentAuthorSettings
    {
        public int? LanguageId { get; set; }
        public int? CompanyId { get; set; }
        public int? LocationId { get; set; }
    }
}
