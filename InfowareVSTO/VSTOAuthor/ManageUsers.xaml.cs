﻿using InfowareVSTO.Common;
using InfowareVSTO.Common.DTOs;
using InfowareVSTO.Common.Models;
using InfowareVSTO.Common.Language;
using InfowareVSTO.Windows;
using InfowareVSTO.Windows.ParagraphNumbering.Util;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using InfowareVSTO.Common.MultiLanguage;
using Microsoft.Office.Core;

namespace InfowareVSTO.VSTOAuthor
{
    /// <summary>
    /// Interaction logic for AuthorInfo.xaml
    /// </summary>
    public partial class ManageUsers : InfowareWindow
    {
        public CurrentUsers currentUsers;

        public List<AuthorDTO> authors;
        public List<FavouriteAuthorDTO> favouriteAuthors;
        public List<ACompanyDTO> aCompanies;
        public List<AuthorAddressDTO> officeLocations;
        public List<UserAssignedRole> assignedRoles;
        public List<MLanguage> multiLanguages;

        public ManageUsersViewModel viewModel;

        public ManageUsers()
        {
            InitializeComponent();
        }

        public static CurrentAuthorSettings CurrentAuthorSettings
        {
            get
            {
                var favAuthors = AdminPanelWebApi.GetFavouriteAuthors();
                var currentUsers = ParseCurrentUsers();
                var currentAuthor = favAuthors?.FirstOrDefault(x => x.Id == currentUsers.AuthorId);

                return new CurrentAuthorSettings() { LanguageId = currentAuthor?.LanguageId, CompanyId = currentAuthor?.CurrentACompanyId , LocationId = currentAuthor?.OfficeLocationId};
            }
        }

        private void OnContentRendered(object sender, EventArgs e)
        {
            using (new LoadingWindowWrapper())
            {
                currentUsers = ParseCurrentUsers();

                authors = AdminPanelWebApi.GetAuthors();
                favouriteAuthors = AdminPanelWebApi.GetFavouriteAuthors();
                aCompanies = AdminPanelWebApi.GetCompanies();
                officeLocations = AdminPanelWebApi.GetAddresses(1);

                multiLanguages = new List<MLanguage>()
                {
                    new MLanguage() {Id = 0, Name= "- " + LanguageManager.GetTranslation(LanguageConstants.Select, "Select") + " -" }
                };
                  multiLanguages.AddRange(AdminPanelWebApi.GetMLanguages());

                assignedRoles = Enum.GetValues(typeof(UserAssignedRole)).Cast<UserAssignedRole>().ToList();

                viewModel = new ManageUsersViewModel
                {
                    Authors = ParseAuthors(authors),
                    FavouriteAuthors = ParseFavouriteAuthors(favouriteAuthors),
                    ACompanies = ParseACompanies(aCompanies),
                    OfficeLocations = ParseOfficeLocations(officeLocations),
                    AssignedRoles = ParseUserAssignedRoles(assignedRoles),
                    Languages = new ObservableCollection<MLanguage>(multiLanguages)
                };
                DataContext = viewModel;

                RefreshAuthorAndAssistantTextBlocks();
            }
        }

        private void OK_Click(object sender, RoutedEventArgs e)
        {
            SaveFavouriteAuthors(true);
            Close();
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private static CurrentUsers ParseCurrentUsers()
        {
            var currentUsers = new CurrentUsers
            {
                AuthorId = AdminPanelWebApi.GetCurrentAuthorId(),
                AssistantId = AdminPanelWebApi.GetCurrentAsistantId()
            };

            var favouriteAuthors = AdminPanelWebApi.GetFavouriteAuthors();
            if (favouriteAuthors != null && favouriteAuthors.Count > 0)
            {
                foreach(var favouriteAuthor in favouriteAuthors)
                {
                    if(favouriteAuthor.Id == currentUsers.AuthorId)
                    {
                        currentUsers.AuthorOfficeLocationId = favouriteAuthor.OfficeLocationId;
                    }
                    else if(favouriteAuthor.Id == currentUsers.AssistantId)
                    {
                        currentUsers.AssistantOfficeLocationId = favouriteAuthor.OfficeLocationId;
                    }
                }
            }
            return currentUsers;
        }

        public static List<KeyValuePair<int, string>> ParseAuthors(List<AuthorDTO> authors)
        {
            var authorList = new List<KeyValuePair<int, string>> { new KeyValuePair<int, string>(0, "- " + LanguageManager.GetTranslation(LanguageConstants.Select, "Select") + " -") };
            if (authors != null)
            {
                foreach(var author in authors)
                {
                    authorList.Add(new KeyValuePair<int, string>(author.Id, ParseAuthorFullNameAndJobTitle(author)));
                }
            }
            return authorList;
        }

        private ObservableCollection<FavouriteAuthor> ParseFavouriteAuthors(List<FavouriteAuthorDTO> favouriteAuthors)
        {
            var favouriteAuthorsList = new ObservableCollection<FavouriteAuthor>();
            if (favouriteAuthors != null)
            {
                foreach(var favouriteAuthor in favouriteAuthors)
                {
                    var author = new FavouriteAuthor
                    {
                        Id = favouriteAuthor.Id,
                        Name = (favouriteAuthor.FirstName + " " + favouriteAuthor.LastName).Trim(),
                        JobTitle = favouriteAuthor.JobTitle,
                        Company = favouriteAuthor.Company,
                        ACompanyId = favouriteAuthor.CurrentACompanyId ?? 0,
                        OfficeLocationId = favouriteAuthor.OfficeLocationId ?? 0,
                        Role = UserAssignedRole.Unnasigned,
                        LanguageID = (MsoLanguageID)(favouriteAuthor.LanguageId ?? 0),
                        CurrentlyAdded = true
                    };
                    if(favouriteAuthor.Id == currentUsers.AuthorId)
                    {
                        author.Role = UserAssignedRole.CurrentAuthor;
                    }
                    else if (favouriteAuthor.Id == currentUsers.AssistantId)
                    {
                        author.Role = UserAssignedRole.CurrentAssistant;
                    }
                    favouriteAuthorsList.Add(author);
                }
            }
            return favouriteAuthorsList;
        }

        private ObservableCollection<KeyValuePair<int, string>> ParseACompanies(List<ACompanyDTO> aCompanies)
        {
            var aCompaniesList = new ObservableCollection<KeyValuePair<int, string>> { new KeyValuePair<int, string>(0, "- " + LanguageManager.GetTranslation(LanguageConstants.Select, "Select") + " -") };
            if (aCompanies != null)
            {
                foreach(var aCompany in aCompanies)
                {
                    aCompaniesList.Add(new KeyValuePair<int, string>(aCompany.Id, aCompany.Name));
                }
            }
            return aCompaniesList;
        }

        private ObservableCollection<KeyValuePair<int, string>> ParseOfficeLocations(List<AuthorAddressDTO> officeLocations)
        {
            var officeLocationsList = new ObservableCollection<KeyValuePair<int, string>> { new KeyValuePair<int, string>(0, "- " + LanguageManager.GetTranslation(LanguageConstants.Select, "Select") + " -") };
            if (officeLocations != null)
            {
                foreach(var officeLocation in officeLocations)
                {
                    officeLocationsList.Add(new KeyValuePair<int, string>(officeLocation.Id, ParseAuthorOfficeLocation(officeLocation)));
                }
            }
            return officeLocationsList;
        }

        private ObservableCollection<KeyValuePair<UserAssignedRole, string>> ParseUserAssignedRoles(List<UserAssignedRole> userAssignedRoles)
        {
            var userAssignedRolesList = new ObservableCollection<KeyValuePair<UserAssignedRole, string>>();
            if (userAssignedRoles != null)
            {
                userAssignedRolesList.Add(new KeyValuePair<UserAssignedRole, string>(UserAssignedRole.Unnasigned, LanguageManager.GetTranslation(LanguageConstants.Unassigned, "Unassigned")));
                userAssignedRolesList.Add(new KeyValuePair<UserAssignedRole, string>(UserAssignedRole.CurrentAuthor, LanguageManager.GetTranslation(LanguageConstants.CurrentAuthor, "Current Author")));
                userAssignedRolesList.Add(new KeyValuePair<UserAssignedRole, string>(UserAssignedRole.CurrentAssistant, LanguageManager.GetTranslation(LanguageConstants.CurrentAssistant, "Current Assistant")));
            }
            return userAssignedRolesList;
        }

        public static string ParseAuthorFullName(string firstName, string lastName)
        {
            var fullName = new List<string>();
            if (!string.IsNullOrEmpty(firstName))
            {
                fullName.Add(firstName);
            }
            if (!string.IsNullOrEmpty(lastName))
            {
                fullName.Add(lastName);
            }
            return string.Join(" ", fullName.ToArray());
        }

        public static string ParseAuthorFullNameAndJobTitle(AuthorDTO author)
        {
            var authorOptions = new List<string>();
            var fullName = ParseAuthorFullName(author.FirstName, author.LastName);
            if(!string.IsNullOrEmpty(fullName))
            {
                authorOptions.Add(fullName);
            }
            if (!string.IsNullOrEmpty(author.JobTitle))
            {
                authorOptions.Add(author.JobTitle);
            }
            return string.Join(", ", authorOptions.ToArray());
        }

        public static string ParseAuthorOfficeLocation(AuthorAddressDTO officeLocation)
        {
            var authorOfficeLocation = new List<string>();
            if (!string.IsNullOrEmpty(officeLocation.Name))
            {
                authorOfficeLocation.Add(officeLocation.Name);
            }
            if (!string.IsNullOrEmpty(officeLocation.City))
            {
                authorOfficeLocation.Add(officeLocation.City);
            }
            return string.Join(" - ", authorOfficeLocation.ToArray());
        }

        private void AddUserAsFavourite_Click(object sender, RoutedEventArgs e)
        {
            var selectedAuthor = AuthorsList.SelectedValue;
            if (selectedAuthor != null && authors != null)
            {
                var selectedAuthorId = Convert.ToInt32(selectedAuthor.ToString());
                if(selectedAuthorId != 0)
                {
                    var foundAuthor = authors.Where(x => x.Id == selectedAuthorId).FirstOrDefault();
                    var foundFavouriteAuthor = viewModel.FavouriteAuthors.Where(x => x.Id == selectedAuthorId).FirstOrDefault();
                    if (foundAuthor != null && foundFavouriteAuthor == null)
                    {
                        if (AdminPanelWebApi.SetFavouriteAuthor(selectedAuthorId))
                        {
                            var favouriteAuthor = new FavouriteAuthor
                            {
                                Id = foundAuthor.Id,
                                Name = ParseAuthorFullName(foundAuthor.FirstName, foundAuthor.LastName),
                                JobTitle = foundAuthor.JobTitle
                            };
                            if(foundAuthor.Id == currentUsers.AuthorId)
                            {
                                favouriteAuthor.Role = UserAssignedRole.CurrentAuthor;
                            }
                            if (foundAuthor.Id == currentUsers.AssistantId)
                            {
                                favouriteAuthor.Role = UserAssignedRole.CurrentAssistant;
                            }

                            if (viewModel.ACompanies.Count == 2)
                            {
                                favouriteAuthor.ACompanyId = viewModel.ACompanies.Last().Key;
                            }
                            if (viewModel.Languages.Count == 2)
                            {
                                favouriteAuthor.LanguageID = viewModel.Languages.Last().Id;
                            }
                            if (viewModel.OfficeLocations.Count == 2)
                            {
                                favouriteAuthor.OfficeLocationId = viewModel.OfficeLocations.Last().Key;
                            }

                            favouriteAuthor.CurrentlyAdded = true;
                            viewModel.FavouriteAuthors.Add(favouriteAuthor);

                            currentUsers = ParseCurrentUsers();
                            RefreshAuthorAndAssistantTextBlocks();

                            new InfowareInformationWindow(message: LanguageManager.GetTranslation(LanguageConstants.UserSuccessfullyAdded, "User successfully added to the Frequent Users list!"), wordDefaultIconType: WordDefaultIconType.Information, autoclose: true).Show();
                        }
                    }
                    else
                    {
                        new InfowareInformationWindow(message: LanguageManager.GetTranslation(LanguageConstants.UserAlreadyAdded,"User already added to favourites!"), wordDefaultIconType: WordDefaultIconType.Information, autoclose: true).Show();
                    }
                    AuthorsList.SelectedIndex = 0;
                }
            }
        }

        private void RemoveUserFromFavourite_Click(object sender, RoutedEventArgs e)
        {
            var favouriteUserId = Convert.ToInt32((sender as Button).Tag);
            if (favouriteUserId != 0)
            {
                var foundFrequentUser = viewModel.FavouriteAuthors.Where(x => x.Id == favouriteUserId).FirstOrDefault();
                if(foundFrequentUser != null)
                {
                    var confirmationWindow = new InfowareConfirmationWindow(LanguageManager.GetTranslation(LanguageConstants.RemoveUserMessage,"Do you want to remove this user from the Current Users list?"));
                    confirmationWindow.ShowDialog();

                    if (confirmationWindow.Result == MessageBoxResult.OK)
                    {
                        if (AdminPanelWebApi.SetFavouriteAuthor(favouriteUserId, false))
                        {
                            if(foundFrequentUser.Role == UserAssignedRole.CurrentAssistant)
                            {
                                AdminPanelWebApi.GetSetCurrentAssistantId();
                            }
                            viewModel.FavouriteAuthors.Remove(foundFrequentUser);

                            currentUsers = ParseCurrentUsers();
                            RefreshAuthorAndAssistantTextBlocks();
                        }
                    }
                }
            }
        }

        private void AssignedRole_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (!IsLoaded)
            {
                return;
            }
            var assignedRoleComboBox = (ComboBox)sender;
            if (assignedRoleComboBox != null)
            {
                var favouriteAuthorId = int.Parse(assignedRoleComboBox.Tag.ToString());
                if(favouriteAuthorId != 0 && viewModel.FavouriteAuthors != null && viewModel.FavouriteAuthors.Count > 0)
                {
                    foreach (var favouriteAuthor in viewModel.FavouriteAuthors)
                    {
                        if(!favouriteAuthor.CurrentlyAdded)
                        {
                            if (favouriteAuthor.Id == favouriteAuthorId)
                            {
                                assignedRoleComboBox.SelectionChanged -= AssignedRole_SelectionChanged;
                                viewModel.FavouriteAuthors.Where(x => x.Id == favouriteAuthor.Id).First().HasChanged = true;
                                assignedRoleComboBox.SelectionChanged += AssignedRole_SelectionChanged;
                            }
                        }
                        else
                        {
                            assignedRoleComboBox.SelectionChanged -= AssignedRole_SelectionChanged;
                            viewModel.FavouriteAuthors.Where(x => x.Id == favouriteAuthor.Id).First().CurrentlyAdded = false;
                            assignedRoleComboBox.SelectionChanged += AssignedRole_SelectionChanged;
                        }

                        var selectedKey = (assignedRoleComboBox.SelectedItem as KeyValuePair<UserAssignedRole, string>?)?.Key;
                        if (selectedKey != null && (favouriteAuthor.Id != (int)assignedRoleComboBox.Tag) && ((UserAssignedRole)selectedKey == UserAssignedRole.CurrentAuthor || (UserAssignedRole)selectedKey == UserAssignedRole.CurrentAssistant) && ((UserAssignedRole)selectedKey == favouriteAuthor.Role))
                        {
                            assignedRoleComboBox.SelectionChanged -= AssignedRole_SelectionChanged;
                            viewModel.FavouriteAuthors.Where(x => x.Id == favouriteAuthor.Id).First().Role = UserAssignedRole.Unnasigned;
                            assignedRoleComboBox.SelectionChanged += AssignedRole_SelectionChanged;
                        }
                    }
                }
            }
        }

        private void OfficeLocation_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!IsLoaded)
            {
                return;
            }
            var officeLocationComboBox = (ComboBox)sender;
            if (officeLocationComboBox != null)
            {
                var favouriteAuthorId = int.Parse(officeLocationComboBox.Tag.ToString());
                if (favouriteAuthorId != 0 && viewModel.FavouriteAuthors != null && viewModel.FavouriteAuthors.Count > 0)
                {
                    foreach (var favouriteAuthor in viewModel.FavouriteAuthors)
                    {
                        if (!favouriteAuthor.CurrentlyAdded)
                        {
                            if (favouriteAuthor.Id == favouriteAuthorId)
                            {
                                officeLocationComboBox.SelectionChanged -= OfficeLocation_SelectionChanged;
                                viewModel.FavouriteAuthors.Where(x => x.Id == favouriteAuthor.Id).First().HasChanged = true;
                                officeLocationComboBox.SelectionChanged += OfficeLocation_SelectionChanged;
                            }
                        }
                        else
                        {
                            officeLocationComboBox.SelectionChanged -= OfficeLocation_SelectionChanged;
                            viewModel.FavouriteAuthors.Where(x => x.Id == favouriteAuthor.Id).First().CurrentlyAdded = false;
                            officeLocationComboBox.SelectionChanged += OfficeLocation_SelectionChanged;
                        }
                    }
                }
            }
        }

        private void Language_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!IsLoaded)
            {
                return;
            }

            var languageComboBox = (ComboBox)sender;
            if (languageComboBox != null)
            {
                var favouriteAuthorId = int.Parse(languageComboBox.Tag.ToString());
                if (favouriteAuthorId != 0 && viewModel.FavouriteAuthors != null && viewModel.FavouriteAuthors.Count > 0)
                {
                    foreach (var favouriteAuthor in viewModel.FavouriteAuthors)
                    {
                        if (!favouriteAuthor.CurrentlyAdded)
                        {
                            if (favouriteAuthor.Id == favouriteAuthorId)
                            {
                                languageComboBox.SelectionChanged -= Language_SelectionChanged;
                                viewModel.FavouriteAuthors.Where(x => x.Id == favouriteAuthor.Id).First().HasChanged = true;
                                languageComboBox.SelectionChanged += Language_SelectionChanged;
                            }
                        }
                        else
                        {
                            languageComboBox.SelectionChanged -= Language_SelectionChanged;
                            viewModel.FavouriteAuthors.Where(x => x.Id == favouriteAuthor.Id).First().CurrentlyAdded = false;
                            languageComboBox.SelectionChanged += Language_SelectionChanged;
                        }
                    }
                }
            }
        }

        private void UpdateUsersBtn_Click(object sender, RoutedEventArgs e)
        {
            SaveFavouriteAuthors();
        }

        private void SaveFavouriteAuthors(bool isOkButtonClicked = false)
        {
            var favouriteAuthors = viewModel.FavouriteAuthors;
            if (favouriteAuthors == null || favouriteAuthors.Where(x => x.Role == UserAssignedRole.CurrentAssistant).Count() == 0)
            {
                AdminPanelWebApi.GetSetCurrentAssistantId();
            }
            if (favouriteAuthors == null || favouriteAuthors.Where(x => x.Role == UserAssignedRole.CurrentAuthor).Count() == 0)
            {
                AdminPanelWebApi.GetSetCurrentAuthorId();
            }
            if (favouriteAuthors != null && favouriteAuthors.Count > 0)
            {
                var changedFavouriteAuthors = favouriteAuthors.Where(x => x.HasChanged == true).ToList();
                if (changedFavouriteAuthors != null && changedFavouriteAuthors.Count > 0)
                {
                    foreach (var changedFavouriteAuthor in changedFavouriteAuthors)
                    {
                        if (changedFavouriteAuthor.Role == UserAssignedRole.CurrentAuthor)
                        {
                            AdminPanelWebApi.GetSetCurrentAuthorId(changedFavouriteAuthor.Id);
                        }
                        else if (changedFavouriteAuthor.Role == UserAssignedRole.CurrentAssistant)
                        {
                            AdminPanelWebApi.GetSetCurrentAssistantId(changedFavouriteAuthor.Id);
                        }
                        AdminPanelWebApi.SetFavouriteAuthorACompany(changedFavouriteAuthor.Id, changedFavouriteAuthor.ACompanyId);
                        AdminPanelWebApi.SetFavouriteAuthorOfficeLocation(changedFavouriteAuthor.Id, changedFavouriteAuthor.OfficeLocationId);
                        AdminPanelWebApi.SetFavouriteAuthorLanguage(changedFavouriteAuthor.Id, (int)changedFavouriteAuthor.LanguageID);
                        viewModel.FavouriteAuthors.Where(x => x.Id == changedFavouriteAuthor.Id).First().HasChanged = false;
                    }
                    currentUsers = ParseCurrentUsers();
                    RefreshAuthorAndAssistantTextBlocks();
                    if (!isOkButtonClicked)
                    {
                        new InfowareInformationWindow(message: LanguageManager.GetTranslation(LanguageConstants.CurrentUsersUpdated,"Current Users list successfully updated!"), wordDefaultIconType: WordDefaultIconType.Information, autoclose: true).Show();
                    }
                }
                else
                {
                    if (!isOkButtonClicked)
                    {
                        new InfowareInformationWindow(message: LanguageManager.GetTranslation(LanguageConstants.NoChangesMadeCurrentUsers,"No changes made to the Current Users list! Nothing to update!"), wordDefaultIconType: WordDefaultIconType.Information, autoclose: true).Show();
                    }
                }
            }
        }

        private void RefreshAuthorAndAssistantTextBlocks()
        {
            CurrentAuthorName.Text = LanguageManager.GetTranslation(LanguageConstants.NoneAssigned, "None Assigned");
            CurrentAuthorOfficeLocation.Text = LanguageManager.GetTranslation(LanguageConstants.NoneAssigned, "None Assigned");
            CurrentAssistantName.Text = LanguageManager.GetTranslation(LanguageConstants.NoneAssigned, "None Assigned");
            CurrentAssistantOfficeLocation.Text = LanguageManager.GetTranslation(LanguageConstants.NoneAssigned, "None Assigned");

            if (currentUsers != null)
            {
                if (currentUsers.AuthorId != 0)
                {
                    var currentAuthor = AdminPanelWebApi.GetAuthor(currentUsers.AuthorId);
                    if (currentAuthor != null)
                    {
                        CurrentAuthorName.Text = ParseAuthorFullName(currentAuthor.FirstName, currentAuthor.LastName);
                    }
                }
                if (currentUsers.AuthorOfficeLocationId != null && currentUsers.AuthorOfficeLocationId != 0)
                {
                    var authorAddress = AdminPanelWebApi.GetAddress(currentUsers.AuthorOfficeLocationId);
                    if(authorAddress != null)
                    {
                        CurrentAuthorOfficeLocation.Text = ParseAuthorOfficeLocation(authorAddress);
                    }
                }

                if (currentUsers.AssistantId != 0)
                {
                    var currentAssistant = AdminPanelWebApi.GetAuthor(currentUsers.AssistantId);
                    if (currentAssistant != null)
                    {
                        CurrentAssistantName.Text = ParseAuthorFullName(currentAssistant.FirstName, currentAssistant.LastName);
                    }
                }
                if (currentUsers.AssistantOfficeLocationId != null && currentUsers.AssistantOfficeLocationId != 0)
                {
                    var assistantAddress = AdminPanelWebApi.GetAddress(currentUsers.AssistantOfficeLocationId);
                    if (assistantAddress != null)
                    {
                        CurrentAssistantOfficeLocation.Text = ParseAuthorOfficeLocation(assistantAddress);
                    }
                }
            }
        }

        private void Company_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!IsLoaded)
            {
                return;
            }
            var companyComboBox = (ComboBox)sender;
            if (companyComboBox != null)
            {
                var favouriteAuthorId = int.Parse(companyComboBox.Tag.ToString());
                if (favouriteAuthorId != 0 && viewModel.FavouriteAuthors != null && viewModel.FavouriteAuthors.Count > 0)
                {
                    foreach (var favouriteAuthor in viewModel.FavouriteAuthors)
                    {
                        if (!favouriteAuthor.CurrentlyAdded)
                        {
                            if (favouriteAuthor.Id == favouriteAuthorId)
                            {
                                companyComboBox.SelectionChanged -= Company_SelectionChanged;
                                viewModel.FavouriteAuthors.Where(x => x.Id == favouriteAuthor.Id).First().HasChanged = true;
                                companyComboBox.SelectionChanged += Company_SelectionChanged;
                            }
                        }
                        else
                        {
                            companyComboBox.SelectionChanged -= Company_SelectionChanged;
                            viewModel.FavouriteAuthors.Where(x => x.Id == favouriteAuthor.Id).First().CurrentlyAdded = false;
                            companyComboBox.SelectionChanged += Company_SelectionChanged;
                        }
                    }
                }
            }
        }
    }
}
