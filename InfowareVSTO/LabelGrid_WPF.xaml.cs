﻿using InfowareVSTO.Windows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace InfowareVSTO
{
	/// <summary>
	/// Interaction logic for LabelGrid_WPF.xaml
	/// </summary>
	public partial class LabelGrid_WPF : InfowareWindow
	{
		public int SelectedButton { get; set; }

		public LabelGrid_WPF()
		{
			InitializeComponent();
		}

		private void Button_Click(object sender, RoutedEventArgs e)
		{
			Button_1x1.Content = Button_1x2.Content = Button_1x3.Content = Button_1x4.Content =
				Button_2x1.Content = Button_2x2.Content = Button_2x3.Content = Button_2x4.Content =
				Button_3x1.Content = Button_3x2.Content = Button_3x3.Content = Button_3x4.Content =
				Button_4x1.Content = Button_4x2.Content = Button_4x3.Content = Button_4x4.Content =
				Button_5x1.Content = Button_5x2.Content = Button_5x3.Content = Button_5x4.Content =
				Button_6x1.Content = Button_6x2.Content = Button_6x3.Content = Button_6x4.Content =
				Button_7x1.Content = Button_7x2.Content = Button_7x3.Content = Button_7x4.Content =
				Button_8x1.Content = Button_8x2.Content = Button_8x3.Content = Button_8x4.Content =
				Button_9x1.Content = Button_9x2.Content = Button_9x3.Content = Button_9x4.Content =
				Button_10x1.Content = Button_10x2.Content = Button_10x3.Content = Button_10x4.Content = "";
			string name = ((Button)sender).Name;
			SelectedButton = Convert.ToInt32(name.Split('_')[1].Split('x')[0]) * 10 + Convert.ToInt32(name.Split('_')[1].Split('x')[1]);
			((Button)sender).Content = "X";
		    new InfowareInformationWindow(SelectedButton.ToString()).ShowDialog();
        }

		private void OkButton_Click(object sender, RoutedEventArgs e)
		{
			this.Close();
		}

		private void CancelButton_Click(object sender, RoutedEventArgs e)
		{
			this.Close();
		}		
	}
}
