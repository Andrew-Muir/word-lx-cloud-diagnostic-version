﻿using System;
using System.IO;
using InfowareVSTO.Settings.Interfaces;
using IniParser;
using IniParser.Model;

namespace InfowareVSTO.Settings
{
    internal class LocalSettingRepository : ISettingRepository
    {
        private static readonly string directoryPath;

        private static readonly string configurationFilePath;

        private static readonly FileIniDataParser fileIniDataParser;

        private static IniData iniData;

        private const string DEFAULT_SECTION_NAME = "General";

        static LocalSettingRepository()
        {
            directoryPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "Infoware");
            configurationFilePath = Path.Combine(directoryPath, "custom_settings.ini");

            fileIniDataParser = new FileIniDataParser();

            if (!Directory.Exists(directoryPath))
            {
                Directory.CreateDirectory(directoryPath);
            }

            if (!File.Exists(configurationFilePath))
            {
                File.CreateText(configurationFilePath).Dispose();
            }

            iniData = fileIniDataParser.ReadFile(configurationFilePath);
        }

        public Setting Retrieve(string settingKey)
        {
            if (File.Exists(configurationFilePath))
            {
                string settingValue = iniData[DEFAULT_SECTION_NAME][settingKey];

                if (settingValue == null)
                {
                    return null;
                }

                return new Setting(settingKey, settingValue);
            }

            return null;
        }

        public void Store(Setting setting)
        {
            iniData[DEFAULT_SECTION_NAME][setting.Key] = setting.Value;
            fileIniDataParser.WriteFile(configurationFilePath, iniData);
        }
    }
}
