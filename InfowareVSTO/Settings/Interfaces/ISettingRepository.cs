﻿namespace InfowareVSTO.Settings.Interfaces
{
    internal interface ISettingRepository
    {
        void Store(Setting setting);

        Setting Retrieve(string settingKey);
    }
}
