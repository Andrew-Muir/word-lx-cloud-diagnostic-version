﻿using InfowareVSTO.MultiLanguage;
using InfowareVSTO.Util;
using InfowareVSTO.Windows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using InfowareVSTO.Windows;

namespace InfowareVSTO
{

    public class SectionType
    {
        public string Value { get; set; }

        public int Id { get; set; }
    }

    public enum CoverPageType
    {
        MultiParty,
        Window,
        FirmCustom
    }

    public class CoverPage
    {
        public CoverPageType CoverPageType { get; set; }

        public string Title { get; set; }

        public int NrOfParties { get; set; }
    }

    /// <summary>
    /// Interaction logic for SectionsDialog.xaml
    /// </summary>
    public partial class SectionsDialog : InfowareWindow
    {
        public bool Ok { get; set; }

        public int ResultId { get; set; }

        public CoverPage CoverPage { get; set; }

        public string CustomCoverPage { get; set; }
        private byte[] MultiPartyAgreementCoverPage { get; set; }

        public SectionsDialog()
        {
            InitializeComponent();

            this.PreserveNr.IsChecked = SettingsManager.GetSettingAsBool("ContinuousPageNumbering", "Tools", true);

            CustomCoverPage = AdminPanelWebApi.GetCustomCoverPage(MLanguageUtil.ActiveDocumentLanguage);
            if (CustomCoverPage != null)
            {
                this.FirmCustom.Visibility = Visibility.Visible;
            }

            MultiPartyAgreementCoverPage = AdminPanelWebApi.GetMultiPartyAgreementCoverPage(MLanguageUtil.ActiveDocumentLanguage);
        }

        private void OkButton_Click(object sender, RoutedEventArgs e)
        {
            Ok = true;
            if (this.MainTabControl.SelectedIndex == 1)
            {
                if (this.Agreement.IsChecked == true)
                {
                    MultiPartyAgreement mpa = new MultiPartyAgreement();
                    mpa.ShowDialog();

                    if (mpa.ResultOk)
                    {
                        this.CoverPage = new CoverPage() { CoverPageType = CoverPageType.MultiParty, NrOfParties = mpa.NrOfParties, Title = mpa.ResultTitle };
                    }
                    else
                    {
                        return;
                    }
                }
                else if (this.Window.IsChecked == true)
                {
                    this.CoverPage = new CoverPage() { CoverPageType = CoverPageType.Window };
                }
                else
                {
                    this.CoverPage = new CoverPage() { CoverPageType = CoverPageType.FirmCustom };
                }

            }
            else
            {
                this.CoverPage = null;
            }

            this.Close();
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            Ok = false;
            this.Close();
        }

        private void SpecialHandlingButton_Click(object sender, RoutedEventArgs e)
        {
            SpecialSectionHandling dialog = new SpecialSectionHandling();
            dialog.ShowDialog();
        }

        public byte[] GetMultiPartyAgreementCoverPage()
        {
            return MultiPartyAgreementCoverPage;
        }
    }
}
