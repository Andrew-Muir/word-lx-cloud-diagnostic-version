﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfowareVSTO
{
    public class StyleCheckbox
    {
        public bool Value { get; set; }

        public string Name { get; set; }
        
        public bool Enabled { get; set; }
    }
}
