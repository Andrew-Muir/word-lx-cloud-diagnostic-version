﻿using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using InfowareVSTO.Addenda;
using InfowareVSTO.Common;
using InfowareVSTO.Common.General;
using InfowareVSTO.Common.Language;
using InfowareVSTO.DataSource;
using InfowareVSTO.Letterhead;
using InfowareVSTO.Managers;
using InfowareVSTO.MultiLanguage;
using InfowareVSTO.TaskPaneControls;
using InfowareVSTO.TaskPaneControls.WordDa;
using InfowareVSTO.Util;
using InfowareVSTO.VSTOAuthor;
using InfowareVSTO.Windows;
using InfowareVSTO.Windows.ParagraphNumbering;
using InfowareVSTO.Windows.ParagraphNumbering.Util;
using Microsoft.Office.Interop.Word;
using Microsoft.Office.Tools;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Windows.Controls;
using System.Windows.Forms;
using Office = Microsoft.Office.Core;
using Word = Microsoft.Office.Interop.Word;


// TODO:  Follow these steps to enable the Ribbon (XML) item:

// 1: Copy the following code block into the ThisAddin, ThisWorkbook, or ThisDocument class.

//  protected override Microsoft.Office.Core.IRibbonExtensibility CreateRibbonExtensibilityObject()
//  {
//      return new Ribbon();
//  }

// 2. Create callback methods in the "Ribbon Callbacks" region of this class to handle user
//    actions, such as clicking a button. Note: if you have exported this Ribbon from the Ribbon designer,
//    move your code from the event handlers to the callback methods and modify the code to work with the
//    Ribbon extensibility (RibbonX) programming model.

// 3. Assign attributes to the control tags in the Ribbon XML file to identify the appropriate callback methods in your code.  

// For more information, see the Ribbon XML documentation in the Visual Studio Tools for Office Help.


namespace InfowareVSTO
{
    [ComVisible(true)]
    public class Ribbon : Office.IRibbonExtensibility
    {
        /// <summary>
        /// Operation id for the features that don't have an operation id defined
        /// </summary>
        private const int PLACEHOLDER_OPERATION_ID = -1;

        private const string INFOWARE_CONTACT_URL = "https://infowaregroup.com/contact-us/";
        private const string TEMPLATE_TYPE_PROP = "LX-TEMPLATE-TYPE";
        public const string INFOWARE = "Infoware";
        private const string CUSTOM_PARAGHRAPH_STYLE_BUTTONID_PREFIX = "BtnCustomPS";
        private const string CUSTOM_INDENT_STYLE_BUTTONID_PREFIX = "BtnCustomIS";
        private const string CUSTOM_CHARACTER_STYLE_BUTTONID_PREFIX = "BtnCustomCS";
        public const string COMPANY_ID_PROP = "LX-COMPANY-ID";
        private bool? hasInformEnabled = null;


        //key is the id and value is the style name 
        private List<ICustomStyle> customParagraphStylesList = new List<ICustomStyle>();
        private List<ICustomStyle> customIndentStylesList = new List<ICustomStyle>();
        private List<ICustomStyle> customCharacterStylesList = new List<ICustomStyle>();

        private Office.IRibbonUI ribbon;

        public MultiPrintSettings MPSettings { get; set; }

        public bool OpenTemplateCalled { get; set; }

        private string hiddenButtons;

        private List<int> hiddenButtonsList;

        public List<int> HiddenButtons
        {
            get
            {
                if (hiddenButtonsList != null)
                {
                    return hiddenButtonsList;
                }

                if (hiddenButtons == null)
                {
                    hiddenButtons = AdminPanelWebApi.GetHiddenRibbonButtons();
                }

                List<int> result = new List<int>();

                if (!string.IsNullOrWhiteSpace(hiddenButtons))
                {
                    string[] strArr = hiddenButtons.Split(',');

                    foreach (string str in strArr)
                    {
                        if (int.TryParse(str.Trim(), out int intResult))
                        {
                            result.Add(intResult);
                        }
                    }
                }

                hiddenButtonsList = result;

                return result;
            }

            set
            {
                if (value == null)
                {
                    hiddenButtons = null;
                    hiddenButtonsList = null;
                }
            }
        }

        private RibbonButtons ribbonButtons;
        private RibbonButtons RibbonButtons
        {
            get
            {
                if (ribbonButtons == null)
                {
                    using (Stream stream = this.GetType().Assembly.GetManifestResourceStream("InfowareVSTO.Data.RibbonButtons.json"))
                    {
                        StreamReader sr = new StreamReader(stream);

                        ribbonButtons = JsonConvert.DeserializeObject<RibbonButtons>(sr.ReadToEnd());
                    }
                }

                return ribbonButtons;
            }
        }


        public Ribbon()
        {
        }

        private void PopulateParagraphOptions()
        {
            string customParStyles = SettingsManager.GetSetting("CustomParagraphStyles", "Styles", null, MLanguageUtil.ActiveDocumentLanguage);
            customParagraphStylesList = new List<ICustomStyle>();
            if (customParStyles == null)
            {
                customParagraphStylesList.Add(new CustomStyle("BtnPlainParagraph", "Plain", LanguageManager.GetTranslation(LanguageConstants.Plain, "Plain")));
                customParagraphStylesList.Add(new CustomStyleSeparator());
                customParagraphStylesList.Add(new CustomStyle("BtnLeftAlignedParagraph", "Left", LanguageManager.GetTranslation(LanguageConstants.LeftAligned, "Left Aligned")));
                customParagraphStylesList.Add(new CustomStyle("BtnCenteredParagraph", "Centre", LanguageManager.GetTranslation(LanguageConstants.Centered, "Centered")));
                customParagraphStylesList.Add(new CustomStyle("BtnCenteredBoldParagraph", "Centre Bold", LanguageManager.GetTranslation(LanguageConstants.CenteredBold, "Centered Bold")));
                customParagraphStylesList.Add(new CustomStyle("BtnRightAlignedParagraph", "Right", LanguageManager.GetTranslation(LanguageConstants.RightAligned, "Right Aligned")));
                customParagraphStylesList.Add(new CustomStyleSeparator());
                customParagraphStylesList.Add(new CustomStyle("BtnHeading1CenteredParagraph", "Heading1Centre", LanguageManager.GetTranslation(LanguageConstants.Heading, "Heading") + " 1 " + LanguageManager.GetTranslation(LanguageConstants.Centered, "Centered")));
                customParagraphStylesList.Add(new CustomStyle("BtnHeading1Paragraph", "Heading1", LanguageManager.GetTranslation(LanguageConstants.Heading, "Heading") + " 1"));
                customParagraphStylesList.Add(new CustomStyle("BtnHeading2Paragraph", "Heading2", LanguageManager.GetTranslation(LanguageConstants.Heading, "Heading") + " 2"));
                customParagraphStylesList.Add(new CustomStyle("BtnHeading3Paragraph", "Heading3", LanguageManager.GetTranslation(LanguageConstants.Heading, "Heading") + " 3"));
                customParagraphStylesList.Add(new CustomStyleSeparator());
                customParagraphStylesList.Add(new CustomStyle("BtnReferenceAParagraph", "Reference", LanguageManager.GetTranslation(LanguageConstants.Reference, "Reference") + " [a]"));
                customParagraphStylesList.Add(new CustomStyle("BtnQuoteParagraph", "Quotes", LanguageManager.GetTranslation(LanguageConstants.Quote, "Quote")));
            }
            else
            {
                int count = 1;
                foreach (string customStyle in customParStyles.Split(';'))
                {

                    if (customStyle.Trim() == "|")
                    {
                        customParagraphStylesList.Add(new CustomStyleSeparator());
                    }

                    string[] strArr = customStyle.Trim().Split(',');
                    if (strArr.Length >= 2)
                    {
                        customParagraphStylesList.Add(new CustomStyle(CUSTOM_PARAGHRAPH_STYLE_BUTTONID_PREFIX + count, strArr[0], strArr[1]));
                        count++;
                    }
                }
            }
        }

        private void PopulateIndentOptions()
        {
            string customIndentStyles = SettingsManager.GetSetting("CustomIndentStyles", "Styles", null, MLanguageUtil.ActiveDocumentLanguage);
            customIndentStylesList = new List<ICustomStyle>();
            if (customIndentStyles == null)
            {
                //TODO need to add correct values
                customIndentStylesList.Add(new CustomStyle("BtnFirstLineIndentDot5", "First Line Indent 0.5\"", LanguageManager.GetTranslation(LanguageConstants.FirstLineIndent, "First Line Indent") + " 0.5"));
                customIndentStylesList.Add(new CustomStyle("BtnFirstLineIndent1", "First Line Indent 1\"", LanguageManager.GetTranslation(LanguageConstants.FirstLineIndent, "First Line Indent") + " 1"));
                customIndentStylesList.Add(new CustomStyle("BtnHangingIndentDot5", "Hanging", LanguageManager.GetTranslation(LanguageConstants.HangingIndent, "Hanging Indent") + " 0.5"));
                customIndentStylesList.Add(new CustomStyleSeparator());
                customIndentStylesList.Add(new CustomStyle("BtnIndentDot5", "Indent 0.5\"", LanguageManager.GetTranslation(LanguageConstants.Indent, "Indent") + " 0.5"));
                customIndentStylesList.Add(new CustomStyle("BtnIndent1", "Indent 1\"", LanguageManager.GetTranslation(LanguageConstants.Indent, "Indent") + " 1"));
                customIndentStylesList.Add(new CustomStyle("BtnIndent1dot5", "Indent 1.5\"", LanguageManager.GetTranslation(LanguageConstants.Indent, "Indent") + " 1.5"));
                customIndentStylesList.Add(new CustomStyle("BtnIndent2", "Indent 2\"", LanguageManager.GetTranslation(LanguageConstants.Indent, "Indent") + " 2"));
                customIndentStylesList.Add(new CustomStyleSeparator());
                customIndentStylesList.Add(new CustomStyle("BtnDoubleIndentDot5", "Block 0.5\"", LanguageManager.GetTranslation(LanguageConstants.DoubleIndent, "Double Indent") + " 0.5"));
                customIndentStylesList.Add(new CustomStyle("BtnDoubleIndent1", "Block 1\"", LanguageManager.GetTranslation(LanguageConstants.DoubleIndent, "Double Indent") + " 1"));
                customIndentStylesList.Add(new CustomStyle("BtnDoubleIndent1dot5", "Block 1.5\"", LanguageManager.GetTranslation(LanguageConstants.DoubleIndent, "Double Indent") + " 1.5"));
                customIndentStylesList.Add(new CustomStyle("BtnDoubleIndent2", "Block 2\"", LanguageManager.GetTranslation(LanguageConstants.DoubleIndent, "Double Indent") + " 2"));
            }
            else
            {
                int count = 1;
                foreach (string customStyle in customIndentStyles.Split(';'))
                {

                    if (customStyle.Trim() == "|")
                    {
                        customIndentStylesList.Add(new CustomStyleSeparator());
                    }

                    string[] strArr = customStyle.Trim().Split(',');
                    if (strArr.Length >= 2)
                    {
                        customIndentStylesList.Add(new CustomStyle(CUSTOM_INDENT_STYLE_BUTTONID_PREFIX + count, strArr[0], strArr[1]));
                        count++;
                    }
                }
            }
        }

        private void PopulateCharacterOptions()
        {
            string customIndentStyles = SettingsManager.GetSetting("CustomCharacterStyles", "Styles", null, MLanguageUtil.ActiveDocumentLanguage);
            customCharacterStylesList = new List<ICustomStyle>();
            if (!string.IsNullOrWhiteSpace(customIndentStyles))
            {
                int count = 1;
                foreach (string customStyle in customIndentStyles.Split(';'))
                {

                    if (customStyle.Trim() == "|")
                    {
                        customCharacterStylesList.Add(new CustomStyleSeparator());
                    }

                    string[] strArr = customStyle.Trim().Split(',');
                    if (strArr.Length >= 2)
                    {
                        customCharacterStylesList.Add(new CustomStyle(CUSTOM_CHARACTER_STYLE_BUTTONID_PREFIX + count, strArr[0], strArr[1]));
                        count++;
                    }
                }
            }
        }

        #region IRibbonExtensibility Members

        public string GetCustomUI(string ribbonID)
        {
            return GetResourceText("InfowareVSTO.Ribbon.xml");
        }

        #endregion

        #region Ribbon Callbacks
        //Create callback methods here. For more information about adding callback methods, visit https://go.microsoft.com/fwlink/?LinkID=271226

        public void Ribbon_Load(Office.IRibbonUI ribbonUI)
        {
            //maybe put this elsewhere and save pattern to a constant
            CultureInfo ci = CultureInfo.CreateSpecificCulture(CultureInfo.CurrentCulture.Name);
            ci.DateTimeFormat.ShortDatePattern = "MM/dd/yyyy";
            System.Threading.Thread.CurrentThread.CurrentCulture = ci;
            ribbon = ribbonUI;
        }

        public void Invalidate()
        {
            try
            {
                this.hasInformEnabled = null;
                DataSourceManager.Invalidate();
                HiddenButtons = null;

                this.ribbon.Invalidate();
            }
            catch (Exception ex)
            {
                Logger.LogException(ex);
            }
        }

        public string GetContent(Office.IRibbonControl control)
        {
            if (control != null)
            {
                switch (control.Id)
                {
                    case "GalleryParagraphOptions":
                        PopulateParagraphOptions();
                        string result = "<menu xmlns=\"http://schemas.microsoft.com/office/2006/01/customui\">";
                        result += "<button id=\"BtnNormalParagraph\" getLabel=\"GetLabel\" onAction=\"OnParagraphButtonClick\" />";
                        int sepCount = 1;
                        foreach (ICustomStyle iCustomStyle in customParagraphStylesList)
                        {
                            if (iCustomStyle is CustomStyleSeparator)
                            {
                                result += $"<menuSeparator id =\"LXPSSep{sepCount}\"/>";
                                sepCount++;
                            }
                            else if (iCustomStyle is CustomStyle)
                            {
                                CustomStyle customStyle = iCustomStyle as CustomStyle;

                                result += $" <button id=\"{customStyle.ButtonID}\" getLabel=\"GetLabel\" onAction=\"OnParagraphButtonClick\" />";
                            }
                        }

                        result += @"</menu>";
                        return result;
                    case "GalleryIndentOptions":
                        PopulateIndentOptions();
                        result = "<menu xmlns=\"http://schemas.microsoft.com/office/2006/01/customui\">";
                        result += "<button id=\"BtnNoIndent\" getLabel=\"GetLabel\" onAction=\"OnIndentButtonClick\" />";
                        sepCount = 1;
                        foreach (ICustomStyle iCustomStyle in customIndentStylesList)
                        {
                            if (iCustomStyle is CustomStyleSeparator)
                            {
                                result += $"<menuSeparator id =\"LXISSep{sepCount}\"/>";
                                sepCount++;
                            }
                            else if (iCustomStyle is CustomStyle)
                            {
                                CustomStyle customStyle = iCustomStyle as CustomStyle;

                                result += $" <button id=\"{customStyle.ButtonID}\" getLabel=\"GetLabel\" onAction=\"OnIndentButtonClick\" />";
                            }
                        }

                        result += @"</menu>";
                        return result;
                    case "GalleryCharacterOptions":
                        PopulateCharacterOptions();
                        result = "<menu xmlns=\"http://schemas.microsoft.com/office/2006/01/customui\">";
                        sepCount = 1;
                        foreach (ICustomStyle iCustomStyle in customCharacterStylesList)
                        {
                            if (iCustomStyle is CustomStyleSeparator)
                            {
                                result += $"<menuSeparator id =\"LXCSSep{sepCount}\"/>";
                                sepCount++;
                            }
                            else if (iCustomStyle is CustomStyle)
                            {
                                CustomStyle customStyle = iCustomStyle as CustomStyle;

                                result += $" <button id=\"{customStyle.ButtonID}\" getLabel=\"GetLabel\" onAction=\"OnCharacterButtonClick\" />";
                            }
                        }

                        result += @"</menu>";
                        return result;
                }
            }

            return string.Empty;
        }

        public bool GetVisible(Office.IRibbonControl control)
        {
            if (!GetVisible(control.Id))
            {
                return false;
            }

            bool hasCharacterStyles = !string.IsNullOrWhiteSpace(SettingsManager.GetSetting("CustomCharacterStyles", "Styles", null, MLanguageUtil.ActiveDocumentLanguage));

            switch (control.Id)
            {
                case "StyleDialog":
                    return !hasCharacterStyles;
                case "StyleDialog2":
                case "GalleryCharacterOptions":
                    return hasCharacterStyles;
            }

            return true;
        }

        public bool GetVisible(string buttonId)
        {
            foreach (RibbonButton button in RibbonButtons.Buttons)
            {
                if (button.ButtonId == buttonId)
                {
                    return !HiddenButtons.Contains(button.Id);
                }

                if (button.SubItems != null)
                {
                    foreach (RibbonButton subButton in button.SubItems)
                    {
                        if (subButton.ButtonId == buttonId)
                        {
                            return !HiddenButtons.Contains(subButton.Id);
                        }
                    }
                }
            }

            return true;
        }

        public bool GetHasCustomRecipientBlock(Office.IRibbonControl control)
        {
            try
            {
                if (ThisAddIn.Instance.Application.Documents.Count > 0)
                {
                    string templateTypeString = DocumentPropertyUtil.ReadProperty(TEMPLATE_TYPE_PROP);
                    if (Enum.TryParse(templateTypeString, out TemplateType templateType) && templateType == TemplateType.Fax)
                    {
                        Word.Document doc = ThisAddIn.Instance.Application.ActiveDocument;

                        return Utils.GetAllContentControls(doc, "LX-TEMPLATE").Where(x => x.Title == CustomConstants.CUSTOM_RECIPIENT_BLOCK).FirstOrDefault() != null;
                    }
                }
            }
            catch(Exception ex)
            {
            }

            return false;
        }

        public bool HasDocumentOpen(Office.IRibbonControl control)
        {
            try
            {
                return ThisAddIn.Instance.Application.Documents.Count > 0;
            }
            catch
            {
                return true;
            }
        }

        public bool HasMatterMasterMatterLoaded(Office.IRibbonControl control)
        {
            return DataSourceManager.LoadedMatter != null &&
                (DataSourceManager.DataSourceType == DataSourceType.MatterMasterMSSQL || DataSourceManager.DataSourceType == DataSourceType.MatterMasterMySql);
        }

      
        public bool IsAdmin(Office.IRibbonControl control)
        {
            return AdminPanelWebApi.IsCurrentUserAdmin();
        }

        public bool HasDataSourceEnabled(Office.IRibbonControl control)
        {
            if (!GetVisible(control.Id))
            {
                return false;
            }

            return DataSourceManager.DataSourceType != DataSourceType.None;
        }

        public string GetTitle(Office.IRibbonControl control)
        {
            if (control != null)
            {
                switch (control.Id)
                {
                    case "LXStyleFontSep":
                        return LanguageManager.GetTranslation(LanguageConstants.Font, "Font");
                    case "LXStyleHeadingSep":
                        return LanguageManager.GetTranslation(LanguageConstants.RunInHeadings, "Run In Headings");
                    case "LXStyleParaSep":
                        return LanguageManager.GetTranslation(LanguageConstants.Paragraph, "Paragraph");
                    case "LXStyleCleanupSep":
                        return LanguageManager.GetTranslation(LanguageConstants.Cleanup, "Cleanup");
                    case "LXHLPSep":
                        return LanguageManager.GetTranslation(LanguageConstants.TrainingGuidesAndVideos, "Training Guides and Videos");
                    default:
                        return null;
                }
            }

            return string.Empty;
        }

        public string GetScreentip(Office.IRibbonControl control)
        {
            if (control != null)
            {
                switch (control.Id)
                {
                    // Manage group
                    case "AuthorsManagement__btn":
                        return LanguageManager.GetTranslation(LanguageConstants.Users, "Users");

                    // InFORM group
                    case "ExpandRowBtn":
                        return LanguageManager.GetTranslation(LanguageConstants.ExpandRow, "Expand Row");
                    case "AddThirdPartyBtn":
                        return LanguageManager.GetTranslation(LanguageConstants.AddThirdParty, "Add Third Party");

                    // Create group
                    case "blankDocument":
                        return LanguageManager.GetTranslation(LanguageConstants.BlankDocument, "Blank Document");
                    case "buttonTemplatesGroup":
                        return LanguageManager.GetTranslation(LanguageConstants.Templates, "Templates");
                    case "menuButtonLetterDocument":
                        return LanguageManager.GetTranslation(LanguageConstants.Letter, "Letter");
                    case "menuButtonMemoDocument":
                        return LanguageManager.GetTranslation(LanguageConstants.Memo, "Memo");
                    case "menuButtonFaxDocument":
                        return LanguageManager.GetTranslation(LanguageConstants.Fax, "Fax");
                    case "menuButtonEnvelopeDocument":
                        return LanguageManager.GetTranslation(LanguageConstants.Envelope, "Envelope");
                    case "menuButtonLabelsDocument":
                        return LanguageManager.GetTranslation(LanguageConstants.Labels, "Labels");
                    case "BtnTemplates":
                        return LanguageManager.GetTranslation(LanguageConstants.OtherTemplates, "Other Templates");
                    case "BtnAttachTemplate":
                        return LanguageManager.GetTranslation(LanguageConstants.AttachTemplate, "Attach Template");
                    case "TemplateDesigner":
                        return LanguageManager.GetTranslation(LanguageConstants.Design, "Design");

                    // Assemble group
                    case "GroupDocumentAssembly":
                        return LanguageManager.GetTranslation(LanguageConstants.Assemble, "Assemble");
                    case "BtnLibrary":
                        return LanguageManager.GetTranslation(LanguageConstants.ClauseLibrary, "Clause Library");
                    case "BtnDataFill":
                        return LanguageManager.GetTranslation(LanguageConstants.DataFill, "Data Fill");
                    case "BtnClauseBuilder":
                        return LanguageManager.GetTranslation(LanguageConstants.ClauseBuilder, "Clause Builder");

                    // Update group
                    case "Update":
                        return LanguageManager.GetTranslation(LanguageConstants.Update, "Update");
                    case "DataSource":
                        return LanguageManager.GetTranslation(LanguageConstants.DataSource, "Data Source");
                    case "UpdateData":
                        return LanguageManager.GetTranslation(LanguageConstants.RefreshData, "Refresh Data");
                    case "UpdateDate":
                        return LanguageManager.GetTranslation(LanguageConstants.UpdateDate, "Update Date");
                    case "InsertEntity":
                        return LanguageManager.GetTranslation(LanguageConstants.InsertEntity, "Insert Entity");

                    // Style and Format group
                    case "GroupStyleAndFormatting":
                        return LanguageManager.GetTranslation(LanguageConstants.StyleAndFormat, "Style and Format");
                    case "BtnParagraphNumbering":
                        return LanguageManager.GetTranslation(LanguageConstants.Numbering, "Numbering (Ctrl+Num 0)");
                    case "BtnToggleToolbar":
                        return LanguageManager.GetTranslation(LanguageConstants.ToggleToolbar, "Toggle Toolbar");
                    case "BtnAdvancedParagraphNumberingOptions":
                        return LanguageManager.GetTranslation(LanguageConstants.AdvancedOptions, "Advanced Options");
                    case "BtnAdjustParagraphNumbering":
                        return LanguageManager.GetTranslation(LanguageConstants.ParaNumOptions, "Paragraph Numbering Options");
                    case "StyleDialog2":
                        return LanguageManager.GetTranslation(LanguageConstants.Styles, "Styles");
                    case "GalleryParagraphOptions":
                        return LanguageManager.GetTranslation(LanguageConstants.Paragraph, "Paragraph");
                    case "BtnNormalParagraph":
                        string standardStyleName = Utils.GetStandardStyleName();
                        if (standardStyleName != Utils.GetLocalizedStyleNameFromBuiltInStyle(WdBuiltinStyle.wdStyleNormal))
                        {
                            return standardStyleName;
                        }
                        else
                        {
                            return LanguageManager.GetTranslation(LanguageConstants.Normal, "Normal");
                        }
                    case "BtnPlainParagraph":
                        return LanguageManager.GetTranslation(LanguageConstants.Plain, "Plain");
                    case "BtnHeading1CenteredParagraph":
                        return LanguageManager.GetTranslation(LanguageConstants.Heading, "Heading") + " 1 " + LanguageManager.GetTranslation(LanguageConstants.Centered, "Centered");
                    case "BtnHeading1Paragraph":
                        return LanguageManager.GetTranslation(LanguageConstants.Heading, "Heading") + " 1";
                    case "BtnHeading2Paragraph":
                        return LanguageManager.GetTranslation(LanguageConstants.Heading, "Heading") + " 2";
                    case "BtnHeading3Paragraph":
                        return LanguageManager.GetTranslation(LanguageConstants.Heading, "Heading") + " 3";
                    case "BtnLeftAlignedParagraph":
                        return LanguageManager.GetTranslation(LanguageConstants.LeftAligned, "Left Aligned");
                    case "BtnCenteredParagraph":
                        return LanguageManager.GetTranslation(LanguageConstants.Centered, "Centered");
                    case "BtnCenteredBoldParagraph":
                        return LanguageManager.GetTranslation(LanguageConstants.CenteredBold, "Centered Bold");
                    case "BtnRightAlignedParagraph":
                        return LanguageManager.GetTranslation(LanguageConstants.RightAligned, "Right Aligned");
                    case "BtnReferenceAParagraph":
                        return LanguageManager.GetTranslation(LanguageConstants.Reference, "Reference") + " [a]";
                    case "BtnQuoteParagraph":
                        return LanguageManager.GetTranslation(LanguageConstants.Quote, "Quote");
                    case "BtnNoIndent":
                        return LanguageManager.GetTranslation(LanguageConstants.NoIndent, "No Indent");
                    case "GalleryIndentOptions":
                        return LanguageManager.GetTranslation(LanguageConstants.Indent, "Indent");
                    case "BtnFirstLineIndentDot5":
                        return LanguageManager.GetTranslation(LanguageConstants.FirstLineIndent, "First Line Indent") + LanguageManager.TranslateNumber(" 0.5");
                    case "BtnFirstLineIndent1":
                        return LanguageManager.GetTranslation(LanguageConstants.FirstLineIndent, "First Line Indent") + " 1";
                    case "BtnHangingIndentDot5":
                        return LanguageManager.GetTranslation(LanguageConstants.HangingIndent, "Hanging Indent") + " 1";
                    case "BtnIndentDot5":
                        return LanguageManager.GetTranslation(LanguageConstants.Indent, "Indent") + LanguageManager.TranslateNumber(" 0.5");
                    case "BtnIndent1":
                        return LanguageManager.GetTranslation(LanguageConstants.Indent, "Indent") + " 1";
                    case "BtnIndent1dot5":
                        return LanguageManager.GetTranslation(LanguageConstants.Indent, "Indent") + LanguageManager.TranslateNumber(" 1.5");
                    case "BtnIndent2":
                        return LanguageManager.GetTranslation(LanguageConstants.Indent, "Indent") + " 2";
                    case "BtnDoubleIndentDot5":
                        return LanguageManager.GetTranslation(LanguageConstants.DoubleIndent, "Double Indent") + LanguageManager.TranslateNumber(" 0.5");
                    case "BtnDoubleIndent1":
                        return LanguageManager.GetTranslation(LanguageConstants.DoubleIndent, "Double Indent") + " 1";
                    case "BtnDoubleIndent1dot5":
                        return LanguageManager.GetTranslation(LanguageConstants.DoubleIndent, "Double Indent") + LanguageManager.TranslateNumber(" 1.5");
                    case "BtnDoubleIndent2":
                        return LanguageManager.GetTranslation(LanguageConstants.DoubleIndent, "Double Indent") + " 2";
                    case "GalleryCharacterOptions":
                        return LanguageManager.GetTranslation(LanguageConstants.Character, "Character");
                    case "StyleDialog":
                        return LanguageManager.GetTranslation(LanguageConstants.Styles, "Styles");
                    case "GalleryStyleToolsOptions":
                        return LanguageManager.GetTranslation(LanguageConstants.StyleTools, "Style Tools");
                    case "StyleTools":
                        return LanguageManager.GetTranslation(LanguageConstants.StyleTools, "Style Tools");
                    case "DocumentFont":
                        return LanguageManager.GetTranslation(LanguageConstants.SetDocFont, "Set Document Font");
                    case "BtnMakeSelectionParagraph":
                        return LanguageManager.GetTranslation(LanguageConstants.MakeSelectionParagraph, "Make Selection Paragraph");
                    case "BtnToggleBlockIndent":
                        return LanguageManager.GetTranslation(LanguageConstants.ToggleBlockIndent, "Toggle Block Indent");
                    case "BtnKeepTogether":
                        return LanguageManager.GetTranslation(LanguageConstants.KeepTogether, "Keep Together");
                    case "Toggle_Alignment":
                        return LanguageManager.GetTranslation(LanguageConstants.ToogleTabStop, "Toggle Right-Aligned Tab Stop");
                    case "GalleryLineSpacingOptions":
                        return LanguageManager.GetTranslation(LanguageConstants.LineSpacing, "Line Spacing");
                    case "BtnSingleSpacing":
                        return LanguageManager.GetTranslation(LanguageConstants.SingleSpacing, "Single Spacing");
                    case "Btn1dot5Spacing":
                        return "1.5 " + LanguageManager.GetTranslation(LanguageConstants.Spacing, "Spacing");
                    case "BtnDoubleSpacing":
                        return LanguageManager.GetTranslation(LanguageConstants.DoubleSpacing, "Double Spacing");
                    case "RunInHeadings":
                        return LanguageManager.GetTranslation(LanguageConstants.RunInHeadings, "Run In Headings");
                    case "BtnQuotationMarks":
                        return LanguageManager.GetTranslation(LanguageConstants.QuotationMarks, "Quotation Marks");
                    case "PasteUnformattedText":
                        return LanguageManager.GetTranslation(LanguageConstants.PasteUnformatted, "Paste as Unformatted Text (Ctrl+Alt+V)");
                    case "FormattingDialog":
                        return LanguageManager.GetTranslation(LanguageConstants.RevealFormatting, "Reveal Formatting");

                    // Insert group
                    case "Insert":
                        return LanguageManager.GetTranslation(LanguageConstants.Insert, "Insert");
                    case "sections":
                        return LanguageManager.GetTranslation(LanguageConstants.Sections, "Sections");
                    case "RemoveTrailingSectionBreak":
                        return LanguageManager.GetTranslation(LanguageConstants.RemoveTrailingSec, "Remove Trailing Section Break");
                    case "TableOfContent":
                        return LanguageManager.GetTranslation(LanguageConstants.TableOfContents, "Table of Contents");
                    case "InsertClosingAgenda":
                        return LanguageManager.GetTranslation(LanguageConstants.ClosingAgenda, "Closing Agenda");
                    case "BtnAddRecipient":
                        return LanguageManager.GetTranslation(LanguageConstants.AddRecipient, "Add Recipient");
                    case "Addenda__btn":
                        return LanguageManager.GetTranslation(LanguageConstants.Addenda, "Addenda");
                    case "AddendaMove":
                        return LanguageManager.GetTranslation(LanguageConstants.MoveAddenda, "Move Addenda");
                    case "AddendaDelete":
                        return LanguageManager.GetTranslation(LanguageConstants.DeleteAddenda, "Delete Addenda");
                    case "AddendaXRef":
                        return LanguageManager.GetTranslation(LanguageConstants.AddendaXRef, "Addenda X-Ref");
                    case "AddendaParaXRef":
                        return LanguageManager.GetTranslation(LanguageConstants.ParaXRef, "Para X Ref");
                    case "AddendaRefreshFields":
                        return LanguageManager.GetTranslation(LanguageConstants.RefreshFields, "Refresh Fields");
                    case "AddendaBookmarks":
                        return LanguageManager.GetTranslation(LanguageConstants.Bookmakrs, "Bookmarks");
                    case "AddendaOptions":
                        return LanguageManager.GetTranslation(LanguageConstants.Options, "Options");
                    case "InsertPrompt":
                        return LanguageManager.GetTranslation(LanguageConstants.InsertPromptBullet, "Insert Prompt Bullet (Ctrl+Shift+B)");
                    case "togglePromptColor":
                        return LanguageManager.GetTranslation(LanguageConstants.TogglePromptColor, "Toggle Prompt Color");
                    case "previousPrompt":
                        return LanguageManager.GetTranslation(LanguageConstants.PreviousPrompt, "Previous Prompt (Ctrl+,)");
                    case "nextPrompt":
                        return LanguageManager.GetTranslation(LanguageConstants.NextPrompt, "Next Prompt (Ctrl+.)");
                    case "GalleryDocumentParts":
                        return LanguageManager.GetTranslation(LanguageConstants.DocumentParts, "Document Parts");
                    case "BtnLetterhead":
                        return LanguageManager.GetTranslation(LanguageConstants.Letterhead, "Letterhead");
                    case "BtnInsertTable":
                        return LanguageManager.GetTranslation(LanguageConstants.Table, "Table");
                    case "BtnSigningLines":
                        return LanguageManager.GetTranslation(LanguageConstants.SigningLine, "Signing Line");
                    case "BtnCopyDocument":
                        return LanguageManager.GetTranslation(LanguageConstants.CopyDocument, "Copy Document");
                    case "BtnPasteDocument":
                        return LanguageManager.GetTranslation(LanguageConstants.PasteDocument, "Paste Document");
                    case "BtnSaveSelectionAsDocument":
                        return LanguageManager.GetTranslation(LanguageConstants.SaveSelectionAsDocument, "Save Selection as Document");
                    case "BtnCreateBasedOnCurrent":
                        return LanguageManager.GetTranslation(LanguageConstants.CreateDocumentBasedOnCurrent, "Create Document Based on Current"); //TODO change this image
                    case "BtnInsertCalendar":
                        return LanguageManager.GetTranslation(LanguageConstants.InsertCalendar, "Insert Calendar");
                    case "GalleryDocumentInfo":
                        return LanguageManager.GetTranslation(LanguageConstants.DocumentInfo, "Document Info");
                    case "BtnDocId":
                        return LanguageManager.GetTranslation(LanguageConstants.DocId, "Document ID");
                    case "BtnWatermarks":
                        return LanguageManager.GetTranslation(LanguageConstants.Watermarks, "Watermarks");
                    case "BtnPageNumbering":
                        return LanguageManager.GetTranslation(LanguageConstants.PageNumbering, "Page Numbering");
                    case "BtnHeaderFooterStamps":
                        return LanguageManager.GetTranslation(LanguageConstants.HeaderFooterStamps, "Header / Footer Stamps");
                    case "BtnDraftDateStamp":
                        return LanguageManager.GetTranslation(LanguageConstants.DraftDateStamp, "Draft Date Stamp");

                    // Print group
                    case "Print":
                        return LanguageManager.GetTranslation(LanguageConstants.Print, "Print");
                    case "MultiPrintIB":
                        return LanguageManager.GetTranslation(LanguageConstants.MultiPrint, "MultiPrint (Ctrl+P)");
                    case "MultiPrintSettings":
                        return LanguageManager.GetTranslation(LanguageConstants.PrinterSetup, "Printer setup");
                    case "MultiPrintCentralizeSettings":
                        return LanguageManager.GetTranslation(LanguageConstants.PushPaperBinSettingsToCompany, "Push Paper Bin Settings to Company");
                    case "PrintCurrentPage":
                        return LanguageManager.GetTranslation(LanguageConstants.PrintCurrentPage, "Print Current Page (Ctrl+Shift+P)");

                    // Help group
                    case "AboutInfoware":
                        return LanguageManager.GetTranslation(LanguageConstants.Help, "Help");
                    case "BtnAbout":
                        return LanguageManager.GetTranslation(LanguageConstants.AboutWordLX, "About Word LX");
                    case "BtnContact":
                        return LanguageManager.GetTranslation(LanguageConstants.ContactInfoware, "Contact Infoware Support");
                    case "BtnLanguage":
                        return LanguageManager.GetTranslation(LanguageConstants.LanguageSettings, "Language Settings");
                    case "BtnHelpGuide":
                        return LanguageManager.GetTranslation(LanguageConstants.OnlineHelpGuide, "Online Help Guide");
                    case "BtnDownloads":
                        return LanguageManager.GetTranslation(LanguageConstants.Downloads, "Downloads");
                    case "BtnVideos":
                        return LanguageManager.GetTranslation(LanguageConstants.TrainingVideos, "Training Videos");

                    // Backstage
                    case "LXNewDocsBS":
                        return LanguageManager.GetTranslation(LanguageConstants.WordLXBlankDoc, "Word LX Blank Document");
                    case "LXBlankDocsBS":
                        return LanguageManager.GetTranslation(LanguageConstants.WordLXBlankDoc, "Word LX Blank Document");
                    case "LXMultiPrintTP":
                        return LanguageManager.GetTranslation(LanguageConstants.MultiPrint, "MultiPrint");
                }
            }
            return null;
        }

        public string GetSupertip(Office.IRibbonControl control)
        {
            if (control != null)
            {
                switch (control.Id)
                {
                    // Manage group
                    case "AuthorsManagement__btn":
                        return LanguageManager.GetTranslation(LanguageConstants.UsersSupertip, "");

                    // InFORM group
                    case "ExpandRowBtn":
                        return LanguageManager.GetTranslation(LanguageConstants.ExpandRowSupertip, "");
                    case "AddThirdPartyBtn":
                        return LanguageManager.GetTranslation(LanguageConstants.AddThirdPartySupertip, "");

                    // Create group
                    case "blankDocument":
                        return LanguageManager.GetTranslation(LanguageConstants.BlankDocumentSupertip, "");
                    case "buttonTemplatesGroup":
                        return LanguageManager.GetTranslation(LanguageConstants.TemplatesSupertip, "");
                    /*
                    case "menuButtonLetterDocument":
                        return LanguageManager.GetTranslation(LanguageConstants.LetterSupertip, "");
                    case "menuButtonMemoDocument":
                        return LanguageManager.GetTranslation(LanguageConstants.MemoSupertip, "");
                    case "menuButtonFaxDocument":
                        return LanguageManager.GetTranslation(LanguageConstants.FaxSupertip, "");
                    case "menuButtonEnvelopeDocument":
                        return LanguageManager.GetTranslation(LanguageConstants.EnvelopeSupertip, "");
                    case "menuButtonLabelsDocument":
                        return LanguageManager.GetTranslation(LanguageConstants.LabelsSupertip, "");
                    case "BtnTemplates":
                        return LanguageManager.GetTranslation(LanguageConstants.OtherTemplatesSupertip, "");
					*/
                    case "BtnAttachTemplate":
                        return LanguageManager.GetTranslation(LanguageConstants.AttachTemplateSupertip, "");
                    case "TemplateDesigner":
                        return LanguageManager.GetTranslation(LanguageConstants.DesignSupertip, "");

                    // Assemble group
                    /* 
                    case "GroupDocumentAssembly":
                        return LanguageManager.GetTranslation(LanguageConstants.AssembleSupertip, "");
                    */
                    case "BtnLibrary":
                        return LanguageManager.GetTranslation(LanguageConstants.ClauseLibrarySupertip, "");
                    case "BtnDataFill":
                        return LanguageManager.GetTranslation(LanguageConstants.DataFillSupertip, "");
                    case "BtnClauseBuilder":
                        return LanguageManager.GetTranslation(LanguageConstants.ClauseBuilderSupertip, "");

                    // Update group
                    /*
                    case "Update":
                        return LanguageManager.GetTranslation(LanguageConstants.UpdateSupertip, "");
                    */
                    case "DataSource":
                        if (DataSourceManager.LoadedMatter == null)
                        {
                            return LanguageManager.GetTranslation(LanguageConstants.NoMatterLoadedSupertip, "");
                        }
                        else
                        {
                            return LanguageManager.GetTranslation(LanguageConstants.CurrentMatterSupertip, "Current Matter") + ": " + DataSourceManager.LoadedMatter.Name;
                        }
                    case "UpdateData":
                        return LanguageManager.GetTranslation(LanguageConstants.RefreshDataSupertip, "");
                    case "UpdateDate":
                        return LanguageManager.GetTranslation(LanguageConstants.UpdateDateSupertip, "");
                    case "InsertEntity":
                        return LanguageManager.GetTranslation(LanguageConstants.InsertEntitySupertip, "");

                    // Style and Format group
                    /*
                    case "GroupStyleAndFormatting":
                        return LanguageManager.GetTranslation(LanguageConstants.StyleAndFormatSupertip, "");
                    */
                    case "BtnParagraphNumbering":
                        return LanguageManager.GetTranslation(LanguageConstants.NumberingSupertip, "");
                    case "BtnToggleToolbar":
                        return LanguageManager.GetTranslation(LanguageConstants.ToggleToolbarSupertip, "");
                    case "BtnAdvancedParagraphNumberingOptions":
                        return LanguageManager.GetTranslation(LanguageConstants.AdvancedOptionsSupertip, "");
                    case "BtnAdjustParagraphNumbering":
                        return LanguageManager.GetTranslation(LanguageConstants.ParaNumOptionsSupertip, "");
                    case "StyleDialog2":
                        return LanguageManager.GetTranslation(LanguageConstants.StylesSupertip, "");
                    /*
                    case "GalleryParagraphOptions":
                        return LanguageManager.GetTranslation(LanguageConstants.ParagraphSupertip, "");
                    case "BtnNormalParagraph":
                        string standardStyleName = Utils.GetStandardStyleName();
                        if (standardStyleName != Utils.GetLocalizedStyleNameFromBuiltInStyle(WdBuiltinStyle.wdStyleNormal))
                        {
                            return standardStyleName;
                        }
                        else
                        {
                            return LanguageManager.GetTranslation(LanguageConstants.Normal, "");
                        }
                    case "BtnPlainParagraph":
                        return LanguageManager.GetTranslation(LanguageConstants.Plain, "");
                    case "BtnHeading1CenteredParagraph":
                        return LanguageManager.GetTranslation(LanguageConstants.Heading, "Heading") + " 1 " + LanguageManager.GetTranslation(LanguageConstants.Centered, "Centered");
                    case "BtnHeading1Paragraph":
                        return LanguageManager.GetTranslation(LanguageConstants.Heading, "Heading") + " 1";
                    case "BtnHeading2Paragraph":
                        return LanguageManager.GetTranslation(LanguageConstants.Heading, "Heading") + " 2";
                    case "BtnHeading3Paragraph":
                        return LanguageManager.GetTranslation(LanguageConstants.Heading, "Heading") + " 3";
                    case "BtnLeftAlignedParagraph":
                        return LanguageManager.GetTranslation(LanguageConstants.LeftAligned, "");
                    case "BtnCenteredParagraph":
                        return LanguageManager.GetTranslation(LanguageConstants.Centered, "");
                    case "BtnCenteredBoldParagraph":
                        return LanguageManager.GetTranslation(LanguageConstants.CenteredBold, "");
                    case "BtnRightAlignedParagraph":
                        return LanguageManager.GetTranslation(LanguageConstants.RightAligned, "");
                    case "BtnReferenceAParagraph":
                        return LanguageManager.GetTranslation(LanguageConstants.Reference, "Reference") + " [a]";
                    case "BtnQuoteParagraph":
                        return LanguageManager.GetTranslation(LanguageConstants.Quote, "");
                    case "BtnNoIndent":
                        return LanguageManager.GetTranslation(LanguageConstants.NoIndent, "");
                    case "GalleryIndentOptions":
                        return LanguageManager.GetTranslation(LanguageConstants.Indent, "");
                    case "BtnFirstLineIndentDot5":
                        return LanguageManager.GetTranslation(LanguageConstants.FirstLineIndent, "") + LanguageManager.TranslateNumber(" 0.5");
                    case "BtnFirstLineIndent1":
                        return LanguageManager.GetTranslation(LanguageConstants.FirstLineIndent, "") + " 1";
                    case "BtnHangingIndentDot5":
                        return LanguageManager.GetTranslation(LanguageConstants.HangingIndent, "") + " 1";
                    case "BtnIndentDot5":
                        return LanguageManager.GetTranslation(LanguageConstants.Indent, "") + LanguageManager.TranslateNumber(" 0.5");
                    case "BtnIndent1":
                        return LanguageManager.GetTranslation(LanguageConstants.Indent, "") + " 1";
                    case "BtnIndent1dot5":
                        return LanguageManager.GetTranslation(LanguageConstants.Indent, "") + LanguageManager.TranslateNumber(" 1.5");
                    case "BtnIndent2":
                        return LanguageManager.GetTranslation(LanguageConstants.Indent, "") + " 2";
                    case "BtnDoubleIndentDot5":
                        return LanguageManager.GetTranslation(LanguageConstants.DoubleIndent, "") + LanguageManager.TranslateNumber(" 0.5");
                    case "BtnDoubleIndent1":
                        return LanguageManager.GetTranslation(LanguageConstants.DoubleIndent, "") + " 1";
                    case "BtnDoubleIndent1dot5":
                        return LanguageManager.GetTranslation(LanguageConstants.DoubleIndent, "") + LanguageManager.TranslateNumber(" 1.5");
                    case "BtnDoubleIndent2":
                        return LanguageManager.GetTranslation(LanguageConstants.DoubleIndent, "") + " 2";
                    case "GalleryCharacterOptions":
                        return LanguageManager.GetTranslation(LanguageConstants.Character, "");
					*/
                    case "StyleDialog":
                        return LanguageManager.GetTranslation(LanguageConstants.StylesSupertip, "");
                    case "GalleryStyleToolsOptions":
                        return LanguageManager.GetTranslation(LanguageConstants.StyleToolsSupertip, "");
                    case "StyleTools":
                        return LanguageManager.GetTranslation(LanguageConstants.StyleToolsSupertip, "");
                    case "DocumentFont":
                        return LanguageManager.GetTranslation(LanguageConstants.SetDocFontSupertip, "");
                    case "BtnMakeSelectionParagraph":
                        return LanguageManager.GetTranslation(LanguageConstants.MakeSelectionParagraphSupertip, "");
                    case "BtnToggleBlockIndent":
                        return LanguageManager.GetTranslation(LanguageConstants.ToggleBlockIndentSupertip, "");
                    case "BtnKeepTogether":
                        return LanguageManager.GetTranslation(LanguageConstants.KeepTogetherSupertip, "");
                    case "Toggle_Alignment":
                        return LanguageManager.GetTranslation(LanguageConstants.ToogleTabStopSupertip, "");
                    case "GalleryLineSpacingOptions":
                        return LanguageManager.GetTranslation(LanguageConstants.LineSpacingSupertip, "");
                    /*
                    case "BtnSingleSpacing":
                        return LanguageManager.GetTranslation(LanguageConstants.SingleSpacing, "");
                    case "Btn1dot5Spacing":
                        return "1.5 " + LanguageManager.GetTranslation(LanguageConstants.Spacing, "");
                    case "BtnDoubleSpacing":
                        return LanguageManager.GetTranslation(LanguageConstants.DoubleSpacing, "");
					*/
                    case "RunInHeadings":
                        return LanguageManager.GetTranslation(LanguageConstants.RunInHeadingsSupertip, "");
                    case "BtnQuotationMarks":
                        return LanguageManager.GetTranslation(LanguageConstants.QuotationMarksSupertip, "");
                    /*
                    case "PasteUnformattedText":
                        return LanguageManager.GetTranslation(LanguageConstants.PasteUnformattedSupertip, "");
                    */
                    case "FormattingDialog":
                        return LanguageManager.GetTranslation(LanguageConstants.RevealFormattingSupertip, "");

                    // Insert group
                    /*
                    case "Insert":
                        return LanguageManager.GetTranslation(LanguageConstants.InsertSupertip, "");
                    */
                    case "sections":
                        return LanguageManager.GetTranslation(LanguageConstants.SectionsSupertip, "");
                    case "RemoveTrailingSectionBreak":
                        return LanguageManager.GetTranslation(LanguageConstants.RemoveTrailingSecSupertip, "");
                    case "TableOfContent":
                        return LanguageManager.GetTranslation(LanguageConstants.TableOfContentsSupertip, "");
                    /*
                    case "InsertClosingAgenda":
                        return LanguageManager.GetTranslation(LanguageConstants.ClosingAgendaSupertip, "");
                    */
                    case "BtnAddRecipient":
                        return LanguageManager.GetTranslation(LanguageConstants.AddRecipientSupertip, "");
                    case "Addenda__btn":
                        return LanguageManager.GetTranslation(LanguageConstants.AddendaSupertip, "");
                    case "AddendaMove":
                        return LanguageManager.GetTranslation(LanguageConstants.MoveAddendaSupertip, "");
                    case "AddendaDelete":
                        return LanguageManager.GetTranslation(LanguageConstants.DeleteAddendaSupertip, "");
                    case "AddendaXRef":
                        return LanguageManager.GetTranslation(LanguageConstants.AddendaXRefSupertip, "");
                    case "AddendaParaXRef":
                        return LanguageManager.GetTranslation(LanguageConstants.ParaXRefSupertip, "");
                    case "AddendaRefreshFields":
                        return LanguageManager.GetTranslation(LanguageConstants.RefreshFieldsSupertip, "");
                    case "AddendaBookmarks":
                        return LanguageManager.GetTranslation(LanguageConstants.BookmarksSupertip, "");
                    /*
                    case "AddendaOptions":
                        return LanguageManager.GetTranslation(LanguageConstants.OptionsSupertip, "");
                    */
                    case "InsertPrompt":
                        return LanguageManager.GetTranslation(LanguageConstants.InsertPromptBulletSupertip, "");
                    case "togglePromptColor":
                        return LanguageManager.GetTranslation(LanguageConstants.TogglePromptColorSupertip, "");
                    case "previousPrompt":
                        return LanguageManager.GetTranslation(LanguageConstants.PreviousPromptSupertip, "");
                    case "nextPrompt":
                        return LanguageManager.GetTranslation(LanguageConstants.NextPromptSupertip, "");
                    /*
                    case "GalleryDocumentParts":
                        return LanguageManager.GetTranslation(LanguageConstants.DocumentPartsSupertip, "");
                    */
                    case "BtnLetterhead":
                        return LanguageManager.GetTranslation(LanguageConstants.LetterheadSupertip, "");
                    case "BtnInsertTable":
                        return LanguageManager.GetTranslation(LanguageConstants.TableSupertip, "");
                    case "BtnSigningLines":
                        return LanguageManager.GetTranslation(LanguageConstants.SigningLineSupertip, "");
                    /*
                    case "BtnCopyDocument":
                        return LanguageManager.GetTranslation(LanguageConstants.CopyDocumentSupertip, "");
                    case "BtnPasteDocument":
                        return LanguageManager.GetTranslation(LanguageConstants.PasteDocumentSupertip, "");
                    */
                    case "BtnSaveSelectionAsDocument":
                        return LanguageManager.GetTranslation(LanguageConstants.SaveSelectionAsDocumentSupertip, "");
                    case "BtnCreateBasedOnCurrent":
                        return LanguageManager.GetTranslation(LanguageConstants.CreateDocumentBasedOnCurrentSupertip, "");
                    case "BtnInsertCalendar":
                        return LanguageManager.GetTranslation(LanguageConstants.InsertCalendarSupertip, "");
                    /*
                    case "GalleryDocumentInfo":
                        return LanguageManager.GetTranslation(LanguageConstants.DocumentInfoSupertip, "");
                    */
                    case "BtnDocId":
                        return LanguageManager.GetTranslation(LanguageConstants.DocIdSupertip, "");
                    case "BtnWatermarks":
                        return LanguageManager.GetTranslation(LanguageConstants.WatermarksSupertip, "");
                    case "BtnPageNumbering":
                        return LanguageManager.GetTranslation(LanguageConstants.PageNumberingSupertip, "");
                    case "BtnHeaderFooterStamps":
                        return LanguageManager.GetTranslation(LanguageConstants.HeaderFooterStampsSupertip, "");
                    case "BtnDraftDateStamp":
                        return LanguageManager.GetTranslation(LanguageConstants.DraftDateStampSupertip, "");

                    // Print group
                    /*
                    case "Print":
                        return LanguageManager.GetTranslation(LanguageConstants.PrintSupertip, "");
                    */
                    case "MultiPrintIB":
                        return LanguageManager.GetTranslation(LanguageConstants.MultiPrintSupertip, "");
                    case "MultiPrintSettings":
                        return LanguageManager.GetTranslation(LanguageConstants.PrinterSetupSupertip, "");
                    case "MultiPrintCentralizeSettings":
                        return LanguageManager.GetTranslation(LanguageConstants.PushPaperBinSettingsToCompanySupertip, "");
                    case "PrintCurrentPage":
                        return LanguageManager.GetTranslation(LanguageConstants.PrintCurrentPageSupertip, "");

                    /*
                    // Help group
                    case "AboutInfoware":
                        return LanguageManager.GetTranslation(LanguageConstants.HelpSupertip, "");
                    case "BtnAbout":
                        return LanguageManager.GetTranslation(LanguageConstants.AboutWordLXSupertip, "");
                    case "BtnContact":
                        return LanguageManager.GetTranslation(LanguageConstants.ContactInfowareSupertip, "");
                    case "BtnLanguage":
                        return LanguageManager.GetTranslation(LanguageConstants.LanguageSettingsSupertip, "");
                    case "BtnHelpGuide":
                        return LanguageManager.GetTranslation(LanguageConstants.OnlineHelpGuideSupertip, "");
                    case "BtnDownloads":
                        return LanguageManager.GetTranslation(LanguageConstants.DownloadsSupertip, "");
                    case "BtnVideos":
                        return LanguageManager.GetTranslation(LanguageConstants.TrainingVideosSupertip, "");

                    // Backstage
                    case "LXNewDocsBS":
                        return LanguageManager.GetTranslation(LanguageConstants.WordLXBlankDocSupertip, "");
                    case "LXBlankDocsBS":
                        return LanguageManager.GetTranslation(LanguageConstants.WordLXBlankDocSupertip, "");
                    case "LXMultiPrintTP":
                        return LanguageManager.GetTranslation(LanguageConstants.MultiPrintSupertip, "");
                    */
                }
            }
            return null;
        }

        public string GetLabel(Office.IRibbonControl control)
        {
            if (control != null)
            {
                if (control.Id.StartsWith(CUSTOM_PARAGHRAPH_STYLE_BUTTONID_PREFIX))
                {
                    foreach (ICustomStyle iCustomStyle in customParagraphStylesList)
                    {
                        if (iCustomStyle is CustomStyle && (iCustomStyle as CustomStyle).ButtonID == control.Id)
                        {
                            return (iCustomStyle as CustomStyle).Label;
                        }
                    }
                }
                if (control.Id.StartsWith(CUSTOM_INDENT_STYLE_BUTTONID_PREFIX))
                {
                    foreach (ICustomStyle iCustomStyle in customIndentStylesList)
                    {
                        if (iCustomStyle is CustomStyle && (iCustomStyle as CustomStyle).ButtonID == control.Id)
                        {
                            return (iCustomStyle as CustomStyle).Label;
                        }
                    }
                }
                if (control.Id.StartsWith(CUSTOM_CHARACTER_STYLE_BUTTONID_PREFIX))
                {
                    foreach (ICustomStyle iCustomStyle in customCharacterStylesList)
                    {
                        if (iCustomStyle is CustomStyle && (iCustomStyle as CustomStyle).ButtonID == control.Id)
                        {
                            return (iCustomStyle as CustomStyle).Label;
                        }
                    }
                }

                switch (control.Id)
                {
                    case "AuthorsManagement":
                        return LanguageManager.GetTranslation(LanguageConstants.Manage, "Manage");
                    case "Create":
                        return LanguageManager.GetTranslation(LanguageConstants.Create, "Create");
                    case "blankDocument":
                        return LanguageManager.GetTranslation(LanguageConstants.BlankDocument, "Blank Document");
                    case "LXNewDocsBS":
                        return LanguageManager.GetTranslation(LanguageConstants.WordLXBlankDoc, "WordLX Blank Document");
                    case "MultiPrintIB":
                        return LanguageManager.GetTranslation(LanguageConstants.MultiPrint, "MultiPrint");
                    case "MultiPrintSettings":
                        return LanguageManager.GetTranslation(LanguageConstants.PrinterSetup, "Printer setup");
                    case "PrintCurrentPage":
                        return LanguageManager.GetTranslation(LanguageConstants.PrintCurrentPage, "Print Current Page (Ctrl+Shift+P)");
                    case "MultiPrintCentralizeSettings":
                        return LanguageManager.GetTranslation(LanguageConstants.PushPaperBinSettingsToCompany, "Push paper bin settings to company");
                    case "InsertClosingAgenda":
                        return LanguageManager.GetTranslation(LanguageConstants.ClosingAgenda, "Closing Agenda");
                    case "RunInHeadings":
                        return LanguageManager.GetTranslation(LanguageConstants.RunInHeadings, "Run In Headings");
                    case "Addenda__btn":
                        return LanguageManager.GetTranslation(LanguageConstants.Addenda, "Addenda");
                    case "AddendaMove":
                        return LanguageManager.GetTranslation(LanguageConstants.MoveAddenda, "Move Addenda");
                    case "AddendaDelete":
                        return LanguageManager.GetTranslation(LanguageConstants.DeleteAddenda, "Delete Addenda");
                    case "AddendaXRef":
                        return LanguageManager.GetTranslation(LanguageConstants.AddendaXRef, "Addenda X-Ref");
                    case "AddendaRefreshFields":
                        return LanguageManager.GetTranslation(LanguageConstants.RefreshFields, "Refresh Fields");
                    case "AddendaBookmarks":
                        return LanguageManager.GetTranslation(LanguageConstants.Bookmakrs, "Bookmarks");
                    case "AddendaOptions":
                        return LanguageManager.GetTranslation(LanguageConstants.Options, "Options");
                    case "InsertPrompt":
                        return LanguageManager.GetTranslation(LanguageConstants.Prompt, "Prompt");
                    case "TableOfContent":
                        return LanguageManager.GetTranslation(LanguageConstants.TableOfContents, "Table of Contents");
                    case "sections":
                        return LanguageManager.GetTranslation(LanguageConstants.Sections, "Sections");
                    case "StyleTools":
                        return LanguageManager.GetTranslation(LanguageConstants.StyleTools, "Style Tools");
                    case "PasteUnformattedText":
                        return LanguageManager.GetTranslation(LanguageConstants.PasteUnformatted, "Paste as Unformatted Text");
                    case "Toggle_Alignment":
                        return LanguageManager.GetTranslation(LanguageConstants.ToogleTabStop, "Toggle Right-Aligned Tab Stop");
                    case "FormattingDialog":
                        return LanguageManager.GetTranslation(LanguageConstants.RevealFormatting, "Reveal Formatting");
                    case "DocumentFont":
                        return LanguageManager.GetTranslation(LanguageConstants.SetDocFont, "Set Doc Font");
                    case "RemoveTrailingSectionBreak":
                        return LanguageManager.GetTranslation(LanguageConstants.RemoveTrailingSec, "Remove Trailing Section Break");
                    case "AddendaParaXRef":
                        return LanguageManager.GetTranslation(LanguageConstants.ParaXRef, "Para X Ref");
                    case "BtnContact":
                        return LanguageManager.GetTranslation(LanguageConstants.ContactInfoware, "Contact Infoware Support");
                    case "BtnToggleToolbar":
                        return LanguageManager.GetTranslation(LanguageConstants.ToggleToolbar, "Toggle Toolbar");
                    case "BtnAdvancedParagraphNumberingOptions":
                        return LanguageManager.GetTranslation(LanguageConstants.AdvancedOptions, "Advanced Options");
                    case "BtnAdjustParagraphNumbering":
                        return LanguageManager.GetTranslation(LanguageConstants.ParaNumOptions, "Paragraph Numbering Options");
                    case "BtnParagraphNumbering":
                        return LanguageManager.GetTranslation(LanguageConstants.Numbering, "Numbering");
                    case "StylesFormatting":
                        return LanguageManager.GetTranslation(LanguageConstants.StylesAndFormat, "Styles and Format");
                    case "StyleDialog":
                        return LanguageManager.GetTranslation(LanguageConstants.Styles, "Styles");
                    case "StyleDialog2":
                        return LanguageManager.GetTranslation(LanguageConstants.Styles, "Styles");
                    case "Insert":
                        return LanguageManager.GetTranslation(LanguageConstants.Insert, "Insert");
                    case "UpdateDate":
                        return LanguageManager.GetTranslation(LanguageConstants.UpdateDate, "Update Date");
                    case "DataSource":
                        return LanguageManager.GetTranslation(LanguageConstants.DataSource, "Data Source");
                    case "Update":
                        return LanguageManager.GetTranslation(LanguageConstants.Update, "Update");
                    case "Print":
                        return LanguageManager.GetTranslation(LanguageConstants.Print, "Print");
                    case "AboutInfoware":
                        return LanguageManager.GetTranslation(LanguageConstants.Help, "Help");
                    case "BtnLanguage":
                        return LanguageManager.GetTranslation(LanguageConstants.LanguageSettings, "Language Settings");
                    case "AuthorsManagement__btn":
                        return LanguageManager.GetTranslation(LanguageConstants.Users, "Users");
                    case "LXBlankDocsBS":
                        return LanguageManager.GetTranslation(LanguageConstants.WordLXBlankDoc, "WordLX Blank Document");
                    case "buttonTemplatesGroup":
                        return LanguageManager.GetTranslation(LanguageConstants.Templates, "Templates");
                    case "BtnTemplates":
                        return LanguageManager.GetTranslation(LanguageConstants.OtherTemplates, "Other Templates");
                    case "TemplateDesigner":
                        return LanguageManager.GetTranslation(LanguageConstants.Design, "Design");
                    case "BtnAttachTemplate":
                        return LanguageManager.GetTranslation(LanguageConstants.Attach, "Attach");
                    case "LXMultiPrintTP":
                        return LanguageManager.GetTranslation(LanguageConstants.MultiPrint, "MultiPrint");
                    case "GalleryParagraphOptions":
                        return LanguageManager.GetTranslation(LanguageConstants.Paragraph, "Paragraph");
                    case "GalleryIndentOptions":
                        return LanguageManager.GetTranslation(LanguageConstants.Indent, "Indent");
                    case "GalleryCharacterOptions":
                        return LanguageManager.GetTranslation(LanguageConstants.Character, "Character");
                    case "GalleryStyleToolsOptions":
                        return LanguageManager.GetTranslation(LanguageConstants.StyleTools, "Style Tools");
                    case "GalleryLineSpacingOptions":
                        return LanguageManager.GetTranslation(LanguageConstants.LineSpacing, "Line Spacing");
                    case "GalleryDocumentParts":
                        return LanguageManager.GetTranslation(LanguageConstants.DocumentParts, "Document Parts");
                    case "GalleryDocumentInfo":
                        return LanguageManager.GetTranslation(LanguageConstants.DocumentInfo, "Document Info");
                    case "BtnInsertTable":
                        return LanguageManager.GetTranslation(LanguageConstants.Table, "Table");
                    case "BtnLetterhead":
                        return LanguageManager.GetTranslation(LanguageConstants.Letterhead, "Letterhead");
                    case "BtnMakeSelectionParagraph":
                        return LanguageManager.GetTranslation(LanguageConstants.MakeSelectionParagraph, "Make Selection Paragraph");
                    case "BtnToggleBlockIndent":
                        return LanguageManager.GetTranslation(LanguageConstants.ToggleBlockIndent, "Toggle Block Indent");
                    case "BtnKeepTogether":
                        return LanguageManager.GetTranslation(LanguageConstants.KeepTogether, "Keep Together");
                    case "BtnQuotationMarks":
                        return LanguageManager.GetTranslation(LanguageConstants.QuotationMarks, "Quotation Marks");
                    case "BtnDocId":
                        return LanguageManager.GetTranslation(LanguageConstants.DocId, "Doc Id");
                    case "BtnLibrary":
                        return LanguageManager.GetTranslation(LanguageConstants.ClauseLibrary, "Clause Library");
                    case "BtnClauseBuilder":
                        return LanguageManager.GetTranslation(LanguageConstants.ClauseBuilder, "Clause Builder");
                    case "BtnDraftDateStamp":
                        return LanguageManager.GetTranslation(LanguageConstants.DraftDateStamp, "Draft Date Stamp");
                    case "BtnDataFill":
                        return LanguageManager.GetTranslation(LanguageConstants.DataFill, "Data Fill");
                    case "UpdateData":
                        return LanguageManager.GetTranslation(LanguageConstants.RefreshData, "Refresh Data");
                    case "BtnCopyDocument":
                        return LanguageManager.GetTranslation(LanguageConstants.CopyDocument, "Copy Document");
                    case "BtnPasteDocument":
                        return LanguageManager.GetTranslation(LanguageConstants.PasteDocument, "Paste Document");
                    case "BtnSaveSelectionAsDocument":
                        return LanguageManager.GetTranslation(LanguageConstants.SaveSelectionAsDocument, "Save Selection as Document");
                    case "BtnHelpGuide":
                        return LanguageManager.GetTranslation(LanguageConstants.OnlineHelpGuide, "Online Help Guide");
                    case "menuButtonLetterDocument":
                        return LanguageManager.GetTranslation(LanguageConstants.Letter, "Letter");
                    case "menuButtonMemoDocument":
                        return LanguageManager.GetTranslation(LanguageConstants.Memo, "Memo");
                    case "menuButtonFaxDocument":
                        return LanguageManager.GetTranslation(LanguageConstants.Fax, "Fax");
                    case "menuButtonEnvelopeDocument":
                        return LanguageManager.GetTranslation(LanguageConstants.Envelope, "Envelope");
                    case "menuButtonLabelsDocument":
                        return LanguageManager.GetTranslation(LanguageConstants.Labels, "Labels");
                    case "BtnSigningLines":
                        return LanguageManager.GetTranslation(LanguageConstants.SigningLine, "Signing Line");
                    case "BtnInsertCalendar":
                        return LanguageManager.GetTranslation(LanguageConstants.InsertCalendar, "Insert Calendar");
                    case "BtnCreateBasedOnCurrent":
                        return LanguageManager.GetTranslation(LanguageConstants.CreateDocumentBasedOnCurrent, "Create Document Based on Current"); //TODO change this image
                    case "BtnWatermarks":
                        return LanguageManager.GetTranslation(LanguageConstants.Watermarks, "Watermarks");
                    case "BtnPageNumbering":
                        return LanguageManager.GetTranslation(LanguageConstants.PageNumbering, "Page Numbering");
                    case "BtnHeaderFooterStamps":
                        return LanguageManager.GetTranslation(LanguageConstants.HeaderFooterStamps, "Header / Footer Stamps");
                    case "BtnAbout":
                        return LanguageManager.GetTranslation(LanguageConstants.AboutWordLX, "About Word LX");
                    case "BtnDownloads":
                        return LanguageManager.GetTranslation(LanguageConstants.Downloads, "Downloads");
                    case "BtnVideos":
                        return LanguageManager.GetTranslation(LanguageConstants.TrainingVideos, "Training Videos");
                    case "BtnNormalParagraph":
                        string standardStyleName = Utils.GetStandardStyleName();
                        if (standardStyleName != Utils.GetLocalizedStyleNameFromBuiltInStyle(WdBuiltinStyle.wdStyleNormal))
                        {
                            return standardStyleName;
                        }
                        else
                        {
                            return LanguageManager.GetTranslation(LanguageConstants.Normal, "Normal");
                        }
                    case "BtnPlainParagraph":
                        return LanguageManager.GetTranslation(LanguageConstants.Plain, "Plain");
                    case "BtnHeading1CenteredParagraph":
                        return LanguageManager.GetTranslation(LanguageConstants.Heading, "Heading") + " 1 " + LanguageManager.GetTranslation(LanguageConstants.Centered, "Centered");
                    case "BtnHeading1Paragraph":
                        return LanguageManager.GetTranslation(LanguageConstants.Heading, "Heading") + " 1";
                    case "BtnHeading2Paragraph":
                        return LanguageManager.GetTranslation(LanguageConstants.Heading, "Heading") + " 2";
                    case "BtnHeading3Paragraph":
                        return LanguageManager.GetTranslation(LanguageConstants.Heading, "Heading") + " 3";
                    case "BtnLeftAlignedParagraph":
                        return LanguageManager.GetTranslation(LanguageConstants.LeftAligned, "Left Aligned");
                    case "BtnCenteredParagraph":
                        return LanguageManager.GetTranslation(LanguageConstants.Centered, "Centered");
                    case "BtnCenteredBoldParagraph":
                        return LanguageManager.GetTranslation(LanguageConstants.CenteredBold, "Centered Bold");
                    case "BtnRightAlignedParagraph":
                        return LanguageManager.GetTranslation(LanguageConstants.RightAligned, "Right Aligned");
                    case "BtnReferenceAParagraph":
                        return LanguageManager.GetTranslation(LanguageConstants.Reference, "Reference") + " [a]";
                    case "BtnQuoteParagraph":
                        return LanguageManager.GetTranslation(LanguageConstants.Quote, "Quote");
                    case "BtnNoIndent":
                        return LanguageManager.GetTranslation(LanguageConstants.NoIndent, "No Indent");
                    case "BtnFirstLineIndentDot5":
                        return LanguageManager.GetTranslation(LanguageConstants.FirstLineIndent, "First Line Indent") + LanguageManager.TranslateNumber(" 0.5");
                    case "BtnFirstLineIndent1":
                        return LanguageManager.GetTranslation(LanguageConstants.FirstLineIndent, "First Line Indent") + " 1";
                    case "BtnHangingIndentDot5":
                        return LanguageManager.GetTranslation(LanguageConstants.HangingIndent, "Hanging Indent") + " 1";
                    case "BtnIndentDot5":
                        return LanguageManager.GetTranslation(LanguageConstants.Indent, "Indent") + LanguageManager.TranslateNumber(" 0.5");
                    case "BtnIndent1":
                        return LanguageManager.GetTranslation(LanguageConstants.Indent, "Indent") + " 1";
                    case "BtnIndent1dot5":
                        return LanguageManager.GetTranslation(LanguageConstants.Indent, "Indent") + LanguageManager.TranslateNumber(" 1.5");
                    case "BtnIndent2":
                        return LanguageManager.GetTranslation(LanguageConstants.Indent, "Indent") + " 2";
                    case "BtnDoubleIndentDot5":
                        return LanguageManager.GetTranslation(LanguageConstants.DoubleIndent, "Double Indent") + LanguageManager.TranslateNumber(" 0.5");
                    case "BtnDoubleIndent1":
                        return LanguageManager.GetTranslation(LanguageConstants.DoubleIndent, "Double Indent") + " 1";
                    case "BtnDoubleIndent1dot5":
                        return LanguageManager.GetTranslation(LanguageConstants.DoubleIndent, "Double Indent") + LanguageManager.TranslateNumber(" 1.5");
                    case "BtnDoubleIndent2":
                        return LanguageManager.GetTranslation(LanguageConstants.DoubleIndent, "Double Indent") + " 2";
                    case "GroupDocumentAssembly":
                        return LanguageManager.GetTranslation(LanguageConstants.Assemble, "Assemble");
                    case "GroupStyleAndFormatting":
                        return LanguageManager.GetTranslation(LanguageConstants.StyleAndFormat, "Style and Format");
                    case "BtnSingleSpacing":
                        return LanguageManager.GetTranslation(LanguageConstants.SingleSpacing, "Single Spacing");
                    case "Btn1dot5Spacing":
                        return "1.5 " + LanguageManager.GetTranslation(LanguageConstants.Spacing, "Spacing");
                    case "BtnDoubleSpacing":
                        return LanguageManager.GetTranslation(LanguageConstants.DoubleSpacing, "Double Spacing");
                    case "BtnInsertContacts":
                        return LanguageManager.GetTranslation(LanguageConstants.Contacts, "Contacts");
                    case "BtnAddRecipient":
                        return LanguageManager.GetTranslation(LanguageConstants.AddRecipient, "Add Recipient");
                    case "InsertEntity":
                        return LanguageManager.GetTranslation(LanguageConstants.InsertEntity, "Insert Entity");
                    case "ExpandRowBtn":
                        return LanguageManager.GetTranslation(LanguageConstants.ExpandRow, "Expand row");
                    case "AddThirdPartyBtn":
                        return LanguageManager.GetTranslation(LanguageConstants.AddThirdParty, "Add Third Party");
                }
            }
            return null;
        }

        public Bitmap GetImage(Office.IRibbonControl control)
        {
            switch (control.Id)
            {
                case "AuthorsManagement__btn":
                case "ManageAuthors":
                    return Properties.Resources.AUTHORS;
                case "BtnCompanies":
                    return Properties.Resources.COMPANIES___32_x_32;
                case "blankDocument":
                case "LXBlankDocsBS":
                    return Properties.Resources.NEWDOCUMENT32x32;
                case "buttonTemplatesGroup":
                    return Properties.Resources.LETTER32x32;
                case "BtnTemplates":
                    return Properties.Resources.TEMPLATES_16P_X_16P;
                case "TemplateDesigner":
                    return Properties.Resources.TEMPLATEDESIGNER;
                case "BtnAttachTemplate":
                    return Properties.Resources.ATTACHTEMPLATE;
                case "LXNewDocsBS":
                    return Properties.Resources.NEWDOCUMENT32x32;
                case "MultiPrintIB":
                    return Properties.Resources.MULTIPRINT;
                case "LXMultiPrintTP":
                    return Properties.Resources.MULTIPRINT;
                case "InsertClosingAgenda":
                    return Properties.Resources.CLOSING_AGENDA_16P_X_16P;
                case "RunInHeadings":
                    return Properties.Resources.RUN_IN_HEADINGS_16P_X_16P;
                case "sections":
                    return Properties.Resources.SECTIONS32x32;
                case "Addenda__btn":
                    return Properties.Resources.ADDENDA;
                case "AddendaMove":
                    return Properties.Resources.MOVE_ADDENDA_16P_X_16P;
                case "AddendaDelete":
                    return Properties.Resources.DELETE_ADDENDA_16P_X_16P;
                case "AddendaXRef":
                    return Properties.Resources.INSERT_ADDENDA_CROSS_REFERENCE_16P_X_16P;
                case "AddendaRefreshFields":
                    return Properties.Resources.REFRESH_FIELDS_16P_X_16P;
                case "AddendaBookmarks":
                    return Properties.Resources.BOOKMARK_16P_X_16P;
                case "AddendaOptions":
                    return Properties.Resources.OPTIONS_16P_X_16P;
                case "InsertPrompt":
                    return Properties.Resources.PROMPT32x32;
                case "TableOfContent":
                    return Properties.Resources.TABLE_OF_CONTENTS_16P_X_16P;
                case "togglePromptColor":
                    return Properties.Resources.CHANGEPROMPTCOLOUR16x16;
                case "previousPrompt":
                    return Properties.Resources.PROMPTARROWLEFT16x16;
                case "nextPrompt":
                    return Properties.Resources.PROMPTARROWRIGHT16x16;
                case "GalleryParagraphOptions":
                    return Properties.Resources.PARAGRAPH___16_X_16;
                case "GalleryIndentOptions":
                    return Properties.Resources.INDENT;
                case "GalleryStyleToolsOptions":
                    return Properties.Resources.STYLETOOLS32x32;
                case "GalleryLineSpacingOptions":
                    return Properties.Resources.LINE_SPACING_16P_X_16P;
                case "GalleryDocumentParts":
                    return Properties.Resources.DOCUMENTPARTS;
                case "GalleryDocumentInfo":
                    return Properties.Resources.DOCUMENTINFO32x32;
                case "BtnInsertTable":
                    return Properties.Resources.TABLE_16P_X_16P;
                case "BtnParagraphNumbering":
                    return Properties.Resources.LIST_NUMBERING___thinner___32_X_32;
                case "BtnLetterhead":
                    return Properties.Resources.LETTERHEAD_16P_X_16P;
                case "BtnMakeSelectionParagraph":
                    return Properties.Resources.MAKE_SELECTION_A_PARAGRAPH_16P_X_16P;
                case "BtnToggleBlockIndent":
                    return Properties.Resources.TOGGLE_BLOCK_INDENT_16P_X_16P;
                case "BtnKeepTogether":
                    return Properties.Resources.SET_KEEP_TOGETHER_16P_X_16P;
                /*case "BtnToggleAlignment":
                    return Properties.Resources.alignment32;*/
                case "BtnQuotationMarks":
                    return Properties.Resources.QUOTATION_MARKS_16P_X_16P;
                case "BtnDocId":
                    return Properties.Resources.DOC_ID_16P_X_16P;
                case "BtnLibrary":
                    return Properties.Resources.CLAUSELIBRARY16x16;
                case "BtnClauseBuilder":
                    return Properties.Resources.CLAUSEBUILDER16x16;
                case "BtnDraftDateStamp":
                    return Properties.Resources.DRAFT_DATE_STAMP_16P_X_16P;
                case "BtnDataFill":
                    return Properties.Resources.DATAFILL16x16;

                case "StyleTools":
                    return Properties.Resources.STYLETOOLS32x32;
                case "AboutInfowareGallery":
                    return Properties.Resources.Infoware_i_32x32_whitebg;
                case "CurrentAuthor":
                case "CurrentAssistant":
                    return Properties.Resources.currentAuthor32;
                case "UpdateData":
                    return Properties.Resources.DOCUMENTDATA16x16;
                case "UpdateDate":
                    return Properties.Resources.DOCUMENTDATE;
                case "Toggle_Alignment":
                    return Properties.Resources.TOGGLE_RIGHT_ALIGNED_TAB_STOP_16P_X_16P;
                case "AddendaParaXRef":
                    return Properties.Resources.INSERT_PARA_CROSS_REFERENCE_16P_X_16P;
                case "BtnCopyDocument":
                    return Properties.Resources.COPY_DOCUMENT_16P_X_16P;
                case "BtnPasteDocument":
                    return Properties.Resources.PASTE_DOCUMENT_16P_X_16P;
                case "BtnSaveSelectionAsDocument":
                    return Properties.Resources.SAVE_SELECTION_AS_DOCUMENT_16P_X_16P;
                case "BtnContact":
                    return Properties.Resources.CONTACT_INFOWARE_16P_X_16P;
                case "menuButtonLetterDocument":
                    return Properties.Resources.LETTER_16P_X_16P;
                case "menuButtonMemoDocument":
                    return Properties.Resources.MEMO_16P_X_16P;
                case "menuButtonFaxDocument":
                    return Properties.Resources.FAX_16P_X_16P;
                case "menuButtonEnvelopeDocument":
                    return Properties.Resources.ENVELOPE_16P_X_16P;
                case "menuButtonLabelsDocument":
                    return Properties.Resources.LABEL_16P_X_16P;
                case "BtnToggleToolbar":
                    return Properties.Resources.TOGGLE_TOOLBAR_16P_X_16P;
                case "BtnAdvancedParagraphNumberingOptions":
                    return Properties.Resources.ADVANCED_OPTIONS_16P_X_16P;
                case "BtnAdjustParagraphNumbering":
                    return Properties.Resources.PARAGRAPH_NUMBER_OPTIONS_16P_X_16P;
                case "DocumentFont":
                    return Properties.Resources.DOCUMENT_FONT_16P_X_16P;
                case "PasteUnformattedText":
                    return Properties.Resources.PASTE_UNFORTMATTED_16P_X_16P;
                case "FormattingDialog":
                    return Properties.Resources.REVEAL_FORMATTING_16P_X_16P;
                case "RemoveTrailingSectionBreak":
                    return Properties.Resources.REMOVE_EXTRA_TRAILING_SECTION_BREAK_16P_X_16P;
                case "BtnSigningLines":
                    return Properties.Resources.SIGNING_16P_X_16P;
                case "BtnInsertCalendar":
                    return Properties.Resources.CALENDAR_16P_X_16P;
                case "BtnCreateBasedOnCurrent":
                    return Properties.Resources.COPY_DOCUMENT_16P_X_16P; //TODO change this image
                case "BtnWatermarks":
                    return Properties.Resources.WATERMARK_16P_X_16P;
                case "BtnPageNumbering":
                    return Properties.Resources.PAGE_NUMBER_16P_X_16P;
                case "BtnHeaderFooterStamps":
                    return Properties.Resources.HEADER_FOOTER_SHAPES_16P_X_16P;
                case "BtnAbout":
                    return Properties.Resources.ABOUT_16p_x_16p_01;
                case "BtnHelpGuide":
                    return Properties.Resources.ONLINE_HELP_16p_x_16p_01;
                case "BtnDownloads":
                    return Properties.Resources.DOWNLOADS_16p_x_16p_01;
                case "BtnVideos":
                    return Properties.Resources.TRAINING_VIDEOS_16p_x_16p_01;
                case "DataSource":
                    if (DataSourceManager.LoadedMatter == null)
                    {
                        return Properties.Resources.DataSource;
                    }
                    else
                    {
                        return Properties.Resources.DataSourceFilled;
                    }
            }
            return null;
        }

        #region Style and formatting callbacks

        public void OnParagraphButtonClick(Office.IRibbonControl control)
        {
            int? actionID = Login.AssureLogin(11);
            if (actionID == null)
            {
                return;
            }

            Range selectionRange = Globals.ThisAddIn.Application.Selection.Range;

            //selectionRange.ParagraphFormat.set_Style(WdBuiltinStyle.wdStyleNormal);

            UndoRecord undoRecord = Globals.ThisAddIn.Application.UndoRecord;
            undoRecord.StartCustomRecord("Paragraph formatting");

            switch (control.Id)
            {
                case "BtnNormalParagraph":
                    RangeUtil.SetSelectionStyle(Utils.GetStandardStyleName());
                    break;
                default:
                    foreach (ICustomStyle iCustomStyle in customParagraphStylesList)
                    {
                        if (iCustomStyle is CustomStyle && (iCustomStyle as CustomStyle).ButtonID == control.Id)
                        {
                            RangeUtil.SetSelectionStyle((iCustomStyle as CustomStyle).StyleName);
                        }
                    }

                    break;
            }

            undoRecord.EndCustomRecord();
        }

        public void OnParagraphNumberingButtonClick(Office.IRibbonControl control)
        {
            int? actionID = Login.AssureLogin(17);
            if (actionID == null)
            {
                return;
            }

            ShowParagraphNumberingMainWindow();
        }


        public void OnToggleToolbarButtonClick(Office.IRibbonControl control)
        {
            int? actionID = Login.AssureLogin(17);
            if (actionID == null)
            {
                return;
            }

            ParagraphNumberingToolbarManager.Instance.ToggleVisibility();
        }

        public void OnAdvancedParagraphNumberingOptionsButtonClick(Office.IRibbonControl control)
        {
            int? actionID = Login.AssureLogin(17);
            if (actionID == null)
            {
                return;
            }

            if (ListTemplateUtil.IsListTemplateUsedInSelection())
            {
                ShowParagraphNumberingMainWindow(skipToAdvancedOptionsWindow: true);
            }
            else
            {
                ParagraphNumberingToolbarManager.Instance.EnsureWindowIsHidden();
                var confirmDialog = new InfowareConfirmationWindow(LanguageManager.GetTranslation(LanguageConstants.NoListsInCurrentSelection, "There are no Lists in the current selection.\nChoose the OK button to select a new Numbering scheme to insert.\nChoose the CANCEL button and navigate to a numbered paragraph."));
                confirmDialog.ShowDialog();
                ParagraphNumberingToolbarManager.Instance.RestoreWindowIfNecessary();
                if (confirmDialog.Result == System.Windows.MessageBoxResult.OK)
                {
                    ShowParagraphNumberingMainWindow();
                }
                else
                {
                    return;
                }
            }
        }

        public void OnAdjustParagraphNumberingButtonClick(Office.IRibbonControl control)
        {
            int? actionID = Login.AssureLogin(17);
            if (actionID == null)
            {
                return;
            }

            ParagraphNumberingToolbarManager.Instance.EnsureWindowIsHidden();
            new ParagraphNumberingAdjustmentWindow().ShowDialog();
            ParagraphNumberingToolbarManager.Instance.RestoreWindowIfNecessary();
        }

        public void ShowParagraphNumberingMainWindow(bool skipToAdvancedOptionsWindow = false)
        {
            //ParagraphNumberingToolbarManager.Instance.EnsureWindowIsHidden();
            new ParagraphNumberingMainWindow(skipToAdvancedOptionsWindow: skipToAdvancedOptionsWindow).ShowDialog();
            //ParagraphNumberingToolbarManager.Instance.RestoreWindowIfNecessary();
        }

        // Style Tools - {START}

        public void OnStyleToolsButtonClick(Office.IRibbonControl control)
        {
            int? actionID = Login.AssureLogin(13);
            if (actionID == null)
            {
                return;
            }

            Range selectionRange = Globals.ThisAddIn.Application.Selection.Range;

            /**
             * Run all actions in a "transaction" so that a single entry is created in Word's undo stack
             * https://docs.microsoft.com/en-us/office/vba/word/concepts/working-with-word/working-with-the-undorecord-object 
             */
            UndoRecord undoRecord = Globals.ThisAddIn.Application.UndoRecord;
            undoRecord.StartCustomRecord("Indent");

            switch (control.Id)
            {
                case "BtnToggleBlockIndent":
                    var blockIndentStyleName = SettingsManager.GetSetting("BlockIndentStyle", "Tools");
                    if (blockIndentStyleName != null)
                    {
                        AlternateNormalStyleWithBlockStyle(selectionRange, blockIndentStyleName);
                    }
                    else
                    {
                        blockIndentStyleName = "Block 1\"";
                        AlternateNormalStyleWithBlockStyle(selectionRange, blockIndentStyleName);
                    }
                    break;
                case "BtnKeepTogether":
                    // need F
                    var keepTogether = new ToolsForStyles.KeepTogether();
                    keepTogether.Show();
                    break;
                case "BtnToggleAlignment":
                    if (selectionRange.ParagraphFormat.Alignment == WdParagraphAlignment.wdAlignParagraphLeft)
                    {
                        selectionRange.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphRight;
                    }
                    else
                    {
                        selectionRange.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphLeft;
                    }
                    break;
            }

            undoRecord.EndCustomRecord();

            if (control.Id == "BtnMakeSelectionParagraph")
            {
                // transform to paragraph
                object o = Missing.Value;
                if (selectionRange.Text != null)
                {
                    selectionRange.MoveEnd(WdUnits.wdCharacter, -1);
                    selectionRange.Find.Execute("\r", ref o, ref o, ref o, ref o, ref o, ref o, ref o, ref o, "\v", Word.WdReplace.wdReplaceAll, ref o, ref o, ref o, ref o);
                }
            }

            if (control.Id == "BtnQuotationMarks")
            { 
                ParagraphNumberingToolbarManager.Instance.EnsureWindowIsHidden();
                var menu = new ToolsForStyles.QuotationMarks();
                menu.ShowDialog();
                ParagraphNumberingToolbarManager.Instance.RestoreWindowIfNecessary();
            }

        }

        private void AlternateNormalStyleWithBlockStyle(Range selectionRange, string blockIndentStyleName)
        {
            if (selectionRange != null)
            {
                List<string> styles = blockIndentStyleName.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(x => x.Trim()).Where(x => ListTemplateUtil.CheckIfStyleExistsInDocument(x)).ToList();

                if (styles.Count > 0)
                {
                    var paragraphStyleName = (selectionRange.Paragraphs[1].get_Style() as Word.Style)?.NameLocal;
                    if (!string.IsNullOrEmpty(paragraphStyleName))
                    {
                        try
                        {
                            var paragraphStyleNameWithoutAlias = ListTemplateUtil.GetStyleNameWithoutAlias(paragraphStyleName);
                            if (styles.Contains(paragraphStyleNameWithoutAlias))
                            {
                                if (styles.Last() == paragraphStyleNameWithoutAlias)
                                {
                                    selectionRange.Paragraphs.set_Style(Utils.GetStandardStyleName());
                                }
                                else
                                {
                                    selectionRange.Paragraphs.set_Style(styles[styles.LastIndexOf(paragraphStyleNameWithoutAlias) + 1]);
                                }
                            }
                            else
                            {
                                selectionRange.Paragraphs.set_Style(styles[0]);
                            }
                        }
                        catch (Exception ex)
                        {
                            Debug.WriteLine(ex);
                        }
                    }
                }
            }
        }

        // Style Tools - {END}
        // Style Tools - {START}

        public void TemplateDesigner_Click(Office.IRibbonControl control)
        {
            int? actionID = Login.AssureLogin(13);
            if (actionID == null)
            {
                return;
            }

            CustomTaskPane taskPane = TaskPaneUtil2.CreateCustomTaskPane(INFOWARE, new TemplateDesigner());
            taskPane.Width = 350;
            taskPane.Visible = true;
        }

        public void OnIndentButtonClick(Office.IRibbonControl control)
        {
            int? actionID = Login.AssureLogin(13);
            if (actionID == null)
            {
                return;
            }

            Range selectionRange = Globals.ThisAddIn.Application.Selection.Range;

            /**
             * Run all actions in a "transaction" so that a single entry is created in Word's undo stack
             * https://docs.microsoft.com/en-us/office/vba/word/concepts/working-with-word/working-with-the-undorecord-object 
             */
            UndoRecord undoRecord = Globals.ThisAddIn.Application.UndoRecord;
            undoRecord.StartCustomRecord("Indent");

            DocumentStyleManager styleManager = DocumentStyleManager.Instance;

            switch (control.Id)
            {
                case "BtnNoIndent":
                    //selectionRange.ParagraphFormat.LeftIndent = 0;
                    //selectionRange.ParagraphFormat.RightIndent = 0;
                    //selectionRange.ParagraphFormat.FirstLineIndent = 0;
                    RangeUtil.SetSelectionStyle(Utils.GetStandardStyleName());
                    break;
                default:
                    foreach (ICustomStyle iCustomStyle in customIndentStylesList)
                    {
                        if (iCustomStyle is CustomStyle && (iCustomStyle as CustomStyle).ButtonID == control.Id)
                        {
                            RangeUtil.SetSelectionStyle((iCustomStyle as CustomStyle).StyleName);
                        }
                    }
                    break;
            }

            undoRecord.EndCustomRecord();
        }
        public void OnCharacterButtonClick(Office.IRibbonControl control)
        {
            int? actionID = Login.AssureLogin(13);
            if (actionID == null)
            {
                return;
            }

            try
            {
                Range selectionRange = Globals.ThisAddIn.Application.Selection.Range;

                /**
                 * Run all actions in a "transaction" so that a single entry is created in Word's undo stack
                 * https://docs.microsoft.com/en-us/office/vba/word/concepts/working-with-word/working-with-the-undorecord-object 
                 */
                UndoRecord undoRecord = Globals.ThisAddIn.Application.UndoRecord;
                undoRecord.StartCustomRecord("Character");

                DocumentStyleManager styleManager = DocumentStyleManager.Instance;

                foreach (ICustomStyle iCustomStyle in customCharacterStylesList)
                {
                    if (iCustomStyle is CustomStyle && (iCustomStyle as CustomStyle).ButtonID == control.Id)
                    {
                        RangeUtil.SetSelectionStyle((iCustomStyle as CustomStyle).StyleName, true);
                    }
                }

                undoRecord.EndCustomRecord();
            }
            catch { }
        }

        public void OnLineSpacingButtonClick(Office.IRibbonControl control)
        {
            int? actionID = Login.AssureLogin(15);
            if (actionID == null)
            {
                return;
            }

            WdLineSpacing lineSpacingOption;

            switch (control.Id)
            {
                case "BtnSingleSpacing":
                    lineSpacingOption = WdLineSpacing.wdLineSpaceSingle;
                    break;
                case "Btn1dot5Spacing":
                    lineSpacingOption = WdLineSpacing.wdLineSpace1pt5;
                    break;
                case "BtnDoubleSpacing":
                    lineSpacingOption = WdLineSpacing.wdLineSpaceDouble;
                    break;
                default:
                    throw new InvalidOperationException($"Control with id {control.Id} cannot be handled");
            }

            using (new DocumentUndoTransaction("Paragraph line spacing"))
            {
                Paragraphs selectedParagraphs = null;

                Selection selection = DocumentUtil.GetSelection();
                if (!string.IsNullOrEmpty(selection?.Range?.Text) && selection.Paragraphs.Count > 0)
                {
                    selectedParagraphs = selection.Paragraphs;
                }

                LineSpacingUtil.ApplyLineSpacing(selectedParagraphs, lineSpacingOption);
            }
        }

        public void OnPageNumberingButtonClick(Office.IRibbonControl control)
        {
            int? actionID = Login.AssureLogin(20);
            if (actionID == null)
            {
                return;
            }

            new InfowarePageNumberingWindow().ShowDialog();
            ThisAddIn.Instance.UnwantedUnfocus = false;
        }

        #endregion

        #region Insert callbacks

        public void OnDocumentPartsButtonClick(Office.IRibbonControl control)
        {
            switch (control.Id)
            {
                case "BtnLetterhead":
                    ToggleLetterhead();
                    return;
                case "BtnInsertTable":
                    ShowTableInsertionDialog();
                    return;
                case "BtnSigningLines":
                    ShowSigningLinesTaskPane();
                    return;
                case "BtnCopyDocument":
                    CopyWholeDocumentToClipboard();
                    return;
                case "BtnPasteDocument":
                    ShowDocumentPastingWindow();
                    return;
                case "BtnSaveSelectionAsDocument":
                    OpenNewDocumentWithSelectedText();
                    return;
                case "BtnWatermarks":
                    OpenWatermarksWindow();
                    return;
                case "BtnInsertCalendar":
                    ShowCalendarInsertWindow();
                    return;
                case "BtnCreateBasedOnCurrent":
                    CreateDocumentBasedOnCurrent();
                    return;
            }
        }

        private void CreateDocumentBasedOnCurrent()
        {
            string fullDocumentPath = DocumentUtil.SaveCopyAs();
            Utils.AddNewDocument(fullDocumentPath);
            File.Delete(fullDocumentPath);
        }

        public void OnDocumentInfoButtonClick(Office.IRibbonControl control)
        {
            switch (control.Id)
            {
                case "BtnHeaderFooterStamps":
                    ShowHeaderFooterStampsWindow();
                    return;
                case "BtnDocId":
                    ShowDocIdInsertionWindow();
                    return;
                case "BtnDraftDateStamp":
                    ShowDraftDateStampInsertionWindow();
                    return;
            }

        }

        public void ToggleLetterhead()
        {
            int? actionID = Login.AssureLogin(22);
            if (actionID == null)
            {
                return;
            }

            LetterheadToggler letterheadToggler = new LetterheadToggler();
            letterheadToggler.ToggleLetterhead();
        }

        private void ShowTableInsertionDialog()
        {
            int? actionID = Login.AssureLogin(21);
            if (actionID == null)
            {
                return;
            }

            ParagraphNumberingToolbarManager.Instance.EnsureWindowIsHidden();
            new InfowareTableInsertionWindow().ShowDialog();
            ParagraphNumberingToolbarManager.Instance.RestoreWindowIfNecessary();
        }

        private void ShowSigningLinesTaskPane()
        {
            int? actionID = Login.AssureLogin(18);
            if (actionID == null)
            {
                return;
            }

            CustomTaskPane taskPane = TaskPaneUtil2.CreateCustomTaskPane(INFOWARE, new SigningLinesUserControl());
            taskPane.Width = 380;
            taskPane.Visible = true;
        }

        private void CopyWholeDocumentToClipboard()
        {
            int? actionID = Login.AssureLogin(23);
            if (actionID == null)
            {
                return;
            }

            Globals.ThisAddIn.Application.ActiveDocument.Content.Copy();

            ParagraphNumberingToolbarManager.Instance.EnsureWindowIsHidden();
            new InfowareInformationWindow(LanguageManager.GetTranslation(LanguageConstants.DocumentHasBeenCopied, "Document has been copied.")).ShowDialog();
            ParagraphNumberingToolbarManager.Instance.RestoreWindowIfNecessary();
        }

        private void ShowDocumentPastingWindow()
        {
            int? actionID = Login.AssureLogin(24);
            if (actionID == null)
            {
                return;
            }

            ParagraphNumberingToolbarManager.Instance.EnsureWindowIsHidden();
            new InfowareDocumentPasteWindow().ShowDialog();
            ParagraphNumberingToolbarManager.Instance.RestoreWindowIfNecessary();
        }

        private void OpenNewDocumentWithSelectedText()
        {
            int? actionID = Login.AssureLogin(34);
            if (actionID == null)
            {
                return;
            }

            Selection selection = Globals.ThisAddIn.Application.Selection;

            if (selection.Range.Start == selection.Range.End)
            {
                ParagraphNumberingToolbarManager.Instance.EnsureWindowIsHidden();
                new InfowareInformationWindow(LanguageManager.GetTranslation(LanguageConstants.NoSelectionHasBeenMade, "No selection has been made")).ShowDialog();
                ParagraphNumberingToolbarManager.Instance.RestoreWindowIfNecessary();
                return;
            }

            selection.Copy();

            var blankDocumentTemplateFromList = Utils.GetTemplateFromTemplateListByType(TemplateType.BlankDocument);
            if (blankDocumentTemplateFromList != null)
            {
                if (blankDocumentTemplateFromList.ResultOk)
                {
                    var downloadedBlankDocumentTemplate = Utils.DownloadAndGetTemplate(blankDocumentTemplateFromList.Template, false);
                    if (downloadedBlankDocumentTemplate != null)
                    {
                        var addedDocument = Utils.AddNewDocument(template: downloadedBlankDocumentTemplate.TemplatePath, documentType: WdNewDocumentType.wdNewBlankDocument);
                        Globals.ThisAddIn.Application.Selection.Paste();
                        DocumentUtils.DoSave(addedDocument);
                    }
                }
            }
            else
            {
                var addedDocument = Utils.AddNewDocument(documentType: WdNewDocumentType.wdNewBlankDocument);
                Globals.ThisAddIn.Application.Selection.Paste();
                DocumentUtils.DoSave(addedDocument);
            }

            Clipboard.Clear();
        }

        private void ShowHeaderFooterStampsWindow()
        {
            int? actionID = Login.AssureLogin(35);
            if (actionID == null)
            {
                return;
            }

            var window = new InfowareHeaderFooterStampsWindow();

            ParagraphNumberingToolbarManager.Instance.EnsureWindowIsHidden();
            if (window.GetHeaderFooterStampCount() == 0)
            {
                new InfowareInformationWindow(LanguageManager.GetTranslation(LanguageConstants.NoHeaderFooterFound, "No Header or Footer stamps found.")).ShowDialog();
            }
            else
            {
                window.ShowDialog();
            }
            ParagraphNumberingToolbarManager.Instance.RestoreWindowIfNecessary();
        }

        private void OpenWatermarksWindow()
        {
            int? actionID = Login.AssureLogin(19);
            if (actionID == null)
            {
                return;
            }

            ParagraphNumberingToolbarManager.Instance.EnsureWindowIsHidden();
            new InfowareWatermarksWindow().ShowDialog();
            ParagraphNumberingToolbarManager.Instance.RestoreWindowIfNecessary();
        }

        private void ShowCalendarInsertWindow()
        {
            int? actionID = Login.AssureLogin(36);
            if (actionID == null)
            {
                return;
            }

            ParagraphNumberingToolbarManager.Instance.EnsureWindowIsHidden();
            new InfowareInsertCalendarWindow().ShowDialog();
            ParagraphNumberingToolbarManager.Instance.RestoreWindowIfNecessary();
        }

        #endregion


        public void ManageCompanies_Click(Office.IRibbonControl control)
        {
            int? actionID = Login.AssureLogin(10);
            if (actionID == null)
            {
                return;
            }

            ParagraphNumberingToolbarManager.Instance.EnsureWindowIsHidden();
            CompaniesWindow companiesWindow = new CompaniesWindow();
            companiesWindow.ShowDialog();
            ParagraphNumberingToolbarManager.Instance.RestoreWindowIfNecessary();
        }

        public void AddAuthors_Click(Office.IRibbonControl control)
        {
            int? actionID = Login.AssureLogin(43);
            if (actionID == null)
            {
                return;
            }
            new AuthorInfo().Show();
        }

        public void ManageAuthors_Click(Office.IRibbonControl control) // ????
        {
            int? actionID = Login.AssureLogin(43);
            if (actionID == null)
            {
                return;
            }
            new ManageUsers().Show();
        }

        public void CurrentAuthor_Click(Office.IRibbonControl control) // ????
        {
            int? actionID = Login.AssureLogin(43);
            if (actionID == null)
            {
                return;
            }
            new CurrentAuthor().Show();
        }

    
       

        public void CurrentAssistant_Click(Office.IRibbonControl control) // ????
        {
            int? actionID = Login.AssureLogin(43);
            if (actionID == null)
            {
                return;
            }
            new CurrentAssistant().Show();
        }

        #region Document assembly callbacks

        public void OnDataFillButtonClick(Office.IRibbonControl control)
        {
            int? actionID = Login.AssureLogin(25);
            if (actionID == null)
            {
                return;
            }


            var dataFill = new DataFillUserControl();
            dataFill.Selection = ThisAddIn.Instance.Application.Selection.Range;
            CustomTaskPane taskPane = TaskPaneUtil2.CreateCustomTaskPane(INFOWARE, dataFill);
            taskPane.Width = 370;
            taskPane.Visible = true;
        }

        public void OnClauseBuilderButtonClick(Office.IRibbonControl control)
        {
            int? actionID = Login.AssureLogin(26);
            if (actionID == null)
            {
                return;
            }
            CustomTaskPane taskPane = TaskPaneUtil2.CreateCustomTaskPane(INFOWARE, new ClauseBuilderUserControl());
            taskPane.Width = 370;
            taskPane.Visible = true;
        }

        private void ShowDocIdInsertionWindow()
        {
            int? actionID = Login.AssureLogin(PLACEHOLDER_OPERATION_ID);
            if (actionID == null)
            {
                return;
            }
            ParagraphNumberingToolbarManager.Instance.EnsureWindowIsHidden();
            new InfowareDocIdWindow().ShowDialog();
            ParagraphNumberingToolbarManager.Instance.RestoreWindowIfNecessary();
        }

        public void OnLibraryButtonClick(Office.IRibbonControl control)
        {
            int? actionID = Login.AssureLogin(27);
            if (actionID == null)
            {
                return;
            }
            ParagraphNumberingToolbarManager.Instance.EnsureWindowIsHidden();

            var dialog = new InfowareWordDaLibraryWindow();
            if (dialog.Open)
            {
                dialog.ShowDialog();
            }

            ParagraphNumberingToolbarManager.Instance.RestoreWindowIfNecessary();
        }

        private void ShowDraftDateStampInsertionWindow()
        {
            int? actionID = Login.AssureLogin(33);
            if (actionID == null)
            {
                return;
            }
            ParagraphNumberingToolbarManager.Instance.EnsureWindowIsHidden();
            new InfowareDraftDateStampWindow().ShowDialog();
            ParagraphNumberingToolbarManager.Instance.RestoreWindowIfNecessary();
        }

        #endregion

        public void PasteUnformattedText_Click(Office.IRibbonControl control) // ????
        {
            int? actionID = Login.AssureLogin(43);
            if (actionID == null)
            {
                return;
            }
            string s = Clipboard.GetText();
            Microsoft.Office.Interop.Word.Document Document = ThisAddIn.Instance.Application.ActiveDocument;
            Microsoft.Office.Interop.Word.Range range = Document.Application.Selection.Range;
            range.Text = s;
        }

        public void TableOfContent_Click(Office.IRibbonControl control)
        {
            int? actionID = Login.AssureLogin(38);
            if (actionID == null)
            {
                return;
            }
            ParagraphNumberingToolbarManager.Instance.EnsureWindowIsHidden();
            var uc1 = new UserControl1();
            uc1.ShowDialog();
            ParagraphNumberingToolbarManager.Instance.RestoreWindowIfNecessary();
            string s = uc1.TitleTB.Text;
            string s1 = uc1.TextPag.Text;
            var item = new ComboboxItem
            {
                Text = "Item Text1",
                Value = 12
            };

        }

        public void Run_in_Headings_Click(Office.IRibbonControl control)
        {
            int? actionID = Login.AssureLogin(40);
            if (actionID == null)
            {
                return;
            }

            Microsoft.Office.Interop.Word.Document document = ThisAddIn.Instance.Application.ActiveDocument;
            Microsoft.Office.Interop.Word.Range range = document.Application.Selection.Range;
            Microsoft.Office.Interop.Word.WdFieldType field = Word.WdFieldType.wdFieldTOCEntry;
            ParagraphNumberingToolbarManager.Instance.EnsureWindowIsHidden();
            Run_in_Headings_WPF wpf = new Run_in_Headings_WPF();
            wpf.ShowDialog();
            ParagraphNumberingToolbarManager.Instance.RestoreWindowIfNecessary();
            string TC;
            string text = wpf.Text;
            string level = wpf.Level;
            bool page = wpf.Page;
            bool multiple = wpf.Multiple;

            Selection originalSelection = DocumentUtil.GetSelection();
            var paragraph = originalSelection.Range.Paragraphs.First.Range;

            string paragraphListFormat = paragraph.ListFormat.ListString;
            string originalText = " \\o [" + text + "] ";

            if (!string.IsNullOrEmpty(paragraphListFormat))
            {
                text = '"' + paragraphListFormat + " " + text + '"';
            }
            else if (string.IsNullOrEmpty(paragraphListFormat) && !string.IsNullOrEmpty(text))
            {
                text = '"' + text + '"';
            }

            if (text != "" && level != "")
            {
                if (page == false && multiple == false)
                {
                    TC = $"{text} \\l {level}{originalText}";
                }
                else if (page == true && multiple == false)
                {
                    TC = $"{text} \\l {level} \\n{originalText}";
                }
                else if (page == false && multiple == true)
                {
                    TC = $"{text} \\f \\l {level}{originalText}";
                }
                else
                {
                    TC = $"{text} \\f \\l {level} \\n{originalText}";
                }
                document.Fields.Add(range, field, TC);
            }
        }
        public void On_Off_Alignment(Office.IRibbonControl control)
        {
            int? actionID = Login.AssureLogin(39);
            if (actionID == null)
            {
                return;
            }

            Microsoft.Office.Interop.Word.Document document = ThisAddIn.Instance.Application.ActiveDocument;
            Microsoft.Office.Interop.Word.Range range = document.Application.Selection.Range;
            float position = range.Sections.First.PageSetup.PageWidth - range.Sections.First.PageSetup.LeftMargin - range.Sections.First.PageSetup.RightMargin;
            bool clear = false;
            foreach (Word.TabStop tabstop in range.Paragraphs.TabStops)
            {
                if (tabstop.Position == position)
                {
                    clear = true;
                }
                break;
            }

            if (clear)
            {
                range.Application.Selection.Paragraphs.TabStops.ClearAll();
            }
            else
            {
                range.Paragraphs.TabStops.ClearAll();
                range.Paragraphs.TabStops.Add(position, Microsoft.Office.Interop.Word.WdTabAlignment.wdAlignTabRight);
            }
        }

        public void FormattingDialog_Click(Office.IRibbonControl control)
        {
            int? actionID = Login.AssureLogin(44);
            if (actionID == null)
            {
                return;
            }

            bool visible;
            visible = ThisAddIn.Instance.Application.TaskPanes[Word.WdTaskPanes.wdTaskPaneRevealFormatting].Visible;
            if (visible)
            {
                ThisAddIn.Instance.Application.TaskPanes[Word.WdTaskPanes.wdTaskPaneRevealFormatting].Visible = false;
            }
            else
            {
                ThisAddIn.Instance.Application.TaskPanes[Word.WdTaskPanes.wdTaskPaneRevealFormatting].Visible = true;
            }
        }
        public void StyleDialog_Click(Office.IRibbonControl control)
        {
            int? actionID = Login.AssureLogin(42);
            if (actionID == null)
            {
                return;
            }

            bool visible;
            visible = ThisAddIn.Instance.Application.TaskPanes[Word.WdTaskPanes.wdTaskPaneFormatting].Visible;
            if (visible)
            {
                ThisAddIn.Instance.Application.TaskPanes[Word.WdTaskPanes.wdTaskPaneFormatting].Visible = false;
            }
            else
            {
                ThisAddIn.Instance.Application.TaskPanes[Word.WdTaskPanes.wdTaskPaneFormatting].Visible = true;
            }
        }

        public void MultiPrint_Click(Office.IRibbonControl control)
        {
            MultiPrint();
            Utils.SetBackDefaultPrinter();
        }
        public void MultiPrintSettings_Click(Office.IRibbonControl control)
        {
            MultiPrint(true);
            Utils.SetBackDefaultPrinter();
        }
        public void MultiPrintCentralizeSettings(Office.IRibbonControl control)
        {
            MultiPrint(pushSettingsToCompany: true);
            Utils.SetBackDefaultPrinter();
        }

        public void PrintCurrentPage_Click(Office.IRibbonControl control)
        {
           ThisAddIn.Instance.Application.ActiveDocument?.PrintOut(Range: WdPrintOutRange.wdPrintCurrentPage);
        }

        public void MultiPrint(bool openSettingsDirectly = false, bool pushSettingsToCompany = false)
        {
            int? actionID = Login.AssureLogin(110);
            if (actionID == null)
            {
                return;
            }
            IMultiPrintSettings impSettings = ThisAddIn.Instance.MPSettings;
            if (impSettings == null)
            {
                MultiPrintSettings MPSettings = null;
                using (new LoadingWindowWrapper())
                {
                    try
                    {
                        var apiResponse = AdminPanelWebApi.GetCustomSetting(Utils.GetProcessorId(), "MultiPrintSettings");
                        if (apiResponse.StatusCode == 200)
                        {
                            MPSettings = Newtonsoft.Json.JsonConvert.DeserializeObject<MultiPrintSettings>((string)apiResponse.Data);
                            ThisAddIn.DoNotOverWriteMPSettings = false;
                        }
                        else
                        {
                            MPSettings = new MultiPrintSettings();
                            MPSettings.PrinterBinSettings = new List<PrinterGeneralSetting>();
                            MPSettings.SpecialPrinterSettings = new PrinterSpecialPrintersSetting();
                            ThisAddIn.DoNotOverWriteMPSettings = true;
                        }
                    }
                    catch
                    {
                        MPSettings = new MultiPrintSettings();
                        MPSettings.PrinterBinSettings = new List<PrinterGeneralSetting>();
                        MPSettings.SpecialPrinterSettings = new PrinterSpecialPrintersSetting();
                        MessageBox.Show("Failed to load MultiPrintSettings...");
                        ThisAddIn.DoNotOverWriteMPSettings = true;
                    }
                }

                impSettings = CreateComposedMultiPrintSettingsIfNeeded(MPSettings);
                ThisAddIn.Instance.MPSettings = impSettings;
            }
            if (!openSettingsDirectly && !pushSettingsToCompany)
            {
                MultiPrint options = new MultiPrint(impSettings);
                ParagraphNumberingToolbarManager.Instance.EnsureWindowIsHidden();
                options.ShowDialog();
                ParagraphNumberingToolbarManager.Instance.RestoreWindowIfNecessary();
            }
            else if (!pushSettingsToCompany)
            {
                MultiPrintSettings_WPF options = new MultiPrintSettings_WPF(impSettings, ThisAddIn.Instance.Application.ActiveDocument);
                ParagraphNumberingToolbarManager.Instance.EnsureWindowIsHidden();
                options.ShowDialog();
                ParagraphNumberingToolbarManager.Instance.RestoreWindowIfNecessary();
            }
            else
            {
                PushPaperBinSettingsDialog dialog = new PushPaperBinSettingsDialog(impSettings.GetMultiPrintSettingsObject());
                ParagraphNumberingToolbarManager.Instance.EnsureWindowIsHidden();
                dialog.Show();
                ParagraphNumberingToolbarManager.Instance.RestoreWindowIfNecessary();
            }
        }

        private IMultiPrintSettings CreateComposedMultiPrintSettingsIfNeeded(MultiPrintSettings mPSettings)
        {
            MultiPrintSettings companySettings = null;

            if (mPSettings.SpecialPrinterSettings == null)
            {
                mPSettings.SpecialPrinterSettings = new PrinterSpecialPrintersSetting();
            }

            string json = AdminPanelWebApi.GetCompanyMultiPrintSettings();

            if (json != null)
            {
                companySettings = JsonConvert.DeserializeObject<MultiPrintSettings>(json);
            }

            if (companySettings != null)
            {
                return new ComposedMultiPrintSettings(companySettings, mPSettings);
            }
            else
            {
                return mPSettings;
            }
        }

        public void Tets_Click(Office.IRibbonControl control)
        {
            Utils.SetDocumentVariable(ThisAddIn.Instance.Application.ActiveDocument, "EnvTemplate", "TestEnveTemplate");

            //LabelGrid_WPF wpf = new LabelGrid_WPF();
            //wpf.ShowDialog();

            //System.Drawing.Printing.PrinterSettings ps = new System.Drawing.Printing.PrinterSettings();
            //ps.PrinterName = "KONICA MINOLTA C200 PCL";
            //if (ps.IsValid)
            //{
            //	var pss = ps.PaperSources;
            //}

            //string printerName = "KONICA MINOLTA C200 PCL";
            //string query = string.Format("SELECT * from Win32_Printer WHERE Name LIKE '%{0}'", printerName);
            //string s = "";
            //using (ManagementObjectSearcher searcher = new ManagementObjectSearcher(query))
            //using (ManagementObjectCollection coll = searcher.Get())
            //{
            //	try
            //	{
            //		foreach (ManagementObject printer in coll)
            //		{
            //			foreach (PropertyData property in printer.Properties)
            //			{
            //				s += string.Format("{0}: {1}\n", property.Name, property.Value);
            //			}
            //		}
            //	}
            //	catch (ManagementException ex)
            //	{
            //		Console.WriteLine(ex.Message);
            //	}
            //}
            //Attributes: 2628 | Availability:  | AvailableJobSheets:  | AveragePagesPerMinute: 0 | Capabilities: System.UInt16[] | CapabilityDescriptions: System.String[] | Caption: KONICA MINOLTA C200 PCL | CharSetsSupported:  | Comment:  | ConfigManagerErrorCode:  | ConfigManagerUserConfig:  | CreationClassName: Win32_Printer | CurrentCapabilities:  | CurrentCharSet:  | CurrentLanguage:  | CurrentMimeType:  | CurrentNaturalLanguage:  | CurrentPaperType:  | Default: True | DefaultCapabilities:  | DefaultCopies:  | DefaultLanguage:  | DefaultMimeType:  | DefaultNumberUp:  | DefaultPaperType:  | DefaultPriority: 0 | Description:  | DetectedErrorState: 1 | DeviceID: KONICA MINOLTA C200 PCL | Direct: False | DoCompleteFirst: True | DriverName: KONICA MINOLTA C200 PCL | EnableBIDI: True | EnableDevQueryPrint: False | ErrorCleared:  | ErrorDescription:  | ErrorInformation:  | ExtendedDetectedErrorState: 1 | ExtendedPrinterStatus: 9 | Hidden: False | HorizontalResolution: 600 | InstallDate:  | JobCountSinceLastReset: 3 | KeepPrintedJobs: False | LanguagesSupported:  | LastErrorCode:  | Local: True | Location:  | MarkingTechnology:  | MaxCopies:  | MaxNumberUp:  | MaxSizeSupported:  | MimeTypesSupported:  | Name: KONICA MINOLTA C200 PCL | NaturalLanguagesSupported:  | Network: False | PaperSizesSupported: System.UInt16[] | PaperTypesAvailable:  | Parameters:  | PNPDeviceID:  | PortName: 192.168.2.12 | PowerManagementCapabilities:  | PowerManagementSupported:  | PrinterPaperNames: System.String[] | PrinterState: 2 | PrinterStatus: 1 | PrintJobDataType: RAW | PrintProcessor: winprint | Priority: 1 | Published: False | Queued: False | RawOnly: False | SeparatorFile:  | ServerName:  | Shared: False | ShareName:  | SpoolEnabled: True | StartTime:  | Status: Error | StatusInfo:  | SystemCreationClassName: Win32_ComputerSystem | SystemName: ROBI-PC | TimeOfLastReset:  | UntilTime:  | VerticalResolution: 600 | WorkOffline: False | 
            //string[] arrExtendedPrinterStatus = {
            //	"","Other", "Unknown", "Idle", "Printing", "Warming Up",
            //	"Stopped Printing", "Offline", "Paused", "Error", "Busy",
            //	"Not Available", "Waiting", "Processing", "Initialization",
            //	"Power Save", "Pending Deletion", "I/O Active", "Manual Feed"
            //};
            //			string[] arrErrorState = {
            //    "Unknown", "Other", "No Error", "Low Paper", "No Paper", "Low Toner",
            //    "No Toner", "Door Open", "Jammed", "Offline", "Service Requested",
            //    "Output Bin Full"
            //};

            //AdminPanelWebApi.RunAsync().GetAwaiter().GetResult();
            //var apiResponse = AdminPanelWebApi.GetUserInfo("AD65F9D6-1488-443B-A058-413559CAB2C2");

            //MultiPrintSettings multiPrintSettings = new MultiPrintSettings();
            //multiPrintSettings.SpecialPrinter = "special printer settings here";
            //PrinterGeneralSetting printerGeneralSetting = new PrinterGeneralSetting();
            //multiPrintSettings.PrinterBinSettings = new List<PrinterGeneralSetting>();
            //printerGeneralSetting.Name = "KONICA MINOLTA C200 PCL";
            //printerGeneralSetting.A4Bin = "Tray1";
            //multiPrintSettings.PrinterBinSettings.Add(printerGeneralSetting);

            //string multiPrintSettingsString = Newtonsoft.Json.JsonConvert.SerializeObject(multiPrintSettings);

            //var apiResponse = AdminPanelWebApi.UpdateCustomSetting("AD65F9D6-1488-443B-A058-413559CAB2C2", Utils.GetProcessorId(), "MultiPrintSettings", multiPrintSettingsString);
            //if (apiResponse.StatusCode == 200)
            //{
            //	apiResponse = AdminPanelWebApi.GetCustomSetting("AD65F9D6-1488-443B-A058-413559CAB2C2", Utils.GetProcessorId(), "MultiPrintSettings");

            //	if (apiResponse.StatusCode == 200)
            //	{
            //		MultiPrintSettings multiPrintSettings2 = Newtonsoft.Json.JsonConvert.DeserializeObject<MultiPrintSettings>((string)apiResponse.Data);
            //	}
            //}



            //PrintDialog printDialog = new PrintDialog();

            //System.Drawing.Printing.PrintDocument printDocument = new System.Drawing.Printing.PrintDocument();

            //printDialog.Document = printDocument;

            //printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(PrintDocument_PrintPage);

            //DialogResult result = printDialog.ShowDialog();

            //if (result == DialogResult.OK)
            //{
            //	printDocument.Print();
            //}

            //Word.Range rng = ThisAddIn.Instance.Application.ActiveDocument.Range(0, 0);
            //rng.Text = "New Text";
            //rng.Select();

            //Application.EnableVisualStyles();
            //         Application.SetCompatibleTextRenderingDefault(false);
            //         Application.Run(new TestForm());

            //Envelope_WPF envelope = new Envelope_WPF();
            //envelope.ShowDialog();

            //new InfowareInformationWindow(envelope.txtHandling.Text).ShowDialog();

            var testForm = new TestForm();
            testForm.ShowDialog();

        }

        public void InsertPrompt_Click(Office.IRibbonControl control)
        {
            int? actionID = Login.AssureLogin(52);
            if (actionID == null)
            {
                return;
            }

            Word.Style promptStyle = Utils.GetOrCreatePromptStyle();

            bool deleteSelection = SettingsManager.GetSetting("DeleteSelOnInsert", "PromptBullets")?.ToLower() == "true";//Todo change section.

            Range range = ThisAddIn.Instance.Application.ActiveWindow.Selection.Range;
            if (!deleteSelection)
            {
                range.Collapse(WdCollapseDirection.wdCollapseEnd);
            }
            else
            {
                range.Text = "";
            }
            range.Select();

            // ThisAddIn.Instance.Application.ActiveWindow.Selection.CopyFormat();
            ThisAddIn.Instance.Application.ActiveWindow.Selection.InsertAfter(" ");
            ThisAddIn.Instance.Application.ActiveWindow.Selection.set_Style(promptStyle);
            List<PromptSymbol> promptSymbols = PromptSymbol.GetPromptSymbols();

            for (int i = 0; i < promptSymbols.Count; i++)
            {
                ThisAddIn.Instance.Application.ActiveWindow.Selection.InsertSymbol(promptSymbols[i].Code, promptSymbols[i].Font);
            }

            range.MoveEnd(WdUnits.wdCharacter, promptSymbols.Count);
            range.set_Style(promptStyle);

            ThisAddIn.Instance.Application.ActiveWindow.Selection.Collapse(WdCollapseDirection.wdCollapseEnd);
            ThisAddIn.Instance.Application.ActiveWindow.Selection.Font.Reset();
        }

        public void togglePromptColor_Click(Office.IRibbonControl control)
        {
            int? actionID = Login.AssureLogin(53);
            if (actionID == null)
            {
                return;
            }

            Word.Style promptStyle = null;
            try
            {
                promptStyle = ThisAddIn.Instance.Application.ActiveDocument.Styles["Prompt"];
            }
            catch (Exception)
            {
            }

            if (promptStyle != null)
            {
                if (promptStyle.Font.Color == WdColor.wdColorBlack)
                {
                    promptStyle.Font.Color = SettingsManager.GetPromptColor();
                }
                else
                {
                    promptStyle.Font.Color = WdColor.wdColorBlack;

                }
            }
        }

        public void SelectRangeAndRemovePromptIfNeeded(Range range)
        {
            range.Select();

            bool removePromptStyle = SettingsManager.GetSetting("ResetOnFind", "PromptBullets")?.ToLower() == "true";

            if (removePromptStyle)
            {
                range.Font.Reset();
            }
        }

        public void previousPrompt_Click(Office.IRibbonControl control)
        {
            int? actionID = Login.AssureLogin(54);
            if (actionID == null)
            {
                return;
            }

            var promptStyle = Utils.GetStyleByName("Prompt");

            if (promptStyle != null)
            {
                Range range = ThisAddIn.Instance.Application.ActiveWindow.Selection.Range;
                range.Collapse(WdCollapseDirection.wdCollapseStart);

                Find find = range.Find;

                find.ClearFormatting();
                find.set_Style(promptStyle);
                find.Format = true;
                find.Forward = false;

                find.Execute();
                if (find.Found)
                {
                    SelectRangeAndRemovePromptIfNeeded(range);
                }
                else
                {
                    Range range2 = ThisAddIn.Instance.Application.ActiveDocument.Content;
                    find = range2.Find;
                    find.ClearFormatting();
                    find.set_Style(promptStyle);
                    find.Format = true;
                    find.Forward = false;

                    find.Execute();
                    if (find.Found)
                    {
                        SelectRangeAndRemovePromptIfNeeded(range2);
                    }
                    else
                    {
                        if ((range2.get_Style() as Word.Style)?.NameLocal == "Prompt")
                        {
                            SelectRangeAndRemovePromptIfNeeded(range2);
                        }
                    }
                }
            }
        }

        public void BtnAddRecipient_Click(Office.IRibbonControl control)
        {
            Word.Document doc = ThisAddIn.Instance.Application.ActiveDocument;

            if (doc != null)
            {
                var list = Utils.GetAllContentControls(doc, "LX-TEMPLATE");

                list = list.Where(x => x.Title == CustomConstants.CUSTOM_RECIPIENT_BLOCK || x.Title == CustomConstants.CUSTOM_RECIPIENT_BLOCK_COPY).ToList();

                bool unFilled = false;

                foreach (Word.ContentControl cc in list)
                {
                    if (!CustomRecipientBlockFilled(cc))
                    {
                        unFilled = true;
                        break;
                    }
                }

                if (!unFilled)
                {
                    Word.ContentControl ccToCopy = list.Where(x => x.Title == CustomConstants.CUSTOM_RECIPIENT_BLOCK_COPY).LastOrDefault();
                    if (ccToCopy == null)
                    {
                        ccToCopy = list.Where(x => x.Title == CustomConstants.CUSTOM_RECIPIENT_BLOCK).FirstOrDefault();
                    }

                    if (ccToCopy != null)
                    {
                        Range range = PaneUtils.CopyCustomRecipientBlock(doc, ccToCopy, 1);

                        var ccToEmpty = range.ContentControls.Cast<Word.ContentControl>().Where(x => x.Title == CustomConstants.CUSTOM_RECIPIENT_BLOCK_COPY).LastOrDefault();
                        //Empty content control
                        if (ccToEmpty != null)
                        {
                            var innerList = ccToEmpty.Range.ContentControls.Cast<Word.ContentControl>().Where(x => x.Tag == CustomConstants.TemplateTags &&
                            (x.Title == "To" || x.Title == "To (Company)" || x.Title == "To (Fax Number)" || x.Title == "To (Phone Number)"));

                            foreach (Word.ContentControl cc2 in innerList)
                            {
                                cc2.Range.Text = "";
                            }
                        }
                    }
                }
            }
        }

     

        private bool CustomRecipientBlockFilled(Word.ContentControl cc)
        {
            if (cc != null)
            {
                var list = cc.Range.ContentControls.Cast<Word.ContentControl>().Where(x => x.Tag == CustomConstants.TemplateTags &&
                (x.Title == "To" || x.Title == "To (Company)" || x.Title == "To (Fax Number)" || x.Title == "To (Phone Number)"));

                foreach (Word.ContentControl cc2 in list)
                {
                    if (!string.IsNullOrWhiteSpace(cc2.Range.Text) && cc2.Range.Text != cc2.Title)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public void nextPrompt_Click(Office.IRibbonControl control)
        {
            int? actionID = Login.AssureLogin(55);

            if (actionID == null)
            {
                return;
            }

            var promptStyle = Utils.GetStyleByName("Prompt");

            if (promptStyle != null)
            {
                Range range = ThisAddIn.Instance.Application.ActiveWindow.Selection.Range;
                range.Collapse(WdCollapseDirection.wdCollapseEnd);
                Find find = range.Find;

                find.ClearFormatting();
                find.set_Style(promptStyle);
                find.Format = true;
                find.Forward = true;
                find.Execute();

                if (find.Found)
                {
                    SelectRangeAndRemovePromptIfNeeded(range);
                }
                else
                {
                    Range range2 = ThisAddIn.Instance.Application.ActiveDocument.Content;
                    find = range2.Find;
                    find.ClearFormatting();
                    find.set_Style(promptStyle);
                    find.Format = true;
                    find.Forward = true;

                    find.Execute();
                    if (find.Found)
                    {
                        SelectRangeAndRemovePromptIfNeeded(range2);
                    }
                    else
                    {
                        if ((range2.get_Style() as Word.Style)?.NameLocal == "Prompt")
                        {
                            SelectRangeAndRemovePromptIfNeeded(range2);
                        }
                    }
                }
            }
        }

        public void DocumentFont_Click(Office.IRibbonControl control)
        {
            int? actionID = Login.AssureLogin(46);
            if (actionID == null)
            {
                return;
            }

            ParagraphNumberingToolbarManager.Instance.EnsureWindowIsHidden();
            DocumentFontDialog dialog = new DocumentFontDialog();
            dialog.ShowDialog();
            ParagraphNumberingToolbarManager.Instance.RestoreWindowIfNecessary();
            if (dialog.Ok)
            {
                string fontName = (dialog.FontComboBox.SelectedItem as ComboBoxItem).Content as string;
                int fontSize = int.Parse((dialog.SizeComboBox.SelectedItem as ComboBoxItem).Content as string);

                List<string> listExcludeStyles = new List<string>();
                string excludeStyles = SettingsManager.GetSetting("KeepDocumentFontSize", "Tools");
                if (excludeStyles != null)
                {
                    excludeStyles = excludeStyles.ToLower();
                    listExcludeStyles = SettingsManager.UnescapeCommaCharacter(excludeStyles.Split(',')).ToList();
                }

                List<string> listExcludeBaseStyles = new List<string>();
                string excludeBaseStyles = SettingsManager.GetSetting("KeepDocumentFontSizeBaseStyles", "Tools");
                if (excludeBaseStyles != null)
                {
                    excludeBaseStyles = excludeBaseStyles.ToLower();
                    listExcludeBaseStyles = SettingsManager.UnescapeCommaCharacter(excludeBaseStyles.Split(',')).ToList();
                }


                List<Word.Style> styles = new List<Word.Style>(ThisAddIn.Instance.Application.ActiveDocument.Styles.Cast<Word.Style>());

                if (dialog.AllStyles.IsChecked == true)
                {
                    foreach (Word.Style style in styles)
                    {
                        bool nonHeading = style.NameLocal.ToLower().IndexOf("heading") != 0 && (style.get_BaseStyle() as Word.Style)?.NameLocal?.ToLower()?.IndexOf("heading") != 0;
                        if (!listExcludeStyles.Contains(style.NameLocal.ToLower()) && !listExcludeBaseStyles.Contains(style.NameLocal.ToLower()) && !(style.get_BaseStyle() is Word.Style && listExcludeBaseStyles.Contains((style.get_BaseStyle() as Word.Style).NameLocal.ToLower())))
                        {
                            if (style.Type == WdStyleType.wdStyleTypeCharacter || style.Type == WdStyleType.wdStyleTypeLinked || style.Type == WdStyleType.wdStyleTypeParagraph || style.Type == WdStyleType.wdStyleTypeParagraphOnly)
                            {
                                style.Font.Name = fontName;
                                if (nonHeading)
                                {
                                    style.Font.Size = (float)fontSize;
                                }
                            }
                        }
                    }
                }
                else
                {
                    foreach (Word.Style style in styles)
                    {
                        bool nonHeading = style.NameLocal.ToLower().IndexOf("heading") != 0 && (style.get_BaseStyle() as Word.Style)?.NameLocal?.ToLower()?.IndexOf("heading") != 0;
                        if (!listExcludeStyles.Contains(style.NameLocal.ToLower()) && !listExcludeBaseStyles.Contains(style.NameLocal.ToLower()) && !(style.get_BaseStyle() is Word.Style && listExcludeBaseStyles.Contains((style.get_BaseStyle() as Word.Style).NameLocal.ToLower())))
                        {
                            if (style.Type == WdStyleType.wdStyleTypeCharacter || style.Type == WdStyleType.wdStyleTypeLinked || style.Type == WdStyleType.wdStyleTypeParagraph || style.Type == WdStyleType.wdStyleTypeParagraphOnly)
                            {
                                if ((style.NameLocal == Utils.GetLocalizedStyleNameFromBuiltInStyle(WdBuiltinStyle.wdStyleNormal)) || ((style.get_BaseStyle() as Word.Style)?.NameLocal == Utils.GetLocalizedStyleNameFromBuiltInStyle(WdBuiltinStyle.wdStyleNormal)))
                                {
                                    style.Font.Name = fontName;
                                    if (nonHeading)
                                    {
                                        style.Font.Size = (float)fontSize;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        public void OnBtnLanguageClick(Office.IRibbonControl control)
        {
            ParagraphNumberingToolbarManager.Instance.EnsureWindowIsHidden();
            new Language.LanguageChooser().ShowDialog();
            ParagraphNumberingToolbarManager.Instance.RestoreWindowIfNecessary();
        }

        public void sections_Click(Office.IRibbonControl control)
        {
            int? actionID = Login.AssureLogin(47);
            if (actionID == null)
            {
                return;
            }

			ParagraphNumberingToolbarManager.Instance.EnsureWindowIsHidden();
            SectionsDialog sectionsDialog = new SectionsDialog();
            ThisAddIn.Instance.UnwantedUnfocus = true;
            sectionsDialog.ShowDialog();
			ParagraphNumberingToolbarManager.Instance.RestoreWindowIfNecessary();

            ButtonEventType buttonEventType = ButtonEventType.Cancel;
            List<KeyValuePair<string, string>> options = new List<KeyValuePair<string, string>>();
            int nrOfSections = DocumentUtil.GetActiveDocument().Sections.Count;
            if (sectionsDialog.Ok)
            {
                buttonEventType = ButtonEventType.OK;
                options.Add(new KeyValuePair<string, string>("double", (sectionsDialog.CoverPage == null && sectionsDialog.Double.IsChecked == true).ToString()));
            }

            int? eventID = null;
            if (actionID > 0)
            {
                eventID = AdminPanelWebApi.LogEvent(actionID, buttonEventType, options);
            }

            Window activeWindow = ThisAddIn.Instance.Application.ActiveWindow;
            Word.Document doc = ThisAddIn.Instance.Application.ActiveDocument;

            if (sectionsDialog.Ok)
            {
                if (sectionsDialog.CoverPage == null)
                {
                    Range range;
                    Section section;

                    bool single = sectionsDialog.Single.IsChecked == true;
                    string size = (sectionsDialog.Size.SelectedItem as ComboBoxItem)?.Content as string;
                    bool portrait = sectionsDialog.Portrait.IsChecked == true;
                    bool restartPageNumbering = sectionsDialog.PreserveNr.IsChecked == false;

                    range = activeWindow.Selection.Range.Duplicate;
                    range.Collapse(WdCollapseDirection.wdCollapseEnd);

                    for (int i = 0; i < (single ? 1 : 2); i++)
                    {
                        range.InsertBreak(WdBreakType.wdSectionBreakNextPage);

                        section = range.Sections.First;

                        if (single || i == 1)
                        {
                            Section firstSection = single ? section : doc.Sections[section.Index - 1];

                            switch (size)
                            {
                                case "Letter":
                                    firstSection.PageSetup.PaperSize = WdPaperSize.wdPaperLetter;
                                    break;
                                case "A4":
                                    firstSection.PageSetup.PaperSize = WdPaperSize.wdPaperA4;
                                    break;
                                case "Legal":
                                    firstSection.PageSetup.PaperSize = WdPaperSize.wdPaperLegal;
                                    break;
                                default:
                                    break;
                            }

                            firstSection.PageSetup.Orientation = portrait ? WdOrientation.wdOrientPortrait : WdOrientation.wdOrientLandscape;
                        }

                        // set Link to Previous, Page Numbering properties
                        Utils.SetSectionHeaderFooterProperties(section.Headers, !restartPageNumbering, restartPageNumbering);
                    }
                }
                else
                {
                    Section mainSection = doc.Sections.Add(doc.Range(0, 0), WdSectionStart.wdSectionNewPage);
                    Section coverPage = doc.Sections[1];

                    // set Header/Footer properties for original section
                    Utils.SetSectionHeaderFooterProperties(mainSection.Headers, false, true);
                    Utils.SetSectionHeaderFooterProperties(mainSection.Footers, false, true);

                    // set Header/Footer properties for Cover Page section
                    Utils.SetSectionHeaderFooterProperties(coverPage.Headers, false, true, true);
                    Utils.SetSectionHeaderFooterProperties(coverPage.Footers, false, true, true);

                    if (sectionsDialog.CoverPage.CoverPageType == CoverPageType.Window)
                    {
                        string companyName = "Infoware";
                        string companyDescription = "Software";

                        JToken currentCompany = AdminPanelWebApi.GetCurrentAuthorACompany();
                        if (currentCompany != null)
                        {
                            companyName = currentCompany["CompanyName"].ToObject<string>();
                            companyDescription = currentCompany["Description"].ToObject<string>();
                        }

                        Range range = coverPage.Range.Duplicate;
                        range.Collapse(WdCollapseDirection.wdCollapseStart);

                        var paragraph2 = range.Paragraphs.Add(range);
                        paragraph2.set_Style(Utils.GetLocalizedStyleNameFromBuiltInStyle(WdBuiltinStyle.wdStyleNormal));
                        paragraph2.Range.Text = '\n' + companyName + '\n' + companyDescription + '\n';

                        Word.Style logoStyle = null;
                        try
                        {
                            logoStyle = ThisAddIn.Instance.Application.ActiveDocument.Styles["Logo"];
                        }
                        catch (Exception ex)
                        {
                        }

                        if (logoStyle == null)
                        {
                            logoStyle = ThisAddIn.Instance.Application.ActiveDocument.Styles.Add("Logo", WdStyleType.wdStyleTypeParagraphOnly);
                            logoStyle.Font.Name = "Times New Roman";
                            logoStyle.Font.Size = 16;
                            logoStyle.Font.Bold = 1;
                            logoStyle.set_BaseStyle(Utils.GetLocalizedStyleNameFromBuiltInStyle(WdBuiltinStyle.wdStyleNormal));
                            logoStyle.Font.SmallCaps = 1;
                            logoStyle.Font.Spacing = 2;
                            logoStyle.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphCenter;
                            logoStyle.ParagraphFormat.RightIndent = 98;
                            logoStyle.ParagraphFormat.LeftIndent = 98;
                            logoStyle.ParagraphFormat.LineSpacing = 12;
                            logoStyle.ParagraphFormat.SpaceAfter = 4;
                            logoStyle.ParagraphFormat.SpaceBefore = 0;
                            logoStyle.Borders.DistanceFromBottom = 4;
                            logoStyle.Borders.DistanceFromLeft = 1;
                            logoStyle.Borders.DistanceFromRight = 1;
                            logoStyle.Borders.DistanceFromTop = 3;
                            logoStyle.Borders.Enable = 1;
                            logoStyle.Borders.OutsideLineStyle = WdLineStyle.wdLineStyleEmboss3D;
                            logoStyle.Borders.OutsideLineWidth = WdLineWidth.wdLineWidth075pt;
                        }
                        Word.Style logoBarSolStyle = null;
                        try
                        {
                            logoBarSolStyle = ThisAddIn.Instance.Application.ActiveDocument.Styles["LogoBarSol"];
                        }
                        catch (Exception ex)
                        {
                        }

                        if (logoBarSolStyle == null)
                        {
                            logoBarSolStyle = ThisAddIn.Instance.Application.ActiveDocument.Styles.Add("LogoBarSol", WdStyleType.wdStyleTypeParagraphOnly);
                            logoBarSolStyle.Font.Name = "Times New Roman";
                            logoBarSolStyle.set_BaseStyle(Utils.GetLocalizedStyleNameFromBuiltInStyle(WdBuiltinStyle.wdStyleNormal));
                            logoBarSolStyle.Font.Size = 10;
                            logoBarSolStyle.Font.SmallCaps = 1;
                            logoBarSolStyle.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphCenter;
                            logoBarSolStyle.ParagraphFormat.SpaceAfter = 12;
                        }

                        coverPage.Range.Paragraphs[1].SpaceAfter = 96;
                        coverPage.Range.Paragraphs[2].set_Style("Logo");
                        coverPage.Range.Paragraphs[3].set_Style("LogoBarSol");

                        float pageWidth = coverPage.PageSetup.PageWidth;
                        Shape shape = doc.Shapes.AddTextbox(Microsoft.Office.Core.MsoTextOrientation.msoTextOrientationHorizontal, (int)((pageWidth - 280) / 2), 260, 280, 140, coverPage.Range);
                        shape.Line.ForeColor.RGB = 10197915;
                        shape.TextFrame.ContainingRange.Paragraphs[1].Alignment = WdParagraphAlignment.wdAlignParagraphCenter;
                        shape.TextFrame.ContainingRange.Font.Size = (float)12.5;
                        Range textBoxRange = shape.TextFrame.ContainingRange.Duplicate;
                        textBoxRange.Collapse(WdCollapseDirection.wdCollapseStart);
                        textBoxRange.Select();
                    }
                    else if (sectionsDialog.CoverPage.CoverPageType == CoverPageType.MultiParty)
                    {
                        var currentActivePrinter = string.Empty;

                        using (new LoadingWindowWrapper())
                        {
                            Range range = coverPage.Range;

                            var nrOfParties = sectionsDialog.CoverPage.NrOfParties;
                            var title = sectionsDialog.CoverPage.Title;

                            var multiPartyAgreementCoverPage = sectionsDialog.GetMultiPartyAgreementCoverPage();
                            if (multiPartyAgreementCoverPage != null && multiPartyAgreementCoverPage.Length > 0)
                            {
                                string multiPartyAgreementCoverPageXml = string.Empty;

                                try
                                {
                                    var tmpFile = Path.ChangeExtension(Path.GetTempFileName(), "docx");
                                    File.WriteAllBytes(tmpFile, multiPartyAgreementCoverPage);

                                    Word.Document multiPartyAgreementCoverPageDocument = Utils.AddNewDocument(tmpFile, isVisible: false);

                                    using (var memStream = new MemoryStream(multiPartyAgreementCoverPage))
                                    {
                                        WordprocessingDocument wdoc = WordprocessingDocument.Open(memStream, false);
                                        multiPartyAgreementCoverPageXml = wdoc.ToFlatOpcString();
                                    }

                                    currentActivePrinter = SetAndGetCurrentActivePrinter();
                                    Utils.CopySectionPageSetup(multiPartyAgreementCoverPageDocument.Sections.First, range.Sections.First);

                                    if (!multiPartyAgreementCoverPageDocument.Saved) { multiPartyAgreementCoverPageDocument.Saved = true; }
                                    multiPartyAgreementCoverPageDocument.Close(WdSaveOptions.wdDoNotSaveChanges);
                                    if (File.Exists(tmpFile))
                                    {
                                        File.Delete(tmpFile);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Debug.WriteLine(ex);
                                }

                                if (!string.IsNullOrEmpty(multiPartyAgreementCoverPageXml))
                                {
                                    range.Collapse();
                                    range.InsertXML(multiPartyAgreementCoverPageXml);

                                    var promptStyle = Utils.GetOrCreatePromptStyle();

                                    Selection originalSelection = DocumentUtil.GetSelection();
                                    int originalStart = originalSelection.Start;
                                    int originalEnd = originalSelection.End;

                                    if (ThisAddIn.Instance.Application?.ActiveDocument != null && ThisAddIn.Instance.Application.ActiveDocument.Bookmarks.Exists("CoverTitle"))
                                    {
                                        var coverTitleBookmark = ThisAddIn.Instance.Application.ActiveDocument.Bookmarks["CoverTitle"];
                                        coverTitleBookmark.Select();

                                        var coverTitleBookmarkRange = ThisAddIn.Instance.Application.Selection.Range;
                                        coverTitleBookmarkRange.Text = title;
                                        coverTitleBookmarkRange.set_Style(promptStyle);
                                    }

                                    if (ThisAddIn.Instance.Application?.ActiveDocument != null && ThisAddIn.Instance.Application.ActiveDocument.Bookmarks.Exists("CoverDate"))
                                    {
                                        var coverDateBookmark = ThisAddIn.Instance.Application.ActiveDocument.Bookmarks["CoverDate"];
                                        coverDateBookmark.Select();

                                        var coverDateBookmarkRange = ThisAddIn.Instance.Application.Selection.Range;
                                        coverDateBookmarkRange.Fields.Add(coverDateBookmarkRange, WdFieldType.wdFieldCreateDate, "\\@ \"MMMM d, yyyy\"");
                                        coverDateBookmarkRange.set_Style(promptStyle);
                                    }

                                    if (ThisAddIn.Instance.Application?.ActiveDocument != null && nrOfParties >= 2)
                                    {
                                        try
                                        {
                                            Range lastRange = null;
                                            Range partyNameBookmarkFormattedTextRange = null;
                                            Range partyAndBookmarkFormattedTextRange = null;

                                            if (ThisAddIn.Instance.Application.ActiveDocument.Bookmarks.Exists("PartyName"))
                                            {
                                                Bookmark partyNameBookmark = ThisAddIn.Instance.Application.ActiveDocument.Bookmarks["PartyName"];
                                                partyNameBookmark.Select();
                                                partyNameBookmarkFormattedTextRange = ThisAddIn.Instance.Application.Selection.Range.FormattedText;
                                                lastRange = partyNameBookmark.Range;
                                            }

                                            if (ThisAddIn.Instance.Application.ActiveDocument.Bookmarks.Exists("PartyAnd"))
                                            {
                                                Bookmark partyAndBookmark = ThisAddIn.Instance.Application.ActiveDocument.Bookmarks["PartyAnd"];
                                                partyAndBookmark.Select();
                                                partyAndBookmarkFormattedTextRange = ThisAddIn.Instance.Application.Selection.Range.FormattedText;
                                                lastRange = partyAndBookmark.Range;
                                            }

                                            if (partyNameBookmarkFormattedTextRange != null)
                                            {
                                                for (var i = 0; i < nrOfParties - 1; ++i)
                                                {
                                                    lastRange.Collapse(WdCollapseDirection.wdCollapseEnd);
                                                    lastRange.FormattedText = partyNameBookmarkFormattedTextRange;

                                                    if (partyAndBookmarkFormattedTextRange != null && i < (nrOfParties - 2))
                                                    {
                                                        lastRange.Expand(WdUnits.wdParagraph);
                                                        lastRange.Collapse(WdCollapseDirection.wdCollapseEnd);
                                                        lastRange.FormattedText = partyAndBookmarkFormattedTextRange;
                                                    }
                                                }
                                            }

                                            if (ThisAddIn.Instance.Application.ActiveDocument.Bookmarks.Exists("PartyName"))
                                            {
                                                ThisAddIn.Instance.Application.ActiveDocument.Bookmarks["PartyName"].Delete();
                                            }

                                            if (ThisAddIn.Instance.Application.ActiveDocument.Bookmarks.Exists("PartyAnd"))
                                            {
                                                ThisAddIn.Instance.Application.ActiveDocument.Bookmarks["PartyAnd"].Delete();
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            Debug.WriteLine(ex);
                                        }
                                    }

                                    Selection selection = DocumentUtil.GetSelection();
                                    selection.Start = originalStart;
                                    selection.End = originalEnd;
                                    selection.Select();

                                    Globals.ThisAddIn.Application.ScreenRefresh();
                                }
                            }
                            else
                            {
                                InsertGeneralMultiPartyCoverPage(coverPage, nrOfParties, title);
                            }
                        }

                        SetBackCurrentActivePrinter(currentActivePrinter);
                    }
                    else
                    {
                        if (sectionsDialog.CustomCoverPage != null)
                        {
                            Range range = coverPage.Range;
                            range.Collapse();
                            range.InsertXML(sectionsDialog.CustomCoverPage);
                        }
                    }
                }
            }

            if (eventID != null && buttonEventType == ButtonEventType.OK)
            {
                string doubleStr = options.Where(kv => kv.Key == "double")?.FirstOrDefault().Value;
                bool Ok = DocumentUtil.GetActiveDocument().Sections.Count == (nrOfSections + (doubleStr == "True" ? 2 : 1));

                AdminPanelWebApi.LogEventResult(eventID, Ok, Ok ? "Section(s) added" : "Section(s) were not added.");
            }
            ParagraphNumberingToolbarManager.Instance.RestoreWindowIfNecessary();
        }

        private void InsertGeneralMultiPartyCoverPage(Section coverPage, int nrOfParties, string title)
        {
            if (coverPage != null && nrOfParties > 0 && title != null)
            {
                var coverTitleStyle = Utils.GetStyleByName("CoverTitle");
                if (coverTitleStyle == null)
                {
                    coverTitleStyle = ThisAddIn.Instance.Application.ActiveDocument.Styles.Add("CoverTitle", WdStyleType.wdStyleTypeParagraph);
                    coverTitleStyle.set_BaseStyle(Utils.GetLocalizedStyleNameFromBuiltInStyle(WdBuiltinStyle.wdStyleNormal));
                    coverTitleStyle.Font.Size = 16;
                    coverTitleStyle.Font.Bold = 1;
                    coverTitleStyle.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphCenter;
                    coverTitleStyle.ParagraphFormat.RightIndent = 72;
                    coverTitleStyle.ParagraphFormat.LeftIndent = 72;
                    coverTitleStyle.ParagraphFormat.LineSpacing = 12;
                    coverTitleStyle.ParagraphFormat.SpaceAfter = 12;
                    coverTitleStyle.ParagraphFormat.SpaceBefore = 0;
                }

                var partyNameStyle = Utils.GetStyleByName("PartyName");
                if (partyNameStyle == null)
                {
                    partyNameStyle = ThisAddIn.Instance.Application.ActiveDocument.Styles.Add("PartyName", WdStyleType.wdStyleTypeParagraph);
                    coverTitleStyle.set_BaseStyle(Utils.GetLocalizedStyleNameFromBuiltInStyle(WdBuiltinStyle.wdStyleNormal));
                    partyNameStyle.Font.Size = 16;
                    partyNameStyle.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphCenter;
                    partyNameStyle.ParagraphFormat.SpaceAfter = 12;
                    partyNameStyle.ParagraphFormat.LineSpacing = 12;
                }

                var promptStyle = Utils.GetOrCreatePromptStyle();

                var coverLineTopStyle = Utils.GetStyleByName("CoverLineTop");
                if (coverLineTopStyle == null)
                {
                    coverLineTopStyle = ThisAddIn.Instance.Application.ActiveDocument.Styles.Add("CoverLineTop", WdStyleType.wdStyleTypeParagraph);
                    coverTitleStyle.set_BaseStyle(Utils.GetLocalizedStyleNameFromBuiltInStyle(WdBuiltinStyle.wdStyleNormal));
                    coverLineTopStyle.Font.Size = 12;
                    coverLineTopStyle.Font.Bold = 1;
                    coverLineTopStyle.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphCenter;
                    coverLineTopStyle.ParagraphFormat.RightIndent = 72;
                    coverLineTopStyle.ParagraphFormat.LeftIndent = 72;
                    coverLineTopStyle.ParagraphFormat.LineSpacing = 2;
                    coverLineTopStyle.ParagraphFormat.SpaceAfter = 36;
                    coverLineTopStyle.ParagraphFormat.SpaceBefore = 12;
                    coverLineTopStyle.Borders[WdBorderType.wdBorderTop].Color = WdColor.wdColorBlack;
                    coverLineTopStyle.Borders[WdBorderType.wdBorderTop].LineStyle = WdLineStyle.wdLineStyleSingle;
                    coverLineTopStyle.Borders[WdBorderType.wdBorderTop].LineWidth = WdLineWidth.wdLineWidth150pt;
                }

                var coverLineBottomStyle = Utils.GetStyleByName("CoverLineBottom");
                if (coverLineBottomStyle == null)
                {
                    coverLineBottomStyle = ThisAddIn.Instance.Application.ActiveDocument.Styles.Add("CoverLineBottom", WdStyleType.wdStyleTypeParagraph);
                    coverTitleStyle.set_BaseStyle(Utils.GetLocalizedStyleNameFromBuiltInStyle(WdBuiltinStyle.wdStyleNormal));
                    coverLineBottomStyle.Font.Size = 12;
                    coverLineBottomStyle.Font.Bold = 1;
                    coverLineBottomStyle.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphCenter;
                    coverLineBottomStyle.ParagraphFormat.RightIndent = 72;
                    coverLineBottomStyle.ParagraphFormat.LeftIndent = 72;
                    coverLineBottomStyle.ParagraphFormat.LineSpacing = 2;
                    coverLineBottomStyle.ParagraphFormat.SpaceAfter = 12;
                    coverLineBottomStyle.ParagraphFormat.SpaceBefore = 36;
                    coverLineBottomStyle.Borders[WdBorderType.wdBorderTop].Color = WdColor.wdColorBlack;
                    coverLineBottomStyle.Borders[WdBorderType.wdBorderTop].LineStyle = WdLineStyle.wdLineStyleSingle;
                    coverLineBottomStyle.Borders[WdBorderType.wdBorderTop].LineWidth = WdLineWidth.wdLineWidth150pt;
                }

                Range range = coverPage.Range.Duplicate;
                range.Collapse(WdCollapseDirection.wdCollapseStart);

                var paragraph2 = range.Paragraphs.Add(range);
                paragraph2.Alignment = WdParagraphAlignment.wdAlignParagraphCenter;

                string partyStr = "[PARTY NAME]\n";
                for (int i = 1; i < nrOfParties; i++)
                {
                    partyStr += "AND\n[PARTY NAME]\n";
                }

                paragraph2.Range.Text = '\n' + title + '\n' + "Made as of " + "\nBetween" + "\n" + partyStr + "\n\n";
                coverPage.Range.Paragraphs[1].set_Style(coverLineTopStyle);
                coverPage.Range.Paragraphs[2].set_Style(coverTitleStyle);
                coverPage.Range.Paragraphs[2].Range.set_Style(promptStyle);
                coverPage.Range.Paragraphs[3].Alignment = WdParagraphAlignment.wdAlignParagraphCenter;

                Range dateRange = coverPage.Range.Paragraphs[3].Range.Duplicate;
                dateRange.MoveEnd(WdUnits.wdCharacter, -1);
                dateRange.Collapse(WdCollapseDirection.wdCollapseEnd);
                dateRange.Fields.Add(dateRange, WdFieldType.wdFieldCreateDate, "\\@ \"MMMM d, yyyy\"");

                coverPage.Range.Paragraphs[4].Alignment = WdParagraphAlignment.wdAlignParagraphCenter;
                coverPage.Range.Paragraphs[5].set_Style(partyNameStyle);
                coverPage.Range.Paragraphs[5].Range.set_Style(promptStyle);

                for (int i = 1; i < nrOfParties; i++)
                {
                    coverPage.Range.Paragraphs[4 + 2 * i].Alignment = WdParagraphAlignment.wdAlignParagraphCenter;
                    coverPage.Range.Paragraphs[5 + 2 * i].set_Style(partyNameStyle);
                    coverPage.Range.Paragraphs[5 + 2 * i].Range.set_Style(promptStyle);
                }

                coverPage.Range.Paragraphs[4 + 2 * nrOfParties].Alignment = WdParagraphAlignment.wdAlignParagraphCenter;
                coverPage.Range.Paragraphs[5 + 2 * nrOfParties].set_Style(coverLineBottomStyle);
                coverPage.PageSetup.VerticalAlignment = WdVerticalAlignment.wdAlignVerticalCenter;
            }
        }

        public void AddendaInsert_Click(Office.IRibbonControl control)
        {
            int? actionID = Login.AssureLogin(56);
            if (actionID == null)
            {
                return;
            }

            ParagraphNumberingToolbarManager.Instance.EnsureWindowIsHidden();
            Addenda.AddendaInsert uc1 = new Addenda.AddendaInsert();
            uc1.ShowDialog();
            ParagraphNumberingToolbarManager.Instance.RestoreWindowIfNecessary();
        }

        public void AddendaMove_Click(Office.IRibbonControl control)
        {
            int? actionID = Login.AssureLogin(57);
            if (actionID == null)
            {
                return;
            }

            ParagraphNumberingToolbarManager.Instance.EnsureWindowIsHidden();
            Addenda.AddendaMove uc1 = new Addenda.AddendaMove();
            uc1.ShowDialog();
            ParagraphNumberingToolbarManager.Instance.RestoreWindowIfNecessary();
        }

        public void AddendaDelete_Click(Office.IRibbonControl control)
        {
            int? actionID = Login.AssureLogin(58);
            if (actionID == null)
            {
                return;
            }

            ParagraphNumberingToolbarManager.Instance.EnsureWindowIsHidden();
            Addenda.AddendaDelete uc1 = new Addenda.AddendaDelete();
            uc1.ShowDialog();
            ParagraphNumberingToolbarManager.Instance.RestoreWindowIfNecessary();
        }

        public void AddendaXRef_Click(Office.IRibbonControl control)
        {
            int? actionID = Login.AssureLogin(59);
            if (actionID == null)
            {
                return;
            }

            ParagraphNumberingToolbarManager.Instance.EnsureWindowIsHidden();
            Addenda.AddendaXRef uc1 = new Addenda.AddendaXRef();
            uc1.ShowDialog();
            ParagraphNumberingToolbarManager.Instance.RestoreWindowIfNecessary();
        }

        public void AddendaParaXRef_Click(Office.IRibbonControl control)
        {
            int? actionID = Login.AssureLogin(60);
            if (actionID == null)
            {
                return;
            }

            Word.Application word = ThisAddIn.Instance.Application;
            Office.CommandBar commandBar;
            Office.CommandBarControl commandBarControl;
            const int InsertCrossReferenceControlId = 775;

            word.CustomizationContext = word.NormalTemplate;
            commandBar = word.CommandBars.Add("InfoTemp");
            commandBarControl = commandBar.Controls.Add(Office.MsoControlType.msoControlButton, InsertCrossReferenceControlId);
            commandBarControl.Execute();
            commandBar.Delete(); // delete the toolbar, we don't need it anymore
            word.NormalTemplate.Saved = true; // mark it clean
        }


        public void AddendaRefreshFields_Click(Office.IRibbonControl control)
        {
            int? actionID = Login.AssureLogin(61);
            if (actionID == null)
            {
                return;
            }

            var addendaOptions = new AddendaOptions();
            addendaOptions.LoadOptionsFromRegistry();
            Utils.UpdateNumeberedAddenda();
            Utils.UpdateAddendaIndexes();
            Utils.UpdateAddendaCrossReferences();

            foreach (Word.Field field in ThisAddIn.Instance.Application.ActiveDocument.Fields)
            {
                try
                {
                    field.Update();
                }
                catch { }
            }
        }


        public void AddendaBookmarks_Click(Office.IRibbonControl control)
        {
            int? actionID = Login.AssureLogin(62);
            if (actionID == null)
            {
                return;
            }

            Microsoft.Office.Interop.Word.Document Document = ThisAddIn.Instance.Application.ActiveDocument;

            if (Document.ActiveWindow.View.ShowBookmarks)
            {
                Document.ActiveWindow.View.ShowBookmarks = false;
            }
            else
            {
                Document.ActiveWindow.View.ShowBookmarks = true;
            }
        }

        public void AddendaOptions_Click(Office.IRibbonControl control)
        {
            int? actionID = Login.AssureLogin(49);
            if (actionID == null)
            {
                return;
            }

            ParagraphNumberingToolbarManager.Instance.EnsureWindowIsHidden();
            Addenda.AddendaOptions uc1 = new Addenda.AddendaOptions();
            uc1.ShowDialog();
            ParagraphNumberingToolbarManager.Instance.RestoreWindowIfNecessary();
        }

        public void InsertClosingAgendaButton_Click(Office.IRibbonControl control)
        {
            int? actionID = Login.AssureLogin(48);
            if (actionID == null)
            {
                return;
            }

            Word.Application ap = ThisAddIn.Instance.Application;
            ParagraphNumberingToolbarManager.Instance.EnsureWindowIsHidden();
            if (ap.Documents.Count >= 1)
            {

                Insert_Closing_Agenda InsertClosingAgendaWPF = new Insert_Closing_Agenda();
                InsertClosingAgendaWPF.ShowDialog();
            }
            else
            {
                new InfowareInformationWindow("There is no document open.").ShowDialog();
            }
            ParagraphNumberingToolbarManager.Instance.RestoreWindowIfNecessary();

        }

        public void DataSource_Click(Office.IRibbonControl control)
        {
            int? actionID = Login.AssureLogin(60);//TODO Change Operation Id
            if (actionID == null)
            {
                return;
            }
            ParagraphNumberingToolbarManager.Instance.EnsureWindowIsHidden();
            DataSourceWindow dataSourceWindow = new DataSourceWindow();
            if (dataSourceWindow.CanOpen)
            {
                dataSourceWindow.ShowDialog();
            }

            ParagraphNumberingToolbarManager.Instance.RestoreWindowIfNecessary();
        }

        public void RemoveExtraTrailingSectionBreak_Click(Office.IRibbonControl control)
        {
            int? actionID = Login.AssureLogin(50);
            if (actionID == null)
            {
                return;
            }

            Word.Document doc = ThisAddIn.Instance.Application.ActiveDocument;
            if (doc.Sections.Count > 1)
            {
                Word.Section lastSection = doc.Sections[doc.Sections.Count];

                if (string.IsNullOrWhiteSpace(lastSection.Range.Text))
                {
                    Utils.SectionSetLinkToPrevious(lastSection);
                    Word.Section previousSection = doc.Sections[doc.Sections.Count - 1];
                    Utils.CopySectionPageSetup(previousSection, lastSection);
                    Word.Range range = previousSection.Range.Duplicate;
                    range.Collapse(Word.WdCollapseDirection.wdCollapseEnd);
                    range.MoveStart(Word.WdUnits.wdCharacter, -1);
                    if (range.Text == "\f")
                    {
                        range.Delete();
                    }
                }
            }
        }



        private string SetAndGetCurrentActivePrinter()
        {
            var currentActivePrinter = Globals.ThisAddIn.Application.ActivePrinter;

            try
            {
                Globals.ThisAddIn.Application.ActivePrinter = "Microsoft Print to PDF";
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }

            return currentActivePrinter;
        }

        private void SetBackCurrentActivePrinter(string currentActivePrinter)
        {
            if (!string.IsNullOrEmpty(currentActivePrinter))
            {
                System.Threading.Tasks.Task.Run(async () =>
                {
                    try
                    {
                        Globals.ThisAddIn.Application.ActivePrinter = currentActivePrinter;
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex);
                    }
                });
            }
        }



        //public void PrintDocument_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        //{
        //	Graphics graphic = e.Graphics;

        //	Font font = new Font("Courier New", 12);


        //	graphic.DrawString("Welcome text, bla bla..", font, new SolidBrush(Color.Red), 10, 10);
        //}
        public void OnBtnInsertContacts(Office.IRibbonControl control)
        {
            int? actionID = Login.AssureLogin(66);
            if (actionID == null)
            {
                return;
            }
            new InsertContactsManager().InsertContacts();
        }

        public void InsertEntity_Click(Office.IRibbonControl control) // ????
        {
            int? actionID = Login.AssureLogin(67);
            if (actionID == null)
            {
                return;
            }
            new InsertEntityManager().InsertEntities();
        }

        public void ExpandRowBtn_Click(Office.IRibbonControl control)
        {
            Word.Document document = ThisAddIn.Instance.Application.ActiveDocument;
            InformDialog.BackPageExpandFirstRow(document);

            DocumentPropertyUtil.SaveShortProperty(CustomConstants.ROW_EXPANDED, "True", true, document);
            this.Invalidate();
        }
        
        public void AddThirdPartyBtn_Click(Office.IRibbonControl control)
        {
            Word.Document document = ThisAddIn.Instance.Application.ActiveDocument;
            InformDialog.BackPageAddThirdParty(document);
            DocumentPropertyUtil.SaveShortProperty(CustomConstants.THIRD_PARTY_ADDED, "True", true, document);
            this.Invalidate();
        }

        public void UpdateDate_Click(Office.IRibbonControl control)
        {
            int? actionID = Login.AssureLogin(51);
            if (actionID == null)
            {
                return;
            }

            /*
            Word.Document DOC = ThisAddIn.Instance.Application.ActiveDocument;

            object o = Missing.Value;
            object oFalse = false;
            object oTrue = true;
            object replace = Word.WdReplace.wdReplaceAll;
            object findWrap = Word.WdFindWrap.wdFindContinue;

            string all_Doc_text = DOC.Content.Text;


            Regex regex = new Regex(@"(January|February|March|April|May|June|July|August|September|October|November|December)\s[0-9]{1,2},\s[0-9]{4}");

            MatchCollection matches = regex.Matches(all_Doc_text);
            if (matches.Count > 0)
            {
                foreach (Word.Range range in DOC.StoryRanges)
                {
                    Word.Find find = range.Find;
                    foreach (Match match in matches)
                    {
                        find.Execute(match.Value, ref o, ref o, ref o, ref oFalse, ref o,
                            ref o, ref findWrap, ref o, DateTime.Now.ToString("MMM dd, yyyy", CultureInfo.InvariantCulture),
                            ref replace, ref o, ref o, ref o, ref o);
                    }
                }
            }*/
            string cultureId = MLanguageUtil.GetResource(MLanguageResourceEnum.DateCultureID);
            Word.Document DOC = ThisAddIn.Instance.Application.ActiveDocument;
            int CurrentDay = DateTime.Now.Day;
            int CurrentMonth = DateTime.Now.Month;
            int CurrentYear = DateTime.Now.Year;
            int CurrentHour = DateTime.Now.Hour;
            int CurrentMinute = DateTime.Now.Minute;
            int CurrentSecond = DateTime.Now.Second;
            int nr_of_changes = 0;
            foreach (Word.Field field in DOC.Fields) // update the Date fields
            {
                if ((field.Type == WdFieldType.wdFieldDate) || (field.Type == WdFieldType.wdFieldCreateDate))
                {
                    field.Update();
                    nr_of_changes++;
                    string TEXT = field.Code.Text;
                    int first = TEXT.IndexOf('\"');
                    int last = TEXT.LastIndexOf('\"');
                    string dateFormat = TEXT.Substring(first + 1, last - first - 1);
                    dateFormat = dateFormat.Replace("am/pm", "tt");

                    field.Result.Text = DateTime.Now.ToString(dateFormat, CultureInfo.CreateSpecificCulture(cultureId));

                }
            }
            // Update Content Controls
            foreach (Microsoft.Office.Interop.Word.ContentControl contentControl in DocumentUtils.GetAllContentControls(DOC))
            {
                if ((contentControl.Title == "Date") && ((contentControl.Tag == "LX-Template") || (contentControl.Tag == "LX-TEMPLATE")))
                {
                    if (contentControl.Type == WdContentControlType.wdContentControlDate)
                    {
                        nr_of_changes++;
                        contentControl.DateCalendarType = WdCalendarType.wdCalendarWestern;
                        string dateFormat = contentControl.DateDisplayFormat.Replace("am/pm", "tt");

                        contentControl.Range.Text = DateTime.Now.ToString(dateFormat, CultureInfo.CreateSpecificCulture(cultureId));
                    }
                }
                if ((contentControl.Type == WdContentControlType.wdContentControlRichText) && (contentControl.Title == "Date") && ((contentControl.Tag == "LX-Template") || (contentControl.Tag == "LX-TEMPLATE")))
                {
                    nr_of_changes++;
                    string Month = "";
                    switch (CurrentMonth)
                    {
                        case 1:
                            Month = "January";
                            break;
                        case 2:
                            Month = "February";
                            break;
                        case 3:
                            Month = "March";
                            break;
                        case 4:
                            Month = "April";
                            break;
                        case 5:
                            Month = "May";
                            break;
                        case 6:
                            Month = "June";
                            break;
                        case 7:
                            Month = "July";
                            break;
                        case 8:
                            Month = "August";
                            break;
                        case 9:
                            Month = "September";
                            break;
                        case 10:
                            Month = "October";
                            break;
                        case 11:
                            Month = "November";
                            break;
                        case 12:
                            Month = "December";
                            break;
                    }

                    contentControl.Range.Text = DateTime.Now.ToString(Utils.GetDefaultDateFormat(), CultureInfo.CreateSpecificCulture(cultureId));
                }
            }
            if (nr_of_changes == 0)
            {
                new InfowareInformationWindow(LanguageManager.GetTranslation(LanguageConstants.NoDateFound, "No date found to update.")).ShowDialog();
            }
        }

        public void blankDocument_Click(Office.IRibbonControl control)
        {
            int? actionID = Login.AssureLogin(2);
            if (actionID == null)
            {
                return;
            }
            OpenTemplateCalled = true;
            Utils.OpenTemplate(TemplateType.BlankDocument);
        }

        public void letterDocument_Click(Office.IRibbonControl control)
        {
            int? actionID = Login.AssureLogin(2);
            if (actionID == null)
            {
                return;
            }
            Utils.OpenTemplate(TemplateType.Letter);
        }

        public void letterMemo_Click(Office.IRibbonControl control)
        {
            int? actionID = Login.AssureLogin(2);
            if (actionID == null)
            {
                return;
            }
            Utils.OpenTemplate(TemplateType.Memo);
        }

        public void letterFax_Click(Office.IRibbonControl control)
        {
            int? actionID = Login.AssureLogin(2);
            if (actionID == null)
            {
                return;
            }
            Utils.OpenTemplate(TemplateType.Fax);
        }

        public void letterEnvelope_Click(Office.IRibbonControl control)
        {
            int? actionID = Login.AssureLogin(2);
            if (actionID == null)
            {
                return;
            }
            Utils.OpenTemplate(TemplateType.Envelope);
        }

        public void letterLabels_Click(Office.IRibbonControl control)
        {
            int? actionID = Login.AssureLogin(2);
            if (actionID == null)
            {
                return;
            }
            Utils.OpenTemplate(TemplateType.Labels);
        }

        public void templates_Click(Office.IRibbonControl control)
        {
            int? actionID = Login.AssureLogin(9);
            if (actionID == null)
            {
                return;
            }

            Utils.OpenTemplate(TemplateType.Custom);
        }

        public void AttachTemplate_Click(Office.IRibbonControl control)
        {
            int? actionID = Login.AssureLogin(32);
            if (actionID == null)
            {
                return;
            }
            ParagraphNumberingToolbarManager.Instance.EnsureWindowIsHidden();
            AttachTemplateDialog atDialog = new AttachTemplateDialog();
            atDialog.ShowDialog();
            ParagraphNumberingToolbarManager.Instance.RestoreWindowIfNecessary();
        }

        public void updataData_Click(Office.IRibbonControl control)
        {
            int? actionID = Login.AssureLogin(2);
            if (actionID == null)
            {
                return;
            }

            string templateTypeString = DocumentPropertyUtil.ReadProperty(TEMPLATE_TYPE_PROP);
            if (Enum.TryParse(templateTypeString, out TemplateType templateType))
            {
                Utils.ShowTaskPane(templateType);
            }
        }

        public void attachTemplate_Click(Office.IRibbonControl control)
        {
            int? actionID = Login.AssureLogin(2);
            if (actionID == null)
            {
                return;
            }
            //NOT YET IMPLEMENTED
        }

        public void templateDesigner_Click(Office.IRibbonControl control)
        {
            int? actionID = Login.AssureLogin(2);
            if (actionID == null)
            {
                return;
            }
            Utils.ShowTaskPane(TemplateType.Custom);
        }

        public void OnBtnContactClick(Office.IRibbonControl control)
        {
            System.Diagnostics.Process.Start(INFOWARE_CONTACT_URL);
        }

        public void OnBtnAboutClick(Office.IRibbonControl control)
        {
            AboutWindow.ShowAboutWindow();
        }

        public void OnBtnHelpGuideClick(Office.IRibbonControl control)
        {
            Microsoft.Office.Tools.CustomTaskPane taskPane = TaskPaneUtil2.CreateCustomTaskPane(INFOWARE, new OnlineHelpGuide());
            taskPane.Width = 375;
            taskPane.Visible = true;
        }

        public void OnBtnDownloadsClick(Office.IRibbonControl control)
        {
            Microsoft.Office.Tools.CustomTaskPane taskPane = TaskPaneUtil2.CreateCustomTaskPane(INFOWARE, new HelpDownloads());
            taskPane.Width = 375;
            taskPane.Visible = true;
        }

        public void OnBtnVideosClick(Office.IRibbonControl control)
        {
            Microsoft.Office.Tools.CustomTaskPane taskPane = TaskPaneUtil2.CreateCustomTaskPane(INFOWARE, new HelpVideos());
            taskPane.Width = 375;
            taskPane.Visible = true;
        }

        public void Inform_Click(Office.IRibbonControl control)
        {
            int? actionID = Login.AssureLogin(64);
            if (actionID == null)
            {
                return;
            }

            ParagraphNumberingToolbarManager.Instance.EnsureWindowIsHidden();
            new InformDialog().ShowDialog();
            ParagraphNumberingToolbarManager.Instance.RestoreWindowIfNecessary();
        }

        public bool HasInformEnabled(Office.IRibbonControl control)
        {
            if (!GetVisible(control.Id))
            {
                return false;
            }

            if (!hasInformEnabled.HasValue)
            {
                hasInformEnabled = AdminPanelWebApi.GetHasInform();
            }

            return hasInformEnabled.Value;
        }

        public bool IsDocumentInformType(Office.IRibbonControl control)
        {
            if (!HasInformEnabled(control)) 
            { 
                return false;
            }

            if (!HasDocumentOpen(null))
            {
                return false;
            }

            string prop = DocumentPropertyUtil.ReadProperty(CustomConstants.FROM_INFORM_PROPERTY);

            return prop == "True";
        }

        public bool RowHasNotBeenExpanded(Office.IRibbonControl control)
        {
            if (!HasInformEnabled(control))
            {
                return false;
            }

            if (!HasDocumentOpen(null))
            {
                return false;
            }

            string prop = DocumentPropertyUtil.ReadProperty(CustomConstants.ROW_EXPANDED);

            return prop == null;
        }

        public bool ThirdPartyHasNotBeenAdded(Office.IRibbonControl control)
        {
            if (!hasInformEnabled.HasValue)
            {
                hasInformEnabled = AdminPanelWebApi.GetHasInform();
            }

            if (!hasInformEnabled.Value)
            {
                return false;
            }

            if (!HasDocumentOpen(null))
            {
                return false;
            }

            string prop = DocumentPropertyUtil.ReadProperty(CustomConstants.THIRD_PARTY_ADDED);

            return prop == null;
        }

        #endregion

        #region Helpers

        private static string GetResourceText(string resourceName)
        {
            var asm = Assembly.GetExecutingAssembly();
            string[] resourceNames = asm.GetManifestResourceNames();
            for (int i = 0; i < resourceNames.Length; ++i)
            {
                if (string.Compare(resourceName, resourceNames[i], StringComparison.OrdinalIgnoreCase) == 0)
                {
                    using (var resourceReader = new StreamReader(asm.GetManifestResourceStream(resourceNames[i])))
                    {
                        if (resourceReader != null)
                        {
                            return resourceReader.ReadToEnd();
                        }
                    }
                }
            }
            return null;
        }

        #endregion
    }
}
