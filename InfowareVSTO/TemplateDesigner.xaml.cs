﻿using InfowareVSTO.Common;
using InfowareVSTO.Common.Language;
using InfowareVSTO.DataSource;
using InfowareVSTO.TaskPaneControls;
using InfowareVSTO.Util;
using InfowareVSTO.Windows;
using InfowareVSTO.Windows.WordDa.Library;
using Microsoft.Office.Interop.Word;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Path = System.IO.Path;

namespace InfowareVSTO
{
    /// <summary>
    /// Interaction logic for TemplateDesigner.xaml
    /// </summary>
    public partial class TemplateDesigner : UserControl
    { 
        private const string TEMPLATE_FILE_FORM_DATA_NAME = "myFile";
        private const string CONTENT_CONTROL_TAG = "LX-TEMPLATE";
        private const string DATA_FIELD_CONTROL_TAG = "WordDA";
        private List<string> vsAll = new List<string>();
        private Microsoft.Office.Interop.Word.Document nativeDocument;

        private Microsoft.Office.Tools.Word.Document vstoDocument;

        public TemplateDesigner()
        {
            InitializeComponent();
            Microsoft.Office.Interop.Word.Document Document = ThisAddIn.Instance.Application.ActiveDocument;
            Microsoft.Office.Core.DocumentProperties properties;
            properties = (Microsoft.Office.Core.DocumentProperties)Document.CustomDocumentProperties;

            foreach (Microsoft.Office.Interop.Word.ContentControl contentControl in DocumentUtil.GetAllContentControls())
            {

                if (contentControl.Tag == DATA_FIELD_CONTROL_TAG && !vsAll.Exists(e => e.Equals(contentControl.Title)))
                {
                    vsAll.Add(contentControl.Title);
                }
            }

            var allowedTemplateTypes = new List<KeyValuePair<TemplateType?, string>>
            {
                new KeyValuePair<TemplateType?, string> (null, LanguageManager.GetTranslation(LanguageConstants.ChooseTemplateType, "Choose Template Type")),
                new KeyValuePair<TemplateType?, string> (TemplateType.BlankDocument, LanguageManager.GetTranslation(LanguageConstants.BlankDocument, "Blank Document")),
                new KeyValuePair<TemplateType?, string> (TemplateType.Letter,LanguageManager.GetTranslation(LanguageConstants.Letter,  TemplateType.Letter.ToString())),
                new KeyValuePair<TemplateType?, string> (TemplateType.Memo,LanguageManager.GetTranslation(LanguageConstants.Memo,  TemplateType.Memo.ToString())),
                new KeyValuePair<TemplateType?, string> (TemplateType.Fax, LanguageManager.GetTranslation(LanguageConstants.Fax, TemplateType.Fax.ToString())),
                new KeyValuePair<TemplateType?, string> (TemplateType.Labels, LanguageManager.GetTranslation(LanguageConstants.Labels, TemplateType.Labels.ToString())),
                new KeyValuePair<TemplateType?, string> (TemplateType.Envelope, LanguageManager.GetTranslation(LanguageConstants.Envelope, TemplateType.Envelope.ToString())),
                new KeyValuePair<TemplateType?, string> (TemplateType.Custom, LanguageManager.GetTranslation(LanguageConstants.Other, "Other"))
            };
            selectedTemplateDesigner.ItemsSource = allowedTemplateTypes;
            selectedTemplateDesigner.DisplayMemberPath = "Value";
            selectedTemplateDesigner.SelectedValuePath = "Key";
            selectedTemplateDesigner.SelectedIndex = 0;

            List<KeyValuePair<int, string>> languages = AdminPanelWebApi.GetCompanyLanguages();
            List<KeyValuePair<int, string>> languageCbItems = new List<KeyValuePair<int, string>>();
            languageCbItems.Add(new KeyValuePair<int, string>(0, LanguageManager.GetTranslation(LanguageConstants.AllLanguages, "All languages")));
            if (languages != null)
            {
                languageCbItems.AddRange(languages.OrderBy(x => x.Value));
            }

            LanguageCb.ItemsSource = languageCbItems;
            LanguageCb.SelectedIndex = 0;
        }

        private void selectedTemplateDesigner_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var selectedVal = selectedTemplateDesigner.SelectedValue;

            if (textForTempChange == null)
            {
                textForTempChange = new TextBlock();
            }
            if (ExpanderAuthorAssistantDataFields == null)
            {
                ExpanderAuthorAssistantDataFields = new Expander();
            }
            if (ExpanderCustomDataField == null)
            {
                ExpanderCustomDataField = new Expander();
            }
            if (ExpanderFirmDataFields == null)
            {
                ExpanderFirmDataFields = new Expander();
            }
            if (ExpanderFormattingElementsDataFields == null)
            {
                ExpanderFormattingElementsDataFields = new Expander();
            }
            if (ExpanderInsertedDataField == null)
            {
                ExpanderInsertedDataField = new Expander();
            }
            if (ExpanderRecipientDataFields == null)
            {
                ExpanderRecipientDataFields = new Expander();
            }

            if (ListOfAuthorAssistantDataFields == null)
            {
                ListOfAuthorAssistantDataFields = new ListView();
            }
            if (ListOfFirmDataFields == null)
            {
                ListOfFirmDataFields = new ListView();
            }
            if (ListOfFormattingElementsDataFields == null)
            {
                ListOfFormattingElementsDataFields = new ListView();
            }
            if (ListOfInsertedDataField == null)
            {
                ListOfInsertedDataField = new ListView();
            }
            if (ListOfRecipientDataFields == null)
            {
                ListOfRecipientDataFields = new ListView();
            }
            if (FileNameMessageOnSave == null)
            {
                FileNameMessageOnSave = new TextBlock();
            }

            textForTempChange.Text = LanguageManager.GetTranslation(LanguageConstants.PlaceCursorAndSelect, "Place cursor in document and select the data field you want to insert.");
            ExpanderAuthorAssistantDataFields.Visibility = Visibility.Hidden;
            ExpanderCustomDataField.Visibility = Visibility.Visible;
            ExpanderFirmDataFields.Visibility = Visibility.Hidden;
            ExpanderFormattingElementsDataFields.Visibility = Visibility.Hidden;
            ExpanderInsertedDataField.Visibility = Visibility.Visible;
            ExpanderRecipientDataFields.Visibility = Visibility.Hidden;
            FileNameMessageOnSave.Visibility = Visibility.Hidden;
            ExpanderFixedClauses.Visibility = Visibility.Visible;
            ExpanderMatterMaster.Visibility = GetMatterMasterVisibility();

            List<string> vs1 = new List<string>();
            List<string> vs2 = new List<string>();
            List<string> vs3 = new List<string>();
            List<string> vs4 = new List<string>();
            ListOfInsertedDataField.ItemsSource = null;
            ListOfInsertedDataField.ItemsSource = vsAll;

            if (selectedVal != null)
            {
                switch (selectedVal)
                {
                    case TemplateType.BlankDocument:
                        textForTempChange.Text = LanguageManager.GetTranslation(LanguageConstants.NoFieldsAvailableBlankDocument, "No fields available to insert for Blank Document.");
                        ExpanderCustomDataField.Visibility = Visibility.Hidden;
                        ExpanderInsertedDataField.Visibility = Visibility.Hidden;
                        ExpanderFixedClauses.Visibility = Visibility.Hidden;
                        ExpanderMatterMaster.Visibility = Visibility.Hidden;
                        break;
                    case TemplateType.Labels:
                        ExpanderFirmDataFields.Visibility = Visibility.Visible;
                        ExpanderAuthorAssistantDataFields.Visibility = Visibility.Visible;
                        ExpanderRecipientDataFields.Visibility = Visibility.Visible;
                        ExpanderFormattingElementsDataFields.Visibility = Visibility.Visible;
                        vs1.Add("Company Logo");
                        vs1.Add("Company Name Rich Text");
                        vs1.Add("Company Name");
                        vs1.Add("Company Description");
                        vs1.Add("Company Address");
                        vs1.Add("Company Address (Graphic)");
                        vs1.Add("Company Address 2");
                        vs1.Add("Company City");
                        vs1.Add("Company Province");
                        vs1.Add("Company Postal Code");
                        vs1.Add("Company Phone");
                        vs1.Add("Company Fax");
                        vs1.Add("Company Website");
                        ListOfFirmDataFields.ItemsSource = null;
                        ListOfFirmDataFields.ItemsSource = vs1;
                        vs2.Add("Recipient");
                        vs2.Add("To (Full Name)");
                        ListOfRecipientDataFields.ItemsSource = null;
                        ListOfRecipientDataFields.ItemsSource = vs2;
                        vs3.Add("Full Name");
                        vs3.Add("Degrees");
                        vs3.Add("Certifications");
                        vs3.Add("Direct Telephone");
                        vs3.Add("Mobile Phone");
                        vs3.Add("Direct Fax");
                        vs3.Add("Email");
                        vs3.Add("Title");
                        vs3.Add("Law Society Number Label");
                        vs3.Add("Lawyer ID");
                        vs3.Add("User1");
                        vs3.Add("User2");
                        vs3.Add("User3");
                        vs3.Add("Assistant Title");
                        vs3.Add("Assistant Name");
                        vs3.Add("Assistant Phone");
                        vs3.Add("Assistant Email");
                        vs3.Add("Author's Initials");
                        vs3.Add("Assistant's Initials");
                        ListOfAuthorAssistantDataFields.ItemsSource = null;
                        ListOfAuthorAssistantDataFields.ItemsSource = vs3;
                        vs4.Add("Office Location");
                        vs4.Add("Date");
                        vs4.Add("Our File Number Tag");
                        vs4.Add("Our File Number");
                        vs4.Add("Their File Number");
                        vs4.Add("Delivery");
                        vs4.Add("Handling");
                        vs4.Add("Attention");
                        vs4.Add("Separator");
                        vs4.Add("Re Line");
                        vs4.Add("Salutation");
                        vs4.Add("Closing");
                        vs4.Add("Enclosure");
                        ListOfFormattingElementsDataFields.ItemsSource = null;
                        ListOfFormattingElementsDataFields.ItemsSource = vs4;
                        break;
                    case TemplateType.Memo:
                        ExpanderFirmDataFields.Visibility = Visibility.Visible;
                        ExpanderAuthorAssistantDataFields.Visibility = Visibility.Visible;
                        ExpanderRecipientDataFields.Visibility = Visibility.Visible;
                        ExpanderFormattingElementsDataFields.Visibility = Visibility.Visible;
                        vs1.Add("Company Logo");
                        vs1.Add("Company Name Rich Text");
                        vs1.Add("Company Name");
                        vs1.Add("Company Description");
                        vs1.Add("Company Address");
                        vs1.Add("Company Address (Graphic)");
                        vs1.Add("Company Address 2");
                        vs1.Add("Company City");
                        vs1.Add("Company Province");
                        vs1.Add("Company Postal Code");
                        vs1.Add("Company Phone");
                        vs1.Add("Company Fax");
                        vs1.Add("Company Website");
                        vs1.Add("Office Name");
                        ListOfFirmDataFields.ItemsSource = null;
                        ListOfFirmDataFields.ItemsSource = vs1;
                        vs2.Add("Full Name");
                        vs2.Add("Degrees");
                        vs2.Add("Certifications");
                        vs2.Add("Direct Telephone");
                        vs2.Add("Mobile Phone");
                        vs2.Add("Direct Fax");
                        vs2.Add("Email");
                        vs2.Add("Title");
                        vs2.Add("Law Society Number Label");
                        vs2.Add("Lawyer ID");
                        vs2.Add("User1");
                        vs2.Add("User2");
                        vs2.Add("User3");
                        vs2.Add("Assistant Title");
                        vs2.Add("Assistant Name");
                        vs2.Add("Assistant Phone");
                        vs2.Add("Assistant Email");
                        vs2.Add("Author's Initials");
                        vs2.Add("Assistant's Initials");
                        ListOfAuthorAssistantDataFields.ItemsSource = null;
                        ListOfAuthorAssistantDataFields.ItemsSource = vs2;
                        vs3.Add("Recipient(s)");
                        vs3.Add("To (Full Name)");
                        vs3.Add("Courtesy Copy");
                        vs3.Add("Blind Copy");
                        ListOfRecipientDataFields.ItemsSource = null;
                        ListOfRecipientDataFields.ItemsSource = vs3;
                        vs4.Add("Re Line");
                        vs4.Add("Date");
                        vs4.Add("Office Location");
                        vs4.Add("Handling");
                        vs4.Add("Our File Number Tag");
                        vs4.Add("Our File Number");
                        vs4.Add("Their File Number");
                        vs4.Add("Delivery");
                        vs4.Add("Enclosure");
                        vs4.Add("Separator");
                        vs4.Add("Salutation");
                        vs4.Add("Closing");
                        ListOfFormattingElementsDataFields.ItemsSource = null;
                        ListOfFormattingElementsDataFields.ItemsSource = vs4;
                        break;
                    case TemplateType.Letter:
                        ExpanderFirmDataFields.Visibility = Visibility.Visible;
                        ExpanderAuthorAssistantDataFields.Visibility = Visibility.Visible;
                        ExpanderRecipientDataFields.Visibility = Visibility.Visible;
                        ExpanderFormattingElementsDataFields.Visibility = Visibility.Visible;
                        vs1.Add("Company Logo");
                        vs1.Add("Company Name Rich Text");
                        vs1.Add("Company Name");
                        vs1.Add("Company Description");
                        vs1.Add("Company Address");
						vs1.Add("Company Address (Graphic)");
                        vs1.Add("Company Address 2");
                        vs1.Add("Company City");
                        vs1.Add("Company Province");
                        vs1.Add("Company Postal Code");
                        vs1.Add("Company Phone");
                        vs1.Add("Company Fax");
                        vs1.Add("Company Website");
                        ListOfFirmDataFields.ItemsSource = null;
                        ListOfFirmDataFields.ItemsSource = vs1;
                        vs2.Add("Full Name");
                        vs2.Add("Degrees");
                        vs2.Add("Certifications");
                        vs2.Add("Direct Telephone");
                        vs2.Add("Mobile Phone");
                        vs2.Add("Direct Fax");
                        vs2.Add("Email");
                        vs2.Add("Title");
                        vs2.Add("Law Society Number Label");
                        vs2.Add("Lawyer ID");
                        vs2.Add("User1");
                        vs2.Add("User2");
                        vs2.Add("User3");
                        vs2.Add("Assistant Title");
                        vs2.Add("Assistant Name");
                        vs2.Add("Assistant Phone");
                        vs2.Add("Assistant Email");
                        vs2.Add("Author's Initials");
                        vs2.Add("Assistant's Initials");
                        vs2.Add("Assistant Block");
                        ListOfAuthorAssistantDataFields.ItemsSource = null;
                        ListOfAuthorAssistantDataFields.ItemsSource = vs2;
                        vs3.Add("Recipient(s)");
                        vs3.Add("To (Full Name)");
                        vs3.Add("Courtesy Copy");
                        vs3.Add("Blind Copy");
                        ListOfRecipientDataFields.ItemsSource = null;
                        ListOfRecipientDataFields.ItemsSource = vs3;
                        vs4.Add("Re Line");
                        vs4.Add("Office Location");
                        vs4.Add("Date");
                        vs4.Add("Our File Number Tag");
                        vs4.Add("Our File Number");
                        vs4.Add("Their File Number");
                        if (SettingsManager.GetSettingAsBool("DiaryDateEnabled", "Letters", false))
                        {
                            vs4.Add("Diary Date");
                        }
                        vs4.Add("Delivery");
                        vs4.Add("Handling");
                        vs4.Add("Attention");
                        vs4.Add("Salutation");
                        vs4.Add("Closing");
                        vs4.Add("Include Per");
                        vs4.Add("Enclosure");
                        vs4.Add("Separator");
                        ListOfFormattingElementsDataFields.ItemsSource = null;
                        ListOfFormattingElementsDataFields.ItemsSource = vs4;
                        break;
                    case TemplateType.Envelope:
                        ExpanderFirmDataFields.Visibility = Visibility.Visible;
                        ExpanderAuthorAssistantDataFields.Visibility = Visibility.Visible;
                        ExpanderRecipientDataFields.Visibility = Visibility.Visible;
                        ExpanderFormattingElementsDataFields.Visibility = Visibility.Visible;
                        vs1.Add("Company Logo");
                        vs1.Add("Company Name Rich Text");
                        vs1.Add("Company Name");
                        vs1.Add("Company Description");
                        vs1.Add("Company Address");
                        vs1.Add("Company Address (Graphic)");
                        vs1.Add("Company Address 2");
                        vs1.Add("Company City");
                        vs1.Add("Company Province");
                        vs1.Add("Company Postal Code");
                        vs1.Add("Company Phone");
                        vs1.Add("Company Fax");
                        vs1.Add("Company Website");
                        ListOfFirmDataFields.ItemsSource = null;
                        ListOfFirmDataFields.ItemsSource = vs1;
                        vs2.Add("Full Name");
                        vs2.Add("Degrees");
                        vs2.Add("Certifications");
                        vs2.Add("Direct Telephone");
                        vs2.Add("Mobile Phone");
                        vs2.Add("Direct Fax");
                        vs2.Add("Email");
                        vs2.Add("Title");
                        vs2.Add("Law Society Number Label");
                        vs2.Add("Lawyer ID");
                        vs2.Add("User1");
                        vs2.Add("User2");
                        vs2.Add("User3");
                        vs2.Add("Assistant Title");
                        vs2.Add("Assistant Name");
                        vs2.Add("Assistant Phone");
                        vs2.Add("Assistant Email");
                        vs2.Add("Author's Initials"); 
                        vs2.Add("Assistant's Initials");
                        ListOfAuthorAssistantDataFields.ItemsSource = null;
                        ListOfAuthorAssistantDataFields.ItemsSource = vs2;
                        vs3.Add("Recipient(s)");
                        vs3.Add("To (Full Name)");
                        ListOfRecipientDataFields.ItemsSource = null;
                        ListOfRecipientDataFields.ItemsSource = vs3;
                        vs4.Add("Our File Number Tag");
                        vs4.Add("Our File Number");
                        vs4.Add("Their File Number");
                        vs4.Add("Office Location");
                        vs4.Add("Date");
                        vs4.Add("Handling");
                        vs4.Add("Attention");
                        vs4.Add("Delivery");
                        vs4.Add("Separator");
                        vs4.Add("Re Line");
                        vs4.Add("Salutation");
                        vs4.Add("Closing");
                        vs4.Add("Enclosure");
                        ListOfFormattingElementsDataFields.ItemsSource = null;
                        ListOfFormattingElementsDataFields.ItemsSource = vs4;
                        break;
                    case TemplateType.Fax:
                        ExpanderFirmDataFields.Visibility = Visibility.Visible;
                        ExpanderAuthorAssistantDataFields.Visibility = Visibility.Visible;
                        ExpanderRecipientDataFields.Visibility = Visibility.Visible;
                        ExpanderFormattingElementsDataFields.Visibility = Visibility.Visible;
                        vs1.Add("Company Logo");
                        vs1.Add("Company Name Rich Text");
                        vs1.Add("Company Name");
                        vs1.Add("Company Description");
                        vs1.Add("Company Address");
                        vs1.Add("Company Address (Graphic)");
                        vs1.Add("Company Address 2");
                        vs1.Add("Company City");
                        vs1.Add("Company Province");
                        vs1.Add("Company Postal Code");
                        vs1.Add("Company Phone");
                        vs1.Add("Company Fax");
                        vs1.Add("Company Website");
                        ListOfFirmDataFields.ItemsSource = null;
                        ListOfFirmDataFields.ItemsSource = vs1;
                        vs2.Add("Full Name");
                        vs2.Add("Degrees");
                        vs2.Add("Certifications");
                        vs2.Add("Direct Telephone");
                        vs2.Add("Mobile Phone");
                        vs2.Add("Direct Fax");
                        vs2.Add("Email");
                        vs2.Add("Title");
                        vs2.Add("Law Society Number Label");
                        vs2.Add("Lawyer ID");
                        vs2.Add("User1");
                        vs2.Add("User2");
                        vs2.Add("User3");
                        vs2.Add("Assistant Title");
                        vs2.Add("Assistant Name");
                        vs2.Add("Assistant Phone");
                        vs2.Add("Assistant Email");
                        vs2.Add("Author's Initials");
                        vs2.Add("Assistant's Initials");
                        ListOfAuthorAssistantDataFields.ItemsSource = null;
                        ListOfAuthorAssistantDataFields.ItemsSource = vs2;
                        vs3.Add("To");
                        vs3.Add("To (Company)");
                        vs3.Add("To (Fax Number)");
                        vs3.Add("To (Phone Number)");
                        vs3.Add("To (Full Name)");
                        vs3.Add("Custom Recipient Block");
                        ListOfRecipientDataFields.ItemsSource = null;
                        ListOfRecipientDataFields.ItemsSource = vs3;
                        vs4.Add("Re Line");
                        vs4.Add("Our File Number Tag");
                        vs4.Add("Our File Number");
                        vs4.Add("Office Location");
                        vs4.Add("Their File Number");
                        vs4.Add("Date");
                        vs4.Add("No. of Pages");
                        vs4.Add("Separator");
                        vs4.Add("Delivery");
                        vs4.Add("Handling");
                        vs4.Add("Salutation");
                        vs4.Add("Closing");
                        vs4.Add("Enclosure");
                        ListOfFormattingElementsDataFields.ItemsSource = null;
                        ListOfFormattingElementsDataFields.ItemsSource = vs4;
                        break;
                    case TemplateType.Custom:
                        ExpanderFirmDataFields.Visibility = Visibility.Visible;
                        ExpanderAuthorAssistantDataFields.Visibility = Visibility.Visible;
                        ExpanderRecipientDataFields.Visibility = Visibility.Visible;
                        ExpanderFormattingElementsDataFields.Visibility = Visibility.Visible;
                        ExpanderCustomDataField.Visibility = Visibility.Visible;
                        ExpanderInsertedDataField.Visibility = Visibility.Visible;
                        vs1.Add("Company Logo");
                        vs1.Add("Company Name Rich Text");
                        vs1.Add("Company Name");
                        vs1.Add("Company Description");
                        vs1.Add("Company Address");
                        vs1.Add("Company Address (Graphic)");
                        vs1.Add("Company Address 2");
                        vs1.Add("Company City");
                        vs1.Add("Company Province");
                        vs1.Add("Company Postal Code");
                        vs1.Add("Company Phone");
                        vs1.Add("Company Fax");
                        vs1.Add("Company Website");
                        ListOfFirmDataFields.ItemsSource = null;
                        ListOfFirmDataFields.ItemsSource = vs1;
                        vs2.Add("Full Name");
                        vs2.Add("Degrees");
                        vs2.Add("Certifications");
                        vs2.Add("Direct Telephone");
                        vs2.Add("Mobile Phone");
                        vs2.Add("Direct Fax");
                        vs2.Add("Email");
                        vs2.Add("Title");
                        vs2.Add("Law Society Number Label");
                        vs2.Add("Lawyer ID");
                        vs2.Add("User1");
                        vs2.Add("User2");
                        vs2.Add("User3");
                        vs2.Add("Assistant Title");
                        vs2.Add("Assistant Name");
                        vs2.Add("Assistant Phone");
                        vs2.Add("Assistant Email");
                        vs2.Add("Author's Initials");
                        vs2.Add("Assistant's Initials");
                        ListOfAuthorAssistantDataFields.ItemsSource = null;
                        ListOfAuthorAssistantDataFields.ItemsSource = vs2;
                        vs3.Add("Recipient(s)");
                        vs3.Add("To (Full Name)");
                        vs3.Add("Courtesy Copy");
                        vs3.Add("Blind Copy");
                        ListOfRecipientDataFields.ItemsSource = null;
                        ListOfRecipientDataFields.ItemsSource = vs3;
                        vs4.Add("Office Location");
                        vs4.Add("Date");
                        vs4.Add("Separator");
                        vs4.Add("Re Line");
                        vs4.Add("Our File Number Tag");
                        vs4.Add("Our File Number");
                        vs4.Add("Their File Number");
                        vs4.Add("Delivery");
                        vs4.Add("Handling");
                        vs4.Add("Salutation");
                        vs4.Add("Closing");
                        vs4.Add("Enclosure");
                        ListOfFormattingElementsDataFields.ItemsSource = null;
                        ListOfFormattingElementsDataFields.ItemsSource = vs4;
                        break;
                }
            }
            else
            {
                ExpanderCustomDataField.Visibility = Visibility.Hidden;
                ExpanderInsertedDataField.Visibility = Visibility.Hidden;
                ExpanderFixedClauses.Visibility = Visibility.Hidden;
                ExpanderMatterMaster.Visibility = Visibility.Hidden;
            }
        }

        private Visibility GetMatterMasterVisibility()
        {
            return DataSourceManager.DataSourceType == DataSourceType.MatterMasterMSSQL || DataSourceManager.DataSourceType == DataSourceType.MatterMasterMySql ? Visibility.Visible : Visibility.Hidden;
        }

        private void MainContAdd_Click(object sender, RoutedEventArgs e)
        {
            object aux = (sender as Button).DataContext;
            if (aux != null)
            {
                if (ListOfInsertedDataField == null)
                {
                    ListOfInsertedDataField = new ListView();
                }
                Range selectionRange = Globals.ThisAddIn.Application.Selection.Range;
                //selectionRange.Text = ((string)aux);
                try
                {
                    Microsoft.Office.Interop.Word.ContentControl contentControl = selectionRange.ContentControls.Add();
                    contentControl.Title = ((string)aux);
                    contentControl.SetPlaceholderText(Text: (string)aux);
                    contentControl.Tag = CONTENT_CONTROL_TAG;
                    //if (!vsAll.Exists(ea => ea.Equals(contentControl.Title)))
                    //{
                    //    vsAll.Add(contentControl.Title);
                    //    ListOfInsertedDataField.ItemsSource = null;
                    //    ListOfInsertedDataField.ItemsSource = vsAll;
                    //}
                }
                catch (System.NotImplementedException)
                {
                    DocumentUtil.ShowCompatibilityModeError(new System.Threading.Tasks.Task(() => { MainContAdd_Click(sender, e); }));
                }
            }
        }

        private void MainContRem_Click(object sender, RoutedEventArgs e)
        {
            object aux = (sender as Button).DataContext;
            if (aux != null)
            {
                if (ListOfInsertedDataField == null)
                {
                    ListOfInsertedDataField = new ListView();
                }
                Microsoft.Office.Interop.Word.Document Document = ThisAddIn.Instance.Application.ActiveDocument;
                Microsoft.Office.Core.DocumentProperties properties;
                properties = (Microsoft.Office.Core.DocumentProperties)Document.CustomDocumentProperties;

                foreach (Microsoft.Office.Interop.Word.ContentControl contentControl in DocumentUtil.GetAllContentControls())
                {
                    if (contentControl.Tag == CONTENT_CONTROL_TAG && contentControl.Title == ((string)aux))
                    {
                        contentControl.Range.Delete();
                        contentControl.Delete();
                    }
                }
                vsAll.Remove(((string)aux));
                ListOfInsertedDataField.ItemsSource = null;
                ListOfInsertedDataField.ItemsSource = vsAll;
            }
        }
        private void MainContCustomAdd_Click(object sender, RoutedEventArgs e)
        {
            object aux = (sender as Button).DataContext;
            if (aux != null)
            {
                if (ListOfInsertedDataField == null)
                {
                    ListOfInsertedDataField = new ListView();
                }
                Range selectionRange = Globals.ThisAddIn.Application.Selection.Range;
                //selectionRange.Text = ((string)aux);
                try
                {
                    Microsoft.Office.Interop.Word.ContentControl contentControl = selectionRange.ContentControls.Add();
                    contentControl.Title = ((string)aux);
                    contentControl.SetPlaceholderText(Text: (string)aux);
                    contentControl.Tag = DATA_FIELD_CONTROL_TAG;
                    if (!vsAll.Exists(ea => ea.Equals(contentControl.Title)))
                    {
                        vsAll.Add(contentControl.Title);
                        ListOfInsertedDataField.ItemsSource = null;
                        ListOfInsertedDataField.ItemsSource = vsAll;
                    }
                }
                catch (System.NotImplementedException)
                {
                    DocumentUtil.ShowCompatibilityModeError(new System.Threading.Tasks.Task(() => { MainContCustomAdd_Click(sender, e); }));
                }
            }
        }

        private void MainContCustomRem_Click(object sender, RoutedEventArgs e)
        {
            object aux = (sender as Button).DataContext;
            if (aux != null)
            {
                if (ListOfInsertedDataField == null)
                {
                    ListOfInsertedDataField = new ListView();
                }
                Microsoft.Office.Interop.Word.Document Document = ThisAddIn.Instance.Application.ActiveDocument;
                Microsoft.Office.Core.DocumentProperties properties;
                properties = (Microsoft.Office.Core.DocumentProperties)Document.CustomDocumentProperties;

                foreach (Microsoft.Office.Interop.Word.ContentControl contentControl in DocumentUtil.GetAllContentControls())
                {
                    if (contentControl.Tag == DATA_FIELD_CONTROL_TAG && contentControl.Title == ((string)aux))
                    {
                        contentControl.Range.Delete();
                        contentControl.Delete();
                    }
                }
                vsAll.Remove(((string)aux));
                ListOfInsertedDataField.ItemsSource = null;
                ListOfInsertedDataField.ItemsSource = vsAll;
            }
        }

        private void CustomDataFieldAddBtn_Click(object sender, RoutedEventArgs e)
        {
            object aux = (CustomDataFieldTextBox as TextBox).Text;
            if (aux != null)
            {
                if (ListOfInsertedDataField == null)
                {
                    ListOfInsertedDataField = new ListView();
                }
                try
                {
                    Range selectionRange = Globals.ThisAddIn.Application.Selection.Range;
                    //selectionRange.Text = ((string)aux);
                    Microsoft.Office.Interop.Word.ContentControl contentControl = selectionRange.ContentControls.Add(WdContentControlType.wdContentControlRichText);
                    contentControl.Title = ((string)aux);
                    contentControl.SetPlaceholderText(Text: (string)aux);
                    contentControl.Tag = DATA_FIELD_CONTROL_TAG;
                    if (!vsAll.Exists(ea => ea.Equals(contentControl.Title)))
                    {
                        vsAll.Add(contentControl.Title);
                        ListOfInsertedDataField.ItemsSource = null;
                        ListOfInsertedDataField.ItemsSource = vsAll;
                    }
                }
                catch (System.NotImplementedException)
                {
                    DocumentUtil.ShowCompatibilityModeError(new System.Threading.Tasks.Task(() => { CustomDataFieldAddBtn_Click(sender, e); }));
                }
            }

        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            TemplateType? selectedVal = (TemplateType?)selectedTemplateDesigner.SelectedValue;
            if (selectedVal == null)
            {
                FileNameMessageOnSave.Text = LanguageManager.GetTranslation(LanguageConstants.PleaseSelectATemplateType, "Please select a template type.");
                FileNameMessageOnSave.Foreground = new SolidColorBrush(Colors.Red);
                FileNameMessageOnSave.Visibility = Visibility.Visible;
                return;
            }

            var fileName = FileNameForDocX.Text;
            if (string.IsNullOrWhiteSpace(fileName))
            {
                FileNameMessageOnSave.Text = LanguageManager.GetTranslation(LanguageConstants.PleaseFillTemplateName, "Please fill in a template name.");
                FileNameMessageOnSave.Foreground = new SolidColorBrush(Colors.Red);
                FileNameMessageOnSave.Visibility = Visibility.Visible;
                return;
            }

            var overwriteTemplate = false;
            var filenameWithExtension = BaseUtils.AddDocxToFilename(FileNameForDocX.Text);
            var templateAlreadyExists = BaseUtils.CheckIfTemplateAlreadyExists(filenameWithExtension, selectedVal);
            if (templateAlreadyExists)
            {
                var confirmationWindow = new InfowareConfirmationWindow(LanguageManager.GetTranslation(LanguageConstants.TemplateAlreadyExistsOverwrite, "Template already exists.\r\nDo you want to overwrite it?"), LanguageManager.GetTranslation(LanguageConstants.TemplateAlreadyExists, "Template already exists"), MessageBoxButton.YesNo);
                confirmationWindow.ShowDialog();

                if (confirmationWindow.Result == MessageBoxResult.Yes)
                {
                    overwriteTemplate = true;
                }
            }

            if (!templateAlreadyExists || (templateAlreadyExists & overwriteTemplate))
            {
                var fullDocumentPath = SaveDocument(filenameWithExtension, selectedVal);
                UploadDocument(fullDocumentPath, selectedVal, overwriteTemplate);
            }
        }

        private string SaveDocument(string fileNameWithExtension, TemplateType? templateType)
        {
            nativeDocument = Globals.ThisAddIn.Application.ActiveDocument;
            //(nativeDocument.CustomDocumentProperties as Microsoft.Office.Core.DocumentProperties).Add("LX-TEMPLATE-TYPE", false, Microsoft.Office.Core.MsoDocProperties.msoPropertyTypeString, (int)templateType.Value);
            DocumentPropertyUtil.SaveShortProperty("LX-TEMPLATE-TYPE", ((int)templateType.Value).ToString(), true);
            vstoDocument = Globals.Factory.GetVstoObject(nativeDocument);

            string tempDirectoryPath = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
            Directory.CreateDirectory(tempDirectoryPath);

            string fullDocumentPath = Path.Combine(tempDirectoryPath, fileNameWithExtension);
            return DocumentUtil.SaveCopyAs(fullDocumentPath);
        }

        private void UploadDocument(string fullDocumentPath, TemplateType? templateType, bool overwriteTemplate = false)
        {
            object authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);

            if (authKey != null && templateType != null)
            {
                var client = new RestClient(ConfigSettings.WebApiUrl);

                string relativeUrl = $"api/WebApi/UploadTemplate?authKey={authKey}&type={(int)templateType.Value}";
                if (overwriteTemplate)
                {
                    relativeUrl = $"{relativeUrl}&overwrite=true";
                }

                if (LanguageCb.SelectedValue is int && (int)LanguageCb.SelectedValue > 0)
                {
                    relativeUrl = $"{relativeUrl}&languageId={(int)LanguageCb.SelectedValue}";
                }

                var request = new RestRequest(relativeUrl, Method.POST);
                request.AddFile(TEMPLATE_FILE_FORM_DATA_NAME, fullDocumentPath);
                IRestResponse response = client.Execute(request);

                File.Delete(fullDocumentPath);
                Directory.Delete(Path.GetDirectoryName(fullDocumentPath));

                if (!response.IsSuccessful)
                {
                    ShowTemplateUploadMessage(false);
                    return;
                }

                JObject content;

                try
                {
                    content = JsonConvert.DeserializeObject<JObject>(response.Content);
                }
                catch (JsonReaderException)
                {
                    // The response didn't contain valid JSON
                    ShowTemplateUploadMessage(false);
                    return;
                }

                int statusCode = content["StatusCode"].ToObject<int>();

                if (statusCode == 200)
                {
                    ShowTemplateUploadMessage(true);
                    return;
                }
                else
                {
                    ShowTemplateUploadMessage(false);
                }
            }
        }

        private void ShowTemplateUploadMessage(bool uploadWasSuccessful)
        {
            FileNameMessageOnSave.Visibility = Visibility.Visible;
            if (uploadWasSuccessful)
            {
                FileNameMessageOnSave.Text = LanguageManager.GetTranslation(LanguageConstants.TemplateUploadedSuccessfully, "Template uploaded successfully.");
                FileNameMessageOnSave.Foreground = new SolidColorBrush(Colors.Green);
            }
            else
            {
                FileNameMessageOnSave.Text = LanguageManager.GetTranslation(LanguageConstants.TemplateUploadUnsuccessfully, "Template upload was unsuccessfull."); 
                FileNameMessageOnSave.Foreground = new SolidColorBrush(Colors.Red);
            }
        }

        private void Expander_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            this.MainScrollViewer.ScrollToVerticalOffset(this.MainScrollViewer.VerticalOffset - e.Delta / 2);
        }

        private void BtnFixedClauses_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new InfowareWordDaLibraryWindow(true);
            if (dialog.Open)
            {
                dialog.ShowDialog();
            }
        }

        private void BtnMatterMaster_Click(object sender, RoutedEventArgs e)
        {
            var mmtd = new MatterMasterTemplateDesigner();
            var taskpane = TaskPaneUtil2.CreateCustomTaskPane(Ribbon.INFOWARE, mmtd);

            taskpane.Width = 370;
            taskpane.Visible = true;
        }

        private void BtnFixedClauseGroups_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new InfowareWordDaLibraryWindow(fixedClauseGroups: true);
            if (dialog.Open)
            {
                dialog.ShowDialog();
            }
        }
    }
}