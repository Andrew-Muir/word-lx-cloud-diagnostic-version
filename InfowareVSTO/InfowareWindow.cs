﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Threading;
using Word = Microsoft.Office.Interop.Word;

namespace InfowareVSTO
{
    public class InfowareWindow : Window
    {
        private const int INTERVAL = 50;
        private const int TRY_COUNT = 20 * 60 * 60 * 3;

        [DllImport("user32")]
        public static extern int SetActiveWindow(int hwnd);

        [DllImport("user32")]
        public static extern int SetForegroundWindow(int hwnd);

        [DllImport("user32")]
        public static extern IntPtr GetForegroundWindow();
        // from winuser.h
        private const int GWL_STYLE = -16,
                          WS_MAXIMIZEBOX = 0x10000,
                          WS_MINIMIZEBOX = 0x20000;

        [DllImport("user32.dll")]
        extern private static int GetWindowLong(IntPtr hwnd, int index);

        [DllImport("user32.dll")]
        extern private static int SetWindowLong(IntPtr hwnd, int index, int value);

        [DllImport("user32.dll")]
        public static extern uint GetWindowThreadProcessId(IntPtr hWnd, IntPtr ProcessId);
        [DllImport("user32.dll")]
        public static extern IntPtr GetKeyboardLayout(uint idThread);

        internal void HideMinimizeButtons()
        {
            IntPtr hwnd = new System.Windows.Interop.WindowInteropHelper(this).Handle;
            var currentStyle = GetWindowLong(hwnd, GWL_STYLE);

            SetWindowLong(hwnd, GWL_STYLE, (currentStyle & ~WS_MINIMIZEBOX & ~WS_MAXIMIZEBOX));
        }

        public bool WindowBlockingDisabled { get; set; } = true;
        public bool FocusOwnerOnClosing { get; set; } = true;
        private int? windowOwner = null;
        private int tries;
        private Timer timer;
        private Dispatcher dispatcher;


        public new void ShowDialog()
        {
            this.ShowDialog(null);
        }

        public void ShowDialog(Window parent)
        {
            SetOwner(parent);
            WindowBlockingDisabled = false;
            base.ShowDialog();
        }

        public new void Show()
        {
            this.Show(null);
        }

        public void Show(Window parent)
        {
            SetOwner(parent);
            base.Show();
        }

        private void SetOwner(Window parent)
        {
            if (parent != null)
            {
                this.Owner = parent;
            }
            else
            {
                WindowInteropHelper wih = new WindowInteropHelper(this);

                if (ThisAddIn.Instance.Application.Build.StartsWith("14"))
                {
                    wih.Owner = Process.GetCurrentProcess().MainWindowHandle;
                    this.windowOwner = (int)Process.GetCurrentProcess().MainWindowHandle;
                }
                else
                {
                    try
                    {
                        wih.Owner = new IntPtr(ThisAddIn.Instance.Application.ActiveWindow.Hwnd);
                        this.windowOwner = ThisAddIn.Instance.Application.ActiveWindow.Hwnd;
                    }
                    catch
                    {
                        wih.Owner = Process.GetCurrentProcess().MainWindowHandle;
                    }
                }
            }
            this.ShowInTaskbar = false;
        }
        protected override void OnInitialized(EventArgs e)
        {
            base.OnInitialized(e);
            this.SourceInitialized += InfowareWindow_SourceInitialized;
        }

        private void InfowareWindow_SourceInitialized(object sender, EventArgs e)
        {
            HideMinimizeButtons();
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            this.WindowBlockingDisabled = true;
            base.OnClosing(e);

            if (FocusOwnerOnClosing)
            {
                try
                {
                    WindowInteropHelper wih = new WindowInteropHelper(this);
                    SetForegroundWindow((int)wih.Owner);
                }
                catch { }
            }
        }

        protected override void OnDeactivated(EventArgs e)
        {
            base.OnDeactivated(e);
            if (this.Visibility == Visibility.Visible && windowOwner != null && !WindowBlockingDisabled)
            {
                if (timer == null)
                {
                    timer = new Timer();
                    dispatcher = Dispatcher.CurrentDispatcher;
                    timer.Elapsed += Timer_Tick;
                    timer.Interval = INTERVAL;
                    timer.Start();
                }
                tries = 0;
            }
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            tries++;
            if (tries > TRY_COUNT)
            {
                StopTimer();
            }

            if (!WindowBlockingDisabled && this.Visibility == Visibility.Visible)
            {
                List<IntPtr> hwnds = new List<IntPtr>();

                try
                {
                    if (ThisAddIn.Instance.Application.Windows.Count > 0)
                    {
                        foreach (Word.Window window in ThisAddIn.Instance.Application.Windows)
                        {
                            try
                            {
                                hwnds.Add(new IntPtr(window.Hwnd));
                            }
                            catch { }
                        }
                    }

                    IntPtr foreground = GetForegroundWindow();
                    if (foreground != null && hwnds.Contains(foreground) && foreground != new IntPtr(this.windowOwner.Value))
                    {
                        SetActiveWindow(windowOwner.Value);
                        SetForegroundWindow(windowOwner.Value);
                        dispatcher?.Invoke(() =>
                        {
                            this.Activate();
                        });

                        StopTimer();
                    }
                }
                catch(Exception ex)
                {
                    Debug.WriteLine(ex);
                }
            }
            else
            {
                StopTimer();
            }
        }

        private void StopTimer()
        {
            if (timer != null)
            {
                timer.Elapsed -= Timer_Tick;
                timer.Stop();
                timer = null;
            }
        }
    }
}
