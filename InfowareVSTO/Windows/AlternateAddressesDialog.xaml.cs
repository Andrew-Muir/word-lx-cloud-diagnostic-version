﻿using InfowareVSTO.Common.DTOs;
using InfowareVSTO.Common.Language;
using InfowareVSTO.Common.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace InfowareVSTO.Windows
{
    /// <summary>
    /// Interaction logic for AlternateAddressesDialog.xaml
    /// </summary>
    public partial class AlternateAddressesDialog : InfowareWindow
    {
        private List<AlternateAddress> addresses;
        private Dictionary<OutlookContact, List<AuthorAddressDTO>> dictionary;
        public AlternateAddressesDialog(Dictionary<OutlookContact, List<AuthorAddressDTO>> alternateAddresses)
        {
            InitializeComponent();
            dictionary = alternateAddresses;
            addresses = ParseAlternateAddresses(alternateAddresses);
            this.AlternateAddressesDataGrid.ItemsSource = addresses;
        }

        private List<AlternateAddress> ParseAlternateAddresses(Dictionary<OutlookContact, List<AuthorAddressDTO>> alternateAddresses)
        {
            List<AlternateAddress> result = new List<AlternateAddress>();
            int i = 0;
            foreach (KeyValuePair<OutlookContact, List<AuthorAddressDTO>> keyValue in alternateAddresses)
            {
                if (keyValue.Value.Count > 1)
                {

                    string name = keyValue.Key.FirstName + " " + keyValue.Key.LastName;
                    if (string.IsNullOrWhiteSpace(name))
                    {
                        name = keyValue.Key.CompanyName;
                    }

                    result.Add(new AlternateAddress()
                    {
                        Contact = keyValue.Key,
                        RadioButtonGroup = "Rb" + i,
                        UseFirstAddress = true,
                        Name = name,
                        DisplayNames = keyValue.Value.Select(x => TranslateAddressType(x.Name)).ToList()

                    }) ;
                }
                i++;
            }

            return result;
        }

        private string TranslateAddressType(string name)
        {
            switch (name)
            {
                case "Business":
                    return LanguageManager.GetTranslation(LanguageConstants.Business, "Business");
                case "Home":
                    return LanguageManager.GetTranslation(LanguageConstants.Home, "Home");
                case "Other":
                    return LanguageManager.GetTranslation(LanguageConstants.Other, "Other");
            }

            return string.Empty;
        }

        private void OKBtn_Click(object sender, RoutedEventArgs e)
        {
            foreach (AlternateAddress address in addresses)
            {
                if (dictionary.TryGetValue(address.Contact, out List<AuthorAddressDTO> list))
                {
                    if (!address.UseFirstAddress)
                    {
                        var addr = list.FirstOrDefault();
                        if (addr != null)
                        {
                            list.Remove(addr);
                        }
                    }
                    else
                    {
                        var addr = list.Skip(1).FirstOrDefault();
                        if (addr != null)
                        {
                            list.Remove(addr);
                        }
                    }
                }
            }

            this.Close();
        }
    }

    public class AlternateAddress
    {
        public string RadioButtonGroup { get; set; }
        public OutlookContact Contact { get; set; }

        public bool UseFirstAddress { get; set; }

        public string Name { get; set; }
        public List<string> DisplayNames { get; set; }
    }
}
