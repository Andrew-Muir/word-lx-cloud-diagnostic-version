﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using InfowareVSTO.Common.Language;
using InfowareVSTO.Util;
using Microsoft.Office.Interop.Word;
using Wpf = System.Windows;

namespace InfowareVSTO.Windows
{
    /// <summary>
    /// Interaction logic for InfowareTableInsertionWindow.xaml
    /// </summary>
    public partial class InfowareTableInsertionWindow : InfowareWindow
    {
        private const string TABLE_TEXT_STYLE_NAME = "Table Text";
        private const string TABLE_HEADING_STYLE_NAME = "Table Heading";
        private const string TABLE_TEXT_STYLE_FONT = "Arial";

        private string tableHeadingStyleName;
        private string TableHeadingStyleName
        {
            get
            {
                if (tableHeadingStyleName != null)
                {
                    return tableHeadingStyleName;
                }

                int builtInStyle = SettingsManager.GetSettingAsInt("TableHeadingStyle", "Styles", int.MaxValue);

                if (builtInStyle < int.MaxValue)
                {
                    string builtInStyleName = Utils.GetLocalizedStyleNameFromBuiltInStyle((WdBuiltinStyle)builtInStyle);
                    if (!builtInStyleName.IsEmpty())
                    {
                        tableHeadingStyleName = builtInStyleName;
                        return builtInStyleName;
                    }
                }
                else
                {
                    string styleName = SettingsManager.GetSetting("TableHeadingStyle", "Styles", TABLE_HEADING_STYLE_NAME);
                    if (styleName != TABLE_HEADING_STYLE_NAME)
                    {
                        try
                        {
                            Style style = ThisAddIn.Instance.Application.ActiveDocument.Styles[styleName];
                            tableHeadingStyleName = style.NameLocal;
                            return style.NameLocal;
                        }
                        catch (Exception ex)
                        {
                            InfowareInformationWindow messageDialog = new InfowareInformationWindow(LanguageManager.GetTranslation(LanguageConstants.Style, "Style") + " " + styleName + LanguageManager.GetTranslation(LanguageConstants.WasNotFound, "was not found."));
                            messageDialog.ShowDialog();
                        }
                    }
                }

                CreateTableHeadingStyle();
                tableHeadingStyleName = TABLE_HEADING_STYLE_NAME;
                return TABLE_HEADING_STYLE_NAME;
            }
        }
        private string tableTextStyleName;
        private string TableTextStyleName
        {
            get
            {
                if (tableTextStyleName != null)
                {
                    return tableTextStyleName;
                }

                int builtInStyle = SettingsManager.GetSettingAsInt("TableTextStyle", "Styles", int.MaxValue);

                if (builtInStyle < int.MaxValue)
                {
                    string builtInStyleName = Utils.GetLocalizedStyleNameFromBuiltInStyle((WdBuiltinStyle)builtInStyle);
                    if (!builtInStyleName.IsEmpty())
                    {
                        tableTextStyleName = builtInStyleName;
                        return builtInStyleName;
                    }
                }
                else
                {
                    string styleName = SettingsManager.GetSetting("TableTextStyle", "Styles", TABLE_TEXT_STYLE_NAME);
                    if (styleName != TABLE_TEXT_STYLE_NAME)
                    {
                        try
                        {
                            Style style = ThisAddIn.Instance.Application.ActiveDocument.Styles[styleName];
                            tableTextStyleName = style.NameLocal;
                            return style.NameLocal;
                        }
                        catch (Exception ex)
                        {
                            InfowareInformationWindow messageDialog = new InfowareInformationWindow(LanguageManager.GetTranslation(LanguageConstants.Style, "Style") + " " + styleName + LanguageManager.GetTranslation(LanguageConstants.WasNotFound, "was not found."));
                            messageDialog.ShowDialog();
                        }
                    }
                }

                CreateTableTextStyle();
                tableTextStyleName = TABLE_TEXT_STYLE_NAME;
                return TABLE_TEXT_STYLE_NAME;
            }
        }

        public InfowareTableInsertionWindow()
        {
            InitializeComponent();
            CreateTableTextStyle();
            CreateTableHeadingStyle();
            UpdateTableActionButtonVisibility();
        }

        /// <summary>
        /// Changes the visibility of the Inset / Edit (table) buttons, based on the existence of tables in the
        /// active document's selection
        /// </summary>
        private void UpdateTableActionButtonVisibility()
        {
            if (IsTableSelected())
            {
                BtnInsert.Visibility = Wpf.Visibility.Collapsed;
                BtnEdit.Visibility = Wpf.Visibility.Visible;

                TbColumnCount.IsEnabled = TbRowCount.IsEnabled = false;
            }
            else
            {
                BtnInsert.Visibility = Wpf.Visibility.Visible;
                BtnEdit.Visibility = Wpf.Visibility.Collapsed;

                TbColumnCount.IsEnabled = TbRowCount.IsEnabled = true;
            }
        }

        private void InsertTable()
        {
            Range selectionRange = Globals.ThisAddIn.Application.ActiveDocument.Application.Selection.Range;

            using (new DocumentUndoTransaction("Table insertion"))
            {
                Table newTable = selectionRange.Tables.Add(selectionRange, GetRowCount(), GetColumnCount());
                ConfigureTable(newTable);
            }

            Close();
        }

        private void EditSelectedTable()
        {
            using (new DocumentUndoTransaction("Table modification"))
            {
                ConfigureTable(GetSelectedTable());
            }

            Close();
        }

        private void ConfigureTable(Table table)
        {
            Row tableHeader = table.Rows[1];

            var tableShading = (WdColor)SettingsManager.GetSettingAsInt("TableHeadingShading", "Styles", -16777216);

            tableHeader.Shading.ForegroundPatternColor = tableShading;

            var tableHeadingAlignment = SettingsManager.GetSettingAsInt("TableHeadingVerticalAlignment", "Styles", 0);

            tableHeader.Cells.VerticalAlignment = (WdCellVerticalAlignment)(tableHeadingAlignment > 1 ? 3 : tableHeadingAlignment);

            object tableHeadingStyleName = TableHeadingStyleName;
            tableHeader.Range.set_Style(tableHeadingStyleName);
            tableHeader.Range.ParagraphFormat.Alignment = (bool)CbHeadingRowsCentered.IsChecked ? WdParagraphAlignment.wdAlignParagraphCenter : WdParagraphAlignment.wdAlignParagraphLeft;

            tableHeader.HeadingFormat = (bool)CbHeadingRowsRepeat.IsChecked ? -1 : 0;
            table.Borders.Enable = (bool)CbAddBorders.IsChecked ? 1 : 0;

            foreach (Row row in table.Rows)
            {
                row.AllowBreakAcrossPages = (bool)CbDoNotBreakAcrossRows.IsChecked ? 0 : -1;
            }

            if ((bool)CbDoNotResizeColumnsToFitContents.IsChecked)
            {
                table.AllowAutoFit = false;
                table.AutoFitBehavior(WdAutoFitBehavior.wdAutoFitFixed);

                foreach (Row row in table.Rows)
                {
                    row.Cells.DistributeWidth();
                }
            }
            else
            {
                table.AutoFitBehavior(WdAutoFitBehavior.wdAutoFitContent);
                table.PreferredWidthType = WdPreferredWidthType.wdPreferredWidthPercent;
                table.PreferredWidth = 100;
            }


            object tableStyleName;
            bool forceNonHeaderRowFont;

            if ((bool)CbFormatNonHeadingRows.IsChecked)
            {
                tableStyleName = TableTextStyleName;
                forceNonHeaderRowFont = false;
            }
            else
            {
                tableStyleName = Utils.GetLocalizedStyleNameFromBuiltInStyle(WdBuiltinStyle.wdStyleNormal);
                forceNonHeaderRowFont = true;
            }

            for (int i = 2; i <= table.Rows.Count; i++)
            {
                table.Rows[i].Range.set_Style(ref tableStyleName);

                if (forceNonHeaderRowFont)
                {
                    Font rowRangeFont = table.Rows[i].Range.Font;

                    rowRangeFont.Name = rowRangeFont.NameAscii = rowRangeFont.NameBi = rowRangeFont.NameOther = TABLE_TEXT_STYLE_FONT;
                }
            }
        }

        private static void CreateTableTextStyle()
        {
            List<Style> styles = new List<Style>(ThisAddIn.Instance.Application.ActiveDocument.Styles.Cast<Style>());
            if (styles == null || !styles.Any(x => x.NameLocal == TABLE_TEXT_STYLE_NAME))
            {
                Style tableTextStyle = Globals.ThisAddIn.Application.ActiveDocument.Styles.Add(TABLE_TEXT_STYLE_NAME);

                object normalStyle = "Normal";
                tableTextStyle.set_BaseStyle(ref normalStyle);
                tableTextStyle.ParagraphFormat.SpaceBefore = tableTextStyle.ParagraphFormat.SpaceAfter = 3;
                tableTextStyle.ParagraphFormat.LineSpacing = 12;
                tableTextStyle.Priority = 1;
                tableTextStyle.Font.Name = tableTextStyle.Font.NameAscii = tableTextStyle.Font.NameBi = tableTextStyle.Font.NameOther = TABLE_TEXT_STYLE_FONT;
                tableTextStyle.LanguageID = WdLanguageID.wdEnglishCanadian;
                tableTextStyle.LanguageIDFarEast = WdLanguageID.wdEnglishCanadian;
            }
        }

        private static void CreateTableHeadingStyle()
        {
            List<Style> styles = new List<Style>(ThisAddIn.Instance.Application.ActiveDocument.Styles.Cast<Style>());
            if (styles == null || !styles.Any(x => x.NameLocal == TABLE_HEADING_STYLE_NAME))
            {
                Style tableHeadingStyle = Globals.ThisAddIn.Application.ActiveDocument.Styles.Add(TABLE_HEADING_STYLE_NAME);

                object tableTextStyle = "Table Text";
                tableHeadingStyle.set_BaseStyle(ref tableTextStyle);
                tableHeadingStyle.Font.Bold = -1;
                tableHeadingStyle.ParagraphFormat.SpaceBefore = 6;
                tableHeadingStyle.ParagraphFormat.SpaceAfter = 6;
            }
        }

        private int GetRowCount()
        {
            return Convert.ToInt32(TbRowCount.Text);
        }

        private int GetColumnCount()
        {
            return Convert.ToInt32(TbColumnCount.Text);
        }


        /// <summary>
        /// Gets a reference to the first of the currently selected tables
        /// </summary>
        /// <returns>a reference to the first of the currently selected tables, or <see langword="null"/> if no such 
        /// table exists</returns>
        private Table GetSelectedTable()
        {
            if (IsTableSelected())
            {
                return Globals.ThisAddIn.Application.Selection.Tables[1];
            }

            return null;
        }

        private bool IsTableSelected()
        {
            Selection selection = Globals.ThisAddIn.Application.Selection;

            bool tableIsSelected = selection.Tables.Count > 0;

            /*
             * Weird stuff. selection.Tables.Count returns 1 when clicking UNDER an existing table, but 
             * selection.Range.Rows.Count throws a runtime exception
             * 
             * Exception:
             * 
             * System.Runtime.InteropServices.COMException
             * HResult=0x800A1713
             * Message=There is no table at this location.
             * Source=InfowareVSTO
             * StackTrace:
             * at Microsoft.Office.Interop.Word.Range.get_Rows()
             * ....
             */

            if (tableIsSelected)
            {
                try
                {
                    return selection.Range.Rows.Count > 0;
                }
                catch
                {
                    return false;
                }
            }

            return false;
        }

        private void OnInsertButtonClick(object sender, System.Windows.RoutedEventArgs e)
        {
            InsertTable();
        }

        private void OnEditButtonClick(object sender, System.Windows.RoutedEventArgs e)
        {
            EditSelectedTable();
        }

        private void OnCancelButtonClick(object sender, System.Windows.RoutedEventArgs e)
        {
            Close();
        }

        private void NumericTextBoxValidator(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            var regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }
    }
}
