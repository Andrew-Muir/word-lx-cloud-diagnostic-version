﻿using InfowareVSTO.Common;
using InfowareVSTO.Common.Language;
using System;
using System.Drawing;
using System.Windows;
using System.Windows.Forms;

namespace InfowareVSTO.Windows
{
    /// <summary>
    /// Interaction logic for InfowareInformationWindow.xaml
    /// </summary>
    public partial class InfowareInformationWindow : InfowareWindow
    {
        private int AutocloseAfterMs = 2000;
        private Timer AutocloseTimer;

        public InfowareInformationWindow(string message, string title = null, WordDefaultIconType wordDefaultIconType = WordDefaultIconType.Information, bool autoclose = false)
        {
            InitializeComponent();

            if (title == null)
            {
                title = LanguageManager.GetTranslation(LanguageConstants.InformationMessage, "Information Message");
            }

            Title = title;
            TbMessage.Text = message;
           
            Icon WordIcon = null;
            switch(wordDefaultIconType)
            {
                case WordDefaultIconType.Information:
                    WordIcon = SystemIcons.Information;
                    break;
                case WordDefaultIconType.Question:
                    WordIcon = SystemIcons.Question;
                    break;
                case WordDefaultIconType.Warning:
                    WordIcon = SystemIcons.Warning;
                    break;
                case WordDefaultIconType.Error:
                    WordIcon = SystemIcons.Error;
                    break;
            }
            NotificationImage.Source = BaseUtils.GetStandardWordNotificationIconsSource(WordIcon);

            if (autoclose)
            {
                OkButton.Visibility = Visibility.Hidden;

                AutocloseTimer = new Timer
                {
                    Interval = AutocloseAfterMs                   
                };
                AutocloseTimer.Tick += new EventHandler(OnTimerTick);
                AutocloseTimer.Start();
            }
        }

        private void OkButtonClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void OnTimerTick(object source, EventArgs args)
        {
            AutocloseTimer.Stop();
            Close();
        }
    }
}
