﻿using System;
using System.Collections.Generic;
using System.Windows;
using InfowareVSTO.MultiLanguage;
using InfowareVSTO.Util;
using Microsoft.Office.Interop.Word;
using Microsoft.Office.Tools.Word;
using System.Linq;
using ContentControl = Microsoft.Office.Interop.Word.ContentControl;
using RestSharp;
using System.Web;
using InfowareVSTO.Common.General;
using Word = Microsoft.Office.Interop.Word;
using InfowareVSTO.Common;

namespace InfowareVSTO.Windows
{
    /// <summary>
    /// Interaction logic for InfowareWatermarksWindow.xaml
    /// </summary>
    public partial class InfowareWatermarksWindow : InfowareWindow
    {
        private const string WATERMARK_CONTROL_TAG = "LX-WATERMARK";

        private const string WATERMARK_CONTROL_NAME_TEMPLATE = WATERMARK_CONTROL_TAG + "_{0}";

        private const string WATERMARK_TEXT_PLACEHOLDER = "##WM-TEXT##";

        private const string WATERMARK_WIDTH_PLACEHOLDER = "##WM-WIDTH";

        private const string WATERMARK_LEFT_MARGIN_PLACEHOLDER = "##WM-LEFTMARGIN##";

        private const int WATERMARK_FONT_SIZE = 24;

        private const string WATERMARK_FONT_NAME = "Arial";

        public static List<string> PrefefinedTexts { get; }

        private Microsoft.Office.Tools.Word.Document vstoDocument;

        private Microsoft.Office.Interop.Word.Document nativeDocument;
        private LegacyWatermark legacyWatermark;

        static InfowareWatermarksWindow()
        {
            var languageId = MLanguageUtil.ActiveDocumentLanguage;

            var defaultWMs = SettingsManager.GetSetting("DefaultWatermarks", "Tools", "Blackline Copy,Confidential,Copy,Draft,Draft for Discussion Purposes,Paid in Full,Precedent,Urgent,Without Prejudice", languageId);

            PrefefinedTexts = SettingsManager.UnescapeCommaCharacter(defaultWMs.Split(',')).ToList();
        }

        public InfowareWatermarksWindow()
        {
            InitializeComponent();

            nativeDocument = Globals.ThisAddIn.Application.ActiveDocument;

            vstoDocument = Globals.Factory.GetVstoObject(nativeDocument);

            SetDefaultWatermarkLocation();

            UpdateDiagonalWatermarkRadioButtonState();
            if (FindWatermarks().Count == 0)
            {
                this.BtnRemove.IsEnabled = false;
            }

            InitLegacyWM();

            DataContext = this;
        }

        private void SetDefaultWatermarkLocation()
        {
            int defaultWatermarkLocation = SettingsManager.GetSettingAsInt("DefaultWatermarkLocation", "Tools", 2);

            switch (defaultWatermarkLocation)
            {
                case 0:
                    this.RbTopRight.IsChecked = true;
                    break;
                case 1:
                    this.RbTopLeft.IsChecked = true;
                    break;
                case 2:
                    this.RbLeft.IsChecked = true;
                    break;
                case 3:
                    this.RbRight.IsChecked = true;
                    break;
                case 4:
                    this.RbDiagonal.IsChecked = true;
                    break;
                default:
                    this.RbLeft.IsChecked = true;
                    break;
            }
        }

        private void InitLegacyWM()
        {
            legacyWatermark = new LegacyWatermark();
            legacyWatermark.Read();
            if (legacyWatermark.HasLegacyWatermark && legacyWatermark.Watermarks.Count > 0)
            {
                this.BtnRemove.IsEnabled = true;

                if (legacyWatermark.LeftMargin)
                {
                    this.RbLeft.IsChecked = true;
                }

                if (legacyWatermark.RightMargin)
                {
                    this.RbRight.IsChecked = true;
                }
                string name = legacyWatermark.Name;
                int index = this.CbPredifinedTexts.Items.Cast<string>().ToList().IndexOf(name);
                if (index >= 0)
                {
                    this.CbPredifinedTexts.SelectedIndex = index;
                    this.RbPredefinedText.IsChecked = true;
                }
                else
                {
                    this.TbCustomText.Text = name;
                    this.RbCustomText.IsChecked = true;
                }
            }
        }

        private void UpdateDiagonalWatermarkRadioButtonState()
        {
            bool showDiagonalWatermarkRadioButton = SettingsManager.GetSettingAsBool("EnableDiagonalWatermarks", "Tools", true);

            if (showDiagonalWatermarkRadioButton)
            {
                RbDiagonal.Visibility = Visibility.Visible;
            }
            else
            {
                if (RbDiagonal.IsChecked == true)
                {
                    RbDiagonal.IsChecked = false;
                    RbLeft.IsChecked = true;
                }

                RbDiagonal.Visibility = Visibility.Collapsed;
            }
        }

        private void OnRemoveButtonClick(object sender, System.Windows.RoutedEventArgs e)
        {
            RemoveWatermark();

            Close();
        }

        private void OnOkButtonClick(object sender, System.Windows.RoutedEventArgs e)
        {
            string watermarkText;

            if ((bool)RbPredefinedText.IsChecked)
            {
                watermarkText = (string)CbPredifinedTexts.SelectedItem;
            }
            else
            {
                watermarkText = TbCustomText.Text;
            }

            InsertWatermark(watermarkText, GetSelectedWatermarkLocation());

            Close();
        }

        private void OnCancelButtonClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void OnTbCustomTextFocus(object sender, RoutedEventArgs e)
        {
            RbCustomText.IsChecked = true;
        }

        private void OnCbPredifinedTextsFocus(object sender, RoutedEventArgs e)
        {
            RbPredefinedText.IsChecked = true;
        }

        private WatermarkLocation GetSelectedWatermarkLocation()
        {
            if ((bool)RbTopRight.IsChecked)
            {
                return WatermarkLocation.TOP_RIGHT;
            }
            else if ((bool)RbTopLeft.IsChecked)
            {
                return WatermarkLocation.TOP_LEFT;
            }
            else if ((bool)RbLeft.IsChecked)
            {
                return WatermarkLocation.LEFT;
            }
            else if ((bool)RbRight.IsChecked)
            {
                return WatermarkLocation.RIGHT;
            }
            else
            {
                return WatermarkLocation.DIAGONAL;
            }
        }

        private void InsertWatermark(string watermarkText, WatermarkLocation watermarkLocation)
        {
            try
            {
                RemoveWatermark();

                using (new DocumentUndoTransaction("Watermark insertion"))
                {
                    ThisAddIn.Instance.Application.ScreenUpdating = false;
                    if (ThisAddIn.Instance.IsFunctionForLogging("ScreenUpdating"))
                        Logger.Log("Disabled Screen Updating (InfowareWatermarksWindow.xaml.cs > InsertWatermark)");
                    foreach (Section wordSection in GetDocumentSections())
                    {
                        if (Utils.IsSectionContinous(wordSection, nativeDocument))
                        {
                            continue;
                        }

                        string watermark = watermarkText;

                        HeaderFooter primaryHeader = wordSection.Headers[WdHeaderFooterIndex.wdHeaderFooterPrimary];
                        HeaderFooter firstPageHeader = wordSection.Headers[WdHeaderFooterIndex.wdHeaderFooterFirstPage];
                        HeaderFooter evenPageHeader = wordSection.Headers[WdHeaderFooterIndex.wdHeaderFooterEvenPages];

                        if (!primaryHeader.LinkToPrevious)
                        {
                            Range primaryHeaderEndRange = primaryHeader.Range.Duplicate;
                            primaryHeaderEndRange.Collapse(WdCollapseDirection.wdCollapseEnd);

                            primaryHeaderEndRange = EnsureRangeNotInOuterContentControl(primaryHeaderEndRange, primaryHeader);
                            if (watermarkLocation != WatermarkLocation.DIAGONAL)
                            {
                                Shape shape = AddNonDiagonalWatermark(primaryHeaderEndRange, primaryHeader, watermark, watermarkLocation);
                                ContentControl watermarkControlEvenPage = nativeDocument.ContentControls.Add(WdContentControlType.wdContentControlRichText, shape.Anchor);
                                watermarkControlEvenPage.Title = GetNewWatermarkControlName();
                                watermarkControlEvenPage.Tag = WATERMARK_CONTROL_TAG;
                            }
                            else
                            {
                                Shape shape = AddDiagonalWatermark(primaryHeaderEndRange, primaryHeader, watermark);
                                ContentControl watermarkControlPrimary = nativeDocument.ContentControls.Add(WdContentControlType.wdContentControlRichText, shape.Anchor);
                                watermarkControlPrimary.Title = GetNewWatermarkControlName();
                                watermarkControlPrimary.Tag = WATERMARK_CONTROL_TAG;
                            }
                        }

                        if (firstPageHeader.Exists && !firstPageHeader.LinkToPrevious)
                        {
                            Range firstpageHeaderEndRange = firstPageHeader.Range.Duplicate;
                            firstpageHeaderEndRange.Collapse(WdCollapseDirection.wdCollapseEnd);
                            firstpageHeaderEndRange = EnsureRangeNotInOuterContentControl(firstpageHeaderEndRange, firstPageHeader);
                            if (watermarkLocation != WatermarkLocation.DIAGONAL)
                            {
                                Shape shape = AddNonDiagonalWatermark(firstpageHeaderEndRange, firstPageHeader, watermark, watermarkLocation);
                                ContentControl watermarkControlEvenPage = nativeDocument.ContentControls.Add(WdContentControlType.wdContentControlRichText, shape.Anchor);
                                watermarkControlEvenPage.Title = GetNewWatermarkControlName();
                                watermarkControlEvenPage.Tag = WATERMARK_CONTROL_TAG;
                            }
                            else
                            {
                                Shape shape = AddDiagonalWatermark(firstpageHeaderEndRange, firstPageHeader, watermark);
                                ContentControl watermarkControlFirstPage = nativeDocument.ContentControls.Add(WdContentControlType.wdContentControlRichText, shape.Anchor);
                                watermarkControlFirstPage.Title = GetNewWatermarkControlName();
                                watermarkControlFirstPage.Tag = WATERMARK_CONTROL_TAG;
                            }
                        }

                        if (evenPageHeader.Exists && !evenPageHeader.LinkToPrevious)
                        {
                            Range evenpageHeaderEndRange = evenPageHeader.Range.Duplicate;
                            evenpageHeaderEndRange.Collapse(WdCollapseDirection.wdCollapseEnd);
                            evenpageHeaderEndRange = EnsureRangeNotInOuterContentControl(evenpageHeaderEndRange, evenPageHeader);
                            if (watermarkLocation != WatermarkLocation.DIAGONAL)
                            {
                                Shape shape = AddNonDiagonalWatermark(evenpageHeaderEndRange, evenPageHeader, watermark, watermarkLocation);
                                ContentControl watermarkControlEvenPage = nativeDocument.ContentControls.Add(WdContentControlType.wdContentControlRichText, shape.Anchor);
                                watermarkControlEvenPage.Title = GetNewWatermarkControlName();
                                watermarkControlEvenPage.Tag = WATERMARK_CONTROL_TAG;
                            }
                            else
                            {
                                Shape shape = AddDiagonalWatermark(evenpageHeaderEndRange, evenPageHeader, watermark);
                                ContentControl watermarkControlEvenPage = nativeDocument.ContentControls.Add(WdContentControlType.wdContentControlRichText, shape.Anchor);
                                watermarkControlEvenPage.Title = GetNewWatermarkControlName();
                                watermarkControlEvenPage.Tag = WATERMARK_CONTROL_TAG;
                            }
                        }
                    }
                    ThisAddIn.Instance.Application.ScreenUpdating = true;
                    if (ThisAddIn.Instance.IsFunctionForLogging("ScreenUpdating"))
                        Logger.Log("Enabled Screen Updating (InfowareWatermarksWindow.xaml.cs > InsertWatermark)");
                }
            }
            catch (NotImplementedException ex)
            {
                DocumentUtil.ShowCompatibilityModeError(new System.Threading.Tasks.Task(() => InsertWatermark(watermarkText, watermarkLocation)));
            }
        }

        private Range EnsureRangeNotInOuterContentControl(Range endRange, HeaderFooter header)
        {
            var contentcontrols = header.Range.ContentControls.Cast<Word.ContentControl>();
            var par = header.Range.Paragraphs.Last;
            foreach (var cc in contentcontrols)
            {
                if (par.Range.Start >= cc.Range.Start - 1 && par.Range.End <= cc.Range.End + 2)
                {
                    if (cc.Range.Paragraphs.Count > 1)
                    {
                        par.Range.InsertParagraphAfter();
                    }
                    else
                    {
                        var tempRange = cc.Range;
                        tempRange.Collapse(WdCollapseDirection.wdCollapseEnd);
                        tempRange.MoveEnd(WdUnits.wdCharacterFormatting, 1);
                        tempRange.MoveStart(WdUnits.wdCharacterFormatting, 1);
                        tempRange.Text = "\u200C";
                    }

                    var tempRange2 = header.Range;
                    tempRange2.Collapse(WdCollapseDirection.wdCollapseEnd);
                    endRange = tempRange2;
                }
            }

            return endRange;
        }

        private Shape AddNonDiagonalWatermark(Range anchor, HeaderFooter headerFooter, string watermarkText, WatermarkLocation watermarkLocation)
        {
            Shape shape = headerFooter.Shapes.AddTextbox(Microsoft.Office.Core.MsoTextOrientation.msoTextOrientationHorizontal, 0, 0, 240, 36, anchor);
            shape.TextFrame.TextRange.Text = watermarkText;
            shape.TextFrame.TextRange.Paragraphs.set_Style(GetOrCreateNonDiagonalWatermarkStyle().NameLocal);
            PositionNonDiagonalStamp(shape, watermarkLocation);
            if (watermarkLocation == WatermarkLocation.TOP_LEFT)
            {
                shape.TextFrame.TextRange.Paragraphs.Format.Alignment = WdParagraphAlignment.wdAlignParagraphLeft;
            }
            else if (watermarkLocation == WatermarkLocation.TOP_RIGHT)
            {
                shape.TextFrame.TextRange.Paragraphs.Format.Alignment = WdParagraphAlignment.wdAlignParagraphRight;
            }
            else
            {
                shape.TextFrame.TextRange.Paragraphs.Format.Alignment = WdParagraphAlignment.wdAlignParagraphCenter;
            }

            var textFrame = shape.TextFrame;
            while (textFrame.Overflowing && textFrame.TextRange.Font.Size > 2)
            {
                textFrame.TextRange.Font.Size = (float)(textFrame.TextRange.Font.Size - 0.5);
                shape.TextFrame.TextRange.Text = watermarkText;
            }
            return shape;
        }

        private void PositionNonDiagonalStamp(Shape shape, WatermarkLocation watermarkLocation)
        {
            int shapeHeight = 40;
            if (watermarkLocation == WatermarkLocation.LEFT)
            {
                shape.Rotation = -90;

                shape.Height = shapeHeight;
                if (shape.Rotation == 0)
                {
                    shape.Rotation = -90;
                }
            }
            else if (watermarkLocation == WatermarkLocation.RIGHT)
            {
                shape.Rotation = 90;

                shape.Height = shapeHeight;
                if (shape.Rotation == 0)
                {
                    shape.Rotation = 90;
                }
            }
            else
            {
                shape.Height = shapeHeight;
            }

            Section section = shape.Anchor.Sections.First;

            float height = section.PageSetup.PageHeight - section.PageSetup.TopMargin - section.PageSetup.BottomMargin;
            float width = section.PageSetup.PageWidth - section.PageSetup.LeftMargin - section.PageSetup.RightMargin;

            if (watermarkLocation == WatermarkLocation.LEFT || watermarkLocation == WatermarkLocation.RIGHT)
            {
                shape.Width = height;
            }
            else
            {
                shape.Width = width;
            }

            switch (watermarkLocation)
            {
                case WatermarkLocation.LEFT:
                    shape.RelativeHorizontalPosition = Word.WdRelativeHorizontalPosition.wdRelativeHorizontalPositionLeftMarginArea;
                    shape.Left = (float)Word.WdShapePosition.wdShapeCenter;
                    shape.RelativeVerticalPosition = Word.WdRelativeVerticalPosition.wdRelativeVerticalPositionPage;
                    shape.Top = (float)Word.WdShapePosition.wdShapeCenter;
                    break;
                case WatermarkLocation.RIGHT:
                    shape.RelativeHorizontalPosition = Word.WdRelativeHorizontalPosition.wdRelativeHorizontalPositionRightMarginArea;
                    shape.Left = (float)Word.WdShapePosition.wdShapeCenter;
                    shape.RelativeVerticalPosition = Word.WdRelativeVerticalPosition.wdRelativeVerticalPositionPage;
                    shape.Top = (float)Word.WdShapePosition.wdShapeCenter;
                    break;
                case WatermarkLocation.TOP_LEFT:
                    shape.RelativeHorizontalPosition = Word.WdRelativeHorizontalPosition.wdRelativeHorizontalPositionPage;
                    shape.Left = 50;
                    shape.RelativeVerticalPosition = Word.WdRelativeVerticalPosition.wdRelativeVerticalPositionTopMarginArea;
                    shape.Top = (float)Word.WdShapePosition.wdShapeBottom;
                    break;
                case WatermarkLocation.TOP_RIGHT:
                    shape.RelativeHorizontalPosition = Word.WdRelativeHorizontalPosition.wdRelativeHorizontalPositionPage;
                    shape.Left = (float)section.PageSetup.LeftMargin + section.PageSetup.RightMargin - 50;
                    shape.RelativeVerticalPosition = Word.WdRelativeVerticalPosition.wdRelativeVerticalPositionTopMarginArea;
                    shape.Top = (float)Word.WdShapePosition.wdShapeBottom;
                    break;
            }




            shape.Line.Visible = Microsoft.Office.Core.MsoTriState.msoFalse;
        }

        private Shape AddDiagonalWatermark(Range anchor, HeaderFooter headerFooter, string watermarkText)
        {
            Shape shape = headerFooter.Shapes.AddTextbox(Microsoft.Office.Core.MsoTextOrientation.msoTextOrientationHorizontal, 0, 0, 240, 48, anchor);
            shape.TextFrame.TextRange.Text = watermarkText;
            shape.TextFrame.TextRange.Paragraphs.set_Style(GetOrCreateWatermarkStyle().NameLocal);
            PositionDiagonalStamp(shape);
            var textFrame = shape.TextFrame;
            while (textFrame.Overflowing && textFrame.TextRange.Font.Size > 2)
            {
                textFrame.TextRange.Font.Size = (float)(textFrame.TextRange.Font.Size - 0.5);
                shape.TextFrame.TextRange.Text = watermarkText;
            }
            return shape;
        }

        private Word.Style GetOrCreateWatermarkStyle()
        {
            Word.Style style = null;
            try
            {
                style = ThisAddIn.Instance.Application.ActiveDocument.Styles["Diagonal Watermark"];
            }
            catch
            {
                style = ThisAddIn.Instance.Application.ActiveDocument.Styles.Add("Diagonal Watermark", Word.WdStyleType.wdStyleTypeParagraphOnly);

                style.set_BaseStyle("");
                style.Font.Name = "Arial";
                style.ParagraphFormat.SpaceBefore = 0;
                style.Font.Size = 90;
                style.Font.Scaling = 80;
                style.Font.Bold = -1;
                style.Font.Color = WdColor.wdColorGray25;
                style.Font.AllCaps = -1;
                style.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphCenter;
                style.ParagraphFormat.SpaceAfter = 0;
            }

            return style;
        }

        private Word.Style GetOrCreateNonDiagonalWatermarkStyle()
        {
            Word.Style style = null;
            try
            {
                style = ThisAddIn.Instance.Application.ActiveDocument.Styles["Watermark"];
            }
            catch
            {
                style = ThisAddIn.Instance.Application.ActiveDocument.Styles.Add("Watermark", Word.WdStyleType.wdStyleTypeParagraphOnly);

                style.set_BaseStyle("");
                style.ParagraphFormat.SpaceBefore = 0;
                style.Font.Name = "Arial";
                style.Font.Size = 27.5F;
                style.Font.Bold = -1;
                style.Font.Color = WdColor.wdColorGray25;
                style.Font.AllCaps = -1;
                style.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphCenter;
                style.ParagraphFormat.SpaceAfter = 0;
            }

            return style;
        }

        private void PositionDiagonalStamp(Word.Shape shape)
        {
            shape.Rotation = -45;

            shape.Height = 96;
            if (shape.Rotation == 0)
            {
                shape.Rotation = -45;
            }

            Section section = shape.Anchor.Sections.First;

            float height = section.PageSetup.PageHeight - section.PageSetup.TopMargin - section.PageSetup.BottomMargin;
            float width = section.PageSetup.PageWidth - section.PageSetup.LeftMargin - section.PageSetup.RightMargin;

            int diagonal = (int)(Math.Sqrt(height * height + width * width));

            shape.Width = diagonal;
            shape.RelativeHorizontalPosition = Word.WdRelativeHorizontalPosition.wdRelativeHorizontalPositionPage;
            shape.Left = (float)Word.WdShapePosition.wdShapeCenter;
            shape.RelativeVerticalPosition = Word.WdRelativeVerticalPosition.wdRelativeVerticalPositionPage;
            shape.Top = (float)Word.WdShapePosition.wdShapeCenter;
            shape.Line.Visible = Microsoft.Office.Core.MsoTriState.msoFalse;
        }

        private void RemoveWatermark()
        {
            using (new DocumentUndoTransaction("Watermark removal"))
            {
                //legacy watermark removal
                if (legacyWatermark != null && legacyWatermark.HasLegacyWatermark && legacyWatermark.Watermarks != null && legacyWatermark.Watermarks.Count > 0)
                {
                    legacyWatermark.RemoveMetadata();
                    legacyWatermark.DeleteShapes();
                }

                List<ContentControl> controlsToRemove = FindWatermarks();

                foreach (ContentControl control in controlsToRemove)
                {
                    /*
                     * Simply removing a control doesn't remove its content
                     * https://social.msdn.microsoft.com/Forums/en-US/8d6060c6-7b46-45b9-811e-b64f8dad83f0/word-deleting-a-content-control-leaves-an-extra-paragraph-mark?forum=vsto
                     */

                    // Works when the content is set via control.Range.Text
                    //control.Range.Delete();

                    // Works every time, even for data inserted via control.Range.InnerXML
                    control.Delete(true);
                }
            }
        }

        private List<ContentControl> FindWatermarks()
        {
            var controlsToRemove = new List<ContentControl>();

            List<ContentControl> controls = GetHeaderContentControls(WdHeaderFooterIndex.wdHeaderFooterPrimary);
            controls.AddRange(GetHeaderContentControls(WdHeaderFooterIndex.wdHeaderFooterFirstPage));
            controls.AddRange(GetHeaderContentControls(WdHeaderFooterIndex.wdHeaderFooterEvenPages));

            foreach (ContentControl control in controls)
            {
                if (control.Tag == WATERMARK_CONTROL_TAG)
                {
                    controlsToRemove.Add(control);
                }
            }

            return controlsToRemove;
        }

        private List<ContentControl> GetHeaderContentControls(WdHeaderFooterIndex headerIndex)
        {
            var controls = new List<ContentControl>();

            ThisAddIn.Instance.Application.ScreenUpdating = false;
            if (ThisAddIn.Instance.IsFunctionForLogging("ScreenUpdating"))
                Logger.Log("Disabled Screen Updating (InfowareWatermarksWindow.xaml.cs > GetHeaderContentControls)");
            foreach (Section section in GetDocumentSections())
            {
                if (Utils.IsSectionContinous(section, nativeDocument))
                {
                    continue;
                }

                HeaderFooter header = section.Headers[headerIndex];
                if (!header.LinkToPrevious)
                {
                    foreach (ContentControl control in header.Range.ContentControls)
                    {
                        controls.Add(control);
                    }
                }
            }
            ThisAddIn.Instance.Application.ScreenUpdating = true;
            if (ThisAddIn.Instance.IsFunctionForLogging("ScreenUpdating"))
                Logger.Log("Enabled Screen Updating (InfowareWatermarksWindow.xaml.cs > GetHeaderContentControls)");

            return controls;
        }

        private string GetNewWatermarkControlName()
        {
            return string.Format(WATERMARK_CONTROL_NAME_TEMPLATE, Guid.NewGuid().ToString());
        }

        private Sections GetDocumentSections()
        {
            return Globals.ThisAddIn.Application.ActiveDocument.Sections;
        }

        private enum WatermarkLocation
        {
            TOP_LEFT,
            TOP_RIGHT,
            LEFT,
            RIGHT,
            DIAGONAL
        }
    }
}
