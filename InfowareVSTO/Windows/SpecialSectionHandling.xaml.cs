﻿using InfowareVSTO.Common;
using Microsoft.Office.Interop.Word;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Section = Microsoft.Office.Interop.Word.Section;

namespace InfowareVSTO.Windows
{
    /// <summary>
    /// Interaction logic for SpecialSectionHandling.xaml
    /// </summary>
    public partial class SpecialSectionHandling : InfowareWindow
    {
        public List<string> standardPaperTypes = new List<string>()
        {
            "plain",
            "letterhead",
            "2ndpageletterhead",
            "envelope",
            "label",
            "filecopy",
            "a4",
            "legal"
        };
        private Document document;

        private SpecialHandlingSetting documentSetting;
        private SpecialHandlingSetting sectionSetting;


        public SpecialSectionHandling()
        {
            InitializeComponent();
            document = ThisAddIn.Instance.Application.ActiveDocument;
            PopulatePaperTypesComboBox();
            PopulateLetterheadComboBox();
            sectionSetting = ReadCurrentSectionSettings(ThisAddIn.Instance.Application.Selection.Range.Sections.First);
            ReadDocumentSettings();
            SetPaperTypeAndLetterhead();
        }

        private void SetPaperTypeAndLetterhead()
        {
            SpecialHandlingSetting toUse = sectionSetting ?? documentSetting;

            this.PaperTypeCB.SelectedItem = toUse.PaperType;
            this.LetterheadCB.SelectedValue = toUse.TemplateId;
        }

        private void ReadDocumentSettings()
        {
            string papertype = Utils.GetDocumentVariable(ThisAddIn.Instance.Application.ActiveDocument, "PaperType");
            int? letterheadIdValue = null;

            string letterheadStr = Utils.GetDocumentVariable(ThisAddIn.Instance.Application.ActiveDocument, "LetterheadTemplateId");
            if (int.TryParse(letterheadStr, out int letterHeadId))
            {
                letterheadIdValue = letterHeadId;
            }

            documentSetting = new SpecialHandlingSetting()
            {
                PaperType = papertype,
                TemplateId = letterheadIdValue
            };
        }

        public static SpecialHandlingSetting ReadCurrentSectionSettings(Section section)
        {
            var bookmark = section.Range.Bookmarks.Cast<Bookmark>().Where(x => x.Name.StartsWith(CustomConstants.SPECIAL_HANDLING_BOOKMARK)).FirstOrDefault();

            if (bookmark != null)
            {
                int index = bookmark.Name.IndexOf(CustomConstants.SPECIAL_HANDLING_LETTERHEAD);

                int templateId = 0;
                string pageType = null;

                if (index > 0)
                {
                    pageType = bookmark.Name.Substring(CustomConstants.SPECIAL_HANDLING_BOOKMARK.Length, index - CustomConstants.SPECIAL_HANDLING_BOOKMARK.Length);

                    string letterheadStr = bookmark.Name.Substring(index + CustomConstants.SPECIAL_HANDLING_LETTERHEAD.Length);
                    int.TryParse(letterheadStr, out templateId);
                }
                else
                {
                    pageType = bookmark.Name.Substring(CustomConstants.SPECIAL_HANDLING_BOOKMARK.Length);
                }

                return new SpecialHandlingSetting()
                {
                    PaperType = pageType,
                    TemplateId = templateId
                };
            }

            return null;
        }

        private void PopulateLetterheadComboBox()
        {   
            var templateList = AdminPanelWebApi.GetTemplateList(Common.TemplateType.Letterhead);
            this.LetterheadCB.ItemsSource = templateList;
       
            this.LetterheadCB.SelectedValuePath = "Id";
            this.LetterheadCB.DisplayMemberPath = "Name";
            this.LetterheadCB.SelectedIndex = 0;
        }

        private void PopulatePaperTypesComboBox()
        {
            List<string> itemSource = new List<string>(standardPaperTypes);
            var specialPapersTypes = SettingsManager.GetSetting("SpecialPaperTypes", "MultiPrint");
            if (!string.IsNullOrWhiteSpace(specialPapersTypes))
            {
                string[] specialPapersTypesArr = specialPapersTypes.Split(',').Select(x => x.Trim()).ToArray();

                itemSource.AddRange(specialPapersTypesArr);
            }

            this.PaperTypeCB.ItemsSource = itemSource;
            this.PaperTypeCB.SelectedIndex = 0;
        }

        private void Ok_Click(object sender, RoutedEventArgs e)
        {
            if (RemoveForWholeDocument.IsChecked == true)
            {
                DoRemoveForWholeDocument();
            }
            else if (RemoveForCurrentSection.IsChecked == true)
            {
                DoRemoveForCurrentSection();
            }
            else if (WholeDocument.IsChecked == true)
            {
                DoRemoveForWholeDocument();

                Utils.SetDocumentVariable(document, "PaperType", this.PaperTypeCB.Text);

                if (LetterheadCB.IsEnabled && LetterheadCB.SelectedValue is int)
                {
                    Utils.SetDocumentVariable(document, "LetterheadTemplateId", this.LetterheadCB.SelectedValue.ToString());
                }
            }
            else
            {
                DoRemoveForCurrentSection();

                var section = ThisAddIn.Instance.Application.Selection.Range.Sections.First;

                var range = section.Range;
                range.Collapse(WdCollapseDirection.wdCollapseStart);


                string name = GetBookmarkName();

                document.Bookmarks.Add(name, range);
            }

            this.Close();
        }

        private string GetBookmarkName()
        {
            string result = CustomConstants.SPECIAL_HANDLING_BOOKMARK + this.PaperTypeCB.Text;
            
            if (LetterheadCB.IsEnabled && LetterheadCB.SelectedValue is int)
            {
                result += CustomConstants.SPECIAL_HANDLING_LETTERHEAD + (int)LetterheadCB.SelectedValue;
            }

            return result;
        }

        private void DoRemoveForCurrentSection()
        {
            var section = ThisAddIn.Instance.Application.Selection.Range.Sections.First;

            RemoveForSection(section);
        }

        private void RemoveForSection(Microsoft.Office.Interop.Word.Section section)
        {
            var toDelete = section.Range.Bookmarks.Cast<Bookmark>().Where(x => x.Name.StartsWith(CustomConstants.SPECIAL_HANDLING_BOOKMARK)).ToList();

            foreach(var bookmark in toDelete)
            {
                bookmark.Delete();
            }
        }

        private void DoRemoveForWholeDocument()
        {
            foreach(Section section in document.Sections)
            {
                RemoveForSection(section);
            }

            Utils.RemoveDocumentVariable(document, "PaperType");
            
            Utils.RemoveDocumentVariable(document, "LetterheadTemplateId");
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }

    public class SpecialHandlingSetting
    {
        public string PaperType { get; set; }
        public int? TemplateId { get; set; }
    }
}
