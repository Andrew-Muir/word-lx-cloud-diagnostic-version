﻿using InfowareVSTO.Common;
using InfowareVSTO.Common.Language;
using InfowareVSTO.DataSource;
using InfowareVSTO.Windows.ParagraphNumbering.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace InfowareVSTO.Windows
{
    /// <summary>
    /// Interaction logic for DataSourceWindow.xaml
    /// </summary>
    public partial class DataSourceWindow : InfowareWindow
    {
        ICollectionView view;
        private List<Matter> matters;
        public bool CanOpen { get; set; } = true;

        public DataSourceWindow()
        {
            InitializeComponent();
            if (DataSourceManager.DataSourceType == DataSourceType.None)
            {
                CanOpen = false;
            }
            else
            {
                RefreshMessage();
            }
        }

        private void RefreshMessage()
        {
            var matter = DataSourceManager.LoadedMatter;
            if (matter == null)
            {
                this.Message.Text = LanguageManager.GetTranslation(LanguageConstants.NoMatterLoaded, "No matter loaded.");
                this.Unload.IsEnabled = false;
            }
            else
            {
                this.Message.Text = LanguageManager.GetTranslation(LanguageConstants.LoadedMatter, "Loaded matter") + $": {matter.Name}({matter.Code})";
                this.Unload.IsEnabled = true;
            }
        }

        private void CancelBtn_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Load_Click(object sender, RoutedEventArgs e)
        {
            if (this.MatterGrid.SelectedItem != null)
            {
                DataSourceManager.LoadedMatter = this.MatterGrid.SelectedItem as Matter;
                ThisAddIn.Instance.Ribbon.Invalidate();
            }
            RefreshMessage();
        }

        private void Unload_Click(object sender, RoutedEventArgs e)
        {
            DataSourceManager.LoadedMatter = null;
            RefreshMessage();
            ThisAddIn.Instance.Ribbon.Invalidate();
        }

        private void DataGridRow_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Load_Click(null, null);
        }

        private void MatterGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.MatterGrid.SelectedItem != null)
            {
                this.Description.Text = (MatterGrid.SelectedItem as Matter).Description;
            }
            else
            {
                this.Description.Text = string.Empty;
            }
        }

        private void SearchBtn_Click(object sender, RoutedEventArgs e)
        {
            string filter = this.SearchBox.Text;

            if (!this.SearchBox.ShowingPlaceholder && !string.IsNullOrWhiteSpace(filter))
            {
                using (new LoadingWindowWrapper())
                {
                    matters = DataSourceManager.LoadMatters(filter);
                }

                this.MatterGrid.ItemsSource = matters;
            }
        }

        private void SearchBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                SearchBtn_Click(null, null);
            }
        }
    }
}
