﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfowareVSTO.Windows.ParagraphNumbering.Interfaces
{
    interface MainAdvancedOptionsInterface
    {
        ParagraphNumberingScheme SelectedParagraphNumberingScheme { get; set; }
        ParagraphNumberingLevel SelectedParagraphNumberingLevel { get; set; }
    }
}
