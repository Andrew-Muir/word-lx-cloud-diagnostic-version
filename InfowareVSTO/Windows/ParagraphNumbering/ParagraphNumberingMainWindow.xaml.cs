﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;
using InfowareVSTO.Util;
using InfowareVSTO.Windows.ParagraphNumbering.Model.Enums;
using InfowareVSTO.Windows.ParagraphNumbering.Repositories;
using InfowareVSTO.Windows.ParagraphNumbering.Util;
using Microsoft.Office.Interop.Word;
using Application = Microsoft.Office.Interop.Word.Application;
using Border = Microsoft.Office.Interop.Word.Border;
using Paragraph = System.Windows.Documents.Paragraph;
using Style = Microsoft.Office.Interop.Word.Style;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Diagnostics;
using InfowareVSTO.Windows.ParagraphNumbering.Interfaces;
using System.Text.RegularExpressions;
using InfowareVSTO.Common;
using InfowareVSTO.Common.Language;
using DocumentFormat.OpenXml.Drawing;
using System.Xml.Serialization;
using System.Web.UI.WebControls.WebParts;

namespace InfowareVSTO.Windows.ParagraphNumbering
{
    /// <summary>
    /// Interaction logic for ParagraphNumberingMainWindow.xaml
    /// </summary>
    public partial class ParagraphNumberingMainWindow : InfowareWindow, INotifyPropertyChanged, MainAdvancedOptionsInterface
    {
        private readonly Application app;

        private readonly Microsoft.Office.Interop.Word.Document nativeDocument;

        private readonly bool skipToAdvancedOptionsWindow;

        public static ParagraphNumberingSchemeManager ParagraphNumberingSchemeManager => ParagraphNumberingSchemeManager.Instance;

        private ParagraphNumberingScheme selectedParagraphNumberingScheme;

        public static LoadingWindowWrapper OpenWhenBackgroundWorkerFinishes = null;

        private KeyValuePair<ParagraphNumberingScheme, ListTemplate>? reupdateListLevels = null;

        public ParagraphNumberingScheme SelectedParagraphNumberingScheme
        {
            get => selectedParagraphNumberingScheme;

            set
            {
                selectedParagraphNumberingScheme = value;
                OnPropertyChanged();
            }
        }

        private int selectedParagraphIndex;

        private ParagraphNumberingLevel selectedParagraphNumberingLevel;

        public ParagraphNumberingLevel SelectedParagraphNumberingLevel
        {
            get => selectedParagraphNumberingLevel;

            set
            {
                selectedParagraphNumberingLevel = value;
                OnPropertyChanged();
            }
        }

        private bool isSelectedParagraphNumberingSchemeReadOnly;

        public bool IsSelectedParagraphNumberingSchemeReadOnly
        {
            get => isSelectedParagraphNumberingSchemeReadOnly;

            set
            {
                isSelectedParagraphNumberingSchemeReadOnly = value;
                OnPropertyChanged();
            }
        }

        private bool isSelectedNumberingSchemeUsedInSelection;

        public bool IsSelectedNumberingSchemeUsedInSelection
        {
            get => isSelectedNumberingSchemeUsedInSelection;

            set
            {
                isSelectedNumberingSchemeUsedInSelection = value;
                OnPropertyChanged();
            }
        }

        private bool isListTemplateUsedInDocumentSelection;

        public bool IsListTemplateUsedInDocumentSelection
        {
            get => isListTemplateUsedInDocumentSelection;

            set
            {
                isListTemplateUsedInDocumentSelection = value;
                OnPropertyChanged();
            }
        }

		private bool wasParagraphNumberingToolbarManagerVisible { get; set; }

		public event PropertyChangedEventHandler PropertyChanged;

        public ParagraphNumberingMainWindow(bool skipToAdvancedOptionsWindow = false)
        {
            InitializeComponent();

            Loaded += OnWindowLoaded;
            Closed += ParagraphNumberingMainWindow_Closed;

            app = Globals.ThisAddIn.Application;

            nativeDocument = app.ActiveDocument;

            //Range selectionRange = app.Selection.Range.Duplicate;
            //selectionRange.Collapse();
            //selectionRange.Select();

            ParagraphNumberingSettingRepository.Instance.Restore();

            InitUi();

            UpdateIsAppliedPropertyOfActiveDocumentListsTemplates();

            DataContext = this;

            this.skipToAdvancedOptionsWindow = skipToAdvancedOptionsWindow;
        }

        private void ParagraphNumberingMainWindow_Closed(object sender, EventArgs e)
        {
            if (ThisAddIn.FirmListDocument != null)
            {
                try
                {
                    var document = ThisAddIn.Instance.Application.ActiveDocument;
                    if (!ThisAddIn.FirmListDocument.Saved) { ThisAddIn.FirmListDocument.Saved = true; }
                    ThisAddIn.FirmListDocument.Close(WdSaveOptions.wdDoNotSaveChanges);
                    ThisAddIn.FirmListDocument = null;
                    if (document != null)
                    {
                        document.Activate();
                    }
                }
                catch (Exception ex)
                {
                }
            }
        }

        private void InitUi()
        {
            using (new LoadingWindowWrapper())
            {
                SetParagraphNumberingLevelPropertyChangeHandler();
                ClearParagraphNumberingSchemeSelection();

                ParagraphNumberingSchemeManager.LoadSynchedParagraphNumberingSchemes();
                ParagraphNumberingSchemeManager.SyncWithListTemplatesFromFirmDocument();
                ParagraphNumberingSchemeManager.SyncWithListTemplatesFromDocument();

         
            }
        }

        private void SetStartingLocation()
        {
            TreeViewStartLocation startLocation = ParagraphNumberingSettingRepository.Instance.TreeViewStartLocation;
            bool useActiveDocument = ParagraphNumberingSettingRepository.Instance.UseActiveDocumentSchemesForStartLocation;

            if (useActiveDocument && ParagraphNumberingSchemeManager.Instance.ActiveDocumentParagraphNumberingSchemes.Count > 0)
            {
                SelectFistItemOfCategory(ParagraphNumberingSchemeManager.Instance.ActiveDocumentParagraphNumberingSchemeType);
                return;
            }

            switch (startLocation)
            {
                case TreeViewStartLocation.FAVOURITE:
                    SelectFistItemOfCategory(ParagraphNumberingSchemeManager.Instance.FavoritesParagraphNumberingSchemeType);
                    break;
                case TreeViewStartLocation.FIRM:
                    SelectFistItemOfCategory(ParagraphNumberingSchemeManager.Instance.FirmDocumentParagraphNumberingSchemeType);
                    break;
                case TreeViewStartLocation.LAST_USED:
                    var scheme = ParagraphNumberingSchemeManager.GetLastUsedParagraphNumberingScheme();
                    SelectScheme(scheme);
                    break;
                case TreeViewStartLocation.SYNCED:
                    SelectFistItemOfCategory(ParagraphNumberingSchemeManager.Instance.SyncedParagraphNumberingSchemeType);
                    break;
            }
        }

        private void SelectScheme(KeyValuePair<List<ParagraphNumberingSchemeCategory>, ParagraphNumberingScheme> scheme)
        {
            if (scheme.Key != null)
            {
                TreeViewItem tvi2 = null;
                foreach(ParagraphNumberingSchemeCategory cateogry in scheme.Key)
                {
                    tvi2 = FindTreeViewChild((ItemsControl)tvi2 ?? TreeNumberingSchemes, cateogry);

                    tvi2.IsExpanded = true;
                }

                var tvi3 = FindTreeViewChild(tvi2, scheme.Value);
                tvi3.IsSelected = true;
                SetSelectedParagraphNumberingScheme(scheme.Value);
            }
            else if (scheme.Value != null)
            {
                var tvi2 = FindTreeViewChild(TreeNumberingSchemes, scheme.Value);
                tvi2.IsSelected = true;
                SetSelectedParagraphNumberingScheme(scheme.Value);
            }
        }

        private TreeViewItem FindTreeViewChild(ItemsControl treeviewItem, object item)
        {
            var tvi = treeviewItem.ItemContainerGenerator.ContainerFromItem(item) as TreeViewItem;
            if (tvi != null)
            {
                return tvi;
            } 

            foreach (var obj in treeviewItem.Items)
            {
                var tvi2 = treeviewItem.ItemContainerGenerator.ContainerFromItem(obj) as TreeViewItem;

                if (tvi2 != null)
                {
                    var tvi3 = FindTreeViewChild(tvi2, item);
                    if(tvi3 != null)
                    {
                        return tvi3;
                    }
                }
            }

            return null;
        }

        private void SelectFistItemOfCategory(ParagraphNumberingSchemeType item)
        {
            var tvi = TreeNumberingSchemes.ItemContainerGenerator.ContainerFromItem(item)
          as TreeViewItem;

            if (tvi != null && tvi.Items.Count > 0)
            {
                // find first scheme

                var firstScheme = FindFirstScheme(tvi.ItemsSource as IList<object>);

                if (firstScheme.Key != null)
                {
                    TreeViewItem tvi2 = null;
                    foreach (ParagraphNumberingSchemeCategory category in firstScheme.Key)
                    {
                        tvi2 = (tvi2 ?? tvi).ItemContainerGenerator.ContainerFromItem(category) as TreeViewItem;

                        tvi2.IsExpanded = true;
                    }

                    var tvi3 = tvi2.ItemContainerGenerator.ContainerFromItem(firstScheme.Value) as TreeViewItem;
                    tvi3.IsSelected = true;
                    SetSelectedParagraphNumberingScheme(firstScheme.Value);
                }
                else if (firstScheme.Value != null)
                {
                    var tvi2 = tvi.ItemContainerGenerator.ContainerFromItem(firstScheme.Value) as TreeViewItem;
                    tvi2.IsSelected = true;
                    SetSelectedParagraphNumberingScheme(firstScheme.Value);
                }
            }
        }

        private KeyValuePair<List<ParagraphNumberingSchemeCategory>, ParagraphNumberingScheme> FindFirstScheme(IList<object> list)
        {
            foreach (var schemeOrCategory in list)
            {
                if (schemeOrCategory is ParagraphNumberingSchemeCategory)
                {
                    var partialResult = FindFirstSchemeFromCategory(schemeOrCategory as ParagraphNumberingSchemeCategory);
                    if (partialResult != null)
                    {
                        List<ParagraphNumberingSchemeCategory> categories = new List<ParagraphNumberingSchemeCategory>();
                        categories.AddRange(partialResult.Value.Key);
                        return new KeyValuePair<List<ParagraphNumberingSchemeCategory>, ParagraphNumberingScheme>(categories, partialResult.Value.Value);
                    }
                }
                else if (schemeOrCategory is ParagraphNumberingScheme)
                {
                    return new KeyValuePair<List<ParagraphNumberingSchemeCategory>, ParagraphNumberingScheme>(null, schemeOrCategory as ParagraphNumberingScheme);
                }
            }

            return new KeyValuePair<List<ParagraphNumberingSchemeCategory>, ParagraphNumberingScheme>(null, null);
        }

        private KeyValuePair<List<ParagraphNumberingSchemeCategory>, ParagraphNumberingScheme>? FindFirstSchemeFromCategory(ParagraphNumberingSchemeCategory category)
        {
            if (category.ParagraphNumberingSchemes.Count > 0)
            {
                return new KeyValuePair<List<ParagraphNumberingSchemeCategory>, ParagraphNumberingScheme>(new List<ParagraphNumberingSchemeCategory>() { category }, category.ParagraphNumberingSchemes.First());
            }
            else if (category.NestedParagraphNumberingSchemeCategory.Count > 0)
            {
                foreach (ParagraphNumberingSchemeCategory subCategory in category.NestedParagraphNumberingSchemeCategory)
                {
                    var partialResult = FindFirstSchemeFromCategory(subCategory);
                    if(partialResult != null)
                    {
                        List<ParagraphNumberingSchemeCategory> categories = new List<ParagraphNumberingSchemeCategory>() { category };
                        categories.AddRange(partialResult.Value.Key);
                        return new KeyValuePair<List<ParagraphNumberingSchemeCategory>, ParagraphNumberingScheme>(categories, partialResult.Value.Value);
                    }
                }
            }

            return null;
        }

        private void UpdateIsAppliedPropertyOfActiveDocumentListsTemplates()
        {
            var usedNumberingSchemes = new HashSet<ParagraphNumberingScheme>();

            foreach (List list in app.ActiveDocument.Lists)
            {
                string listTemplateName = ListTemplateUtil.GetNameOfListTemplateUsedInRange(list.Range);

                if (listTemplateName == null)
                {
                    // No extension generated list template can be found in the given list
                    continue;
                }

                foreach (ParagraphNumberingScheme numberingScheme in ParagraphNumberingSchemeManager.ActiveDocumentParagraphNumberingSchemeType.ParagraphNumberingSchemesOrCategories)
                {
                    if (listTemplateName == ListTemplateUtil.GetListTemplateName(numberingScheme))
                    {
                        usedNumberingSchemes.Add(numberingScheme);
                        break;
                    }
                }
            }

            foreach (ParagraphNumberingScheme numberingScheme in ParagraphNumberingSchemeManager.ActiveDocumentParagraphNumberingSchemeType.ParagraphNumberingSchemesOrCategories)
            {
                numberingScheme.IsApplied = usedNumberingSchemes.Contains(numberingScheme);
            }
        }

        private void OnWindowLoaded(object sender, RoutedEventArgs e)
        {
            if (skipToAdvancedOptionsWindow)
            {
                /*
                 * Hide this window, and open the Advanced options window for the ParagraphNumberingScheme corresponding
                 * to the list in selection
                 */

                if (ParagraphNumberingToolbarManager.Instance.windowIsShown)
                {                 
                    wasParagraphNumberingToolbarManagerVisible = true;
                    ParagraphNumberingToolbarManager.Instance.ToggleVisibility(true);
                }
                else
                    wasParagraphNumberingToolbarManagerVisible = false;

                Visibility = Visibility.Hidden;
                SkipToAdvancedOptionsWindow();
                Close();
                return;
            }
            SetStartingLocation();

            bool isListTemplateUsedInSelection = ListTemplateUtil.IsListTemplateUsedInSelection();

            if (isListTemplateUsedInSelection)
            {
                if (!DocumentUtil.IsZeroLengthSelection())
                {
                    Range firstListTemplateRange = DocumentUtil.GetFirstListInSelection().Range.Duplicate;
                    firstListTemplateRange.Collapse();
                    firstListTemplateRange.Select();
                }
            }

            // Handle TreeView selection

            if (ParagraphNumberingSettingRepository.Instance.UseActiveDocumentSchemesForStartLocation && isListTemplateUsedInSelection)
            {
                string usedListTemplateName = ListTemplateUtil.GetNameOfListTemplateUsedInSelection();

                SelectParagraphNumberingSchemeByName(usedListTemplateName);

                TreeViewItem treeViewItem = TreeNumberingSchemes.GetTreeViewItemFromChildObject(GetSelectedParagraphNumberingScheme());

                if (treeViewItem != null)
                {
                    treeViewItem.IsSelected = true;
                }
            }

            IsListTemplateUsedInDocumentSelection = isListTemplateUsedInSelection;

			if (ParagraphNumberingToolbarManager.Instance.windowIsShown)
			{
                ThisAddIn.Instance.UnwantedUnfocus = true;
                wasParagraphNumberingToolbarManagerVisible = true;
				ParagraphNumberingToolbarManager.Instance.ToggleVisibility(true);
				ThisAddIn.Instance.UnwantedUnfocus = false;
			}
			else
				wasParagraphNumberingToolbarManagerVisible = false;
		}

        private void SkipToAdvancedOptionsWindow()
        {
            if (ListTemplateUtil.IsListTemplateUsedInSelection())
            {
                if (!DocumentUtil.IsZeroLengthSelection())
                {
                    Range firstListTemplateRange = DocumentUtil.GetFirstListInSelection().Range.Duplicate;
                    firstListTemplateRange.Collapse();
                    firstListTemplateRange.Select();
                }

                string usedListTemplateName = ListTemplateUtil.GetNameOfListTemplateUsedInSelection();

                foreach (ParagraphNumberingScheme numberingScheme in GetAllParagraphNumberingSchemesInActiveDocument())
                {
                    if (ListTemplateUtil.GetListTemplateName(numberingScheme) == usedListTemplateName)
                    {
                        SetSelectedParagraphNumberingScheme(numberingScheme);
                        OpenAdvancedOptionsWindow(numberingScheme, true);
                        return;
                    }
                }
            }
            else
            {
                return;
            }
        }

        private void SelectParagraphNumberingSchemeByName(string usedListTemplateName)
        {
            foreach (ParagraphNumberingScheme numberingScheme in GetAllParagraphNumberingSchemesInActiveDocument())
            {
                if (ListTemplateUtil.GetListTemplateName(numberingScheme) == usedListTemplateName)
                {
                    SetSelectedParagraphNumberingScheme(numberingScheme);
                    return;
                }
            }
        }

        private void SetParagraphNumberingLevelPropertyChangeHandler()
        {
            foreach (ParagraphNumberingScheme paragraphNumberingScheme in GetAllParagraphNumberingSchemes())
            {
                SetParagraphNumberingLevelPropertyChangeHandler(paragraphNumberingScheme);
            }
        }

        private void SetParagraphNumberingLevelPropertyChangeHandler(ParagraphNumberingScheme paragraphNumberingScheme)
        {
            foreach (ParagraphNumberingLevel paragraphNumberingLevel in paragraphNumberingScheme.ParagraphNumberingLevels)
            {
                paragraphNumberingLevel.PropertyChanged += (object sender, PropertyChangedEventArgs e) => OnParagraphNumberingLevelPropertyChanged(sender, e, paragraphNumberingLevel);
                paragraphNumberingLevel.ParagraphFormat.PropertyChanged += (object sender, PropertyChangedEventArgs e) => OnParagraphNumberingLevelPropertyChanged(sender, e, paragraphNumberingLevel);
            }
        }

        private void OnParagraphNumberingLevelPropertyChanged(object sender, PropertyChangedEventArgs e, ParagraphNumberingLevel numberingLevel)
        {
            ParagraphUtil.PopulateParagraphNumberingLevels(LevelUserControl.FdParagraphNumberingLevels.Blocks,
                GetSelectedParagraphNumberingScheme(), OnParagraphClick, selectedParagraphIndex);
        }

        #region Event handlers

        private void OnTreeNumberingSchemesMouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            ClearParagraphNumberingSchemeSelection();
        }

        private void OnNumberingSchemeNameMouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            var textBlock = (TextBlock)sender;

            using (new LoadingWindowWrapper())
            {
                SetSelectedParagraphNumberingScheme((ParagraphNumberingScheme)textBlock.DataContext);
            }

            /**
             * This is required so that OnTreeNumberingSchemesMouseLeftButtonUp() is no longer called when a user 
             * presses on a ParagraphNumberingScheme TreeView node
             */
            e.Handled = true;
        }

        private void OnCbParagraphNumberingLevelsSelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            ParagraphNumberingScheme selectedNumberingScheme = GetSelectedParagraphNumberingScheme();

            if (selectedNumberingScheme == null)
            {
                // Callback called before the Paragraph numbering scheme had the chance to initialize
                return;
            }

            int selectedIndex = CbParagraphNumberingLevels.SelectedIndex;

            ParagraphUtil.MarkParagraphAsSelected(LevelUserControl.FdParagraphNumberingLevels.Blocks, selectedIndex);

            selectedParagraphIndex = selectedIndex;
            SelectedParagraphNumberingLevel = selectedNumberingScheme.ParagraphNumberingLevels[selectedIndex];
        }

        private void OnBtnAdvanceOptionsClick(object sender, RoutedEventArgs e)
        {
            OpenAdvancedOptionsWindow(GetSelectedParagraphNumberingScheme());
        }

        private void OpenAdvancedOptionsWindow(ParagraphNumberingScheme paragraphNumberingScheme, bool closeCurrentWindowOnAdvancedOptionsCancellation = false)
        {
            new ParagraphNumberingAdvancedOptions(paragraphNumberingScheme,
                () => AddAndApplyParagraphNumberingScheme(),
                () => ParagraphNumberingSchemeManager.LoadSynchedParagraphNumberingSchemes()).ShowDialog();

            if (closeCurrentWindowOnAdvancedOptionsCancellation)
            {
                Close();
            }

            // TODO: this code is duplicated a bunch of times, try to refactor
            ParagraphUtil.PopulateParagraphNumberingLevels(LevelUserControl.FdParagraphNumberingLevels.Blocks,
                paragraphNumberingScheme, OnParagraphClick, selectedParagraphIndex);

            // Necessary to update the IsBold, IsItalic ... buttons, of which properties depend on props from both the level and the level's paragraph format
            paragraphNumberingScheme.ParagraphNumberingLevels.ForEach(it => it.RaisePropertyChangedForComplexProps());
        }

        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

        private void OnParagraphClick(Paragraph selectedParagraph, ParagraphNumberingLevel numberingLevel)
        {
            ParagraphUtil.MarkParagraphAsSelected(LevelUserControl.FdParagraphNumberingLevels.Blocks, selectedParagraph);

            int selectedIndex = ParagraphUtil.GetParagraphIndex(LevelUserControl.FdParagraphNumberingLevels.Blocks, selectedParagraph);

            selectedParagraphIndex = selectedIndex;
            SelectedParagraphNumberingLevel = GetSelectedParagraphNumberingScheme().ParagraphNumberingLevels[selectedIndex];

            CbParagraphNumberingLevels.SelectedIndex = numberingLevel.Index - 1;
        }

        private ParagraphNumberingScheme GetSelectedParagraphNumberingScheme()
        {
            //return (ParagraphNumberingScheme) ListNumberingSchemes.SelectedItem;
            return SelectedParagraphNumberingScheme;
        }

        private void OnBtnAddClick(object sender, System.Windows.RoutedEventArgs e)
        {
            AddAndApplyParagraphNumberingSchemeUsingWrappers();
        }

        private void OnBtnAddAndApplySchemeClick(object sender, System.Windows.RoutedEventArgs e)
        {
            AddAndApplyParagraphNumberingSchemeUsingWrappers();
        }

        private void AddAndApplyParagraphNumberingSchemeUsingWrappers()
        {
            using (new LoadingWindowWrapper())
            {
                string listTemplateName = ListTemplateUtil.GetListTemplateName(GetSelectedParagraphNumberingScheme());

                using (new IntensiveDocumentInteractionWrapper(false, undoTransactionName: $"Added and applied {listTemplateName}"))
                {
                    StaticCustomUndoRecord.Start($"Added and applied {listTemplateName}");
                    AddAndApplyParagraphNumberingScheme();
                    ParagraphNumberingSchemeManager.RefreshActiveDocumentParagraphNumberingSchemes(GetSelectedParagraphNumberingScheme());
                    Globals.ThisAddIn.currentSelectedListTemplateName = listTemplateName;
                    Utils.ReplaceTcFieldsInDocument(ThisAddIn.Instance.Application.ActiveDocument);
                    StaticCustomUndoRecord.Stop(true);
                }
            }
        }

        private void OnBtnAddSchemeClick(object sender, System.Windows.RoutedEventArgs e)
        {
            using (new LoadingWindowWrapper())
            {
                string listTemplateName = ListTemplateUtil.GetListTemplateName(GetSelectedParagraphNumberingScheme());

                using (new IntensiveDocumentInteractionWrapper(undoTransactionName: $"Added {listTemplateName}"))
                {
                    CreateOrUpdateListTemplate();
                }
            }
        }

        private void OnBtnEditSchemeClick(object sender, System.Windows.RoutedEventArgs e)
        {
            OpenAdvancedOptionsWindow(GetSelectedParagraphNumberingScheme());
        }

        private void OnBtnSetSchemeAsFavoriteClick(object sender, System.Windows.RoutedEventArgs e)
        {
            ParagraphNumberingSchemeManager.SetFavoriteParagraphNumberingScheme(GetSelectedParagraphNumberingScheme());
        }

        private void OnBtnRemoveSchemeFromFavoritesClick(object sender, System.Windows.RoutedEventArgs e)
        {
            ParagraphNumberingSchemeManager.RemoveFavoriteParagraphNumberingScheme(GetSelectedParagraphNumberingScheme());
        }

        private void AddAndApplyParagraphNumberingScheme(bool closeWindow = true)
        {
            ParagraphNumberingScheme selectedNumberingScheme = GetSelectedParagraphNumberingScheme();

            ParagraphNumberingSchemeManager.SetLastUsedParagraphNumberingScheme(selectedNumberingScheme);
            if ((bool)CbExcludeAllLevelsFromToc.IsChecked)
            {
                foreach (ParagraphNumberingLevel numberingLevel in selectedNumberingScheme.ParagraphNumberingLevels)
                {
                    numberingLevel.ParagraphFormat.OutlineLevel = WdOutlineLevel.wdOutlineLevelBodyText;
                }
            }

            if (ListTemplateUtil.IsListTemplateUsedInSelection())
            {
                string usedListTemplateName = ListTemplateUtil.GetNameOfListTemplateUsedInSelection();

                if (usedListTemplateName == ListTemplateUtil.GetListTemplateName(selectedNumberingScheme))
                {
                    ReplaceListTemplate(selectedNumberingScheme, ListTemplateUtil.GetListTemplateByName(usedListTemplateName));
                }
                else
                {
                    string warningMessage = GetListTemplateInsertionWarningMessage(usedListTemplateName, selectedNumberingScheme.Name);

                    var confirmationWindow = new InfowareConfirmationWindow(warningMessage, "Infoware insert new numbering", MessageBoxButton.YesNoCancel);
                    confirmationWindow.Height = 340;
                    confirmationWindow.ShowDialog();

                    if (confirmationWindow.Result == MessageBoxResult.Cancel)
                    {
                        return;
                    }
                    else if (confirmationWindow.Result == MessageBoxResult.Yes)
                    {
                        ReplaceListTemplate(selectedNumberingScheme, ListTemplateUtil.GetListTemplateByName(usedListTemplateName));
                    }
                    else
                    {
                        if (IsSelectedParagraphNumberingSchemeReadOnly)
                        {
                            ParagraphNumberingSchemeManager.AddCustomParagraphNumberingSchemeToActiveDocumentSchemes(selectedNumberingScheme);
                        }

                        // Apply style on a new paragraph
                        app.Selection.Range.InsertParagraphAfter();
                        Range newParagraphRange = app.Selection.Paragraphs.Last.Range;
                        newParagraphRange.Select();

                        CreateOrUpdateListTemplate();

                        object firstLevelStyleName = ListTemplateUtil.GetParagraphStyleName(selectedNumberingScheme, 1);
                        newParagraphRange.set_Style(ref firstLevelStyleName);
                    }
                }
            }
            else
            {
                if (IsSelectedParagraphNumberingSchemeReadOnly)
                {
                    ParagraphNumberingSchemeManager.AddCustomParagraphNumberingSchemeToActiveDocumentSchemes(selectedNumberingScheme);
                }

                bool applyListTemplate = !ParagraphNumberingSettingRepository.Instance.DoNotApplyLevel1Style;

                /**
                 * The original implementation doesn't seem to apply the template at all when the value evals to true. 
                 * Anyway, we wouldn't be able to exclude from application just the first level (the selection is not
                 * a list, therefore, there's no structure)
                 */

                AddListTemplate(app.Selection.Range, applyListTemplate);
            }

            AddNewLinePrefixIfNecessary(selectedNumberingScheme);
            CheckReupdateParagraphStyles();

            if (closeWindow)
            {
                Close();
            }
        }

        private void OnBtnCancelClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void OnBtnUpdateClick(object sender, RoutedEventArgs e)
        {
            // Update every instance of the selected style

            //foreach (List list in app.ActiveDocument.Lists)
            //{
            //   if (list.Range.ListFormat.ListTemplate.Name == GetSelectedParagraphNumberingScheme().Name)
            //}

            using (new LoadingWindowWrapper())
            {
                using (new IntensiveDocumentInteractionWrapper(false, undoTransactionName: "List template update"))
                {
                    StaticCustomUndoRecord.Start("List template update");

                    ParagraphNumberingScheme selectedParagraphNumberingScheme = GetSelectedParagraphNumberingScheme();
                    ParagraphNumberingSchemeManager.SetLastUsedParagraphNumberingScheme(selectedParagraphNumberingScheme);

                    string listTemplateName = ListTemplateUtil.GetListTemplateName(selectedParagraphNumberingScheme);
                    ListTemplate listTemplate = ListTemplateUtil.GetListTemplateByName(listTemplateName);
                 

                    UpdateListTemplate(selectedParagraphNumberingScheme, listTemplate);
                    UpdateListTemplateParagraphStyles(selectedParagraphNumberingScheme, listTemplate);
                    ParagraphNumberingSchemeManager.RefreshActiveDocumentParagraphNumberingSchemes(GetSelectedParagraphNumberingScheme());
                    RemoveNewLinePrefixIfNecessary(selectedParagraphNumberingScheme);
                    AddNewLinePrefixIfNecessary(selectedParagraphNumberingScheme);
                    Utils.ReplaceTcFieldsInDocument(ThisAddIn.Instance.Application.ActiveDocument);

                    StaticCustomUndoRecord.Stop(true);
                }
            }

            Close();
        }

        private void OnBtnReplaceClick(object sender, RoutedEventArgs e)
        {
            using (new LoadingWindowWrapper())
            {
                using (new IntensiveDocumentInteractionWrapper(undoTransactionName: "List template replacement"))
                {
                    StaticCustomUndoRecord.Start("List template replacement");
                    // Replace every instance of the selected style
                    ReplaceListTemplate(GetSelectedParagraphNumberingScheme(), ListTemplateUtil.GetListTemplateByName(ListTemplateUtil.GetNameOfListTemplateUsedInSelection()));
                    ParagraphNumberingSchemeManager.RefreshActiveDocumentParagraphNumberingSchemes(GetSelectedParagraphNumberingScheme());
                    ParagraphNumberingSchemeManager.SetLastUsedParagraphNumberingScheme(GetSelectedParagraphNumberingScheme());
                    Utils.ReplaceTcFieldsInDocument(ThisAddIn.Instance.Application.ActiveDocument);

                    CheckReupdateParagraphStyles();

                    RemoveNewLinePrefixIfNecessary(GetSelectedParagraphNumberingScheme());
                    AddNewLinePrefixIfNecessary(GetSelectedParagraphNumberingScheme());
                    StaticCustomUndoRecord.Stop();
                }
            }

            Close();
        }

        private void OnOptionsButtonClick(object sender, RoutedEventArgs e)
        {
            new ParagraphNumberingMainOptionsWindow().ShowDialog();
        }

        private void OnNumberingSchemeNamePreviewMouseRightButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            //var textBlock = (TextBlock) sender;

            //var paragraphNumberingScheme = (ParagraphNumberingScheme) textBlock.DataContext;

            //e.Handled = ParagraphNumberingSchemeManager.IsReadOnlyParagraphNumberingScheme(paragraphNumberingScheme);

            var textBlock = (TextBlock)sender;

            var paragraphNumberingScheme = (ParagraphNumberingScheme)textBlock.DataContext;

            TreeViewItem treeViewItem = TreeNumberingSchemes.GetTreeViewItemFromChildObject(paragraphNumberingScheme);

            if (treeViewItem != null)
            {
                treeViewItem.IsSelected = true;
            }

            SetSelectedParagraphNumberingScheme(paragraphNumberingScheme);
        }

        private void OnDeleteActiveDocumentSchemeButtonClick(object sender, RoutedEventArgs e)
        {
            var menuItem = (MenuItem)sender;

            var paragraphNumberingScheme = (ParagraphNumberingScheme)menuItem.DataContext;

            var confirmationWindow = new InfowareConfirmationWindow("Confirm the removal of all styles in this numbering scheme from the active document.", "Infoware remove schemes styles from document");
            confirmationWindow.ShowDialog();

            if (confirmationWindow.Result == MessageBoxResult.Cancel)
            {
                return;
            }

            using (new LoadingWindowWrapper())
            {
                string listTemplateName = ListTemplateUtil.GetListTemplateName(paragraphNumberingScheme);

                using (new IntensiveDocumentInteractionWrapper(false, undoTransactionName: $"Deleted {listTemplateName}"))
                {
                    StaticCustomUndoRecord.Start($"Deleted {listTemplateName}");
                    ListTemplateUtil.DeleteListTemplateAndLinkedStyles(paragraphNumberingScheme);

                    ClearParagraphNumberingSchemeSelection();
                    IsListTemplateUsedInDocumentSelection = ListTemplateUtil.IsListTemplateUsedInSelection();

                    ParagraphNumberingSchemeManager.RemoveActiveDocumentParagraphNumberingScheme(paragraphNumberingScheme);
                    StaticCustomUndoRecord.Stop(true);
                }
            }
        }

        private void OnEditActiveDocumentSchemeButtonClick(object sender, RoutedEventArgs e)
        {
            var menuItem = (MenuItem)sender;
            OpenAdvancedOptionsWindow((ParagraphNumberingScheme)menuItem.DataContext);
        }

        private void OnMoveCursorToFirstParagraphInListClick(object sender, RoutedEventArgs e)
        {
            if (!ListTemplateUtil.IsListTemplateUsedInSelection())
            {
                return;
            }

            string listTemplateName = ListTemplateUtil.GetNameOfListTemplateUsedInSelection();

            Range firstListRange = ListTemplateUtil.GetFirstListUsingListTemplate(listTemplateName).Range.Duplicate;
            firstListRange.Collapse();
            firstListRange.Select();
        }

        private void AddListTemplate(Range destinationRange, bool apply = true)
        {
            ListTemplate listTemplate = CreateOrUpdateListTemplate();

            if (apply)
            {
                ApplyListTemplate(destinationRange, listTemplate);

            }
        }

        private void CheckReupdateParagraphStyles()
        {
            if (reupdateListLevels != null)
            {
                UpdateListTemplateParagraphStyles(reupdateListLevels.Value.Key, reupdateListLevels.Value.Value);
                reupdateListLevels = null;
            }
        }

        private ListTemplate CreateOrUpdateListTemplate()
        {
            return CreateOrUpdateListTemplate(GetSelectedParagraphNumberingScheme());
        }

        private ListTemplate CreateOrUpdateListTemplate(ParagraphNumberingScheme paragraphNumberingScheme)
        {
            bool isMultiLevel = paragraphNumberingScheme.ParagraphNumberingLevels.Count > 1;

            object listTemplateName = ListTemplateUtil.GetListTemplateName(paragraphNumberingScheme);

            object trueValue = true;
            object falseValue = false;

            ListTemplate customListTemplate = ListTemplateUtil.GetListTemplateByName(listTemplateName.ToString());

            if (customListTemplate == null)
            {
                if (isMultiLevel)
                    customListTemplate = nativeDocument.ListTemplates.Add(ref trueValue, ref listTemplateName);
                else
                    customListTemplate = nativeDocument.ListTemplates.Add(ref falseValue, ref listTemplateName);
                this.reupdateListLevels = new KeyValuePair<ParagraphNumberingScheme, ListTemplate>(paragraphNumberingScheme, customListTemplate);
            }

            UpdateListTemplate(paragraphNumberingScheme, customListTemplate);
            UpdateListTemplateParagraphStyles(paragraphNumberingScheme, customListTemplate);

            return customListTemplate;
        }

        private void ApplyListTemplate(Range destinationRange, ListTemplate customListTemplate)
        {
            object trueValue = true;
            object applyTo = WdListApplyTo.wdListApplyToWholeList;
            object listBehaviour = WdDefaultListBehavior.wdWord10ListBehavior;

            //app.Selection.Range.ListFormat.ApplyListTemplate(customListTemplate, ref falseValue, ref applyTo, ref listBehaviour);
            destinationRange.ListFormat.ApplyListTemplate(customListTemplate, ref trueValue, ref applyTo, ref listBehaviour);

            /*
             * 
             * Note, this shouldn't be necessary, because customListTemplate is already the right one and it's up to date, but for some reason, 
             * replacing the template (even with itself) seems to fix the indentation bug (+ special indent) that we were having
             */
            //ReplaceListTemplate(GetSelectedParagraphNumberingScheme(), customListTemplate);
        }

        private void ReplaceListTemplate(ParagraphNumberingScheme replacementParagraphNumberingScheme, ListTemplate listTemplate)
        {
            EnsureBaseStyleExist(replacementParagraphNumberingScheme);

            if (ParagraphNumberingSchemeManager.Instance?.ActiveDocumentParagraphNumberingSchemes != null && ParagraphNumberingSchemeManager.Instance.ActiveDocumentParagraphNumberingSchemes.Any(x => (x as ParagraphNumberingScheme).Name == replacementParagraphNumberingScheme.Name))
            {
                ReplaceListTemplateInDocument(replacementParagraphNumberingScheme, listTemplate);
            }
            else if (ParagraphNumberingSchemeManager.Instance?.FirmDocumentParagraphNumberingSchemes != null && ListTemplateUtil.CheckIfNumberingSchemeIsInFirmListDocument(ParagraphNumberingSchemeManager.Instance.FirmDocumentParagraphNumberingSchemes, replacementParagraphNumberingScheme.Name))
            {
                ReplaceListTemplateInFirmListDocument(replacementParagraphNumberingScheme, listTemplate);
            }
            else if (ParagraphNumberingSchemeManager.Instance?.SyncedParagraphNumberingSchemes != null && ParagraphNumberingSchemeManager.Instance.SyncedParagraphNumberingSchemes.Any(x => (x as ParagraphNumberingScheme).Name == replacementParagraphNumberingScheme.Name))
            {
                ReplaceListTemplateInDocument(replacementParagraphNumberingScheme, listTemplate);
            }
            else if (ParagraphNumberingSchemeManager.Instance?.BuiltInParagraphNumberingSchemes != null && ParagraphNumberingSchemeManager.Instance.BuiltInParagraphNumberingSchemes.Any(x => (x as ParagraphNumberingScheme).Name == replacementParagraphNumberingScheme.Name))
            {
                ReplaceListTemplateInDocument(replacementParagraphNumberingScheme, listTemplate);
            }
        }

        private void ReplaceListTemplateInDocument(ParagraphNumberingScheme replacementParagraphNumberingScheme, ListTemplate listTemplate)
        {
            ParagraphNumberingScheme oldParagraphNumberingScheme = GetActiveDocumentParagraphNumberingScheme(listTemplate);

            string replacementListTemplateName = ListTemplateUtil.GetListTemplateName(replacementParagraphNumberingScheme);
            ListTemplate replacementListTemplate = ListTemplateUtil.GetListTemplateByName(replacementListTemplateName);

            bool performListTemplatePropertyUpdates = true;

            if (replacementParagraphNumberingScheme.ParagraphNumberingLevels.Count != listTemplate.ListLevels.Count)
            {
                replacementListTemplate = CreateOrUpdateListTemplate(replacementParagraphNumberingScheme);

                if (IsSelectedParagraphNumberingSchemeReadOnly)
                {
                    ParagraphNumberingSchemeManager.AddCustomParagraphNumberingSchemeToActiveDocumentSchemes(replacementParagraphNumberingScheme);
                }

                performListTemplatePropertyUpdates = false;
            }

            bool renameStyles;

            if (replacementListTemplate == null)
            {
                if (replacementParagraphNumberingScheme.CreateContinuationParagraphStyles)
                {
                    foreach (ParagraphNumberingLevel numberingLevel in replacementParagraphNumberingScheme.ParagraphNumberingLevels)
                    {
                        string oldTemplateContinuationParagraphStyleName = ListTemplateUtil.GetContinuationParagraphStyleName(oldParagraphNumberingScheme, numberingLevel.Index);

                        if (ListTemplateUtil.CheckStyleExists(oldTemplateContinuationParagraphStyleName))
                        {
                            continue;
                        }

                        /*
                         * For the old paragraph numbering scheme no continuation styles (at least for this level) were created.
                         * Create new ones for the replacement numbering scheme
                         */

                        string continuationParagraphStyleName = ListTemplateUtil.GetContinuationParagraphStyleName(replacementParagraphNumberingScheme, numberingLevel.Index);

                        CreateContinuationParagraphStyle(replacementParagraphNumberingScheme, numberingLevel, replacementParagraphNumberingScheme.BaseStyleName, continuationParagraphStyleName);
                    }
                }

                // The list we want to switch to doesn't yet exist. Renaming the current one and its styles should be enough

                renameStyles = true;

                listTemplate.Name = replacementListTemplateName;

                if (IsSelectedParagraphNumberingSchemeReadOnly)
                {
                    ParagraphNumberingSchemeManager.AddCustomParagraphNumberingSchemeToActiveDocumentSchemes(replacementParagraphNumberingScheme);
                }
            }
            else
            {
                var oldParagraphListTemplateName = ListTemplateUtil.GetListTemplateName(oldParagraphNumberingScheme);
                if (oldParagraphListTemplateName != replacementListTemplateName)
                {
                    var oldParagraphListTemplateList = ListTemplateUtil.GetListTemplateByName(oldParagraphListTemplateName);
                    var replacementListTemplateList = ListTemplateUtil.GetListTemplateByName(replacementListTemplateName);

                    for (int i = 1; i <= replacementParagraphNumberingScheme.ParagraphNumberingLevels.Count; i++)
                    {
                        string oldStyleName = ListTemplateUtil.GetParagraphStyleName(oldParagraphNumberingScheme, i);
                        string replacementStyleName = ListTemplateUtil.GetParagraphStyleName(replacementParagraphNumberingScheme, i);

                        if ((oldParagraphListTemplateList != null && oldParagraphListTemplateList.ListLevels[i] != null && oldParagraphListTemplateList.ListLevels[i].LinkedStyle != null) & (replacementListTemplateList != null && replacementListTemplateList.ListLevels[i] != null && replacementListTemplateList.ListLevels[i].LinkedStyle != null))
                        {
                            oldStyleName = oldParagraphListTemplateList.ListLevels[i].LinkedStyle;
                            replacementStyleName = replacementListTemplateList.ListLevels[i].LinkedStyle;
                        }

                        DocumentUtil.ReplaceStyle(oldStyleName, replacementStyleName);

                        string oldContinuationStyleName = ListTemplateUtil.GetContinuationParagraphStyleName(oldParagraphNumberingScheme, i);
                        string replacementContinuationStyleName = ListTemplateUtil.GetContinuationParagraphStyleName(replacementParagraphNumberingScheme, i);

                        var oldParagraphListTemplateContinuationStyleName = ListTemplateUtil.GetContinuationStyleNameFromNumberedStyleName(oldStyleName);
                        var replaceParagraphListTemplateContinuationStyleName = ListTemplateUtil.GetContinuationStyleNameFromNumberedStyleName(replacementStyleName);

                        if (oldParagraphListTemplateContinuationStyleName != null && replaceParagraphListTemplateContinuationStyleName != null)
                        {
                            oldContinuationStyleName = oldParagraphListTemplateContinuationStyleName;
                            replacementContinuationStyleName = replaceParagraphListTemplateContinuationStyleName;
                        }

                        DocumentUtil.ReplaceStyle(oldContinuationStyleName, replacementContinuationStyleName);
                    }

                    ListTemplateUtil.DeleteListTemplateAndLinkedStyles(oldParagraphNumberingScheme);
                }

                listTemplate = replacementListTemplate;

                renameStyles = false;
            }

            if (performListTemplatePropertyUpdates)
            {
                UpdateListTemplate(replacementParagraphNumberingScheme, listTemplate);

                for (int levelIndex = 1; levelIndex <= replacementParagraphNumberingScheme.ParagraphNumberingLevels.Count; levelIndex++)
                {
                    ParagraphNumberingLevel numberingLevel = replacementParagraphNumberingScheme.ParagraphNumberingLevels[levelIndex - 1];

                    ListLevel listLevel = listTemplate.ListLevels[levelIndex];

                    string linkedStyleName = listLevel.LinkedStyle;
                    linkedStyleName = ListTemplateUtil.EnsureFullParagraphStyleName(linkedStyleName, oldParagraphNumberingScheme);

                    Style linkedStyle = ListTemplateUtil.GetParagraphStyleByName(linkedStyleName);

                    if (linkedStyle != null)
                    {
                        if (renameStyles)
                        {
                            linkedStyle.NameLocal = ListTemplateUtil.GetParagraphStyleName(replacementParagraphNumberingScheme, levelIndex);
                        }

                        UpdateParagraphStyle(numberingLevel, linkedStyle, replacementParagraphNumberingScheme.BaseStyleName);

                        // Re-assigning the linked style (paragraph style) seems to fix the negative indentation issue
                        listLevel.LinkedStyle = linkedStyle.NameLocal;
                    }

                    string continuationStyleName = ListTemplateUtil.GetContinuationStyleNameFromLinkedStyleName(listLevel?.LinkedStyle, listLevel.Index);
                    Style continuationStyle = ListTemplateUtil.GetParagraphStyleByName(continuationStyleName);

                    if (continuationStyle != null)
                    {
                        if (renameStyles)
                        {
                            continuationStyle.NameLocal = ListTemplateUtil.GetContinuationParagraphStyleName(replacementParagraphNumberingScheme, levelIndex);
                        }

                        UpdateParagraphStyle(numberingLevel, continuationStyle, replacementParagraphNumberingScheme.BaseStyleName, true);
                    }
                }
            }

            UpdateListTemplateParagraphStyleFollowingStyle(replacementParagraphNumberingScheme);

            // oldParagraphNumberingScheme is always required by ApplyListTemplate() which calls this method since it 
            // somehow fixes the "wrong indentation is applied" bug. Removing the scheme would also remove it from the
            // toolbar, which is not what we want since in this case, oldParagraphNumberingScheme and replacementListTemplate
            // are exactly the same thing and only once instance exists in the "Active document" template list
            if (replacementListTemplate != null && oldParagraphNumberingScheme.Name != replacementListTemplate.Name)
            {
                ParagraphNumberingSchemeManager.RemoveActiveDocumentParagraphNumberingScheme(oldParagraphNumberingScheme);
            }


            this.reupdateListLevels = new KeyValuePair<ParagraphNumberingScheme, ListTemplate>(replacementParagraphNumberingScheme, listTemplate);

            AddNewLinePrefixIfNecessary(replacementParagraphNumberingScheme);
        }

        private void ReplaceListTemplateInFirmListDocument(ParagraphNumberingScheme replacementParagraphNumberingScheme, ListTemplate listTemplate)
        {
            ParagraphNumberingScheme oldParagraphNumberingScheme = GetActiveDocumentParagraphNumberingScheme(listTemplate);

            var oldParagraphListTemplateName = ListTemplateUtil.GetListTemplateName(oldParagraphNumberingScheme);
            var replacementListTemplateName = ListTemplateUtil.GetListTemplateName(replacementParagraphNumberingScheme);

            if (oldParagraphListTemplateName != replacementListTemplateName)
            {
                var oldListTemplateList = ListTemplateUtil.GetListTemplateByName(oldParagraphListTemplateName);
                var replacementListTemplateList = ListTemplateUtil.GetListTemplateByNameFromFirmListDocument(replacementListTemplateName);

                UpdateListTemplate(replacementParagraphNumberingScheme, oldListTemplateList);

                oldListTemplateList.Name = ListTemplateUtil.GetListTemplateNameFormFirstLevel(replacementListTemplateList);
                oldListTemplateList.OutlineNumbered = replacementListTemplateList.OutlineNumbered;

                for (int i = 1; i <= oldParagraphNumberingScheme.ParagraphNumberingLevels.Count; i++)
                {
                    try
                    {
                        var oldStyleName = oldListTemplateList.ListLevels[i].LinkedStyle;
                        var firmListNewStyleName = replacementListTemplateList.ListLevels[i].LinkedStyle;

                        var firmListNewParagraphStyleName = ListTemplateUtil.GetParagraphStyleName(replacementParagraphNumberingScheme, i);

                        var oldContinuationStyleName = ListTemplateUtil.GetContinuationParagraphStyleName(oldParagraphNumberingScheme, i);
                        var firmListOldContinuationStyleName = ListTemplateUtil.GetContinuationStyleNameFromLinkedStyleName(firmListNewStyleName, i);
                        var firmListNewContinuationStyleName = ListTemplateUtil.GetContinuationParagraphStyleName(replacementParagraphNumberingScheme, i);

                        if (ListTemplateUtil.CheckIfStyleExistsInDocument(firmListNewStyleName, ThisAddIn.FirmListDocument))
                        {
                            Style normalFirmListFDocumentStyle = ThisAddIn.FirmListDocument.Styles[firmListNewStyleName];
                            CopyStyle(ThisAddIn.FirmListDocument, ThisAddIn.Instance.Application.ActiveDocument, normalFirmListFDocumentStyle);
                            // ThisAddIn.Instance.Application.OrganizerCopy(ThisAddIn.FirmListDocument.FullName, ThisAddIn.Instance.Application.ActiveDocument.FullName, firmListNewStyleName, WdOrganizerObject.wdOrganizerObjectStyles);
                            Style normalActiveDocumentStyle = null;
                            try
                            {
                                normalActiveDocumentStyle = ThisAddIn.Instance.Application.ActiveDocument.Styles[firmListNewStyleName];
                            }
                            catch
                            {
                            }

                            if (normalActiveDocumentStyle != null)
                            {
                                normalActiveDocumentStyle.NameLocal = firmListNewParagraphStyleName;
                                DocumentUtil.ReplaceStyle(oldStyleName, firmListNewParagraphStyleName);
                                var normalActiveDocumentBaseStyleName = ListTemplateUtil.GetBaseStyleNameFromStyle(normalActiveDocumentStyle);

                                oldListTemplateList.ListLevels[i].LinkedStyle = firmListNewParagraphStyleName;
                                normalActiveDocumentStyle.ParagraphFormat.LeftIndent = normalFirmListFDocumentStyle.ParagraphFormat.LeftIndent;
                                normalActiveDocumentStyle.ParagraphFormat.FirstLineIndent = normalFirmListFDocumentStyle.ParagraphFormat.FirstLineIndent;

                                UpdateNumberingLevelFromParagraphStyle(replacementParagraphNumberingScheme.ParagraphNumberingLevels[i - 1], normalActiveDocumentStyle, normalActiveDocumentBaseStyleName);
                                UpdateParagraphStyle(replacementParagraphNumberingScheme.ParagraphNumberingLevels[i - 1], normalActiveDocumentStyle, normalActiveDocumentBaseStyleName);
                            }
                        }

                        if (replacementParagraphNumberingScheme.CreateContinuationParagraphStyles && ListTemplateUtil.CheckIfStyleExistsInDocument(firmListOldContinuationStyleName, ThisAddIn.FirmListDocument))
                        {
                            Style firmListOldContinuationStyle = ThisAddIn.FirmListDocument.Styles[firmListOldContinuationStyleName];
                            CopyStyle(ThisAddIn.FirmListDocument, ThisAddIn.Instance.Application.ActiveDocument, firmListOldContinuationStyle);
                            //ThisAddIn.Instance.Application.OrganizerCopy(ThisAddIn.FirmListDocument.FullName, ThisAddIn.Instance.Application.ActiveDocument.FullName, firmListOldContinuationStyleName, WdOrganizerObject.wdOrganizerObjectStyles);
                            Style continuationActiveDocumentStyle = null;
                            try
                            {
                                continuationActiveDocumentStyle = ThisAddIn.Instance.Application.ActiveDocument.Styles[firmListOldContinuationStyleName];
                            }
                            catch
                            {
                            }
                            if (continuationActiveDocumentStyle != null)
                            {
                                continuationActiveDocumentStyle.NameLocal = firmListNewContinuationStyleName;
                                DocumentUtil.ReplaceStyle(oldContinuationStyleName, firmListNewContinuationStyleName);
                                var continuationActiveDocumentBaseStyleName = ListTemplateUtil.GetBaseStyleNameFromStyle(continuationActiveDocumentStyle);

                                UpdateNumberingLevelFromParagraphStyle(replacementParagraphNumberingScheme.ParagraphNumberingLevels[i - 1], continuationActiveDocumentStyle, continuationActiveDocumentBaseStyleName, true);
                                UpdateParagraphStyle(replacementParagraphNumberingScheme.ParagraphNumberingLevels[i - 1], continuationActiveDocumentStyle, continuationActiveDocumentBaseStyleName);
                            }
                        }

                        if (ListTemplateUtil.CheckIfStyleExistsInDocument(oldStyleName, ThisAddIn.Instance.Application.ActiveDocument))
                        {
                            ThisAddIn.Instance.Application.ActiveDocument.Styles[oldStyleName].Delete();
                        }

                        if (ListTemplateUtil.CheckIfStyleExistsInDocument(oldContinuationStyleName, ThisAddIn.Instance.Application.ActiveDocument))
                        {
                            ThisAddIn.Instance.Application.ActiveDocument.Styles[oldContinuationStyleName].Delete();
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex);
                    }
                }

                listTemplate.Name = replacementListTemplateName;

                UpdateListTemplateParagraphStyleFollowingStyle(replacementParagraphNumberingScheme);
                this.reupdateListLevels = new KeyValuePair<ParagraphNumberingScheme, ListTemplate>(replacementParagraphNumberingScheme, listTemplate);
            }
        }

        private void UpdateListTemplate(ParagraphNumberingScheme paragraphNumberingScheme, ListTemplate listTemplate)
        {
            var lists = ListTemplateUtil.GetAllListsUsingListTemplate(listTemplate.Name);

            for (int levelIndex = 1; levelIndex <= paragraphNumberingScheme.ParagraphNumberingLevels.Count; levelIndex++)
            {
                ParagraphNumberingLevel currentNumberingLevel = paragraphNumberingScheme.ParagraphNumberingLevels[levelIndex - 1];

                ListLevel listLevel = listTemplate.ListLevels[levelIndex];

                if (currentNumberingLevel.FontPreferences.Name != null)
                {
                    listLevel.Font.Name = currentNumberingLevel.FontPreferences.Name;
                }

                if (currentNumberingLevel.FontPreferences.Size > 0)
                {
                    listLevel.Font.Size = currentNumberingLevel.FontPreferences.Size;
                }

                listLevel.Font.Bold = currentNumberingLevel.FontPreferences.IsBold ? -1 : 0;
                listLevel.Font.Italic = currentNumberingLevel.FontPreferences.IsItalic ? -1 : 0;
                listLevel.Font.Underline = currentNumberingLevel.FontPreferences.UnderlineType;
                listLevel.Font.AllCaps = currentNumberingLevel.FontPreferences.IsAllCaps ? -1 : 0;

                if (currentNumberingLevel.FontPreferences.HexColor != null)
                {
                    try
                    {
                        listLevel.Font.TextColor.RGB = GetRgbFromWpfColor(currentNumberingLevel.FontPreferences.HexColor);
                    }
                    catch (Exception)
                    {

                        listLevel.Font.Color = FontUtil.ConvertHexToWdColor(currentNumberingLevel.FontPreferences.HexColor);
                    }
                }

                // This is actually required, otherwise switching from a bullet to a number "number format" will throw an exception
                listLevel.NumberStyle = WdListNumberStyle.wdListNumberStyleNone;

                if (currentNumberingLevel.BulletCharacterIsEnabled)
                {
                    listLevel.NumberFormat = currentNumberingLevel.BulletCharacter;
                }
                else
                {
                    listLevel.NumberFormat = currentNumberingLevel.NumberFormat;
                }

                // Has to be after NumberFormat, otherwise it will throw an exception when BulletCharacterIsEnabled == true
                listLevel.NumberStyle = currentNumberingLevel.NumberStyle;

                listLevel.TabPosition = Utils.LimitMaxListLevelValues(app.InchesToPoints(currentNumberingLevel.TabPosition), CustomConstants.LIST_LEVEL_MAX_POSITIONS_VALUE_IN_DP);
                listLevel.Alignment = currentNumberingLevel.NumberAlignment;
                listLevel.NumberPosition = Utils.LimitMaxListLevelValues(app.InchesToPoints(currentNumberingLevel.NumberIndent), CustomConstants.LIST_LEVEL_MAX_POSITIONS_VALUE_IN_DP);

                float textPosition = app.InchesToPoints(currentNumberingLevel.ParagraphFormat.LeftIndent);

                if (currentNumberingLevel.ParagraphFormat.SpecialIndentType != SpecialParagraphIndentation.NONE)
                {
                    textPosition += app.InchesToPoints(currentNumberingLevel.ParagraphFormat.SpecialIndentValue);
                }

                listLevel.TextPosition = Utils.LimitMaxListLevelValues(textPosition, CustomConstants.LIST_LEVEL_MAX_POSITIONS_VALUE_IN_DP);

                switch (currentNumberingLevel.TrailingCharacter)
                {
                    case TrailingCharacter.TAB:
                        listLevel.TrailingCharacter = WdTrailingCharacter.wdTrailingTab;
                        break;
                    case TrailingCharacter.SPACE:
                        listLevel.TrailingCharacter = WdTrailingCharacter.wdTrailingSpace;
                        break;
                    case TrailingCharacter.NONE:
                        listLevel.TrailingCharacter = WdTrailingCharacter.wdTrailingNone;
                        break;
                    case TrailingCharacter.NEW_LINE:
                        listLevel.TrailingCharacter = WdTrailingCharacter.wdTrailingNone;
                        break;
                }

                listLevel.StartAt = currentNumberingLevel.StartAt;
                listLevel.ResetOnHigher = currentNumberingLevel.RestartAfterLevel;

                ListTemplateUtil.AddRemoveNewLineCharacterIfExists(currentNumberingLevel.TrailingCharacter, lists, listTemplate.Name, currentNumberingLevel);
            }
        }

        private void UpdateListTemplateParagraphStyles(ParagraphNumberingScheme paragraphNumberingScheme, ListTemplate listTemplate)
        {
            EnsureBaseStyleExist(paragraphNumberingScheme);

            bool createContinuationParagraphStyles = paragraphNumberingScheme.CreateContinuationParagraphStyles;

            for (int levelIndex = 1; levelIndex <= paragraphNumberingScheme.ParagraphNumberingLevels.Count; levelIndex++)
            {
                ParagraphNumberingLevel currentLevel = paragraphNumberingScheme.ParagraphNumberingLevels[levelIndex - 1];

                ListLevel listLevel = listTemplate.ListLevels[levelIndex];

                string paragraphStyleName = ListTemplateUtil.GetParagraphStyleName(paragraphNumberingScheme, levelIndex);

                if (!string.IsNullOrEmpty(listLevel.LinkedStyle))
                {
                    paragraphStyleName = listLevel.LinkedStyle;
                }

                Style paragraphStyle = ListTemplateUtil.GetParagraphStyleByName(paragraphStyleName);

                if (paragraphStyle == null)
                {
                    CreateParagraphStyle(paragraphNumberingScheme, currentLevel, paragraphNumberingScheme.BaseStyleName, paragraphStyleName, listLevel);
                }
                else
                {
                    listLevel.LinkedStyle = paragraphStyleName;
                    UpdateParagraphStyle(currentLevel, paragraphStyle, paragraphNumberingScheme.BaseStyleName);
                }
            }

            if (createContinuationParagraphStyles)
            {
                for (int levelIndex = 1; levelIndex <= paragraphNumberingScheme.ParagraphNumberingLevels.Count; levelIndex++)
                {
                    ParagraphNumberingLevel currentLevel = paragraphNumberingScheme.ParagraphNumberingLevels[levelIndex - 1];

                    ListLevel listLevel = listTemplate.ListLevels[levelIndex];

                    string continuationParagraphStyleName = ListTemplateUtil.GetContinuationStyleNameFromLinkedStyleName(listLevel?.LinkedStyle, listLevel.Index);

                    if (string.IsNullOrEmpty(continuationParagraphStyleName))
                    {
                        continuationParagraphStyleName = ListTemplateUtil.GetContinuationParagraphStyleName(paragraphNumberingScheme, levelIndex);
                    }

                    Style continuationParagraphStyle = ListTemplateUtil.GetParagraphStyleByName(continuationParagraphStyleName);

                    if (continuationParagraphStyle == null)
                    {
                        CreateContinuationParagraphStyle(paragraphNumberingScheme, currentLevel, paragraphNumberingScheme.BaseStyleName, continuationParagraphStyleName);
                    }
                    else
                    {
                        UpdateParagraphStyle(currentLevel, continuationParagraphStyle, paragraphNumberingScheme.BaseStyleName, isContinuationStyle: true);
                    }
                }
            }

            UpdateListTemplateParagraphStyleFollowingStyle(paragraphNumberingScheme);
        }

        private void UpdateListTemplateParagraphStyleFollowingStyle(ParagraphNumberingScheme paragraphNumberingScheme)
        {
            var listTemplate = ListTemplateUtil.GetListTemplateByName(paragraphNumberingScheme.Name);

            foreach (ParagraphNumberingLevel numberingLevel in paragraphNumberingScheme.ParagraphNumberingLevels)
            {
                ListLevel listLevel = listTemplate.ListLevels[numberingLevel.Index];

                string paragraphStyleName = listLevel?.LinkedStyle;

                if (string.IsNullOrEmpty(paragraphStyleName))
                {
                    paragraphStyleName = ListTemplateUtil.GetParagraphStyleName(paragraphNumberingScheme, numberingLevel.Index); ;
                }

                Style paragraphStyle = ListTemplateUtil.GetParagraphStyleByName(paragraphStyleName);

                object nextParagraphStyleName;

                if (string.IsNullOrEmpty(numberingLevel.ParagraphFormat.StyleToFollow))
                {
                    nextParagraphStyleName = paragraphStyle?.NameLocal;
                }
                else
                {
                    nextParagraphStyleName = numberingLevel.ParagraphFormat.StyleToFollow;
                }

                try
                {
                    paragraphStyle.set_NextParagraphStyle(ref nextParagraphStyleName);
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex);
                }
            }
        }

        private Style CreateParagraphStyle(ParagraphNumberingScheme paragraphNumberingScheme, ParagraphNumberingLevel numberingLevel, string baseStyleName, string styleName, ListLevel listLevel = null)
        {
            if (numberingLevel.ParagraphFormat.StyleType == WdStyleType.wdStyleTypeParagraph)
            {
                numberingLevel.ParagraphFormat.StyleType = WdStyleType.wdStyleTypeParagraphOnly;
            }

            Style paragraphStyle = null;

            if (ParagraphNumberingSchemeManager.Instance?.ActiveDocumentParagraphNumberingSchemes != null && ParagraphNumberingSchemeManager.Instance.ActiveDocumentParagraphNumberingSchemes.Any(x => (x as ParagraphNumberingScheme).Name == paragraphNumberingScheme.Name))
            {
                paragraphStyle = nativeDocument.Styles.Add(styleName, numberingLevel.ParagraphFormat.StyleType);

                if (listLevel != null)
                {
                    listLevel.LinkedStyle = styleName;
                }
            }
            else if (ParagraphNumberingSchemeManager.Instance?.FirmDocumentParagraphNumberingSchemes != null && ListTemplateUtil.CheckIfNumberingSchemeIsInFirmListDocument(ParagraphNumberingSchemeManager.Instance.FirmDocumentParagraphNumberingSchemes, paragraphNumberingScheme.Name))
            {
                Style firmListDocumentListLevelStyle = ListTemplateUtil.GetStyleFromDocumentListTemplateListLevel(paragraphNumberingScheme, numberingLevel.Index, ThisAddIn.FirmListDocument);

                if (firmListDocumentListLevelStyle?.NameLocal != null)
                {
                    CopyStyle(ThisAddIn.FirmListDocument, ThisAddIn.Instance.Application.ActiveDocument, firmListDocumentListLevelStyle);
                    //ThisAddIn.Instance.Application.OrganizerCopy(ThisAddIn.FirmListDocument.FullName, ThisAddIn.Instance.Application.ActiveDocument.FullName, firmListDocumentListLevelStyle.NameLocal, WdOrganizerObject.wdOrganizerObjectStyles);
                    Style style = null;
                    try
                    {
                        style = ThisAddIn.Instance.Application.ActiveDocument.Styles[firmListDocumentListLevelStyle.NameLocal];
                    }
                    catch
                    {
                    }
                    if (style != null)
                    {
                        baseStyleName = !string.IsNullOrEmpty(ListTemplateUtil.GetBaseStyleNameFromStyle(style)) ? ListTemplateUtil.GetBaseStyleNameFromStyle(style) : Utils.GetLocalizedStyleNameFromBuiltInStyle(WdBuiltinStyle.wdStyleNormal);

                        style.NameLocal = styleName;

                        listLevel.LinkedStyle = styleName;
                        style.ParagraphFormat.LeftIndent = firmListDocumentListLevelStyle.ParagraphFormat.LeftIndent;
                        style.ParagraphFormat.FirstLineIndent = firmListDocumentListLevelStyle.ParagraphFormat.FirstLineIndent;

                        paragraphNumberingScheme.BaseStyleName = baseStyleName;
                    }
                }
            }
            else if (ParagraphNumberingSchemeManager.Instance?.SyncedParagraphNumberingSchemes != null && ParagraphNumberingSchemeManager.Instance.SyncedParagraphNumberingSchemes.Any(x => (x as ParagraphNumberingScheme).Name == paragraphNumberingScheme.Name))
            {
                paragraphStyle = nativeDocument.Styles.Add(styleName, numberingLevel.ParagraphFormat.StyleType);

                if (listLevel != null)
                {
                    listLevel.LinkedStyle = styleName;
                }
            }
            else if (ParagraphNumberingSchemeManager.Instance?.BuiltInParagraphNumberingSchemes != null && ParagraphNumberingSchemeManager.Instance.BuiltInParagraphNumberingSchemes.Any(x => (x as ParagraphNumberingScheme).Name == paragraphNumberingScheme.Name))
            {
                paragraphStyle = nativeDocument.Styles.Add(styleName, numberingLevel.ParagraphFormat.StyleType);

                if (listLevel != null)
                {
                    listLevel.LinkedStyle = styleName;
                }
            }

            UpdateParagraphStyle(numberingLevel, paragraphStyle, baseStyleName);

            return paragraphStyle;
        }

        private void CopyStyle(Document firmListDocument, Document activeDocument, Style firmListDocumentListLevelStyle)
        {
            if (!ListTemplateUtil.CheckIfStyleExistsInDocument(firmListDocumentListLevelStyle.NameLocal, activeDocument))
            {
                StaticCustomUndoRecord.Stop();
                Range range = firmListDocument.Content;
                var find = range.Find;

                find.ClearFormatting();
                find.ClearFormatting();
                find.set_Style(firmListDocumentListLevelStyle);
                find.Format = true;
                find.Forward = true;
                find.Execute();

                if (find.Found)
                {
                    range = range.Paragraphs.First.Range;
                    range.Copy();
                    var range2 = range;
                    List<string> beforeList = DocumentUtil.GetAllListTemplatesNames();
                    ThisAddIn.Instance.Application.UndoRecord.StartCustomRecord("Copied style");
                    range = activeDocument.Content;
                    range.Collapse(WdCollapseDirection.wdCollapseEnd);
                    range.Paste();
                    range.Delete();
                    ThisAddIn.Instance.Application.UndoRecord.EndCustomRecord();
                    List<string> afterList = DocumentUtil.GetAllListTemplatesNames();
                    List<string> extra = afterList.Where(x => !beforeList.Contains(x)).ToList();
                    List<string> stylesToDelete = new List<string>();

                    foreach (string listTemplateName in extra)
                    {
                        if (firmListDocumentListLevelStyle.ListTemplate?.Name == listTemplateName)
                        {
                            continue;
                        }

                        ListTemplate listTemplate = ListTemplateUtil.GetListTemplateByExactName(listTemplateName);

                        foreach (ListLevel listLevel in listTemplate.ListLevels)
                        {
                            if (!string.IsNullOrWhiteSpace(listLevel.LinkedStyle))
                            {
                                stylesToDelete.Add(listLevel.LinkedStyle);
                            }
                        }
                    }

                    if (stylesToDelete.Count > 0)
                    {
                        //Undo 
                        activeDocument.Undo(1);
                        //Delete Styles
                        ThisAddIn.Instance.Application.UndoRecord.StartCustomRecord("Copied style");
                        firmListDocument.Range(0, range2.Start).Delete();
                        firmListDocument.Range(range2.End, firmListDocument.Content.End).Delete();
                        firmListDocument.Activate();
                        foreach (var style in stylesToDelete)
                        {
                            try
                            {
                                var styleObj = firmListDocument.Styles[style];
                                styleObj.Delete();
                            }
                            catch (Exception ex)
                            {

                            }
                        }
                        ThisAddIn.Instance.Application.UndoRecord.EndCustomRecord();
                        //Copy Again
                        range2.Copy();
                        range = activeDocument.Content;
                        range.Collapse(WdCollapseDirection.wdCollapseEnd);
                        range.Paste();
                        range.Delete();
                        firmListDocument.Undo(1);
                    }
                }
                StaticCustomUndoRecord.Start();
            }
        }

        private Style CreateContinuationParagraphStyle(ParagraphNumberingScheme paragraphNumberingScheme, ParagraphNumberingLevel numberingLevel, string baseStyleName, string styleName)
        {
            if (numberingLevel.ContinuationParagraphFormat.StyleType == WdStyleType.wdStyleTypeParagraph)
            {
                numberingLevel.ContinuationParagraphFormat.StyleType = WdStyleType.wdStyleTypeParagraphOnly;
            }

            Style paragraphStyle = null;

            if (ParagraphNumberingSchemeManager.Instance?.ActiveDocumentParagraphNumberingSchemes != null && ParagraphNumberingSchemeManager.Instance.ActiveDocumentParagraphNumberingSchemes.Any(x => (x as ParagraphNumberingScheme).Name == paragraphNumberingScheme.Name))
            {
                paragraphStyle = nativeDocument.Styles.Add(styleName, numberingLevel.ContinuationParagraphFormat.StyleType);
            }
            else if (ParagraphNumberingSchemeManager.Instance?.FirmDocumentParagraphNumberingSchemes != null && ListTemplateUtil.CheckIfNumberingSchemeIsInFirmListDocument(ParagraphNumberingSchemeManager.Instance.FirmDocumentParagraphNumberingSchemes, paragraphNumberingScheme.Name))
            {
                Style firmListDocumentListLevelStyle = ListTemplateUtil.GetStyleFromDocumentListTemplateListLevel(paragraphNumberingScheme, numberingLevel.Index, ThisAddIn.FirmListDocument, true);

                if (firmListDocumentListLevelStyle?.NameLocal != null)
                {
                    CopyStyle(ThisAddIn.FirmListDocument, ThisAddIn.Instance.Application.ActiveDocument, firmListDocumentListLevelStyle);
                    //ThisAddIn.Instance.Application.OrganizerCopy(ThisAddIn.FirmListDocument.FullName, ThisAddIn.Instance.Application.ActiveDocument.FullName, firmListDocumentListLevelStyle.NameLocal, WdOrganizerObject.wdOrganizerObjectStyles);

                    Style style = null;
                    try
                    {
                        style = ThisAddIn.Instance.Application.ActiveDocument.Styles[firmListDocumentListLevelStyle.NameLocal];
                    }
                    catch
                    {
                    }

                    if (style?.NameLocal != null)
                    {
                        style.NameLocal = styleName;
                    }

                    UpdateNumberingLevelFromParagraphStyle(numberingLevel, style, baseStyleName, true);
                }
            }
            else if (ParagraphNumberingSchemeManager.Instance?.SyncedParagraphNumberingSchemes != null && ParagraphNumberingSchemeManager.Instance.SyncedParagraphNumberingSchemes.Any(x => (x as ParagraphNumberingScheme).Name == paragraphNumberingScheme.Name))
            {
                paragraphStyle = nativeDocument.Styles.Add(styleName, numberingLevel.ContinuationParagraphFormat.StyleType);
            }
            else if (ParagraphNumberingSchemeManager.Instance?.BuiltInParagraphNumberingSchemes != null && ParagraphNumberingSchemeManager.Instance.BuiltInParagraphNumberingSchemes.Any(x => (x as ParagraphNumberingScheme).Name == paragraphNumberingScheme.Name))
            {
                paragraphStyle = nativeDocument.Styles.Add(styleName, numberingLevel.ContinuationParagraphFormat.StyleType);
            }

            UpdateParagraphStyle(numberingLevel, paragraphStyle, baseStyleName, isContinuationStyle: true);

            return paragraphStyle;
        }

        private void UpdateParagraphStyle(ParagraphNumberingLevel numberingLevel, Style paragraphStyle, string baseStyleName, bool isContinuationStyle = false)
        {
            if (paragraphStyle?.NameLocal != null && paragraphStyle.NameLocal != Utils.GetLocalizedStyleNameFromBuiltInStyle(WdBuiltinStyle.wdStyleNormal) && paragraphStyle.NameLocal != Utils.GetLocalizedStyleNameFromBuiltInStyle(WdBuiltinStyle.wdStyleDefaultParagraphFont))
            {
                ParagraphFormat paragraphFormat = isContinuationStyle ? numberingLevel.ContinuationParagraphFormat : numberingLevel.ParagraphFormat;

                if (paragraphFormat.HideUntilUsed)
                {
                    paragraphStyle.Visibility = true;
                    paragraphStyle.UnhideWhenUsed = true;
                }
                else
                {
                    paragraphStyle.Visibility = false;
                }

                paragraphStyle.set_BaseStyle(baseStyleName);
                if (paragraphFormat.FontPreferences.Name != null)
                {
                    paragraphStyle.Font.Name = paragraphFormat.FontPreferences.Name;
                }
                if (paragraphFormat.FontPreferences.Size > 0)
                {
                    paragraphStyle.Font.Size = paragraphFormat.FontPreferences.Size;
                }

                paragraphStyle.Font.AllCaps = paragraphFormat.FontPreferences.IsAllCaps ? -1 : 0;
                paragraphStyle.Font.SmallCaps = paragraphFormat.FontPreferences.IsSmallCaps ? -1 : 0;
                if (paragraphFormat.FontPreferences.HexColor != null)
                {
                    try
                    {


                        paragraphStyle.Font.TextColor.RGB = GetRgbFromWpfColor(paragraphFormat.FontPreferences.HexColor);
                    }
                    catch (Exception)
                    {
                        paragraphStyle.Font.Color = FontUtil.ConvertHexToWdColor(paragraphFormat.FontPreferences.HexColor);
                    }
                }

                // Line spacing
                paragraphStyle.ParagraphFormat.SpaceBefore = paragraphFormat.SpacingBefore;
                paragraphStyle.ParagraphFormat.SpaceAfter = paragraphFormat.SpacingAfter;

                if (paragraphFormat.LineSpacingValue > 0)
                {
                    // Throws exception if the value is not at least 0.7
                    paragraphStyle.ParagraphFormat.LineSpacing = paragraphFormat.LineSpacingValue;
                }

                if (paragraphFormat.LineSpacingType.HasValue)
                {
                    paragraphStyle.ParagraphFormat.LineSpacingRule = paragraphFormat.LineSpacingType.Value;
                }

                // Indentation

                paragraphStyle.ParagraphFormat.LeftIndent = Utils.LimitMaxListLevelValues(app.InchesToPoints(paragraphFormat.LeftIndent + (paragraphFormat.SpecialIndentType == SpecialParagraphIndentation.HANGING ? paragraphFormat.SpecialIndentValue : 0)), CustomConstants.LIST_LEVEL_MAX_POSITIONS_VALUE_IN_DP);
                paragraphStyle.ParagraphFormat.RightIndent = Utils.LimitMaxListLevelValues(app.InchesToPoints(paragraphFormat.RightIndent), CustomConstants.LIST_LEVEL_MAX_POSITIONS_VALUE_IN_DP);

                float specialIndentValue = Utils.LimitMaxListLevelValues(app.InchesToPoints(paragraphFormat.SpecialIndentValue), CustomConstants.LIST_LEVEL_MAX_POSITIONS_VALUE_IN_DP);

                switch (paragraphFormat.SpecialIndentType)
                {
                    case SpecialParagraphIndentation.FIRST_LINE:
                        paragraphStyle.ParagraphFormat.FirstLineIndent = specialIndentValue;
                        break;
                    case SpecialParagraphIndentation.HANGING:
                        paragraphStyle.ParagraphFormat.FirstLineIndent = -specialIndentValue;
                        //paragraphStyle.ParagraphFormat.LeftIndent += specialIndentValue;
                        break;
                    default:
                        paragraphStyle.ParagraphFormat.FirstLineIndent = 0;
                        break;
                }

                paragraphStyle.NoSpaceBetweenParagraphsOfSameStyle = paragraphFormat.NoSpaceBetweenParagraphsOfSameStyle;
                paragraphStyle.ParagraphFormat.OutlineLevel = paragraphFormat.OutlineLevel;
                paragraphStyle.ParagraphFormat.WidowControl = paragraphFormat.WindowControl ? -1 : 0;
                paragraphStyle.ParagraphFormat.KeepWithNext = paragraphFormat.KeepWithNext ? -1 : 0;
                paragraphStyle.ParagraphFormat.KeepTogether = paragraphFormat.KeepLinesTogether ? -1 : 0;
                paragraphStyle.ParagraphFormat.PageBreakBefore = paragraphFormat.PageBreakBefore ? -1 : 0;

                paragraphStyle.Font.Bold = paragraphFormat.FontPreferences.IsBold ? -1 : 0;
                paragraphStyle.Font.Italic = paragraphFormat.FontPreferences.IsItalic ? -1 : 0;

                paragraphStyle.Font.Underline = paragraphFormat.FontPreferences.UnderlineType;

                if (paragraphFormat.IsLeftAligned)
                {
                    paragraphStyle.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphLeft;
                }
                else if (paragraphFormat.IsCenterAligned)
                {
                    paragraphStyle.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphCenter;
                }
                else if (paragraphFormat.IsRightAligned)
                {
                    paragraphStyle.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphRight;
                }
                else if (paragraphFormat.IsJustifiedAligned)
                {
                    paragraphStyle.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphJustify;
                }

                if (paragraphFormat.BordersAreEnabled)
                {
                    paragraphStyle.Borders.Enable = -1;
                    paragraphStyle.Borders.Shadow = paragraphFormat.BorderHasShadow;
                    paragraphStyle.Borders.DistanceFromLeft = paragraphFormat.BorderDistanceFromLeft;
                    paragraphStyle.Borders.DistanceFromTop = paragraphFormat.BorderDistanceFromTop;
                    paragraphStyle.Borders.DistanceFromRight = paragraphFormat.BorderDistanceFromRight;
                    paragraphStyle.Borders.DistanceFromBottom = paragraphFormat.BorderDistanceFromBottom;

                    Border leftBorder = paragraphStyle.Borders[WdBorderType.wdBorderLeft];
                    leftBorder.LineStyle = paragraphFormat.LeftBorderLineStyle;
                    leftBorder.LineWidth = paragraphFormat.LeftBorderLineWidth;
                    leftBorder.Color = paragraphFormat.LeftBorderColor;

                    Border topBorder = paragraphStyle.Borders[WdBorderType.wdBorderTop];
                    topBorder.LineStyle = paragraphFormat.TopBorderLineStyle;
                    topBorder.LineWidth = paragraphFormat.TopBorderLineWidth;
                    topBorder.Color = paragraphFormat.TopBorderColor;

                    Border rightBorder = paragraphStyle.Borders[WdBorderType.wdBorderRight];
                    rightBorder.LineStyle = paragraphFormat.RightBorderLineStyle;
                    rightBorder.LineWidth = paragraphFormat.RightBorderLineWidth;
                    rightBorder.Color = paragraphFormat.RightBorderColor;

                    Border bottomBorder = paragraphStyle.Borders[WdBorderType.wdBorderBottom];
                    bottomBorder.LineStyle = paragraphFormat.BottomBorderLineStyle;
                    bottomBorder.LineWidth = paragraphFormat.BottomBorderLineWidth;
                    bottomBorder.Color = paragraphFormat.BottomBorderColor;

                    Border horizontalBorder = paragraphStyle.Borders[WdBorderType.wdBorderHorizontal];
                    horizontalBorder.LineStyle = WdLineStyle.wdLineStyleNone;

                    Shading shading = paragraphStyle.Shading;
                    shading.Texture = paragraphFormat.BorderShadingTexture;
                    shading.BackgroundPatternColor = paragraphFormat.BorderShadingBackgroundColor;
                    shading.ForegroundPatternColor = paragraphFormat.BorderShadingForegroundColor;
                }
                else
                {
                    paragraphStyle.Borders.Enable = 0;
                }

                try
                {
                    Style baseStyle = ThisAddIn.Instance.Application.ActiveDocument.Styles[baseStyleName];
                    paragraphStyle.LanguageID = baseStyle.LanguageID;
                }
                catch(Exception ex)
                {
                    Style normalStyle = ThisAddIn.Instance.Application.ActiveDocument.Styles[WdBuiltinStyle.wdStyleNormal];
                    paragraphStyle.LanguageID = numberingLevel.ParagraphFormat.FontPreferences.LanguageId != WdLanguageID.wdLanguageNone ? numberingLevel.ParagraphFormat.FontPreferences.LanguageId : normalStyle.LanguageID;
                }
            }
        }

        private void UpdateNumberingLevelFromParagraphStyle(ParagraphNumberingLevel numberingLevel, Style paragraphStyle, string baseStyleName, bool isContinuationStyle = false)
        {
            if (paragraphStyle?.NameLocal != null && paragraphStyle.NameLocal != Utils.GetLocalizedStyleNameFromBuiltInStyle(WdBuiltinStyle.wdStyleNormal) && paragraphStyle.NameLocal != Utils.GetLocalizedStyleNameFromBuiltInStyle(WdBuiltinStyle.wdStyleDefaultParagraphFont))
            {
                ParagraphFormat paragraphFormat = isContinuationStyle ? numberingLevel.ContinuationParagraphFormat : numberingLevel.ParagraphFormat;

                if (paragraphStyle.Visibility && paragraphStyle.UnhideWhenUsed)
                {
                    paragraphFormat.HideUntilUsed = true;
                }
                else
                {
                    paragraphFormat.HideUntilUsed = false;
                }

                paragraphStyle.set_BaseStyle(baseStyleName);

                if (!string.IsNullOrEmpty(paragraphStyle?.Font?.Name))
                {
                    paragraphFormat.FontPreferences.Name = paragraphStyle.Font.Name;
                }

                if (paragraphStyle?.Font?.Size != null && paragraphStyle.Font.Size > 0)
                {
                    paragraphFormat.FontPreferences.Size = paragraphStyle.Font.Size;
                }

                paragraphFormat.FontPreferences.IsAllCaps = paragraphStyle.Font.AllCaps == -1 ? true : false;
                paragraphFormat.FontPreferences.IsSmallCaps = paragraphStyle.Font.SmallCaps == -1 ? true : false;

                paragraphFormat.FontPreferences.HexColor = FontUtil.ConvertWdColorToHex(paragraphStyle.Font.Color);

                paragraphFormat.SpacingBefore = paragraphStyle.ParagraphFormat.SpaceBefore;
                paragraphFormat.SpacingAfter = paragraphStyle.ParagraphFormat.SpaceAfter;

                if (paragraphStyle.ParagraphFormat.LineSpacing > 0)
                {
                    paragraphFormat.LineSpacingValue = paragraphStyle.ParagraphFormat.LineSpacing;
                }

                paragraphFormat.LineSpacingType = paragraphStyle.ParagraphFormat.LineSpacingRule;

                paragraphFormat.LeftIndent = app.PointsToInches(paragraphStyle.ParagraphFormat.LeftIndent);
                paragraphFormat.RightIndent = app.PointsToInches(paragraphStyle.ParagraphFormat.RightIndent);
                paragraphFormat.SpecialIndentValue = app.PointsToInches(Math.Abs(paragraphStyle.ParagraphFormat.FirstLineIndent));

                if (paragraphStyle.ParagraphFormat.FirstLineIndent == 0)
                {
                    paragraphFormat.IsWrapToNumberIndent = true;
                }
                else if (paragraphStyle.ParagraphFormat.FirstLineIndent < 0)
                {
                    paragraphFormat.IsHangIndent = true;
                    paragraphFormat.LeftIndent -= paragraphFormat.SpecialIndentValue;
                }
                else
                {
                    paragraphFormat.IsWrapToMarginIndent = true;
                }

                paragraphFormat.NoSpaceBetweenParagraphsOfSameStyle = paragraphStyle.NoSpaceBetweenParagraphsOfSameStyle;
                paragraphFormat.OutlineLevel = paragraphStyle.ParagraphFormat.OutlineLevel;
                paragraphFormat.WindowControl = paragraphStyle.ParagraphFormat.WidowControl == -1 ? true : false;
                paragraphFormat.KeepWithNext = paragraphStyle.ParagraphFormat.KeepWithNext == -1 ? true : false;
                paragraphFormat.KeepLinesTogether = paragraphStyle.ParagraphFormat.KeepTogether == -1 ? true : false;
                paragraphFormat.PageBreakBefore = paragraphStyle.ParagraphFormat.PageBreakBefore == -1 ? true : false;

                paragraphFormat.FontPreferences.IsBold = paragraphStyle.Font.Bold == -1 ? true : false;
                paragraphFormat.FontPreferences.IsItalic = paragraphStyle.Font.Italic == -1 ? true : false;
                paragraphFormat.FontPreferences.UnderlineType = paragraphStyle.Font.Underline;

                paragraphFormat.IsLeftAligned = paragraphStyle.ParagraphFormat.Alignment == WdParagraphAlignment.wdAlignParagraphLeft ? true : false;
                paragraphFormat.IsCenterAligned = paragraphStyle.ParagraphFormat.Alignment == WdParagraphAlignment.wdAlignParagraphCenter ? true : false;
                paragraphFormat.IsRightAligned = paragraphStyle.ParagraphFormat.Alignment == WdParagraphAlignment.wdAlignParagraphRight ? true : false;
                paragraphFormat.IsJustifiedAligned = paragraphStyle.ParagraphFormat.Alignment == WdParagraphAlignment.wdAlignParagraphJustify ? true : false;

                if (paragraphStyle.Borders.Enable == -1)
                {
                    paragraphFormat.BordersAreEnabled = true;
                    paragraphFormat.BorderHasShadow = paragraphStyle.Borders.Shadow;
                    paragraphFormat.BorderDistanceFromLeft = paragraphStyle.Borders.DistanceFromLeft;
                    paragraphFormat.BorderDistanceFromTop = paragraphStyle.Borders.DistanceFromTop;
                    paragraphFormat.BorderDistanceFromRight = paragraphStyle.Borders.DistanceFromRight;
                    paragraphFormat.BorderDistanceFromBottom = paragraphStyle.Borders.DistanceFromBottom;

                    Border leftBorder = paragraphStyle.Borders[WdBorderType.wdBorderLeft];
                    paragraphFormat.LeftBorderLineStyle = leftBorder.LineStyle;
                    paragraphFormat.LeftBorderLineWidth = leftBorder.LineWidth;
                    paragraphFormat.LeftBorderColor = leftBorder.Color;

                    Border topBorder = paragraphStyle.Borders[WdBorderType.wdBorderTop];
                    paragraphFormat.TopBorderLineStyle = topBorder.LineStyle;
                    paragraphFormat.TopBorderLineWidth = topBorder.LineWidth;
                    paragraphFormat.TopBorderColor = topBorder.Color;

                    Border rightBorder = paragraphStyle.Borders[WdBorderType.wdBorderRight];
                    paragraphFormat.RightBorderLineStyle = rightBorder.LineStyle;
                    paragraphFormat.RightBorderLineWidth = rightBorder.LineWidth;
                    paragraphFormat.RightBorderColor = rightBorder.Color;

                    Border bottomBorder = paragraphStyle.Borders[WdBorderType.wdBorderBottom];
                    paragraphFormat.BottomBorderLineStyle = bottomBorder.LineStyle;
                    paragraphFormat.BottomBorderLineWidth = bottomBorder.LineWidth;
                    paragraphFormat.BottomBorderColor = bottomBorder.Color;

                    Border horizontalBorder = paragraphStyle.Borders[WdBorderType.wdBorderHorizontal];
                    horizontalBorder.LineStyle = WdLineStyle.wdLineStyleNone;

                    Shading shading = paragraphStyle.Shading;
                    paragraphFormat.BorderShadingTexture = shading.Texture;
                    paragraphFormat.BorderShadingBackgroundColor = shading.BackgroundPatternColor;
                    paragraphFormat.BorderShadingForegroundColor = shading.ForegroundPatternColor;
                }
                else
                {
                    paragraphFormat.BordersAreEnabled = false;
                }

                paragraphFormat.FontPreferences.LanguageId = paragraphStyle.LanguageID;
            }
        }

        private void EnsureBaseStyleExist(ParagraphNumberingScheme paragraphNumberingScheme)
        {
            if (!ListTemplateUtil.CheckStyleExists(paragraphNumberingScheme.BaseStyleName))
            {
                string replacementStyleName = Utils.GetStandardStyleName();
                new InfowareInformationWindow(string.Format(LanguageManager.GetTranslation(LanguageConstants.TheBaseStyleWasNotFound, "The style {0} was not found in the current document.\nThe chosen numbering system will use {1} as its base style."), paragraphNumberingScheme.BaseStyleName, replacementStyleName), Title).ShowDialog();
                paragraphNumberingScheme.BaseStyleName = replacementStyleName;
            }
        }

        private void RemoveNewLinePrefixIfNecessary(ParagraphNumberingScheme paragraphNumberingScheme)
        {
            List<List> lists = ListTemplateUtil.GetAllListsUsingListTemplate(ListTemplateUtil.GetListTemplateName(paragraphNumberingScheme));

            foreach (List list in lists)
            {
                ListTemplateUtil.RemoveNewLinePrefix(list);
            }
        }

        private void AddNewLinePrefixIfNecessary(ParagraphNumberingScheme paragraphNumberingScheme)
        {
            List<List> lists = ListTemplateUtil.GetAllListsUsingListTemplate(ListTemplateUtil.GetListTemplateName(paragraphNumberingScheme));

            foreach (ParagraphNumberingLevel numberingLevel in paragraphNumberingScheme.ParagraphNumberingLevels)
            {
                if (numberingLevel.TrailingCharacter == TrailingCharacter.NEW_LINE)
                {
                    foreach (List list in lists)
                    {
                        ListTemplateUtil.AddNewLinePrefix(list, numberingLevel.Index);
                    }
                }
            }
        }

        private int GetRgbFromWpfColor(string wpfColor)
        {
            // Removes the leading #
            string color = wpfColor.Substring(1);

            // Skip transparency
            if (color.Length == 8)
            {
                color = color.Substring(2);
            }

            string r = color.Substring(0, 2);
            string g = color.Substring(2, 2);
            string b = color.Substring(4, 2);

            // Directly converting the value to hex renders the wrong color in Word (that is, int.Parse(color, NumberStyles.HexNumber))
            // https://stackoverflow.com/questions/17286329/c-sharp-word-interop-automation-2013-set-font-color-to-an-rgb-value
            return int.Parse(r, NumberStyles.HexNumber) + 0x100 * int.Parse(g, NumberStyles.HexNumber) + 0x10000 * int.Parse(b, NumberStyles.HexNumber);
        }

        private ParagraphNumberingScheme GetActiveDocumentParagraphNumberingScheme(ListTemplate listTemplate)
        {
            return ParagraphNumberingSchemeManager.GetActiveDocumentParagraphNumberingScheme(listTemplate);
        }

        private List<ParagraphNumberingScheme> GetAllParagraphNumberingSchemes()
        {
            return ParagraphNumberingSchemeManager.GetAllParagraphNumberingSchemes();
        }

        private List<ParagraphNumberingScheme> GetAllParagraphNumberingSchemesInActiveDocument()
        {
            return ParagraphNumberingSchemeManager.GetAllParagraphNumberingSchemesInActiveDocument();
        }

        private static List<ParagraphNumberingScheme> GetAllParagraphNumberingSchemes(IList<ParagraphNumberingSchemeType> paragraphNumberingSchemeTypes)
        {
            return ParagraphNumberingSchemeManager.GetAllParagraphNumberingSchemes(paragraphNumberingSchemeTypes);
        }

        private string GetListTemplateInsertionWarningMessage(string usedListTemplateName, string replacementListTemplateName)
        {
            return $@"The current selection is already using a numbering scheme '{usedListTemplateName}'. If
you click Yes, all occurences of the '{usedListTemplateName}' numbering scheme will be replaced in the document with the chosen scheme: '{replacementListTemplateName}' and the
original scheme will be removed.

Do you want to replace the current numbering scheme with your chosen numbering scheme?

If not, answer No and the new scheme will be added in a new paragraph";
        }

        private void SetSelectedParagraphNumberingScheme(ParagraphNumberingScheme paragraphNumberingScheme)
        {
            if (paragraphNumberingScheme.ToSync != null)
            {
                Document documentToExtractFrom = ThisAddIn.FirmListDocument;
                if (ParagraphNumberingSchemeManager != null && ParagraphNumberingSchemeManager.ActiveDocumentParagraphNumberingSchemes != null && ParagraphNumberingSchemeManager.ActiveDocumentParagraphNumberingSchemes.Count > 0 && ParagraphNumberingSchemeManager.ActiveDocumentParagraphNumberingSchemes.Contains(paragraphNumberingScheme))
                {
                    documentToExtractFrom = ThisAddIn.Instance.Application.ActiveDocument;
                }

                //If Active Document templates will be synched the same way, FirmDocument parameter needs to be removed for that case.
                ParagraphNumberingSchemeManager.SyncParagraphNumberingLevelsWithListTemplate(paragraphNumberingScheme, paragraphNumberingScheme.ToSync, documentToExtractFrom);
                paragraphNumberingScheme.ToSync = null;
            }

            IsSelectedParagraphNumberingSchemeReadOnly = ParagraphNumberingSchemeManager.IsReadOnlyParagraphNumberingScheme(paragraphNumberingScheme);

            if (IsSelectedParagraphNumberingSchemeReadOnly)
            {
                paragraphNumberingScheme = paragraphNumberingScheme.Clone();
                paragraphNumberingScheme.IsAddedToDocument = true;
            }

            SelectedParagraphNumberingScheme = paragraphNumberingScheme;

            SelectedParagraphNumberingLevel = SelectedParagraphNumberingScheme.ParagraphNumberingLevels[0];

            SetParagraphNumberingLevelPropertyChangeHandler(SelectedParagraphNumberingScheme);

            selectedParagraphIndex = CbParagraphNumberingLevels.SelectedIndex = 0;

            ParagraphUtil.PopulateParagraphNumberingLevels(LevelUserControl.FdParagraphNumberingLevels.Blocks, SelectedParagraphNumberingScheme, OnParagraphClick);

            TbNumberingSchemeDescription.Text = SelectedParagraphNumberingScheme.Description;

            ParagraphNumberingSchemeLevelsContainer.IsEnabled = true;

            if (ListTemplateUtil.IsListTemplateUsedInSelection())
            {
                IsSelectedNumberingSchemeUsedInSelection =
                    ListTemplateUtil.GetNameOfListTemplateUsedInSelection() == ListTemplateUtil.GetListTemplateName(SelectedParagraphNumberingScheme)
                && !IsSelectedParagraphNumberingSchemeReadOnly;
            }
            else
            {
                IsSelectedNumberingSchemeUsedInSelection = false;
            }

            CbExcludeAllLevelsFromToc.IsChecked = GetExcludeAllLevelsIsChecked();
        }

        private void ClearParagraphNumberingSchemeSelection()
        {
            SelectedParagraphNumberingLevel = null;
            SelectedParagraphNumberingScheme = null;
            selectedParagraphIndex = CbParagraphNumberingLevels.SelectedIndex = 0;

            TbNumberingSchemeDescription.Text = "";
            CbExcludeAllLevelsFromToc.IsChecked = false;
            ParagraphUtil.ClearParagraphs(LevelUserControl.FdParagraphNumberingLevels.Blocks);

            ParagraphNumberingSchemeLevelsContainer.IsEnabled = false;
        }

        private void OnDeleteSyncedDocumentSchemeButtonClick(object sender, RoutedEventArgs e)
        {
            if (sender != null)
            {
                var paragraphNumberingScheme = (ParagraphNumberingScheme)((MenuItem)sender).DataContext;
                if (paragraphNumberingScheme != null)
                {
                    var confirmationWindow = new InfowareConfirmationWindow("Confirm the removal this numbering scheme?", "Infoware remove numbering scheme");
                    confirmationWindow.ShowDialog();

                    if (confirmationWindow.Result == MessageBoxResult.OK)
                    {
                        using (new LoadingWindowWrapper())
                        {
                            string listTemplateName = ListTemplateUtil.GetListTemplateName(paragraphNumberingScheme);
                            using (new IntensiveDocumentInteractionWrapper(undoTransactionName: $"Deleted {listTemplateName}"))
                            {
                                ListTemplateUtil.DeleteListTemplateAndLinkedStyles(paragraphNumberingScheme);

                                ClearParagraphNumberingSchemeSelection();
                                IsListTemplateUsedInDocumentSelection = ListTemplateUtil.IsListTemplateUsedInSelection();

                                if (AdminPanelWebApi.DeleteParagraphNumberingScheme(paragraphNumberingScheme.Name))
                                {
                                    ParagraphNumberingSchemeManager.RemoveSyncedParagraphNumberingScheme(paragraphNumberingScheme);
                                }
                            }
                        }
                    }
                }
            }
        }

        private void CbExcludeAllLevelsFromToc_Click(object sender, RoutedEventArgs e)
        {
            var allLevelsCheckbox = (sender as System.Windows.Controls.CheckBox);
            var selectedParagraphNumberingScheme = GetSelectedParagraphNumberingScheme();

            if (selectedParagraphNumberingScheme != null && selectedParagraphNumberingScheme.ParagraphNumberingLevels != null && selectedParagraphNumberingScheme.ParagraphNumberingLevels.Count > 0)
            {
                foreach (var numberingLevel in selectedParagraphNumberingScheme.ParagraphNumberingLevels)
                {
                    if (allLevelsCheckbox?.IsChecked == true)
                    {
                        numberingLevel.ParagraphFormat.OutlineLevel = WdOutlineLevel.wdOutlineLevelBodyText;
                    }
                    else
                    {
                        numberingLevel.ParagraphFormat.OutlineLevel = GetOutlineLevelBasedOnLevelIndex(numberingLevel.Index);
                    }
                }
            }
        }

        private bool GetExcludeAllLevelsIsChecked()
        {
            var excludeAllLevelesIsChecked = false;
            var selectedParagraphNumberingScheme = GetSelectedParagraphNumberingScheme();

            if (selectedParagraphNumberingScheme != null && selectedParagraphNumberingScheme.ParagraphNumberingLevels != null && selectedParagraphNumberingScheme.ParagraphNumberingLevels.Count > 0)
            {
                if (selectedParagraphNumberingScheme.ParagraphNumberingLevels.Where(x => x.ParagraphFormat.OutlineLevel == WdOutlineLevel.wdOutlineLevelBodyText).Count() == selectedParagraphNumberingScheme.ParagraphNumberingLevels.Count)
                {
                    excludeAllLevelesIsChecked = true;
                }
            }

            return excludeAllLevelesIsChecked;
        }

        private WdOutlineLevel GetOutlineLevelBasedOnLevelIndex(int numberingLevelIndex)
        {
            var outlineLevel = WdOutlineLevel.wdOutlineLevelBodyText;

            if (numberingLevelIndex != 0)
            {
                switch (numberingLevelIndex)
                {
                    case 1: outlineLevel = WdOutlineLevel.wdOutlineLevel1; break;
                    case 2: outlineLevel = WdOutlineLevel.wdOutlineLevel2; break;
                    case 3: outlineLevel = WdOutlineLevel.wdOutlineLevel3; break;
                    case 4: outlineLevel = WdOutlineLevel.wdOutlineLevel4; break;
                    case 5: outlineLevel = WdOutlineLevel.wdOutlineLevel5; break;
                    case 6: outlineLevel = WdOutlineLevel.wdOutlineLevel6; break;
                    case 7: outlineLevel = WdOutlineLevel.wdOutlineLevel7; break;
                    case 8: outlineLevel = WdOutlineLevel.wdOutlineLevel8; break;
                    case 9: outlineLevel = WdOutlineLevel.wdOutlineLevel9; break;
                }
            }

            return outlineLevel;
        }

		protected override void OnClosed(EventArgs e)
		{
			if(wasParagraphNumberingToolbarManagerVisible)
				ParagraphNumberingToolbarManager.Instance.ToggleVisibility(true);
			base.OnClosed(e);
		}
	}
}
