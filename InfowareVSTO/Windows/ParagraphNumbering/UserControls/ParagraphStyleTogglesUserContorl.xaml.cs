﻿using InfowareVSTO.Windows.ParagraphNumbering.Interfaces;
using InfowareVSTO.Windows.ParagraphNumbering.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace InfowareVSTO.Windows.ParagraphNumbering.UserControls
{
    /// <summary>
    /// Interaction logic for ParagraphStyleTogglesUserContorl.xaml
    /// </summary>
    public partial class ParagraphStyleTogglesUserContorl : UserControl 
    {
        public ParagraphStyleTogglesUserContorl()
        {
            InitializeComponent();
        }

        private void HangButton_Click(object sender, RoutedEventArgs e)
        {
            SetParagrapFormatOnListLevel(SpecialParagraphIndentation.HANGING);
        }

        private void WrapToNumberButton_Click(object sender, RoutedEventArgs e)
        {
            SetParagrapFormatOnListLevel(SpecialParagraphIndentation.NONE);
        }

        private void WrapToMarginButton_Click(object sender, RoutedEventArgs e)
        {
            SetParagrapFormatOnListLevel(SpecialParagraphIndentation.FIRST_LINE);
        }

        private void SetParagrapFormatOnListLevel(SpecialParagraphIndentation specialParagraphIndentationType)
        {
            MainAdvancedOptionsInterface parentWindow = null;
            if(this.DataContext is ParagraphNumberingMainWindow)
            {
                parentWindow = (this.DataContext as ParagraphNumberingMainWindow);
            }
            else if(this.DataContext is ParagraphNumberingAdvancedOptions)
            {
                parentWindow = (this.DataContext as ParagraphNumberingAdvancedOptions);
            }

            if (parentWindow != null)
            {
                var listTemplate = ListTemplateUtil.GetListTemplateByName(parentWindow?.SelectedParagraphNumberingScheme?.Name != null ? parentWindow?.SelectedParagraphNumberingScheme?.Name : "");
                if (listTemplate != null && parentWindow?.SelectedParagraphNumberingLevel != null)
                {
                    var listLevel = listTemplate.ListLevels[parentWindow.SelectedParagraphNumberingLevel.Index];
                    if (listLevel != null)
                    {
                        if(specialParagraphIndentationType == SpecialParagraphIndentation.HANGING)
                        {
                            parentWindow.SelectedParagraphNumberingLevel.ParagraphFormat.LeftIndent = Globals.ThisAddIn.Application.PointsToInches(listLevel.NumberPosition);
                            parentWindow.SelectedParagraphNumberingLevel.ParagraphFormat.SpecialIndentType = SpecialParagraphIndentation.HANGING;
                            parentWindow.SelectedParagraphNumberingLevel.ParagraphFormat.SpecialIndentValue = Globals.ThisAddIn.Application.PointsToInches(listLevel.TabPosition - listLevel.NumberPosition);
                        }
                        else if(specialParagraphIndentationType == SpecialParagraphIndentation.NONE)
                        {
                            parentWindow.SelectedParagraphNumberingLevel.ParagraphFormat.LeftIndent = Globals.ThisAddIn.Application.PointsToInches(listLevel.NumberPosition);
                            parentWindow.SelectedParagraphNumberingLevel.ParagraphFormat.SpecialIndentType = SpecialParagraphIndentation.NONE;
                            parentWindow.SelectedParagraphNumberingLevel.ParagraphFormat.SpecialIndentValue = 0;
                        }
                        else if(specialParagraphIndentationType == SpecialParagraphIndentation.FIRST_LINE)
                        {
                            parentWindow.SelectedParagraphNumberingLevel.ParagraphFormat.LeftIndent = 0;
                            parentWindow.SelectedParagraphNumberingLevel.ParagraphFormat.SpecialIndentType = SpecialParagraphIndentation.FIRST_LINE;
                            parentWindow.SelectedParagraphNumberingLevel.ParagraphFormat.SpecialIndentValue = Globals.ThisAddIn.Application.PointsToInches(listLevel.NumberPosition);
                        }
                    }
                }
            }
        }
    }
}
