﻿using System;
using System.Windows;
using InfowareVSTO.Util;
using InfowareVSTO.Windows.ParagraphNumbering.Dialogs;
using Microsoft.Office.Interop.Word;
using Style = Microsoft.Office.Interop.Word.Style;

namespace InfowareVSTO.Windows.ParagraphNumbering
{
    /// <summary>
    /// Interaction logic for ParagraphNumberingAdjustmentWindow.xaml
    /// </summary>
    public partial class ParagraphNumberingAdjustmentWindow : InfowareWindow
    {
        private const string RESTART_NUMBERING_MESSAGEBOX_TITLE = "Infoware restart numbering.";

        private const string CONTINUE_NUMBERING_MESSAGEBOX_TITLE = "Infoware continue numbering.";

        private const string INSERT_LIST_NUM_MESSAGEBOX_TITLE = "Infoware insert ListNum.";

        private const string RESTART_LIST_NUM_MESSAGEBOX_TITLE = "Infoware restart ListNum.";

        private const string PARAGRAPH_IS_NOT_OUTLINE_NUMBERED = "This paragraph is not Outline numbered.";

        private const string TRAILING_LIST_NUM_CHARACTER = "\t";

        private const string LIST_NUM_START_PARAM = @"\s1";

        // TODO: read from config file
        private const bool listNumHangingIndent = true;

        // TODO: read from config file
        private static object listBehaviour = WdDefaultListBehavior.wdWord10ListBehavior;

        private static object falseValue = false;

        // TODO: read from config file
        public bool EnableCustomStartNumberButton { get; set; } = true;

        private int? UserSuppliedListStartingNumber;		

		public ParagraphNumberingAdjustmentWindow()
        {
            InitializeComponent();

            DataContext = this;			
		}

        private void OnButtonRestartNumberingClick(object sender, System.Windows.RoutedEventArgs e)
        {
            Range selectionRange = DocumentUtil.GetSelection().Range;

            if (selectionRange.ListFormat.ListType != WdListType.wdListNoNumbering)
            {
                if (selectionRange.Fields.Count != 0)
                {
                    new InfowareInformationWindow("Use the 'Restart ListNum' button for ListNum fields", RESTART_NUMBERING_MESSAGEBOX_TITLE).ShowDialog();
                    Close();
                    return;
                }
                ListTemplateUtil.RestartNumberingInSelection();
                Close();
            }
            else
            {
                ShowParagraphNotOutlinedMessage(RESTART_NUMBERING_MESSAGEBOX_TITLE);			
			}
        }

        private void OnButtonContinueNumberingClick(object sender, System.Windows.RoutedEventArgs e)
        {
            Range selectionRange = DocumentUtil.GetSelection().Range;

            if (selectionRange.ListFormat.ListType != WdListType.wdListNoNumbering)
            {
                if (selectionRange.ListFormat.CanContinuePreviousList(selectionRange.ListFormat.ListTemplate) != WdContinue.wdContinueDisabled)
                {
                    ListTemplateUtil.ContinueNumberingInSelection();
                    Close();
                }
                else
                {
                    new InfowareInformationWindow("This list cannot be continued. Check the Style definition.", CONTINUE_NUMBERING_MESSAGEBOX_TITLE).ShowDialog();
                }
            }
            else
            {
                ShowParagraphNotOutlinedMessage(CONTINUE_NUMBERING_MESSAGEBOX_TITLE);
            }
        }

        private void OnButtonStartAtNumberingClick(object sender, System.Windows.RoutedEventArgs e)
        {
            using (new IntensiveDocumentInteractionWrapper(undoTransactionName: "Custom level start"))
            {
                Selection selection = DocumentUtil.GetSelection();
                Range selectionRange = selection.Range;

                //ListFormat listFormat = selectionRange.ListFormat;
                //WdOutlineLevel outlineLevel = selectionRange.ParagraphFormat.OutlineLevel;
                int listLevelNumber = selectionRange.ListFormat.ListLevelNumber;

                if (selectionRange.ListFormat.ListType != WdListType.wdListNoNumbering)
                {
                    ListLevel currentListLevel = selectionRange.ListFormat.ListTemplate.ListLevels[listLevelNumber];
                    int currentListStartingNumber = currentListLevel.StartAt;

                    new ParagraphNumberingListLevelInputWindow(currentListStartingNumber, OnListLevelInputWindowOkButtonClick).ShowDialog();

                    if (UserSuppliedListStartingNumber == null)
                    {
                        // The user didn't press the Ok button (probably closed the window)
                        return;
                    }

                    int userSuppliedListStartingNumber = UserSuppliedListStartingNumber.Value;

                    //bool indentCheck = false;

                    //float leftIndent = 0;
                    //float styleLeftIndent = 0;
                    //var firstParagraphStyle = (Style) selection.Paragraphs.First.get_Style();

                    //if (selectionRange.Paragraphs.Count == 1)
                    //{
                    //    indentCheck = true;
                    //    styleLeftIndent = firstParagraphStyle.ParagraphFormat.LeftIndent;
                    //    leftIndent = selection.Paragraphs.First.Format.LeftIndent;
                    //}

                    //selection.Range.ListFormat.ApplyListTemplate(selectionRange.ListFormat.ListTemplate, ref falseValue, WdListApplyTo.wdListApplyToThisPointForward, ref listBehaviour);
                    //selection.Range.ListFormat.ListTemplate.ListLevels[listLevelNumber].StartAt = userSuppliedListStartingNumber;

                    //selection.Range.ListFormat.ApplyListTemplate(selectionRange.ListFormat.ListTemplate, ref falseValue, WdListApplyTo.wdListApplyToWholeList, ref listBehaviour);
                    currentListLevel.StartAt = userSuppliedListStartingNumber;

                    //if (indentCheck)
                    //{
                    //    if (firstParagraphStyle.ParagraphFormat.LeftIndent != styleLeftIndent)
                    //    {
                    //        firstParagraphStyle.ParagraphFormat.LeftIndent = styleLeftIndent;
                    //        selection.Paragraphs.First.Format.LeftIndent = leftIndent;
                    //    }
                    //}
                    Close();
                }
                else
                {
                    ShowParagraphNotOutlinedMessage(CONTINUE_NUMBERING_MESSAGEBOX_TITLE);
                }
            }
        }

        private void OnListLevelInputWindowOkButtonClick(int newListStartingNumber)
        {
            UserSuppliedListStartingNumber = newListStartingNumber;
        }

        private void OnButtonInsertListNumClick(object sender, RoutedEventArgs e)
        {
            Selection originalSelection = DocumentUtil.GetSelection();
            int originalStart = originalSelection.Start;
            int originalEnd = originalSelection.End;

            Range selectionRange = originalSelection.Range;

            string usedListTemplateName = selectionRange.ListFormat.ListTemplate?.Name;

            if (string.IsNullOrEmpty(usedListTemplateName))
            {
                new InfowareInformationWindow("The numbering buttons are intended for use inside numbered lists.", INSERT_LIST_NUM_MESSAGEBOX_TITLE).ShowDialog();
                return;
            }

            int listLevelNumber = selectionRange.ListFormat.ListLevelNumber;
            int newListLevelNumber = listLevelNumber + 1;

            if (newListLevelNumber < 1 || newListLevelNumber > selectionRange.ListFormat.ListTemplate.ListLevels.Count)
            {
                new InfowareInformationWindow("Cannot insert ListNum. Level index outside of range", INSERT_LIST_NUM_MESSAGEBOX_TITLE).ShowDialog();
                return;
            }

            using (new IntensiveDocumentInteractionWrapper(undoTransactionName: "ListNum insertion"))
            {
                object listNumText = $"\"{usedListTemplateName}\" \\l {newListLevelNumber}";

                Field field = selectionRange.Fields.Add(selectionRange, WdFieldType.wdFieldListNum, Text: ref listNumText, PreserveFormatting: ref falseValue);
                DocumentUtil.GetSelection().Range.InsertAfter(TRAILING_LIST_NUM_CHARACTER);

                Range rangeAfterTrailingCharacter = DocumentUtil.GetSelection().Range.Duplicate;
                rangeAfterTrailingCharacter.Expand(WdUnits.wdCharacter);
                rangeAfterTrailingCharacter.Collapse(WdCollapseDirection.wdCollapseEnd);
                rangeAfterTrailingCharacter.Select();

                //ListLevel newListLevel = selectionRange.ListFormat.ListTemplate.ListLevels[newListLevelNumber];
                //string newLevelStyleName = newListLevel.LinkedStyle;

                //Style newLevelStyle = ListTemplateUtil.GetParagraphStyleByName(newLevelStyleName);

                //if (newListLevel.Alignment == WdListLevelAlignment.wdListLevelAlignRight)
                //{
                //    // Algorithm ported from the Enterprise extension code

                //    float listNumTabDiff = (newLevelStyle.ParagraphFormat.FirstLineIndent + newLevelStyle.ParagraphFormat.LeftIndent)
                //        - ((float) originalSelection.Information[WdInformation.wdHorizontalPositionRelativeToPage] - selectionRange.Sections.First.PageSetup.LeftMargin);

                //    selectionRange.ParagraphFormat.TabStops[1].Position += listNumTabDiff;
                //}

                //if (listNumHangingIndent)
                //{
                //    // Algorithm ported from the Enterprise extension code

                //    //ListLevel currentListLevel = selection.Range.ListFormat.ListTemplate.ListLevels[listLevelNumber];
                //    var listLevelStyle = (Style) originalSelection.Range.get_Style();

                //    float newHangingIndent = newLevelStyle.ParagraphFormat.LeftIndent;

                //    float firstLineIndentDiff = newLevelStyle.ParagraphFormat.LeftIndent - listLevelStyle.ParagraphFormat.LeftIndent;

                //    originalSelection.Range.ParagraphFormat.LeftIndent = newHangingIndent;
                //    originalSelection.Range.ParagraphFormat.FirstLineIndent -= firstLineIndentDiff;
                //    originalSelection.Range.ParagraphFormat.TabStops.Add(newHangingIndent, WdTabAlignment.wdAlignTabLeft);
                //}

                Selection selection = DocumentUtil.GetSelection();
                selection.Start = originalStart;
                selection.End = originalEnd;
                selection.Select();

                Globals.ThisAddIn.Application.ScreenRefresh();
            }
            Close();
        }

        private void OnButtonRestartListNumClick(object sender, RoutedEventArgs e)
        {
            Range selectionRange = DocumentUtil.GetSelection().Range;

            if (selectionRange.Fields.Count == 1)
            {
                using (new IntensiveDocumentInteractionWrapper(undoTransactionName: "ListNum restart"))
                {
                    Field firstField = selectionRange.Fields[1];
                    Range firstFieldRange = firstField.Code;

                    int listNumStartParamIndex = firstField.Code.Text.IndexOf(LIST_NUM_START_PARAM);

                    if (listNumStartParamIndex == -1)
                    {
                        firstFieldRange.Collapse(WdCollapseDirection.wdCollapseEnd);
                        firstFieldRange.InsertBefore(LIST_NUM_START_PARAM);
                    }
                    else
                    {
                        firstFieldRange.Text = firstFieldRange.Text.Replace(LIST_NUM_START_PARAM, "");
                    }
                }
                Close();
            }
            else
            {
                new InfowareInformationWindow("Select the ListNum field to restart first", RESTART_LIST_NUM_MESSAGEBOX_TITLE).ShowDialog();
            }
        }

        private void ShowParagraphNotOutlinedMessage(string dialogTitle)
        {
            new InfowareInformationWindow(PARAGRAPH_IS_NOT_OUTLINE_NUMBERED, dialogTitle).ShowDialog();
        }
	}

}
