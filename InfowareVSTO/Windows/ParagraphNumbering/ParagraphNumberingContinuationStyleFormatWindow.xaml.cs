﻿using System.Collections.Generic;
using InfowareVSTO.Common.Language;
using InfowareVSTO.Windows.ParagraphNumbering.Model;
using InfowareVSTO.Windows.ParagraphNumbering.Util;
using Microsoft.Office.Interop.Word;

namespace InfowareVSTO.Windows.ParagraphNumbering
{
    /// <summary>
    /// Interaction logic for ParagraphNumberingContinuationStyleFormatWindow.xaml
    /// </summary>
    public partial class ParagraphNumberingContinuationStyleFormatWindow : InfowareWindow
    {
        private ContinuationParagraphFormat originalContinuationParagraphFormat;

        public ContinuationParagraphFormat ContinuationParagraphFormat { get; }

        public static List<LineSpacingOption> LineSpacingOptions { get; }

        public static List<SpecialParagraphIndentationOption> SpecialParagraphIndentationOptions { get; }

        static ParagraphNumberingContinuationStyleFormatWindow()
        {
            LineSpacingOptions = LineSpacingOptionManager.Instance.LineSpacingOptions;

            SpecialParagraphIndentationOptions = SpecialParagraphIndentationOptionManager.Instance.SpecialParagraphIndentationOptions;
        }

        public ParagraphNumberingContinuationStyleFormatWindow(string continuationStyleName, ContinuationParagraphFormat continuationParagraphFormat)
        {
            InitializeComponent();

            Title = LanguageManager.GetTranslation(LanguageConstants.ContinuationStyle, "Continuation Style") + " " + continuationStyleName;

            originalContinuationParagraphFormat = continuationParagraphFormat;

            ContinuationParagraphFormat = originalContinuationParagraphFormat.Clone();

            if (ContinuationParagraphFormat.MirrorNumberingParagraphFormat)
            {
                // Just in case the user altered the numbering paragraph's format, without yet clicking Apply 
                ContinuationParagraphFormat.SyncPropertiesWithNumberingParagraphStyle();
            }

            DataContext = this;

            UpdateComboBoxSelections();

            ContinuationParagraphFormat.PropertyChanged += OnContinuationParagraphFormatPropertyChanged;
        }

        private void UpdateComboBoxSelections()
        {
            CbLineSpacingTypes.SelectedItem = LineSpacingOptionManager.Instance.GetLineSpacingOptionInstance(ContinuationParagraphFormat.LineSpacingType);
            CbSpecial.SelectedItem = SpecialParagraphIndentationOptionManager.Instance.GetSpecialParagraphIndentationInstance(ContinuationParagraphFormat.SpecialIndentType);
        }

        #region Callbacks

        private void OnBtnBorderClick(object sender, System.Windows.RoutedEventArgs e)
        {
            ParagraphFormatUtil.OpenBorderSettingsDialog(ContinuationParagraphFormat);
        }

        private void OnFontPickerButtonClickClick(object sender, System.Windows.RoutedEventArgs e)
        {
            new ParagraphNumberingFontPickerWindow(enableSmallCapsCheckBox: true, unNumberedParagraphForntPreferences: ContinuationParagraphFormat.FontPreferences).ShowDialog();
        }

        private void OnContinuationParagraphFormatPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            UpdateComboBoxSelections();
        }

        private void OnOkButtonClick(object sender, System.Windows.RoutedEventArgs e)
        {
            originalContinuationParagraphFormat.Copy(ContinuationParagraphFormat);
            Close();
        }

        private void OnCancelButtonClick(object sender, System.Windows.RoutedEventArgs e)
        {
            Close();
        }

        private void OnCbLineSpacingTypesSelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            ContinuationParagraphFormat.LineSpacingType = ((LineSpacingOption) CbLineSpacingTypes.SelectedItem).Value;

            switch (ContinuationParagraphFormat.LineSpacingType)
            {
                case WdLineSpacing.wdLineSpace1pt5:
                case WdLineSpacing.wdLineSpaceSingle:
                case WdLineSpacing.wdLineSpaceDouble:
                    TbLineSpacingValue.IsEnabled = false;
                    break;
                default:
                    TbLineSpacingValue.IsEnabled = true;
                    break;
            }
        }

        private void OnCbSpecialSelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            ContinuationParagraphFormat.SpecialIndentType = ((SpecialParagraphIndentationOption) CbSpecial.SelectedItem).Value;
        }

        #endregion
    }
}
