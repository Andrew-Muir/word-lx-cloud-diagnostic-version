﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Media;
using InfowareVSTO.Common.Language;
using InfowareVSTO.Util;
using InfowareVSTO.Windows.ParagraphNumbering.Interfaces;
using InfowareVSTO.Windows.ParagraphNumbering.Model;
using InfowareVSTO.Windows.ParagraphNumbering.Model.Enums;
using InfowareVSTO.Windows.ParagraphNumbering.Repositories;
using InfowareVSTO.Windows.ParagraphNumbering.Repositories.Interfaces;
using InfowareVSTO.Windows.ParagraphNumbering.Util;
using Microsoft.Office.Interop.Word;
using Paragraph = System.Windows.Documents.Paragraph;
using Style = Microsoft.Office.Interop.Word.Style;

namespace InfowareVSTO.Windows.ParagraphNumbering
{
    /// <summary>
    /// Interaction logic for ParagraphNumberingAdvancedOptionsWindow.xaml
    /// </summary>
    public partial class ParagraphNumberingAdvancedOptions : InfowareWindow, INotifyPropertyChanged, MainAdvancedOptionsInterface
    {
        private const float LEVEL_INDENTATION_CHANGE = 0.5f;

        private const float MIN_LEVEL_TAB_POSITION = -22;

        private const float MAX_LEVEL_TAB_POSITION = 22;

        private const float MIN_LEVEL_NUMBER_POSITION = -22;

        private const float MAX_LEVEL_NUMBER_POSITION = 22;

        private const float MIN_SPECIAL_INDENTATION_VALUE = -22;

        private const float MAX_SPECIAL_INDENTATION_VALUE = 22;

        private const float MIN_PARAGRAPH_LEFT_INDENTATION_VALUE = -22;

        private const float MAX_PARAGRAPH_LEFT_INDENTATION_VALUE = 22;

        private static readonly Regex singleDigitRegex = new Regex(@"\d", RegexOptions.Compiled);

        private readonly Action onApplyButtonClick;

        private readonly Action onSaveNewButtonClick;

        private readonly ParagraphNumberingScheme originalParagraphNumberingScheme;

        public ParagraphNumberingScheme SelectedParagraphNumberingScheme { get; set; }

        private int selectedParagraphIndex;

        private ParagraphNumberingLevel selectedParagraphNumberingLevel;

        public ParagraphNumberingLevel SelectedParagraphNumberingLevel
        {
            get => selectedParagraphNumberingLevel;

            set
            {
                selectedParagraphNumberingLevel = value;
                OnPropertyChanged();
            }
        }

        private bool isSelectedParagraphNumberingLevelUsingBullets;

        public bool IsSelectedParagraphNumberingLevelUsingBullets
        {
            get => isSelectedParagraphNumberingLevelUsingBullets;

            set
            {
                isSelectedParagraphNumberingLevelUsingBullets = value;
                OnPropertyChanged();
            }
        }

        private readonly string messageBoxTitle;		

		public event PropertyChangedEventHandler PropertyChanged;

        public static List<SpecialCharacter> SpecialCharacters { get; }

        public static List<LineSpacingOption> LineSpacingOptions { get; }

        public static List<ListNumberStyleOption> ListNumberStyleOptions { get; set; }

        public static List<ListLevelAlignmentOption> ListLevelAlignmentOptions { get; set; }

        public static List<TrailingCharacterOption> TrailingCharacterOptions { get; set; }

        public static List<OutlineLevelOption> OutlineLevelOptions { get; set; }

        public ObservableCollection<PreviousLevelOption> PreviousLevelOptions { get; set; }

        public ObservableCollection<LevelRestartingOption> LevelRestartingOptions { get; set; }

        public static List<SpecialParagraphIndentationOption> SpecialParagraphIndentationOptions { get; set; }

        public static List<string> BaseStyleNames { get; set; }

        public ObservableCollection<string> FollowableStyleNames { get; set; }

        private static IParagraphNumberingSchemeRepository paragraphNumberingSchemeRepository;

        static ParagraphNumberingAdvancedOptions()
        {
            paragraphNumberingSchemeRepository = new ParagraphNumberingSchemeRepository();

            SpecialCharacters = new List<SpecialCharacter>()
            {
                new SpecialCharacter(LanguageManager.GetTranslation(LanguageConstants.EmDash, "Em Dash"), '—'),
                new SpecialCharacter(LanguageManager.GetTranslation(LanguageConstants.EnDash, "En Dash"), '–'),
                new SpecialCharacter(LanguageManager.GetTranslation(LanguageConstants.NonBreakingSpace, "Non-breaking space"), ' '),
                new SpecialCharacter(LanguageManager.GetTranslation(LanguageConstants.Ellipsis, "Ellipsis"), '…'),
                new SpecialCharacter(LanguageManager.GetTranslation(LanguageConstants.MiddleDot, "Middle Dot"), '·')
            };

            LineSpacingOptions = LineSpacingOptionManager.Instance.LineSpacingOptions;

            ListNumberStyleOptions = new List<ListNumberStyleOption>()
            {
                new ListNumberStyleOption(LanguageManager.GetTranslation(LanguageConstants.None, "(none)"), WdListNumberStyle.wdListNumberStyleNone),
                new ListNumberStyleOption("1, 2, 3", WdListNumberStyle.wdListNumberStyleArabic),
                new ListNumberStyleOption("I, II, III", WdListNumberStyle.wdListNumberStyleUppercaseRoman),
                new ListNumberStyleOption("i, ii, iii", WdListNumberStyle.wdListNumberStyleLowercaseRoman),
                new ListNumberStyleOption("A, B, C", WdListNumberStyle.wdListNumberStyleUppercaseLetter),
                new ListNumberStyleOption("a, b, c", WdListNumberStyle.wdListNumberStyleLowercaseLetter),
                new ListNumberStyleOption(LanguageManager.GetTranslation(LanguageConstants.OnestTwondThreerd,"1st, 2nd, 3rd"), WdListNumberStyle.wdListNumberStyleOrdinal),
                new ListNumberStyleOption(LanguageManager.GetTranslation(LanguageConstants.OneTwoThree,"One, Two, Three"), WdListNumberStyle.wdListNumberStyleCardinalText),
                new ListNumberStyleOption(LanguageManager.GetTranslation(LanguageConstants.FirstSecondThird,"First, Second, Third"), WdListNumberStyle.wdListNumberStyleOrdinalText),
                new ListNumberStyleOption(LanguageManager.GetTranslation(LanguageConstants.Bullets,"Bullets"), WdListNumberStyle.wdListNumberStyleBullet),
                new ListNumberStyleOption("1, 1.1, 1.2 (" + LanguageManager.GetTranslation(LanguageConstants.Legal, "legal").ToLower() + ")", WdListNumberStyle.wdListNumberStyleLegal),
                new ListNumberStyleOption("01, 1.01, 1.02 (" + LanguageManager.GetTranslation(LanguageConstants.Legal, "legal").ToLower() + ")", WdListNumberStyle.wdListNumberStyleLegalLZ),
                new ListNumberStyleOption("01, 02, 03", WdListNumberStyle.wdListNumberStyleArabicLZ),
                new ListNumberStyleOption("001, 002, 003", WdListNumberStyle.wdListNumberStyleArabicLZ2),
                new ListNumberStyleOption("0001, 0002, 0003", WdListNumberStyle.wdListNumberStyleArabicLZ3),
                new ListNumberStyleOption("00001, 00002, 00003", WdListNumberStyle.wdListNumberStyleArabicLZ4),
            };

            ListLevelAlignmentOptions = new List<ListLevelAlignmentOption>()
            {
                new ListLevelAlignmentOption(LanguageManager.GetTranslation(LanguageConstants.Left, "Left"), WdListLevelAlignment.wdListLevelAlignLeft),
                new ListLevelAlignmentOption(LanguageManager.GetTranslation(LanguageConstants.Center, "Center"), WdListLevelAlignment.wdListLevelAlignCenter),
                new ListLevelAlignmentOption(LanguageManager.GetTranslation(LanguageConstants.Right, "Right"), WdListLevelAlignment.wdListLevelAlignRight)
            };

            TrailingCharacterOptions = new List<TrailingCharacterOption>()
            {
                new TrailingCharacterOption(LanguageManager.GetTranslation(LanguageConstants.Tab, "Tab"), TrailingCharacter.TAB),
                new TrailingCharacterOption(LanguageManager.GetTranslation(LanguageConstants.Space, "Space"), TrailingCharacter.SPACE),
                new TrailingCharacterOption(LanguageManager.GetTranslation(LanguageConstants.NewLine, "NewLine"), TrailingCharacter.NEW_LINE),
                new TrailingCharacterOption(LanguageManager.GetTranslation(LanguageConstants.None, "None"), TrailingCharacter.NONE),
            };

            OutlineLevelOptions = Enumerable.Range(1, 9).Select(it =>
            new OutlineLevelOption(LanguageManager.GetTranslation(LanguageConstants.Level, "Level") + " " + it, (WdOutlineLevel) it)).ToList();

            OutlineLevelOptions.Add(new OutlineLevelOption(Utils.GetLocalizedStyleNameFromBuiltInStyle(WdBuiltinStyle.wdStyleBodyText), WdOutlineLevel.wdOutlineLevelBodyText));

            SpecialParagraphIndentationOptions = SpecialParagraphIndentationOptionManager.Instance.SpecialParagraphIndentationOptions;

            var tempList = new List<string>()
            {
                Utils.GetLocalizedStyleNameFromBuiltInStyle(WdBuiltinStyle.wdStyleNormal),
                Utils.GetLocalizedStyleNameFromBuiltInStyle(WdBuiltinStyle.wdStyleBodyText),
                "No Spacing",
                Utils.GetLocalizedStyleNameFromBuiltInStyle(WdBuiltinStyle.wdStyleHeading1),
                Utils.GetLocalizedStyleNameFromBuiltInStyle(WdBuiltinStyle.wdStyleHeading2),
                Utils.GetLocalizedStyleNameFromBuiltInStyle(WdBuiltinStyle.wdStyleTitle)
            };

            BaseStyleNames = new List<string>();
            string standardStyleName = Utils.GetStandardStyleName();
            if (!tempList.Contains(standardStyleName))
            {
                BaseStyleNames.Add(standardStyleName);
            }

            BaseStyleNames.AddRange(tempList);
        }

        public ParagraphNumberingAdvancedOptions(ParagraphNumberingScheme selectedParagraphNumberingScheme, Action onApplyButtonClick, Action onSaveNewButtonClick)
        {
            InitializeComponent();

            messageBoxTitle = Title;

            PreviousLevelOptions = new ObservableCollection<PreviousLevelOption>();
            LevelRestartingOptions = new ObservableCollection<LevelRestartingOption>();
            FollowableStyleNames = new ObservableCollection<string>();

            originalParagraphNumberingScheme = selectedParagraphNumberingScheme;
            SelectedParagraphNumberingScheme = selectedParagraphNumberingScheme.Clone();

            SetBaseStyleName();

            SelectedParagraphNumberingLevel = SelectedParagraphNumberingScheme.ParagraphNumberingLevels[0];

            this.onApplyButtonClick = onApplyButtonClick;
            this.onSaveNewButtonClick = onSaveNewButtonClick;

            InitUi();
            DataContext = this;
        }

        private void InitUi()
        {
            SetParagraphNumberingLevelPropertyChangeHandler();
            PopulateParagraphNumberingLevels();

            UpdateFollowableStyleNames();
            UpdateLevelRestartingOptions();
            UpdateComboBoxSelections();
        }

        private void PopulateParagraphNumberingLevels(int selectedParagraphIndex = 0)
        {
            ParagraphUtil.PopulateParagraphNumberingLevels(LevelUserControl.FdParagraphNumberingLevels.Blocks, SelectedParagraphNumberingScheme,
                OnParagraphClick, selectedParagraphIndex);
        }

        private void SetParagraphNumberingLevelPropertyChangeHandler()
        {
            foreach (ParagraphNumberingLevel paragraphNumberingLevel in SelectedParagraphNumberingScheme.ParagraphNumberingLevels)
            {
                paragraphNumberingLevel.ParagraphFormat.PropertyChanged += (object sender, PropertyChangedEventArgs e) => OnParagraphNumberingLevelPropertyChanged(sender, e, paragraphNumberingLevel);
            }
        }

        private void OnParagraphNumberingLevelPropertyChanged(object sender, PropertyChangedEventArgs e, ParagraphNumberingLevel numberingLevel)
        {
            ParagraphUtil.PopulateParagraphNumberingLevels(LevelUserControl.FdParagraphNumberingLevels.Blocks,
                SelectedParagraphNumberingScheme, OnParagraphClick, selectedParagraphIndex);

            // Necessary in order to sync the interaction with the special indent buttons (3 buttons) with the ComboBox selection
            UpdateSpecialParagraphIndentationComboBoxSelection();
        }

        private ListLevelAlignmentOption GetListLevelAlignmentOptionInstance(WdListLevelAlignment numberAlignment)
        {
            return ListLevelAlignmentOptions.First(it => it.Value == numberAlignment);
        }

        private TrailingCharacterOption GetTrailingCharacterOptionInstance(TrailingCharacter trailingCharacter)
        {
            if (trailingCharacter == TrailingCharacter.NONE)
            {
                var listTemplateName = ListTemplateUtil.GetListTemplateName(SelectedParagraphNumberingScheme);
                var lists = ListTemplateUtil.GetAllListsUsingListTemplate(listTemplateName);
                trailingCharacter = ListTemplateUtil.GetNewLineTrailingCharacterFromDocumentListParagraph(trailingCharacter, lists, listTemplateName, SelectedParagraphNumberingLevel);
            }
            
            return TrailingCharacterOptions.First(it => it.Value == trailingCharacter);
        }

        private LevelRestartingOption GetLevelRestartingOptionInstance(int level)
        {
            return LevelRestartingOptions.First(it => it.Value == level);
        }

        private ListNumberStyleOption GetListNumberStyleOptionInstance(WdListNumberStyle listNumberStyle)
        {
            return ListNumberStyleOptions.First(it => it.Value == listNumberStyle);
        }

        private OutlineLevelOption GetOutlineLevelOptionInstance(WdOutlineLevel outlineLevel)
        {
            return OutlineLevelOptions.First(it => it.Value == outlineLevel);
        }

        #region Event handlers

        private void OnParagraphClick(Paragraph selectedParagraph, ParagraphNumberingLevel numberingLevel)
        {
            ParagraphUtil.MarkParagraphAsSelected(LevelUserControl.FdParagraphNumberingLevels.Blocks, selectedParagraph);

            int selectedIndex = ParagraphUtil.GetParagraphIndex(LevelUserControl.FdParagraphNumberingLevels.Blocks, selectedParagraph);

            selectedParagraphIndex = selectedIndex;
            SelectedParagraphNumberingLevel = SelectedParagraphNumberingScheme.ParagraphNumberingLevels[selectedIndex];

            UpdatePreviousLevelOptions();
            UpdateLevelRestartingOptions();

            UpdateComboBoxSelections();

            if (SelectedParagraphNumberingLevel.BulletCharacterIsEnabled)
            {
                UpdateBulletPreviewFont();
            }
        }

        private void UpdateComboBoxSelections()
        {
            CbNumberStyle.SelectedItem = GetListNumberStyleOptionInstance(SelectedParagraphNumberingLevel.NumberStyle);
            CbNumberAlignment.SelectedItem = GetListLevelAlignmentOptionInstance(SelectedParagraphNumberingLevel.NumberAlignment);
            CbTrailingCharacters.SelectedItem = GetTrailingCharacterOptionInstance(SelectedParagraphNumberingLevel.TrailingCharacter);
            CbRestartAfterLevel.SelectedItem = GetLevelRestartingOptionInstance(SelectedParagraphNumberingLevel.RestartAfterLevel);
            CbLineSpacingTypes.SelectedItem = LineSpacingOptionManager.Instance.GetLineSpacingOptionInstance(SelectedParagraphNumberingLevel.ParagraphFormat.LineSpacingType);
            CbOutlineLevels.SelectedItem = GetOutlineLevelOptionInstance(SelectedParagraphNumberingLevel.ParagraphFormat.OutlineLevel);

            UpdateFollowableStyleNamesSelection();

            UpdateSpecialParagraphIndentationComboBoxSelection();
        }

        private void UpdateSpecialParagraphIndentationComboBoxSelection()
        {
            CbSpecial.SelectedItem = SpecialParagraphIndentationOptionManager.Instance.GetSpecialParagraphIndentationInstance(SelectedParagraphNumberingLevel.ParagraphFormat.SpecialIndentType);
        }

        private void UpdatePreviousLevelOptions()
        {
            PreviousLevelOptions.Clear();

            if (selectedParagraphIndex == 0)
            {
                return;
            }

            Enumerable.Range(1, selectedParagraphIndex).Select(it => new PreviousLevelOption(it)).ToList()
                .ForEach(it => PreviousLevelOptions.Add(it));
        }

        private void UpdateFollowableStyleNames()
        {
            FollowableStyleNames.Clear();
            FollowableStyleNames.Add(Utils.GetLocalizedStyleNameFromBuiltInStyle(WdBuiltinStyle.wdStyleNormal));
            FollowableStyleNames.Add(Utils.GetLocalizedStyleNameFromBuiltInStyle(WdBuiltinStyle.wdStyleBodyText));

            string standardStyleName = Utils.GetStandardStyleName();
            if (!FollowableStyleNames.Contains(standardStyleName))
            {
                FollowableStyleNames.Add(standardStyleName);
            }

            var listTemplateName = ListTemplateUtil.GetListTemplateName(SelectedParagraphNumberingScheme);

            if (ParagraphNumberingSchemeManager.Instance.ActiveDocumentParagraphNumberingSchemes.Any(x => (x as ParagraphNumberingScheme).Name == SelectedParagraphNumberingScheme.Name))
            {
                var listTemplate = ListTemplateUtil.GetListTemplateByName(listTemplateName);
                var activeDocumentFollowableStyleNames = ListTemplateUtil.GetFollowableStyleNamesFromListTemplate(listTemplate, SelectedParagraphNumberingScheme);

                if (activeDocumentFollowableStyleNames.Count > 0)
                {
                    foreach (var followableStyleName in activeDocumentFollowableStyleNames)
                    {
                        FollowableStyleNames.Add(followableStyleName);
                    }
                }
            }
            else if (ListTemplateUtil.CheckIfNumberingSchemeIsInFirmListDocument(ParagraphNumberingSchemeManager.Instance.FirmDocumentParagraphNumberingSchemes, SelectedParagraphNumberingScheme.Name))
            {
                var listTemplate = ListTemplateUtil.GetListTemplateByNameFromFirmListDocument(listTemplateName);
                var firmListDocumentFollowableStyleNames = ListTemplateUtil.GetFollowableStyleNamesFromListTemplate(listTemplate, SelectedParagraphNumberingScheme);

                if (firmListDocumentFollowableStyleNames.Count > 0)
                {
                    foreach (var followableStyleName in firmListDocumentFollowableStyleNames)
                    {
                        FollowableStyleNames.Add(followableStyleName);
                    }
                }
            }
            else if (ParagraphNumberingSchemeManager.Instance.SyncedParagraphNumberingSchemes.Any(x => (x as ParagraphNumberingScheme).Name == SelectedParagraphNumberingScheme.Name))
            {
                var personalFollowableStyleNames = ListTemplateUtil.GetFollowableStyleNamesFromSyncedNumberingSchemes(SelectedParagraphNumberingScheme);
                if (personalFollowableStyleNames.Count > 0)
                {
                    foreach (var followableStyleName in personalFollowableStyleNames)
                    {
                        FollowableStyleNames.Add(followableStyleName);
                    }
                }
            }
            else if (ParagraphNumberingSchemeManager.Instance.BuiltInParagraphNumberingSchemes.Any(x => (x as ParagraphNumberingScheme).Name == SelectedParagraphNumberingScheme.Name))
            {
                if (SelectedParagraphNumberingScheme?.ParagraphNumberingLevels != null && SelectedParagraphNumberingScheme.ParagraphNumberingLevels.Count > 0)
                {
                    foreach (var selectedParagraphNumberingLevel in SelectedParagraphNumberingScheme.ParagraphNumberingLevels)
                    {
                        if (!FollowableStyleNames.Contains(selectedParagraphNumberingLevel.ParagraphFormat.StyleToFollow))
                        {
                            FollowableStyleNames.Add(selectedParagraphNumberingLevel.ParagraphFormat.StyleToFollow);
                        }
                    }

                    if (SelectedParagraphNumberingScheme.CreateContinuationParagraphStyles)
                    {
                        foreach (var selectedParagraphNumberingLevel in SelectedParagraphNumberingScheme.ParagraphNumberingLevels)
                        {
                            var continuationStyleToFollowName = SelectedParagraphNumberingScheme.Name + ListTemplateUtil.PARAGRAPH_STYLE_NAME_WORD_SEPARATOR + ListTemplateUtil.CONTINUATION_PARAGRAPH_STYLE_NAME_INDICATOR + ListTemplateUtil.PARAGRAPH_STYLE_NAME_LEVEL_INDICATOR + selectedParagraphNumberingLevel.Index;
                            if (!FollowableStyleNames.Contains(continuationStyleToFollowName))
                            {
                                FollowableStyleNames.Add(continuationStyleToFollowName);
                            }
                        }
                    }
                }
            }

            UpdateFollowableStyleNamesSelection();
        }

        private void UpdateFollowableStyleNamesSelection()
        {
            var styleToFollowIndex = -1;

            var styleToFollow = SelectedParagraphNumberingLevel?.ParagraphFormat?.StyleToFollow?.Split(',').FirstOrDefault()?.Trim();
            if (styleToFollow != null)
            {
                styleToFollowIndex = FollowableStyleNames.IndexOf(styleToFollow);
            }

            if (styleToFollowIndex == -1)
            {
                styleToFollowIndex = FollowableStyleNames.IndexOf(ListTemplateUtil.GetParagraphStyleName(SelectedParagraphNumberingScheme, SelectedParagraphNumberingLevel.Index));
            }

            CbStyleToFollow.SelectedIndex = styleToFollowIndex;
        }

        private void UpdateLevelRestartingOptions()
        {
            LevelRestartingOptions.Clear();

            Enumerable.Range(1, selectedParagraphIndex).ToList().Select(it => new LevelRestartingOption(it.ToString(), it)).ToList()
                .ForEach(it => LevelRestartingOptions.Add(it));

            LevelRestartingOptions.Add(new LevelRestartingOption(LanguageManager.GetTranslation(LanguageConstants.DontRestart, "Don't restart"), 0));
        }

        private void OnBtnNumberingFontClick(object sender, System.Windows.RoutedEventArgs e)
        {
            new ParagraphNumberingFontPickerWindow(() =>
            {
                UpdateBulletPreviewFont();
                RenderPreview();
            }, numberingFontPreferences: SelectedParagraphNumberingLevel.FontPreferences, enableSmallCapsCheckBox: false).ShowDialog();
        }

        private void OnBtnBorderClick(object sender, System.Windows.RoutedEventArgs e)
        {
            ParagraphFormatUtil.OpenBorderSettingsDialog(SelectedParagraphNumberingLevel.ParagraphFormat, GetWindow(this));
        }

        private void OnBtnParagraphFontClick(object sender, System.Windows.RoutedEventArgs e)
        {
            new ParagraphNumberingFontPickerWindow(RenderPreview, SelectedParagraphNumberingLevel.FontPreferences,
                SelectedParagraphNumberingLevel.ParagraphFormat.FontPreferences, SelectedParagraphNumberingLevel.ContinuationParagraphFormat.FontPreferences, true, true).ShowDialog();
        }

        private void OnBulletWindowButtonClick(object sender, System.Windows.RoutedEventArgs e)
        {
            string uiLanguageName = ApplicationUtil.GetUiLanguageName();

            ParagraphNumberingLevel selectedNumberingLevel = SelectedParagraphNumberingLevel;
            string fontName = selectedNumberingLevel.FontPreferences.Name;

            dynamic symbolDialog = Globals.ThisAddIn.Application.Dialogs[WdWordDialog.wdDialogInsertSymbol];
            string originalFontName = symbolDialog.Font;

            // Every bullet is made out of a single character
            int bulletCharacterCode = selectedNumberingLevel.BulletCharacter[0];

            string fontNameShortcutChar = null;
            string charCodeShortcutChar = null;

            if (uiLanguageName == "English")
            {
                fontNameShortcutChar = "F";
                charCodeShortcutChar = "C";
            }
            else if (uiLanguageName == "French")
            {
                fontNameShortcutChar = "P";
                charCodeShortcutChar = "a";
            }

            if (fontNameShortcutChar != null && charCodeShortcutChar != null)
            {
                SendKeys.Send($"%{fontNameShortcutChar}{fontName}");
                SendKeys.Send($"%{charCodeShortcutChar}{bulletCharacterCode}");
            }

            int result = symbolDialog.Display();

            /*
             * The documentation states that -1 means the user pressed the Ok button. Sadly, the value seems to be 
             * -1 even if the user closes the window via X or if they press the Close button
             */

            if (result == -1)
            {
                // This only seems to happen when the user closes the window via X or when Cancel is pressed

                if (symbolDialog.Font == "(normal text)")
                {
                    return;
                }

                selectedNumberingLevel.FontPreferences.Name = symbolDialog.Font;

                int charCode = int.Parse(symbolDialog.CharNum);

                if (charCode < 0)
                {
                    // Only the least significant 12 bits matter
                    charCode = charCode & 0xFFF;
                }

                selectedNumberingLevel.BulletCharacter = ((char) charCode).ToString();

                PopulateParagraphNumberingLevels(selectedParagraphIndex);
                UpdateBulletPreviewFont();
            }
        }

        private void RenderPreview()
        {
            PopulateParagraphNumberingLevels(selectedParagraphIndex);
        }

        private void OnBtnApplyClick(object sender, System.Windows.RoutedEventArgs e)
        {
            using (new LoadingWindowWrapper(false))
            {
                originalParagraphNumberingScheme.Copy(SelectedParagraphNumberingScheme);
                onApplyButtonClick();
            }

            Close();
        }

        private void OnBtnCancelClick(object sender, System.Windows.RoutedEventArgs e)
        {
            Close();
        }

        private void OnBtnSaveNewClick(object sender, System.Windows.RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(TbNewListTemplateDisplayName.Text))
            {
                new InfowareInformationWindow("Invalid template display name.", messageBoxTitle).ShowDialog();
                return;
            }
            else if (string.IsNullOrEmpty(TbNewListTemplateName.Text))
            {
                new InfowareInformationWindow("Invalid template name.", messageBoxTitle).ShowDialog();
                return;
            }

            string styleAliasPrefix = TbNewListTemplateStyleAliasPrefix.Text;

            if (!string.IsNullOrEmpty(styleAliasPrefix))
            {
                if (ParagraphNumberingSchemeManager.Instance.IsStyleAliasPrefixInUse(styleAliasPrefix))
                {
                    new InfowareInformationWindow("The chosen style alias prefix already in use.", messageBoxTitle).ShowDialog();
                    return;
                }
            }

            ParagraphNumberingScheme newParagraphNumberingScheme = SelectedParagraphNumberingScheme.Clone();

            // Required when the user is trying to create a new template from an "Active document" template, which have these properties set to true
            newParagraphNumberingScheme.IsAddedToDocument = newParagraphNumberingScheme.IsApplied = false;

            newParagraphNumberingScheme.DisplayName = TbNewListTemplateDisplayName.Text;
            newParagraphNumberingScheme.Name = TbNewListTemplateName.Text;
            newParagraphNumberingScheme.RootStylePrefix = !string.IsNullOrEmpty(TbStyleNamePrefix.Text) ? TbStyleNamePrefix.Text : TbNewListTemplateName.Text + ListTemplateUtil.PARAGRAPH_STYLE_NAME_WORD_SEPARATOR + ListTemplateUtil.PARAGRAPH_STYLE_NAME_LEVEL_INDICATOR;
            newParagraphNumberingScheme.StyleAliasPrefix = TbNewListTemplateStyleAliasPrefix.Text;

            newParagraphNumberingScheme.BaseStyleName = (string) CbBaseStyleNames.SelectedItem;

            Document documentToExtractFrom = null;
            ListTemplate listTemplate = null;

            if (ParagraphNumberingSchemeManager.Instance?.ActiveDocumentParagraphNumberingSchemes != null && ParagraphNumberingSchemeManager.Instance.ActiveDocumentParagraphNumberingSchemes.Any(x => (x as ParagraphNumberingScheme).Name == originalParagraphNumberingScheme.Name))
            {
                documentToExtractFrom = ThisAddIn.Instance.Application.ActiveDocument;
                listTemplate = ListTemplateUtil.GetListTemplateByName(originalParagraphNumberingScheme?.Name);
            }
            else if (ParagraphNumberingSchemeManager.Instance?.FirmDocumentParagraphNumberingSchemes != null && ListTemplateUtil.CheckIfNumberingSchemeIsInFirmListDocument(ParagraphNumberingSchemeManager.Instance.FirmDocumentParagraphNumberingSchemes, originalParagraphNumberingScheme.Name))
            {
                documentToExtractFrom = ThisAddIn.FirmListDocument;
                listTemplate = ListTemplateUtil.GetListTemplateByNameFromFirmListDocument(originalParagraphNumberingScheme?.Name);
            }

            foreach (ParagraphNumberingLevel numberingLevel in newParagraphNumberingScheme.ParagraphNumberingLevels)
            {
                try
                {
                    if(listTemplate != null && documentToExtractFrom != null)
                    {
                        ListLevel listLevel = listTemplate?.ListLevels[numberingLevel.Index];

                        string paragraphStyleName = ListTemplateUtil.GetParagraphStyleName(originalParagraphNumberingScheme, numberingLevel.Index);
                        if (!string.IsNullOrEmpty(listLevel?.LinkedStyle))
                        {
                            paragraphStyleName = listLevel.LinkedStyle;
                        }

                        Style paragraphStyle = ListTemplateUtil.GetParagraphStyleByName(paragraphStyleName, documentToExtractFrom);
                        if (paragraphStyle != null)
                        {
                            numberingLevel.ParagraphFormat.FontPreferences.LanguageId = paragraphStyle.LanguageID;
                        }

                        string continuationParagraphStyleName = ListTemplateUtil.GetContinuationStyleNameFromLinkedStyleName(listLevel?.LinkedStyle, listLevel.Index);
                        if (string.IsNullOrEmpty(continuationParagraphStyleName))
                        {
                            continuationParagraphStyleName = ListTemplateUtil.GetContinuationParagraphStyleName(originalParagraphNumberingScheme, numberingLevel.Index);
                        }

                        Style continuationParagraphStyle = ListTemplateUtil.GetParagraphStyleByName(continuationParagraphStyleName, documentToExtractFrom);
                        if (continuationParagraphStyle != null)
                        {
                            numberingLevel.ContinuationParagraphFormat.FontPreferences.LanguageId = continuationParagraphStyle.LanguageID;
                        }
                    }

                    string styleToFollow = numberingLevel.ParagraphFormat.StyleToFollow;
                    if (ListTemplateUtil.CheckIfStyleIsContinuationStyle(styleToFollow))
                    {
                        styleToFollow = ListTemplateUtil.GetContinuationParagraphStyleName(newParagraphNumberingScheme, ListTemplateUtil.GetLevelNumberFromStyleName(styleToFollow));
                    }
                    else if (ListTemplateUtil.CheckIfStyleIsParagraphNumberingLevelStyle(styleToFollow))
                    {
                        styleToFollow = ListTemplateUtil.GetParagraphStyleName(newParagraphNumberingScheme, ListTemplateUtil.GetLevelNumberFromStyleName(styleToFollow));
                    }

                    if (string.IsNullOrEmpty(styleToFollow))
                    {
                        continue;
                    }

                    numberingLevel.ParagraphFormat.StyleToFollow = ListTemplateUtil.AppendStyleAliasIfNecessary(styleToFollow, newParagraphNumberingScheme);
                }
                catch(Exception ex)
                {
                    Debug.WriteLine(ex);
                }
            }

            bool storedSuccessfully = paragraphNumberingSchemeRepository.Store(newParagraphNumberingScheme);

            if (storedSuccessfully)
            {
                new InfowareInformationWindow($"Successfully stored template named {newParagraphNumberingScheme.Name}.", messageBoxTitle).ShowDialog();
            }
            else
            {
                new InfowareInformationWindow($"Unable to store template named {newParagraphNumberingScheme.Name}.", messageBoxTitle).ShowDialog();
            }

            onSaveNewButtonClick();
            Close();
        }

        private void OnTbNumberFormatTextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            SelectedParagraphNumberingLevel.NumberFormat = TbNumberFormat.Text;
            PopulateParagraphNumberingLevels(selectedParagraphIndex);
        }

        private void OnCbNumberStyleSelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            WdListNumberStyle initialNumberStyle = SelectedParagraphNumberingLevel.NumberStyle;

            SelectedParagraphNumberingLevel.NumberStyle = ((ListNumberStyleOption) CbNumberStyle.SelectedItem).Value;

            if (SelectedParagraphNumberingLevel.NumberStyle != WdListNumberStyle.wdListNumberStyleBullet)
            {
                TbNumberFormat.Visibility = Visibility.Visible;
                CbSpecialCharacters.Visibility = Visibility.Visible;

                TbBulletPreview.Visibility = Visibility.Collapsed;
                BtnBulletWindow.Visibility = Visibility.Collapsed;

                SelectedParagraphNumberingLevel.BulletCharacterIsEnabled = false;

                /*
                 * The user is switching away from the Bullet style. Set the default font for numbering (usually Times New Roman)
                 * to (among others) correctly render the preview
                 */
               /* if (initialNumberStyle == WdListNumberStyle.wdListNumberStyleBullet)
                {
                    SelectedParagraphNumberingLevel.UseDefaultNumberingFont();
                }*/

                IsSelectedParagraphNumberingLevelUsingBullets = false;
            }
            else
            {
                TbNumberFormat.Visibility = Visibility.Collapsed;
                CbSpecialCharacters.Visibility = Visibility.Collapsed;

                TbBulletPreview.Visibility = Visibility.Visible;
                BtnBulletWindow.Visibility = Visibility.Visible;

                SelectedParagraphNumberingLevel.BulletCharacterIsEnabled = true;

                /*
                 * The user is switching to the Bullet style. Set the default font for numbering (usually Symbol)
                 * to (among others) correctly render the preview
                 */
                if (initialNumberStyle != WdListNumberStyle.wdListNumberStyleBullet)
                {
                    SelectedParagraphNumberingLevel.UseDefaultBulletChar();
                    SelectedParagraphNumberingLevel.UseDefaultBulletFont();
                }

                IsSelectedParagraphNumberingLevelUsingBullets = true;
            }

            PopulateParagraphNumberingLevels(selectedParagraphIndex);

            UpdateBulletPreviewFont();
        }

        private void UpdateBulletPreviewFont()
        {
            if (SelectedParagraphNumberingLevel.FontPreferences.Name != null)
            {
                TbBulletPreview.FontFamily = new FontFamily(SelectedParagraphNumberingLevel.FontPreferences.Name);
            }
        }

        private void OnCbSpecialCharactersSelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            var specialCharacter = (SpecialCharacter) CbSpecialCharacters.SelectedItem;

            int textboxCursorPosition = TbNumberFormat.SelectionStart;
            string specialCharacterValue = specialCharacter.Value.ToString();

            TbNumberFormat.Text = TbNumberFormat.Text.Insert(textboxCursorPosition, specialCharacterValue);

            // Place the cursor after the newly inserted character
            TbNumberFormat.SelectionStart = textboxCursorPosition + 1;
        }

        private void OnCbPreviousLevelsSelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (CbPreviousLevels.SelectedItem == null)
            {
                return;
            }

            var previousLevelOption = ((PreviousLevelOption) CbPreviousLevels.SelectedItem);

            CbPreviousLevels.SelectedItem = null;

            int currentLevel = selectedParagraphIndex + 1;

            MatchCollection matches = MsWordConstants.MSWORD_NUMBERING_REGEX.Matches(TbNumberFormat.Text);

            if (matches.Count == 1)
            {
                // Only contains the current level

                TbNumberFormat.Text = previousLevelOption.Value + TbNumberFormat.Text;
                return;
            }

            Match firstMatch = matches[0];

            int existingPreviousLevel = int.Parse(singleDigitRegex.Match(firstMatch.Value).Value);

            if (existingPreviousLevel == previousLevelOption.Level)
            {
                // Do nothing
                return;
            }
            else if (previousLevelOption.Level < existingPreviousLevel)
            {
                TbNumberFormat.Text = previousLevelOption.Value + TbNumberFormat.Text;
            }
            else
            {
                TbNumberFormat.Text = TbNumberFormat.Text.Insert(firstMatch.Index + firstMatch.Length, previousLevelOption.Value);
            }
        }

        private void OnCbNumberAlignmentSelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            SelectedParagraphNumberingLevel.NumberAlignment = ((ListLevelAlignmentOption) CbNumberAlignment.SelectedItem).Value;
        }

        private void OnCbTrailingCharactersSelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            SelectedParagraphNumberingLevel.TrailingCharacter = ((TrailingCharacterOption) CbTrailingCharacters.SelectedItem).Value;
        }

        private void OnCbRestartAfterLevelSelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            // The selection is cleared when the ObservableCollection is cleared (by UpdateLevelRestartingOptions())
            if (CbRestartAfterLevel.SelectedItem == null)
            {
                return;
            }

            SelectedParagraphNumberingLevel.RestartAfterLevel = ((LevelRestartingOption) CbRestartAfterLevel.SelectedItem).Value;
        }

        private void OnBtnSetAllLevelsClick(object sender, System.Windows.RoutedEventArgs e)
        {
            new ParagraphNumberingSetForAllLevelsWindow(SelectedParagraphNumberingScheme, () =>
            {
                RenderPreview();
                UpdateComboBoxSelections();
                SelectedParagraphNumberingLevel.RaisePropertyChangedForPropsWithAdditionalValues();

            }).ShowDialog();
        }

        private void OnCbLineSpacingTypesSelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            SelectedParagraphNumberingLevel.ParagraphFormat.LineSpacingType = ((LineSpacingOption) CbLineSpacingTypes.SelectedItem).Value;

            switch (SelectedParagraphNumberingLevel.ParagraphFormat.LineSpacingType)
            {
                case WdLineSpacing.wdLineSpace1pt5:
                case WdLineSpacing.wdLineSpaceSingle:
                case WdLineSpacing.wdLineSpaceDouble:
                    TbLineSpacingValue.IsEnabled = false;
                    break;
                default:
                    TbLineSpacingValue.IsEnabled = true;
                    break;
            }
        }

        private void OnCbOutlineLevelsSelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            SelectedParagraphNumberingLevel.ParagraphFormat.OutlineLevel = ((OutlineLevelOption) CbOutlineLevels.SelectedItem).Value;
        }

        private void OnCbSpecialSelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            SelectedParagraphNumberingLevel.ParagraphFormat.SpecialIndentType = ((SpecialParagraphIndentationOption) CbSpecial.SelectedItem).Value;
        }

        private void OnCbStyleToFollowSelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            SelectedParagraphNumberingLevel.ParagraphFormat.StyleToFollow = (string) CbStyleToFollow.SelectedItem;
        }

        private void OnCbCreateContinuationStyleCheckedChanged(object sender, RoutedEventArgs e)
        {
            UpdateFollowableStyleNames();

            if (FollowableStyleNames.Contains(SelectedParagraphNumberingLevel.ParagraphFormat.StyleToFollow))
            {
                return;
            }

            SelectedParagraphNumberingLevel.ParagraphFormat.StyleToFollow = ListTemplateUtil.GetParagraphStyleName(SelectedParagraphNumberingScheme, SelectedParagraphNumberingLevel.Index);
        }

        private void OnContinuationStyleButtonClick(object sender, RoutedEventArgs e)
        {
            string continuationStyleName = ListTemplateUtil.GetContinuationParagraphStyleName(SelectedParagraphNumberingScheme, SelectedParagraphNumberingLevel.Index);

            new ParagraphNumberingContinuationStyleFormatWindow(continuationStyleName, SelectedParagraphNumberingLevel.ContinuationParagraphFormat).ShowDialog();
        }

        private void OnBtnDescreaseIndentClick(object sender, RoutedEventArgs e)
        {
            if (WpfUtil.IsShiftKeyDown())
            {
                if (!SelectedParagraphNumberingLevel.ParagraphFormat.IsHangIndent)
                {
                    SelectedParagraphNumberingLevel.ParagraphFormat.IsHangIndent = true;
                }

                if (SelectedParagraphNumberingLevel.ParagraphFormat.SpecialIndentValue == 0)
                {
                    // Disables special indentation
                    SelectedParagraphNumberingLevel.ParagraphFormat.IsWrapToNumberIndent = true;
                }
                else
                {
                    if (SelectedParagraphNumberingLevel.ParagraphFormat.SpecialIndentValue - LEVEL_INDENTATION_CHANGE >= MIN_SPECIAL_INDENTATION_VALUE)
                    {
                        SelectedParagraphNumberingLevel.ParagraphFormat.SpecialIndentValue -= LEVEL_INDENTATION_CHANGE;
                    }
                    else
                    {
                        SelectedParagraphNumberingLevel.ParagraphFormat.SpecialIndentValue = MIN_SPECIAL_INDENTATION_VALUE;
                    }
                }
            }
            else
            {
                if (SelectedParagraphNumberingLevel.NumberIndent - LEVEL_INDENTATION_CHANGE >= MIN_LEVEL_NUMBER_POSITION)
                {
                    SelectedParagraphNumberingLevel.NumberIndent -= LEVEL_INDENTATION_CHANGE;
                }
                else
                {
                    SelectedParagraphNumberingLevel.NumberIndent = MIN_LEVEL_NUMBER_POSITION;
                }

                if (SelectedParagraphNumberingLevel.ParagraphFormat.LeftIndent - LEVEL_INDENTATION_CHANGE >= MIN_PARAGRAPH_LEFT_INDENTATION_VALUE)
                {
                    SelectedParagraphNumberingLevel.ParagraphFormat.LeftIndent -= LEVEL_INDENTATION_CHANGE;
                }
                else
                {
                    SelectedParagraphNumberingLevel.ParagraphFormat.LeftIndent = MIN_PARAGRAPH_LEFT_INDENTATION_VALUE;
                }
            }

            if (SelectedParagraphNumberingLevel.TabPosition - LEVEL_INDENTATION_CHANGE >= MIN_LEVEL_TAB_POSITION)
            {
                SelectedParagraphNumberingLevel.TabPosition -= LEVEL_INDENTATION_CHANGE;
            }
            else
            {
                SelectedParagraphNumberingLevel.TabPosition = MIN_LEVEL_TAB_POSITION;
            }
        }

        private void OnBtnIncreaseIndentClick(object sender, RoutedEventArgs e)
        {
            if (WpfUtil.IsShiftKeyDown())
            {
                if (!SelectedParagraphNumberingLevel.ParagraphFormat.IsHangIndent)
                {
                    SelectedParagraphNumberingLevel.ParagraphFormat.IsHangIndent = true;
                }

                if (SelectedParagraphNumberingLevel.ParagraphFormat.SpecialIndentValue + LEVEL_INDENTATION_CHANGE <= MAX_SPECIAL_INDENTATION_VALUE)
                {
                    SelectedParagraphNumberingLevel.ParagraphFormat.SpecialIndentValue += LEVEL_INDENTATION_CHANGE;
                }
                else
                {
                    SelectedParagraphNumberingLevel.ParagraphFormat.SpecialIndentValue = MAX_SPECIAL_INDENTATION_VALUE;
                }
            }
            else
            {
                if (SelectedParagraphNumberingLevel.NumberIndent + LEVEL_INDENTATION_CHANGE <= MAX_LEVEL_NUMBER_POSITION)
                {
                    SelectedParagraphNumberingLevel.NumberIndent += LEVEL_INDENTATION_CHANGE;
                }
                else
                {
                    SelectedParagraphNumberingLevel.NumberIndent = MAX_LEVEL_NUMBER_POSITION;
                }

                if (SelectedParagraphNumberingLevel.ParagraphFormat.LeftIndent + LEVEL_INDENTATION_CHANGE <= MAX_PARAGRAPH_LEFT_INDENTATION_VALUE)
                {
                    SelectedParagraphNumberingLevel.ParagraphFormat.LeftIndent += LEVEL_INDENTATION_CHANGE;
                }
                else
                {
                    SelectedParagraphNumberingLevel.ParagraphFormat.LeftIndent = MAX_PARAGRAPH_LEFT_INDENTATION_VALUE;
                }
            }

            if (SelectedParagraphNumberingLevel.TabPosition + LEVEL_INDENTATION_CHANGE <= MAX_LEVEL_TAB_POSITION)
            {
                SelectedParagraphNumberingLevel.TabPosition += LEVEL_INDENTATION_CHANGE;
            }
            else
            {
                SelectedParagraphNumberingLevel.TabPosition = MAX_LEVEL_TAB_POSITION;
            }
        }

        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void SetBaseStyleName()
        {
            if (!string.IsNullOrEmpty(SelectedParagraphNumberingScheme?.BaseStyleName))
            {
                if(!BaseStyleNames.Contains(SelectedParagraphNumberingScheme.BaseStyleName))
                {
                    BaseStyleNames.Add(SelectedParagraphNumberingScheme.BaseStyleName);
                }

                SelectedParagraphNumberingScheme.BaseStyleName = SelectedParagraphNumberingScheme.BaseStyleName;
            }
            else
            {
                SelectedParagraphNumberingScheme.BaseStyleName = Utils.GetLocalizedStyleNameFromBuiltInStyle(WdBuiltinStyle.wdStyleNormal);
            }
        }		
		#endregion
	}
}
