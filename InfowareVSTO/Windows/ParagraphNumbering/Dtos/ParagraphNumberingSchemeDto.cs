﻿namespace InfowareVSTO.Windows.ParagraphNumbering.Dtos
{
    internal class ParagraphNumberingSchemeDto
    {
        public long Id { get; set; }

        public string ParagraphNumberingSchemeName { get; set; }

        public string ParagraphNumberingSchemeValue { get; set; }
    }
}
