﻿using System.Linq;

namespace InfowareVSTO.Windows.ParagraphNumbering.Model
{
    public class AllLevelsOption
    {
        public float FirstLevelNumberIndent { get; set; }
        public float FirstLevelTextIndent { get; set; }
        public float AdditionalLevelIndent { get; set; }

        public SpecialParagraphIndentationOption FirstLineSpecialParagraphIndentationType { get; set; }
        public float FirstLineSpecialIndentationValue { get; set; }

        public SpecialParagraphIndentationOption AdditionalLinesParagraphIndentationType { get; set; }
        public float AdditionalLinesParagraphIndentationValue { get; set; }

        public AllLevelsOption(ParagraphNumberingScheme paragraphNumberingScheme, ParagraphNumberingLevel firstParagraphNumberingLevel)
        {
            FirstLevelNumberIndent = firstParagraphNumberingLevel.NumberIndent;
            FirstLevelTextIndent = firstParagraphNumberingLevel.TabPosition - firstParagraphNumberingLevel.NumberIndent;
            AdditionalLevelIndent = firstParagraphNumberingLevel.TabPosition - firstParagraphNumberingLevel.NumberIndent;

            FirstLineSpecialParagraphIndentationType = ListTemplateUtil.GetSpecialParagraphIndentationOptionFromSpecialParagraphIndentation(firstParagraphNumberingLevel.ParagraphFormat.SpecialIndentType);
            FirstLineSpecialIndentationValue = firstParagraphNumberingLevel.ParagraphFormat.SpecialIndentValue;

            AdditionalLinesParagraphIndentationType = FirstLineSpecialParagraphIndentationType;
            AdditionalLinesParagraphIndentationValue = FirstLineSpecialIndentationValue;
        }
    }
}
