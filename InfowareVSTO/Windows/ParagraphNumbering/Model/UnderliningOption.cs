﻿using Microsoft.Office.Interop.Word;

namespace InfowareVSTO.Windows.ParagraphNumbering.Model
{
    public class UnderliningOption
    {
        public string Name { get; set; }

        public WdUnderline Value { get; set; }

        public UnderliningOption(string name, WdUnderline value)
        {
            Name = name;
            Value = value;
        }
    }
}
