﻿using Microsoft.Office.Interop.Word;

namespace InfowareVSTO.Windows.ParagraphNumbering.Model
{
    public class ListLevelAlignmentOption
    {
        public string Name { get; set; }

        public WdListLevelAlignment Value { get; set; }

        public ListLevelAlignmentOption(string name, WdListLevelAlignment value)
        {
            Name = name;
            Value = value;
        }
    }
}
