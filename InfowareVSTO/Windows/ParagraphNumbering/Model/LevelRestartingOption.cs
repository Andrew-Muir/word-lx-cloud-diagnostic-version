﻿namespace InfowareVSTO.Windows.ParagraphNumbering.Model
{
    public class LevelRestartingOption
    {
        public string Name { get; set; }

        public int Value { get; set; }

        public LevelRestartingOption(string name, int value)
        {
            Name = name;
            Value = value;
        }
    }
}
