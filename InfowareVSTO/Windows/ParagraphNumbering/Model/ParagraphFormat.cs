﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using InfowareVSTO.Windows.ParagraphNumbering.Model;
using Microsoft.Office.Interop.Word;

namespace InfowareVSTO.Windows.ParagraphNumbering
{
    public class ParagraphFormat : INotifyPropertyChanged
    {
        private bool IsDeserializing;

        private float spacingBefore;

        /// <summary>
        /// Value in points
        /// </summary>
        public float SpacingBefore
        {
            get => spacingBefore;

            set
            {
                spacingBefore = value;

                OnPropertyChanged();
            }
        }

        private float spacingAfter;

        /// <summary>
        /// Value in points
        /// </summary>
        public float SpacingAfter
        {
            get => spacingAfter;

            set
            {
                spacingAfter = value;

                OnPropertyChanged();
            }
        }

        private WdLineSpacing? lineSpacingType;

        public WdLineSpacing? LineSpacingType
        {
            get => lineSpacingType;

            set
            {
                lineSpacingType = value;

                OnPropertyChanged();
            }
        }

        private float lineSpacingValue;

        /// <summary>
        /// Value in points, only relevant for some line spacing types
        /// </summary>
        public float LineSpacingValue
        {
            get => lineSpacingValue;

            set
            {
                lineSpacingValue = value;

                OnPropertyChanged();
            }
        }

        public float AdditionalLeftIndent { get; set; }

        private float leftIndent;

        /// <summary>
        /// Value in inches
        /// </summary>
        public float LeftIndent
        {
            get => leftIndent + AdditionalLeftIndent;

            set
            {
                leftIndent = value;
                AdditionalLeftIndent = 0;
                OnPropertyChanged();
            }
        }

        private float rightIndent;

        /// <summary>
        /// Value in inches
        /// </summary>
        public float RightIndent
        {
            get => rightIndent;

            set
            {
                rightIndent = value;

                OnPropertyChanged();
            }
        }

        private SpecialParagraphIndentation specialIndentType;

        public SpecialParagraphIndentation SpecialIndentType
        {
            get => specialIndentType;

            set
            {
                specialIndentType = value;

                OnPropertyChanged(nameof(IsHangIndent));
                OnPropertyChanged(nameof(IsWrapToNumberIndent));
                OnPropertyChanged(nameof(IsWrapToMarginIndent));

                OnPropertyChanged();
            }
        }

        private float specialIndentValue;

        /// <summary>
        /// Value in inches, only relevant to <see cref="SpecialParagraphIndentation.FIRST_LINE"/> and
        /// <see cref="SpecialParagraphIndentation.HANGING"/>
        /// </summary>
        public float SpecialIndentValue
        {
            get => specialIndentValue;

            set
            {
                specialIndentValue = value;
                OnPropertyChanged();
            }
        }

        private bool isLeftAligned;

        public bool IsLeftAligned
        {
            get => isLeftAligned;

            set
            {
                isLeftAligned = value;

                if (value)
                {
                    IsCenterAligned = IsRightAligned = IsJustifiedAligned = false;
                }

                OnPropertyChanged();
            }
        }

        private bool isCenterAligned;

        public bool IsCenterAligned
        {
            get => isCenterAligned;

            set
            {
                isCenterAligned = value;

                if (value)
                {
                    IsLeftAligned = IsRightAligned = IsJustifiedAligned = false;
                }

                OnPropertyChanged();
            }
        }

        private bool isRightAligned;

        public bool IsRightAligned
        {
            get => isRightAligned;

            set
            {
                isRightAligned = value;

                if (value)
                {
                    IsLeftAligned = IsCenterAligned = IsJustifiedAligned = false;
                }

                OnPropertyChanged();
            }
        }

        private bool isJustifiedAligned;

        public bool IsJustifiedAligned
        {
            get => isJustifiedAligned;

            set
            {
                isJustifiedAligned = value;

                if (value)
                {
                    IsLeftAligned = IsCenterAligned = IsRightAligned = false;
                }

                OnPropertyChanged();
            }
        }

        private bool noSpaceBetweenParagraphsOfSameStyle;

        public bool NoSpaceBetweenParagraphsOfSameStyle
        {
            get => noSpaceBetweenParagraphsOfSameStyle;

            set
            {
                noSpaceBetweenParagraphsOfSameStyle = value;

                OnPropertyChanged();
            }
        }

        public WdOutlineLevel OutlineLevel { get; set; }

        private bool windowControl;

        public bool WindowControl
        {
            get => windowControl;

            set
            {
                windowControl = value;

                OnPropertyChanged();
            }
        }

        private bool keepWithNext;

        public bool KeepWithNext
        {
            get => keepWithNext;

            set
            {
                keepWithNext = value;

                OnPropertyChanged();
            }
        }

        private bool keepLinesTogether;

        public bool KeepLinesTogether
        {
            get => keepLinesTogether; 

            set
            {
                keepLinesTogether = value;

                OnPropertyChanged();
            }
        }

        private bool pageBreakBefore;

        public bool PageBreakBefore
        {
            get => pageBreakBefore;

            set
            {
                pageBreakBefore = value;

                OnPropertyChanged();
            }
        }

        public bool IsHangIndent
        {
            get => SpecialIndentType == SpecialParagraphIndentation.HANGING;

            set
            {
                if (!IsDeserializing)
                {
                    SpecialIndentType = SpecialParagraphIndentation.HANGING;

                    OnPropertyChanged();
                    OnPropertyChanged(nameof(IsWrapToNumberIndent));
                    OnPropertyChanged(nameof(IsWrapToMarginIndent));
                }
            }
        }

        public bool IsWrapToNumberIndent
        {
            get => SpecialIndentType == SpecialParagraphIndentation.NONE;

            set
            {
                if (!IsDeserializing)
                {
                    SpecialIndentType = SpecialParagraphIndentation.NONE;

                    OnPropertyChanged();
                    OnPropertyChanged(nameof(IsHangIndent));
                    OnPropertyChanged(nameof(IsWrapToMarginIndent));
                }
            }
        }

        public bool IsWrapToMarginIndent
        {
            get => SpecialIndentType == SpecialParagraphIndentation.FIRST_LINE;

            set
            {
                if (!IsDeserializing)
                {
                    SpecialIndentType = SpecialParagraphIndentation.FIRST_LINE;

                    OnPropertyChanged();
                    OnPropertyChanged(nameof(IsHangIndent));
                    OnPropertyChanged(nameof(IsWrapToNumberIndent));
                }
            }
        }

        private bool bordersAreEnabled;

        public bool BordersAreEnabled
        {
            get => bordersAreEnabled;

            set
            {
                bordersAreEnabled = value;

                if (!bordersAreEnabled)
                {
                    LeftBorderLineStyle = WdLineStyle.wdLineStyleNone;
                    TopBorderLineStyle = WdLineStyle.wdLineStyleNone;
                    RightBorderLineStyle = WdLineStyle.wdLineStyleNone;
                    BottomBorderLineStyle = WdLineStyle.wdLineStyleNone;
                }
            }
        }

        public WdLineStyle LeftBorderLineStyle { get; set; }

        public WdLineWidth LeftBorderLineWidth { get; set; }

        public WdColor LeftBorderColor { get; set; }

        public WdLineStyle TopBorderLineStyle { get; set; }

        public WdLineWidth TopBorderLineWidth { get; set; }

        public WdColor TopBorderColor { get; set; }

        public WdLineStyle RightBorderLineStyle { get; set; }

        public WdLineWidth RightBorderLineWidth { get; set; }

        public WdColor RightBorderColor { get; set; }

        public WdLineStyle BottomBorderLineStyle { get; set; }

        public WdLineWidth BottomBorderLineWidth { get; set; }

        public WdColor BottomBorderColor { get; set; }

        public bool BorderHasShadow { get; set; }

        public int BorderDistanceFromLeft { get; set; }

        public int BorderDistanceFromTop { get; set; }

        public int BorderDistanceFromRight { get; set; }

        public int BorderDistanceFromBottom { get; set; }

        public WdTextureIndex BorderShadingTexture { get; set; }

        public WdColor BorderShadingBackgroundColor { get; set; }

        public WdColor BorderShadingForegroundColor { get; set; }

        public ParagraphFontPreferences FontPreferences { get; set; }

        private bool hideUntilUsed;

        public bool HideUntilUsed
        {
            get => hideUntilUsed;

            set
            {
                hideUntilUsed = value;
                OnPropertyChanged();
            }
        }

        private string styleToFollow;

        public string StyleToFollow
        {
            get => styleToFollow;

            set
            {
                styleToFollow = value;

                OnPropertyChanged();
            }
        }

        public WdStyleType StyleType { get; set; }
        public string PredefinedStyleName { get; internal set; }

        [OnDeserializing]
        internal void OnDeserializingMethod(StreamingContext context)
        {
            IsDeserializing = true;
        }

        [OnDeserialized]
        internal void OnDeserializedMethod(StreamingContext context)
        {
            IsDeserializing = false;
        }

        public ParagraphFormat(float spacingBefore, float spacingAfter, WdLineSpacing? lineSpacingType, float lineSpacingValue,
    float leftIndent, float rightIndent, SpecialParagraphIndentation specialIndentType, float specialIndentValue, 
    bool isLeftAligned, bool isCenterAligned, bool isRightAligned, bool isJustifiedAligned, 
    bool noSpaceBetweenParagraphsOfSameStyle = false, WdOutlineLevel outlineLevel = WdOutlineLevel.wdOutlineLevelBodyText,
    bool windowControl = true, bool keepWithNext = false, bool keepLinesTogether = false, bool pageBreakBefore = false,
    ParagraphFontPreferences fontPreferences = null, string styleToFollow = null, WdStyleType styleType = WdStyleType.wdStyleTypeParagraph)
        {
            SpacingBefore = spacingBefore;
            SpacingAfter = spacingAfter;
            LineSpacingType = lineSpacingType;
            LineSpacingValue = lineSpacingValue;
            LeftIndent = leftIndent;
            RightIndent = rightIndent;
            SpecialIndentType = specialIndentType;
            SpecialIndentValue = specialIndentValue;

            if (fontPreferences == null)
            {
                fontPreferences = new ParagraphFontPreferences();
            }

            FontPreferences = fontPreferences;

            IsLeftAligned = isLeftAligned;
            IsCenterAligned = isCenterAligned;
            IsRightAligned = isRightAligned;
            IsJustifiedAligned = isJustifiedAligned;
            NoSpaceBetweenParagraphsOfSameStyle = noSpaceBetweenParagraphsOfSameStyle;
            OutlineLevel = outlineLevel;
            WindowControl = windowControl;
            KeepWithNext = keepWithNext;
            KeepLinesTogether = keepLinesTogether;
            PageBreakBefore = pageBreakBefore;

            StyleToFollow = styleToFollow;

            StyleType = styleType;
        }

        public ParagraphFormat()
        {

        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void RasePropertyChangedForPropsWithAdditionalValues()
        {
            OnPropertyChanged(nameof(LeftIndent));
        }

        public void Copy(ParagraphFormat paragraphFormat)
        {
            SpacingBefore = paragraphFormat.SpacingBefore;
            SpacingAfter = paragraphFormat.SpacingAfter;
            LineSpacingType = paragraphFormat.LineSpacingType;
            LineSpacingValue = paragraphFormat.LineSpacingValue;

            LeftIndent = paragraphFormat.LeftIndent - paragraphFormat.AdditionalLeftIndent;
            AdditionalLeftIndent = paragraphFormat.AdditionalLeftIndent;
            RightIndent = paragraphFormat.RightIndent;

            SpecialIndentType = paragraphFormat.SpecialIndentType;
            SpecialIndentValue = paragraphFormat.SpecialIndentValue;
            IsLeftAligned = paragraphFormat.IsLeftAligned;
            IsCenterAligned = paragraphFormat.IsCenterAligned;
            IsRightAligned = paragraphFormat.IsRightAligned;
            IsJustifiedAligned = paragraphFormat.IsJustifiedAligned;
            NoSpaceBetweenParagraphsOfSameStyle = paragraphFormat.NoSpaceBetweenParagraphsOfSameStyle;
            OutlineLevel = paragraphFormat.OutlineLevel;
            WindowControl = paragraphFormat.WindowControl;
            KeepWithNext = paragraphFormat.KeepWithNext;
            KeepLinesTogether = paragraphFormat.KeepLinesTogether;
            PageBreakBefore = paragraphFormat.PageBreakBefore;

            BordersAreEnabled = paragraphFormat.BordersAreEnabled;

            LeftBorderLineStyle = paragraphFormat.LeftBorderLineStyle;

            LeftBorderLineWidth = paragraphFormat.LeftBorderLineWidth;

            LeftBorderColor = paragraphFormat.LeftBorderColor;

            TopBorderLineStyle = paragraphFormat.TopBorderLineStyle;

            TopBorderLineWidth = paragraphFormat.TopBorderLineWidth;

            TopBorderColor = paragraphFormat.TopBorderColor;

            RightBorderLineStyle = paragraphFormat.RightBorderLineStyle;

            RightBorderLineWidth = paragraphFormat.RightBorderLineWidth;

            RightBorderColor = paragraphFormat.RightBorderColor;

            BottomBorderLineStyle = paragraphFormat.BottomBorderLineStyle;

            BottomBorderLineWidth = paragraphFormat.BottomBorderLineWidth;

            BottomBorderColor = paragraphFormat.BottomBorderColor;

            BorderHasShadow = paragraphFormat.BorderHasShadow;

            BorderDistanceFromLeft = paragraphFormat.BorderDistanceFromLeft;

            BorderDistanceFromTop = paragraphFormat.BorderDistanceFromTop;

            BorderDistanceFromRight = paragraphFormat.BorderDistanceFromRight;

            BorderDistanceFromBottom = paragraphFormat.BorderDistanceFromBottom;

            BorderShadingTexture = paragraphFormat.BorderShadingTexture;

            BorderShadingBackgroundColor = paragraphFormat.BorderShadingBackgroundColor;

            BorderShadingForegroundColor = paragraphFormat.BorderShadingForegroundColor;

            FontPreferences.Copy(paragraphFormat.FontPreferences);

            HideUntilUsed = paragraphFormat.HideUntilUsed;

            StyleToFollow = paragraphFormat.StyleToFollow;

            StyleType = paragraphFormat.StyleType;
        }

        protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
