﻿using Microsoft.Office.Interop.Word;

namespace InfowareVSTO.Windows.ParagraphNumbering.Model
{
    public class LineSpacingOption
    {
        public string Name { get; set; }

        public WdLineSpacing Value { get; set; }

        public LineSpacingOption(string name, WdLineSpacing value)
        {
            Name = name;
            Value = value;
        }
    }
}
