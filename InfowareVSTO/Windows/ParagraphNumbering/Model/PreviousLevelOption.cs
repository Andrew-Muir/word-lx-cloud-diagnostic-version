﻿namespace InfowareVSTO.Windows.ParagraphNumbering.Model
{
    public class PreviousLevelOption
    {
        public string Name { get; }

        public int Level { get; set; }

        public string Value { get; }

        public PreviousLevelOption(int level)
        {
            Level = level;

            Name = $"Level {level}";
            Value = $"%{level}";
        }
    }
}
