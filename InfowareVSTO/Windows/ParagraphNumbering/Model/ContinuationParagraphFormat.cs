﻿using Microsoft.Office.Interop.Word;

namespace InfowareVSTO.Windows.ParagraphNumbering.Model
{
    public class ContinuationParagraphFormat : ParagraphFormat
    {
        private bool mirrorNumberingParagraphFormat;

        public bool MirrorNumberingParagraphFormat
        {
            get => mirrorNumberingParagraphFormat;

            set
            {
                mirrorNumberingParagraphFormat = value;

                if (value)
                {
                    SyncPropertiesWithNumberingParagraphStyle();
                }

                OnPropertyChanged();
            }
        }

        private ParagraphFormat numberingParagraphFormat;

        public ParagraphFormat NumberingParagraphFormat
        {
            get => numberingParagraphFormat;

            set
            {
                numberingParagraphFormat = value;

                if (MirrorNumberingParagraphFormat)
                {
                    SyncPropertiesWithNumberingParagraphStyle();
                }
            }
        }

        public ContinuationParagraphFormat(ParagraphFormat numberingParagraphFormat, float spacingBefore, float spacingAfter, WdLineSpacing lineSpacingType, float lineSpacingValue,
            float leftIndent, float rightIndent, SpecialParagraphIndentation specialIndentType, float specialIndentValue,
            bool isLeftAligned, bool isCenterAligned, bool isRightAligned, bool isJustifiedAligned,
            bool noSpaceBetweenParagraphsOfSameStyle = false, WdOutlineLevel outlineLevel = WdOutlineLevel.wdOutlineLevelBodyText,
            bool windowControl = true, bool keepWithNext = false, bool keepLinesTogether = false, bool pageBreakBefore = false,
            ParagraphFontPreferences fontPreferences = null) : base(spacingBefore, spacingAfter, lineSpacingType,
                lineSpacingValue, leftIndent, rightIndent, specialIndentType, specialIndentValue, isLeftAligned,
                isCenterAligned, isRightAligned, isJustifiedAligned, noSpaceBetweenParagraphsOfSameStyle, outlineLevel,
                windowControl, keepWithNext, keepLinesTogether, pageBreakBefore, fontPreferences)
        {
            NumberingParagraphFormat = numberingParagraphFormat;
            MirrorNumberingParagraphFormat = false;
        }

        public ContinuationParagraphFormat(ParagraphFormat numberingParagraphFormat)
            : base(numberingParagraphFormat.SpacingBefore, numberingParagraphFormat.SpacingAfter, numberingParagraphFormat.LineSpacingType,
                  numberingParagraphFormat.LineSpacingValue, numberingParagraphFormat.SpecialIndentType == SpecialParagraphIndentation.NONE ? numberingParagraphFormat.LeftIndent : numberingParagraphFormat.LeftIndent + numberingParagraphFormat.SpecialIndentValue,
                  numberingParagraphFormat.RightIndent, SpecialParagraphIndentation.NONE, 0, numberingParagraphFormat.IsLeftAligned, numberingParagraphFormat.IsCenterAligned, numberingParagraphFormat.IsRightAligned,
                  numberingParagraphFormat.IsJustifiedAligned, numberingParagraphFormat.NoSpaceBetweenParagraphsOfSameStyle, numberingParagraphFormat.OutlineLevel,
                  numberingParagraphFormat.WindowControl, numberingParagraphFormat.KeepWithNext, numberingParagraphFormat.KeepLinesTogether, numberingParagraphFormat.PageBreakBefore,
                  (ParagraphFontPreferences) numberingParagraphFormat.FontPreferences.Clone())
        {
            NumberingParagraphFormat = numberingParagraphFormat;
            MirrorNumberingParagraphFormat = true;
        }

        public ContinuationParagraphFormat() : base(0, 0, WdLineSpacing.wdLineSpace1pt5, 0, 0, 0, SpecialParagraphIndentation.NONE, 0, false, false, false, false)
        {
            // Constructor required by Newtonsoft.Json
        }

        public void Copy(ContinuationParagraphFormat continuationParagraphFormat)
        {
            base.Copy(continuationParagraphFormat);

            NumberingParagraphFormat.Copy(continuationParagraphFormat.NumberingParagraphFormat);
            MirrorNumberingParagraphFormat = continuationParagraphFormat.MirrorNumberingParagraphFormat;
        }

        public ContinuationParagraphFormat Clone()
        {
            var newContinuationParagraphFormat = new ContinuationParagraphFormat()
            {
                NumberingParagraphFormat = new ParagraphFormat() { FontPreferences = new ParagraphFontPreferences() }
            };

            newContinuationParagraphFormat.Copy(this);

            return newContinuationParagraphFormat;
        }

        public void SyncPropertiesWithNumberingParagraphStyle()
        {
            if (NumberingParagraphFormat == null)
            {
                return;
            }

            SpacingBefore = NumberingParagraphFormat.SpacingBefore;
            SpacingAfter = NumberingParagraphFormat.SpacingAfter;
            LineSpacingType = NumberingParagraphFormat.LineSpacingType;
            LineSpacingValue = NumberingParagraphFormat.LineSpacingValue;
            LeftIndent = NumberingParagraphFormat.SpecialIndentType == SpecialParagraphIndentation.NONE ? NumberingParagraphFormat.LeftIndent : NumberingParagraphFormat.LeftIndent + NumberingParagraphFormat.SpecialIndentValue;
            RightIndent = NumberingParagraphFormat.RightIndent;
            SpecialIndentType = SpecialParagraphIndentation.NONE;
            SpecialIndentValue = 0;
            IsLeftAligned = NumberingParagraphFormat.IsLeftAligned;
            IsCenterAligned = NumberingParagraphFormat.IsCenterAligned;
            IsRightAligned = NumberingParagraphFormat.IsRightAligned;
            IsJustifiedAligned = NumberingParagraphFormat.IsJustifiedAligned;
            NoSpaceBetweenParagraphsOfSameStyle = NumberingParagraphFormat.NoSpaceBetweenParagraphsOfSameStyle;
            OutlineLevel = NumberingParagraphFormat.OutlineLevel;
            WindowControl = NumberingParagraphFormat.WindowControl;
            KeepWithNext = NumberingParagraphFormat.KeepWithNext;
            KeepLinesTogether = NumberingParagraphFormat.KeepLinesTogether;
            PageBreakBefore = NumberingParagraphFormat.PageBreakBefore;

            BordersAreEnabled = NumberingParagraphFormat.BordersAreEnabled;

            LeftBorderLineStyle = NumberingParagraphFormat.LeftBorderLineStyle;

            LeftBorderLineWidth = NumberingParagraphFormat.LeftBorderLineWidth;

            LeftBorderColor = NumberingParagraphFormat.LeftBorderColor;

            TopBorderLineStyle = NumberingParagraphFormat.TopBorderLineStyle;

            TopBorderLineWidth = NumberingParagraphFormat.TopBorderLineWidth;

            TopBorderColor = NumberingParagraphFormat.TopBorderColor;

            RightBorderLineStyle = NumberingParagraphFormat.RightBorderLineStyle;

            RightBorderLineWidth = NumberingParagraphFormat.RightBorderLineWidth;

            RightBorderColor = NumberingParagraphFormat.RightBorderColor;

            BottomBorderLineStyle = NumberingParagraphFormat.BottomBorderLineStyle;

            BottomBorderLineWidth = NumberingParagraphFormat.BottomBorderLineWidth;

            BottomBorderColor = NumberingParagraphFormat.BottomBorderColor;

            BorderHasShadow = NumberingParagraphFormat.BorderHasShadow;

            BorderDistanceFromLeft = NumberingParagraphFormat.BorderDistanceFromLeft;

            BorderDistanceFromTop = NumberingParagraphFormat.BorderDistanceFromTop;

            BorderDistanceFromRight = NumberingParagraphFormat.BorderDistanceFromRight;

            BorderDistanceFromBottom = NumberingParagraphFormat.BorderDistanceFromBottom;

            BorderShadingTexture = NumberingParagraphFormat.BorderShadingTexture;

            BorderShadingBackgroundColor = NumberingParagraphFormat.BorderShadingBackgroundColor;

            BorderShadingForegroundColor = NumberingParagraphFormat.BorderShadingForegroundColor;

            FontPreferences = (ParagraphFontPreferences) NumberingParagraphFormat.FontPreferences.Clone();
        }
    }
}
