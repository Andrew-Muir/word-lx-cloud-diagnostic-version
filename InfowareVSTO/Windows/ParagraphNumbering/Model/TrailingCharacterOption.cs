﻿using InfowareVSTO.Windows.ParagraphNumbering.Model.Enums;

namespace InfowareVSTO.Windows.ParagraphNumbering.Model
{
    public class TrailingCharacterOption
    {
        public string Name { get; set; }

        public TrailingCharacter Value { get; set; }

        public TrailingCharacterOption(string name, TrailingCharacter value)
        {
            Name = name;
            Value = value;
        }
    }
}
