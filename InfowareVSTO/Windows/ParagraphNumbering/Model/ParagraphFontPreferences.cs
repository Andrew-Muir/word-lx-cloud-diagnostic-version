﻿using System;
using Microsoft.Office.Interop.Word;

namespace InfowareVSTO.Windows.ParagraphNumbering.Model
{
    public class ParagraphFontPreferences : FontPreferences, ICloneable
    {
        public bool IsSmallCaps { get; set; }

        public bool ApplyToUnnumbererdStyle { get; set; }

        public bool ApplyToNumberStyle { get; set; }

        public ParagraphFontPreferences(string name, int size, bool isBold, bool isItalic, bool isAllCaps, WdUnderline underlineType, string hexColor, WdLanguageID languageId,
            bool isSmallCaps, bool applyToUnnumbererdStyle, bool applyToNumberStyle) : base(name, size, isBold, isItalic, isAllCaps, underlineType, hexColor, languageId)
        {
            IsSmallCaps = isSmallCaps;

            ApplyToUnnumbererdStyle = applyToUnnumbererdStyle;

            ApplyToNumberStyle = applyToNumberStyle;
        }

        public ParagraphFontPreferences()
        {
            
        }

        public object Clone()
        {
            return MemberwiseClone();
        }

        public void Copy(ParagraphFontPreferences paragraphFontPreferences)
        {
            base.Copy(paragraphFontPreferences);

            IsSmallCaps = paragraphFontPreferences.IsSmallCaps;
            ApplyToUnnumbererdStyle = paragraphFontPreferences.ApplyToUnnumbererdStyle;
            ApplyToNumberStyle = paragraphFontPreferences.ApplyToNumberStyle;
        }
    }
}
