﻿namespace InfowareVSTO.Windows.ParagraphNumbering.Model
{
    /// <summary>
    /// Represents a special character, appendable to the number format (<see cref="ParagraphNumberingAdvancedOptions"/>) 
    /// </summary>
    public class SpecialCharacter
    {
        public string Name { get; set; }

        public char Value { get; set; }

        public SpecialCharacter(string name, char value)
        {
            Name = name;
            Value = value;
        }
    }
}
