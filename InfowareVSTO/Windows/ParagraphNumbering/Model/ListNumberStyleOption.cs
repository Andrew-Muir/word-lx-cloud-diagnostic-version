﻿using Microsoft.Office.Interop.Word;

namespace InfowareVSTO.Windows.ParagraphNumbering.Model
{
    public class ListNumberStyleOption
    {
        public string Representation { get; set; }

        public WdListNumberStyle Value { get; set; }

        public ListNumberStyleOption(string representation, WdListNumberStyle value)
        {
            Representation = representation;
            Value = value;
        }
    }
}
