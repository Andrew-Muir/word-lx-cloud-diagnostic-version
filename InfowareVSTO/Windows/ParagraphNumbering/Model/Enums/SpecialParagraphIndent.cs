﻿namespace InfowareVSTO.Windows.ParagraphNumbering
{
    public enum SpecialParagraphIndentation
    {
        NONE,
        FIRST_LINE,
        HANGING
    }
}
