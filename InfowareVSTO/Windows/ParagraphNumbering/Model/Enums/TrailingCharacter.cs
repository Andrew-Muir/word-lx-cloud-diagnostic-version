﻿namespace InfowareVSTO.Windows.ParagraphNumbering.Model.Enums
{
    public enum TrailingCharacter
    {
        NONE,
        TAB,
        SPACE,
        NEW_LINE
    }
}
