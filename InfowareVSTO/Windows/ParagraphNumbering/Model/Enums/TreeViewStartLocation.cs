﻿namespace InfowareVSTO.Windows.ParagraphNumbering.Model.Enums
{
    public enum TreeViewStartLocation
    {
        FIRM,
        SYNCED,
        LAST_USED,
        FAVOURITE,
    }
}
