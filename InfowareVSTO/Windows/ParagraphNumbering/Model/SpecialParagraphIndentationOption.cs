﻿namespace InfowareVSTO.Windows.ParagraphNumbering.Model
{
    public class SpecialParagraphIndentationOption
    {
        public string Name { get; set; }

        public SpecialParagraphIndentation Value { get; set; }

        public SpecialParagraphIndentationOption(string name, SpecialParagraphIndentation value)
        {
            Name = name;
            Value = value;
        }
    }
}
