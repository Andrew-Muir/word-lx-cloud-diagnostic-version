﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using InfowareVSTO.Windows.ParagraphNumbering.Model;
using InfowareVSTO.Windows.ParagraphNumbering.Model.Enums;
using Microsoft.Office.Interop.Word;

namespace InfowareVSTO.Windows.ParagraphNumbering
{
    public class ParagraphNumberingLevel : INotifyPropertyChanged
    {
        public int Index { get; set; }

        private string firstNumberFormatRepresentation;

        public string FirstNumberFormatRepresentation
        {
            get => firstNumberFormatRepresentation;

            set
            {
                firstNumberFormatRepresentation = value;
                OnPropertyChanged();
            }
        }

        private string numberFormat;

        public string NumberFormat
        {
            get => numberFormat;

            set
            {
                numberFormat = value;
                FirstNumberFormatRepresentation = GetFirstNumberFormatRepresentation(numberFormat, numberStyle);

                OnPropertyChanged();
            }
        }

        private WdListNumberStyle numberStyle;

        public WdListNumberStyle NumberStyle
        {
            get => numberStyle;

            set
            {
                numberStyle = value;

                if (value == WdListNumberStyle.wdListNumberStyleBullet)
                {
                    BulletCharacterIsEnabled = true;
                }

                FirstNumberFormatRepresentation = GetFirstNumberFormatRepresentation(numberFormat, numberStyle);
            }
        }

        private bool bulletCharacterIsEnabled;

        public bool BulletCharacterIsEnabled
        {
            get => bulletCharacterIsEnabled;

            set
            {
                bulletCharacterIsEnabled = value;

                if (value && string.IsNullOrEmpty(BulletCharacter))
                {
                    BulletCharacter = MsWordConstants.CIRCLE_BULLET_CHAR;
                }
            }
        }

        private string bulletCharacter;

        public string BulletCharacter
        {
            get => bulletCharacter;

            set
            {
                bulletCharacter = value;

                if (!string.IsNullOrEmpty(bulletCharacter))
                {
                    // Required due to the way we currently "clone" objects (via Newtonsoft.Json)

                    BulletCharacterIsEnabled = true;
                }

                OnPropertyChanged();
            }
        }

        public ParagraphFormat ParagraphFormat { get; set; }

        public ContinuationParagraphFormat ContinuationParagraphFormat { get; set; }

        public float AdditionalTabPosition { get; set; }

        private float tabPosition;

        public float TabPosition
        {
            get => tabPosition + AdditionalTabPosition;

            set
            {
                tabPosition = value;
                AdditionalTabPosition = 0;
                OnPropertyChanged();
            }
        }

        public FontPreferences FontPreferences { get; set; }

        public WdListLevelAlignment NumberAlignment { get; set; }

        public float AdditionalNumberIndent { get; set; }

        private float numberIndent;

        public float NumberIndent
        {
            get => numberIndent + AdditionalNumberIndent;

            set
            {
                numberIndent = value;
                AdditionalNumberIndent = 0;
                OnPropertyChanged();
            }
        }

        private TrailingCharacter trailingCharacter { get; set; }

        public TrailingCharacter TrailingCharacter
        {
            get => trailingCharacter;

            set
            {
                trailingCharacter = value;
                OnPropertyChanged();
            }
        }

        public int StartAt { get; set; }

        public bool IsBold
        {
            get => FontPreferences.IsBold && ParagraphFormat.FontPreferences.IsBold;

            set
            {
                FontPreferences.IsBold = ParagraphFormat.FontPreferences.IsBold = value;

                OnPropertyChanged();
            }
        }

        public bool IsUnderlined
        {
            get => FontPreferences.UnderlineType == WdUnderline.wdUnderlineSingle
                && ParagraphFormat.FontPreferences.UnderlineType == WdUnderline.wdUnderlineSingle;

            set
            {
                FontPreferences.UnderlineType = ParagraphFormat.FontPreferences.UnderlineType =
                    value ? WdUnderline.wdUnderlineSingle : WdUnderline.wdUnderlineNone;

                OnPropertyChanged();
            }
        }

        public bool IsItalic
        {
            get => FontPreferences.IsItalic && ParagraphFormat.FontPreferences.IsItalic;

            set
            {
                FontPreferences.IsItalic = ParagraphFormat.FontPreferences.IsItalic = value;

                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Corresponds to <see cref="ListLevel.ResetOnHigher"/>. If set to 0, the functionality is disabled
        /// </summary>
        public int RestartAfterLevel { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Creates a <see cref="ParagraphNumberingLevel"/> with all of its default values (null strings, false bools, etc.)
        /// </summary>
        public ParagraphNumberingLevel()
        {

        }

        public ParagraphNumberingLevel(int index, string numberFormat, WdListNumberStyle numberStyle, ParagraphFormat paragraphFormat,
            float tabPosition, ContinuationParagraphFormat continuationParagraphFormat = null, FontPreferences fontPreferences = null, WdListLevelAlignment numberAlignment = WdListLevelAlignment.wdListLevelAlignLeft, float numberIndent = 0, int startAt = 1, TrailingCharacter trailingCharacter = TrailingCharacter.TAB)
        {
            Index = index;
            FirstNumberFormatRepresentation = GetFirstNumberFormatRepresentation(numberFormat, numberStyle);
            NumberFormat = numberFormat;
            NumberStyle = numberStyle;
            ParagraphFormat = paragraphFormat;
            TabPosition = tabPosition;

            if (continuationParagraphFormat == null)
            {
                continuationParagraphFormat = new ContinuationParagraphFormat(paragraphFormat);
            }

            ContinuationParagraphFormat = continuationParagraphFormat;

            if (fontPreferences == null)
            {
                fontPreferences = new FontPreferences();
            }

            FontPreferences = fontPreferences;

            NumberAlignment = numberAlignment;
            NumberIndent = numberIndent;

            StartAt = startAt;
            TrailingCharacter = trailingCharacter;

            RestartAfterLevel = index - 1;
        }

        public void Copy(ParagraphNumberingLevel paragraphNumberingLevel)
        {
            Index = paragraphNumberingLevel.Index;
            FirstNumberFormatRepresentation = paragraphNumberingLevel.FirstNumberFormatRepresentation;
            NumberFormat = paragraphNumberingLevel.NumberFormat;
            NumberStyle = paragraphNumberingLevel.NumberStyle;
            BulletCharacterIsEnabled = paragraphNumberingLevel.BulletCharacterIsEnabled;
            BulletCharacter = paragraphNumberingLevel.BulletCharacter;

            ParagraphFormat.Copy(paragraphNumberingLevel.ParagraphFormat);
            ContinuationParagraphFormat.Copy(paragraphNumberingLevel.ContinuationParagraphFormat);

            TabPosition = paragraphNumberingLevel.TabPosition - paragraphNumberingLevel.AdditionalTabPosition;
            AdditionalTabPosition = paragraphNumberingLevel.AdditionalTabPosition;

            FontPreferences.Copy(paragraphNumberingLevel.FontPreferences);

            NumberAlignment = paragraphNumberingLevel.NumberAlignment;

            NumberIndent = paragraphNumberingLevel.NumberIndent - paragraphNumberingLevel.AdditionalNumberIndent;
            AdditionalNumberIndent = paragraphNumberingLevel.AdditionalNumberIndent;

            TrailingCharacter = paragraphNumberingLevel.TrailingCharacter;

            StartAt = paragraphNumberingLevel.StartAt;
            RestartAfterLevel = paragraphNumberingLevel.RestartAfterLevel;            
        }

        public static ParagraphNumberingLevel CreateDefaultInstace()
        {
            return new ParagraphNumberingLevel();
        }

        public void UseDefaultNumberingFont()
        {
            FontPreferences.Name = MsWordConstants.DEFAULT_FONT;
        }

        public void UseDefaultBulletFont()
        {
            FontPreferences.Name = MsWordConstants.DEFAULT_BULLET_FONT;
        }

        public void UseDefaultBulletChar()
        {
            BulletCharacter = MsWordConstants.CIRCLE_BULLET_CHAR;
        }

        private static string GetFirstNumberFormatRepresentation(string numberFormat, WdListNumberStyle numberStyle)
        {
            string replacement = null;

            switch (numberStyle)
            {
                case WdListNumberStyle.wdListNumberStyleNone:
                    return "";
                case WdListNumberStyle.wdListNumberStyleArabic:
                    replacement = "1";
                    break;
                case WdListNumberStyle.wdListNumberStyleUppercaseRoman:
                    replacement = "I";
                    break;
                case WdListNumberStyle.wdListNumberStyleLowercaseRoman:
                    replacement = "i";
                    break;
                case WdListNumberStyle.wdListNumberStyleUppercaseLetter:
                    replacement = "A";
                    break;
                case WdListNumberStyle.wdListNumberStyleLowercaseLetter:
                    replacement = "a";
                    break;
                case WdListNumberStyle.wdListNumberStyleOrdinal:
                    replacement = "1st";
                    break;
                case WdListNumberStyle.wdListNumberStyleCardinalText:
                    replacement = "One";
                    break;
                case WdListNumberStyle.wdListNumberStyleOrdinalText:
                    replacement = "First";
                    break;
                case WdListNumberStyle.wdListNumberStyleBullet:
                    // For bullets we return their actual representation
                    return numberFormat;
                case WdListNumberStyle.wdListNumberStyleLegal:
                    replacement = "1";
                    break;
                case WdListNumberStyle.wdListNumberStyleLegalLZ:
                    replacement = "01";
                    break;
                case WdListNumberStyle.wdListNumberStyleArabicLZ:
                    replacement = "01";
                    break;
                case WdListNumberStyle.wdListNumberStyleArabicLZ2:
                    replacement = "001";
                    break;
                case WdListNumberStyle.wdListNumberStyleArabicLZ3:
                    replacement = "0001";
                    break;
                case WdListNumberStyle.wdListNumberStyleArabicLZ4:
                    replacement = "00001";
                    break;
                default:
                    throw new NotImplementedException();
            }

            return MsWordConstants.MSWORD_NUMBERING_REGEX.Replace(numberFormat, replacement);
        }

        public void RaisePropertyChangedForPropsWithAdditionalValues()
        {
            OnPropertyChanged(nameof(TabPosition));
            OnPropertyChanged(nameof(NumberIndent));

            ParagraphFormat.RasePropertyChangedForPropsWithAdditionalValues();
        }

        public void RaisePropertyChangedForComplexProps()
        {
            OnPropertyChanged(nameof(IsBold));
            OnPropertyChanged(nameof(IsItalic));
            OnPropertyChanged(nameof(IsUnderlined));
        }

        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
