﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Data;

namespace InfowareVSTO.Windows.ParagraphNumbering
{
    public class ParagraphNumberingSchemeCategory
    {
        public string Name { get; set; }

        public ObservableCollection<ParagraphNumberingScheme> ParagraphNumberingSchemes { get; set; }

        public ObservableCollection<ParagraphNumberingSchemeCategory> NestedParagraphNumberingSchemeCategory { get; set; }

        public IEnumerable Items
        {
            get
            {
                var items = new CompositeCollection();
                items.Add(new CollectionContainer { Collection = NestedParagraphNumberingSchemeCategory });
                items.Add(new CollectionContainer { Collection = ParagraphNumberingSchemes });
                return items;
            }
        }


        public ParagraphNumberingSchemeCategory(string name, ObservableCollection<ParagraphNumberingScheme> paragraphNumberingSchemes, ObservableCollection<ParagraphNumberingSchemeCategory> nestedCategories = null)
        {
            Name = name;
            ParagraphNumberingSchemes = paragraphNumberingSchemes;
            NestedParagraphNumberingSchemeCategory = nestedCategories ?? new ObservableCollection<ParagraphNumberingSchemeCategory>();
        }
    }
}
