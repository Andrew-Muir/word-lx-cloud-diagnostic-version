﻿using System.Collections.Generic;

namespace InfowareVSTO.Windows.ParagraphNumbering
{
    public class ParagraphNumberingSchemeType
    {
        public string Name { get; set; }

        public bool IsDefault { get; set; }

        public IList<object> ParagraphNumberingSchemesOrCategories { get; set; }

        public ParagraphNumberingSchemeType(string name, IList<object> paragraphNumberingSchemesOrCategories)
        {
            Name = name;
            ParagraphNumberingSchemesOrCategories = paragraphNumberingSchemesOrCategories;
        }
    }
}
