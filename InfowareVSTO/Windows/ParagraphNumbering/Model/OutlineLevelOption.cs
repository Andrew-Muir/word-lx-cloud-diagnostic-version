﻿using Microsoft.Office.Interop.Word;

namespace InfowareVSTO.Windows.ParagraphNumbering.Model
{
    public class OutlineLevelOption
    {
        public string Name { get; set; }

        public WdOutlineLevel Value { get; set; }

        public OutlineLevelOption(string name, WdOutlineLevel value)
        {
            Name = name;
            Value = value;
        }
    }
}
