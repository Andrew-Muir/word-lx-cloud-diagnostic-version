﻿using Microsoft.Office.Interop.Word;

namespace InfowareVSTO.Windows.ParagraphNumbering.Model
{
    public class FontPreferences
    {
        public string Name { get; set; }

        public float Size { get; set; }

        public bool IsBold { get; set; }

        public bool IsItalic { get; set; }

        public bool IsAllCaps { get; set; }

        public WdUnderline UnderlineType { get; set; }

        public string HexColor { get; set; }

        public WdLanguageID LanguageId { get; set; }

        public FontPreferences(string name, float size, bool isBold, bool isItalic, bool isAllCaps, WdUnderline underlineType,
            string hexColor, WdLanguageID languageId)
        {
            Name = name;
            Size = size;
            IsBold = isBold;
            IsItalic = isItalic;
            IsAllCaps = isAllCaps;
            UnderlineType = underlineType;
            HexColor = hexColor;
            LanguageId = languageId;
        }

        public FontPreferences()
        {

        }

        public void Copy(FontPreferences fontPreferences)
        {
            Name = fontPreferences.Name;
            Size = fontPreferences.Size;
            IsBold = fontPreferences.IsBold;
            IsItalic = fontPreferences.IsItalic;
            IsAllCaps = fontPreferences.IsAllCaps;
            UnderlineType = fontPreferences.UnderlineType;
            HexColor = fontPreferences.HexColor;
            LanguageId = fontPreferences.LanguageId;
        }
    }
}
