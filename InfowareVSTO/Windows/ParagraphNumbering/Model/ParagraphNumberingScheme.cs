﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using InfowareVSTO.Windows.ParagraphNumbering.Model;
using InfowareVSTO.Windows.ParagraphNumbering.Model.Enums;
using InfowareVSTO.Windows.ParagraphNumbering.Util;
using Microsoft.Office.Interop.Word;
using InfowareVSTO.Windows.ParagraphNumbering.Model.Enums;

namespace InfowareVSTO.Windows.ParagraphNumbering
{
    public class ParagraphNumberingScheme :INotifyPropertyChanged
    {
        public string Name { get; set; }

        public string DisplayName { get; set; }

        public TreeViewStartLocation? TreeStartLocation { get; set; }

        public List<ParagraphNumberingLevel> ParagraphNumberingLevels { get; set; }

        public string Description { get; set; }

        public bool IsApplied { get; set; }

        public bool IsAddedToDocument { get; set; }

        public string BaseStyleName { get; set; }

        public bool CreateContinuationParagraphStyles { get; set; }

        public string RootStylePrefix { get; set; }

        public string StyleAliasPrefix { get; set; }

        public bool IsSchemeFavorite 
        {
            get
            {
                return ParagraphNumberingSchemeManager.Instance.FavoriteListNames?.Contains(this.Name) == true;
            }
            set
            {
                OnPropertyChanged();
            }
        }

        public ListTemplate ToSync { get; set; }

        public ParagraphNumberingScheme(string name, string displayName, List<ParagraphNumberingLevel> paragraphNumeringLevels, string description = "",
            bool createContinuationParagraphStyles = true, string baseStyleName = null)
        {
            Name = name;
            DisplayName = displayName;
            ParagraphNumberingLevels = paragraphNumeringLevels;

            Description = description;

            BaseStyleName = baseStyleName ?? Utils.GetLocalizedStyleNameFromBuiltInStyle(WdBuiltinStyle.wdStyleNormal);
            CreateContinuationParagraphStyles = createContinuationParagraphStyles;
        }

        private ParagraphNumberingScheme()
        {

        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        public ParagraphNumberingScheme Clone()
        {
            //// TODO: proper implementation that doesn't depend on Newtonsoft.Json

            //ParagraphNumberingScheme newParagraphNumberingScheme = JsonConvert.DeserializeObject<ParagraphNumberingScheme>(JsonConvert.SerializeObject(this));

            //// Necessary due to the way we clone the objects. NumberingParagraphFormat needs to reference the exact same ParagraphFormat instance
            //foreach (ParagraphNumberingLevel numberingLevel in newParagraphNumberingScheme.ParagraphNumberingLevels)
            //{
            //    numberingLevel.ContinuationParagraphFormat.NumberingParagraphFormat = numberingLevel.ParagraphFormat;
            //}

            var newParagraphNumberingScheme = new ParagraphNumberingScheme
            {
                ParagraphNumberingLevels = new List<ParagraphNumberingLevel>()
            };

            for (int i = 0; i < ParagraphNumberingLevels.Count; i++)
            {
                var numberingLevel = new ParagraphNumberingLevel()
                {
                    ParagraphFormat = new ParagraphFormat()
                    {
                        FontPreferences = new ParagraphFontPreferences()
                    },

                    FontPreferences = new FontPreferences()
                };

                numberingLevel.ContinuationParagraphFormat = new ContinuationParagraphFormat()
                {
                    FontPreferences = new ParagraphFontPreferences(),
                    NumberingParagraphFormat = numberingLevel.ParagraphFormat
                };

                newParagraphNumberingScheme.ParagraphNumberingLevels.Add(numberingLevel);
            }

            newParagraphNumberingScheme.Copy(this);

            return newParagraphNumberingScheme;
        }

        public void Copy(ParagraphNumberingScheme paragraphNumberingScheme)
        {
            Name = paragraphNumberingScheme.Name;
            DisplayName = paragraphNumberingScheme.DisplayName;

            for (int i = 0; i < ParagraphNumberingLevels.Count; i++)
            {
                ParagraphNumberingLevels[i].Copy(paragraphNumberingScheme.ParagraphNumberingLevels[i]);
            }

            Description = paragraphNumberingScheme.Description;
            IsApplied = paragraphNumberingScheme.IsApplied;
            IsAddedToDocument = paragraphNumberingScheme.IsAddedToDocument;
            BaseStyleName = paragraphNumberingScheme.BaseStyleName;
            CreateContinuationParagraphStyles = paragraphNumberingScheme.CreateContinuationParagraphStyles;
            RootStylePrefix = paragraphNumberingScheme.RootStylePrefix;
            StyleAliasPrefix = paragraphNumberingScheme.StyleAliasPrefix;
        }

        public static ParagraphNumberingScheme CreateNew(string name, string displayName)
        {
            return new ParagraphNumberingScheme(name, displayName, new List<ParagraphNumberingLevel>()
            {      new ParagraphNumberingLevel(1, "%1.", WdListNumberStyle.wdListNumberStyleArabic,
                    new ParagraphFormat(0, 8, WdLineSpacing.wdLineSpaceSingle, 0, 0, 0, SpecialParagraphIndentation.HANGING, 0.5f,
                     false, false, false, true), 0.5f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 0),

                    new ParagraphNumberingLevel(2, "(%2)", WdListNumberStyle.wdListNumberStyleLowercaseLetter,
                    new ParagraphFormat(0, 8, WdLineSpacing.wdLineSpaceSingle, 0, 0.5f, 0, SpecialParagraphIndentation.HANGING, 0.5f,
                     false, false, false, true), 1f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 0.5f),

                    new ParagraphNumberingLevel(3, "(%3)", WdListNumberStyle.wdListNumberStyleLowercaseRoman,
                    new ParagraphFormat(0, 8, WdLineSpacing.wdLineSpaceSingle, 0, 1.2f, 0, SpecialParagraphIndentation.HANGING, 0.3f,
                     false, false, false, true), 1.5f, numberAlignment:WdListLevelAlignment.wdListLevelAlignRight, numberIndent: 1.2f),

                    new ParagraphNumberingLevel(4, "(%4)", WdListNumberStyle.wdListNumberStyleUppercaseLetter,
                    new ParagraphFormat(0, 8, WdLineSpacing.wdLineSpaceSingle, 0, 1.5f, 0, SpecialParagraphIndentation.HANGING, 0.5f,
                     false, false, false, true), 2f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 1.5f),

                    new ParagraphNumberingLevel(5, "(%5)", WdListNumberStyle.wdListNumberStyleUppercaseRoman,
                    new ParagraphFormat(0, 8, WdLineSpacing.wdLineSpaceSingle, 0, 2.2f, 0, SpecialParagraphIndentation.HANGING, 0.3f,
                     false, false, false, true), 2.5f, numberAlignment:WdListLevelAlignment.wdListLevelAlignRight, numberIndent: 2.2f),

                    new ParagraphNumberingLevel(6, "(%6)", WdListNumberStyle.wdListNumberStyleArabic,
                    new ParagraphFormat(0, 8, WdLineSpacing.wdLineSpaceSingle, 0, 2.5f, 0, SpecialParagraphIndentation.HANGING, 0.5f,
                     false, false, false, true), 3f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 2.5f),

                    new ParagraphNumberingLevel(7, "%7)", WdListNumberStyle.wdListNumberStyleLowercaseLetter,
                    new ParagraphFormat(0, 8, WdLineSpacing.wdLineSpaceSingle, 0, 3.0f, 0, SpecialParagraphIndentation.HANGING, 0.5f,
                     false, false, false, true), 3.5f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 3f),

                    new ParagraphNumberingLevel(8, "%8)", WdListNumberStyle.wdListNumberStyleLowercaseRoman,
                    new ParagraphFormat(0, 8, WdLineSpacing.wdLineSpaceSingle, 0, 3.7f, 0, SpecialParagraphIndentation.HANGING, 0.3f,
                     false, false, false, true), 4f, numberAlignment:WdListLevelAlignment.wdListLevelAlignRight, numberIndent: 3.7f),

                    new ParagraphNumberingLevel(9, "%9)", WdListNumberStyle.wdListNumberStyleArabic,
                    new ParagraphFormat(0, 8, WdLineSpacing.wdLineSpaceSingle, 0, 4.0f, 0, SpecialParagraphIndentation.HANGING, 0.5f,
                     false, false, false, true), 4.5f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 4f)

            });
        
        }
    }
}
