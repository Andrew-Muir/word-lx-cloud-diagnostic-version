﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using InfowareVSTO.Common;
using InfowareVSTO.Windows.ParagraphNumbering.Model;

namespace InfowareVSTO.Windows.ParagraphNumbering
{
    /// <summary>
    /// Interaction logic for ParagraphNumberingSetForAllLevelsWindow.xaml
    /// </summary>
    public partial class ParagraphNumberingSetForAllLevelsWindow : InfowareWindow
    {
        private readonly Action onOkClick;

        private ParagraphNumberingScheme selectedParagraphNumberingScheme;

        private ParagraphNumberingLevel firstParagraphNumberingLevel;

        public AllLevelsOption AllLevelsOption { get; set; }

        public List<SpecialParagraphIndentationOption> SpecialParagraphIndentationOptions { get; set; }

        public ParagraphNumberingSetForAllLevelsWindow(ParagraphNumberingScheme paragraphNumberingScheme, Action onOkClick)
        {
            InitializeComponent();

            this.onOkClick = onOkClick;
            selectedParagraphNumberingScheme = paragraphNumberingScheme;
            firstParagraphNumberingLevel = ListTemplateUtil.GetFirstLevelOfParagraphScheme(selectedParagraphNumberingScheme);
            
            AllLevelsOption = new AllLevelsOption(selectedParagraphNumberingScheme, firstParagraphNumberingLevel);
            SpecialParagraphIndentationOptions = SpecialParagraphIndentationOptionManager.Instance.SpecialParagraphIndentationOptions;

            DataContext = this;
        }

        private void OnBtnOkClick(object sender, System.Windows.RoutedEventArgs e)
        {
            SyncSelectedParagraphNumberingSchemeWithUi();
            Close();

            onOkClick();
        }

        private void SyncSelectedParagraphNumberingSchemeWithUi()
        {
            firstParagraphNumberingLevel.NumberIndent = Utils.LimitMaxListLevelValues(AllLevelsOption.FirstLevelNumberIndent, CustomConstants.LIST_LEVEL_MAX_POSITIONS_VALUE_IN_INCHES);
            firstParagraphNumberingLevel.TabPosition = Utils.LimitMaxListLevelValues(AllLevelsOption.FirstLevelNumberIndent + AllLevelsOption.FirstLevelTextIndent, CustomConstants.LIST_LEVEL_MAX_POSITIONS_VALUE_IN_INCHES);

            firstParagraphNumberingLevel.ParagraphFormat.LeftIndent = Utils.LimitMaxListLevelValues(AllLevelsOption.FirstLevelNumberIndent, CustomConstants.LIST_LEVEL_MAX_POSITIONS_VALUE_IN_INCHES);
            firstParagraphNumberingLevel.ParagraphFormat.SpecialIndentType = AllLevelsOption.FirstLineSpecialParagraphIndentationType.Value;

            firstParagraphNumberingLevel.ContinuationParagraphFormat.MirrorNumberingParagraphFormat = false;

            if (firstParagraphNumberingLevel.ParagraphFormat.SpecialIndentType == SpecialParagraphIndentation.HANGING)
            {
                firstParagraphNumberingLevel.ParagraphFormat.SpecialIndentValue = Utils.LimitMaxListLevelValues(AllLevelsOption.FirstLineSpecialIndentationValue, CustomConstants.LIST_LEVEL_MAX_POSITIONS_VALUE_IN_INCHES);
                firstParagraphNumberingLevel.ContinuationParagraphFormat.LeftIndent = Utils.LimitMaxListLevelValues(firstParagraphNumberingLevel.TabPosition, CustomConstants.LIST_LEVEL_MAX_POSITIONS_VALUE_IN_INCHES);
            }
            else if(firstParagraphNumberingLevel.ParagraphFormat.SpecialIndentType == SpecialParagraphIndentation.FIRST_LINE)
            {
                firstParagraphNumberingLevel.ParagraphFormat.SpecialIndentValue = Utils.LimitMaxListLevelValues(AllLevelsOption.FirstLineSpecialIndentationValue, CustomConstants.LIST_LEVEL_MAX_POSITIONS_VALUE_IN_INCHES);
                firstParagraphNumberingLevel.ContinuationParagraphFormat.LeftIndent = Utils.LimitMaxListLevelValues(firstParagraphNumberingLevel.ParagraphFormat.LeftIndent, CustomConstants.LIST_LEVEL_MAX_POSITIONS_VALUE_IN_INCHES);
            }
            else if(firstParagraphNumberingLevel.ParagraphFormat.SpecialIndentType == SpecialParagraphIndentation.NONE)
            {
                firstParagraphNumberingLevel.ParagraphFormat.SpecialIndentValue = 0;
                firstParagraphNumberingLevel.ContinuationParagraphFormat.LeftIndent = Utils.LimitMaxListLevelValues(firstParagraphNumberingLevel.ParagraphFormat.LeftIndent, CustomConstants.LIST_LEVEL_MAX_POSITIONS_VALUE_IN_INCHES);
            }

            if (selectedParagraphNumberingScheme?.ParagraphNumberingLevels != null && selectedParagraphNumberingScheme.ParagraphNumberingLevels.Count > 0)
            {
                foreach (ParagraphNumberingLevel numberingLevel in selectedParagraphNumberingScheme.ParagraphNumberingLevels.Skip(firstParagraphNumberingLevel.Index))
                {
                    ParagraphNumberingLevel previousLevel = null;

                    if (numberingLevel == selectedParagraphNumberingScheme.ParagraphNumberingLevels.First())
                    {
                        previousLevel = firstParagraphNumberingLevel;
                    }
                    else
                    {
                        previousLevel = selectedParagraphNumberingScheme.ParagraphNumberingLevels[numberingLevel.Index - 2];
                    }

                    if (previousLevel != null)
                    {
                        numberingLevel.NumberIndent = Utils.LimitMaxListLevelValues(previousLevel.ParagraphFormat.LeftIndent + AllLevelsOption.AdditionalLevelIndent, CustomConstants.LIST_LEVEL_MAX_POSITIONS_VALUE_IN_INCHES);
                        numberingLevel.TabPosition = Utils.LimitMaxListLevelValues(previousLevel.ParagraphFormat.LeftIndent + AllLevelsOption.AdditionalLinesParagraphIndentationValue + AllLevelsOption.AdditionalLinesParagraphIndentationValue, CustomConstants.LIST_LEVEL_MAX_POSITIONS_VALUE_IN_INCHES);

                        numberingLevel.ParagraphFormat.LeftIndent = Utils.LimitMaxListLevelValues(previousLevel.ParagraphFormat.LeftIndent + AllLevelsOption.AdditionalLevelIndent, CustomConstants.LIST_LEVEL_MAX_POSITIONS_VALUE_IN_INCHES);
                        numberingLevel.ParagraphFormat.SpecialIndentType = AllLevelsOption.AdditionalLinesParagraphIndentationType.Value;

                        numberingLevel.ContinuationParagraphFormat.MirrorNumberingParagraphFormat = false;

                        if (numberingLevel.ParagraphFormat.SpecialIndentType == SpecialParagraphIndentation.HANGING)
                        {
                            numberingLevel.ParagraphFormat.SpecialIndentValue = Utils.LimitMaxListLevelValues(AllLevelsOption.AdditionalLinesParagraphIndentationValue, CustomConstants.LIST_LEVEL_MAX_POSITIONS_VALUE_IN_INCHES);
                            numberingLevel.ContinuationParagraphFormat.LeftIndent = Utils.LimitMaxListLevelValues(numberingLevel.TabPosition, CustomConstants.LIST_LEVEL_MAX_POSITIONS_VALUE_IN_INCHES);
                        }
                        else if(numberingLevel.ParagraphFormat.SpecialIndentType == SpecialParagraphIndentation.FIRST_LINE)
                        {
                            numberingLevel.ParagraphFormat.SpecialIndentValue = AllLevelsOption.AdditionalLinesParagraphIndentationValue;
                            numberingLevel.ContinuationParagraphFormat.LeftIndent = Utils.LimitMaxListLevelValues(numberingLevel.ParagraphFormat.LeftIndent, CustomConstants.LIST_LEVEL_MAX_POSITIONS_VALUE_IN_INCHES);
                        }
                        else if(numberingLevel.ParagraphFormat.SpecialIndentType == SpecialParagraphIndentation.NONE)
                        {
                            numberingLevel.ParagraphFormat.SpecialIndentValue = 0;
                            numberingLevel.ContinuationParagraphFormat.LeftIndent = Utils.LimitMaxListLevelValues(numberingLevel.ParagraphFormat.LeftIndent, CustomConstants.LIST_LEVEL_MAX_POSITIONS_VALUE_IN_INCHES);
                        }
                    }
                }
            }
        }

        private void OnBtnCancelClick(object sender, System.Windows.RoutedEventArgs e)
        {
            Close();
        }
    }
}
