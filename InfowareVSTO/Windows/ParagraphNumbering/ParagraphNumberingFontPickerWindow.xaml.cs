﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
using InfowareVSTO.Common.Language;
using InfowareVSTO.Windows.ParagraphNumbering.Model;
using Microsoft.Office.Interop.Word;
using Brushes = System.Windows.Media.Brushes;
using Paragraph = System.Windows.Documents.Paragraph;
using Pen = System.Windows.Media.Pen;

namespace InfowareVSTO.Windows.ParagraphNumbering
{
    /// <summary>
    /// Interaction logic for ParagraphNumberingFontPickerWindow.xaml
    /// </summary>
    public partial class ParagraphNumberingFontPickerWindow : InfowareWindow
    {
        public bool EnableSmallCapsCheckBox { get; }

        public bool EnableParagraphFontSpecificOptions { get; set; }

        public static List<int> FontSizes { get; set; }

        public static List<string> SystemFonts { get; set; }

        public static List<UnderliningOption> UnderliningOptions { get; set; }

        private const string SAMPLE_TEXT_PREFIX = "____";

        private const string SAMPLE_TEXT_SUFFIX = SAMPLE_TEXT_PREFIX;

        private static string SAMPLE_TEXT = LanguageManager.GetTranslation(LanguageConstants.Sample, "Sample");

        private static readonly string SAMPLE_TEXT_UPPERCASE = SAMPLE_TEXT.ToUpperInvariant();

        private static readonly string SAMPLE_TEXT_LOWERCASE = SAMPLE_TEXT.ToLowerInvariant();

        private FontPreferences NumberingFontPreferences { get; }

        private FontPreferences UnNumberedParagraphFontPreferences { get; }

        private FontPreferences ActiveFontPreferences { get; }

        private readonly Action onOkAction;

        private const string FLOAT_REGEX = @"^[0-9]*\.?[0-9]*$";
        private const string NATURAL_NO_REGEX = @"^[0-9]*$";

        static ParagraphNumberingFontPickerWindow()
        {
            FontSizes = new List<int>();

            for (int fontSize = 8; fontSize <= 12; fontSize++)
            {
                FontSizes.Add(fontSize);
            }

            for (int fontSize = 14; fontSize <= 28; fontSize += 2)
            {
                FontSizes.Add(fontSize);
            }

            for (int fontSize = 36; fontSize <= 72; fontSize += 12)
            {
                FontSizes.Add(fontSize);
            }

            SystemFonts = new List<string>(Fonts.SystemFontFamilies.Select(it => it.ToString()).OrderBy(it => it));

            UnderliningOptions = new List<UnderliningOption>()
            {
                new UnderliningOption("None", WdUnderline.wdUnderlineNone),
                new UnderliningOption("Single", WdUnderline.wdUnderlineSingle),
                new UnderliningOption("Words", WdUnderline.wdUnderlineWords),
                new UnderliningOption("Double", WdUnderline.wdUnderlineDouble),
                new UnderliningOption("Dotted", WdUnderline.wdUnderlineDotted),
                new UnderliningOption("Dash", WdUnderline.wdUnderlineDash),
                new UnderliningOption("DashDot", WdUnderline.wdUnderlineDotDash),
                new UnderliningOption("DashDotDot", WdUnderline.wdUnderlineDotDotDash),
                new UnderliningOption("Wave", WdUnderline.wdUnderlineWavy),
                new UnderliningOption("Thick", WdUnderline.wdUnderlineThick)
            };
        }

        public ParagraphNumberingFontPickerWindow(Action onOkAction = null, FontPreferences numberingFontPreferences = null, 
            ParagraphFontPreferences paragraphFontPreferences = null, ParagraphFontPreferences unNumberedParagraphForntPreferences = null, bool enableSmallCapsCheckBox = true, bool enableParagraphFontSpecificOptions = false)
        {
            InitializeComponent();

            this.onOkAction = onOkAction;

            if (enableParagraphFontSpecificOptions)
            {
                Title = LanguageManager.GetTranslation(LanguageConstants.ParagraphFontFormatting, "Paragraph Font Formatting");
            }
            else
            {
                Title = LanguageManager.GetTranslation(LanguageConstants.NumberFontFormatting, "Number Font Formatting");
            }

            NumberingFontPreferences = numberingFontPreferences;

            UnNumberedParagraphFontPreferences = unNumberedParagraphForntPreferences;

            EnableSmallCapsCheckBox = enableSmallCapsCheckBox;

            if (numberingFontPreferences != null)
            {
                ActiveFontPreferences = numberingFontPreferences;
            }

            if (unNumberedParagraphForntPreferences != null)
            {
                ActiveFontPreferences = unNumberedParagraphForntPreferences;
            }

            if (paragraphFontPreferences != null)
            {
                ActiveFontPreferences = paragraphFontPreferences;
            }

            EnableParagraphFontSpecificOptions = enableParagraphFontSpecificOptions;
            DataContext = this;
            InitUi();
        }

        private void InitUi()
        {
            SyncUiWithFontPreferences();
        }

        private UnderliningOption GetUnderliningOption(WdUnderline underlineType)
        {
            return UnderliningOptions.FirstOrDefault(it => it.Value == underlineType);
        }

        private void SyncUiWithFontPreferences()
        {
            if (ActiveFontPreferences is ParagraphFontPreferences paragraphFontPreferences)
            {
                CbIsSmallCaps.IsChecked = paragraphFontPreferences.IsSmallCaps;
                CbApplyToUnnumberedStyle.IsChecked = paragraphFontPreferences.ApplyToUnnumbererdStyle;
                CbNumberInheritsParagraphFont.IsChecked = paragraphFontPreferences.ApplyToNumberStyle;
            }


            if (ActiveFontPreferences.Size == Math.Floor(ActiveFontPreferences.Size) && CbFontSizes.Items.Contains((int)ActiveFontPreferences.Size))
            {
                CbFontSizes.SelectedItem = ActiveFontPreferences.Size;
            }
            else
            {
                CbFontSizes.Text = ActiveFontPreferences.Size.ToString("0.#");
            }
            CbFonts.SelectedIndex = SystemFonts.IndexOf(ActiveFontPreferences.Name);
            FontColor.SelectedColor = (Color) ColorConverter.ConvertFromString(ActiveFontPreferences.HexColor ?? "#000000");
            CbUnderliningType.SelectedItem = GetUnderliningOption(ActiveFontPreferences.UnderlineType);

            CbIsBold.IsChecked = ActiveFontPreferences.IsBold;
            CbIsItalic.IsChecked = ActiveFontPreferences.IsItalic;
            CbIsAllCaps.IsChecked = ActiveFontPreferences.IsAllCaps;
        }

        private void SyncFontPreferencesWithUi()
        {
            SyncFontPreferencesWithUi(ActiveFontPreferences);

            if (ActiveFontPreferences is ParagraphFontPreferences paragraphFontPreferences)
            {
                paragraphFontPreferences.IsSmallCaps = (bool) CbIsSmallCaps.IsChecked;
                paragraphFontPreferences.ApplyToUnnumbererdStyle = (bool) CbApplyToUnnumberedStyle.IsChecked;
                paragraphFontPreferences.ApplyToNumberStyle = (bool) CbNumberInheritsParagraphFont.IsChecked;

                if ((bool) CbApplyToUnnumberedStyle.IsChecked)
                {
                    SyncFontPreferencesWithUi(UnNumberedParagraphFontPreferences);
                }

                if ((bool) CbNumberInheritsParagraphFont.IsChecked && NumberingFontPreferences != null)
                {
                    SyncFontPreferencesWithUi(NumberingFontPreferences);
                }
            }
        }

        private void SyncFontPreferencesWithUi(FontPreferences fontPreferences)
        {
            fontPreferences.Name = CbFonts.SelectedItem?.ToString();
            if (CbFontSizes.SelectedItem != null)
            {
                fontPreferences.Size = (int)CbFontSizes.SelectedItem;
            }
            else if (float.TryParse(CbFontSizes.Text, out float floatResult))
            {
                fontPreferences.Size = floatResult;
            }

            fontPreferences.HexColor = new ColorConverter().ConvertToString(FontColor.SelectedColor);
            fontPreferences.UnderlineType = ((UnderliningOption) CbUnderliningType.SelectedItem).Value;
            fontPreferences.IsBold = (bool) CbIsBold.IsChecked;
            fontPreferences.IsItalic = (bool) CbIsItalic.IsChecked;
            fontPreferences.IsAllCaps = (bool) CbIsAllCaps.IsChecked;

            if (fontPreferences is ParagraphFontPreferences unNumberedParagraphFontPreferences)
            {
                unNumberedParagraphFontPreferences.IsSmallCaps = (bool) CbIsSmallCaps.IsChecked;
            }
        }

        private WdUnderline GetSelectedUnderliningStyleValue()
        {
            return ((UnderliningOption) CbUnderliningType.SelectedItem).Value;
        }

        private void RenderPreview()
        {
            try
            {
                var sampleInline = new Run(SAMPLE_TEXT);

                if ((bool) CbIsAllCaps.IsChecked)
                {
                    sampleInline.Text = SAMPLE_TEXT_UPPERCASE;
                }
                else if ((bool) CbIsSmallCaps.IsChecked)
                {
                    sampleInline.Typography.Capitals = FontCapitals.AllSmallCaps;
                }

                var paragraph = new Paragraph()
                {
                    TextAlignment = TextAlignment.Center,
                };

                paragraph.Inlines.Add(SAMPLE_TEXT_PREFIX);
                paragraph.Inlines.Add(sampleInline);
                paragraph.Inlines.Add(SAMPLE_TEXT_SUFFIX);

                FdPreview.Blocks.Clear();
                FdPreview.Blocks.Add(paragraph);

                sampleInline.FontWeight = (bool) CbIsBold.IsChecked ? FontWeights.Bold : FontWeights.Normal;
                sampleInline.FontStyle = (bool) CbIsItalic.IsChecked ? FontStyles.Italic : FontStyles.Normal;

                int? fontSize = CbFontSizes.SelectedItem as int?;
                if (fontSize.HasValue)
                {
                    sampleInline.FontSize = fontSize.Value;
                }
                else if (double.TryParse(CbFontSizes.Text, out double doubleResult))
                {
                    sampleInline.FontSize = doubleResult;
                }
                sampleInline.FontFamily = Fonts.SystemFontFamilies.FirstOrDefault(it => it.ToString() == CbFonts.SelectedItem.ToString());

                sampleInline.Foreground = new SolidColorBrush(FontColor.SelectedColor.Value);

                var textDecoration = new TextDecoration()
                {
                    Pen = new Pen
                    {
                        Thickness = 1,
                        Brush = Brushes.Black,
                    }
                };

                switch (GetSelectedUnderliningStyleValue())
                {
                    case WdUnderline.wdUnderlineNone:
                        break;
                    case WdUnderline.wdUnderlineSingle:
                    case WdUnderline.wdUnderlineWords:
                        sampleInline.TextDecorations = TextDecorations.Underline;
                        break;
                    case WdUnderline.wdUnderlineDouble:
                        // TODO: inlines don't support double underlines, so we use a simple underline
                        sampleInline.TextDecorations = TextDecorations.Underline;
                        break;
                    case WdUnderline.wdUnderlineDotted:
                        textDecoration.Pen.DashStyle = DashStyles.Dot;
                        sampleInline.TextDecorations.Add(textDecoration);
                        break;
                    case WdUnderline.wdUnderlineDash:
                        textDecoration.Pen.DashStyle = DashStyles.Dash;
                        sampleInline.TextDecorations.Add(textDecoration);
                        break;
                    case WdUnderline.wdUnderlineDotDash:
                        textDecoration.Pen.DashStyle = DashStyles.DashDot;
                        sampleInline.TextDecorations.Add(textDecoration);
                        break;
                    case WdUnderline.wdUnderlineDotDotDash:
                        textDecoration.Pen.DashStyle = DashStyles.DashDotDot;
                        sampleInline.TextDecorations.Add(textDecoration);
                        break;
                    case WdUnderline.wdUnderlineWavy:
                        // TODO: inlines don't support wavy underlines, so we use a simple underline
                        sampleInline.TextDecorations = TextDecorations.Underline;
                        break;
                    case WdUnderline.wdUnderlineThick:
                        textDecoration.Pen.DashStyle = DashStyles.Solid;
                        textDecoration.Pen.Thickness = 2;
                        sampleInline.TextDecorations.Add(textDecoration);
                        break;
                }

                textDecoration.PenThicknessUnit = TextDecorationUnit.FontRecommended;
            }
            catch (NullReferenceException)
            {
                // Sometimes SelectedItem objects are null, to avoid checking each and every one of them, catch and ignore the exception
            }
        }

        #region Event handlers

        private void OnCbFontsSelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            RenderPreview();
        }

        private void OnCbFontSizesSelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            RenderPreview();
        }

        private void OnCbUnderliningTypeSelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            RenderPreview();
        }

        private void OnCbIsBoldCheckedChanged(object sender, RoutedEventArgs e)
        {
            RenderPreview();
        }

        private void OnCbIsItalicCheckedChanged(object sender, RoutedEventArgs e)
        {
            RenderPreview();
        }

        private void OnCbIsAllCapsCheckedChanged(object sender, RoutedEventArgs e)
        {
            if ((bool) CbIsAllCaps.IsChecked && (bool) CbIsSmallCaps.IsChecked)
            {
                CbIsSmallCaps.IsChecked = false;
            }

            RenderPreview();
        }

        private void OnCbIsSmallCapsCheckedChanged(object sender, RoutedEventArgs e)
        {
            if ((bool) CbIsSmallCaps.IsChecked)
            {
                if ((bool) CbIsAllCaps.IsChecked)
                {
                    CbIsAllCaps.IsChecked = false;
                }

                CbNumberInheritsParagraphFont.IsChecked = CbNumberInheritsParagraphFont.IsEnabled = false;
            }
            else
            {
                CbNumberInheritsParagraphFont.IsChecked = ((ParagraphFontPreferences) ActiveFontPreferences).ApplyToNumberStyle;
                CbNumberInheritsParagraphFont.IsEnabled = EnableParagraphFontSpecificOptions;
            }

            RenderPreview();
        }

        private void OnFontColorChanged(object sender, RoutedPropertyChangedEventArgs<System.Windows.Media.Color?> e)
        {
            RenderPreview();
        }

        private void OnOkButtonClick(object sender, RoutedEventArgs e)
        {
            SyncFontPreferencesWithUi();
            Close();

            onOkAction?.Invoke();
        }

        private void OnCancelButtonClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        #endregion

        private void CbFontSizes_PreviewTextInput(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            bool hasPoint = false;
            ComboBox senderTB = sender as ComboBox;

            if (senderTB != null && e != null)
            {
                hasPoint = senderTB.Text.Contains('.');
                Regex regex = new Regex(hasPoint ? NATURAL_NO_REGEX : FLOAT_REGEX);
                e.Handled = !regex.IsMatch(e.Text);
            }
        }
    }
}
