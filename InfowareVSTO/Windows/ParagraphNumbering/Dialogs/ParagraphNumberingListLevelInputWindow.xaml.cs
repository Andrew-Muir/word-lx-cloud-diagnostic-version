﻿using System;
using System.Windows;

namespace InfowareVSTO.Windows.ParagraphNumbering.Dialogs
{
    /// <summary>
    /// Interaction logic for ParagraphNumberingListLevelInputWindow.xaml
    /// </summary>
    public partial class ParagraphNumberingListLevelInputWindow : InfowareWindow
    {
        private readonly Action<int> onOkCallback;

        public int ListStartingNumber { get; set; }

        public ParagraphNumberingListLevelInputWindow(int currentListStartingNumber, Action<int> onOkCallback)
        {
            InitializeComponent();

            ListStartingNumber = currentListStartingNumber;
            this.onOkCallback = onOkCallback;

            DataContext = this;
        }

        private void OnOkButtonClick(object sender, RoutedEventArgs e)
        {
            onOkCallback(ListStartingNumber);
            Close();
        }

        private void OnTbListStartingNumberTextInput(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            bool isNumber = int.TryParse(e.Text, out int number);

            if (!isNumber)
            {
                e.Handled = true;
            }

            // Only allow numbers greater than 1
            e.Handled = number < 1;
        }
    }
}
