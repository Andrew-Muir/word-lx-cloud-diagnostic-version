﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Input;
using InfowareVSTO.Common.Language;
using InfowareVSTO.Util;
using InfowareVSTO.Windows.ParagraphNumbering.Model.Enums;
using InfowareVSTO.Windows.ParagraphNumbering.Repositories;
using InfowareVSTO.Windows.ParagraphNumbering.Util;
using Microsoft.Office.Interop.Word;
using Application = Microsoft.Office.Interop.Word.Application;
using Button = System.Windows.Controls.Button;
using Style = Microsoft.Office.Interop.Word.Style;
using Window = Microsoft.Office.Interop.Word.Window;

namespace InfowareVSTO.Windows.ParagraphNumbering.Toolbars
{
    /// <summary>
    /// Interaction logic for ParagraphNumberingToolbarWindow.xaml
    /// </summary>
    public partial class ParagraphNumberingToolbarWindow : InfowareWindow, INotifyPropertyChanged
    {
        private const int MAX_LIST_LEVEL_INDEX = 9;

        private static readonly string MISSING_STYLE_TEMPLATE = "The style named {0} is not available in the current document";

        private static readonly string MISSING_LIST_IN_SELECTION = "No list or list item in selection";

        private readonly string INFO_MESSAGEBOX_TITLE;

        private readonly string ERROR_MESSAGEBOX_TITLE;

        // TODO: read from configuration file
        
        private static readonly Application app;

        private bool showExtendedButtons;

        private bool firstOpen = false;

        public bool ShowExtendedButtons
        {
            get => showExtendedButtons;

            set
            {
                showExtendedButtons = value;

                OnPropertyChanged();
            }
        }

        public ObservableCollection<object> ActiveDocumentParagraphNumberingSchemes { get; } = ParagraphNumberingSchemeManager.Instance.ActiveDocumentParagraphNumberingSchemes;

        public event PropertyChangedEventHandler PropertyChanged;

        static ParagraphNumberingToolbarWindow()
        {
            app = Globals.ThisAddIn.Application;
        }

        public ParagraphNumberingToolbarWindow()
        {
            InitializeComponent();

            string title = "Paragraph Numbering Toolbar";

            INFO_MESSAGEBOX_TITLE = title;

            ERROR_MESSAGEBOX_TITLE = $"{title} - error";

            /*
             * We don't call the Restore() method directly, because that would update all properties based on the
             * values in the underlying repo, and that would reset the customization done by the user in the 
             * Main Options window (if the user somehow manages to open the toolbar while the Main Options window is open)
             */
            ParagraphNumberingSettingRepository.Instance.RestoreIfNeverRestored();

            ShowExtendedButtons = ParagraphNumberingSettingRepository.Instance.ShowExtendedToolbarButtons;

            ActiveDocumentParagraphNumberingSchemes.CollectionChanged += OnActiveDocumentParagraphNumberingSchemesCollectionChanged;

            if (ParagraphNumberingSettingRepository.Instance.NeverOpenedToolbar)
            {
                UpdatePosition(app.ActiveWindow);
                ParagraphNumberingSettingRepository.Instance.NeverOpenedToolbar = false;
                ParagraphNumberingSettingRepository.Instance.Store();
            }
            else if (ParagraphNumberingSettingRepository.Instance.RememberToolbarPosition)
            {
                RestorePreviousPosition();
            }
            this.BtnReturnToNormal.ToolTip = string.Format(LanguageManager.GetTranslation(LanguageConstants.ReturnTo0Style, "Return to {0} Style"), Utils.GetStandardStyleName());

            DataContext = this;

            this.Loaded += ParagraphNumberingToolbarWindow_Loaded;
        }

        private void ParagraphNumberingToolbarWindow_Loaded(object sender, RoutedEventArgs e)
        {
            firstOpen = true;
        }

        internal void UpdatePosition(Microsoft.Office.Interop.Word.Window wordWindow)
        {
            float left = app.PointsToPixels(wordWindow.Left);
            float top = app.PointsToPixels(wordWindow.Top);

            Left = left + ParagraphNumberingSettingRepository.Instance.RelativeToolbarPosition.X;
            Top = top + ParagraphNumberingSettingRepository.Instance.RelativeToolbarPosition.Y;
        }

        private void RestorePreviousPosition()
        {
            Left = ParagraphNumberingSettingRepository.Instance.ToolbarPosition.X;
            Top = ParagraphNumberingSettingRepository.Instance.ToolbarPosition.Y;
        }

        private void UpdateRelativeToolbarPosition()
        {
            if (ParagraphNumberingSettingRepository.Instance.RememberToolbarPosition)
            {
                Window wordWindow = app.ActiveWindow;
                float left = app.PointsToPixels(wordWindow.Left);
                float top = app.PointsToPixels(wordWindow.Top);

                ParagraphNumberingSettingRepository.Instance.ToolbarPosition = new System.Windows.Point(Left, Top);
                ParagraphNumberingSettingRepository.Instance.RelativeToolbarPosition = new System.Windows.Point(Left - left, Top - top);

                ParagraphNumberingSettingRepository.Instance.Store();
            }
        }

        #region Callbacks

        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void OnActiveDocumentParagraphNumberingSchemesCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (ActiveDocumentParagraphNumberingSchemes.Count == 0)
            {
                return;
            }

            if (ListTemplateUtil.IsListTemplateUsedInSelection())
            {
                string usedParagraphNumberingSchemeName = ListTemplateUtil.GetNameOfListTemplateUsedInSelection();
                ParagraphNumberingScheme usedParagraphNumberingScheme = ParagraphNumberingSchemeManager.Instance.GetActiveDocumentParagraphNumberingSchemeByName(usedParagraphNumberingSchemeName);
                CbParagraphNumberingSchemes.SelectedItem = usedParagraphNumberingScheme;
            }
            else
            {
                CbParagraphNumberingSchemes.SelectedIndex = 0;
            }

            EnableDisableLevelButtons();
        }

        private void OnCbParagraphNumberingSchemesSelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (CbParagraphNumberingSchemes.IsDropDownOpen || firstOpen)
            {
                EnableDisableLevelButtons();
                Utils.ActivateApplication();
                firstOpen = false;
            }
        }

        private void EnableDisableLevelButtons()
        {
            ParagraphNumberingScheme selectedNumberingScheme = GetSelectedParagraphNumberingScheme();

            if (selectedNumberingScheme == null)
            {
                return;
            }

            int lastLevelIndex = 0;

            foreach (ParagraphNumberingLevel numberingLevel in selectedNumberingScheme.ParagraphNumberingLevels)
            {
                lastLevelIndex = numberingLevel.Index;

                var levelButon = (Button)PnlLevelButtons.Children[lastLevelIndex - 1];
                levelButon.IsEnabled = true;
            }

            // Disable buttons that don't have any corresponding levels
            for (int i = lastLevelIndex + 1; i <= MAX_LIST_LEVEL_INDEX; i++)
            {
                ((Button)PnlLevelButtons.Children[i - 1]).IsEnabled = false;
            }
            Globals.ThisAddIn.currentSelectedListTemplateName = ListTemplateUtil.GetListTemplateName(selectedNumberingScheme);
        }

        private void OnRightMouseButtonLevelClick(object sender, MouseButtonEventArgs e)
        {
            // Fails to get called when the left mouse button is called. For that, we use the OnLevelButtonClick() handler

            var listLevelButton = (Button) sender;
            int listLevelNumber = int.Parse(listLevelButton.Tag.ToString());

            e.Handled = true;

            ListTemplateUtil.ChangeListLevelInSelection(listLevelNumber, isContinuationParagraphStyle: true, ListTemplateUtil.GetListTemplateName(GetSelectedParagraphNumberingScheme()));
            RefreshScreen();
        }

        private void OnLeftMouseButtonLevelClick(object sender, RoutedEventArgs e)
        {
            var listLevelButton = (Button) sender;
            int listLevelNumber = int.Parse(listLevelButton.Tag.ToString());

            e.Handled = true;

            ListTemplateUtil.ChangeListLevelInSelection(listLevelNumber, isContinuationParagraphStyle: false, ListTemplateUtil.GetListTemplateName(GetSelectedParagraphNumberingScheme()));
            RefreshScreen();
        }

        private void OnPreviousLevelButtonClick(object sender, RoutedEventArgs e)
        {
            using (new IntensiveDocumentInteractionWrapper(undoTransactionName: "Level promotion"))
            {
                ChangeLevel(true);
            }
        }

        private void OnNextLevelButtonClick(object sender, RoutedEventArgs e)
        {
            using (new IntensiveDocumentInteractionWrapper(undoTransactionName: "Level demotion"))
            {
                ChangeLevel(false);
            }
        }

        private void ChangeLevel(bool promote)
        {
            if (!DocumentUtil.DocumentContainsLists())
            {
                new InfowareInformationWindow(MISSING_LIST_IN_SELECTION, ERROR_MESSAGEBOX_TITLE).ShowDialog();
                return;
            }


            Selection originalSelection = DocumentUtil.GetSelection();
            int originalStart = originalSelection.Start;
            int originalEnd = originalSelection.End;

            Range selectionRange = originalSelection.Range;

            foreach (Paragraph paragraph in selectionRange.Paragraphs)
            {
                Range paragraphRange = paragraph.Range;
                paragraphRange.Select();

                ListFormat paragraphListFormat = paragraphRange.ListFormat;

                string paragraphStyleName = ((Style) paragraph.get_Style()).NameLocal;

                if (paragraphListFormat.ListType == WdListType.wdListNoNumbering || paragraphListFormat.ListType == WdListType.wdListBullet)
                {
                    // Infoware specific un-numbered paragraph (eg.: Gen_No#_L1)

                    if (ListTemplateUtil.IsGeneratedContinuationParagraphStyle(paragraphStyleName))
                    {
                        paragraphStyleName = ListTemplateUtil.GetStyleNameWithoutAlias(paragraphStyleName);
                        int paragraphStyleLevelNumber = ListTemplateUtil.GetLevelNumberFromStyleName(paragraphStyleName);
                        int newListLevelNumber;

                        if (promote)
                        {
                            if (paragraphStyleLevelNumber <= 1)
                            {
                                // Nothing to do, already at level 1
                                continue;
                            }

                            newListLevelNumber = paragraphStyleLevelNumber - 1;
                        }
                        else
                        {
                            if (paragraphStyleLevelNumber >= 9)
                            {
                                // Nothing to do, already at the highest level (usually 9)
                                continue;
                            }

                            newListLevelNumber = paragraphStyleLevelNumber + 1;
                        }

                        string newParagraphStyleName = ListTemplateUtil.ReplaceStyleNameLevel(paragraphStyleName, newListLevelNumber);

                        if (!ListTemplateUtil.CheckStyleExists(newParagraphStyleName))
                        {
                            // No style with this name, executing the style dialog would error
                            continue;
                        }

                        DocumentUtil.ApplyStyleToSelection(newParagraphStyleName);
                    }
                    else
                    {
                        if (promote)
                        {
                            paragraph.Outdent();
                        }
                        else
                        {
                            paragraph.Indent();
                        }
                    }
                }
                else
                {
                    int listLevelNumber = paragraphListFormat.ListLevelNumber;

                    int newListLevelNumber;

                    if (promote)
                    {
                        // Promote (un-indent)

                        if (listLevelNumber == 1)
                        {
                            // Nothing to do, already at level 1
                            continue;
                        }

                        newListLevelNumber = listLevelNumber - 1;
                    }
                    else
                    {
                        // Demote (indent)

                        if (listLevelNumber >= paragraphListFormat.ListTemplate.ListLevels.Count)
                        {
                            // Nothing to do, already at the highest level (usually 9)
                            continue;
                        }

                        newListLevelNumber = listLevelNumber + 1;
                    }

                    string newListLevelStyleName = paragraphListFormat.ListTemplate?.ListLevels[newListLevelNumber].LinkedStyle;

                    /**
                     * Comments in the original extension state that the list template can be lost by direct number formatting
                     * 
                     * ------
                     * 'since our listtemplate can get disconnected by direct formatting on numbers
                        'we should try style already on the para
                        strTemp2 = .Style.NameLocal
                        'first, do we have an alias, remove it"
                     * -----
                     * 
                     * Code ported from the original extension
                     */

                    if (string.IsNullOrEmpty(newListLevelStyleName))
                    {
                        paragraphStyleName = ListTemplateUtil.GetStyleNameWithoutAlias(paragraphStyleName);

                        int paragraphStyleLevelNumber = ListTemplateUtil.GetLevelNumberFromStyleName(paragraphStyleName);

                        newListLevelNumber = paragraphStyleLevelNumber + (promote ? -1 : 1);

                        newListLevelStyleName = ListTemplateUtil.ReplaceStyleNameLevel(paragraphStyleName, newListLevelNumber);
                    }
                    else
                    {
                        newListLevelStyleName = ListTemplateUtil.EnsureFullParagraphStyleName(newListLevelStyleName, GetSelectedParagraphNumberingScheme());
                    }

                    ListTemplateUtil.RemoveNewLinePrefixInSelection();

                    DocumentUtil.ApplyStyleToSelection(newListLevelStyleName);

                    ParagraphNumberingLevel usedNumberingLevel = GetSelectedParagraphNumberingScheme().ParagraphNumberingLevels[newListLevelNumber - 1];

                    if (usedNumberingLevel.TrailingCharacter == TrailingCharacter.NEW_LINE)
                    {
                        ListTemplateUtil.AddNewLinePrefixInSelection();
                    }
                }
            }

            Utils.ReplaceTcFieldsInDocument(ThisAddIn.Instance.Application.ActiveDocument);

            Selection selection = DocumentUtil.GetSelection();
            selection.Start = originalStart;
            selection.End = originalEnd;
            selection.Select();

            RefreshScreen();
            Utils.ActivateApplication();
        }

        private void OnButtonToggleNumberOrContinuationStyleClick(object sender, RoutedEventArgs e)
        {
            using (new IntensiveDocumentInteractionWrapper(undoTransactionName: "Numbering / Continuation toggle"))
            {
                Selection originalSelection = DocumentUtil.GetSelection();
                int originalStart = originalSelection.Start;
                int originalEnd = originalSelection.End;

                Range selectionRange = originalSelection.Range;

                Paragraph selectedParagraph = originalSelection?.Range?.Paragraphs[1];
                dynamic paragraphStyle = selectedParagraph.get_Style();

                if (selectedParagraph != null)
                {
                    var paragraphStyleName = ListTemplateUtil.GetStyleNameWithoutAlias(paragraphStyle.NameLocal);
                    var levelIndex = ListTemplateUtil.GetLevelNumberFromStyleName(paragraphStyleName);

                    if(ListTemplateUtil.CheckIfStyleIsContinuationStyle(paragraphStyleName))
                    {
                        DocumentUtil.ApplyStyleToSelection(ListTemplateUtil.GetLinkedStyleNameFromContinuationStyleName(paragraphStyleName, levelIndex));
                    }
                    else
                    {
                        DocumentUtil.ApplyStyleToSelection(ListTemplateUtil.GetContinuationStyleNameFromLinkedStyleName(paragraphStyleName, levelIndex));
                    }
                }

                Utils.ReplaceTcFieldsInDocument(ThisAddIn.Instance.Application.ActiveDocument);

                Selection selection = DocumentUtil.GetSelection();
                originalSelection.Start = originalStart;
                originalSelection.End = originalEnd;
                originalSelection.Select();

                RefreshScreen();
                Utils.ActivateApplication();
            }
        }

        private void OnButtonRestartNumberingClick(object sender, RoutedEventArgs e)
        {
            if (DocumentUtil.DocumentContainsLists())
            {
                ListTemplateUtil.RestartNumberingInSelection();

                Utils.ReplaceTcFieldsInDocument(ThisAddIn.Instance.Application.ActiveDocument);
                
                RefreshScreen();
                Utils.ActivateApplication();
            }
        }

        private void OnButtonContinueNumberingClick(object sender, RoutedEventArgs e)
        {
            if (DocumentUtil.DocumentContainsLists())
            {
                ListTemplateUtil.ContinueNumberingInSelection();

                Utils.ReplaceTcFieldsInDocument(ThisAddIn.Instance.Application.ActiveDocument);

                RefreshScreen();
                Utils.ActivateApplication();
            }
        }

        private void OnButtonNormalStyleClick(object sender, RoutedEventArgs e)
        {
            using (new IntensiveDocumentInteractionWrapper(undoTransactionName: "Style reset"))
            {
                DocumentUtil.ApplyStyleToSelection(Utils.GetStandardStyle().NameLocal);

                Utils.ReplaceTcFieldsInDocument(ThisAddIn.Instance.Application.ActiveDocument);

                RefreshScreen();
                Utils.ActivateApplication();
            }
        }

        private void OnDragSpaceMouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
            {
                DragMove();
            }
        }

        private void OnDragSpaceMouseEnter(object sender, MouseEventArgs e)
        {
            Mouse.OverrideCursor = Cursors.SizeAll;
        }

        private void OnDragSpaceMouseLeave(object sender, MouseEventArgs e)
        {
            Mouse.OverrideCursor = null;
            UpdateRelativeToolbarPosition();

            Utils.ActivateApplication();
        }

        #endregion

        private ParagraphNumberingScheme GetSelectedParagraphNumberingScheme()
        {
            return (ParagraphNumberingScheme) CbParagraphNumberingSchemes.SelectedItem;
        }

        private void RefreshScreen()
        {
            Globals.ThisAddIn.Application.ScreenRefresh();
        }

        private void OnToolbarShrinkButtonClick(object sender, RoutedEventArgs e)
        {
            HideAllButtons();
        }

        private void OnToolbarUnshrinkButtonClick(object sender, RoutedEventArgs e)
        {
            ShowAllButtons();
        }

        private void OnToolbarHideButtonClick(object sender, RoutedEventArgs e)
        {
            ParagraphNumberingToolbarManager.Instance.ToggleVisibility();
        }

        private void OnUnshrinkButtonClick(object sender, RoutedEventArgs e)
        {
            ShowAllButtons();
        }

        private void HideAllButtons()
        {
            PnlAllButtons.Visibility = Visibility.Collapsed;
        }
        private void ShowAllButtons()
        {
            PnlAllButtons.Visibility = Visibility.Visible;
        }
    }
}
