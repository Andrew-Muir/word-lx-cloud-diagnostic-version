﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using InfowareVSTO.Windows.ParagraphNumbering.Model.Enums;
using InfowareVSTO.Windows.ParagraphNumbering.Repositories;

namespace InfowareVSTO.Windows.ParagraphNumbering
{
    /// <summary>
    /// Interaction logic for ParagraphNumberingMainOptionsWindow.xaml
    /// </summary>
    public partial class ParagraphNumberingMainOptionsWindow : InfowareWindow
    {
        public ParagraphNumberingSettingRepository Settings { get; set; }

        public ParagraphNumberingMainOptionsWindow()
        {
            InitializeComponent();

            Settings = ParagraphNumberingSettingRepository.Instance;
            Settings.Restore();

            InitUi();

            DataContext = this;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void InitUi()
        {
            UpdateStartLocationRadioButtons();
        }

        private void UpdateStartLocationRadioButtons()
        {
            switch (Settings.TreeViewStartLocation)
            {
                case TreeViewStartLocation.FIRM:
                    RbFirmStartLocation.IsChecked = true;
                    break;
                case TreeViewStartLocation.SYNCED:
                    RbSyncedStartLocation.IsChecked = true;
                    break;
                case TreeViewStartLocation.LAST_USED:
                    RbLastUsedStartLocation.IsChecked = true;
                    break;
                case TreeViewStartLocation.FAVOURITE:
                    RbFavoriteStartLocation.IsChecked = true;
                    break;
            }
        }

        private void OnStartLocationRadioButtonCheckedChanged(object sender, System.Windows.RoutedEventArgs e)
        {
            if (RbFirmStartLocation.IsChecked.Value)
            {
                Settings.TreeViewStartLocation = TreeViewStartLocation.FIRM;
            }
            else if (RbSyncedStartLocation.IsChecked.Value)
            {
                Settings.TreeViewStartLocation = TreeViewStartLocation.SYNCED;
            }
            else if (RbLastUsedStartLocation.IsChecked.Value)
            {
                Settings.TreeViewStartLocation = TreeViewStartLocation.LAST_USED;
            }
            else if (RbFavoriteStartLocation.IsChecked.Value)
            {
                Settings.TreeViewStartLocation = TreeViewStartLocation.FAVOURITE;
            }
        }

        private void OnOkButtonClick(object sender, System.Windows.RoutedEventArgs e)
        {
            Settings.Store();
            ParagraphNumberingToolbarManager.Instance.UpdateExtendedButtonState(Settings.ShowExtendedToolbarButtons);

            Close();
        }

        private void OnCancelButtonClick(object sender, System.Windows.RoutedEventArgs e)
        {
            Close();
        }

        private void OnExpandAllButtonClick(object sender, System.Windows.RoutedEventArgs e)
        {
            ExpanderGeneral.IsExpanded = ExpanderAdvanced.IsExpanded = true;
        }

        private void OnCollapseAllButtonClick(object sender, System.Windows.RoutedEventArgs e)
        {
            ExpanderGeneral.IsExpanded = ExpanderAdvanced.IsExpanded = false;
        }
    }
}
