﻿using InfowareVSTO.Util;
using Microsoft.Office.Interop.Word;

namespace InfowareVSTO.Windows.ParagraphNumbering.Util
{
    internal static class ParagraphFormatUtil
    {
        public static void OpenBorderSettingsDialog(ParagraphFormat paragraphFormat, System.Windows.Window parentWindow = null)
        {
            Dialog borderDialog = Globals.ThisAddIn.Application.Dialogs[WdWordDialog.wdDialogFormatBordersAndShading];
            borderDialog.DefaultTab = WdWordDialogTab.wdDialogFormatBordersAndShadingTabBorders;

            using (var temporaryParagraph = new TemporaryParagraph(DocumentUtil.GetSelection()))
            {
                Borders borders = temporaryParagraph.Paragraph.Borders;
                Shading shading = temporaryParagraph.Paragraph.Shading;

                Section firstSelectionSection = DocumentUtil.GetSelection().Sections.First;
                bool pageHasBorders = firstSelectionSection.Borders.Enable == -1;

                if (paragraphFormat.BordersAreEnabled)
                {
                    ApplyBorderSettingsToParagraph(paragraphFormat, temporaryParagraph.Paragraph);
                }

                int result = borderDialog.Show();

                if (parentWindow != null && (result == -2 || result == -1 || result == 0))
                {
                    parentWindow.Activate();
                    parentWindow.Focus();
                }

                if (result != 0)
                {
                    bool bordersAreEnabled = borders.Enable == -1;

                    paragraphFormat.BordersAreEnabled = bordersAreEnabled;

                    if (bordersAreEnabled)
                    {
                        Border leftBorder = borders[WdBorderType.wdBorderLeft];

                        if (leftBorder.LineStyle != WdLineStyle.wdLineStyleNone)
                        {
                            paragraphFormat.LeftBorderLineStyle = leftBorder.LineStyle;
                            paragraphFormat.LeftBorderLineWidth = leftBorder.LineWidth;
                            paragraphFormat.LeftBorderColor = leftBorder.Color;
                        }
                        else
                        {
                            paragraphFormat.LeftBorderLineStyle = WdLineStyle.wdLineStyleNone;
                        }

                        Border topBorder = borders[WdBorderType.wdBorderTop];

                        if (topBorder.LineStyle != WdLineStyle.wdLineStyleNone)
                        {
                            paragraphFormat.TopBorderLineStyle = topBorder.LineStyle;
                            paragraphFormat.TopBorderLineWidth = topBorder.LineWidth;
                            paragraphFormat.TopBorderColor = topBorder.Color;
                        }
                        else
                        {
                            paragraphFormat.TopBorderLineStyle = WdLineStyle.wdLineStyleNone;
                        }

                        Border rightBorder = borders[WdBorderType.wdBorderRight];

                        if (rightBorder.LineStyle != WdLineStyle.wdLineStyleNone)
                        {
                            paragraphFormat.RightBorderLineStyle = rightBorder.LineStyle;
                            paragraphFormat.RightBorderLineWidth = rightBorder.LineWidth;
                            paragraphFormat.RightBorderColor = rightBorder.Color;
                        }
                        else
                        {
                            paragraphFormat.RightBorderLineStyle = WdLineStyle.wdLineStyleNone;
                        }

                        Border bottomBorder = borders[WdBorderType.wdBorderBottom];

                        if (bottomBorder.LineStyle != WdLineStyle.wdLineStyleNone)
                        {
                            paragraphFormat.BottomBorderLineStyle = bottomBorder.LineStyle;
                            paragraphFormat.BottomBorderLineWidth = bottomBorder.LineWidth;
                            paragraphFormat.BottomBorderColor = bottomBorder.Color;
                        }
                        else
                        {
                            paragraphFormat.BottomBorderLineStyle = WdLineStyle.wdLineStyleNone;
                        }

                        paragraphFormat.BorderHasShadow = borders.Shadow;
                        paragraphFormat.BorderDistanceFromLeft = borders.DistanceFromLeft;
                        paragraphFormat.BorderDistanceFromTop = borders.DistanceFromTop;
                        paragraphFormat.BorderDistanceFromRight = borders.DistanceFromRight;
                        paragraphFormat.BorderDistanceFromBottom = borders.DistanceFromBottom;

                        // Code ported from the original code, but do we really need the conditions???
                        if (shading.Texture != WdTextureIndex.wdTextureNone || shading.BackgroundPatternColor != WdColor.wdColorAutomatic
                            || (shading.ForegroundPatternColor != WdColor.wdColorAutomatic && shading.ForegroundPatternColor != WdColor.wdColorBlack))
                        {
                            paragraphFormat.BorderShadingTexture = shading.Texture;
                            paragraphFormat.BorderShadingBackgroundColor = shading.BackgroundPatternColor;
                            paragraphFormat.BorderShadingForegroundColor = shading.ForegroundPatternColor;
                        }
                        else
                        {
                            paragraphFormat.BorderShadingTexture = WdTextureIndex.wdTextureNone;
                            paragraphFormat.BorderShadingBackgroundColor = WdColor.wdColorAutomatic;
                            paragraphFormat.BorderShadingForegroundColor = WdColor.wdColorAutomatic;
                        }
                    }

                    if (!pageHasBorders && firstSelectionSection.Borders.Enable == -1)
                    {
                        // User messed with the page level border (second tab in the dialog), clear the changes
                        firstSelectionSection.Borders.Enable = 0;

                        foreach (Border border in firstSelectionSection.Borders)
                        {
                            border.LineStyle = WdLineStyle.wdLineStyleNone;
                        }
                    }
                }
            }
        }

        private static void ApplyBorderSettingsToParagraph(ParagraphFormat paragraphFormat, Paragraph paragraph)
        {
            Borders borders = paragraph.Borders;
            Shading shading = paragraph.Shading;

            Border leftBorder = borders[WdBorderType.wdBorderLeft];

            leftBorder.LineStyle = paragraphFormat.LeftBorderLineStyle;

            if (paragraphFormat.LeftBorderLineStyle != WdLineStyle.wdLineStyleNone)
            {
                leftBorder.LineWidth = paragraphFormat.LeftBorderLineWidth;
                leftBorder.Color = paragraphFormat.LeftBorderColor;
            }

            Border topBorder = borders[WdBorderType.wdBorderTop];

            topBorder.LineStyle = paragraphFormat.TopBorderLineStyle;

            if (paragraphFormat.TopBorderLineStyle != WdLineStyle.wdLineStyleNone)
            {
                topBorder.LineWidth = paragraphFormat.TopBorderLineWidth;
                topBorder.Color = paragraphFormat.TopBorderColor;
            }

            Border rightBorder = borders[WdBorderType.wdBorderRight];

            rightBorder.LineStyle = paragraphFormat.RightBorderLineStyle;

            if (paragraphFormat.RightBorderLineStyle != WdLineStyle.wdLineStyleNone)
            {
                rightBorder.LineWidth = paragraphFormat.RightBorderLineWidth;
                rightBorder.Color = paragraphFormat.RightBorderColor;
            }

            Border bottomBorder = borders[WdBorderType.wdBorderBottom];

            bottomBorder.LineStyle = paragraphFormat.BottomBorderLineStyle;

            if (paragraphFormat.BottomBorderLineStyle != WdLineStyle.wdLineStyleNone)
            {
                bottomBorder.LineWidth = paragraphFormat.BottomBorderLineWidth;
                bottomBorder.Color = paragraphFormat.BottomBorderColor;
            }

            borders.DistanceFromLeft = paragraphFormat.BorderDistanceFromLeft;
            borders.DistanceFromTop = paragraphFormat.BorderDistanceFromTop;
            borders.DistanceFromRight = paragraphFormat.BorderDistanceFromRight;
            borders.DistanceFromBottom = paragraphFormat.BorderDistanceFromBottom;
            borders.Shadow = paragraphFormat.BorderHasShadow;

            shading.Texture = paragraphFormat.BorderShadingTexture;
            shading.BackgroundPatternColor = paragraphFormat.BorderShadingBackgroundColor;
            shading.ForegroundPatternColor = paragraphFormat.BorderShadingForegroundColor;
        }
    }
}
