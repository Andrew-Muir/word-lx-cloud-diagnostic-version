﻿using System.Collections.Generic;
using System.Linq;
using InfowareVSTO.Common.Language;
using InfowareVSTO.Windows.ParagraphNumbering.Model;

namespace InfowareVSTO.Windows.ParagraphNumbering
{
    internal class SpecialParagraphIndentationOptionManager
    {
        public static SpecialParagraphIndentationOptionManager Instance { get; }

        public List<SpecialParagraphIndentationOption> SpecialParagraphIndentationOptions { get; }

        static SpecialParagraphIndentationOptionManager()
        {
            Instance = new SpecialParagraphIndentationOptionManager();
        }

        private SpecialParagraphIndentationOptionManager()
        {
            SpecialParagraphIndentationOptions = new List<SpecialParagraphIndentationOption>()
            {
                new SpecialParagraphIndentationOption(LanguageManager.GetTranslation(LanguageConstants.None, "(none)"), SpecialParagraphIndentation.NONE),
                new SpecialParagraphIndentationOption(LanguageManager.GetTranslation(LanguageConstants.FirstLine,"First Line"), SpecialParagraphIndentation.FIRST_LINE),
                new SpecialParagraphIndentationOption(LanguageManager.GetTranslation(LanguageConstants.Hanging,"Hanging"), SpecialParagraphIndentation.HANGING)
            };
        }

        public SpecialParagraphIndentationOption GetSpecialParagraphIndentationInstance(SpecialParagraphIndentation specialParagraphIdentation)
        {
            return SpecialParagraphIndentationOptions.First(it => it.Value == specialParagraphIdentation);
        }
    }
}
