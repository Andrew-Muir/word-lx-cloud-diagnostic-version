﻿using System.Collections.Generic;
using System.Linq;
using InfowareVSTO.Common.Language;
using InfowareVSTO.Windows.ParagraphNumbering.Model;
using Microsoft.Office.Interop.Word;

namespace InfowareVSTO.Windows.ParagraphNumbering
{
    internal class LineSpacingOptionManager
    {
        public static LineSpacingOptionManager Instance { get; }

        public List<LineSpacingOption> LineSpacingOptions { get; }

        static LineSpacingOptionManager()
        {
            Instance = new LineSpacingOptionManager();
        }

        private LineSpacingOptionManager()
        {
            LineSpacingOptions = new List<LineSpacingOption>()
            {
                new LineSpacingOption(LanguageManager.GetTranslation(LanguageConstants.Single, "Single"), WdLineSpacing.wdLineSpaceSingle),
                new LineSpacingOption(LanguageManager.GetTranslation(LanguageConstants.OneP5Lines,"1.5 lines"), WdLineSpacing.wdLineSpace1pt5),
                new LineSpacingOption(LanguageManager.GetTranslation(LanguageConstants.Double, "Double"),WdLineSpacing.wdLineSpaceDouble),
                new LineSpacingOption(LanguageManager.GetTranslation(LanguageConstants.AtLeast, "At least"), WdLineSpacing.wdLineSpaceAtLeast),
                new LineSpacingOption(LanguageManager.GetTranslation(LanguageConstants.Exactly, "Exactly"), WdLineSpacing.wdLineSpaceExactly),
                new LineSpacingOption(LanguageManager.GetTranslation(LanguageConstants.Multiple, "Multiple"), WdLineSpacing.wdLineSpaceMultiple),
            };
        }

        public LineSpacingOption GetLineSpacingOptionInstance(WdLineSpacing? lineSpacing)
        {
            if (lineSpacing == null)
            {
                return null;
            }

            return LineSpacingOptions.First(it => it.Value == lineSpacing);
        }
    }
}
