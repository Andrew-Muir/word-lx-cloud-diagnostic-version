﻿using System;
using System.Windows.Input;

namespace InfowareVSTO.Windows.ParagraphNumbering.Util
{
    public class LoadingWindowWrapper : IDisposable
    {
        private bool isDisposed;

        private LoadingWindow loadingWindow;
        private bool changeCursor = true;

        public LoadingWindowWrapper(bool changeCursor = true)
        {
            loadingWindow = new LoadingWindow();
            loadingWindow.Show();
            this.changeCursor = changeCursor;

            if (changeCursor)
            {
                Mouse.OverrideCursor = Cursors.Wait;
            }
        }

        public void Dispose()
        {
            if (isDisposed)
            {
                return;
            }

            loadingWindow.Close();
            if (changeCursor)
            {
                Mouse.OverrideCursor = Cursors.Arrow;
            }

            isDisposed = true;
        }
    }
}
