﻿using System;
using InfowareVSTO.Common;
using Microsoft.Office.Interop.Word;

namespace InfowareVSTO.Windows.ParagraphNumbering.Util
{
    internal class TemporaryParagraph : IDisposable
    {
        private readonly int originalSelectionStart;

        private readonly int originalSelectionEnd;

        private readonly Application app;

        private bool movedToTemporaryParagraph;

        private bool isDisposed;

        public Paragraph Paragraph { get; private set; }

        public TemporaryParagraph(Selection currentSelection)
        {
            originalSelectionStart = currentSelection.Start;
            originalSelectionEnd = currentSelection.End;

            app = Globals.ThisAddIn.Application;

            GoToTemporaryParagraph();
        }

        private void GoToTemporaryParagraph()
        {
            if (movedToTemporaryParagraph)
            {
                // GoToToOriginalPosition() must be called before calling this again
                return;
            }

            app.ScreenUpdating = false;
            if (ThisAddIn.Instance.IsFunctionForLogging("ScreenUpdating"))
                Logger.Log("Disabled Screen Updating (Temporaryparagraph.cs)");

            int temporaryPragraphStart = app.ActiveDocument.StoryRanges[WdStoryType.wdMainTextStory].End;

            app.Selection.Start = temporaryPragraphStart;
            app.Selection.End = temporaryPragraphStart;
            app.Selection.InsertParagraphAfter();

            Paragraph = app.Selection.Paragraphs.Last;
            Paragraph.set_Style(Utils.GetLocalizedStyleNameFromBuiltInStyle(WdBuiltinStyle.wdStyleNormal));
            Paragraph.Range.Select();

            movedToTemporaryParagraph = true;
        }

        private void GoToToOriginalPosition()
        {
            if (!movedToTemporaryParagraph)
            {
                return;
            }

            Paragraph.Range.Delete();

            app.Selection.Start = originalSelectionStart;
            app.Selection.End = originalSelectionEnd;

            app.ScreenUpdating = true;
            if (ThisAddIn.Instance.IsFunctionForLogging("ScreenUpdating"))
                Logger.Log("Enabled Screen Updating (Temporaryparagraph.cs)");
        }

        public void Dispose()
        {
            if (isDisposed)
            {
                return;
            }

            GoToToOriginalPosition();
            isDisposed = true;
        }
    }
}
