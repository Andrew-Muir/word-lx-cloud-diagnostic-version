﻿using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using InfowareVSTO.Windows.ParagraphNumbering.Toolbars;
using InfowareVSTO.Windows.ParagraphNumbering.Util;
using System;
using System.Windows.Interop;

namespace InfowareVSTO.Windows.ParagraphNumbering
{
    internal class ParagraphNumberingToolbarManager
    {
        public static ParagraphNumberingToolbarManager Instance { get; }

        private ParagraphNumberingToolbarWindow toolbarWindow;

        public bool windowIsShown;

        private bool windowIsClosed = true;

        private bool explicitlyHidWindow = false;

        static ParagraphNumberingToolbarManager()
        {
            Instance = new ParagraphNumberingToolbarManager();
        }

        private ParagraphNumberingToolbarManager()
        {

        }

        public void ToggleVisibility(bool onlyVisibility = false)
        {
            if (toolbarWindow == null || windowIsClosed)
            {
                toolbarWindow = new ParagraphNumberingToolbarWindow
                {
                    Visibility = Visibility.Hidden
                };

                windowIsClosed = false;
                windowIsShown = true;
                toolbarWindow.Closed += OnWindowClosed;
            }

            if (toolbarWindow.Visibility == Visibility.Visible)
            {
                toolbarWindow.Visibility = Visibility.Hidden;
                windowIsShown = false;
            }
            else
            {
                toolbarWindow.Visibility = Visibility.Visible;

                if (!windowIsShown)
                {
                    windowIsShown = true;
                    toolbarWindow.Show();
                }

                if (onlyVisibility == false)
                    ListTemplateUtil.ExtractParagraphNumberingSchemesFromDocument(ThisAddIn.Instance.Application.ActiveDocument);
            }
        }

        public void EnsureWindowIsHidden()
        {
            if (toolbarWindow == null)
            {
                return;
            }

            explicitlyHidWindow = true;
            toolbarWindow.Visibility = Visibility.Hidden;
        }

        public void RestoreWindowIfNecessary()
        {
            if (toolbarWindow != null && explicitlyHidWindow && windowIsShown)
            {
                explicitlyHidWindow = false;
                toolbarWindow.Visibility = Visibility.Visible;
            }

            Dispatcher dispatcher = Dispatcher.CurrentDispatcher;

            System.Threading.Tasks.Task.Delay(100).ContinueWith(t => RestoreWindoDelayed(dispatcher));
            System.Threading.Tasks.Task.Delay(500).ContinueWith(t => RestoreWindoDelayed(dispatcher));
            System.Threading.Tasks.Task.Delay(1400).ContinueWith(t => RestoreWindoDelayed(dispatcher));
            System.Threading.Tasks.Task.Delay(3000).ContinueWith(t => RestoreWindoDelayed(dispatcher));
        }

        private void RestoreWindoDelayed(Dispatcher dispatcher)
        {
            if (toolbarWindow != null && explicitlyHidWindow && windowIsShown)
            {
                explicitlyHidWindow = false;

                dispatcher.Invoke(() =>
                {
                    toolbarWindow.Visibility = Visibility.Visible;
                });
            }
        }

        public void UpdateExtendedButtonState(bool showExtendedButtons)
        {
            if (toolbarWindow == null)
            {
                return;
            }

            toolbarWindow.ShowExtendedButtons = showExtendedButtons;
        }

        public void OnWordWindowSizeChanged(Microsoft.Office.Interop.Word.Document document, Microsoft.Office.Interop.Word.Window wordWindow)
        {
            if (toolbarWindow == null)
            {
                return;
            }

            toolbarWindow.UpdatePosition(wordWindow);
        }

        private void OnWindowClosed(object sender, System.EventArgs e)
        {
            windowIsClosed = true;
            windowIsShown = false;
        }

        public void SetSelectedValue(ParagraphNumberingScheme paragraphNumberingScheme)
        {
            toolbarWindow.CbParagraphNumberingSchemes.SelectedItem = paragraphNumberingScheme;
        }

        public void SetToolbarOwner(Microsoft.Office.Interop.Word.Window window)
        {
            if (toolbarWindow != null)
            {
                try
                {
                    WindowInteropHelper wih = new WindowInteropHelper(toolbarWindow);
                    wih.Owner = new IntPtr(window.Hwnd);
                }
                catch { }
            }
        }
    }
}
