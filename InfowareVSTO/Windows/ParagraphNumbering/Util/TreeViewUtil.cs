﻿using System.Windows.Controls;

namespace InfowareVSTO.Windows.ParagraphNumbering
{
    internal static class TreeViewUtil
    {
        public static TreeViewItem GetTreeViewItemFromChildObject(this ItemsControl control, object childObject)
        {
            var treeViewItem = control.ItemContainerGenerator.ContainerFromItem(childObject) as TreeViewItem;

            if (treeViewItem != null)
            {
                return treeViewItem;
            }

            foreach (object child in control.Items)
            {
                treeViewItem = control.ItemContainerGenerator.ContainerFromItem(child) as TreeViewItem;

                treeViewItem = GetTreeViewItemFromChildObject(treeViewItem, childObject);

                if (treeViewItem != null)
                {
                    return treeViewItem;
                }
            }

            return null;
        }
    }
}
