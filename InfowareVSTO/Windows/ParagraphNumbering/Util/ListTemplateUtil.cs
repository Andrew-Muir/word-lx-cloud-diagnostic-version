﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using InfowareVSTO.Common.Models;
using InfowareVSTO.Util;
using InfowareVSTO.Windows.ParagraphNumbering.Model;
using InfowareVSTO.Windows.ParagraphNumbering.Model.Enums;
using InfowareVSTO.Windows.ParagraphNumbering.Util;
using Microsoft.Office.Interop.Word;
using CommandBarControl = Microsoft.Office.Core.CommandBarControl;

namespace InfowareVSTO.Windows.ParagraphNumbering
{
    internal static class ListTemplateUtil
    {
        private const int WORD_RESTART_LIST_ITEM_COMMAND_ID = 5810;

        private const int WORD_CONTINUE_LIST_ITEM_COMMAND_ID = 6125;

        public const string CONTINUATION_PARAGRAPH_STYLE_NAME_INDICATOR = "No#";

        public const string PARAGRAPH_STYLE_NAME_LEVEL_INDICATOR = "L";

        public const string PARAGRAPH_STYLE_NAME_WORD_SEPARATOR = " ";

        public const string PARAGRAPH_STYLE_NAME_UNDERSCORE_WORD_SEPARATOR = "_";

        public const string CONTINUATION_PARAGRAPH_STYLE_EXTENSION_NAME_INDICATOR = "ext";

        private static readonly Regex levelIndicatorRegex = new Regex($@"(\d+)$");

        private static readonly Regex continuationParagraphIndicatorRegex = new Regex($@"{CONTINUATION_PARAGRAPH_STYLE_NAME_INDICATOR}{PARAGRAPH_STYLE_NAME_WORD_SEPARATOR}{PARAGRAPH_STYLE_NAME_LEVEL_INDICATOR}(\d+)$");

        private static readonly Regex listTemplateNameFromStyleNameRegex = new Regex($@"(.+?)(?:{PARAGRAPH_STYLE_NAME_WORD_SEPARATOR}{CONTINUATION_PARAGRAPH_STYLE_NAME_INDICATOR})?{PARAGRAPH_STYLE_NAME_WORD_SEPARATOR}{PARAGRAPH_STYLE_NAME_LEVEL_INDICATOR}(\d+)(?:,\w+)?$");

        public static string GetParagraphStyleName(ParagraphNumberingScheme numberingScheme, int levelIndex)
        {

            var paragraphStyleName = string.IsNullOrWhiteSpace(numberingScheme.RootStylePrefix) ? $"{numberingScheme.Name}{PARAGRAPH_STYLE_NAME_WORD_SEPARATOR}{PARAGRAPH_STYLE_NAME_LEVEL_INDICATOR}{levelIndex}" :
                $"{numberingScheme.RootStylePrefix}{levelIndex}";
            AppendStyleAliasIfNecessary(paragraphStyleName, numberingScheme);

            return paragraphStyleName;
        }

        public static string GetContinuationParagraphStyleName(ParagraphNumberingScheme numberingScheme, int levelIndex)
        {
            string continuationParagraphStyleName = string.Empty;

            if (numberingScheme != null)
            {

                if (!string.IsNullOrWhiteSpace(numberingScheme.RootStylePrefix))
                {
                    if (numberingScheme.RootStylePrefix.Last().ToString() == PARAGRAPH_STYLE_NAME_LEVEL_INDICATOR)
                    {
                        continuationParagraphStyleName = $"{numberingScheme.RootStylePrefix.Substring(0, numberingScheme.RootStylePrefix.Length - 1)}{CONTINUATION_PARAGRAPH_STYLE_NAME_INDICATOR}{PARAGRAPH_STYLE_NAME_LEVEL_INDICATOR}{levelIndex}";
                    }
                    else
                    {
                        continuationParagraphStyleName = $"{numberingScheme.RootStylePrefix.Substring(0, numberingScheme.RootStylePrefix.Length)}{CONTINUATION_PARAGRAPH_STYLE_NAME_INDICATOR}{levelIndex}";
                    }
                }
                else
                {
                    continuationParagraphStyleName = $"{numberingScheme.Name}{PARAGRAPH_STYLE_NAME_WORD_SEPARATOR}{CONTINUATION_PARAGRAPH_STYLE_NAME_INDICATOR}{PARAGRAPH_STYLE_NAME_LEVEL_INDICATOR}{levelIndex}";
                }
            }

            return continuationParagraphStyleName;
        }

        public static string EnsureFullParagraphStyleName(string styleName, ParagraphNumberingScheme numberingScheme)
        {
            return AppendStyleAliasIfNecessary(styleName, numberingScheme);
        }

        public static string AppendStyleAliasIfNecessary(string styleName, ParagraphNumberingScheme numberingScheme)
        {
            if (string.IsNullOrWhiteSpace(numberingScheme.StyleAliasPrefix))
            {
                return styleName;
            }

            string styleNameWithoutAlias = GetStyleNameWithoutAlias(styleName);

            return $"{styleNameWithoutAlias},{numberingScheme.StyleAliasPrefix}{GetLevelNumberFromStyleName(styleNameWithoutAlias)}";
        }

        public static string GetStyleNamePrefix(ParagraphNumberingScheme numberingScheme)
        {
            return string.IsNullOrWhiteSpace(numberingScheme.RootStylePrefix) ? $"{numberingScheme.Name}{PARAGRAPH_STYLE_NAME_WORD_SEPARATOR}{PARAGRAPH_STYLE_NAME_LEVEL_INDICATOR}" :
                numberingScheme.RootStylePrefix;
        }

        public static string GetListTemplateName(ParagraphNumberingScheme numberingScheme)
        {
            return numberingScheme.Name;
        }

        public static string GetParagraphNumberingSchemeName(ListTemplate listTemplate)
        {
            return listTemplate.Name;
        }

        public static Style GetParagraphStyleByName(string styleName, Document document = null)
        {
            try
            {
                if (document != null)
                {
                    return document.Styles[styleName];
                }
                else
                {
                    return DocumentUtil.GetActiveDocument().Styles[styleName];
                }
            }
            catch (Exception ex)
            {
                return null;
            }

            //foreach (Style style in DocumentUtil.GetActiveDocument().Styles)
            //{
            //    if (style.NameLocal == styleName)
            //    {
            //        return style;
            //    }
            //}

            //return null;
        }

        public static ListTemplate GetListTemplateByName(string listTemplateName)
        {
            //try
            //{
            //    return DocumentUtil.GetActiveDocument().ListTemplates[listTemplateName];
            //}
            //catch (Exception)
            //{
            //    return null;
            //}

            foreach (ListTemplate template in DocumentUtil.GetActiveDocument().ListTemplates)
            {
                if (template.Name == listTemplateName)
                {
                    return template;
                }
            }

            foreach (ListTemplate template in DocumentUtil.GetActiveDocument().ListTemplates)
            {
                if (GetListTemplateNameFormFirstLevel(template) == listTemplateName)
                {
                    return template;
                }
            }

            return null;
        }

        public static string GetListTemplateNameFormFirstLevel(ListTemplate listTemplate)
        {
            string name = GetListTemplateNameFormFirstLevelBaseNaem(listTemplate);

            if (name == listTemplate.Name)
            {
                return name;
            }

            if (listTemplate.Name.StartsWith("_"))
            {
                name = "_" + name;
            }

            if (listTemplate.Name.StartsWith(name) && listTemplate.Name.Length > name.Length && listTemplate.Name[name.Length] == '_')
            {
                name = name + "_";
            }

            return name;

        }

        private static string GetListTemplateNameFormFirstLevelBaseNaem(ListTemplate listTemplate)
        {
            try
            {
                foreach (ListLevel level in listTemplate.ListLevels)
                {
                    List<string> list = new List<string>(level.LinkedStyle.Split(new char[] { ' ', '_' }));

                    if (list.Count == 1 || level.LinkedStyle.Length < 2)
                    {
                        string name = level.LinkedStyle;
                        if (name.Length > 2 && name[name.Length - 2] == 'L')
                        {
                            return name.Substring(0, name.Length - 2).Trim();
                        }
                        else if (name.Length > 1)
                        {
                            return name.Substring(0, name.Length - 1).Trim();
                        }
                        else
                        {
                            return RemoveExtraNumberListTemplateName(listTemplate.Name);
                        }
                    }

                    string number = list.Where(x => Regex.IsMatch(x, @"\d")).LastOrDefault();
                    if (number != null)
                    {
                        int lastIndex = list.LastIndexOf(number);
                        if (lastIndex > 0)
                        {
                            list.RemoveAt(lastIndex);
                        }
                    }

                    return string.Join(" ", list);
                }
            }
            catch
            {
            }

            return RemoveExtraNumberListTemplateName(listTemplate.Name);
        }

        public static string RemoveExtraNumberListTemplateName(string name)
        {
            string regexStr = @"^.*-\d{9}-\w$";
            Regex regex = new Regex(regexStr);

            if (regex.IsMatch(name))
            {
                return name.Substring(0, name.Length - 12);
            }

            return name;
        }

        public static bool IsGeneratedContinuationParagraphStyle(string styleName)
        {
            //return styleName.Contains(UN_NUMBERED_PARAGRAPH_INDICATOR);
            return continuationParagraphIndicatorRegex.IsMatch(styleName);
        }

        public static bool IsGeneratedContinuationParagraphStyle(Style style)
        {
            return IsGeneratedContinuationParagraphStyle(style.NameLocal);
        }

        public static void RestartNumberingInSelection()
        {
            CommandBarControl restartListNumberingControl = DocumentUtil.GetCommandBarControlWithId(WORD_RESTART_LIST_ITEM_COMMAND_ID);

            if (restartListNumberingControl == null)
            {
                // Among others, this is null when the list level is already at 1
                return;
            }

            restartListNumberingControl.Execute();
        }

        internal static void ContinueNumberingInSelection()
        {
            var firstParagraphName = ThisAddIn.Instance.Application?.ActiveWindow?.Selection?.Paragraphs?.First?.Range?.ListFormat?.ListTemplate?.Name;

            CommandBarControl continueListNumberingControl = DocumentUtil.GetCommandBarControlWithId(WORD_CONTINUE_LIST_ITEM_COMMAND_ID);

            if (continueListNumberingControl == null)
            {
                // Among others, this is null when the cursor is outside of a list
                return;
            }

            try
            {
                continueListNumberingControl.Execute();

                if (ThisAddIn.Instance.Application?.ActiveWindow?.Selection?.Paragraphs?.First?.Range?.ListFormat?.ListTemplate?.Name != null && string.IsNullOrEmpty(ThisAddIn.Instance.Application.ActiveWindow.Selection.Paragraphs.First.Range.ListFormat.ListTemplate.Name) && !string.IsNullOrEmpty(firstParagraphName))
                {
                    ThisAddIn.Instance.Application.ActiveWindow.Selection.Paragraphs.First.Range.ListFormat.ListTemplate.Name = firstParagraphName;
                }
            }
            catch (COMException)
            {
                // Sometimes the object is not null, even when outside of a list. In that case, an exception is thrown
            }
        }

        internal static string GetStyleNameWithoutAlias(string styleNameWithAlias)
        {
            // It seems that Word styles can be aliased. Aliases come after a comma or a semicolon
            // http://editorium.com/archive/style-aliases/

            int commaIndex = styleNameWithAlias.IndexOf(",");

            if (commaIndex == -1)
            {
                int semicolonIndex = styleNameWithAlias.IndexOf(";");

                return semicolonIndex == -1 ? styleNameWithAlias : styleNameWithAlias.Substring(0, semicolonIndex);
            }
            else
            {
                return styleNameWithAlias.Substring(0, commaIndex);
            }
        }

        private static bool DoesStyleNameContainAlias(string styleName)
        {
            return styleName != GetStyleNameWithoutAlias(styleName);
        }

        internal static int GetLevelNumberFromStyleName(string styleName)
        {
            Match match = levelIndicatorRegex.Match(GetStyleNameWithoutAlias(styleName));

            if (match.Success)
            {
                return int.Parse(match.Groups[1].Value);
            }

            return -1;
        }

        internal static string ReplaceStyleNameLevel(string styleName, int newLevelNumber)
        {
            if (newLevelNumber < 1 || newLevelNumber > 9)
            {
                throw new ArgumentOutOfRangeException(nameof(newLevelNumber));
            }

            int oldLevelNumber = GetLevelNumberFromStyleName(styleName);

            if (oldLevelNumber == -1)
            {
                // The style name doesn't seem to follow the naming convention used by GetParagraphStyleName() and GetContinuationParagraphStyleName()
                return null;
            }

            var levelIndicatorRegexPattern = $@"(?<={PARAGRAPH_STYLE_NAME_LEVEL_INDICATOR})(\d+)(,\w+([1-9]))?$";

            return Regex.Replace(styleName, levelIndicatorRegexPattern, newLevelNumber.ToString());
        }

        internal static string GetNumberedStyleNameFromContinuationStyleName(string continuationStyleName, ParagraphNumberingScheme numberingScheme)
        {
            string numberedStyleName = continuationStyleName.Replace($"{CONTINUATION_PARAGRAPH_STYLE_NAME_INDICATOR}", "");
            return AppendStyleAliasIfNecessary(numberedStyleName, numberingScheme);
        }

        internal static string GetContinuationStyleNameFromNumberedStyleName(string numberedStyleName)
        {
            int index = numberedStyleName.IndexOf($"{PARAGRAPH_STYLE_NAME_WORD_SEPARATOR}{PARAGRAPH_STYLE_NAME_LEVEL_INDICATOR}");

            if (index == -1)
            {
                // The style name doesn't seem to follow the naming convention used by GetContinuationParagraphStyleName()
                return null;
            }

            numberedStyleName = GetStyleNameWithoutAlias(numberedStyleName);

            return numberedStyleName.Insert(index, $"{CONTINUATION_PARAGRAPH_STYLE_NAME_INDICATOR}");
        }

        internal static bool CheckStyleExists(string styleName)
        {
            return GetParagraphStyleByName(styleName) != null;
        }

        public static bool IsListTemplateUsedInSelection()
        {
            return IsListTemplateUsedInRange(DocumentUtil.GetCollapsedSelectionRange());
        }

        public static bool IsListTemplateUsedInRange(Range range)
        {
            return GetNameOfListTemplateUsedInRange(range) != null;
        }

        public static string GetNameOfListTemplateUsedInSelection()
        {
            return GetNameOfListTemplateUsedInRange(DocumentUtil.GetCollapsedSelectionRange());
        }

        public static string GetNameOfListTemplateUsedInRange(Range range)
        {
            /**
             * app.Selection.Range.ListFormat.ListTemplate will be null if the selection contains even a char from a
             * paragraph without a list template
             */

            // Example of when ListTemplate is null: the user changes the style of a paragraph in an extension generated list
            if (range.ListFormat.ListTemplate != null)
            {
                // Lists using Word supplied list template have an empty ListTemplate.Name property
                if (range.ListFormat.ListTemplate.Name == string.Empty)
                {
                    Marshal.ReleaseComObject(range.ListFormat.ListTemplate);
                    Marshal.ReleaseComObject(range.ListFormat);
                    return null;
                }

                Marshal.ReleaseComObject(range.ListFormat.ListTemplate);
                Marshal.ReleaseComObject(range.ListFormat);
                return GetListTemplateNameFormFirstLevel(range.ListFormat.ListTemplate);
            }

            Marshal.ReleaseComObject(range.ListFormat);
            if (range.Paragraphs.Count == 0)
            {
                return null;
            }

            try
            {
                var selectionStyle = (Style)range.Paragraphs[1].get_Style();

                Match match = listTemplateNameFromStyleNameRegex.Match(selectionStyle.NameLocal);

                if (match.Success)
                {
                    if (!IsGeneratedContinuationParagraphStyle(selectionStyle.NameLocal) && range.ListFormat.ListType == WdListType.wdListNoNumbering)
                    {
                        // The selection somehow got stuck with a numbering style, but it doesn't belong to a list
                        Marshal.ReleaseComObject(selectionStyle);
                        return null;
                    }
                    Marshal.ReleaseComObject(selectionStyle);
                    return match.Groups[1].Value;
                }
                Marshal.ReleaseComObject(selectionStyle);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }

            return null;
        }

        public static List<List> GetAllListsUsingListTemplate(string listTemplateName)
        {
            var lists = new List<List>();

            if (!string.IsNullOrEmpty(listTemplateName))
            {
                foreach (List list in GetAllListsInActiveDocument())
                {
                    if (list?.Range?.Paragraphs.Count > 0 && list?.Range?.Paragraphs[1]?.Range?.ListFormat?.ListTemplate?.Name == listTemplateName)
                    {
                        lists.Add(list);
                    }
                }
            }

            return lists;
        }

        public static List GetFirstListUsingListTemplate(string listTemplateName, Document fromDocument = null)
        {
            Lists lists = GetAllListsInActiveDocument(fromDocument);
            foreach (List list in lists)
            {
                if (GetNameOfListTemplateUsedInRange(list.Range) == listTemplateName)
                {
                    Marshal.ReleaseComObject(lists);
                    return list;
                }
                Marshal.ReleaseComObject(list);
            }

            return null;
        }

        public static Lists GetAllListsInActiveDocument(Document document = null)
        {
            if (document == null)
            {
                document = Globals.ThisAddIn.Application.ActiveDocument;
            }

            return document.Lists;
        }

        public static HashSet<string> GetAllListTemplateNames()
        {
            var listTemplateNames = new HashSet<string>();

            foreach (ParagraphNumberingScheme paragraphNumberingScheme in ParagraphNumberingSchemeManager.Instance.GetAllParagraphNumberingSchemes())
            {
                string listTemplateName = GetListTemplateName(paragraphNumberingScheme);
                listTemplateNames.Add(listTemplateName);
            }

            return listTemplateNames;
        }

        public static List<ListTemplate> GetAllCustomListTemplatesInDocument(Document document = null)
        {
            var listTemplates = new List<ListTemplate>();
            HashSet<string> listTemplateNames = GetAllListTemplateNames();

            try
            {
                foreach (List list in GetAllListsInActiveDocument(document))
                {
                    ListTemplate listTemplate = list.Range.ListFormat.ListTemplate;

                    if (listTemplate == null)
                    {
                        /**
                         * The ListTemplate might be null if the user switches to a continuation style on any paragraph (No#)
                         * 
                         * When that happens, we attempt to get the ListTemplate from the first paragraph that has one set
                         */
                        foreach (Paragraph listParagraph in list.Range.Paragraphs)
                        {
                            listTemplate = listParagraph.Range.ListFormat.ListTemplate;

                            if (listTemplate != null)
                            {
                                break;
                            }
                        }
                    }

                    if (listTemplate == null)
                    {
                        // Give up
                        continue;
                    }

                    if (string.IsNullOrWhiteSpace(listTemplate.Name))
                    {
                        var generatedTemplateName = GetRootStyleNamePrefix(listTemplate.ListLevels[1]?.LinkedStyle);
                        if (string.IsNullOrEmpty(generatedTemplateName))
                        {
                            var random = new Random();
                            generatedTemplateName = "UnnamedList" + Math.Abs(random.Next() % 100000);
                        }

                        listTemplate.Name = generatedTemplateName;
                    }

                    if (listTemplates.FirstOrDefault(x => x.Name == listTemplate.Name) == null)
                    {
                        listTemplates.Add(listTemplate);
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }

            return listTemplates;
        }

        public static void DeleteListTemplateAndLinkedStyles(ParagraphNumberingScheme paragraphNumberingScheme)
        {
            string replacementStyleName = Utils.GetLocalizedStyleNameFromBuiltInStyle(WdBuiltinStyle.wdStyleNormal);

            ListTemplate listTemplate = GetListTemplateByName(GetListTemplateName(paragraphNumberingScheme));

            if (listTemplate == null)
            {
                return;
            }

            foreach (ListLevel level in listTemplate.ListLevels)
            {
                string linkedStyleName = level.LinkedStyle;
                linkedStyleName = EnsureFullParagraphStyleName(linkedStyleName, paragraphNumberingScheme);

                DocumentUtil.ReplaceStyle(linkedStyleName, replacementStyleName);
                GetParagraphStyleByName(linkedStyleName).Delete();

                string continuationStyleName = GetContinuationParagraphStyleName(paragraphNumberingScheme, level.Index);
                DocumentUtil.ReplaceStyle(continuationStyleName, replacementStyleName);
                GetParagraphStyleByName(continuationStyleName)?.Delete();
            }

            // List templates cannot be deleted, so we rename it to avoid future naming collisions
            listTemplate.Name = Guid.NewGuid().ToString();
        }

        public static void AddNewLinePrefix(List list, int listLevelIndex)
        {
            foreach (Paragraph paragraph in list.Range.Paragraphs)
            {
                if (paragraph.Range.ListFormat.ListLevelNumber == listLevelIndex)
                {
                    AddNewLineToFirstParagraphInRange(paragraph);
                }
            }
        }

        public static void AddNewLinePrefixInSelection()
        {
            foreach (Paragraph paragraph in DocumentUtil.GetSelection().Paragraphs)
            {
                AddNewLineToFirstParagraphInRange(paragraph);
            }
        }

        private static void AddNewLineToFirstParagraphInRange(Paragraph paragraph)
        {
            if (IsParagrapUsingNewLine(paragraph))
            {
                return;
            }

            Range paragraphRange = paragraph.Range.Duplicate;
            paragraphRange.Collapse();

            paragraphRange.Text = MsWordConstants.NEW_LINE_TRAILING_CHARACTER;
        }

        private static void AddNewLineToFirstParagraphInRangeWithoutVerification(Paragraph paragraph)
        {
            Range paragraphRange = paragraph.Range.Duplicate;
            paragraphRange.Collapse();

            paragraphRange.Text = MsWordConstants.NEW_LINE_TRAILING_CHARACTER;
        }

        public static void RemoveNewLinePrefix(List list)
        {
            RemoveNewLinePrefix(list.Range);
        }

        public static void RemoveNewLinePrefixInSelection()
        {
            foreach (Paragraph paragraph in DocumentUtil.GetSelection().Paragraphs)
            {
                RemoveNewLinePrefix(paragraph);
            }
        }

        public static void RemoveNewLinePrefix(Range range)
        {
            foreach (Paragraph paragraph in range.Paragraphs)
            {
                RemoveNewLinePrefix(paragraph);
            }
        }

        private static void RemoveNewLinePrefix(Paragraph paragraph)
        {
            Range paragraphRange = paragraph.Range.Duplicate;
            paragraphRange.Collapse();

            paragraphRange.MoveEnd(WdUnits.wdCharacter, 1);

            if (paragraphRange.Text == MsWordConstants.NEW_LINE_TRAILING_CHARACTER)
            {
                paragraphRange.Delete();
            }
        }

        public static bool IsListUsingNewLine(List list)
        {
            if (list.Range.Paragraphs.Count == 0)
            {
                return false;
            }

            return IsParagrapUsingNewLine(list.Range.Paragraphs.First);
        }

        public static bool IsListUsingNewLine(List list, int listLevel)
        {
            if (list.Range.Paragraphs.Count == 0)
            {
                return false;
            }

            foreach (Paragraph paragraph in list.Range.Paragraphs)
            {
                if (paragraph.Range.ListFormat.ListLevelNumber != listLevel)
                {
                    continue;
                }

                if (IsParagrapUsingNewLine(paragraph))
                {
                    return true;
                }
            }

            return false;
        }

        public static bool IsParagrapUsingNewLine(Paragraph paragraph)
        {
            Range paragraphRange = paragraph.Range.Duplicate;
            paragraphRange.Collapse();

            paragraphRange.MoveEnd(WdUnits.wdCharacter, 1);

            return paragraphRange.Text == MsWordConstants.NEW_LINE_TRAILING_CHARACTER;
        }

        public static void ExtractParagraphNumberingSchemesFromDocument(Document document)
        {
            ParagraphNumberingSchemeManager.Instance.ActiveDocumentParagraphNumberingSchemes?.Clear();

            if (document != null && ParagraphNumberingSchemeManager.Instance?.ActiveDocumentParagraphNumberingSchemes != null)
            {
                var documentListTemplates = GetAllCustomListTemplatesInDocument(document);

                foreach (ListTemplate listTemplate in (document ?? ThisAddIn.Instance.Application.ActiveDocument).ListTemplates)
                {
                    if (!documentListTemplates.Select(x => x.Name).Contains(listTemplate.Name) && !string.IsNullOrEmpty(listTemplate.ListLevels[1]?.LinkedStyle))
                    {
                        documentListTemplates.Add(listTemplate);
                    }
                }

                if (documentListTemplates != null && documentListTemplates.Count > 0)
                {
                    foreach (ListTemplate listTemplate in documentListTemplates)
                    {
                        var schemeName = GetListTemplateNameFormFirstLevel(listTemplate);
                        if (!string.IsNullOrEmpty(schemeName))
                        {
                            string displayName = ParagraphNumberingSchemeManager.Instance.GetParagraphNumberingFromFirmListStructure(schemeName);

                            var paragraphNumberingScheme = ParagraphNumberingScheme.CreateNew(schemeName, displayName ?? schemeName);

                            for (int i = 8; i >= listTemplate.ListLevels.Count; i--)
                            {
                                paragraphNumberingScheme.ParagraphNumberingLevels.RemoveAt(i);
                            }

                            for (int i = listTemplate.ListLevels.Count; i > 1; i--)
                            {
                                if (string.IsNullOrWhiteSpace(listTemplate.ListLevels[i].LinkedStyle))
                                {
                                    paragraphNumberingScheme.ParagraphNumberingLevels.RemoveAt(i - 1);
                                }
                                else
                                {
                                    break;
                                }
                            }

                            paragraphNumberingScheme.ToSync = listTemplate;

                            ParagraphNumberingSchemeManager.Instance.ActiveDocumentParagraphNumberingSchemes.Add(paragraphNumberingScheme);
                        }
                    }
                }
            }
        }

        public static TrailingCharacter GetNewLineTrailingCharacterFromDocumentListParagraph(TrailingCharacter trailingCharacter, List<List> lists, string listTemplateName, ParagraphNumberingLevel selectedParagraphNumberingLevel)
        {
            if (lists.Count > 0 && !string.IsNullOrEmpty(listTemplateName) && selectedParagraphNumberingLevel?.FirstNumberFormatRepresentation != null)
            {
                foreach (var list in lists)
                {
                    if (list?.Range?.Paragraphs != null && list.Range.Paragraphs.Count > 0 && list.Range.Paragraphs[1].Range.ListFormat.ListTemplate.Name == listTemplateName)
                    {
                        foreach (Paragraph paragraph in list.Range.Paragraphs)
                        {
                            if (paragraph.Range.ListFormat.ListLevelNumber == selectedParagraphNumberingLevel.Index && !string.IsNullOrEmpty(paragraph.Range.ListFormat.ListString) && IsParagrapUsingNewLine(paragraph))
                            {
                                trailingCharacter = TrailingCharacter.NEW_LINE;
                                break;
                            }
                        }
                    }
                }
            }

            return trailingCharacter;
        }

        public static void AddRemoveNewLineCharacterIfExists(TrailingCharacter trailingCharacter, List<List> lists, string listTemplateName, ParagraphNumberingLevel selectedParagraphNumberingLevel)
        {
            if (lists.Count > 0)
            {
                foreach (List list in lists)
                {
                    if (list?.Range?.Paragraphs != null && list.Range.Paragraphs.Count > 0 && list.Range.Paragraphs[1].Range.ListFormat.ListTemplate.Name == listTemplateName)
                    {
                        foreach (Paragraph paragraph in list.Range.Paragraphs)
                        {
                            if (paragraph.Range.ListFormat.ListLevelNumber == selectedParagraphNumberingLevel.Index && !string.IsNullOrEmpty(paragraph.Range.ListFormat.ListString))
                            {
                                if (IsParagrapUsingNewLine(paragraph))
                                {
                                    RemoveNewLinePrefix(paragraph);
                                }

                                if (trailingCharacter == TrailingCharacter.NEW_LINE && !IsParagrapUsingNewLine(paragraph))
                                {
                                    AddNewLineToFirstParagraphInRangeWithoutVerification(paragraph);
                                }
                            }
                        }
                    }
                }
            }
        }

        public static bool CheckIfListParagraphHasNewLine(List<List> lists, string listTemplateName, int listLevelNumber)
        {
            if (lists.Count > 0 && !string.IsNullOrEmpty(listTemplateName))
            {
                foreach (var list in lists)
                {
                    if (list?.Range?.Paragraphs != null && list.Range.Paragraphs.Count > 0 && list.Range.Paragraphs[1].Range.ListFormat.ListTemplate.Name == listTemplateName)
                    {
                        foreach (Paragraph paragraph in list.Range.Paragraphs)
                        {
                            if (paragraph.Range.ListFormat.ListLevelNumber == listLevelNumber && !string.IsNullOrEmpty(paragraph.Range.ListFormat.ListString) && IsParagrapUsingNewLine(paragraph))
                            {
                                return true;
                            }
                        }
                    }
                }
            }

            return false;
        }

        public static ParagraphNumberingLevel GetFirstLevelOfParagraphScheme(ParagraphNumberingScheme paragraphNumberingScheme)
        {
            ParagraphNumberingLevel paragraphNumberingLevel = null;

            if (paragraphNumberingScheme != null)
            {
                paragraphNumberingLevel = paragraphNumberingScheme?.ParagraphNumberingLevels.FirstOrDefault(x => x.ParagraphFormat.IsLeftAligned == true || x.ParagraphFormat.IsJustifiedAligned == true);
            }

            return paragraphNumberingLevel;
        }

        public static SpecialParagraphIndentationOption GetSpecialParagraphIndentationOptionFromSpecialParagraphIndentation(SpecialParagraphIndentation specialParagraphIndentation)
        {
            var specialParagraphIndentationOption = new SpecialParagraphIndentationOption("(none)", SpecialParagraphIndentation.NONE);

            if (SpecialParagraphIndentationOptionManager.Instance?.SpecialParagraphIndentationOptions != null)
            {
                specialParagraphIndentationOption = SpecialParagraphIndentationOptionManager.Instance.SpecialParagraphIndentationOptions.FirstOrDefault(x => x.Value == specialParagraphIndentation);
            }

            return specialParagraphIndentationOption;
        }

        public static string GetRootStyleNamePrefix(string linkedStyleName)
        {
            var rootStyleName = string.Empty;

            if (!string.IsNullOrEmpty(linkedStyleName))
            {
                if (Int32.TryParse(linkedStyleName[linkedStyleName.Length - 1].ToString(), out int linkedStyleLevelIndex))
                {
                    rootStyleName = linkedStyleName.Remove(linkedStyleName.Length - 1);
                }
                else
                {
                    if (Regex.IsMatch(linkedStyleName, @"( ?L)? ?\d"))
                    {
                        var matches = Regex.Matches(linkedStyleName, @"( ?L)? ?\d");

                        var match = matches[matches.Count - 1];

                        linkedStyleName = linkedStyleName.Substring(0, match.Index) + linkedStyleName.Substring(match.Index + match.Length);
                    }

                    rootStyleName = linkedStyleName + " L";
                }
            }

            return rootStyleName;
        }

        public static ListTemplate GetListTemplateByName(ParagraphNumberingScheme paragraphNumberingScheme, string listTemplateName)
        {
            if (ParagraphNumberingSchemeManager.Instance.ActiveDocumentParagraphNumberingSchemes.Any(x => (x as ParagraphNumberingScheme).Name == paragraphNumberingScheme.Name) && ThisAddIn.Instance.Application.ActiveDocument?.ListTemplates != null)
            {
                foreach (ListTemplate template in ThisAddIn.Instance.Application.ActiveDocument.ListTemplates)
                {
                    if (template.Name == listTemplateName)
                    {
                        return template;
                    }
                }
            }
            else if (CheckIfNumberingSchemeIsInFirmListDocument(ParagraphNumberingSchemeManager.Instance.FirmDocumentParagraphNumberingSchemes, paragraphNumberingScheme.Name) && ThisAddIn.FirmListDocument?.ListTemplates != null)
            {
                foreach (ListTemplate template in ThisAddIn.FirmListDocument.ListTemplates)
                {
                    if (GetListTemplateNameFormFirstLevel(template) == listTemplateName)
                    {
                        return template;
                    }
                }
            }

            return null;
        }

        public static string ExtractStyleToFollowNameFromNextParagraphStyleName(string nextParagraphStyleName)
        {
            var styleToFollowName = string.Empty;

            if (string.IsNullOrEmpty(nextParagraphStyleName))
            {
                styleToFollowName = nextParagraphStyleName;
            }

            return styleToFollowName;
        }

        public static ListTemplate GetListTemplateByExactName(string listTemplateName)
        {
            foreach (ListTemplate template in ThisAddIn.Instance.Application.ActiveDocument.ListTemplates)
            {
                if (template.Name == listTemplateName)
                {
                    return template;
                }
            }

            return null;
        }

        public static string GetStyleAliasPrefix(string linkedStyleName)
        {
            var styleAliasPrefix = string.Empty;

            if (!string.IsNullOrEmpty(linkedStyleName))
            {
                var linkedStyleNameArr = linkedStyleName.Split(',');

                if (linkedStyleNameArr.Length > 1 && !string.IsNullOrEmpty(linkedStyleNameArr[1]))
                {
                    var linkedStyleNamePrefix = linkedStyleNameArr[1].Trim();

                    if (!string.IsNullOrEmpty(linkedStyleNamePrefix))
                    {
                        if (Int32.TryParse(linkedStyleNamePrefix[linkedStyleNamePrefix.Length - 1].ToString(), out int linkedStyleNamePrefixLevelIndex))
                        {
                            styleAliasPrefix = linkedStyleNamePrefix.Remove(linkedStyleNamePrefix.Length - 1);
                        }
                        else
                        {
                            styleAliasPrefix = linkedStyleName;
                        }
                    }
                }
            }

            return styleAliasPrefix;
        }

        public static string ExtractFirstLevelLinkedStyleNameFromParagraphNumberingScheme(ParagraphNumberingScheme paragraphNumberingScheme)
        {
            var linkedStyleName = string.Empty;

            if (paragraphNumberingScheme != null)
            {
                var listTemplate = GetListTemplateByName(paragraphNumberingScheme, GetListTemplateName(paragraphNumberingScheme));

                if (listTemplate?.ListLevels != null && listTemplate.ListLevels.Count > 0 && listTemplate.ListLevels[1].LinkedStyle != null)
                {
                    linkedStyleName = listTemplate.ListLevels[1].LinkedStyle.Trim();
                }
            }

            return linkedStyleName;
        }

        public static string ExtractFirstLevelStyleToFollowNameFromParagraphNumberingScheme(ParagraphNumberingScheme paragraphNumberingScheme)
        {
            var styleToFollowName = string.Empty;

            if (paragraphNumberingScheme != null && paragraphNumberingScheme.ParagraphNumberingLevels.Count > 0 && paragraphNumberingScheme?.ParagraphNumberingLevels[0]?.ParagraphFormat?.StyleToFollow != null)
            {
                styleToFollowName = paragraphNumberingScheme?.ParagraphNumberingLevels[0].ParagraphFormat.StyleToFollow;
            }

            return styleToFollowName;
        }

        public static int GetStyleToFollowIndex(ParagraphNumberingScheme paragraphNumberingScheme, ParagraphNumberingLevel paragraphNumberingLevel, List<string> followableStyleNames)
        {
            if (paragraphNumberingScheme != null && followableStyleNames != null && followableStyleNames.Count > 0 && !string.IsNullOrEmpty(paragraphNumberingLevel.ParagraphFormat.StyleToFollow))
            {
                var followableStyleNameToCompare = paragraphNumberingLevel.ParagraphFormat.StyleToFollow;

                if (!string.IsNullOrEmpty(paragraphNumberingScheme.StyleAliasPrefix) && paragraphNumberingLevel.ParagraphFormat.StyleToFollow != Utils.GetLocalizedStyleNameFromBuiltInStyle(WdBuiltinStyle.wdStyleNormal) && paragraphNumberingLevel.ParagraphFormat.StyleToFollow != Utils.GetLocalizedStyleNameFromBuiltInStyle(WdBuiltinStyle.wdStyleBodyText))
                {
                    var styleAliasPrefixArr = paragraphNumberingLevel.ParagraphFormat.StyleToFollow.Split(',');

                    if (styleAliasPrefixArr.Length > 0 && !string.IsNullOrEmpty(styleAliasPrefixArr[0]))
                    {
                        followableStyleNameToCompare = styleAliasPrefixArr[0].Trim();
                    }
                }

                return followableStyleNames.IndexOf(followableStyleNameToCompare);
            }

            return -1;
        }

        public static ListTemplate GetListTemplateByNameFromDocuments(string listTemplateName)
        {
            if (!string.IsNullOrEmpty(listTemplateName))
            {
                var documentListTemplates = DocumentUtil.GetActiveDocument()?.ListTemplates;
                if (documentListTemplates != null)
                {
                    foreach (ListTemplate template in documentListTemplates)
                    {
                        if (template.Name == listTemplateName)
                        {
                            return template;
                        }
                    }
                }

                var firmListDocumentListTemplates = ThisAddIn.FirmListDocument?.ListTemplates;
                if (firmListDocumentListTemplates != null)
                {
                    foreach (ListTemplate template in firmListDocumentListTemplates)
                    {
                        if (template.Name == listTemplateName)
                        {
                            return template;
                        }
                    }
                }
            }

            return null;
        }

        public static ListTemplate GetListTemplateByNameFromFirmListDocument(string listTemplateName)
        {
            if (!string.IsNullOrEmpty(listTemplateName))
            {
                var firmListDocumentListTemplates = ThisAddIn.FirmListDocument?.ListTemplates;
                if (firmListDocumentListTemplates != null)
                {
                    foreach (ListTemplate template in firmListDocumentListTemplates)
                    {
                        if (GetListTemplateNameFormFirstLevel(template) == listTemplateName)
                        {
                            return template;
                        }
                    }
                }
            }

            return null;
        }

        public static bool CheckIfNumberingSchemeIsInFirmListDocument(ObservableCollection<object> firmListDocumentNumberingSchemes, string selectedNumberingSchemeName)
        {
            bool result = false;
            if (firmListDocumentNumberingSchemes != null && firmListDocumentNumberingSchemes.Count > 0)
            {
                foreach (var firmListDocumentNumberingScheme in firmListDocumentNumberingSchemes)
                {
                    if (firmListDocumentNumberingScheme is ParagraphNumberingSchemeCategory)
                    {
                        result = result || CheckIfNumberingSchemeIsInFirmListDocumentInParagraphNumberingCategory(firmListDocumentNumberingScheme as ParagraphNumberingSchemeCategory, selectedNumberingSchemeName);
                    }
                    else if (firmListDocumentNumberingScheme is ParagraphNumberingScheme)
                    {
                        result = result || CheckIfNumberingSchemeIsInFirmListDocumentInParagraphNumberingScheme(firmListDocumentNumberingScheme as ParagraphNumberingScheme, selectedNumberingSchemeName);
                    }
                }
            }

            return result;
        }

        public static bool CheckIfNumberingSchemeIsInFirmListDocumentInParagraphNumberingScheme(ParagraphNumberingScheme scheme, string selectedNumberingSchemeName)
        {
            if (scheme.Name == selectedNumberingSchemeName)
            {
                return true;
            }

            return false;
        }
        public static bool CheckIfNumberingSchemeIsInFirmListDocumentInParagraphNumberingCategory(ParagraphNumberingSchemeCategory category, string selectedNumberingSchemeName)
        {
            bool result = false;
            foreach (var firmListDocumentNumberingSchemeCategoryScheme in category.ParagraphNumberingSchemes)
            {
                result = result || CheckIfNumberingSchemeIsInFirmListDocumentInParagraphNumberingScheme(firmListDocumentNumberingSchemeCategoryScheme, selectedNumberingSchemeName);
            }

            foreach (var subcategory in category.NestedParagraphNumberingSchemeCategory)
            {
                result = result || CheckIfNumberingSchemeIsInFirmListDocumentInParagraphNumberingCategory(subcategory, selectedNumberingSchemeName);
            }

            return result;
        }

        public static ObservableCollection<string> GetFollowableStyleNamesFromListTemplate(ListTemplate listTemplate, ParagraphNumberingScheme selectedParagraphNumberingScheme)
        {
            var followableStyleNames = new ObservableCollection<string>();

            if (listTemplate?.ListLevels != null && listTemplate.ListLevels.Count > 0)
            {
                foreach (ListLevel listLevel in listTemplate.ListLevels)
                {
                    var styleToFollow = GetParagraphStyleName(selectedParagraphNumberingScheme, listLevel.Index);

                    if (!followableStyleNames.Contains(styleToFollow))
                    {
                        followableStyleNames.Add(styleToFollow);
                    }
                }

                if (selectedParagraphNumberingScheme?.CreateContinuationParagraphStyles != null && selectedParagraphNumberingScheme.CreateContinuationParagraphStyles)
                {
                    foreach (ListLevel listLevel in listTemplate.ListLevels)
                    {
                        var continuationStyleToFollow = GetContinuationParagraphStyleName(selectedParagraphNumberingScheme, listLevel.Index);

                        if (!string.IsNullOrEmpty(continuationStyleToFollow) && !followableStyleNames.Contains(continuationStyleToFollow))
                        {
                            followableStyleNames.Add(continuationStyleToFollow);
                        }
                    }
                }
            }

            return followableStyleNames;
        }

        public static ObservableCollection<string> GetFollowableStyleNamesFromSyncedNumberingSchemes(ParagraphNumberingScheme selectedParagraphNumberingScheme)
        {
            var followableStyleNames = new ObservableCollection<string>();

            if (selectedParagraphNumberingScheme != null && selectedParagraphNumberingScheme.ParagraphNumberingLevels != null)
            {
                foreach (var selectedNumberingLevel in selectedParagraphNumberingScheme.ParagraphNumberingLevels)
                {
                    var styleToFollow = GetParagraphStyleName(selectedParagraphNumberingScheme, selectedNumberingLevel.Index);

                    if (!string.IsNullOrEmpty(styleToFollow))
                    {
                        followableStyleNames.Add(styleToFollow);
                    }
                }

                if (selectedParagraphNumberingScheme.CreateContinuationParagraphStyles)
                {
                    foreach (var selectedNumberingLevel in selectedParagraphNumberingScheme.ParagraphNumberingLevels)
                    {
                        var continuationStyleToFollow = GetContinuationParagraphStyleName(selectedParagraphNumberingScheme, selectedNumberingLevel.Index);

                        if (!string.IsNullOrEmpty(continuationStyleToFollow) && !followableStyleNames.Contains(continuationStyleToFollow))
                        {
                            followableStyleNames.Add(continuationStyleToFollow);
                        }
                    }
                }
            }

            return followableStyleNames;
        }

        public static string GetContinuationStyleNameFromLinkedStyleName(string linkedStyleName, int levelIndex)
        {
            var continuationParagraphStyleName = string.Empty;

            if (!string.IsNullOrEmpty(linkedStyleName))
            {
                if (Regex.Match(linkedStyleName, @".+?([_][L][\d])+$").Success)
                {
                    continuationParagraphStyleName = Regex.Replace(linkedStyleName.Trim(), @"([_][L][\d])+$", string.Empty);
                    continuationParagraphStyleName = continuationParagraphStyleName.Trim() + ListTemplateUtil.PARAGRAPH_STYLE_NAME_UNDERSCORE_WORD_SEPARATOR + ListTemplateUtil.CONTINUATION_PARAGRAPH_STYLE_NAME_INDICATOR + ListTemplateUtil.PARAGRAPH_STYLE_NAME_LEVEL_INDICATOR + levelIndex;
                }
                else if (Regex.Match(linkedStyleName, @".+?( ext [\d])+$").Success)
                {
                    continuationParagraphStyleName = Regex.Replace(linkedStyleName.Trim(), @".+?( ext [\d])+$", string.Empty);
                    continuationParagraphStyleName = continuationParagraphStyleName.Trim() + ListTemplateUtil.PARAGRAPH_STYLE_NAME_WORD_SEPARATOR + ListTemplateUtil.CONTINUATION_PARAGRAPH_STYLE_EXTENSION_NAME_INDICATOR + ListTemplateUtil.PARAGRAPH_STYLE_NAME_WORD_SEPARATOR + levelIndex;
                }
                else if (Regex.Match(linkedStyleName, @".+?([L][\d])+$").Success)
                {
                    continuationParagraphStyleName = Regex.Replace(linkedStyleName.Trim(), @"([L][\d])+$", string.Empty);
                    continuationParagraphStyleName = continuationParagraphStyleName.Trim() + ListTemplateUtil.PARAGRAPH_STYLE_NAME_WORD_SEPARATOR + ListTemplateUtil.CONTINUATION_PARAGRAPH_STYLE_NAME_INDICATOR + ListTemplateUtil.PARAGRAPH_STYLE_NAME_LEVEL_INDICATOR + levelIndex;
                }
                else if (Regex.Match(linkedStyleName, @".+?[\d]+$").Success)
                {
                    continuationParagraphStyleName = Regex.Replace(linkedStyleName.Trim(), @"[\d]+$", string.Empty);
                    continuationParagraphStyleName = continuationParagraphStyleName.Trim() + ListTemplateUtil.CONTINUATION_PARAGRAPH_STYLE_NAME_INDICATOR + levelIndex;
                }
                else
                {
                    continuationParagraphStyleName = linkedStyleName.Split(' ').First().Trim() + ListTemplateUtil.PARAGRAPH_STYLE_NAME_WORD_SEPARATOR + ListTemplateUtil.CONTINUATION_PARAGRAPH_STYLE_NAME_INDICATOR + ListTemplateUtil.PARAGRAPH_STYLE_NAME_LEVEL_INDICATOR + levelIndex;
                }
            }

            return continuationParagraphStyleName;
        }

        public static bool CheckIfStyleIsContinuationStyle(string continuationStyleName)
        {
            var styleIsContinuationStyle = false;

            if (!string.IsNullOrEmpty(continuationStyleName) && Regex.Match(continuationStyleName, @"(No#L?[\d])").Success)
            {
                styleIsContinuationStyle = true;
            }

            return styleIsContinuationStyle;
        }

        public static bool CheckIfStyleIsParagraphNumberingLevelStyle(string styleName)
        {
            var styleIsNumberingStyle = false;

            if (!string.IsNullOrEmpty(styleName) && Regex.Match(styleName, @"(L[\d])").Success)
            {
                styleIsNumberingStyle = true;
            }

            return styleIsNumberingStyle;
        }

        public static string GetLinkedStyleNameFromContinuationStyleName(string continuationStyleName, int levelIndex)
        {
            var linkedStyleName = string.Empty;

            if (!string.IsNullOrEmpty(continuationStyleName))
            {
                if (continuationStyleName.Contains(CONTINUATION_PARAGRAPH_STYLE_NAME_INDICATOR))
                {
                    linkedStyleName = continuationStyleName.Replace(CONTINUATION_PARAGRAPH_STYLE_NAME_INDICATOR, "").Replace("  ", " ").Replace("_ ", "_").Trim();
                }
                else
                {
                    var styleArr = continuationStyleName.Split(' ', '_');

                    if (styleArr != null && styleArr.Length > 0)
                    {
                        List<string> styleList = styleArr.ToList();
                        bool numberSeparated = false;
                        styleList.RemoveAt(styleList.Count - 1);
                        while (styleList.Count > 1 && RemoveSuffix(styleList[styleList.Count - 1]))
                        {
                            if (styleList[styleList.Count - 1].ToLower() == ListTemplateUtil.CONTINUATION_PARAGRAPH_STYLE_NAME_INDICATOR.ToLower())
                            {
                                numberSeparated = true;
                            }
                            styleList.RemoveAt(styleList.Count - 1);
                        }

                        linkedStyleName = continuationStyleName.Substring(0, string.Join(" ", styleList).Length + 1) + ListTemplateUtil.PARAGRAPH_STYLE_NAME_LEVEL_INDICATOR + (numberSeparated ? " " : "") + levelIndex;
                    }
                }
            }

            return linkedStyleName;
        }

        public static bool RemoveSuffix(string input)
        {
            return input.ToLower() == ListTemplateUtil.CONTINUATION_PARAGRAPH_STYLE_EXTENSION_NAME_INDICATOR.ToLower() ||
                input.ToLower() == ListTemplateUtil.CONTINUATION_PARAGRAPH_STYLE_NAME_INDICATOR.ToLower() ||
                input.ToLower() == ListTemplateUtil.PARAGRAPH_STYLE_NAME_LEVEL_INDICATOR.ToLower() ||
                input.ToLower() == "no" || input == "#";
        }

        public static string GetBaseStyleNameFromListTemplateInDocument(ListTemplate listTemplate, Document document)
        {
            var baseStyleName = string.Empty;

            try
            {
                if (listTemplate?.ListLevels != null && listTemplate.ListLevels.Count > 0 && document != null)
                {
                    var linkedStyleName = listTemplate?.ListLevels[1]?.LinkedStyle;
                    if (!string.IsNullOrEmpty(linkedStyleName))
                    {
                        Style linkedStyle = document.Styles[linkedStyleName];
                        if (linkedStyle != null)
                        {
                            dynamic baseStyle = linkedStyle.get_BaseStyle();
                            if (baseStyle.NameLocal != null)
                            {
                                baseStyleName = baseStyle.NameLocal;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }

            return baseStyleName;
        }

        public static Style GetStyleFromDocumentListTemplateListLevel(ParagraphNumberingScheme paragraphNumberingScheme, int levelIndex, Document document, bool isContinuationStyle = false)
        {
            Style style = null;

            if (paragraphNumberingScheme != null)
            {
                ListTemplate listTemplate = GetListTemplateByNameFromFirmListDocument(GetListTemplateName(paragraphNumberingScheme));

                if (listTemplate != null && listTemplate.ListLevels.Count >= levelIndex)
                {
                    ListLevel listLevel = listTemplate.ListLevels[levelIndex];

                    if (listLevel?.LinkedStyle != null)
                    {
                        if (isContinuationStyle)
                        {
                            style = GetParagraphStyleByName(GetContinuationStyleNameFromLinkedStyleName(listLevel.LinkedStyle, levelIndex), document);
                        }
                        else
                        {
                            style = GetParagraphStyleByName(listLevel.LinkedStyle, document);
                        }
                    }
                }
            }

            return style;
        }

        public static string GetBaseStyleNameFromStyle(Style style)
        {
            string baseStyleName = string.Empty;

            if (style != null)
            {
                try
                {
                    dynamic baseStyle = style.get_BaseStyle();

                    if (baseStyle?.NameLocal != null)
                    {
                        baseStyleName = baseStyle.NameLocal;
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex);
                }
            }

            return baseStyleName;
        }

        public static bool CheckIfStyleExistsInDocument(string styleName, Document document = null)
        {
            try
            {
                if (document != null)
                {
                    return document.Styles[styleName] != null;
                }
                else
                {
                    return DocumentUtil.GetActiveDocument().Styles[styleName] != null;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static string GetParagraphNumberingSchemeStyleToFollow(ParagraphNumberingScheme paragraphNumberingScheme, Style styleToFollow)
        {
            string styleToFollowName = string.Empty;

            if (paragraphNumberingScheme != null && !string.IsNullOrEmpty(styleToFollow?.NameLocal))
            {
                var levelNumber = GetLevelNumberFromStyleName(styleToFollow.NameLocal);

                if (levelNumber != -1)
                {
                    styleToFollowName = GetParagraphStyleName(paragraphNumberingScheme, levelNumber);
                }
                else
                {
                    styleToFollowName = styleToFollow.NameLocal;
                }
            }

            return styleToFollowName;
        }

        public static string GetParagraphNumberingSchemeContinuationContinuationStyleName(ParagraphNumberingScheme paragraphNumberingScheme, Style styleToFollow)
        {
            string continuationStyleToFollowName = string.Empty;

            if (paragraphNumberingScheme != null && !string.IsNullOrEmpty(styleToFollow?.NameLocal))
            {
                var levelNumber = GetLevelNumberFromStyleName(styleToFollow.NameLocal);

                if (levelNumber != -1)
                {
                    continuationStyleToFollowName = GetContinuationParagraphStyleName(paragraphNumberingScheme, levelNumber);
                }
                else
                {
                    continuationStyleToFollowName = styleToFollow.NameLocal;
                }
            }

            return continuationStyleToFollowName;
        }

        public static SelectedParagraphModel GetSelectionParagraphNumberAndRange(Paragraph selectedParagraph, int paragraphCountToGoBack = 0)
        {
            var selectedParagraphModel = new SelectedParagraphModel();

            if (paragraphCountToGoBack == 0)
            {
                selectedParagraphModel.LevelNumber = GetParagraphFullListNumber(selectedParagraph);
                selectedParagraphModel.Type = selectedParagraph.Range.ListFormat.ListType;
                selectedParagraphModel.Range = selectedParagraph?.Range;
            }
            else
            {
                for (var i = 1; i <= paragraphCountToGoBack; ++i)
                {
                    var previousParagraph = selectedParagraph.Previous(i);
                    var numberingLevel = GetParagraphFullListNumber(previousParagraph);

                    if (!string.IsNullOrEmpty(numberingLevel))
                    {
                        selectedParagraphModel.LevelNumber = numberingLevel;
                        selectedParagraphModel.Type = previousParagraph.Range.ListFormat.ListType;
                        selectedParagraphModel.Range = previousParagraph.Range;
                        break;
                    }
                }
            }

            return selectedParagraphModel;
        }

        private static string GetParagraphFullListNumber(Paragraph input)
        {
            string result = string.Empty;

            if (!string.IsNullOrWhiteSpace(input?.Range?.ListFormat?.ListString))
            {
                result = input?.Range?.ListFormat?.ListString;
                int count = result.Split(new string[] { "." }, StringSplitOptions.RemoveEmptyEntries).Length;
                int levelToSearch = input.Range.ListFormat.ListLevelNumber;
                levelToSearch = levelToSearch - count + 1;

                bool inputParagraphFound = false;
                var listParagraphs = input.Range.ListFormat.List.ListParagraphs;
                for (int i = listParagraphs.Count; i > 0; i--)
                {
                    if (!inputParagraphFound)
                    {
                        if (listParagraphs[i].Range.Start == input.Range.Start)
                        {
                            inputParagraphFound = true;
                            levelToSearch--;
                        }
                    }
                    else if (levelToSearch == 0)
                    {
                        break;
                    }
                    else if (listParagraphs[i].Range.ListFormat.ListLevelNumber <= levelToSearch)
                    {
                        bool trimPunctuation = result.Length > 0 && Regex.IsMatch(result[0].ToString(), @"[^a-zA-Z\d:]");

                        result = (trimPunctuation ? Utils.TrimEndPunctuationMarks(listParagraphs[i].Range.ListFormat.ListString) : listParagraphs[i].Range.ListFormat.ListString) + result;
                        count = listParagraphs[i].Range.ListFormat.ListString.Split(new string[] { "." }, StringSplitOptions.RemoveEmptyEntries).Length;

                        levelToSearch = listParagraphs[i].Range.ListFormat.ListLevelNumber - count;
                    }
                }
            }

            return result;
        }

        public static void ChangeListLevelInSelection(int levelNumber, bool isContinuationParagraphStyle, string listTemplateName = null)
        {
            if(!string.IsNullOrEmpty(listTemplateName))
            {
                using (new IntensiveDocumentInteractionWrapper(undoTransactionName: $"Level changed to {levelNumber}"))
                {
                    ListTemplate customListTemplate = GetListTemplateByName(listTemplateName);
                    var currentLevel = (customListTemplate != null && customListTemplate.ListLevels.Count > 0) ? customListTemplate.ListLevels[levelNumber] : null;

                    if (currentLevel != null)
                    {
                        var replacementStyleName = string.Empty;

                        if (isContinuationParagraphStyle)
                        {
                            replacementStyleName = GetContinuationStyleNameFromLinkedStyleName(currentLevel.LinkedStyle, currentLevel.Index);
                        }
                        else
                        {
                            replacementStyleName = currentLevel?.LinkedStyle;
                        }

                        DocumentUtil.ApplyStyleToSelection(replacementStyleName);
                    }

                    var lists = GetAllListsUsingListTemplate(customListTemplate.Name);
                    RemoveNewLinePrefixInSelection();
                    if (currentLevel != null && currentLevel.TrailingCharacter == WdTrailingCharacter.wdTrailingNone)
                    {
                        if (!isContinuationParagraphStyle)
                        {
                            if (CheckIfListParagraphHasNewLine(lists, customListTemplate?.Name, currentLevel.Index))
                            {
                                AddNewLinePrefixInSelection();
                            }
                        }
                    }


                    Utils.ReplaceTcFieldsInDocument(ThisAddIn.Instance.Application.ActiveDocument);
                    Utils.ActivateApplication();
                }
            }
        }
    }
}
