﻿using System.Text.RegularExpressions;

namespace InfowareVSTO.Windows.ParagraphNumbering
{
    internal static class MsWordConstants
    {
        public const string MSWORD_NUMBERING_LEVEL_PREFIX = "%";

        public static readonly Regex MSWORD_NUMBERING_REGEX = new Regex($@"{MSWORD_NUMBERING_LEVEL_PREFIX}\d", RegexOptions.Compiled);

        public const string TIMES_NEW_ROMAN_FONT = "Times New Roman";

        public const string DEFAULT_FONT = TIMES_NEW_ROMAN_FONT;

        public const string SYMBOL_FONT = "Symbol";

        public const string WINGDINGS_FONT = "Wingdings";

        public const string DEFAULT_BULLET_FONT = SYMBOL_FONT;

        public static readonly string CIRCLE_BULLET_CHAR = ((char) (-3913 & 0xFFF)).ToString(); // Symbol font

        public static readonly string SQUARE_BULLET_CHAR = ((char) (-3929 & 0xFFF)).ToString(); // Wingdings font

        public static readonly string ARROW_BULLET_CHAR = ((char) (-3880 & 0xFFF)).ToString(); // Wingdings font

        public static readonly string RHOMBUS_BULLET_CHAR = ((char) (-3978 & 0xFFF)).ToString(); // Wingdings font

        // Style that seems to be used by the enterprise extension for "Table" numbering schemes. Doesn't seem to actually exist in MSWord
        public const string TABLE_TEXT_STYLE_NAME = "TableText";

        public const string NEW_LINE_TRAILING_CHARACTER = "\v";
    }
}
