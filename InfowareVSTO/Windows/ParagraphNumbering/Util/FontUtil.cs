﻿using Microsoft.Office.Interop.Word;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfowareVSTO.Windows.ParagraphNumbering.Util
{
    public class FontUtil
    {


        public static WdColor ConvertHexToWdColor(string hex)
        {
            if (hex == null && hex.Length < 7)
            {
                return WdColor.wdColorAutomatic;
            }

            if (hex.Length > 8)
            {
                hex = hex.Substring(3);
            }
            else
            {
                hex = hex.Substring(1);
            }

            try
            {
                int value = int.Parse(hex.Substring(0, 2), System.Globalization.NumberStyles.HexNumber) +
                   int.Parse(hex.Substring(2, 2), System.Globalization.NumberStyles.HexNumber) * 256 +
                    int.Parse(hex.Substring(4, 2), System.Globalization.NumberStyles.HexNumber) * 256 * 256;
                return (WdColor)value;

            }
            catch (Exception)
            {
                return WdColor.wdColorAutomatic;
            }
        }

        public static string ConvertWdColorToHex(WdColor color)
        {

            int value = (int)color;
            int blue = value / 65536;
            if(value < 0 || value > 0xFFFFFF || value == 9999999)
            {
                return "#000000";
            }

            int red = value % 256;
       
            int green;
            green = value - blue * 65536 - red; 
            int hexInt = red * 65536 + green + blue;
            return "#FF" +  hexInt.ToString("X6");
        }
    }
}
