﻿using System;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using InfowareVSTO.Util;
using Microsoft.Office.Interop.Word;
using Paragraph = System.Windows.Documents.Paragraph;

namespace InfowareVSTO.Windows.ParagraphNumbering
{
    internal static class ParagraphUtil
    {
        public static void PopulateParagraphNumberingLevels(BlockCollection paragraphCollection, ParagraphNumberingScheme selectedNumberingScheme,
            Action<Paragraph, ParagraphNumberingLevel> paragraphClickHandler, int selectedParagraphIndex = 0)
        {
            paragraphCollection.Clear();

            if (selectedNumberingScheme?.ParagraphNumberingLevels != null)
            {
                foreach (ParagraphNumberingLevel level in selectedNumberingScheme.ParagraphNumberingLevels)
                {
                    Paragraph newParagraph = CreateParagraphForNumberingLevel(selectedNumberingScheme, level.Index, paragraphClickHandler);
                    paragraphCollection.Add(newParagraph);
                }
            }

            MarkParagraphAsSelected(paragraphCollection, selectedParagraphIndex);
        }

        private static Paragraph CreateParagraphForNumberingLevel(ParagraphNumberingScheme numberingScheme, int paragraphNumberingLevelIndex,
            Action<Paragraph, ParagraphNumberingLevel> paragraphClickHandler)
        {
            ParagraphNumberingLevel numberingLevel = numberingScheme.ParagraphNumberingLevels[paragraphNumberingLevelIndex - 1];

            string paragraphNumber = $"{(numberingLevel.BulletCharacterIsEnabled ? numberingLevel.BulletCharacter : numberingLevel.FirstNumberFormatRepresentation)} ";
            string paragraphContent = string.IsNullOrWhiteSpace(numberingScheme.RootStylePrefix) ? $"{numberingScheme.Name}_L{paragraphNumberingLevelIndex} ------------" :
                $"{numberingScheme.RootStylePrefix}{paragraphNumberingLevelIndex} ------------";

            Inline paragraphNumberInline = new Run(numberingLevel.FontPreferences.IsAllCaps ? paragraphNumber.ToUpper() : paragraphNumber);
            if (numberingLevel.FontPreferences.Name != null)
            {
                paragraphNumberInline.FontFamily = new FontFamily(numberingLevel.FontPreferences.Name);
            }

            Inline paragraphContentInline = new Run(numberingLevel.ParagraphFormat.FontPreferences.IsAllCaps ? paragraphContent.ToUpper() : paragraphContent);
            if (numberingLevel.ParagraphFormat.FontPreferences.Name != null)
            {
                paragraphContentInline.FontFamily = new FontFamily(numberingLevel.ParagraphFormat.FontPreferences.Name);
            }

            if (numberingLevel.FontPreferences.IsBold)
            {
                paragraphNumberInline = new Bold(paragraphNumberInline);
            }

            if (numberingLevel.ParagraphFormat.FontPreferences.IsBold)
            {
                paragraphContentInline = new Bold(paragraphContentInline);
            }

            if (numberingLevel.FontPreferences.IsItalic)
            {
                paragraphNumberInline = new Italic(paragraphNumberInline);
            }

            if (numberingLevel.ParagraphFormat.FontPreferences.IsItalic)
            {
                paragraphContentInline = new Italic(paragraphContentInline);
            }

            if ((int)numberingLevel.FontPreferences.UnderlineType != 9999999 && (int)numberingLevel.FontPreferences.UnderlineType != (int)WdUnderline.wdUnderlineNone)
            {
                paragraphNumberInline = new Underline(paragraphNumberInline);
            }

            if (numberingLevel.ParagraphFormat.FontPreferences.UnderlineType != WdUnderline.wdUnderlineNone)
            {
                paragraphContentInline = new Underline(paragraphContentInline);
            }

            if (numberingLevel.ParagraphFormat.FontPreferences.IsSmallCaps)
            {
                paragraphContentInline.Typography.Capitals = FontCapitals.AllSmallCaps;
            }

            paragraphNumberInline.Foreground = WpfUtil.GetColorFromHex(numberingLevel.FontPreferences.HexColor);
            paragraphContentInline.Foreground = WpfUtil.GetColorFromHex(numberingLevel.ParagraphFormat.FontPreferences.HexColor);

            var paragraph = new Paragraph(paragraphNumberInline)
            {
                LineHeight = 1.5
            };

            paragraph.Inlines.Add(paragraphContentInline);

            /*
             * Sets only for the first line, but is shouldn't matter since the preview window's paragraphs are one line long
             * As per https://docs.microsoft.com/en-us/dotnet/api/system.windows.documents.paragraph.textindent, 
             * 1 inch = 96, but with that value the paragraphs don't fit in the preview window
             */
            paragraph.TextIndent = numberingLevel.ParagraphFormat.LeftIndent * 48;

            if (numberingLevel.ParagraphFormat.IsLeftAligned)
            {
                paragraph.TextAlignment = TextAlignment.Left;
            }
            else if (numberingLevel.ParagraphFormat.IsCenterAligned)
            {
                paragraph.TextAlignment = TextAlignment.Center;
            }
            else if (numberingLevel.ParagraphFormat.IsRightAligned)
            {
                paragraph.TextAlignment = TextAlignment.Right;
            }
            else if (numberingLevel.ParagraphFormat.IsJustifiedAligned)
            {
                paragraph.TextAlignment = TextAlignment.Justify;
            }

            paragraph.PreviewMouseDown += (object sender, MouseButtonEventArgs e) => paragraphClickHandler((Paragraph) sender, numberingLevel);

            return paragraph;
        }

        public static void MarkParagraphAsSelected(BlockCollection paragraphCollection, int paragraphIndex)
        {
            int i = 0;

            foreach (Paragraph paragraph in paragraphCollection)
            {
                if (i == paragraphIndex)
                {
                    MarkParagraphAsSelected(paragraphCollection, paragraph);
                    return;
                }

                i++;
            }
        }

        public static void MarkParagraphAsSelected(BlockCollection paragraphCollection, Paragraph paragraph)
        {
            ResetParagraphBackgroundColor(paragraphCollection);

            paragraph.Background = Brushes.LightBlue;

            paragraph.BringIntoView();
        }

        public static int GetParagraphIndex(BlockCollection paragraphCollection, Paragraph paragraph)
        {
            int i = 0;

            foreach (Paragraph existingParagraph in paragraphCollection)
            {
                if (existingParagraph == paragraph)
                {
                    return i;
                }

                i++;
            }

            return -1;
        }

        private static void ResetParagraphBackgroundColor(BlockCollection paragraphCollection)
        {
            foreach (Paragraph paragraph in paragraphCollection)
            {
                paragraph.Background = Brushes.White;
            }
        }

        public static void ClearParagraphs(BlockCollection paragraphCollection)
        {
            paragraphCollection.Clear();
        }
    }
}
