﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using InfowareVSTO.Common.Language;
using InfowareVSTO.Util;
using InfowareVSTO.Windows.ParagraphNumbering.Model;
using InfowareVSTO.Windows.ParagraphNumbering.Model.Enums;
using InfowareVSTO.Windows.ParagraphNumbering.Repositories;
using InfowareVSTO.Windows.ParagraphNumbering.Repositories.Interfaces;
using Microsoft.Office.Interop.Word;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace InfowareVSTO.Windows.ParagraphNumbering.Util
{
    public class ParagraphNumberingSchemeManager
    {
        private const string FAVORITE_LISTS_SETTING_NAME = "FavoriteLists";
        public static ParagraphNumberingSchemeManager Instance { get; }

        public HashSet<ParagraphNumberingScheme> BuiltInParagraphNumberingSchemes { get; private set; }

        public HashSet<ParagraphNumberingScheme> SyncedParagraphNumberingSchemes { get; private set; }

        public Dictionary<string, ParagraphNumberingScheme> ListTemplateNamesToBuiltInParagraphNumberingSchemes = new Dictionary<string, ParagraphNumberingScheme>();

        public Dictionary<string, ParagraphNumberingScheme> ListTemplateNamesToSyncedParagraphNumberingSchemes = new Dictionary<string, ParagraphNumberingScheme>();

        public Dictionary<string, ParagraphNumberingScheme> ListTemplateNamesToFirmParagraphNumberingSchemes = new Dictionary<string, ParagraphNumberingScheme>();

        public ObservableCollection<ParagraphNumberingSchemeType> ParagraphNumberingSchemeTypes { get; set; }

        public ParagraphNumberingSchemeType ActiveDocumentParagraphNumberingSchemeType { get; private set; }

        public ObservableCollection<object> ActiveDocumentParagraphNumberingSchemes { get; set; }

        public ParagraphNumberingSchemeType FirmDocumentParagraphNumberingSchemeType { get; private set; }

        public ObservableCollection<object> FirmDocumentParagraphNumberingSchemes { get; private set; }

        public ParagraphNumberingSchemeType BuiltInParagraphNumberingSchemeType { get; private set; }

        public ParagraphNumberingSchemeType SyncedParagraphNumberingSchemeType { get; private set; }

        public ObservableCollection<object> FavoritesParagraphNumberingSchemes { get; private set; }

        public ParagraphNumberingSchemeType FavoritesParagraphNumberingSchemeType { get; set; }

        public List<string> FavoriteListNames { get; set; }

        private JToken FirmListStructure { get; set; }

        private static readonly Application app;

        private static readonly IParagraphNumberingSchemeRepository paragraphNumberingSchemeRepository;

        private static ParagraphNumberingScheme firstFirmParagraphNumberingScheme;

        static ParagraphNumberingSchemeManager()
        {
            paragraphNumberingSchemeRepository = new ParagraphNumberingSchemeRepository();

            Instance = new ParagraphNumberingSchemeManager();

            app = Globals.ThisAddIn.Application;
        }

        private ParagraphNumberingSchemeManager()
        {
            InitDefaultParagraphNumberingSchemes();
            GetFavoriteLists();
        }

        private void GetFavoriteLists()
        {
            var response = AdminPanelWebApi.GetCustomSetting(Utils.STATIC_PROCESSOR_ID, FAVORITE_LISTS_SETTING_NAME);
            if (!string.IsNullOrWhiteSpace(response?.Data?.ToString()))
            {
                try
                {
                    var list = JsonConvert.DeserializeObject<List<string>>(response.Data.ToString());
                    if (list != null)
                    {
                        FavoriteListNames = list;
                    }
                }
                catch(Exception ex)
                {
                }
            }
        }

        private void InitDefaultParagraphNumberingSchemes()
        {
            var schemeGen = new ParagraphNumberingScheme("Gen", "General 1. (a) (i)", new List<ParagraphNumberingLevel>()
                {
                    new ParagraphNumberingLevel(1, "%1.", WdListNumberStyle.wdListNumberStyleArabic,
                    new ParagraphFormat(0, 8, null, 0, 0, 0, SpecialParagraphIndentation.HANGING, 0.5f,
                     false, false, false, true), 0.5f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 0),

                    new ParagraphNumberingLevel(2, "(%2)", WdListNumberStyle.wdListNumberStyleLowercaseLetter,
                    new ParagraphFormat(0, 8, null, 0, 0.5f, 0, SpecialParagraphIndentation.HANGING, 0.5f,
                     false, false, false, true), 1f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 0.5f),

                    new ParagraphNumberingLevel(3, "(%3)", WdListNumberStyle.wdListNumberStyleLowercaseRoman,
                    new ParagraphFormat(0, 8, null, 0, 1.2f, 0, SpecialParagraphIndentation.HANGING, 0.3f,
                     false, false, false, true), 1.5f, numberAlignment:WdListLevelAlignment.wdListLevelAlignRight, numberIndent: 1.2f),

                    new ParagraphNumberingLevel(4, "(%4)", WdListNumberStyle.wdListNumberStyleUppercaseLetter,
                    new ParagraphFormat(0, 8, null, 0, 1.5f, 0, SpecialParagraphIndentation.HANGING, 0.5f,
                     false, false, false, true), 2f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 1.5f),

                    new ParagraphNumberingLevel(5, "(%5)", WdListNumberStyle.wdListNumberStyleUppercaseRoman,
                    new ParagraphFormat(0, 8, null, 0, 2.2f, 0, SpecialParagraphIndentation.HANGING, 0.3f,
                     false, false, false, true), 2.5f, numberAlignment:WdListLevelAlignment.wdListLevelAlignRight, numberIndent: 2.2f),

                    new ParagraphNumberingLevel(6, "(%6)", WdListNumberStyle.wdListNumberStyleArabic,
                    new ParagraphFormat(0, 8, null, 0, 2.5f, 0, SpecialParagraphIndentation.HANGING, 0.5f,
                     false, false, false, true), 3f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 2.5f),

                    new ParagraphNumberingLevel(7, "%7)", WdListNumberStyle.wdListNumberStyleLowercaseLetter,
                    new ParagraphFormat(0, 8, null, 0, 3.0f, 0, SpecialParagraphIndentation.HANGING, 0.5f,
                     false, false, false, true), 3.5f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 3f),

                    new ParagraphNumberingLevel(8, "%8)", WdListNumberStyle.wdListNumberStyleLowercaseRoman,
                    new ParagraphFormat(0, 8, null, 0, 3.7f, 0, SpecialParagraphIndentation.HANGING, 0.3f,
                     false, false, false, true), 4f, numberAlignment:WdListLevelAlignment.wdListLevelAlignRight, numberIndent: 3.7f),

                    new ParagraphNumberingLevel(9, "%9)", WdListNumberStyle.wdListNumberStyleArabic,
                    new ParagraphFormat(0, 8, null, 0, 4.0f, 0, SpecialParagraphIndentation.HANGING, 0.5f,
                     false, false, false, true), 4.5f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 4f)
            }, "");

            firstFirmParagraphNumberingScheme = schemeGen;

            var bulletFontPreferences = new FontPreferences(MsWordConstants.SYMBOL_FONT, 10, false, false, false, WdUnderline.wdUnderlineNone, "#000000", WdLanguageID.wdEnglishCanadian);

            var schemeBullets0ptAfter = new ParagraphNumberingScheme("Bullets2", "Bullets 0pt after", new List<ParagraphNumberingLevel>()
                {
                    new ParagraphNumberingLevel(1, "%1.", WdListNumberStyle.wdListNumberStyleBullet,
                    new ParagraphFormat(0, 0, null, 0, 0, 0, SpecialParagraphIndentation.HANGING, 0.5f,
                     false, false, false, true), 0.5f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 0, fontPreferences: bulletFontPreferences) { BulletCharacter = MsWordConstants.CIRCLE_BULLET_CHAR},

                    new ParagraphNumberingLevel(2, "(%2)", WdListNumberStyle.wdListNumberStyleBullet,
                    new ParagraphFormat(0, 0, null, 0, 0.5f, 0, SpecialParagraphIndentation.HANGING, 0.5f,
                     false, false, false, true), 1f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 0.5f, fontPreferences: bulletFontPreferences) { BulletCharacter = MsWordConstants.CIRCLE_BULLET_CHAR},

                    new ParagraphNumberingLevel(3, "(%3)", WdListNumberStyle.wdListNumberStyleBullet,
                    new ParagraphFormat(0, 0, null, 0, 1.0f, 0, SpecialParagraphIndentation.HANGING, 0.5f,
                     false, false, false, true), 1.5f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 1.0f, fontPreferences: bulletFontPreferences) { BulletCharacter = MsWordConstants.CIRCLE_BULLET_CHAR},

                    new ParagraphNumberingLevel(4, "(%4)", WdListNumberStyle.wdListNumberStyleBullet,
                    new ParagraphFormat(0, 0, null, 0, 1.5f, 0, SpecialParagraphIndentation.HANGING, 0.5f,
                     false, false, false, true), 2f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 1.5f, fontPreferences: bulletFontPreferences) { BulletCharacter = MsWordConstants.CIRCLE_BULLET_CHAR},

                    new ParagraphNumberingLevel(5, "(%5)", WdListNumberStyle.wdListNumberStyleBullet,
                    new ParagraphFormat(0, 0, null, 0, 2.0f, 0, SpecialParagraphIndentation.HANGING, 0.5f,
                     false, false, false, true), 2.5f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 2.0f, fontPreferences: bulletFontPreferences) { BulletCharacter = MsWordConstants.CIRCLE_BULLET_CHAR},

                    new ParagraphNumberingLevel(6, "(%6)", WdListNumberStyle.wdListNumberStyleBullet,
                    new ParagraphFormat(0, 0, null, 0, 2.5f, 0, SpecialParagraphIndentation.HANGING, 0.5f,
                     false, false, false, true), 3f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 2.5f, fontPreferences: bulletFontPreferences) { BulletCharacter = MsWordConstants.CIRCLE_BULLET_CHAR},

                    new ParagraphNumberingLevel(7, "%7)", WdListNumberStyle.wdListNumberStyleBullet,
                    new ParagraphFormat(0, 0, null, 0, 3.0f, 0, SpecialParagraphIndentation.HANGING, 0.5f,
                     false, false, false, true), 3.5f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 3f, fontPreferences: bulletFontPreferences) { BulletCharacter = MsWordConstants.CIRCLE_BULLET_CHAR},

                    new ParagraphNumberingLevel(8, "%8)", WdListNumberStyle.wdListNumberStyleBullet,
                    new ParagraphFormat(0, 0, null, 0, 3.5f, 0, SpecialParagraphIndentation.HANGING, 0.5f,
                     false, false, false, true), 4f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 3.5f, fontPreferences: bulletFontPreferences) { BulletCharacter = MsWordConstants.CIRCLE_BULLET_CHAR},

                    new ParagraphNumberingLevel(9, "%9)", WdListNumberStyle.wdListNumberStyleBullet,
                    new ParagraphFormat(0, 0, null, 0, 4.0f, 0, SpecialParagraphIndentation.HANGING, 0.5f,
                     false, false, false, true), 4.5f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 4f, fontPreferences: bulletFontPreferences) { BulletCharacter = MsWordConstants.CIRCLE_BULLET_CHAR}
            }, "Same as the Bullets1 scheme, but paragraph spacing after set to 0 pt. Used if you are typing a bulleted list and don’t want blank lines between your bulleted paragraphs.");

            var schemeBullets12ptAfter = new ParagraphNumberingScheme("Bullets1", "Bullets 12pt after", new List<ParagraphNumberingLevel>()
                {
                    new ParagraphNumberingLevel(1, "%1.", WdListNumberStyle.wdListNumberStyleBullet,
                    new ParagraphFormat(0, 12, null, 0, 0, 0, SpecialParagraphIndentation.HANGING, 0.5f,
                     false, false, false, true), 0.5f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 0, fontPreferences: bulletFontPreferences) { BulletCharacter = MsWordConstants.CIRCLE_BULLET_CHAR},

                    new ParagraphNumberingLevel(2, "(%2)", WdListNumberStyle.wdListNumberStyleBullet,
                    new ParagraphFormat(0, 12, null, 0, 0.5f, 0, SpecialParagraphIndentation.HANGING, 0.5f,
                     false, false, false, true), 1f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 0.5f, fontPreferences: bulletFontPreferences) { BulletCharacter = MsWordConstants.CIRCLE_BULLET_CHAR},

                    new ParagraphNumberingLevel(3, "(%3)", WdListNumberStyle.wdListNumberStyleBullet,
                    new ParagraphFormat(0, 12, null, 0, 1.0f, 0, SpecialParagraphIndentation.HANGING, 0.5f,
                     false, false, false, true), 1.5f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 1.0f, fontPreferences: bulletFontPreferences) { BulletCharacter = MsWordConstants.CIRCLE_BULLET_CHAR},

                    new ParagraphNumberingLevel(4, "(%4)", WdListNumberStyle.wdListNumberStyleBullet,
                    new ParagraphFormat(0, 12, null, 0, 1.5f, 0, SpecialParagraphIndentation.HANGING, 0.5f,
                     false, false, false, true), 2f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 1.5f, fontPreferences: bulletFontPreferences) { BulletCharacter = MsWordConstants.CIRCLE_BULLET_CHAR},

                    new ParagraphNumberingLevel(5, "(%5)", WdListNumberStyle.wdListNumberStyleBullet,
                    new ParagraphFormat(0, 12, null, 0, 2.0f, 0, SpecialParagraphIndentation.HANGING, 0.5f,
                     false, false, false, true), 2.5f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 2.0f, fontPreferences: bulletFontPreferences) { BulletCharacter = MsWordConstants.CIRCLE_BULLET_CHAR},

                    new ParagraphNumberingLevel(6, "(%6)", WdListNumberStyle.wdListNumberStyleBullet,
                    new ParagraphFormat(0, 12, null, 0, 2.5f, 0, SpecialParagraphIndentation.HANGING, 0.5f,
                     false, false, false, true), 3f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 2.5f, fontPreferences: bulletFontPreferences) { BulletCharacter = MsWordConstants.CIRCLE_BULLET_CHAR},

                    new ParagraphNumberingLevel(7, "%7)", WdListNumberStyle.wdListNumberStyleBullet,
                    new ParagraphFormat(0, 12, null, 0, 3.0f, 0, SpecialParagraphIndentation.HANGING, 0.5f,
                     false, false, false, true), 3.5f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 3f, fontPreferences: bulletFontPreferences) { BulletCharacter = MsWordConstants.CIRCLE_BULLET_CHAR},

                    new ParagraphNumberingLevel(8, "%8)", WdListNumberStyle.wdListNumberStyleBullet,
                    new ParagraphFormat(0, 12, null, 0, 3.5f, 0, SpecialParagraphIndentation.HANGING, 0.5f,
                     false, false, false, true), 4f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 3.5f, fontPreferences: bulletFontPreferences) { BulletCharacter = MsWordConstants.CIRCLE_BULLET_CHAR},

                    new ParagraphNumberingLevel(9, "%9)", WdListNumberStyle.wdListNumberStyleBullet,
                    new ParagraphFormat(0, 12, null, 0, 4.0f, 0, SpecialParagraphIndentation.HANGING, 0.5f,
                     false, false, false, true), 4.5f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 4f, fontPreferences: bulletFontPreferences) { BulletCharacter = MsWordConstants.CIRCLE_BULLET_CHAR}
            }, "This has paragraph spacing after set to 12 pt for general documents.");

            var bulletFontPreferencesSize12 = new FontPreferences();
            var wingdingsBulletFontPreferences = new FontPreferences();

            var schemeBulletsVarious = new ParagraphNumberingScheme("BulletsS", "Bullets Various", new List<ParagraphNumberingLevel>()
                {
                    new ParagraphNumberingLevel(1, "%1.", WdListNumberStyle.wdListNumberStyleBullet,
                    new ParagraphFormat(0, 12, null, 0, 0, 0, SpecialParagraphIndentation.HANGING, 0.5f,
                     true, false, false, false), 0.5f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 0, fontPreferences: bulletFontPreferencesSize12) { BulletCharacter = MsWordConstants.CIRCLE_BULLET_CHAR},

                    new ParagraphNumberingLevel(2, "(%2)", WdListNumberStyle.wdListNumberStyleBullet,
                    new ParagraphFormat(0, 12, null, 0, 0.5f, 0, SpecialParagraphIndentation.HANGING, 0.5f,
                     true, false, false, false), 1f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 0.5f, fontPreferences: wingdingsBulletFontPreferences) { BulletCharacter = MsWordConstants.SQUARE_BULLET_CHAR},

                    new ParagraphNumberingLevel(3, "(%3)", WdListNumberStyle.wdListNumberStyleBullet,
                    new ParagraphFormat(0, 12, null, 0, 1.0f, 0, SpecialParagraphIndentation.HANGING, 0.5f,
                     true, false, false, false), 1.5f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 1.0f, fontPreferences: wingdingsBulletFontPreferences) { BulletCharacter = MsWordConstants.ARROW_BULLET_CHAR},

                    new ParagraphNumberingLevel(4, "(%4)", WdListNumberStyle.wdListNumberStyleBullet,
                    new ParagraphFormat(0, 12, null, 0, 1.5f, 0, SpecialParagraphIndentation.HANGING, 0.5f,
                     true, false, false, false), 2f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 1.5f, fontPreferences: wingdingsBulletFontPreferences) { BulletCharacter = MsWordConstants.RHOMBUS_BULLET_CHAR},

                    new ParagraphNumberingLevel(5, "(%5)", WdListNumberStyle.wdListNumberStyleBullet,
                    new ParagraphFormat(0, 12, null, 0, 2.0f, 0, SpecialParagraphIndentation.HANGING, 0.5f,
                     true, false, false, false), 2.5f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 2.0f, fontPreferences: bulletFontPreferencesSize12) { BulletCharacter = MsWordConstants.CIRCLE_BULLET_CHAR},

                    new ParagraphNumberingLevel(6, "(%6)", WdListNumberStyle.wdListNumberStyleBullet,
                    new ParagraphFormat(0, 12, null, 0, 2.5f, 0, SpecialParagraphIndentation.HANGING, 0.5f,
                     true, false, false, false), 3f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 2.5f, fontPreferences: wingdingsBulletFontPreferences) { BulletCharacter = MsWordConstants.SQUARE_BULLET_CHAR},

                    new ParagraphNumberingLevel(7, "%7)", WdListNumberStyle.wdListNumberStyleBullet,
                    new ParagraphFormat(0, 12, null, 0, 3.0f, 0, SpecialParagraphIndentation.HANGING, 0.5f,
                     true, false, false, false), 3.5f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 3f, fontPreferences: wingdingsBulletFontPreferences) { BulletCharacter = MsWordConstants.ARROW_BULLET_CHAR},

                    new ParagraphNumberingLevel(8, "%8)", WdListNumberStyle.wdListNumberStyleBullet,
                    new ParagraphFormat(0, 12, null, 0, 3.5f, 0, SpecialParagraphIndentation.HANGING, 0.5f,
                     true, false, false, false), 4f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 3.5f, fontPreferences: wingdingsBulletFontPreferences) { BulletCharacter = MsWordConstants.RHOMBUS_BULLET_CHAR},

                    new ParagraphNumberingLevel(9, "%9)", WdListNumberStyle.wdListNumberStyleBullet,
                    new ParagraphFormat(0, 12, null, 0, 4.0f, 0, SpecialParagraphIndentation.HANGING, 0.5f,
                     true, false, false, false), 4.5f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 4f, fontPreferences: bulletFontPreferencesSize12) { BulletCharacter = MsWordConstants.CIRCLE_BULLET_CHAR}
            }, "");

            var schemeShortArg = new ParagraphNumberingScheme("ShortAgr", "Short Agreement 1. 1.1 (a) (i)", new List<ParagraphNumberingLevel>()
                {
                    new ParagraphNumberingLevel(1, "%1.", WdListNumberStyle.wdListNumberStyleArabic,
                    new ParagraphFormat(0, 8, null, 0, 0, 0, SpecialParagraphIndentation.HANGING, 0.5f,
                     false, false, false, true, outlineLevel: WdOutlineLevel.wdOutlineLevel1), 0.5f, numberIndent: 0),

                    new ParagraphNumberingLevel(2, "%1.%2", WdListNumberStyle.wdListNumberStyleArabic,
                    new ParagraphFormat(0, 8, null, 0, 0.5f, 0, SpecialParagraphIndentation.HANGING, 0.5f,
                     false, false, false, true, outlineLevel: WdOutlineLevel.wdOutlineLevel2), 1f, numberIndent: 0.5f),

                     new ParagraphNumberingLevel(3, "(%3)", WdListNumberStyle.wdListNumberStyleLowercaseLetter,
                    new ParagraphFormat(0, 8, null, 0, 1f, 0, SpecialParagraphIndentation.HANGING, 0.5f,
                     false, false, false, true, outlineLevel: WdOutlineLevel.wdOutlineLevelBodyText), 1.5f, numberIndent: 1f),

                    new ParagraphNumberingLevel(4, "(%4)", WdListNumberStyle.wdListNumberStyleLowercaseRoman,
                    new ParagraphFormat(0, 8, null, 0, 1.7f, 0, SpecialParagraphIndentation.HANGING, 0.3f,
                     false, false, false, true, outlineLevel: WdOutlineLevel.wdOutlineLevelBodyText), 2f,
                    numberAlignment: WdListLevelAlignment.wdListLevelAlignRight, numberIndent: 1.7f),

                    new ParagraphNumberingLevel(5, "(%5)", WdListNumberStyle.wdListNumberStyleUppercaseLetter,
                    new ParagraphFormat(0, 8, null, 0, 2f, 0, SpecialParagraphIndentation.HANGING, 0.5f,
                     false, false, false, true, outlineLevel: WdOutlineLevel.wdOutlineLevelBodyText), 2.5f,
                    numberAlignment: WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 2f),

                    new ParagraphNumberingLevel(6, "(%6)", WdListNumberStyle.wdListNumberStyleUppercaseRoman,
                    new ParagraphFormat(0, 8, null, 0, 2.7f, 0, SpecialParagraphIndentation.HANGING, 0.3f,
                     false, false, false, true, outlineLevel: WdOutlineLevel.wdOutlineLevelBodyText), 3f,
                    numberAlignment: WdListLevelAlignment.wdListLevelAlignRight, numberIndent: 2.7f),

                    new ParagraphNumberingLevel(7, "%7)", WdListNumberStyle.wdListNumberStyleLowercaseRoman,
                    new ParagraphFormat(0, 8, null, 0, 3.2f, 0, SpecialParagraphIndentation.HANGING, 0.3f,
                     false, false, false, true, outlineLevel: WdOutlineLevel.wdOutlineLevelBodyText), 3.5f,
                    numberAlignment: WdListLevelAlignment.wdListLevelAlignRight, numberIndent: 3.2f),

                     new ParagraphNumberingLevel(8, "%8.", WdListNumberStyle.wdListNumberStyleLowercaseLetter,
                    new ParagraphFormat(0, 8, null, 0, 3.5f, 0, SpecialParagraphIndentation.HANGING, 0.5f,
                     false, false, false, true, outlineLevel: WdOutlineLevel.wdOutlineLevelBodyText), 4f,
                    numberAlignment: WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 3.5f),

                     new ParagraphNumberingLevel(9, "%9.", WdListNumberStyle.wdListNumberStyleLowercaseRoman,
                    new ParagraphFormat(0, 8, null, 0, 4.2f, 0, SpecialParagraphIndentation.HANGING, 0.3f,
                     false, false, false, true, outlineLevel: WdOutlineLevel.wdOutlineLevelBodyText), 4.5f,
                    numberAlignment: WdListLevelAlignment.wdListLevelAlignRight, numberIndent: 4.2f)
                }, "");

            var schemeArticle = new ParagraphNumberingScheme("Art", "Article 1 1.1 (a) (i)", new List<ParagraphNumberingLevel>()
                {
                    new ParagraphNumberingLevel(1, "ARTICLE %1", WdListNumberStyle.wdListNumberStyleArabic,
                    new ParagraphFormat(6, 8, null, 0, 0, 0, SpecialParagraphIndentation.NONE, 0,
                     false, true, false, false, keepLinesTogether:true, keepWithNext:true, fontPreferences: new ParagraphFontPreferences(),
                     outlineLevel: WdOutlineLevel.wdOutlineLevel1), 0.5f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 0,
                    fontPreferences: new FontPreferences(), trailingCharacter: TrailingCharacter.NONE
                    ),

                    new ParagraphNumberingLevel(2, "%1.%2", WdListNumberStyle.wdListNumberStyleArabic,
                    new ParagraphFormat(0, 8, null, 0, 0f, 0, SpecialParagraphIndentation.HANGING, 0.5f,
                     false, false, false, true, outlineLevel: WdOutlineLevel.wdOutlineLevel2), 0.5f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 0f),

                    new ParagraphNumberingLevel(3, "(%3)", WdListNumberStyle.wdListNumberStyleLowercaseLetter,
                    new ParagraphFormat(0, 8, null, 0, 0.5f, 0, SpecialParagraphIndentation.HANGING, 0.5f,
                     false, false, false, true), 1f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 0.5f),

                    new ParagraphNumberingLevel(4, "(%4)", WdListNumberStyle.wdListNumberStyleLowercaseRoman,
                    new ParagraphFormat(0, 8, null, 0, 1.2f, 0, SpecialParagraphIndentation.HANGING, 0.3f,
                     false, false, false, true), 1.5f, numberAlignment:WdListLevelAlignment.wdListLevelAlignRight, numberIndent: 1.2f),

                    new ParagraphNumberingLevel(5, "(%5)", WdListNumberStyle.wdListNumberStyleUppercaseLetter,
                    new ParagraphFormat(0, 8, null, 0, 1.5f, 0, SpecialParagraphIndentation.HANGING, 0.5f,
                     false, false, false, true), 2f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 1.5f),

                    new ParagraphNumberingLevel(6, "(%6)", WdListNumberStyle.wdListNumberStyleUppercaseRoman,
                    new ParagraphFormat(0, 8, null, 0, 2.2f, 0, SpecialParagraphIndentation.HANGING, 0.3f,
                     false, false, false, true), 2.5f, numberAlignment:WdListLevelAlignment.wdListLevelAlignRight, numberIndent: 2.2f),

                    new ParagraphNumberingLevel(7, "%7)", WdListNumberStyle.wdListNumberStyleLowercaseRoman,
                    new ParagraphFormat(0, 8, null, 0, 2.7f, 0, SpecialParagraphIndentation.HANGING, 0.3f,
                     false, false, false, true), 3f, numberAlignment:WdListLevelAlignment.wdListLevelAlignRight, numberIndent: 2.7f),

                    new ParagraphNumberingLevel(8, "%8.", WdListNumberStyle.wdListNumberStyleLowercaseLetter,
                    new ParagraphFormat(0, 8, null, 0, 3.0f, 0, SpecialParagraphIndentation.HANGING, 0.5f,
                     false, false, false, true), 3.5f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 3.0f),

                    new ParagraphNumberingLevel(9, "%9.", WdListNumberStyle.wdListNumberStyleLowercaseRoman,
                    new ParagraphFormat(0, 8, null, 0, 3.7f, 0, SpecialParagraphIndentation.HANGING, 0.3f,
                     false, false, false, true), 4f, numberAlignment:WdListLevelAlignment.wdListLevelAlignRight, numberIndent: 3.7f)
            }, "");

            schemeArticle.ParagraphNumberingLevels.FirstOrDefault().ParagraphFormat.StyleToFollow = ListTemplateUtil.GetParagraphStyleName(schemeArticle, 2);

            var schemeArticleLeft = new ParagraphNumberingScheme("ArtL", "Article Left Article 1 1.1 (a) (i)", new List<ParagraphNumberingLevel>()
                {
                    new ParagraphNumberingLevel(1, "ARTICLE %1", WdListNumberStyle.wdListNumberStyleArabic,
                    new ParagraphFormat(6, 8, null, 0, 0, 0, SpecialParagraphIndentation.NONE, 0,
                     true, false, false, false, keepLinesTogether:true, keepWithNext:true, fontPreferences: new ParagraphFontPreferences(),
                     outlineLevel: WdOutlineLevel.wdOutlineLevel1), 0.5f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 0,
                    fontPreferences: new FontPreferences(), trailingCharacter: TrailingCharacter.NONE),

                    new ParagraphNumberingLevel(2, "%1.%2", WdListNumberStyle.wdListNumberStyleArabic,
                    new ParagraphFormat(0, 8, null, 0, 0f, 0, SpecialParagraphIndentation.HANGING, 0.5f,
                     true, false, false, false, outlineLevel: WdOutlineLevel.wdOutlineLevel2), 0.5f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 0f),

                    new ParagraphNumberingLevel(3, "(%3)", WdListNumberStyle.wdListNumberStyleLowercaseLetter,
                    new ParagraphFormat(0, 8, null, 0, 0.5f, 0, SpecialParagraphIndentation.HANGING, 0.5f,
                     true, false, false, false, outlineLevel: WdOutlineLevel.wdOutlineLevel3), 1f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 0.5f),

                    new ParagraphNumberingLevel(4, "(%4)", WdListNumberStyle.wdListNumberStyleLowercaseRoman,
                    new ParagraphFormat(0, 8, null, 0, 1.0f, 0, SpecialParagraphIndentation.HANGING, 0.5f,
                     true, false, false, false, outlineLevel: WdOutlineLevel.wdOutlineLevel4), 1.5f, numberAlignment:WdListLevelAlignment.wdListLevelAlignRight, numberIndent: 1.0f),

                    new ParagraphNumberingLevel(5, "(%5)", WdListNumberStyle.wdListNumberStyleUppercaseLetter,
                    new ParagraphFormat(0, 8, null, 0, 1.5f, 0, SpecialParagraphIndentation.HANGING, 0.5f,
                     true, false, false, false), 2f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 1.5f),

                    new ParagraphNumberingLevel(6, "(%6)", WdListNumberStyle.wdListNumberStyleUppercaseRoman,
                    new ParagraphFormat(0, 8, null, 0, 2.0f, 0, SpecialParagraphIndentation.HANGING, 0.5f,
                     true, false, false, false), 2.5f, numberAlignment:WdListLevelAlignment.wdListLevelAlignRight, numberIndent: 2.0f),

                    new ParagraphNumberingLevel(7, "(%7)", WdListNumberStyle.wdListNumberStyleArabic,
                    new ParagraphFormat(0, 8, null, 0, 2.5f, 0, SpecialParagraphIndentation.HANGING, 0.5f,
                     true, false, false, false), 3f, numberAlignment:WdListLevelAlignment.wdListLevelAlignRight, numberIndent: 2.5f),

                    new ParagraphNumberingLevel(8, "%8)", WdListNumberStyle.wdListNumberStyleLowercaseLetter,
                    new ParagraphFormat(0, 8, null, 0, 3.0f, 0, SpecialParagraphIndentation.HANGING, 0.5f,
                     true, false, false, false), 3.5f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 3.0f),

                    new ParagraphNumberingLevel(9, "%9)", WdListNumberStyle.wdListNumberStyleLowercaseRoman,
                    new ParagraphFormat(0, 8, null, 0, 3.5f, 0, SpecialParagraphIndentation.HANGING, 0.5f,
                     true, false, false, false), 4f, numberAlignment:WdListLevelAlignment.wdListLevelAlignRight, numberIndent: 3.5f)
            }, "");

            schemeArticleLeft.ParagraphNumberingLevels.FirstOrDefault().ParagraphFormat.StyleToFollow = ListTemplateUtil.GetParagraphStyleName(schemeArticleLeft, 2);

            var schemeMixedNumberingBullets = new ParagraphNumberingScheme("Mixed", "Mixed numbering bullets", new List<ParagraphNumberingLevel>()
                {
                    new ParagraphNumberingLevel(1, "%1.", WdListNumberStyle.wdListNumberStyleArabic,
                    new ParagraphFormat(0, 8, null, 0, 0, 0, SpecialParagraphIndentation.HANGING, 0.5f,
                     false, false, false, true), 0.5f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 0),

                    new ParagraphNumberingLevel(2, "(%2)", WdListNumberStyle.wdListNumberStyleBullet,
                    new ParagraphFormat(0, 8, null, 0, 0.5f, 0, SpecialParagraphIndentation.HANGING, 0.5f,
                     false, false, false, true), 1f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 0.5f, fontPreferences: bulletFontPreferencesSize12) { BulletCharacter = MsWordConstants.CIRCLE_BULLET_CHAR },

                    new ParagraphNumberingLevel(3, "(%3)", WdListNumberStyle.wdListNumberStyleLowercaseLetter,
                    new ParagraphFormat(0, 8, null, 0, 1f, 0, SpecialParagraphIndentation.HANGING, 0.3f,
                     false, false, false, true), 1.3f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 1f),

                    new ParagraphNumberingLevel(4, "(%4)", WdListNumberStyle.wdListNumberStyleLowercaseRoman,
                    new ParagraphFormat(0, 8, null, 0, 1.5f, 0, SpecialParagraphIndentation.HANGING, 0.5f,
                     false, false, false, true), 2f, numberAlignment:WdListLevelAlignment.wdListLevelAlignRight, numberIndent: 1.5f),

                    new ParagraphNumberingLevel(5, "(%5)", WdListNumberStyle.wdListNumberStyleUppercaseRoman,
                    new ParagraphFormat(0, 8, null, 0, 2.0f, 0, SpecialParagraphIndentation.HANGING, 0.3f,
                     false, false, false, true), 2.5f, numberAlignment:WdListLevelAlignment.wdListLevelAlignRight, numberIndent: 2.0f),

                    new ParagraphNumberingLevel(6, "(%6)", WdListNumberStyle.wdListNumberStyleArabic,
                    new ParagraphFormat(0, 8, null, 0, 2.5f, 0, SpecialParagraphIndentation.HANGING, 0.5f,
                     false, false, false, true), 3f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 2.5f),

                    new ParagraphNumberingLevel(7, "%7)", WdListNumberStyle.wdListNumberStyleLowercaseLetter,
                    new ParagraphFormat(0, 8, null, 0, 3.0f, 0, SpecialParagraphIndentation.HANGING, 0.5f,
                     false, false, false, true), 3.5f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 3f),

                    new ParagraphNumberingLevel(8, "%8)", WdListNumberStyle.wdListNumberStyleLowercaseRoman,
                    new ParagraphFormat(0, 8, null, 0, 3.7f, 0, SpecialParagraphIndentation.HANGING, 0.3f,
                     false, false, false, true), 4f, numberAlignment:WdListLevelAlignment.wdListLevelAlignRight, numberIndent: 3.7f),

                    new ParagraphNumberingLevel(9, "%9)", WdListNumberStyle.wdListNumberStyleArabic,
                    new ParagraphFormat(0, 8, null, 0, 4.0f, 0, SpecialParagraphIndentation.HANGING, 0.5f,
                     false, false, false, true), 4.5f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 4f)
            }, "");

            var schemeReportNumbering = new ParagraphNumberingScheme("Rep", "Report Numbering", new List<ParagraphNumberingLevel>()
                {
                    new ParagraphNumberingLevel(1, "Section %1.", WdListNumberStyle.wdListNumberStyleArabic,
                    new ParagraphFormat(6, 8, null, 0, 0, 0, SpecialParagraphIndentation.NONE, 0,
                     false, true, false, false, keepLinesTogether:true, keepWithNext:true, fontPreferences: new ParagraphFontPreferences(),
                     outlineLevel: WdOutlineLevel.wdOutlineLevel1), 0.5f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 0,
                    fontPreferences: new FontPreferences(), trailingCharacter: TrailingCharacter.NONE),

                    new ParagraphNumberingLevel(2, "%1.%2", WdListNumberStyle.wdListNumberStyleArabic,
                    new ParagraphFormat(0, 8, null, 0, 0f, 0, SpecialParagraphIndentation.HANGING, 0.5f,
                     false, false, false, true, outlineLevel: WdOutlineLevel.wdOutlineLevel2), 0.5f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 0f),

                    new ParagraphNumberingLevel(3, "(%3)", WdListNumberStyle.wdListNumberStyleLowercaseLetter,
                    new ParagraphFormat(0, 8, null, 0, 0.5f, 0, SpecialParagraphIndentation.HANGING, 0.5f,
                     false, false, false, true), 1f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 0.5f),

                    new ParagraphNumberingLevel(4, "(%4)", WdListNumberStyle.wdListNumberStyleLowercaseRoman,
                    new ParagraphFormat(0, 8, null, 0, 1.2f, 0, SpecialParagraphIndentation.HANGING, 0.3f,
                     false, false, false, true), 1.5f, numberAlignment:WdListLevelAlignment.wdListLevelAlignRight, numberIndent: 1.2f),

                    new ParagraphNumberingLevel(5, "(%5)", WdListNumberStyle.wdListNumberStyleUppercaseLetter,
                    new ParagraphFormat(0, 8, null, 0, 1.5f, 0, SpecialParagraphIndentation.HANGING, 0.5f,
                     false, false, false, true), 2f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 1.5f),

                    new ParagraphNumberingLevel(6, "(%6)", WdListNumberStyle.wdListNumberStyleArabic,
                    new ParagraphFormat(0, 8, null, 0, 2.2f, 0, SpecialParagraphIndentation.HANGING, 0.3f,
                     false, false, false, true), 2.5f, numberAlignment:WdListLevelAlignment.wdListLevelAlignRight, numberIndent: 2.2f),

                    new ParagraphNumberingLevel(7, "%7)", WdListNumberStyle.wdListNumberStyleLowercaseLetter,
                    new ParagraphFormat(0, 8, null, 0, 2.7f, 0, SpecialParagraphIndentation.HANGING, 0.3f,
                     false, false, false, true), 3f, numberAlignment:WdListLevelAlignment.wdListLevelAlignRight, numberIndent: 2.7f),

                    new ParagraphNumberingLevel(8, "%8)", WdListNumberStyle.wdListNumberStyleLowercaseRoman,
                    new ParagraphFormat(0, 8, null, 0, 3.0f, 0, SpecialParagraphIndentation.HANGING, 0.5f,
                     false, false, false, true), 3.5f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 3.0f),

                    new ParagraphNumberingLevel(9, "(%9)", WdListNumberStyle.wdListNumberStyleUppercaseLetter,
                    new ParagraphFormat(0, 8, null, 0, 3.7f, 0, SpecialParagraphIndentation.HANGING, 0.3f,
                     false, false, false, true), 4f, numberAlignment:WdListLevelAlignment.wdListLevelAlignRight, numberIndent: 3.7f)
            }, "");

            schemeReportNumbering.ParagraphNumberingLevels.FirstOrDefault().ParagraphFormat.StyleToFollow = ListTemplateUtil.GetParagraphStyleName(schemeReportNumbering, 2);

            var schemeRecitals = new ParagraphNumberingScheme("Recitals", "Recitals", new List<ParagraphNumberingLevel>()
                {
                    new ParagraphNumberingLevel(1, "%1.", WdListNumberStyle.wdListNumberStyleUppercaseLetter,
                    new ParagraphFormat(0, 8, null, 0, 0, 0, SpecialParagraphIndentation.HANGING, 0.5f,
                     true, false, false, false), 0.5f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 0)
            }, "", createContinuationParagraphStyles: false);

            var corporateCategory = new ParagraphNumberingSchemeCategory("Corporate", new ObservableCollection<ParagraphNumberingScheme>()
            {
                schemeArticle,
                schemeArticleLeft,
                schemeShortArg
            });

            var estatesCategory = new ParagraphNumberingSchemeCategory("Estates", new ObservableCollection<ParagraphNumberingScheme>()
            {
                schemeShortArg
            });

            var litigationCategory = new ParagraphNumberingSchemeCategory("Litigation", new ObservableCollection<ParagraphNumberingScheme>()
            {
                schemeShortArg
            });

            var schemeTableGen = new ParagraphNumberingScheme("TTGen", "General Numbering for Tables", new List<ParagraphNumberingLevel>()
                {
                    new ParagraphNumberingLevel(1, "%1.", WdListNumberStyle.wdListNumberStyleArabic,
                    new ParagraphFormat(0, 8, null, 0, 0, 0, SpecialParagraphIndentation.HANGING, 0.25f,
                     true, false, false, false), 0.25f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 0),

                    new ParagraphNumberingLevel(2, "(%2)", WdListNumberStyle.wdListNumberStyleLowercaseLetter,
                    new ParagraphFormat(0, 8, null, 0, 0.25f, 0, SpecialParagraphIndentation.HANGING, 0.25f,
                     true, false, false, false), 0.5f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 0.25f),

                    new ParagraphNumberingLevel(3, "(%3)", WdListNumberStyle.wdListNumberStyleLowercaseRoman,
                    new ParagraphFormat(0, 8, null, 0, 0.5f, 0, SpecialParagraphIndentation.HANGING, 0.25f,
                     true, false, false, false), 0.75f, numberAlignment:WdListLevelAlignment.wdListLevelAlignRight, numberIndent: 0.5f),

                    new ParagraphNumberingLevel(4, "(%4)", WdListNumberStyle.wdListNumberStyleUppercaseLetter,
                    new ParagraphFormat(0, 8, null, 0, 0.75f, 0, SpecialParagraphIndentation.HANGING, 0.25f,
                     true, false, false, false), 1f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 0.75f),

                    new ParagraphNumberingLevel(5, "(%5)", WdListNumberStyle.wdListNumberStyleUppercaseRoman,
                    new ParagraphFormat(0, 8, null, 0, 1f, 0, SpecialParagraphIndentation.HANGING, 0.25f,
                     true, false, false, false), 1.25f, numberAlignment:WdListLevelAlignment.wdListLevelAlignRight, numberIndent: 1f),

                    new ParagraphNumberingLevel(6, "(%6)", WdListNumberStyle.wdListNumberStyleArabic,
                    new ParagraphFormat(0, 8, null, 0, 1.25f, 0, SpecialParagraphIndentation.HANGING, 0.25f,
                     true, false, false, false), 1.5f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 1.25f),

                    new ParagraphNumberingLevel(7, "%7)", WdListNumberStyle.wdListNumberStyleLowercaseLetter,
                    new ParagraphFormat(0, 8, null, 0, 1.5f, 0, SpecialParagraphIndentation.HANGING, 0.25f,
                     true, false, false, false), 1.75f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 1.5f),

                    new ParagraphNumberingLevel(8, "%8)", WdListNumberStyle.wdListNumberStyleLowercaseRoman,
                    new ParagraphFormat(0, 8, null, 0, 1.75f, 0, SpecialParagraphIndentation.HANGING, 0.25f,
                     true, false, false, false), 2f, numberAlignment:WdListLevelAlignment.wdListLevelAlignRight, numberIndent: 1.75f),

                    new ParagraphNumberingLevel(9, "%9)", WdListNumberStyle.wdListNumberStyleArabic,
                    new ParagraphFormat(0, 8, null, 0, 2f, 0, SpecialParagraphIndentation.HANGING, 0.25f,
                     true, false, false, false), 2.25f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 2f)
            }, "", baseStyleName: MsWordConstants.TABLE_TEXT_STYLE_NAME);

            var schemeTableGenNoIndent = new ParagraphNumberingScheme("TTGen3", "General Numbering for Tables (No Indent)", new List<ParagraphNumberingLevel>()
                {
                    new ParagraphNumberingLevel(1, "%1.", WdListNumberStyle.wdListNumberStyleArabic,
                    new ParagraphFormat(0, 8, null, 0, 0, 0, SpecialParagraphIndentation.HANGING, 0.25f,
                     true, false, false, false), 0.25f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 0),

                    new ParagraphNumberingLevel(2, "(%2)", WdListNumberStyle.wdListNumberStyleLowercaseLetter,
                    new ParagraphFormat(0, 8, null, 0, 0, 0, SpecialParagraphIndentation.HANGING, 0.25f,
                     true, false, false, false), 0.25f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 0),

                    new ParagraphNumberingLevel(3, "(%3)", WdListNumberStyle.wdListNumberStyleLowercaseRoman,
                    new ParagraphFormat(0, 8, null, 0, 0, 0, SpecialParagraphIndentation.HANGING, 0.25f,
                     true, false, false, false), 0.25f, numberAlignment:WdListLevelAlignment.wdListLevelAlignRight, numberIndent: 0),

                    new ParagraphNumberingLevel(4, "(%4)", WdListNumberStyle.wdListNumberStyleUppercaseLetter,
                    new ParagraphFormat(0, 8, null, 0, 0, 0, SpecialParagraphIndentation.HANGING, 0.25f,
                     true, false, false, false), 0.25f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 0),

                    new ParagraphNumberingLevel(5, "(%5)", WdListNumberStyle.wdListNumberStyleUppercaseRoman,
                    new ParagraphFormat(0, 8, null, 0, 0, 0, SpecialParagraphIndentation.HANGING, 0.25f,
                     true, false, false, false), 0.25f, numberAlignment:WdListLevelAlignment.wdListLevelAlignRight, numberIndent: 0),

                    new ParagraphNumberingLevel(6, "(%6)", WdListNumberStyle.wdListNumberStyleArabic,
                    new ParagraphFormat(0, 8, null, 0, 0, 0, SpecialParagraphIndentation.HANGING, 0.25f,
                     true, false, false, false), 0.25f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 0),

                    new ParagraphNumberingLevel(7, "%7)", WdListNumberStyle.wdListNumberStyleLowercaseLetter,
                    new ParagraphFormat(0, 8, null, 0, 0, 0, SpecialParagraphIndentation.HANGING, 0.25f,
                     true, false, false, false), 0.25f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 0),

                    new ParagraphNumberingLevel(8, "%8)", WdListNumberStyle.wdListNumberStyleLowercaseRoman,
                    new ParagraphFormat(0, 8, null, 0, 0, 0, SpecialParagraphIndentation.HANGING, 0.25f,
                     true, false, false, false), 0.25f, numberAlignment:WdListLevelAlignment.wdListLevelAlignRight, numberIndent: 0),

                    new ParagraphNumberingLevel(9, "%9)", WdListNumberStyle.wdListNumberStyleArabic,
                    new ParagraphFormat(0, 8, null, 0, 0, 0, SpecialParagraphIndentation.HANGING, 0.25f,
                     true, false, false, false), 0.25f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 0)
            }, "General Numbering to use in a table (tight spacing)", baseStyleName: MsWordConstants.TABLE_TEXT_STYLE_NAME);

            var schemeSimpleBulletsForTables = new ParagraphNumberingScheme("TTBullets", "Simple Bullets for Tables", new List<ParagraphNumberingLevel>()
                {
                    new ParagraphNumberingLevel(1, "%1.", WdListNumberStyle.wdListNumberStyleBullet,
                    new ParagraphFormat(0, 8, null, 0, 0, 0, SpecialParagraphIndentation.HANGING, 0.25f,
                     true, false, false, false), 0.25f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 0, fontPreferences: bulletFontPreferencesSize12) { BulletCharacter = MsWordConstants.CIRCLE_BULLET_CHAR},

                    new ParagraphNumberingLevel(2, "(%2)", WdListNumberStyle.wdListNumberStyleBullet,
                    new ParagraphFormat(0, 8, null, 0, 0.25f, 0, SpecialParagraphIndentation.HANGING, 0.25f,
                     true, false, false, false), 0.5f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 0.25f, fontPreferences: bulletFontPreferencesSize12) { BulletCharacter = MsWordConstants.CIRCLE_BULLET_CHAR},

                    new ParagraphNumberingLevel(3, "(%3)", WdListNumberStyle.wdListNumberStyleBullet,
                    new ParagraphFormat(0, 8, null, 0, 0.5f, 0, SpecialParagraphIndentation.HANGING, 0.25f,
                     true, false, false, false), 0.75f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 0.5f, fontPreferences: bulletFontPreferencesSize12) { BulletCharacter = MsWordConstants.CIRCLE_BULLET_CHAR},

                    new ParagraphNumberingLevel(4, "(%4)", WdListNumberStyle.wdListNumberStyleBullet,
                    new ParagraphFormat(0, 8, null, 0, 0.75f, 0, SpecialParagraphIndentation.HANGING, 0.25f,
                     true, false, false, false), 1f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 0.75f, fontPreferences: bulletFontPreferencesSize12) { BulletCharacter = MsWordConstants.CIRCLE_BULLET_CHAR},

                    new ParagraphNumberingLevel(5, "(%5)", WdListNumberStyle.wdListNumberStyleBullet,
                    new ParagraphFormat(0, 8, null, 0, 1f, 0, SpecialParagraphIndentation.HANGING, 0.25f,
                     true, false, false, false), 1.25f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 1f, fontPreferences: bulletFontPreferencesSize12) { BulletCharacter = MsWordConstants.CIRCLE_BULLET_CHAR},

                    new ParagraphNumberingLevel(6, "(%6)", WdListNumberStyle.wdListNumberStyleBullet,
                    new ParagraphFormat(0, 8, null, 0, 1.25f, 0, SpecialParagraphIndentation.HANGING, 0.25f,
                     true, false, false, false), 1.5f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 1.25f, fontPreferences: bulletFontPreferencesSize12) { BulletCharacter = MsWordConstants.CIRCLE_BULLET_CHAR},

                    new ParagraphNumberingLevel(7, "%7)", WdListNumberStyle.wdListNumberStyleBullet,
                    new ParagraphFormat(0, 8, null, 0, 1.5f, 0, SpecialParagraphIndentation.HANGING, 0.25f,
                     true, false, false, false), 1.75f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 1.5f, fontPreferences: bulletFontPreferencesSize12) { BulletCharacter = MsWordConstants.CIRCLE_BULLET_CHAR},

                    new ParagraphNumberingLevel(8, "%8)", WdListNumberStyle.wdListNumberStyleBullet,
                    new ParagraphFormat(0, 8, null, 0, 1.75f, 0, SpecialParagraphIndentation.HANGING, 0.25f,
                     true, false, false, false), 2f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 1.75f, fontPreferences: bulletFontPreferencesSize12) { BulletCharacter = MsWordConstants.CIRCLE_BULLET_CHAR},

                    new ParagraphNumberingLevel(9, "%9)", WdListNumberStyle.wdListNumberStyleBullet,
                    new ParagraphFormat(0, 8, null, 0, 2f, 0, SpecialParagraphIndentation.HANGING, 0.25f,
                     true, false, false, false), 2.25f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 2f, fontPreferences: bulletFontPreferencesSize12) { BulletCharacter = MsWordConstants.CIRCLE_BULLET_CHAR}
            }, "", baseStyleName: MsWordConstants.TABLE_TEXT_STYLE_NAME);

            var schemeFancyBulletsForTables = new ParagraphNumberingScheme("TTBulletsS", "Fancy Bullets for Tables", new List<ParagraphNumberingLevel>()
                {
                    new ParagraphNumberingLevel(1, "%1.", WdListNumberStyle.wdListNumberStyleBullet,
                    new ParagraphFormat(0, 8, null, 0, 0, 0, SpecialParagraphIndentation.HANGING, 0.25f,
                     true, false, false, false), 0.25f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 0, fontPreferences: bulletFontPreferencesSize12) { BulletCharacter = MsWordConstants.CIRCLE_BULLET_CHAR},

                    new ParagraphNumberingLevel(2, "(%2)", WdListNumberStyle.wdListNumberStyleBullet,
                    new ParagraphFormat(0, 8, null, 0, 0.25f, 0, SpecialParagraphIndentation.HANGING, 0.25f,
                     true, false, false, false), 0.5f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 0.25f, fontPreferences: wingdingsBulletFontPreferences) { BulletCharacter = MsWordConstants.SQUARE_BULLET_CHAR},

                    new ParagraphNumberingLevel(3, "(%3)", WdListNumberStyle.wdListNumberStyleBullet,
                    new ParagraphFormat(0, 8, null, 0, 0.5f, 0, SpecialParagraphIndentation.HANGING, 0.25f,
                     true, false, false, false), 0.75f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 0.5f, fontPreferences: wingdingsBulletFontPreferences) { BulletCharacter = MsWordConstants.ARROW_BULLET_CHAR},

                    new ParagraphNumberingLevel(4, "(%4)", WdListNumberStyle.wdListNumberStyleBullet,
                    new ParagraphFormat(0, 8, null, 0, 0.75f, 0, SpecialParagraphIndentation.HANGING, 0.25f,
                     true, false, false, false), 1f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 0.75f, fontPreferences: wingdingsBulletFontPreferences) { BulletCharacter = MsWordConstants.RHOMBUS_BULLET_CHAR},

                    new ParagraphNumberingLevel(5, "(%5)", WdListNumberStyle.wdListNumberStyleBullet,
                    new ParagraphFormat(0, 8, null, 0, 1f, 0, SpecialParagraphIndentation.HANGING, 0.25f,
                     true, false, false, false), 1.25f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 1f, fontPreferences: bulletFontPreferencesSize12) { BulletCharacter = MsWordConstants.CIRCLE_BULLET_CHAR},

                    new ParagraphNumberingLevel(6, "(%6)", WdListNumberStyle.wdListNumberStyleBullet,
                    new ParagraphFormat(0, 8, null, 0, 1.25f, 0, SpecialParagraphIndentation.HANGING, 0.25f,
                     true, false, false, false), 1.5f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 1.25f, fontPreferences: wingdingsBulletFontPreferences) { BulletCharacter = MsWordConstants.SQUARE_BULLET_CHAR},

                    new ParagraphNumberingLevel(7, "%7)", WdListNumberStyle.wdListNumberStyleBullet,
                    new ParagraphFormat(0, 8, null, 0, 1.5f, 0, SpecialParagraphIndentation.HANGING, 0.25f,
                     true, false, false, false), 1.75f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 1.5f, fontPreferences: wingdingsBulletFontPreferences) { BulletCharacter = MsWordConstants.ARROW_BULLET_CHAR},

                    new ParagraphNumberingLevel(8, "%8)", WdListNumberStyle.wdListNumberStyleBullet,
                    new ParagraphFormat(0, 8, null, 0, 1.75f, 0, SpecialParagraphIndentation.HANGING, 0.25f,
                     true, false, false, false), 2f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 1.75f, fontPreferences: wingdingsBulletFontPreferences) { BulletCharacter = MsWordConstants.RHOMBUS_BULLET_CHAR},

                    new ParagraphNumberingLevel(9, "%9)", WdListNumberStyle.wdListNumberStyleBullet,
                    new ParagraphFormat(0, 8, null, 0, 2f, 0, SpecialParagraphIndentation.HANGING, 0.25f,
                     true, false, false, false), 2.25f, numberAlignment:WdListLevelAlignment.wdListLevelAlignLeft, numberIndent: 2f, fontPreferences: bulletFontPreferencesSize12) { BulletCharacter = MsWordConstants.CIRCLE_BULLET_CHAR}
            }, "", baseStyleName: MsWordConstants.TABLE_TEXT_STYLE_NAME);

            var tablesCategory = new ParagraphNumberingSchemeCategory("Tables", new ObservableCollection<ParagraphNumberingScheme>()
            {
                schemeFancyBulletsForTables,
                schemeTableGen,
                schemeTableGenNoIndent,
                schemeSimpleBulletsForTables
            });

            ActiveDocumentParagraphNumberingSchemes = new ObservableCollection<object>();

            ActiveDocumentParagraphNumberingSchemeType = new ParagraphNumberingSchemeType(LanguageManager.GetTranslation(LanguageConstants.ActiveDocument, "Active Document"), ActiveDocumentParagraphNumberingSchemes);

            FavoritesParagraphNumberingSchemes = new ObservableCollection<object>();

            FavoritesParagraphNumberingSchemeType = new ParagraphNumberingSchemeType(LanguageManager.GetTranslation(LanguageConstants.Favorite, "Favorite"), FavoritesParagraphNumberingSchemes);

            FirmDocumentParagraphNumberingSchemes = new ObservableCollection<object>();

            FirmDocumentParagraphNumberingSchemeType = new ParagraphNumberingSchemeType(LanguageManager.GetTranslation(LanguageConstants.Firm, "Firm"), FirmDocumentParagraphNumberingSchemes);

            BuiltInParagraphNumberingSchemeType = new ParagraphNumberingSchemeType(LanguageManager.GetTranslation(LanguageConstants.Default, "Default"), new List<object>()
                {
                    schemeGen,
                    schemeBullets0ptAfter,
                    schemeBullets12ptAfter,
                    schemeBulletsVarious,
                    corporateCategory,
                    estatesCategory,
                    litigationCategory,
                    schemeMixedNumberingBullets,
                    schemeRecitals,
                    schemeReportNumbering,
                    tablesCategory
                })
            { IsDefault = true };


            SyncedParagraphNumberingSchemeType = new ParagraphNumberingSchemeType(LanguageManager.GetTranslation(LanguageConstants.Personal, "Personal"), new ObservableCollection<object>());

            ParagraphNumberingSchemeTypes = new ObservableCollection<ParagraphNumberingSchemeType>()
            {
                ActiveDocumentParagraphNumberingSchemeType,
                SyncedParagraphNumberingSchemeType,
                FavoritesParagraphNumberingSchemeType,
                FirmDocumentParagraphNumberingSchemeType,
                BuiltInParagraphNumberingSchemeType
            };

            //string jsonRepresentation = JsonConvert.SerializeObject(DefaultParagraphNumberingSchemes, Formatting.Indented);
            //File.WriteAllText(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), "ParagraphNumberingSchemes.json"), jsonRepresentation);

            BuiltInParagraphNumberingSchemes = new HashSet<ParagraphNumberingScheme>(GetAllBuiltInParagraphNumberingSchemes());

            foreach (ParagraphNumberingScheme builtInParagraphNumberingScheme in BuiltInParagraphNumberingSchemes)
            {
                string listTemplateName = ListTemplateUtil.GetListTemplateName(builtInParagraphNumberingScheme);

                ListTemplateNamesToBuiltInParagraphNumberingSchemes.Add(listTemplateName, builtInParagraphNumberingScheme);
            }
        }

        public void LoadSynchedParagraphNumberingSchemes()
        {
            List<ParagraphNumberingScheme> syncedParagraphNumberingSchemes = FetchSyncedParagraphNumberingSchemes();

            SyncedParagraphNumberingSchemeType.ParagraphNumberingSchemesOrCategories.Clear();
            syncedParagraphNumberingSchemes.ForEach(it => SyncedParagraphNumberingSchemeType.ParagraphNumberingSchemesOrCategories.Add(it));
            syncedParagraphNumberingSchemes.ForEach(it => it.TreeStartLocation = TreeViewStartLocation.SYNCED);

            SyncedParagraphNumberingSchemes = new HashSet<ParagraphNumberingScheme>(syncedParagraphNumberingSchemes);

            ListTemplateNamesToSyncedParagraphNumberingSchemes.Clear();

            foreach (ParagraphNumberingScheme synchedParagraphNumberingScheme in syncedParagraphNumberingSchemes)
            {
                string listTemplateName = ListTemplateUtil.GetListTemplateName(synchedParagraphNumberingScheme);

                ListTemplateNamesToSyncedParagraphNumberingSchemes.Add(listTemplateName, synchedParagraphNumberingScheme);
            }
        }

        public List<ParagraphNumberingScheme> GetAllParagraphNumberingSchemes()
        {
            return GetAllParagraphNumberingSchemes(ParagraphNumberingSchemeTypes);
        }

        public List<ParagraphNumberingScheme> GetAllParagraphNumberingSchemes(IList<ParagraphNumberingSchemeType> paragraphNumberingSchemeTypes)
        {
            var numberingSchemes = new List<ParagraphNumberingScheme>();

            foreach (ParagraphNumberingSchemeType schemeType in paragraphNumberingSchemeTypes)
            {
                foreach (object schemeCategoryOrScheme in schemeType.ParagraphNumberingSchemesOrCategories)
                {
                    if (schemeCategoryOrScheme is ParagraphNumberingScheme numberingScheme)
                    {
                        numberingSchemes.Add(numberingScheme);
                    }
                    else if (schemeCategoryOrScheme is ParagraphNumberingSchemeCategory schemeCategory)
                    {
                        numberingSchemes.AddRange(GetAllParagraphNumberingSchemesFromCategory(schemeCategory));
                    }
                }
            }

            return numberingSchemes;
        }

        public List<ParagraphNumberingScheme> GetAllParagraphNumberingSchemesFromCategory(ParagraphNumberingSchemeCategory category)
        {
            List<ParagraphNumberingScheme> result = new List<ParagraphNumberingScheme>();
            foreach(var scheme in category.ParagraphNumberingSchemes)
            {
                result.Add(scheme);
            }

            foreach(var subCategory in category.NestedParagraphNumberingSchemeCategory)
            {
                result.AddRange(GetAllParagraphNumberingSchemesFromCategory(subCategory));
            }

            return result;
        }

        public List<ParagraphNumberingScheme> GetAllParagraphNumberingSchemesInActiveDocument()
        {
            return GetAllParagraphNumberingSchemes(new List<ParagraphNumberingSchemeType> { ActiveDocumentParagraphNumberingSchemeType });
        }

        public List<ParagraphNumberingScheme> GetAllBuiltInParagraphNumberingSchemes()
        {
            return GetAllParagraphNumberingSchemes(new List<ParagraphNumberingSchemeType> { BuiltInParagraphNumberingSchemeType });
        }

        public ParagraphNumberingScheme GetActiveDocumentParagraphNumberingScheme(ListTemplate listTemplate)
        {
            string paragraphNumberingSchemeName = ListTemplateUtil.GetListTemplateNameFormFirstLevel(listTemplate);

            return GetAllParagraphNumberingSchemesInActiveDocument().FirstOrDefault(it => it.Name == paragraphNumberingSchemeName);
        }

        public List<string> GetAllParagraphNumberingSchemeNames()
        {
            return GetAllParagraphNumberingSchemes().Select(it => it.Name).ToList();
        }

        public void RemoveActiveDocumentParagraphNumberingScheme(ParagraphNumberingScheme activeDocumentParagraphNumberingScheme)
        {
            ActiveDocumentParagraphNumberingSchemes.Remove(activeDocumentParagraphNumberingScheme);
        }

        public void SyncWithListTemplatesFromDocument()
        {
            this.ActiveDocumentParagraphNumberingSchemes.Clear();
            List<ListTemplate> documentListTemplates = ListTemplateUtil.GetAllCustomListTemplatesInDocument();

            if (documentListTemplates.Count == 0)
            {
                return;
            }

            /**
             *Remove all of the ParagraphNumberingSchemeInstance from the "Active document" tree branch.
             */
            RemoveActiveDocumentParagraphNumberingSchemes();

            var usedParagraphNumberingSchemeNames = new HashSet<string>();

            foreach (ListTemplate listTemplate in documentListTemplates)
            {
                string prefix = ListTemplateUtil.GetListTemplateNameFormFirstLevel(listTemplate);
                ParagraphNumberingScheme readOnlyParagraphNumberingScheme = GetReadOnlyParagraphNumberingSchemeByListTemplateName(listTemplate.Name);

                ParagraphNumberingScheme documentBasedParagraphNumberingScheme = readOnlyParagraphNumberingScheme != null ? readOnlyParagraphNumberingScheme.Clone() : ParagraphNumberingScheme.CreateNew(prefix, prefix);

                documentBasedParagraphNumberingScheme.BaseStyleName = ListTemplateUtil.GetBaseStyleNameFromListTemplateInDocument(listTemplate, ThisAddIn.Instance.Application.ActiveDocument);
                documentBasedParagraphNumberingScheme.RootStylePrefix = ListTemplateUtil.GetRootStyleNamePrefix(listTemplate.ListLevels[1]?.LinkedStyle);
                documentBasedParagraphNumberingScheme.StyleAliasPrefix = ListTemplateUtil.GetStyleAliasPrefix(listTemplate.ListLevels[1]?.LinkedStyle);
                documentBasedParagraphNumberingScheme.IsAddedToDocument = true;

                //SyncParagraphNumberingLevelsWithListTemplate(documentBasedParagraphNumberingScheme, listTemplate);
                documentBasedParagraphNumberingScheme.ToSync = listTemplate;

                AddCustomParagraphNumberingSchemeToActiveDocumentSchemes(documentBasedParagraphNumberingScheme);

                usedParagraphNumberingSchemeNames.Add(documentBasedParagraphNumberingScheme.Name);
            }

            AddUnusedCustomParagraphNumberingSchemeToActiveDocumentSchemes(usedParagraphNumberingSchemeNames);
        }

        public void SyncWithListTemplatesFromFirmDocument()
        {
            if (ThisAddIn.FirmListDocument == null)
            {
                byte[] bytes = AdminPanelWebApi.GetFirmListsDocument();
                if (bytes != null)
                {
                    var tmpFile = Path.ChangeExtension(Path.GetTempFileName(), "docx");
                    File.WriteAllBytes(tmpFile, bytes);
                    Document activeDocument = Globals.ThisAddIn.Application.ActiveDocument;
                    Document doc = Utils.AddNewDocument(tmpFile);
                    doc.ActiveWindow.Visible = false;
                    ThisAddIn.FirmListDocument = doc;
                    File.Delete(tmpFile);
                    List<ListTemplate> documentListTemplates = ListTemplateUtil.GetAllCustomListTemplatesInDocument(doc);

                    if (documentListTemplates.Count == 0)
                    {
                        return;
                    }

                    foreach (ListTemplate listTemplate in documentListTemplates)
                    {
                        if (!string.IsNullOrEmpty(listTemplate.Name))
                        {
                            string prefix = ListTemplateUtil.GetListTemplateNameFormFirstLevel(listTemplate);
                            ParagraphNumberingScheme documentBasedParagraphNumberingScheme = ParagraphNumberingScheme.CreateNew(prefix, prefix);

                            documentBasedParagraphNumberingScheme.BaseStyleName = ListTemplateUtil.GetBaseStyleNameFromListTemplateInDocument(listTemplate, ThisAddIn.FirmListDocument);
                            documentBasedParagraphNumberingScheme.RootStylePrefix = ListTemplateUtil.GetRootStyleNamePrefix(listTemplate.ListLevels[1]?.LinkedStyle);
                            documentBasedParagraphNumberingScheme.StyleAliasPrefix = ListTemplateUtil.GetStyleAliasPrefix(listTemplate.ListLevels[1]?.LinkedStyle);

                            documentBasedParagraphNumberingScheme.ToSync = listTemplate;
                            //SyncParagraphNumberingLevelsWithListTemplate(documentBasedParagraphNumberingScheme, listTemplate);
                            AddCustomParagraphNumberingSchemeToFirmDocumentSchemes(documentBasedParagraphNumberingScheme);
                        }
                    }

                    ApplyFirmDocumentStructure();
                    RefreshFavortiesParagrapNumberingSchemes();
                    activeDocument.Activate();
                    //doc.Close(WdSaveOptions.wdDoNotSaveChanges);
                }

                var toRemove = ParagraphNumberingSchemeTypes.FirstOrDefault(x => x.IsDefault);
                if (toRemove != null)
                {
                    ParagraphNumberingSchemeTypes.Remove(toRemove);
                }
            }
        }

        private void RefreshFavortiesParagrapNumberingSchemes()
        {
            FavoritesParagraphNumberingSchemes.Clear();
            foreach (object scheme in FirmDocumentParagraphNumberingSchemes)
            {
                if (scheme is ParagraphNumberingScheme)
                {
                    var pnScheme = scheme as ParagraphNumberingScheme;

                    if (FavoriteListNames?.Contains(pnScheme.Name) == true && !FavoritesParagraphNumberingSchemes.Contains(pnScheme))
                    {
                        FavoritesParagraphNumberingSchemes.Add(pnScheme);
                    }
                }
                else if (scheme is ParagraphNumberingSchemeCategory)
                {
                    foreach (ParagraphNumberingScheme resultedScheme in GetFavoriteSchemesFromCategory(scheme as ParagraphNumberingSchemeCategory))
                    {
                        if (!FavoritesParagraphNumberingSchemes.Contains(resultedScheme))
                        {
                            FavoritesParagraphNumberingSchemes.Add(resultedScheme);
                        }
                    }
                }
            }
        }

        public List<ParagraphNumberingScheme> GetFavoriteSchemesFromCategory(ParagraphNumberingSchemeCategory category)
        {
            List<ParagraphNumberingScheme> result = new List<ParagraphNumberingScheme>();

            foreach(ParagraphNumberingScheme scheme in category.ParagraphNumberingSchemes)
            {
                if (FavoriteListNames?.Contains(scheme.Name) == true)
                {
                    result.Add(scheme);
                }
            }

            foreach (ParagraphNumberingSchemeCategory subCategory in category.NestedParagraphNumberingSchemeCategory)
            {
                result.AddRange(GetFavoriteSchemesFromCategory(subCategory));
            }

            return result;
        }

        private void ApplyFirmDocumentStructure()
        {
            JToken structure = AdminPanelWebApi.GetFirmListStructure();

            Dictionary<string, ParagraphNumberingScheme> initial = new Dictionary<string, ParagraphNumberingScheme>();

            foreach (object scheme in FirmDocumentParagraphNumberingSchemes)
            {
                if (scheme is ParagraphNumberingScheme)
                {
                    initial[(scheme as ParagraphNumberingScheme).Name] = (scheme as ParagraphNumberingScheme);
                    ListTemplateNamesToFirmParagraphNumberingSchemes[(scheme as ParagraphNumberingScheme).Name] = (scheme as ParagraphNumberingScheme);
                }
            }

            ObservableCollection<object> categories = new ObservableCollection<object>();

            if (structure != null)
            {
                JArray lists = structure["Lists"] as JArray;
                if (lists != null)
                {
                    foreach (JToken token in lists)
                    {
                        if (token["Name"] != null)
                        {
                            ExtractFirmListScheme(initial, categories, token);
                        }
                        if (token["CategoryName"] != null)
                        {
                            ExtractFirmListCategory(initial, categories, token);
                        }
                    }

                    foreach (string listName in initial.Keys)
                    {
                        categories.Add(initial[listName]);
                    }

                    FirmDocumentParagraphNumberingSchemes = categories;

                    FirmDocumentParagraphNumberingSchemeType.ParagraphNumberingSchemesOrCategories = FirmDocumentParagraphNumberingSchemes;
                }
            }
        }

        private static void ExtractFirmListCategory(Dictionary<string, ParagraphNumberingScheme> initial, ObservableCollection<object> categories, JToken token)
        {
            string categoryName = token["CategoryName"].ToString();

            JArray sublists = token["Lists"] as JArray;
            if (sublists != null)
            {
                ObservableCollection<object> addedSubLists = new ObservableCollection<object>();

                foreach (JToken subList in sublists)
                {
                    if (subList["Name"] != null)
                    {
                        ExtractFirmListScheme(initial, addedSubLists, subList);
                    }
                    if (subList["CategoryName"] != null)
                    {
                        ExtractFirmListCategory(initial, addedSubLists, subList);
                    }
                }

                var schemes = new ObservableCollection<ParagraphNumberingScheme>(addedSubLists.Where(x => x is ParagraphNumberingScheme).Cast<ParagraphNumberingScheme>());
                var subCategories = new ObservableCollection<ParagraphNumberingSchemeCategory>(addedSubLists.Where(x => x is ParagraphNumberingSchemeCategory).Cast<ParagraphNumberingSchemeCategory>());

                categories.Add(new ParagraphNumberingSchemeCategory(categoryName, schemes, subCategories));
            }
        }

        private static void ExtractFirmListScheme(Dictionary<string, ParagraphNumberingScheme> initial, ObservableCollection<object> categories, JToken token)
        {
            ParagraphNumberingScheme scheme = null;
            if (initial.TryGetValue(token["Name"].ToString(), out scheme))
            {
                if (token["DisplayName"] != null)
                {
                    scheme.DisplayName = token["DisplayName"].ToString();
                }

                if (token["Description"] != null)
                {
                    scheme.Description = token["Description"].ToString();
                }

                categories.Add(scheme);
                initial.Remove(scheme.Name);
            }
        }

        private void AddUnusedCustomParagraphNumberingSchemeToActiveDocumentSchemes(HashSet<string> usedParagraphNumberingSchemeNames)
        {
            foreach (ParagraphNumberingScheme paragraphNumberingScheme in GetAllParagraphNumberingSchemes())
            {
                if (usedParagraphNumberingSchemeNames.Contains(paragraphNumberingScheme.Name))
                {
                    continue;
                }

                ListTemplate listTemplate = ListTemplateUtil.GetListTemplateByName(ListTemplateUtil.GetListTemplateName(paragraphNumberingScheme));

                if (listTemplate == null)
                {
                    // No list template was create for this numbering scheme
                    continue;
                }

                if (string.IsNullOrEmpty(listTemplate.ListLevels[1]?.LinkedStyle))
                {
                    continue;
                }

                ParagraphNumberingScheme documentBasedParagraphNumberingScheme = paragraphNumberingScheme.Clone();
                documentBasedParagraphNumberingScheme.IsAddedToDocument = true;
                documentBasedParagraphNumberingScheme.IsApplied = false;

                SyncParagraphNumberingLevelsWithListTemplate(documentBasedParagraphNumberingScheme, listTemplate);

                documentBasedParagraphNumberingScheme.DisplayName = GetCustomParagraphNumberingSchemeDisplayName(paragraphNumberingScheme);
                AddCustomParagraphNumberingSchemeToActiveDocumentSchemes(documentBasedParagraphNumberingScheme);
            }
        }

        public void AddCustomParagraphNumberingSchemeToActiveDocumentSchemes(ParagraphNumberingScheme paragraphNumberingScheme)
        {
            // A scheme that has the same name as the currently selected one
            ParagraphNumberingScheme existingScheme = null;

            foreach (object schemeOrCategory in ActiveDocumentParagraphNumberingSchemeType.ParagraphNumberingSchemesOrCategories)
            {
                if (schemeOrCategory is ParagraphNumberingScheme scheme && scheme.Name == paragraphNumberingScheme.Name)
                {
                    existingScheme = scheme;
                    break;
                }
            }

            if (existingScheme != null)
            {
                ActiveDocumentParagraphNumberingSchemeType.ParagraphNumberingSchemesOrCategories.Remove(existingScheme);
            }

            ActiveDocumentParagraphNumberingSchemeType.ParagraphNumberingSchemesOrCategories.Add(paragraphNumberingScheme);

            paragraphNumberingScheme.DisplayName = GetParagraphNumberingFromFirmListStructure(paragraphNumberingScheme.Name) ?? GetCustomParagraphNumberingSchemeDisplayName(paragraphNumberingScheme);

            // TODO: sort the list by scheme name
        }

        public string GetParagraphNumberingFromFirmListStructure(string name)
        {
            if (FirmListStructure == null)
            {
                FirmListStructure = AdminPanelWebApi.GetFirmListStructure();
            }

            if (FirmListStructure != null)
            {
                JArray lists = FirmListStructure["Lists"] as JArray;
                if (lists != null)
                {
                    foreach (JToken token in lists)
                    {
                        if (token["Name"] != null)
                        {
                            string result = GetParagraphNumberingFromFirmListStructureScheme(name, token);
                            if (result != null)
                            {
                                return result;
                            }
                        }

                        if (token["CategoryName"] != null)
                        {
                            string result = GetParagraphNumberingFromFirmListStructureCategory(name, token);
                            if (result != null)
                            {
                                return result;
                            }
                        }
                    }
                }
            }

            return null;
        }

        public string GetParagraphNumberingFromFirmListStructureScheme(string name, JToken token)
        {
            if (name == token["Name"].ToString())
            {
                if (token["DisplayName"] != null)
                {
                    return token["DisplayName"].ToString();
                }
            }
            return null;
        }

        public string GetParagraphNumberingFromFirmListStructureCategory(string name, JToken token)
        {
            string categoryName = token["CategoryName"].ToString();

            JArray sublists = token["Lists"] as JArray;
            if (sublists != null)
            {
                foreach (JToken subList in sublists)
                {
                    if (subList["Name"] != null)
                    {
                        string result = GetParagraphNumberingFromFirmListStructureScheme(name, subList);
                        if (result != null)
                        {
                            return result;
                        }
                    }

                    if (subList["CategoryName"] != null)
                    {
                        string result = GetParagraphNumberingFromFirmListStructureCategory(name, subList);
                        if (result != null)
                        {
                            return result;
                        }
                    }
                }
            }
            return null;
        }


        public void AddCustomParagraphNumberingSchemeToFirmDocumentSchemes(ParagraphNumberingScheme paragraphNumberingScheme)
        {
            // A scheme that has the same name as the currently selected one
            ParagraphNumberingScheme existingScheme = null;

            foreach (object schemeOrCategory in FirmDocumentParagraphNumberingSchemeType.ParagraphNumberingSchemesOrCategories)
            {
                if (schemeOrCategory is ParagraphNumberingScheme scheme && scheme.Name == paragraphNumberingScheme.Name)
                {
                    existingScheme = scheme;
                    break;
                }
            }

            if (existingScheme != null)
            {
                FirmDocumentParagraphNumberingSchemeType.ParagraphNumberingSchemesOrCategories.Remove(existingScheme);
            }

            FirmDocumentParagraphNumberingSchemeType.ParagraphNumberingSchemesOrCategories.Add(paragraphNumberingScheme);

            // paragraphNumberingScheme.DisplayName = GetCustomParagraphNumberingSchemeDisplayName(paragraphNumberingScheme);
            // TODO: sort the list by scheme name
        }

        public bool IsReadOnlyParagraphNumberingScheme(ParagraphNumberingScheme paragraphNumberingScheme)
        {
            return BuiltInParagraphNumberingSchemes.Contains(paragraphNumberingScheme) ||
                SyncedParagraphNumberingSchemes.Contains(paragraphNumberingScheme);
        }

        public ParagraphNumberingScheme GetActiveDocumentParagraphNumberingSchemeByName(string activeDocumentParagraphNumberingSchemeName)
        {
            foreach (ParagraphNumberingScheme paragraphNumberingScheme in GetAllParagraphNumberingSchemesInActiveDocument())
            {
                if (paragraphNumberingScheme.Name == activeDocumentParagraphNumberingSchemeName)
                {
                    return paragraphNumberingScheme;
                }
            }

            return null;
        }

        public ParagraphNumberingScheme GetBuiltInParagraphNumberingSchemeByName(string builtInParagraphNumberingSchemeName)
        {
            foreach (ParagraphNumberingScheme paragraphNumberingScheme in BuiltInParagraphNumberingSchemes)
            {
                if (paragraphNumberingScheme.Name == builtInParagraphNumberingSchemeName)
                {
                    return paragraphNumberingScheme;
                }
            }

            return null;
        }

        public ParagraphNumberingScheme GetFavoriteParagraphNumberingScheme()
        {
            string favoriteParagraphNumberingSchemeName = ParagraphNumberingSettingRepository.Instance.FavoriteParagraphNumberingSchemeName;
            return GetBuiltInParagraphNumberingSchemeByName(favoriteParagraphNumberingSchemeName);
        }

        public void SetFavoriteParagraphNumberingScheme(ParagraphNumberingScheme selectedParagraphNumberingScheme)
        {
            if (FavoriteListNames == null)
            {
                FavoriteListNames = new List<string>();
            }

            if (!FavoriteListNames.Contains(selectedParagraphNumberingScheme.Name))
            {
                FavoriteListNames.Add(selectedParagraphNumberingScheme.Name);
            }
            SaveFavoriteListsNames();
            RefreshFavortiesParagrapNumberingSchemes();
            selectedParagraphNumberingScheme.IsSchemeFavorite = true;
        }

        public void RemoveFavoriteParagraphNumberingScheme(ParagraphNumberingScheme selectedParagraphNumberingScheme)
        {
            if (FavoriteListNames == null)
            {
                FavoriteListNames = new List<string>();
            }

            if (FavoriteListNames.Contains(selectedParagraphNumberingScheme.Name))
            {
                FavoriteListNames.Remove(selectedParagraphNumberingScheme.Name);
            }
            SaveFavoriteListsNames();
            RefreshFavortiesParagrapNumberingSchemes();
            selectedParagraphNumberingScheme.IsSchemeFavorite = false;
        }

        private void SaveFavoriteListsNames()
        {
            string json = JsonConvert.SerializeObject(FavoriteListNames);

            AdminPanelWebApi.UpdateCustomSetting(Utils.STATIC_PROCESSOR_ID, FAVORITE_LISTS_SETTING_NAME, json);
        }

        public ParagraphNumberingScheme GetFirstBuiltInParagraphNumberingScheme()
        {
            return GetAllBuiltInParagraphNumberingSchemes().FirstOrDefault();
        }

        public ParagraphNumberingScheme GetFirstSyncedParagraphNumberingScheme()
        {
            return (ParagraphNumberingScheme)SyncedParagraphNumberingSchemeType.ParagraphNumberingSchemesOrCategories.FirstOrDefault();
        }

        public ParagraphNumberingScheme GetFirstFirmParagraphNumberingScheme()
        {
            return firstFirmParagraphNumberingScheme;
        }

        public KeyValuePair<List<ParagraphNumberingSchemeCategory>, ParagraphNumberingScheme> GetLastUsedParagraphNumberingScheme()
        {
            string lastUsedParagraphNumberingSchemeName = ParagraphNumberingSettingRepository.Instance.LastUsedParagraphNumberingSchemeName;

            ParagraphNumberingScheme scheme = GetPersonalScheme(lastUsedParagraphNumberingSchemeName);
            if(scheme != null)
            {
                return new KeyValuePair<List<ParagraphNumberingSchemeCategory>, ParagraphNumberingScheme> (null, scheme);
            }

            return GetFirmScheme(lastUsedParagraphNumberingSchemeName);
        }

        private KeyValuePair<List<ParagraphNumberingSchemeCategory>, ParagraphNumberingScheme> GetFirmScheme(string schemeName)
        {
            foreach(object schemeOrCategory in FirmDocumentParagraphNumberingSchemes)
            {
                if (schemeOrCategory is ParagraphNumberingScheme)
                {
                    var scheme = schemeOrCategory as ParagraphNumberingScheme;
                    if (scheme.Name == schemeName)
                    {
                        return new KeyValuePair<List<ParagraphNumberingSchemeCategory>, ParagraphNumberingScheme>(null, scheme);
                    }
                }
                else if (schemeOrCategory is ParagraphNumberingSchemeCategory)
                {
                    var category  = schemeOrCategory as ParagraphNumberingSchemeCategory;
                    var partialResult = GetFirmSchemeFromCategory(category, schemeName);

                    if (partialResult != null)
                    {
                        return new KeyValuePair<List<ParagraphNumberingSchemeCategory>, ParagraphNumberingScheme>(partialResult.Value.Key, partialResult.Value.Value);
                    }
                }
            }

            return new KeyValuePair<List<ParagraphNumberingSchemeCategory>, ParagraphNumberingScheme>(null, null); ;
        }

        private KeyValuePair<List<ParagraphNumberingSchemeCategory>, ParagraphNumberingScheme>? GetFirmSchemeFromCategory(ParagraphNumberingSchemeCategory category, string schemeName)
        {
            foreach(ParagraphNumberingScheme scheme in category.ParagraphNumberingSchemes)
            {
                if (scheme.Name == schemeName)
                {
                    return new KeyValuePair<List<ParagraphNumberingSchemeCategory>, ParagraphNumberingScheme>(new List<ParagraphNumberingSchemeCategory>() { category}, scheme);
                }
            }

            foreach(ParagraphNumberingSchemeCategory subCategory in category.NestedParagraphNumberingSchemeCategory)
            {
                var partialResult = GetFirmSchemeFromCategory(subCategory, schemeName);

                if (partialResult != null)
                {
                    List<ParagraphNumberingSchemeCategory> categories = new List<ParagraphNumberingSchemeCategory>() { category };
                    categories.AddRange(partialResult.Value.Key);

                    return new KeyValuePair<List<ParagraphNumberingSchemeCategory>, ParagraphNumberingScheme>(categories, partialResult.Value.Value);
                }
            }

            return null;
        }

        private ParagraphNumberingScheme GetPersonalScheme(string schemeName)
        {
            foreach(ParagraphNumberingScheme scheme in SyncedParagraphNumberingSchemes)
            {
                if (scheme.Name == schemeName)
                {
                    return scheme;
                }
            }

            return null;
        }

        public void SetLastUsedParagraphNumberingScheme(ParagraphNumberingScheme selectedParagraphNumberingScheme)
        {
            ParagraphNumberingSettingRepository.Instance.LastUsedParagraphNumberingSchemeName = selectedParagraphNumberingScheme.Name;
            ParagraphNumberingSettingRepository.Instance.Store();
        }

        public bool IsStyleAliasPrefixInUse(string styleAliasPrefix)
        {
            foreach (ParagraphNumberingScheme paragraphNumberingScheme in GetAllParagraphNumberingSchemes())
            {
                if (paragraphNumberingScheme.StyleAliasPrefix == styleAliasPrefix)
                {
                    return true;
                }
            }

            return false;
        }

        private void RemoveActiveDocumentParagraphNumberingSchemes()
        {
            ActiveDocumentParagraphNumberingSchemeType.ParagraphNumberingSchemesOrCategories.Clear();
        }

        public static bool SyncParagraphNumberingLevelsWithListTemplate(ParagraphNumberingScheme paragraphNumberingScheme, ListTemplate listTemplate, Document fromDocument = null)
        {
            try
            {
                List<ParagraphNumberingLevel> toDelete = new List<ParagraphNumberingLevel>();
                foreach (ParagraphNumberingLevel numberingLevel in paragraphNumberingScheme.ParagraphNumberingLevels)
                {
                    if (numberingLevel.Index <= listTemplate.ListLevels.Count)
                    {
                        ListLevel listLevel = listTemplate.ListLevels[numberingLevel.Index];
                        if (string.IsNullOrWhiteSpace(listLevel.LinkedStyle))
                        {
                            toDelete.Add(numberingLevel);
                        }
                        else
                        {
                            SyncParagraphNumberingLevelWithListLevel(paragraphNumberingScheme, numberingLevel, listLevel, fromDocument);
                            Marshal.ReleaseComObject(listLevel);
                        }
                    }
                    else
                    {
                        toDelete.Add(numberingLevel);
                    }
                }
                foreach(ParagraphNumberingLevel numberingLevel in toDelete)
                {
                    paragraphNumberingScheme.ParagraphNumberingLevels.Remove(numberingLevel);
                }
                Marshal.ReleaseComObject(listTemplate.ListLevels);
                return true;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
                return false;
            }
        }

        public static void SyncParagraphNumberingLevelWithListLevel(ParagraphNumberingScheme numberingScheme, ParagraphNumberingLevel numberingLevel, ListLevel listLevel, Document fromDocument = null)
        {
            numberingLevel.NumberStyle = listLevel.NumberStyle;

            if (numberingLevel.NumberStyle != WdListNumberStyle.wdListNumberStyleBullet)
            {
                numberingLevel.NumberFormat = listLevel.NumberFormat;
            }
            else
            {
                numberingLevel.BulletCharacter = listLevel.NumberFormat;
                numberingLevel.NumberFormat = $"%{numberingLevel.Index}";
            }

            numberingLevel.TabPosition = app.PointsToInches(listLevel.TabPosition);
            numberingLevel.NumberAlignment = listLevel.Alignment;
            numberingLevel.NumberIndent = app.PointsToInches(listLevel.NumberPosition);

            switch (listLevel.TrailingCharacter)
            {
                case WdTrailingCharacter.wdTrailingTab:
                    numberingLevel.TrailingCharacter = TrailingCharacter.TAB;
                    break;
                case WdTrailingCharacter.wdTrailingSpace:
                    numberingLevel.TrailingCharacter = TrailingCharacter.SPACE;
                    break;
                case WdTrailingCharacter.wdTrailingNone:
                    // Only care about the first list, since there can only be one list with a given list template
                    List listUsingListTemplate = ListTemplateUtil.GetFirstListUsingListTemplate(ListTemplateUtil.GetListTemplateName(numberingScheme), fromDocument);

                    numberingLevel.TrailingCharacter = TrailingCharacter.NONE;

                    if (listUsingListTemplate != null)
                    {
                        if (ListTemplateUtil.IsListUsingNewLine(listUsingListTemplate, numberingLevel.Index))
                        {
                            numberingLevel.TrailingCharacter = TrailingCharacter.NEW_LINE;
                            break;
                        }
                        Marshal.ReleaseComObject(listUsingListTemplate);
                    }
                    break;
            }

            numberingLevel.StartAt = listLevel.StartAt;
            numberingLevel.RestartAfterLevel = listLevel.ResetOnHigher;

            numberingLevel.FontPreferences.Name = listLevel.Font.Name;
            numberingLevel.FontPreferences.Size = (float)listLevel.Font.Size;
            numberingLevel.FontPreferences.IsItalic = listLevel.Font.Italic == -1;
            numberingLevel.FontPreferences.IsBold = listLevel.Font.Bold == -1;
            numberingLevel.FontPreferences.IsAllCaps = listLevel.Font.AllCaps == -1;
            numberingLevel.FontPreferences.UnderlineType = Utils.GetValidUnderlineValue(listLevel.Font.Underline);
            numberingLevel.FontPreferences.HexColor = FontUtil.ConvertWdColorToHex(listLevel.Font.Color);

            numberingLevel.IsBold = listLevel.Font.Bold == -1;
            numberingLevel.IsItalic = listLevel.Font.Italic == -1;
            numberingLevel.IsUnderlined = Utils.GetValidUnderlineValue(listLevel.Font.Underline) != WdUnderline.wdUnderlineNone;

            Marshal.ReleaseComObject(listLevel.Font);

            string linkedStyleName = listLevel.LinkedStyle;

            linkedStyleName = ListTemplateUtil.EnsureFullParagraphStyleName(linkedStyleName, numberingScheme);
            Style paragraphStyle = ListTemplateUtil.GetParagraphStyleByName(linkedStyleName, fromDocument);

            SyncParagraphFormatWithStyle(numberingScheme, numberingLevel.ParagraphFormat, paragraphStyle);
            if (listLevel.Font.Italic == 9999999)
            {
                numberingLevel.FontPreferences.IsItalic = numberingLevel.ParagraphFormat.FontPreferences.IsItalic;
            }
            if (listLevel.Font.Bold == 9999999)
            {
                numberingLevel.FontPreferences.IsBold = numberingLevel.ParagraphFormat.FontPreferences.IsBold;
            }
            if (listLevel.Font.AllCaps == 9999999)
            {
                numberingLevel.FontPreferences.IsAllCaps = numberingLevel.ParagraphFormat.FontPreferences.IsAllCaps;
            }

            if (paragraphStyle != null)
            {
                Marshal.ReleaseComObject(paragraphStyle);
            }

            string continuationStyleName = ListTemplateUtil.GetContinuationParagraphStyleName(numberingScheme, numberingLevel.Index);

            Style continuationStyle = ListTemplateUtil.GetParagraphStyleByName(continuationStyleName, fromDocument);

            if (continuationStyle != null)
            {
                SyncParagraphFormatWithStyle(numberingScheme, numberingLevel.ContinuationParagraphFormat, continuationStyle);
                Marshal.ReleaseComObject(continuationStyle);
            }
        }

        public static void SyncParagraphFormatWithStyle(ParagraphNumberingScheme paragraphNumberingScheme, ParagraphFormat paragraphFormat, Style style)
        {
            if (style != null)
            {
                paragraphFormat.PredefinedStyleName = style.NameLocal;
                paragraphFormat.HideUntilUsed = style.Visibility;

                paragraphFormat.StyleType = style.Type;

                paragraphFormat.FontPreferences.Name = style.Font.Name;
                paragraphFormat.FontPreferences.Size = (float)style.Font.Size;
                paragraphFormat.FontPreferences.IsAllCaps = style.Font.AllCaps == -1;
                paragraphFormat.FontPreferences.IsSmallCaps = style.Font.SmallCaps == -1;
                paragraphFormat.FontPreferences.IsBold = style.Font.Bold == -1;
                paragraphFormat.FontPreferences.IsItalic = style.Font.Italic == -1;
                paragraphFormat.FontPreferences.UnderlineType = Utils.GetValidUnderlineValue(style.Font.Underline);
                paragraphFormat.FontPreferences.HexColor = FontUtil.ConvertWdColorToHex(style.Font.Color);
                Marshal.ReleaseComObject(style.Font);

                paragraphFormat.SpacingBefore = (int)style.ParagraphFormat.SpaceBefore;
                paragraphFormat.SpacingAfter = (int)style.ParagraphFormat.SpaceAfter;
                paragraphFormat.LineSpacingValue = style.ParagraphFormat.LineSpacing;
                paragraphFormat.LineSpacingType = style.ParagraphFormat.LineSpacingRule;

                paragraphFormat.LeftIndent = app.PointsToInches(style.ParagraphFormat.LeftIndent);
                paragraphFormat.RightIndent = app.PointsToInches(style.ParagraphFormat.RightIndent);
                paragraphFormat.SpecialIndentValue = app.PointsToInches(Math.Abs(style.ParagraphFormat.FirstLineIndent));

                if (style.ParagraphFormat.FirstLineIndent == 0)
                {
                    paragraphFormat.IsWrapToNumberIndent = true;
                }
                else if (style.ParagraphFormat.FirstLineIndent < 0)
                {
                    paragraphFormat.IsHangIndent = true;
                    paragraphFormat.LeftIndent -= paragraphFormat.SpecialIndentValue;
                }
                else
                {
                    paragraphFormat.IsWrapToMarginIndent = true;
                }

                paragraphFormat.NoSpaceBetweenParagraphsOfSameStyle = style.NoSpaceBetweenParagraphsOfSameStyle;
                paragraphFormat.OutlineLevel = style.ParagraphFormat.OutlineLevel;
                paragraphFormat.WindowControl = style.ParagraphFormat.WidowControl == -1;
                paragraphFormat.KeepWithNext = style.ParagraphFormat.KeepWithNext == -1;
                paragraphFormat.KeepLinesTogether = style.ParagraphFormat.KeepTogether == -1;
                paragraphFormat.PageBreakBefore = style.ParagraphFormat.PageBreakBefore == -1;

                switch (style.ParagraphFormat.Alignment)
                {
                    case WdParagraphAlignment.wdAlignParagraphLeft:
                        paragraphFormat.IsLeftAligned = true;
                        break;
                    case WdParagraphAlignment.wdAlignParagraphCenter:
                        paragraphFormat.IsCenterAligned = true;
                        break;
                    case WdParagraphAlignment.wdAlignParagraphRight:
                        paragraphFormat.IsRightAligned = true;
                        break;
                    case WdParagraphAlignment.wdAlignParagraphJustify:
                        paragraphFormat.IsJustifiedAligned = true;
                        break;
                }
                Marshal.ReleaseComObject(style.ParagraphFormat);

                paragraphFormat.BordersAreEnabled = style.Borders.Enable != 0;
                if (paragraphFormat.BordersAreEnabled == true)
                {
                    paragraphFormat.BorderHasShadow = style.Borders.Shadow;
                    paragraphFormat.BorderDistanceFromLeft = style.Borders.DistanceFromLeft;
                    paragraphFormat.BorderDistanceFromTop = style.Borders.DistanceFromTop;
                    paragraphFormat.BorderDistanceFromRight = style.Borders.DistanceFromRight;
                    paragraphFormat.BorderDistanceFromBottom = style.Borders.DistanceFromBottom;

                    Border leftBorder = style.Borders[WdBorderType.wdBorderLeft];
                    paragraphFormat.LeftBorderLineStyle = leftBorder.LineStyle;
                    paragraphFormat.LeftBorderLineWidth = leftBorder.LineWidth;
                    paragraphFormat.LeftBorderColor = leftBorder.Color;

                    Border topBorder = style.Borders[WdBorderType.wdBorderTop];
                    paragraphFormat.TopBorderLineStyle = topBorder.LineStyle;
                    paragraphFormat.TopBorderLineWidth = topBorder.LineWidth;
                    paragraphFormat.TopBorderColor = topBorder.Color;

                    Border rightBorder = style.Borders[WdBorderType.wdBorderRight];
                    paragraphFormat.RightBorderLineStyle = rightBorder.LineStyle;
                    paragraphFormat.RightBorderLineWidth = rightBorder.LineWidth;
                    paragraphFormat.RightBorderColor = rightBorder.Color;

                    Border bottomBorder = style.Borders[WdBorderType.wdBorderBottom];
                    paragraphFormat.BottomBorderLineStyle = bottomBorder.LineStyle;
                    paragraphFormat.BottomBorderLineWidth = bottomBorder.LineWidth;
                    paragraphFormat.BottomBorderColor = bottomBorder.Color;

                    Marshal.ReleaseComObject(leftBorder);
                    Marshal.ReleaseComObject(topBorder);
                    Marshal.ReleaseComObject(rightBorder);
                    Marshal.ReleaseComObject(bottomBorder);
                }

                Marshal.ReleaseComObject(style.Borders);
                Shading shading = style.Shading;
                paragraphFormat.BorderShadingTexture = shading.Texture;
                paragraphFormat.BorderShadingBackgroundColor = shading.BackgroundPatternColor;
                paragraphFormat.BorderShadingForegroundColor = shading.ForegroundPatternColor;
                int f = Marshal.ReleaseComObject(shading);

                if (paragraphFormat is ContinuationParagraphFormat continuationParagraphFormat)
                {
                    continuationParagraphFormat.MirrorNumberingParagraphFormat = false;
                }

                Style styleToFollow = (Style)style.get_NextParagraphStyle();

                var styleToFollowName = styleToFollow.NameLocal;
                if (ListTemplateUtil.CheckIfStyleIsContinuationStyle(styleToFollowName))
                {
                    styleToFollowName = ListTemplateUtil.GetParagraphNumberingSchemeContinuationContinuationStyleName(paragraphNumberingScheme, styleToFollow);
                }
                else if (ListTemplateUtil.CheckIfStyleIsParagraphNumberingLevelStyle(styleToFollowName))
                {
                    styleToFollowName = ListTemplateUtil.GetParagraphNumberingSchemeStyleToFollow(paragraphNumberingScheme, styleToFollow);
                }

                paragraphFormat.StyleToFollow = styleToFollowName;
                Marshal.ReleaseComObject(styleToFollow);
            }
        }

        private ParagraphNumberingScheme GetReadOnlyParagraphNumberingSchemeByListTemplateName(string listTemplateName)
        {
            if (ListTemplateNamesToFirmParagraphNumberingSchemes.ContainsKey(listTemplateName))
            {
                return ListTemplateNamesToFirmParagraphNumberingSchemes[listTemplateName];
            }
            else if (ListTemplateNamesToBuiltInParagraphNumberingSchemes.ContainsKey(listTemplateName))
            {
                return ListTemplateNamesToBuiltInParagraphNumberingSchemes[listTemplateName];
            }
            else if (ListTemplateNamesToSyncedParagraphNumberingSchemes.ContainsKey(listTemplateName))
            {
                return ListTemplateNamesToSyncedParagraphNumberingSchemes[listTemplateName];
            }

            return null;
        }

        private string GetCustomParagraphNumberingSchemeDisplayName(ParagraphNumberingScheme builtInParagraphNumberingScheme)
        {
            return builtInParagraphNumberingScheme.DisplayName;
        }

        private List<ParagraphNumberingScheme> FetchSyncedParagraphNumberingSchemes()
        {
            return paragraphNumberingSchemeRepository.RetrieveAll();
        }

        public void RemoveSyncedParagraphNumberingScheme(ParagraphNumberingScheme paragraphNumberingScheme)
        {
            if (SyncedParagraphNumberingSchemeType != null && SyncedParagraphNumberingSchemeType.ParagraphNumberingSchemesOrCategories != null)
            {
                var syncedParagraphNumberingSchemeType = SyncedParagraphNumberingSchemeType.ParagraphNumberingSchemesOrCategories.Where(x => (x as ParagraphNumberingScheme).Name == paragraphNumberingScheme.Name).FirstOrDefault();
                if (syncedParagraphNumberingSchemeType != null)
                {
                    SyncedParagraphNumberingSchemeType.ParagraphNumberingSchemesOrCategories.Remove(syncedParagraphNumberingSchemeType);
                }
            }
        }

        public void RefreshActiveDocumentParagraphNumberingSchemes(ParagraphNumberingScheme paragraphNumberingScheme)
        {
            if (ActiveDocumentParagraphNumberingSchemes != null && ActiveDocumentParagraphNumberingSchemes.Count > 0)
            {
                var sameScheme = ActiveDocumentParagraphNumberingSchemes.FirstOrDefault(x => (x as ParagraphNumberingScheme).Name == paragraphNumberingScheme.Name);
                if (sameScheme != null)
                {
                    ActiveDocumentParagraphNumberingSchemes.Remove(sameScheme);
                }
            }

            ActiveDocumentParagraphNumberingSchemes.Add(paragraphNumberingScheme);
        }
    }
}
