﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using InfowareVSTO.Windows.ParagraphNumbering.Repositories.Interfaces;
using Newtonsoft.Json;

namespace InfowareVSTO.Windows.ParagraphNumbering.Repositories
{
    internal class LocalParagraphNumberingSchemeRepository : IParagraphNumberingSchemeRepository
    {
        private static readonly string directoryPath;

        private const string fileExtension = ".json";

        static LocalParagraphNumberingSchemeRepository()
        {
            directoryPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "InfowareParagraphNumberingSchemes");
        }

        public bool Store(ParagraphNumberingScheme paragraphNumberingScheme)
        {
            string value = JsonConvert.SerializeObject(paragraphNumberingScheme, Formatting.Indented);

            // Create the dir if it doesn't already exist
            Directory.CreateDirectory(directoryPath);

            File.WriteAllText(Path.Combine(directoryPath, paragraphNumberingScheme.DisplayName + fileExtension), value);

            return true;
        }

        public List<ParagraphNumberingScheme> RetrieveAll()
        {
            var paragraphNumberingSchemes = new List<ParagraphNumberingScheme>();

            foreach (string filePath in GetStoredFilePaths())
            {
                string fileContent = File.ReadAllText(filePath);

                ParagraphNumberingScheme numberingScheme = JsonConvert.DeserializeObject<ParagraphNumberingScheme>(fileContent);
                paragraphNumberingSchemes.Add(numberingScheme);
            }

            return paragraphNumberingSchemes;
        }

        private List<string> GetStoredFilePaths()
        {
            return Directory.GetFiles(directoryPath).ToList();
        }
    }
}
