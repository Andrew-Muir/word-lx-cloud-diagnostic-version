﻿using System;
using System.Windows;
using InfowareVSTO.Settings;
using InfowareVSTO.Settings.Interfaces;
using InfowareVSTO.Windows.ParagraphNumbering.Model.Enums;
using InfowareVSTO.Windows.ParagraphNumbering.Repositories.Interfaces;
using Newtonsoft.Json;

namespace InfowareVSTO.Windows.ParagraphNumbering.Repositories
{
    public class ParagraphNumberingSettingRepository : IParagraphNumberingSettingRepository
    {
        private const string TOOLBAR_POSITION_KEY = "toolbar_position";

        private const string RELATIVE_TOOLBAR_POSITION_KEY = "relative_toolbar_position";

        private const string NEVER_OPENED_TOOLBAR_KEY = "never_opened_toolbar";

        private const string SHOULD_REMEBER_TOOLBAR_POSITION_KEY = "remember_toolbar_position";

        private const string SHOW_EXTENDED_TOOLBAR_BUTTONS_KEY = "show_extended_toolbar_buttons";

        private const string SHOULD_APPLY_LEVEL1_STYLE_KEY = "apply_level1_style";

        private const string USE_ACTIVE_DOCUMENT_SCHEMES_FOR_START_LOCATION_KEY = "use_active_document_schemes_for_start_location";

        private const string TREEVIEW_START_LOCATION_KEY = "treeview_start_location";

        private const string FAVORITE_PARAGRAPH_NUMBERING_SCHEME_NAME_KEY = "favorite_paragraph_numbering_scheme_name";

        private const string LAST_USED_PARAGRAPH_NUMBERING_SCHEME_NAME_KEY = "last_used_paragraph_numbering_scheme_name";

        private static readonly ISettingRepository settingRepository;

        public static ParagraphNumberingSettingRepository Instance { get; private set; }

        public bool RememberToolbarPosition { get; set; }

        public bool ShowExtendedToolbarButtons { get; set; }

        public bool DoNotApplyLevel1Style { get; set; }

        public TreeViewStartLocation TreeViewStartLocation { get; set; }

        public bool UseActiveDocumentSchemesForStartLocation { get; set; }

        public Point ToolbarPosition { get; set; }

        public Point RelativeToolbarPosition { get; set; }

        public bool NeverOpenedToolbar { get; set; }

        public string FavoriteParagraphNumberingSchemeName { get; set; }

        public string LastUsedParagraphNumberingSchemeName { get; set; }

        private bool restored = false;

        static ParagraphNumberingSettingRepository()
        {
            settingRepository = new LocalSettingRepository();

            Instance = new ParagraphNumberingSettingRepository();
        }

        public void Restore()
        {
            Setting setting = settingRepository.Retrieve(SHOULD_REMEBER_TOOLBAR_POSITION_KEY);
            RememberToolbarPosition = setting == null ? true : bool.Parse(setting.Value);

            setting = settingRepository.Retrieve(SHOW_EXTENDED_TOOLBAR_BUTTONS_KEY);
            ShowExtendedToolbarButtons = setting == null ? true : bool.Parse(setting.Value);

            setting = settingRepository.Retrieve(SHOULD_APPLY_LEVEL1_STYLE_KEY);
            DoNotApplyLevel1Style = setting == null ? false : !bool.Parse(setting.Value);

            setting = settingRepository.Retrieve(TREEVIEW_START_LOCATION_KEY);
            TreeViewStartLocation = setting == null ? TreeViewStartLocation.FIRM : (TreeViewStartLocation) Enum.Parse(typeof(TreeViewStartLocation), setting.Value.ToString());

            setting = settingRepository.Retrieve(USE_ACTIVE_DOCUMENT_SCHEMES_FOR_START_LOCATION_KEY);
            UseActiveDocumentSchemesForStartLocation = setting == null ? true : bool.Parse(setting.Value);

            setting = settingRepository.Retrieve(TOOLBAR_POSITION_KEY);
            ToolbarPosition = setting == null ? new Point(0, 0) : JsonConvert.DeserializeObject<Point>(setting.Value);

            setting = settingRepository.Retrieve(RELATIVE_TOOLBAR_POSITION_KEY);
            RelativeToolbarPosition = setting == null ? new Point(250, 5) : JsonConvert.DeserializeObject<Point>(setting.Value);

            setting = settingRepository.Retrieve(NEVER_OPENED_TOOLBAR_KEY);
            NeverOpenedToolbar = setting == null ? true : bool.Parse(setting.Value);

            setting = settingRepository.Retrieve(FAVORITE_PARAGRAPH_NUMBERING_SCHEME_NAME_KEY);
            FavoriteParagraphNumberingSchemeName = setting == null ? "Gen" : setting.Value;

            setting = settingRepository.Retrieve(LAST_USED_PARAGRAPH_NUMBERING_SCHEME_NAME_KEY);
            LastUsedParagraphNumberingSchemeName = setting?.Value;

            restored = true;
        }

        public void RestoreIfNeverRestored()
        {
            if (restored)
            {
                return;
            }

            Restore();
        }

        public void Store()
        {
            Store(SHOULD_REMEBER_TOOLBAR_POSITION_KEY, RememberToolbarPosition.ToString());
            Store(SHOW_EXTENDED_TOOLBAR_BUTTONS_KEY, ShowExtendedToolbarButtons.ToString());
            Store(SHOULD_APPLY_LEVEL1_STYLE_KEY, (!DoNotApplyLevel1Style).ToString());
            Store(TREEVIEW_START_LOCATION_KEY, TreeViewStartLocation.ToString());
            Store(USE_ACTIVE_DOCUMENT_SCHEMES_FOR_START_LOCATION_KEY, UseActiveDocumentSchemesForStartLocation.ToString());
            Store(TOOLBAR_POSITION_KEY, JsonConvert.SerializeObject(ToolbarPosition));
            Store(RELATIVE_TOOLBAR_POSITION_KEY, JsonConvert.SerializeObject(RelativeToolbarPosition));
            Store(NEVER_OPENED_TOOLBAR_KEY, NeverOpenedToolbar.ToString());
            Store(FAVORITE_PARAGRAPH_NUMBERING_SCHEME_NAME_KEY, FavoriteParagraphNumberingSchemeName);
            Store(LAST_USED_PARAGRAPH_NUMBERING_SCHEME_NAME_KEY, LastUsedParagraphNumberingSchemeName);
        }

        private void Store(string key, string value)
        {
            settingRepository.Store(new Setting(key, value));
        }
    }
}
