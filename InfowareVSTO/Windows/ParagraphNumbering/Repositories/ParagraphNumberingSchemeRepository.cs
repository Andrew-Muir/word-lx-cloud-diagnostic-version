﻿using System.Collections.Generic;
using System.Net;
using InfowareVSTO.Windows.ParagraphNumbering.Repositories.Interfaces;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace InfowareVSTO.Windows.ParagraphNumbering.Repositories
{
    internal class ParagraphNumberingSchemeRepository : IParagraphNumberingSchemeRepository
    {
        public bool Store(ParagraphNumberingScheme paragraphNumberingScheme)
        {
            string paragraphNumberingSchemeValue = JsonConvert.SerializeObject(paragraphNumberingScheme);
            ApiResponseDTO apiResponse = AdminPanelWebApi.UpdateParagraphNumberingScheme(paragraphNumberingScheme.Name, paragraphNumberingSchemeValue);

            return apiResponse.StatusCode == (int) HttpStatusCode.OK || apiResponse.StatusCode == (int) HttpStatusCode.Created;
        }

        public List<ParagraphNumberingScheme> RetrieveAll()
        {
            ApiResponseDTO apiResponse = AdminPanelWebApi.GetParagraphNumberingSchemes();

            var syncedParagraphNumberingSchemes = new List<ParagraphNumberingScheme>();

            if (apiResponse.StatusCode == (int) HttpStatusCode.MultipleChoices || apiResponse.Data == null)
            {
                return syncedParagraphNumberingSchemes;
            }

            var responseData = (JToken) apiResponse.Data;

            foreach (JProperty entry in responseData)
            {
                ParagraphNumberingScheme syncedParagraphNumberingScheme = null;

                try
                {
                    syncedParagraphNumberingScheme = JsonConvert.DeserializeObject<ParagraphNumberingScheme>(entry.Value.ToString());

                    if (string.IsNullOrEmpty(syncedParagraphNumberingScheme.Name))
                    {
                        /*
                         * It seems that JsonConvert.DeserializeObject() doesn't throw an exception when the input is invalid.
                         * Even if the Name property (or any property) is missing, deserialization will work "fine"
                         */
                        continue;
                    }

                    syncedParagraphNumberingSchemes.Add(syncedParagraphNumberingScheme);
                }
                catch (JsonException)
                {
                    continue;
                }
            }

            return syncedParagraphNumberingSchemes;
        }
    }
}
