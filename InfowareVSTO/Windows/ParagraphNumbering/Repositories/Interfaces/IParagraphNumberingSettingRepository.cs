﻿using System.Windows;
using InfowareVSTO.Windows.ParagraphNumbering.Model.Enums;

namespace InfowareVSTO.Windows.ParagraphNumbering.Repositories.Interfaces
{
    internal interface IParagraphNumberingSettingRepository
    {
        Point ToolbarPosition { get; set; }

        bool RememberToolbarPosition { get; set; }

        bool ShowExtendedToolbarButtons { get; set; }

        bool DoNotApplyLevel1Style { get; set; }

        TreeViewStartLocation TreeViewStartLocation { get; set; }

        bool UseActiveDocumentSchemesForStartLocation { get; set; }

        string FavoriteParagraphNumberingSchemeName { get; set; }

        string LastUsedParagraphNumberingSchemeName { get; set; }

        void Restore();

        void RestoreIfNeverRestored();

        void Store();
    }
}
