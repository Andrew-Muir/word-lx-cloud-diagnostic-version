﻿using System.Collections.Generic;

namespace InfowareVSTO.Windows.ParagraphNumbering.Repositories.Interfaces
{
    internal interface IParagraphNumberingSchemeRepository
    {
        /// <summary>
        /// Stores the specified <see cref="ParagraphNumberingScheme"/> instance
        /// </summary>
        /// <param name="paragraphNumberingScheme">the <see cref="ParagraphNumberingScheme"/> instance to store</param>
        /// <returns>true, if the given <see cref="ParagraphNumberingScheme"/> instance was store successfully</returns>
        bool Store(ParagraphNumberingScheme paragraphNumberingScheme);

        /// <summary>
        /// Returns a <see cref="List"/> of <see cref="ParagraphNumberingScheme"/> instances, previously stored via
        /// the <see cref="Store(ParagraphNumberingScheme)"/> method. The returned <see cref="List"/> should be empty
        /// (not null) if no <see cref="ParagraphNumberingScheme"/> instances were stored
        /// </summary>
        /// <returns>a <see cref="List"/> of <see cref="ParagraphNumberingScheme"/> instances, previously stored via
        /// the <see cref="Store(ParagraphNumberingScheme)"/> method</returns>
        List<ParagraphNumberingScheme> RetrieveAll();
    }
}
