﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using InfowareVSTO;
using InfowareVSTO.Common.Language;
using Newtonsoft.Json;

namespace InfowareVSTO.Windows
{
    /// <summary>
    /// Interaction logic for PushPaperBinSettingsDialog.xaml
    /// </summary>
    public partial class PushPaperBinSettingsDialog : InfowareWindow
    {
        MultiPrintSettings MPSettings { get; set; }
        public PushPaperBinSettingsDialog(MultiPrintSettings MPSettings)
        {
            InitializeComponent();
            PopulateOfficeLocationComboBox();
            this.MPSettings = MPSettings;
        }

        private void PopulateOfficeLocationComboBox()
        {
            var itemSource = Utils.GetLocationsForCombobox();
            itemSource.Insert(0, new KeyValuePair<int, string>(0, LanguageManager.GetTranslation(LanguageConstants.Default, "Default")));
            this.OfficeLocations.ItemsSource = itemSource;
            this.OfficeLocations.SelectedIndex = 0;
        }

        private void OkButton_Click(object sender, RoutedEventArgs e)
        {
            string json =  JsonConvert.SerializeObject(MPSettings);
            AdminPanelWebApi.PushMultiPrintSettingsToCompany(json, (int)OfficeLocations.SelectedValue, Overwrite.IsChecked == true);
            this.Close();
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
