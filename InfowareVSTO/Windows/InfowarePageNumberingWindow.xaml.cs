﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Linq;
using System.Windows;
using InfowareVSTO.Util;
using Microsoft.Office.Interop.Word;
using ContentControl = Microsoft.Office.Interop.Word.ContentControl;
using InfowareVSTO.Common.General;
using InfowareVSTO.MultiLanguage;
using InfowareVSTO.Windows.ParagraphNumbering;
using InfowareVSTO.Common.Language;
using InfowareVSTO.Common;

namespace InfowareVSTO.Windows
{
    /// <summary>
    /// Interaction logic for InfowarePageNumberingWindow.xaml
    /// </summary>
    public partial class InfowarePageNumberingWindow : InfowareWindow
    {
        private static readonly string PAGE_NUMBERING_CONTROL_NAME_TEMPLATE = PAGE_NUMBERING_CONTROL_NAME_TEMPLATE + "_{0}";

        private const char SETTING_SEPARATOR = ';';

        public static List<PageNumberFormat> PageNumberFormats { get; set; }

        private bool wasParagraphNumberingToolbarManagerVisible { get; set; }

        static InfowarePageNumberingWindow()
        {
            var languageId = MLanguageUtil.ActiveDocumentLanguage;
            PageNumberFormats = new List<PageNumberFormat>()
            {
                new PageNumberFormat("1, 2, 3, ..."),
                new PageNumberFormat("- 1 -, - 2 -, - 3 -, ...", prefix: "- ", suffix: " -"),
                new PageNumberFormat("1., 2., 3., ...", suffix: "."),
                new PageNumberFormat("Page x", MLanguageUtil.GetResource(MLanguageResourceEnum.PNPage, languageId) + " "),
                new PageNumberFormat("Page x of y", prefix:  MLanguageUtil.GetResource(MLanguageResourceEnum.PNPage, languageId) + " ", suffix: " " + MLanguageUtil.GetResource(MLanguageResourceEnum.PNOf, languageId), appendTotalPageCount: true),
                new PageNumberFormat("- i -, - ii -, - iii -, ...", prefix: "- ", suffix: " -", format: "roman"),
                new PageNumberFormat("(i), (ii), (iii), ...", prefix: "(", suffix: ")", format: "roman"),
                new PageNumberFormat("i, ii, iii, ...", format: "roman"),
                new PageNumberFormat("I, II, III, ...", format: "ROMAN"),
                new PageNumberFormat("a, b, c, ...", format: "alphabetic"),
                new PageNumberFormat("A, B, C, ...", format: "ALPHABETIC"),
            };
        }

        public InfowarePageNumberingWindow()
        {
            InitializeComponent();

            DataContext = this;

            UpdateWindowTitle();

            InitUi();

            if (ParagraphNumberingToolbarManager.Instance.windowIsShown)
            {
                wasParagraphNumberingToolbarManagerVisible = true;
                ParagraphNumberingToolbarManager.Instance.ToggleVisibility(true);
            }
            else
                wasParagraphNumberingToolbarManagerVisible = false;
        }

        private void UpdateWindowTitle(bool includeIndexOfSelectedSection = true)
        {
            if (includeIndexOfSelectedSection)
            {
                Title = string.Format(LanguageManager.GetTranslation(LanguageConstants.PageNumberingTitleWithIndex, "Page Number: Section {0}"), GetSelectionSection().Index);
            }
            else
            {
                Title = LanguageManager.GetTranslation(LanguageConstants.PageNumberingTitle, "Page Number");
            }
        }

        private void UpdateAlignmentRadioButtonState()
        {
            int alignmentOptionIndex = SettingsManager.GetSettingAsInt("PageNumberAlignment", "Tools", 1);

            if (alignmentOptionIndex == 0)
            {
                RbLeftAlignment.IsChecked = true;
            }
            else if (alignmentOptionIndex == 1)
            {
                RbCenterAlignment.IsChecked = true;
            }
            else if (alignmentOptionIndex == 2)
            {
                RbRightAlignment.IsChecked = true;
            }
            else
            {
                RbCenterAlignment.IsChecked = true;
            }
        }

        private void UpdateNumberingLocationRadioButtonState()
        {
            bool numberingInHeader = SettingsManager.GetSettingAsBool("PageNumberInHeader", "Tools", true);

            RbTopOfPage.IsChecked = numberingInHeader;
            RbBottomOfPage.IsChecked = !numberingInHeader;
        }

        private void InitUi()
        {
            UpdateAlignmentRadioButtonState();
            UpdateNumberingLocationRadioButtonState();

            List<ContentControl> controls = GetSelectionSectionControls();

            if (controls.Count == 0)
            {
                var section = GetSelectionSection();
                bool legacyfound = false;
                WdParagraphAlignment alignment = WdParagraphAlignment.wdAlignParagraphCenter;
                bool positionHeader = true;
                bool firstPage = section.PageSetup.DifferentFirstPageHeaderFooter != -1;

                var header = section.Headers[WdHeaderFooterIndex.wdHeaderFooterFirstPage];
                var pageNumbers = header.Range.Fields.Cast<Field>().Where(x => x.Type == WdFieldType.wdFieldPage || x.Type == WdFieldType.wdFieldNumPages).ToList();
                pageNumbers.AddRange(Utils.GetPageNoTextboxes(header).FirstOrDefault()?.TextFrame.TextRange.Fields.Cast<Field>().Where(x => x.Type == WdFieldType.wdFieldPage || x.Type == WdFieldType.wdFieldNumPages).ToList() ?? new List<Field>());
                foreach (Field pn in pageNumbers)
                {
                    legacyfound = true;
                    alignment = pn.Code.Paragraphs.First.Alignment;
                    firstPage = true;
                    positionHeader = true;
                }

                header = section.Headers[WdHeaderFooterIndex.wdHeaderFooterPrimary];
                pageNumbers = header.Range.Fields.Cast<Field>().Where(x => x.Type == WdFieldType.wdFieldPage || x.Type == WdFieldType.wdFieldNumPages).ToList();
                pageNumbers.AddRange(Utils.GetPageNoTextboxes(header).FirstOrDefault()?.TextFrame.TextRange.Fields.Cast<Field>().Where(x => x.Type == WdFieldType.wdFieldPage || x.Type == WdFieldType.wdFieldNumPages).ToList() ?? new List<Field>());
                foreach (Field pn in pageNumbers)
                {
                    legacyfound = true;
                    alignment = pn.Code.Paragraphs.First.Alignment;
                    positionHeader = true;
                }

                header = section.Footers[WdHeaderFooterIndex.wdHeaderFooterFirstPage];
                pageNumbers = header.Range.Fields.Cast<Field>().Where(x => x.Type == WdFieldType.wdFieldPage || x.Type == WdFieldType.wdFieldNumPages).ToList();
                pageNumbers.AddRange(Utils.GetPageNoTextboxes(header).FirstOrDefault()?.TextFrame.TextRange.Fields.Cast<Field>().Where(x => x.Type == WdFieldType.wdFieldPage || x.Type == WdFieldType.wdFieldNumPages).ToList() ?? new List<Field>());
                foreach (Field pn in pageNumbers)
                {
                    legacyfound = true;
                    alignment = pn.Code.Paragraphs.First.Alignment;
                    firstPage = true;
                    positionHeader = false;
                }

                header = section.Footers[WdHeaderFooterIndex.wdHeaderFooterPrimary];
                pageNumbers = header.Range.Fields.Cast<Field>().Where(x => x.Type == WdFieldType.wdFieldPage || x.Type == WdFieldType.wdFieldNumPages).ToList();
                pageNumbers.AddRange(Utils.GetPageNoTextboxes(header).FirstOrDefault()?.TextFrame.TextRange.Fields.Cast<Field>().Where(x => x.Type == WdFieldType.wdFieldPage || x.Type == WdFieldType.wdFieldNumPages).ToList() ?? new List<Field>());
                foreach (Field pn in pageNumbers)
                {
                    legacyfound = true;
                    alignment = pn.Code.Paragraphs.First.Alignment;
                    positionHeader = false;
                }

                if (legacyfound)
                {
                    InitUILegacy(alignment, positionHeader, firstPage);
                }
            }
            else
            {
                // All controls "should" have matching titles, so picking any will do
                string serializedSettings = controls[0].Title;
                if (!string.IsNullOrEmpty(serializedSettings))
                {
                    SyncUiWithSerializedSettings(serializedSettings);
                }
            }
        }

        private void InitUILegacy(WdParagraphAlignment alignment, bool positionHeader, bool firstPage)
        {
            if (positionHeader)
            {
                this.RbTopOfPage.IsChecked = true;
            }
            else
            {
                this.RbBottomOfPage.IsChecked = true;
            }

            this.CbShowPageNumberOnFirstPage.IsChecked = firstPage;

            switch (alignment)
            {
                case WdParagraphAlignment.wdAlignParagraphLeft:
                case WdParagraphAlignment.wdAlignParagraphJustify:
                    this.RbLeftAlignment.IsChecked = true;
                    break;
                case WdParagraphAlignment.wdAlignParagraphCenter:
                    this.RbCenterAlignment.IsChecked = true;
                    break;
                case WdParagraphAlignment.wdAlignParagraphRight:
                    this.RbRightAlignment.IsChecked = true;
                    break;
            }
        }

        private void OnRemoveButtonClick(object sender, RoutedEventArgs e)
        {
            RemovePageNumbering();

            Close();
        }

        private void OnOkButtonClick(object sender, RoutedEventArgs e)
        {
            InsertPageNumbering();

            Close();
        }

        private void OnCancelButtonClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void RemovePageNumbering()
        {
            using (new DocumentUndoTransaction("Page numbering removal"))
            {
                List<ContentControl> controls;

                List<Section> sections = new List<Section>();

                if ((bool)CbApplyToAllSections.IsChecked)
                {
                    controls = GetAllControls();
                    sections.AddRange(GetDocumentSections().Cast<Section>());
                }
                else
                {
                    controls = GetSelectionSectionControls();
                    sections.Add(GetSelectionSection());
                }

                foreach (ContentControl control in controls)
                {
                    try
                    {
                        Paragraph paragraph = control.Range.Paragraphs[1];
                        control.Delete(true);
                        if (ParagraphIsEmpty(paragraph))
                        {
                            paragraph.Range.Delete();
                        }
                        else
                        {
                            paragraph.Alignment = WdParagraphAlignment.wdAlignParagraphLeft;
                        }

                    }
                    catch (COMException)
                    {
                        /**
                         * For some unknown reason, adding a control to a section, seems to add that control to all sections,
                         * thus, GetAllControls() will return duplicates if there are multiple sections in the document and 
                         * attempting to delete a previously deleted control will throw an exception
                         */
                    }
                }

                //remove legacy page numbers
                ThisAddIn.Instance.Application.ScreenUpdating = false;
                if (ThisAddIn.Instance.IsFunctionForLogging("ScreenUpdating"))
                    Logger.Log("Disabled Screen Updating (InfowarePageNumberingWindow.xaml.cs > RemovePageNumbering)");
                foreach (Section section in sections)
                {
                    //delete all page numbers
                    foreach (HeaderFooter header in section.Headers)
                    {
                        var pageNumbers = header.Range.Fields.Cast<Field>().Where(x => x.Type == WdFieldType.wdFieldPage || x.Type == WdFieldType.wdFieldNumPages).ToList();
                        pageNumbers.AddRange(Utils.GetPageNoTextboxes(header).FirstOrDefault()?.TextFrame.TextRange.Fields.Cast<Field>().Where(x => x.Type == WdFieldType.wdFieldPage || x.Type == WdFieldType.wdFieldNumPages).ToList() ?? new List<Field>());
                        foreach (Field pn in pageNumbers)
                        {
                            try
                            {
                                pn.Code.Paragraphs.First.Range.Delete();
                            }
                            catch { }
                        }
                    }

                    foreach (HeaderFooter footer in section.Footers)
                    {
                        var pageNumbers = footer.Range.Fields.Cast<Field>().Where(x => x.Type == WdFieldType.wdFieldPage || x.Type == WdFieldType.wdFieldNumPages).ToList();
                        pageNumbers.AddRange(Utils.GetPageNoTextboxes(footer).FirstOrDefault()?.TextFrame.TextRange.Fields.Cast<Field>().Where(x => x.Type == WdFieldType.wdFieldPage || x.Type == WdFieldType.wdFieldNumPages).ToList() ?? new List<Field>());
                        foreach (Field pn in pageNumbers)
                        {
                            try
                            {
                                pn.Code.Paragraphs.First.Range.Delete();
                            }
                            catch { }
                        }
                    }
                }
                ThisAddIn.Instance.Application.ScreenUpdating = true;
                if (ThisAddIn.Instance.IsFunctionForLogging("ScreenUpdating"))
                    Logger.Log("Enabled Screen Updating (InfowarePageNumberingWindow.xaml.cs > RemovePageNumbering)");
            }
        }

        private bool ParagraphIsEmpty(Paragraph paragraph)
        {
            return string.IsNullOrWhiteSpace(paragraph?.Range?.Text) && paragraph.Range.ShapeRange.Count == 0;
        }

        private void InsertPageNumbering()
        {
            if (!DocumentUtil.ShowCompatibilityModeErrorPreventive())
            {
                return;
            }

            RemovePageNumbering();

            using (new DocumentUndoTransaction("Page numbering insertion"))
            {
                bool applyToAllSections = (bool)CbApplyToAllSections.IsChecked;

                var destinationHeaderOrFooterList = new List<HeaderFooter>();

                if (applyToAllSections)
                {
                    ThisAddIn.Instance.Application.ScreenUpdating = false;
                    if (ThisAddIn.Instance.IsFunctionForLogging("ScreenUpdating"))
                        Logger.Log("Disabled Screen Updating (InfowarePageNumberingWindow.xaml.cs > InsertPageNumbering)");
                    foreach (Section section in GetDocumentSections())
                    {
                        AddHeaderOrFooterFromSection(destinationHeaderOrFooterList, section);
                    }
                    ThisAddIn.Instance.Application.ScreenUpdating = true;
                    if (ThisAddIn.Instance.IsFunctionForLogging("ScreenUpdating"))
                        Logger.Log("Enabled Screen Updating (InfowarePageNumberingWindow.xaml.cs > InsertPageNumbering)");
                }
                else
                {
                    AddHeaderOrFooterFromSection(destinationHeaderOrFooterList, GetSelectionSection());
                }

                if ((bool)RbStartAt.IsChecked)
                {
                    for (int i = 0; i < destinationHeaderOrFooterList.Count; i++)
                    {
                        HeaderFooter headerOrFooter = destinationHeaderOrFooterList[i];
                        if (headerOrFooter.Exists)
                            if (i == 0)
                            {
                                headerOrFooter.PageNumbers.RestartNumberingAtSection = true;
                                headerOrFooter.PageNumbers.StartingNumber = int.Parse(TbStartPage.Text);
                            }
                    }
                }

                else if ((bool)RbContinueNumberingFromPreviousSection.IsChecked)
                {
                    foreach (HeaderFooter headerOrFooter in destinationHeaderOrFooterList)
                    {
                        if (headerOrFooter.Exists)
                            headerOrFooter.PageNumbers.RestartNumberingAtSection = false;
                    }
                }

                bool leftAlignment = (bool)RbLeftAlignment.IsChecked;
                bool centerAlignment = (bool)RbCenterAlignment.IsChecked;

                var pageNumberFormat = (PageNumberFormat)ComboNumberingFormats.SelectedItem;

                string serializedSettings = GetSerializedSettings();

                foreach (HeaderFooter headerOrFooter in destinationHeaderOrFooterList)
                {
                    if (headerOrFooter.Exists)
                    {
                        Range range = headerOrFooter.Range;
                        range.Collapse(WdCollapseDirection.wdCollapseStart);
                        range.InsertParagraphBefore();
                        range.MoveStart(WdUnits.wdCharacter, 1);
                        range = range.Paragraphs[1].Previous().Range;
                        Microsoft.Office.Tools.Word.RichTextContentControl control = GetVstoDocument().Controls.AddRichTextContentControl(range, GetNewControlName());
                        control.Tag = CustomConstants.PAGE_NUMBERING_TAG;

                        WdParagraphAlignment alignment;

                        if (leftAlignment)
                        {
                            alignment = WdParagraphAlignment.wdAlignParagraphLeft;
                        }
                        else if (centerAlignment)
                        {
                            alignment = WdParagraphAlignment.wdAlignParagraphCenter;
                        }
                        else
                        {
                            alignment = WdParagraphAlignment.wdAlignParagraphRight;
                        }

                        control.Range.ParagraphFormat.Alignment = alignment;
                        control.Title = serializedSettings;

                        object pageField = WdFieldType.wdFieldPage;
                        Field field = control.Range.Fields.Add(control.Range, ref pageField, pageNumberFormat.Format, false);

                        Range prefixRange = control.Range.Duplicate;
                        prefixRange.Collapse(WdCollapseDirection.wdCollapseStart);
                        prefixRange.Text = pageNumberFormat.Prefix;

                        Range suffixRange = control.Range.Duplicate;
                        suffixRange.Collapse(WdCollapseDirection.wdCollapseEnd);
                        suffixRange.Text = pageNumberFormat.Suffix;

                        if (pageNumberFormat.AppendTotalPageCount)
                        {
                            Range endRange = suffixRange.Duplicate;
                            endRange.InsertAfter(" ");
                            endRange.Collapse(WdCollapseDirection.wdCollapseEnd);

                            object totalPageCountField = WdFieldType.wdFieldNumPages;
                            endRange.Fields.Add(endRange, ref totalPageCountField);
                        }
                    }
                }
            }
        }

        private void AddHeaderOrFooterFromSection(List<HeaderFooter> headerOrFooterList, Section section)
        {
            bool topOfPage = (bool)RbTopOfPage.IsChecked;

            bool showPageNumberOnFirstPage = (bool)CbShowPageNumberOnFirstPage.IsChecked;

            if (topOfPage)
            {
                HeaderFooter headerPrimary = section.Headers[WdHeaderFooterIndex.wdHeaderFooterPrimary];
                headerOrFooterList.Add(headerPrimary);
                HeaderFooter headerEvenPages = section.Headers[WdHeaderFooterIndex.wdHeaderFooterEvenPages];
                if (headerEvenPages.Exists)
                {
                    headerOrFooterList.Add(headerEvenPages);
                }
            }
            else
            {
                HeaderFooter footerPrimary = section.Footers[WdHeaderFooterIndex.wdHeaderFooterPrimary];
                headerOrFooterList.Add(footerPrimary);
                HeaderFooter headerEvenPages = section.Footers[WdHeaderFooterIndex.wdHeaderFooterEvenPages];
                if (headerEvenPages.Exists)
                {
                    headerOrFooterList.Add(headerEvenPages);
                }
            }

            bool differentFirstPage = section.PageSetup.DifferentFirstPageHeaderFooter == -1;
            if (showPageNumberOnFirstPage)
            {
                if (topOfPage)
                {
                    HeaderFooter headerFirstPage = section.Headers[WdHeaderFooterIndex.wdHeaderFooterFirstPage];
                    if (headerFirstPage.Exists)
                    {
                        headerOrFooterList.Add(headerFirstPage);
                    }
                }
                else
                {
                    HeaderFooter headerFirstPage = section.Footers[WdHeaderFooterIndex.wdHeaderFooterFirstPage];
                    if (headerFirstPage.Exists)
                    {
                        headerOrFooterList.Add(headerFirstPage);
                    }
                }
            }
            else
            {
                // Use the "FirstPage" header / footer, in which we won't insert any page number related content controls
                section.PageSetup.DifferentFirstPageHeaderFooter = -1;
            }
        }

        private string GetSerializedSettings()
        {
            var settingBuilder = new StringBuilder();

            settingBuilder.Append((bool)RbLeftAlignment.IsChecked ? "left"
                : (bool)RbCenterAlignment.IsChecked ? "centered" : "right");

            settingBuilder.Append(SETTING_SEPARATOR).Append((bool)RbTopOfPage.IsChecked ? "header" : "footer");

            settingBuilder.Append(SETTING_SEPARATOR).Append(ComboNumberingFormats.SelectedIndex);

            //settingBuilder.Append(SETTING_SEPARATOR).Append((bool) CbApplyToAllSections.IsChecked);

            settingBuilder.Append(SETTING_SEPARATOR).Append((bool)CbShowPageNumberOnFirstPage.IsChecked);


            //string numberingOption = null;

            //if ((bool) RbContinueNumberingFromPreviousSection.IsChecked)
            //{
            //    numberingOption = "op1";
            //}
            //else if ((bool) RbStartAt.IsChecked)
            //{
            //    numberingOption = TbStartPage.Text;
            //}

            //if (numberingOption != null)
            //{
            //    settingBuilder.Append(SETTING_SEPARATOR).Append(numberingOption);
            //}

            return settingBuilder.ToString();
        }

        private void SyncUiWithSerializedSettings(string serializedSettings)
        {
            string[] settings = serializedSettings.Split(SETTING_SEPARATOR);

            string alignment = settings[0];

            if (alignment == "left")
            {
                RbLeftAlignment.IsChecked = true;
            }
            else if (alignment == "centered")
            {
                RbCenterAlignment.IsChecked = true;
            }
            else if (alignment == "right")
            {
                RbRightAlignment.IsChecked = true;
            }
            else
            {
                throw new InvalidOperationException($"Unsupported alignment '{alignment}'");
            }

            string position = settings[1];

            if (position == "header")
            {
                RbTopOfPage.IsChecked = true;
            }
            else if (position == "footer")
            {
                RbBottomOfPage.IsChecked = true;
            }
            else
            {
                throw new InvalidOperationException($"Unsupported position '{position}'");
            }

            int numberFormatIndex = int.Parse(settings[2]);
            ComboNumberingFormats.SelectedIndex = numberFormatIndex;

            if (settings.Length == 3)
            {
                // As of writing this, the JS extension stores only the first 3 settings (position, alignment, format)
                return;
            }

            //bool applyToAllSections = bool.Parse(settings[3]);
            //CbApplyToAllSections.IsChecked = applyToAllSections;

            bool showPageNumberOnFirstPage = bool.Parse(settings[3]);
            CbShowPageNumberOnFirstPage.IsChecked = showPageNumberOnFirstPage;


            //if (settings.Length == 4)
            //{
            //    return;
            //}

            //string numberingOption = settings[4];

            //if (numberingOption == "op1")
            //{
            //    RbContinueNumberingFromPreviousSection.IsChecked = true;
            //}
            //else
            //{
            //    RbStartAt.IsChecked = true;
            //    TbStartPage.Text = numberingOption;
            //}
        }

        private List<ContentControl> GetAllControls()
        {
            var controls = new List<ContentControl>();
            ThisAddIn.Instance.Application.ScreenUpdating = false;
            if (ThisAddIn.Instance.IsFunctionForLogging("ScreenUpdating"))
                Logger.Log("Disabled Screen Updating (InfowarePageNumberingWindow.xaml.cs > GetAllControls)");
            foreach (Section section in GetDocumentSections())
            {
                HeaderFooter headerPrimary = section.Headers[WdHeaderFooterIndex.wdHeaderFooterPrimary];
                controls.AddRange(GetControls(headerPrimary));

                HeaderFooter headerFirstPage = section.Headers[WdHeaderFooterIndex.wdHeaderFooterFirstPage];
                if (headerFirstPage.Exists)
                {
                    controls.AddRange(GetControls(headerFirstPage));
                }

                HeaderFooter headerEvenPages = section.Headers[WdHeaderFooterIndex.wdHeaderFooterEvenPages];
                if (headerEvenPages.Exists)
                {
                    controls.AddRange(GetControls(headerEvenPages));
                }

                HeaderFooter footerPrimary = section.Footers[WdHeaderFooterIndex.wdHeaderFooterPrimary];
                controls.AddRange(GetControls(footerPrimary));

                HeaderFooter footerFirstPage = section.Footers[WdHeaderFooterIndex.wdHeaderFooterFirstPage];
                if (footerFirstPage.Exists)
                {
                    controls.AddRange(GetControls(footerFirstPage));
                }

                HeaderFooter footerEvenPages = section.Footers[WdHeaderFooterIndex.wdHeaderFooterEvenPages];
                if (footerEvenPages.Exists)
                {
                    controls.AddRange(GetControls(footerEvenPages));
                }
            }
            ThisAddIn.Instance.Application.ScreenUpdating = true;
            if (ThisAddIn.Instance.IsFunctionForLogging("ScreenUpdating"))
                Logger.Log("Enabled Screen Updating (InfowarePageNumberingWindow.xaml.cs > GetAllControls)");
            return controls.Where(x => x.Tag == CustomConstants.PAGE_NUMBERING_TAG).ToList();
        }

        private List<ContentControl> GetSelectionSectionControls()
        {
            var controls = new List<ContentControl>();

            Section section = GetSelectionSection();

            HeaderFooter headerPrimary = section.Headers[WdHeaderFooterIndex.wdHeaderFooterPrimary];
            controls.AddRange(GetControls(headerPrimary));

            HeaderFooter headerFirstPage = section.Headers[WdHeaderFooterIndex.wdHeaderFooterFirstPage];
            if (headerFirstPage.Exists)
            {
                controls.AddRange(GetControls(headerFirstPage));
            }

            HeaderFooter headerEvenPages = section.Headers[WdHeaderFooterIndex.wdHeaderFooterEvenPages];
            if (headerEvenPages.Exists)
            {
                controls.AddRange(GetControls(headerEvenPages));
            }

            HeaderFooter footerPrimary = section.Footers[WdHeaderFooterIndex.wdHeaderFooterPrimary];
            controls.AddRange(GetControls(footerPrimary));

            HeaderFooter footerFirstPage = section.Footers[WdHeaderFooterIndex.wdHeaderFooterFirstPage];
            if (footerFirstPage.Exists)
            {
                controls.AddRange(GetControls(footerFirstPage));
            }

            HeaderFooter footerEvenPages = section.Footers[WdHeaderFooterIndex.wdHeaderFooterEvenPages];
            if (footerEvenPages.Exists)
            {
                controls.AddRange(GetControls(footerEvenPages));
            }

            return controls.Where(x => x.Tag == CustomConstants.PAGE_NUMBERING_TAG).ToList();
        }

        private List<ContentControl> GetControls(HeaderFooter headerOrFooter)
        {
            var controls = new List<ContentControl>();

            foreach (ContentControl control in headerOrFooter.Range.ContentControls)
            {
                if (control.Tag == CustomConstants.PAGE_NUMBERING_TAG)
                {
                    controls.Add(control);
                }
            }

            return controls;
        }

        private string GetNewControlName()
        {
            return string.Format(PAGE_NUMBERING_CONTROL_NAME_TEMPLATE, Guid.NewGuid());
        }

        private Sections GetDocumentSections()
        {
            return Globals.ThisAddIn.Application.ActiveDocument.Sections;
        }

        private Section GetSelectionSection()
        {
            Section section = Globals.ThisAddIn.Application.Selection.Sections.First;

            // Required when "Apply to all sections" is unchecked (this seems to be done implicitly by the JS extension)
            if (section.Headers[WdHeaderFooterIndex.wdHeaderFooterFirstPage].Exists)
            {
                section.Headers[WdHeaderFooterIndex.wdHeaderFooterFirstPage].LinkToPrevious = false;
            }
            if (section.Headers[WdHeaderFooterIndex.wdHeaderFooterEvenPages].Exists)
            {
                section.Headers[WdHeaderFooterIndex.wdHeaderFooterEvenPages].LinkToPrevious = false;
            }
            section.Headers[WdHeaderFooterIndex.wdHeaderFooterPrimary].LinkToPrevious = false;

            if (section.Footers[WdHeaderFooterIndex.wdHeaderFooterFirstPage].Exists)
            {
                section.Footers[WdHeaderFooterIndex.wdHeaderFooterFirstPage].LinkToPrevious = false;
            }
            if (section.Footers[WdHeaderFooterIndex.wdHeaderFooterEvenPages].Exists)
            {
                section.Footers[WdHeaderFooterIndex.wdHeaderFooterEvenPages].LinkToPrevious = false;
            }
            section.Footers[WdHeaderFooterIndex.wdHeaderFooterPrimary].LinkToPrevious = false;

            return section;
        }

        private Microsoft.Office.Tools.Word.Document GetVstoDocument()
        {
            return Globals.Factory.GetVstoObject(Globals.ThisAddIn.Application.ActiveDocument);
        }

        private void NumericTextBoxValidator(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            var regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void OnCbApplyToAllSectionsClick(object sender, RoutedEventArgs e)
        {
            UpdateWindowTitle(includeIndexOfSelectedSection: !(bool)CbApplyToAllSections.IsChecked);
        }

        public class PageNumberFormat
        {
            public string Representation { get; set; }

            public string Prefix { get; set; }

            public string Suffix { get; set; }

            public string Format { get; set; }

            public bool AppendTotalPageCount { get; set; }

            public PageNumberFormat(string representation, string prefix = "", string suffix = "", string format = "", bool appendTotalPageCount = false)
            {
                Representation = representation;

                Prefix = prefix;

                Suffix = suffix;

                if (format != "")
                {
                    Format = $@"\* {format}";
                }
                else
                {
                    Format = "";
                }

                AppendTotalPageCount = appendTotalPageCount;
            }

            public override string ToString()
            {
                return Representation;
            }
        }

        protected override void OnClosed(System.EventArgs e)
        {
            if (wasParagraphNumberingToolbarManagerVisible)
            {
                ParagraphNumberingToolbarManager.Instance.ToggleVisibility(false);
                ThisAddIn.Instance.UnwantedUnfocus = true;
            }
            base.OnClosed(e);
        }
    }
}
