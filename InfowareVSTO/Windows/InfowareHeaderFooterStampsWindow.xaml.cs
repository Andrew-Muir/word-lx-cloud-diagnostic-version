﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using InfowareVSTO.Common;
using InfowareVSTO.Common.Language;
using InfowareVSTO.Constants;
using Microsoft.Office.Interop.Word;
using ContentControl = Microsoft.Office.Interop.Word.ContentControl;

namespace InfowareVSTO.Windows
{
    /// <summary>
    /// Interaction logic for InfowareHeaderFooterStampsWindow.xaml
    /// </summary>
    public partial class InfowareHeaderFooterStampsWindow : InfowareWindow
    {
        public List<HeaderFooterStamp> HeaderFooterStamps { get; private set; }

        public InfowareHeaderFooterStampsWindow()
        {
            InitializeComponent();

            LoadHeaderFooterStamps();

            DataContext = this;
        }

        public int GetHeaderFooterStampCount()
        {
            return HeaderFooterStamps.Count;
        }

        private void LoadHeaderFooterStamps()
        {
            List<HeaderFooterStamp> headerFooterStamps = GetAllHeaderFooterStamps();

            HeaderFooterStamps = new List<HeaderFooterStamp>(headerFooterStamps);
        }

        private void OnSelectDeselectClick(object sender, RoutedEventArgs e)
        {
            var selectDeselectBtn = (Button)sender;
            if (selectDeselectBtn != null)
            {
                if (CheckAllHeaderFooterStampsStates(true))
                {
                    SetStateForAllListItems(false);
                }
                else if (CheckAllHeaderFooterStampsStates(false))
                {
                    SetStateForAllListItems(true);
                }
                else
                {
                    if (CheckIfAtleastOneHeaderAndFooterStampIsChecked())
                    {
                        SetStateForAllListItems(true);
                    }
                    else
                    {
                        SetStateForAllListItems(false);
                    }
                }
            }
        }

        private bool CheckAllHeaderFooterStampsStates(bool areAllChecked = true)
        {
            if (HeaderFooterStamps != null && HeaderFooterStamps.TrueForAll(x => x.IsSelectedForRemoval == areAllChecked))
            {
                return true;
            }
            return false;
        }

        private bool CheckIfAtleastOneHeaderAndFooterStampIsChecked()
        {
            if (HeaderFooterStamps != null && HeaderFooterStamps.Any(x => x.IsSelectedForRemoval == true))
            {
                return true;
            }
            return false;
        }

        private void SetStateForAllListItems(bool isChecked)
        {
            foreach (HeaderFooterStamp stamp in HeaderFooterStamps)
            {
                stamp.IsSelectedForRemoval = isChecked;
            }
        }

        private void OnRemoveSelectedButtonClick(object sender, RoutedEventArgs e)
        {
            foreach (HeaderFooterStamp stamp in HeaderFooterStamps)
            {
                if (stamp.IsSelectedForRemoval)
                {
                    try
                    {
                        stamp.ContentControl.Delete(true);
                    }
                    catch (System.Exception ex)
                    {
                    }
                }
            }

            Close();
        }

        private void OnCancelButtonClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private List<HeaderFooterStamp> GetAllHeaderFooterStamps()
        {
            var headerFooterStamps = new List<HeaderFooterStamp>();

            int sectionIndex = 0;
            ThisAddIn.Instance.Application.ScreenUpdating = false;
            if (ThisAddIn.Instance.IsFunctionForLogging("ScreenUpdating"))
                Logger.Log("Disabled Screen Updating (InfowareHeaderFooterStampsWindow.xaml.cs > GetAllHeaderFooterStamps)");
            foreach (Section section in GetDocumentSections())
            {
                HeaderFooter headerPrimary = section.Headers[WdHeaderFooterIndex.wdHeaderFooterPrimary];
                AddAllHeaderFooterStamps(headerFooterStamps, sectionIndex, LanguageManager.GetTranslation(LanguageConstants.HeaderPrimary, "Header Primary"), headerPrimary);


                HeaderFooter headerEvenPages = section.Headers[WdHeaderFooterIndex.wdHeaderFooterEvenPages];
                AddAllHeaderFooterStamps(headerFooterStamps, sectionIndex, LanguageManager.GetTranslation(LanguageConstants.HeaderEvenPages, "Header Even Pages"), headerEvenPages);


                HeaderFooter headerFirstPage = section.Headers[WdHeaderFooterIndex.wdHeaderFooterFirstPage];
                AddAllHeaderFooterStamps(headerFooterStamps, sectionIndex, LanguageManager.GetTranslation(LanguageConstants.HeaderFirstPage, "Header First Page"), headerFirstPage);


                HeaderFooter footerPrimary = section.Footers[WdHeaderFooterIndex.wdHeaderFooterPrimary];
                AddAllHeaderFooterStamps(headerFooterStamps, sectionIndex, LanguageManager.GetTranslation(LanguageConstants.FooterPrimary, "Footer Primary"), footerPrimary);


                HeaderFooter footerEvenPages = section.Footers[WdHeaderFooterIndex.wdHeaderFooterEvenPages];
                AddAllHeaderFooterStamps(headerFooterStamps, sectionIndex, LanguageManager.GetTranslation(LanguageConstants.FooterEvenPages, "Footer Even Pages"), footerEvenPages);


                HeaderFooter footerFirstPage = section.Footers[WdHeaderFooterIndex.wdHeaderFooterFirstPage];
                AddAllHeaderFooterStamps(headerFooterStamps, sectionIndex, LanguageManager.GetTranslation(LanguageConstants.FooterFirstPage, "Footer First Page"), footerFirstPage);

                sectionIndex++;
            }
            ThisAddIn.Instance.Application.ScreenUpdating = true;
            if (ThisAddIn.Instance.IsFunctionForLogging("ScreenUpdating"))
                Logger.Log("Enabled Screen Updating (InfowareHeaderFooterStampsWindow.xaml.cs > GetAllHeaderFooterStamps)");

            return headerFooterStamps;
        }

        private void AddAllHeaderFooterStamps(List<HeaderFooterStamp> headerFooterStamps, int sectionIndex,
            string headerOrFooterName, HeaderFooter headerOrFooter)
        {
            foreach (HeaderFooterStamp stamp in GetWatermarkControls(headerOrFooter)
                .Select(control => new HeaderFooterStamp(sectionIndex, headerOrFooterName, "Watermark", control)))
            {
                headerFooterStamps.Add(stamp);
            }

            foreach (HeaderFooterStamp stamp in GetPageNumberingControls(headerOrFooter)
                .Select(control => new HeaderFooterStamp(sectionIndex, headerOrFooterName, "PageNumbering", control)))
            {
                headerFooterStamps.Add(stamp);
            }

            foreach (HeaderFooterStamp stamp in GetDocumentIdControls(headerOrFooter)
                .Select(control => new HeaderFooterStamp(sectionIndex, headerOrFooterName, "DocumentId", control)))
            {
                headerFooterStamps.Add(stamp);
            }

            foreach (HeaderFooterStamp stamp in GetLetterheadControls(headerOrFooter)
                .Select(control => new HeaderFooterStamp(sectionIndex, headerOrFooterName, "Letterhead", control)))
            {
                headerFooterStamps.Add(stamp);
            }

            foreach (HeaderFooterStamp stamp in GetDraftDateStampControls(headerOrFooter)
                .Select(control => new HeaderFooterStamp(sectionIndex, headerOrFooterName, "DraftDateStamp", control)))
            {
                headerFooterStamps.Add(stamp);
            }
        }

        private List<ContentControl> GetWatermarkControls(HeaderFooter headerOrFooter)
        {
            return GetControls(headerOrFooter, HeaderFooterControlTags.WATERMARK);
        }

        private List<ContentControl> GetPageNumberingControls(HeaderFooter headerOrFooter)
        {
            return GetControls(headerOrFooter, HeaderFooterControlTags.PAGE_NUMBERING);
        }

        private List<ContentControl> GetDocumentIdControls(HeaderFooter headerOrFooter)
        {
            return GetControls(headerOrFooter, HeaderFooterControlTags.DOCUMENT_ID);
        }

        private List<ContentControl> GetLetterheadControls(HeaderFooter headerOrFooter)
        {
            return GetControls(headerOrFooter, HeaderFooterControlTags.LETTERHEAD);
        }

        private List<ContentControl> GetDraftDateStampControls(HeaderFooter headerOrFooter)
        {
            return GetControls(headerOrFooter, HeaderFooterControlTags.DRAFT_DATE_STAMP);
        }

        private List<ContentControl> GetControls(HeaderFooter headerOrFooter, string controlTag)
        {
            var controls = new List<ContentControl>();

            foreach (ContentControl control in headerOrFooter.Range.ContentControls)
            {
                if (control.Tag == controlTag)
                {
                    controls.Add(control);
                }
            }

            return controls;
        }

        private Sections GetDocumentSections()
        {
            return Globals.ThisAddIn.Application.ActiveDocument.Sections;
        }

        public class HeaderFooterStamp : INotifyPropertyChanged
        {
            public int SectionIndex { get; }

            public string ContainerName { get; }

            public string StampType { get; }

            private bool isSelectedForRemoval;

            public bool IsSelectedForRemoval
            {
                get => isSelectedForRemoval;

                set
                {
                    isSelectedForRemoval = value;
                    RaisePropertyChanged();
                }
            }

            internal ContentControl ContentControl { get; }

            public event PropertyChangedEventHandler PropertyChanged;

            private void RaisePropertyChanged([CallerMemberName] string propertyName = null)
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }

            public HeaderFooterStamp(int sectionIndex, string containerName, string stampType, ContentControl contentControl)
            {
                SectionIndex = sectionIndex;
                ContainerName = containerName;
                StampType = stampType;
                ContentControl = contentControl;
            }

            public override string ToString()
            {
                return $"S{SectionIndex} - {ContainerName} - {StampType}";
            }
        }
    }
}
