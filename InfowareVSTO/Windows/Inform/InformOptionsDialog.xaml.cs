﻿using InfowareVSTO.Common.DTOs;
using Newtonsoft.Json;
using System;
using System.Windows;

namespace InfowareVSTO.Windows
{
    /// <summary>
    /// Interaction logic for DataSourceWindow.xaml
    /// </summary>
    public partial class InformOptionsDialog : InfowareWindow
    {
        private const string OPTIONS_KEY = "InformOptions";
        public InformOptionsDialog()
        {
            InitializeComponent();
            InformOptions options = GetOptions();
            this.DataContext = options;

            SetRadioButtonsValue(options);
        }

        private void SetRadioButtonsValue(InformOptions options)
        {
            if (options != null)
            {
                BackPageOption bpOption = options.BackPageOption;

                switch (bpOption)
                {
                    case BackPageOption.Append:
                        this.BPAppend.IsChecked = true;
                        break;
                    case BackPageOption.SeparateDoc:
                        this.BPSeparateDoc.IsChecked = true;
                        break;
                    case BackPageOption.Ask:
                        this.BPAsk.IsChecked = true;
                        break;
                    case BackPageOption.NeverInsert:
                        this.BPNeverInsert.IsChecked = true;
                        break;
                }
            }
            else
            {
                this.BPAppend.IsChecked = true;
            }
        }

        public static InformOptions GetOptions()
        {
            string json = RegistryManager.GetStringRegistryValue(OPTIONS_KEY);

            try
            {
                InformOptions result = JsonConvert.DeserializeObject<InformOptions>(json);
                return result ?? new InformOptions();
            }
            catch (Exception ex) 
            {
                return new InformOptions();
            }
        }

        private void OKBtn_Click(object sender, RoutedEventArgs e)
        {
            InformOptions options = this.DataContext as InformOptions;
            GetRadioButtonValues(options);
            this.SaveOptions(options);
            this.Close();
        }

        private void GetRadioButtonValues(InformOptions options)
        {
            if (options != null)
            {
                BackPageOption bpOption = new BackPageOption();
                if (BPAppend.IsChecked == true)
                {
                    bpOption = BackPageOption.Append;
                }
                if (BPSeparateDoc.IsChecked == true)
                {
                    bpOption = BackPageOption.SeparateDoc;
                }
                if (BPAsk.IsChecked == true)
                {
                    bpOption = BackPageOption.Ask;
                }
                if (BPNeverInsert.IsChecked == true)
                {
                    bpOption = BackPageOption.NeverInsert;
                }

                options.BackPageOption = bpOption;
            }
        }

        private void SaveOptions(InformOptions options)
        {
            string json = JsonConvert.SerializeObject(options);
            RegistryManager.SaveToRegistry(OPTIONS_KEY, json);
        }

        private void CancelBtn_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
