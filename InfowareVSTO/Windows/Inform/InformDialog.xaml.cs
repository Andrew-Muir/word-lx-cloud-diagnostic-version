﻿using InfowareVSTO.Common;
using InfowareVSTO.Common.Language;
using InfowareVSTO.Common.Models;
using InfowareVSTO.TaskPaneControls.WordDa;
using InfowareVSTO.Util;
using InfowareVSTO.VSTOAuthor;
using Microsoft.Office.Tools;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Office.Interop.Word;
using InfowareVSTO.Common.DTOs;
using Microsoft.Office.Core;
using InfowareVSTO.MultiLanguage;
using InfowareVSTO.Common.General;
using Section = Microsoft.Office.Interop.Word.Section;
using Style = Microsoft.Office.Interop.Word.Style;
using Application = Microsoft.Office.Interop.Word.Application;
using System.Diagnostics;
using InfowareVSTO.Windows.Inform;
using System.IO;
using Microsoft.Xaml.Behaviors.Core;
using InfowareVSTO.DataSource;

namespace InfowareVSTO.Windows
{
    /// <summary>
    /// Interaction logic for DataSourceWindow.xaml
    /// </summary>
    public partial class InformDialog : InfowareWindow
    {
        private const string SELECTED_FROM_COLLECTION_KEY = "InformSelectedFormCollection";
        public const string INFORM_LAWYER_TAG = "LX-INFORMLAWYER";
        public ObservableCollection<TemplateListModel> selectedForms;
        private readonly Application application;
        private ObservableCollection<TemplateListModel> forms;

        private Dictionary<int, ObservableCollection<TemplateListModel>> allForms = new Dictionary<int, ObservableCollection<TemplateListModel>>();

        public ICollectionView FormsView
        {
            get { return CollectionViewSource.GetDefaultView(this.forms); }
        }

        public static string CurrentFormCollection { get; internal set; }

        public InformDialog()
        {
            InitializeComponent();
            this.FormCollections.ItemsSource = AdminPanelWebApi.GetFormCollectionList();
            this.selectedForms = new ObservableCollection<TemplateListModel>();
            this.SelectedForms.ItemsSource = selectedForms;
            var authors = AdminPanelWebApi.GetAuthors();
            var parsedAuthors = ManageUsers.ParseAuthors(authors);
            if (parsedAuthors != null && parsedAuthors.Count > 0 && parsedAuthors[0].Key == 0)
            {
                parsedAuthors.RemoveAt(0);
            }

            this.AuthorsList.ItemsSource = parsedAuthors;
            int selectedAuthorId = AdminPanelWebApi.GetCurrentAuthorId();
            this.AuthorsList.SelectedValue = selectedAuthorId;
            int? selectedFormCollectionId = RegistryManager.GetIntRegistryValue(SELECTED_FROM_COLLECTION_KEY);
            if (selectedFormCollectionId.HasValue)
            {
                this.FormCollections.SelectedValue = selectedFormCollectionId.Value;
            }
            else if (this.FormCollections.Items.Count > 0)
            {
                this.FormCollections.SelectedIndex = 0;
            }

            this.application = Globals.ThisAddIn.Application;
        }

        private bool Filter(TemplateListModel o)
        {
            string text = this.SearchBox.ShowingPlaceholder ? "" : this.SearchBox.Text;

            string[] strArr = text.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);

            foreach (string str in strArr)
            {
                if (!o.DisplayName.ToLower().Contains(str.ToLower()))
                {
                    return false;
                }
            }

            return true;
        }

        private void OKBtn_Click(object sender, RoutedEventArgs e)
        {
            if (this.SelectedForms.Items != null && this.SelectedForms.Items.Count > 0)
            {
                this.Close();
                foreach (var item in this.SelectedForms.Items)
                {
                    if (item is TemplateListModel)
                    {
                        var template = item as TemplateListModel;

                        var downloadedTemplate = Utils.DownloadAndGetTemplate(template, true, true);
                        if (downloadedTemplate?.Document != null)
                        {
                            bool informLawyerFilled = false;
                            CurrentFormCollection = template.FormCollectioName;

                            informLawyerFilled = ApplySettings(downloadedTemplate.Document, template.FormCollectionId, template.Id, informLawyerFilled);

                            if (MatterMasterPaneUtils.HasMatterMasterMatterLoaded)
                            {
                                informLawyerFilled = MatterMasterPaneUtils.FillInformLawyerFields(downloadedTemplate.Document);
                            }

                            DocumentPropertyUtil.SaveShortProperty(CustomConstants.FROM_INFORM_PROPERTY, "True", true, downloadedTemplate.Document);
                            ThisAddIn.Instance.Ribbon.Invalidate();
                            downloadedTemplate.Document.Activate();
                            var dataFill = new DataFillUserControl(includeInformLawyerTag: !informLawyerFilled);

                            // not sure if this is needed, since we can't fill in the
                            // Form Name in the Back Page until the Back Page has been inserted
                            string formName = template.DisplayName;

                            dataFill.FillTitleOfDocument(formName);

                            dataFill.Selection = ThisAddIn.Instance.Application.Selection.Range;
                            Microsoft.Office.Tools.CustomTaskPane taskPane = TaskPaneUtil2.CreateCustomTaskPane(Ribbon.INFOWARE, dataFill);
                            taskPane.Width = 370;
                            taskPane.Visible = true;
                        }
                    }
                }
            }
        }


        private bool ApplySettings(Document document, int formCollectionId, int formId, bool informLawyerFilled)
        {
            bool result = informLawyerFilled;
            if (document != null)
            {
                //Author and Firm 
                PaneDTO paneDTO = new PaneDTO();
                MsoLanguageID languageID = DocumentUtils.GetDocumentLanguage(document);
                var selectedAuthor = new AuthorWithPathsDTO();
                if (AuthorsList.SelectedItem != null && (int)AuthorsList.SelectedValue > 0)
                    paneDTO.Author = AdminPanelWebApi.GetAuthor((int)AuthorsList.SelectedValue, languageID);
                int currentAssistantId = AdminPanelWebApi.GetCurrentAsistantId();
                if (currentAssistantId > 0)
                {
                    paneDTO.Assistant = AdminPanelWebApi.GetAuthor(currentAssistantId);
                }
                // Law society nr label
                paneDTO.LawSocietyNumberLabel = SettingsManager.GetSetting("LawSocietyNrLabel", "Inform", null, (MsoLanguageID)formCollectionId);
                paneDTO.InformOptions = InformOptionsDialog.GetOptions();

                // Apply default left margin
                // Do this before adding Back Page, or else Back Page will also get this setting
                float defaultLeftMargin = SettingsManager.GetSettingAsFloat("DefaultLeftMargin", "Inform", -1, (MsoLanguageID)formCollectionId);

                if (defaultLeftMargin != -1)
                {
                    ThisAddIn.Instance.Application.ScreenUpdating = false;
                    if (ThisAddIn.Instance.IsFunctionForLogging("ScreenUpdating"))
                        Logger.Log("Disabled Screen Updating (InformDialog.xaml.cs > ApplySettings)");
                    foreach (Section section in document.Sections)
                    {
                        try
                        {
                            section.PageSetup.LeftMargin = this.application.InchesToPoints(defaultLeftMargin);
                        }
                        catch (Exception ex)
                        {
                            Debug.WriteLine(ex);
                        }
                    }
                    ThisAddIn.Instance.Application.ScreenUpdating = true;
                    if (ThisAddIn.Instance.IsFunctionForLogging("ScreenUpdating"))
                        Logger.Log("Enabled Screen Updating (InformDialog.xaml.cs > ApplySettings)");
                }

                byte[] backPageBytes = null;
                int backPageId = AdminPanelWebApi.GetBackPageId(formId);
                if (backPageId > 0)
                {
                    backPageBytes = AdminPanelWebApi.GetInformTemplate(backPageId);
                }

                bool backPageInserted = false;

                if (backPageBytes != null)
                {
                    BackPageOption backPageOption = paneDTO.InformOptions.BackPageOption;

                    if (backPageOption == BackPageOption.Ask)
                    {
                        BackPageConfirmationDialog bpcDialog = new BackPageConfirmationDialog();
                        bpcDialog.ShowDialog();

                        backPageOption = bpcDialog.Result;
                    }

                    string extension = ".docx";
                    if (allForms.TryGetValue(formCollectionId, out var tempForms))
                    {
                        extension = tempForms.Where(x => x.Id == backPageId).FirstOrDefault()?.Extension ?? ".docx";
                    }

                    if (backPageOption == BackPageOption.Append)
                    {
                        InsertBackPage(document, backPageBytes, extension);
                    }

                    if (backPageOption == BackPageOption.SeparateDoc)
                    {
                        InsertSeparateDocBackPage(backPageBytes, extension);
                    }

                    backPageInserted = backPageOption == BackPageOption.Append || backPageOption == BackPageOption.SeparateDoc;
                }

                PaneUtils.InsertGeneralTemplateDTO(document, paneDTO);

                if (!informLawyerFilled && paneDTO.Author != null)
                {
                    result = true;
                    PaneUtils.InsertGeneralTemplateDTO(document, paneDTO, specialTag: INFORM_LAWYER_TAG);
                }

                InfowareVSTO.DataSource.MatterMasterPaneUtils.FillMatterMasterPlaceholders(document, !backPageInserted || paneDTO.InformOptions.UseShortStyleOfCause, paneDTO.InformOptions);

                //Options
                //Admin Panel Settings
                //Base Font
                float baseFontSize = SettingsManager.GetSettingAsFloat("BaseFontSize", "Inform", -1, (MsoLanguageID)formCollectionId);
                string baseFontName = SettingsManager.GetSetting("BaseFontName", "Inform", null, (MsoLanguageID)formCollectionId);
                ApplyBaseFont(document, baseFontSize, baseFontName);

                ApplyOptions(document, paneDTO.InformOptions);
            }
            return result;
        }

        private void InsertSeparateDocBackPage(byte[] backPageBytes, string extension)
        {
            Random rand = new Random();
            int nr = rand.Next() % 100000;

            string name = "Backpage" + nr + extension;
            try
            {
                File.WriteAllBytes(NonWordTemplateManager.TEMP_FOLDER + name, backPageBytes);
                Document document = Utils.AddNewDocument(NonWordTemplateManager.TEMP_FOLDER + name, isVisible: true);
                var dataFill = new DataFillUserControl();
                dataFill.Selection = ThisAddIn.Instance.Application.Selection.Range;

                // Populate Form Name
                string formName = GetFormName(document);
                if (formName != null)
                {
                    dataFill.FillTitleOfDocument(formName);
                }

                // populate lawyer info
                if (MatterMasterPaneUtils.HasMatterMasterMatterLoaded)
                {
                    bool informLawyerFilled = MatterMasterPaneUtils.FillInformLawyerFields(document);
                }

                Microsoft.Office.Tools.CustomTaskPane taskPane = TaskPaneUtil2.CreateCustomTaskPane(Ribbon.INFOWARE, dataFill);
                taskPane.Width = 370;
                taskPane.Visible = true;
            }
            catch { }
        }


        private void InsertBackPage(Document document, byte[] bytes, string extension)
        {
            if (document != null)
            {
                var tmpFile = System.IO.Path.ChangeExtension(System.IO.Path.GetTempFileName(), extension);
                File.WriteAllBytes(tmpFile, bytes);
                Range range = document.Content;

                range.Collapse(WdCollapseDirection.wdCollapseEnd);
                try
                {
                    range.InsertBreak(WdBreakType.wdSectionBreakNextPage);
                }
                catch
                {
                    range.Text = "\r";
                    range.Collapse(WdCollapseDirection.wdCollapseEnd);
                    range.InsertBreak(WdBreakType.wdSectionBreakNextPage);
                }

                Section section = document.Sections.Last;
                section.Footers[WdHeaderFooterIndex.wdHeaderFooterPrimary].PageNumbers.RestartNumberingAtSection = false;
                section.Footers[WdHeaderFooterIndex.wdHeaderFooterPrimary].LinkToPrevious = false;
                section.Footers[WdHeaderFooterIndex.wdHeaderFooterPrimary].Range.Text = "";

                if (section.Footers[WdHeaderFooterIndex.wdHeaderFooterFirstPage].Exists)
                {
                    section.Footers[WdHeaderFooterIndex.wdHeaderFooterFirstPage].PageNumbers.RestartNumberingAtSection = false;
                    section.Footers[WdHeaderFooterIndex.wdHeaderFooterFirstPage].LinkToPrevious = false;
                    section.Footers[WdHeaderFooterIndex.wdHeaderFooterFirstPage].Range.Text = "";
                }

                if (section.Footers[WdHeaderFooterIndex.wdHeaderFooterEvenPages].Exists)
                {
                    section.Footers[WdHeaderFooterIndex.wdHeaderFooterEvenPages].PageNumbers.RestartNumberingAtSection = false;
                    section.Footers[WdHeaderFooterIndex.wdHeaderFooterEvenPages].LinkToPrevious = false;
                    section.Footers[WdHeaderFooterIndex.wdHeaderFooterEvenPages].Range.Text = "";
                }

                section.Headers[WdHeaderFooterIndex.wdHeaderFooterPrimary].PageNumbers.RestartNumberingAtSection = false;
                section.Headers[WdHeaderFooterIndex.wdHeaderFooterPrimary].LinkToPrevious = false;
                section.Headers[WdHeaderFooterIndex.wdHeaderFooterPrimary].Range.Text = "";

                if (section.Headers[WdHeaderFooterIndex.wdHeaderFooterFirstPage].Exists)
                {
                    section.Headers[WdHeaderFooterIndex.wdHeaderFooterFirstPage].PageNumbers.RestartNumberingAtSection = false;
                    section.Headers[WdHeaderFooterIndex.wdHeaderFooterFirstPage].LinkToPrevious = false;
                    section.Headers[WdHeaderFooterIndex.wdHeaderFooterFirstPage].Range.Text = "";
                }

                if (section.Headers[WdHeaderFooterIndex.wdHeaderFooterEvenPages].Exists)
                {
                    section.Headers[WdHeaderFooterIndex.wdHeaderFooterEvenPages].PageNumbers.RestartNumberingAtSection = false;
                    section.Headers[WdHeaderFooterIndex.wdHeaderFooterEvenPages].LinkToPrevious = false;
                    section.Headers[WdHeaderFooterIndex.wdHeaderFooterEvenPages].Range.Text = "";
                }

                range = document.Sections.Last.Range;
                range.Paragraphs.set_Style(Utils.GetLocalizedStyleNameFromBuiltInStyle(WdBuiltinStyle.wdStyleNormal));
                range.Font.Reset();
                range.InsertFile(tmpFile);

                // Populate Form Name
                string formName = GetFormName(document);
                if (formName != null)
                {
                    InsertFormNameIntoBackPage(document, formName);
                }

                // Open Back Page document as hidden, need to open document in order to copy Page Layout properties
                Document doc = application.Documents.Open(FileName: tmpFile, AddToRecentFiles: false, Visible: false);

                // Copy page layout
                Utils.CopySectionPageSetup(doc.Sections.First, section);

                // Close Physical document
                if (!doc.Saved) { doc.Saved = true; }
                doc.Close(WdSaveOptions.wdDoNotSaveChanges);

                try
                {
                    if (File.Exists(tmpFile))
                    {
                        File.Delete(tmpFile);
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                }
            }
        }

        // Helper function to get form name from a court form
        // Based on use of "Form Name" style
        private string GetFormName(Document document)
        {
            Range formNameRange = document.Sections.First.Range;
            foreach (Microsoft.Office.Interop.Word.Paragraph para in formNameRange.Paragraphs)
            {
                Style style = (Microsoft.Office.Interop.Word.Style)para.get_Style();
                if (style.NameLocal == "Form Title")
                {
                    // we've found the Form Name, return it
                    string formName = para.Range.Text.Trim();
                    if (formName != "" && formName != null)
                    {
                        return formName;
                    }
                }
            }

            // we reach here if we didn't find the form name
            return null;
        }

        // Helper function to insert form name in a Back Page
        private void InsertFormNameIntoBackPage(Document document, string formName)
        {
            // Back Page is "last" section of the document, whether
            // it was appended or separate
            Range backPageRange = document.Sections.Last.Range;
            foreach (Microsoft.Office.Interop.Word.Paragraph para in backPageRange.Paragraphs)
            {
                Style style = (Microsoft.Office.Interop.Word.Style)para.get_Style();
                if (style.NameLocal == "Form Title Backer")
                {
                    // remove Prompt style
                    para.Range.Font.Reset();

                    // we've found the placeholder, replace it with the title
                    if (para.Range.Tables.Count > 0)
                    {
                        para.Range.Text = formName;
                    }
                    else
                    {
                        // preserve the trailing white space character
                        para.Range.Text = formName + para.Range.Text.Substring(para.Range.Text.Length);
                    }

                    return;
                }
            }
        }

        private static void ApplyBaseFont(Document document, float baseFontSize, string baseFontName)
        {
            if ((baseFontSize != -1 || baseFontName != null) && document != null)
            {
                Style stlLinked;

                string sOrgFont;
                float sngOrgFontSize;

                bool bFont = false;
                bool bSize = false;

                string sNormalStyle;

                // get current base values

                var normalStyle = document.Styles[WdBuiltinStyle.wdStyleNormal];
                sNormalStyle = normalStyle.NameLocal;

                var normalFont = normalStyle.Font;
                sOrgFont = normalFont.Name;
                sngOrgFontSize = normalFont.Size;

                // check what we need to do
                if (!string.IsNullOrWhiteSpace(baseFontName) && baseFontName.ToUpper() != sOrgFont?.ToUpper())
                    bFont = true;
                if (baseFontSize == -1)
                    bSize = false;
                else if (baseFontSize != sngOrgFontSize)
                    bSize = true;

                if (!bFont && !bSize)
                {
                    return;
                }

                // make the changes to the base styles

                normalFont = document.Styles[WdBuiltinStyle.wdStyleNormal].Font;
                if (bFont)
                    normalFont.Name = baseFontName;
                if (bSize)
                    normalFont.Size = baseFontSize;

                {
                    Font normalSingleFont = null;

                    try
                    {
                        normalSingleFont = document.Styles["Normal Single"].Font;
                    }
                    catch { }

                    if (normalSingleFont != null)
                    {
                        if (bFont)
                            normalSingleFont.Name = baseFontName;
                        if (bSize)
                            normalSingleFont.Size = baseFontSize;
                    }
                };

                // adjust others
                foreach (Style astyle in document.Styles)
                {
                    if (astyle.InUse && (astyle.Type == WdStyleType.wdStyleTypeParagraph || astyle.Type == WdStyleType.wdStyleTypeParagraphOnly || astyle.Type == WdStyleType.wdStyleTypeCharacter))
                    {
                        var font = astyle.Font;
                        if (bFont)
                            font.Name = baseFontName;
                        // change font size if it should match Normal
                        if (bSize)
                        {
                            if (font.Size == sngOrgFontSize)
                                font.Size = baseFontSize;
                        }
                    }
                }

                // adjust numbering
                foreach (ListTemplate oListTemp in document.ListTemplates)
                {
                    if (oListTemp.OutlineNumbered)
                    {
                        foreach (ListLevel oListLev in oListTemp.ListLevels)
                        {
                            stlLinked = null;
                            try
                            {
                                stlLinked = document.Styles[oListLev.LinkedStyle];
                            }
                            catch { }
                            if (stlLinked != null)
                            {
                                if (oListLev.Font.Name != stlLinked.Font.Name)
                                    oListLev.Font.Name = stlLinked.Font.Name;
                                if (oListLev.Font.Size != stlLinked.Font.Size)
                                    oListLev.Font.Size = stlLinked.Font.Size;
                            }
                        }
                    }
                }
            }
        }

        private void ApplyOptions(Document document, InformOptions options)
        {
            //CompactSpacingLSOC 
            //AlwaysUseRealDouble
            //AlwaysSetView100 
            document.Activate();
            if (options.AlwaysSetView100)
            {
                document.ActiveWindow.View.Zoom.Percentage = 100;
            }
            else
            {
                document.ActiveWindow.View.Zoom.PageFit = WdPageFit.wdPageFitBestFit;
            }

            List<Style> styles = new List<Style>(document.Styles.Cast<Style>());
            foreach (Style style in styles)
            {
                if (style.Type == WdStyleType.wdStyleTypeCharacter || style.Type == WdStyleType.wdStyleTypeLinked || style.Type == WdStyleType.wdStyleTypeParagraph || style.Type == WdStyleType.wdStyleTypeParagraphOnly)
                {
                    if (options.CompactSpacingLSOC)
                    {
                        if (style.NameLocal == "SOCAnd")
                        {
                            style.ParagraphFormat.SpaceAfter = 12;
                        }
                        if (style.NameLocal == "SOCP1Title" || style.NameLocal == "SOCP2Title")
                        {
                            style.ParagraphFormat.SpaceAfter = 0;
                        }
                    }

                    if (style.NameLocal == "DoubleSpace" || (style.get_BaseStyle() as Style)?.NameLocal == "DoubleSpace")
                    {
                        if (options.AlwaysUseRealDouble)
                        {
                            style.ParagraphFormat.LineSpacingRule = WdLineSpacing.wdLineSpaceDouble;
                        }
                        else
                        {
                            style.ParagraphFormat.LineSpacingRule = WdLineSpacing.wdLineSpace1pt5;
                        }
                    }
                }
            }
        }

        private void CancelBtn_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void ButtonAdd_Click(object sender, RoutedEventArgs e)
        {
            if (this.Forms.SelectedItems != null && this.Forms.SelectedItems.Count > 0)
            {
                foreach (var item in this.Forms.SelectedItems)
                {
                    if (item is TemplateListModel)
                    {
                        var template = item as TemplateListModel;
                        template.FormCollectioName = this.FormCollections.Text;

                        if (selectedForms.Where(x => x.Id == template.Id).FirstOrDefault() == null)
                        {
                            selectedForms.Add(template);
                        }
                    }
                }
            }
        }

        private void ButtonRemove_Click(object sender, RoutedEventArgs e)
        {
            List<TemplateListModel> toDelete = new List<TemplateListModel>();
            if (this.SelectedForms.SelectedItems != null && this.SelectedForms.SelectedItems.Count > 0)
            {
                foreach (var item in this.SelectedForms.SelectedItems)
                {
                    if (item is TemplateListModel)
                    {
                        var template = item as TemplateListModel;
                        toDelete.Add(template);
                    }
                }

                foreach (var item in toDelete)
                {
                    selectedForms.Remove(item);
                }
            }
        }

        private void ButtonRemoveAll_Click(object sender, RoutedEventArgs e)
        {
            InfowareConfirmationWindow icw = new InfowareConfirmationWindow(
                LanguageManager.GetTranslation(LanguageConstants.AreYouSureRemoveAllForms, "Are you sure you want to remove all selected forms?"),
                null,
                MessageBoxButton.YesNo
            );
            icw.ShowDialog();
            if (icw.Result == MessageBoxResult.Yes)
            {
                this.selectedForms.Clear();
            }
        }

        private void FormCollections_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.FormCollections.SelectedItem != null)
            {
                int id = ((KeyValuePair<int, string>)this.FormCollections.SelectedItem).Key;
                List<TemplateListModel> tempForms = AdminPanelWebApi.GetFormList(id);
                this.forms = new ObservableCollection<TemplateListModel>(tempForms.OrderBy(x => x.DisplayName));
                this.allForms[id] = forms;
                this.Forms.ItemsSource = FormsView;
                FormsView.Filter = new Predicate<object>(o => Filter(o as TemplateListModel));
                RegistryManager.SaveToRegistry(SELECTED_FROM_COLLECTION_KEY, id);
            }
        }

        private void Forms_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ButtonAdd_Click(null, null);
        }

        private void SelectedForms_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ButtonRemove_Click(null, null);
        }

        private void Options_Click(object sender, RoutedEventArgs e)
        {
            new InformOptionsDialog().ShowDialog();
        }

        private void SearchBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            FormsView.Refresh();
        }

        public static void BackPageExpandFirstRow(Document document)
        {
            const float rowStartingHeight = 144;
            const float rowHeightIncrement = 6;
            const float rowMinHeight = 34;

            // adjust row heights
            Section backer = document.Sections.Last;
            Microsoft.Office.Interop.Word.Table backerTable = backer.Range.Tables[1];
            int firstRowPage = (int)backerTable.Rows.First.Range.Information[WdInformation.wdActiveEndPageNumber];
            backerTable.Rows[1].SetHeight(rowStartingHeight, WdRowHeightRule.wdRowHeightAtLeast);
            backerTable.Rows[2].SetHeight(60, WdRowHeightRule.wdRowHeightAtLeast);
            backerTable.Rows[2].Cells[2].Range.Paragraphs[1].SpaceBefore = 6;

            // is the table all one on page?
            int lastRowPage = (int)backerTable.Rows.Last.Range.Information[WdInformation.wdActiveEndPageNumber];

            // if not, decrease row height until everything fits on one page again
            if (lastRowPage != firstRowPage && backerTable.Rows[1].Height != (float)WdConstants.wdUndefined)
            {
                Row firstRow = backerTable.Rows[2];
                firstRow.HeightRule = WdRowHeightRule.wdRowHeightAtLeast;
                while (lastRowPage != firstRowPage && firstRow.Height >= rowMinHeight)
                {
                    firstRow.Height -= rowHeightIncrement;
                    lastRowPage = (int)backerTable.Rows.Last.Range.Information[WdInformation.wdActiveEndPageNumber];
                }
            }
        }

        public static void BackPageAddThirdParty(Document document)
        {
            Section backer = document.Sections.Last;
            Microsoft.Office.Interop.Word.Table backerTable = backer.Range.Tables[1];
            Row row = backerTable.Rows[1];

            // add new column for "and" separator
            Cell newCell = row.Cells.Add(row.Cells[4]);
            newCell.Width = document.Application.InchesToPoints(float.Parse("0.45"));
            newCell.Range.Text = "and";
            newCell.Range.ParagraphFormat.SpaceBefore = 10;
            newCell.Range.ParagraphFormat.SpaceAfter = 0;

            // add column for Third Party
            newCell = row.Cells.Add(row.Cells[5]);
            newCell.Width = document.Application.InchesToPoints(float.Parse("2.5"));
            newCell.Select();
            Selection selection = document.Application.Selection;

            // add placeholder
            selection.TypeText("«third party»");
            selection.set_Style("Prompt");
            selection.TypeText(" ");
            selection.Font.Reset();
            selection.TypeText(Convert.ToChar(11).ToString()); // vertical tab
            selection.TypeText("Third Party");

            // set bookmark
            selection.StartOf(WdUnits.wdLine, WdMovementType.wdExtend);

            // set paragraph formatting for the party cells
            selection.ParagraphFormat.SpaceBefore = 0;
            selection.ParagraphFormat.SpaceAfter = 12;
            selection.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphCenter;

            // Party 1
            row.Cells[1].Width = document.Application.InchesToPoints(float.Parse("2.5"));
            row.Cells[1].Range.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphCenter;

            // Party 2
            row.Cells[3].Width = document.Application.InchesToPoints(float.Parse("2.5"));
            row.Cells[3].Range.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphCenter;

            // Court File Number
            row.Cells[6].Width = document.Application.InchesToPoints(float.Parse("2.25"));
        }
    }
}
