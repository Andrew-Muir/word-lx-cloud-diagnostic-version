﻿using InfowareVSTO.Common.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace InfowareVSTO.Windows.Inform
{
    /// <summary>
    /// Interaction logic for BackPageConfirmationDialog.xaml
    /// </summary>
    public partial class BackPageConfirmationDialog : InfowareWindow
    {
        public BackPageOption Result { get; set; } = BackPageOption.NeverInsert;

        public BackPageConfirmationDialog()
        {
            InitializeComponent();

            this.Append.IsChecked = true;
        }

        private void OKBtn_Click(object sender, RoutedEventArgs e)
        {
            if (Append.IsChecked == true)
            {
                Result = BackPageOption.Append;
            }

            if (SeparateDoc.IsChecked == true)
            {
                Result = BackPageOption.SeparateDoc;
            }

            if (DontInsert.IsChecked == true)
            {
                Result = BackPageOption.NeverInsert;
            }

            this.Close();
        }

        private void CancelBtn_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
