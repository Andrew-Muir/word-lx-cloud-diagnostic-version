﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using InfowareVSTO.Util;
using Microsoft.Office.Interop.Word;
using Style = Microsoft.Office.Interop.Word.Style;

namespace InfowareVSTO.Windows
{
    /// <summary>
    /// Interaction logic for InfowareLineSpacingWindow.xaml
    /// </summary>
    public partial class InfowareLineSpacingWindow : InfowareWindow
    {
        private readonly WdLineSpacing lineSpacingOption;

        // A set of style names for which line spacing options will not apply
        private HashSet<string> ignoredStyleNames;

        private List<string> ignoredBaseStyles;

        public InfowareLineSpacingWindow(WdLineSpacing lineSpacingOption)
        {
            InitializeComponent();

            UpdateIgnoredStyleNames();

            UpdateIgnoredBaseStyleNames();

            this.lineSpacingOption = lineSpacingOption;
        }

        private void UpdateIgnoredStyleNames()
        {
            string rawValue = SettingsManager.GetSetting("KeepStandardSpacing", "DocLineSpacing");

            if (rawValue == null)
            {
                ignoredStyleNames = new HashSet<string>();
                return;
            }

            ignoredStyleNames = new HashSet<string>(SettingsManager.UnescapeCommaCharacter(rawValue.Split(',')).Select(it => it.Trim()));
        }

        private void UpdateIgnoredBaseStyleNames()
        {
            string rawValue = SettingsManager.GetSetting("KeepStandardSpacingBaseStyles", "DocLineSpacing");

            if (rawValue == null)
            {
                ignoredBaseStyles = new List<string>();
                return;
            }

            ignoredBaseStyles = new List<string>(SettingsManager.UnescapeCommaCharacter(rawValue.Split(',')).Select(it => it.Trim()));
        }

        private void OnBtnWholeDocumentClick(object sender, RoutedEventArgs e)
        {
            using (new DocumentUndoTransaction("Whole document line spacing"))
            {
                ApplyLineSpacing(DocumentUtil.GetActiveDocument().Paragraphs);
            }

            Close();
        }

        private void OnBtnSelectionClick(object sender, RoutedEventArgs e)
        {
            using (new DocumentUndoTransaction("Selection line spacing"))
            {
                ApplyLineSpacing(DocumentUtil.GetSelection().Paragraphs);
            }

            Close();
        }

        private void ApplyLineSpacing(Paragraphs paragraphs)
        {
            foreach (Paragraph paragraph in paragraphs)
            {
                if (ShouldIgnoreParagraph(paragraph))
                {
                    continue;
                }

                paragraph.Format.LineSpacingRule = lineSpacingOption;
            }
        }

        private void OnBtnCancelClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private bool ShouldIgnoreParagraph(Paragraph paragraph)
        {
            var paragraphStyle = (Style) paragraph.get_Style();
            var baseStyle = paragraphStyle.get_BaseStyle(); 
            return ignoredStyleNames.Contains(paragraphStyle.NameLocal) || (baseStyle != null && ignoredBaseStyles.Contains((baseStyle as Style)?.NameLocal)) ;
        }
    }
}
