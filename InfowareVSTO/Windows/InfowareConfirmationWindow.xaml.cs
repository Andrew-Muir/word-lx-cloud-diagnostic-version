﻿using InfowareVSTO.Common;
using InfowareVSTO.Common.Language;
using System;
using System.Windows;
using System.Windows.Controls;

namespace InfowareVSTO.Windows
{
    /// <summary>
    /// Interaction logic for InfowareInformationWindow.xaml
    /// </summary>
    public partial class InfowareConfirmationWindow : InfowareWindow
    {
        public MessageBoxResult Result { get; set; }

        public InfowareConfirmationWindow(string message, string title = null, MessageBoxButton messageBoxButton = MessageBoxButton.OKCancel)
        {
            InitializeComponent();
            if (title == null)
            {
                title = LanguageManager.GetTranslation(LanguageConstants.Confirmation, "Confirmation");
            }

            Title = title;
            TbMessage.Text = message;

            var okButton = new Button { Content = LanguageManager.GetTranslation(LanguageConstants.OK, "OK") };
            okButton.Click += OkButton_Click;

            var cancelButton = new Button { Content = LanguageManager.GetTranslation(LanguageConstants.Cancel, "Cancel") };
            cancelButton.Click += CancelButton_Click;

            var yesButton = new Button { Content = LanguageManager.GetTranslation(LanguageConstants.Yes, "Yes") };
            yesButton.Click += YesButton_Click;

            var noButton = new Button { Content = LanguageManager.GetTranslation(LanguageConstants.No, "No") };
            noButton.Click += NoButton_Click;

            switch (messageBoxButton)
            {
                case MessageBoxButton.OK:
                    BottomPanel.Children.Add(okButton);
                    break;
                case MessageBoxButton.OKCancel:
                    BottomPanel.Children.Add(okButton);
                    BottomPanel.Children.Add(cancelButton);
                    break;
                case MessageBoxButton.YesNo:
                    BottomPanel.Children.Add(yesButton);
                    BottomPanel.Children.Add(noButton);
                    break;
                case MessageBoxButton.YesNoCancel:
                    BottomPanel.Children.Add(yesButton);
                    BottomPanel.Children.Add(noButton);
                    BottomPanel.Children.Add(cancelButton);
                    break;
            }
        }

        private void OkButton_Click(object sender, RoutedEventArgs e)
        {
            Result = MessageBoxResult.OK;
            Close();
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            Result = MessageBoxResult.Cancel;
            Close();
        }

        private void YesButton_Click(object sender, RoutedEventArgs e)
        {
            Result = MessageBoxResult.Yes;
            Close();
        }

        private void NoButton_Click(object sender, RoutedEventArgs e)
        {
            Result = MessageBoxResult.No;
            Close();
        }
    }
}
