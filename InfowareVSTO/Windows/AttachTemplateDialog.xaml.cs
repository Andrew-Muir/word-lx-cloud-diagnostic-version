﻿using DocumentFormat.OpenXml.Wordprocessing;
using InfowareVSTO.Common;
using InfowareVSTO.Common.Models;
using InfowareVSTO.Util;
using InfowareVSTO.VSTOAuthor;
using Microsoft.Office.Interop.Word;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Word = Microsoft.Office.Interop.Word;

namespace InfowareVSTO.Windows
{
    /// <summary>
    /// Interaction logic for AttachTemplateDialog.xaml
    /// </summary>
    public partial class AttachTemplateDialog : InfowareWindow
    {
        public AttachTemplateDialog()
        {
            InitializeComponent();
        }

        private void CancelBtn_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void OKBtn_Click(object sender, RoutedEventArgs e)
        {
            if (this.BlankDocument.IsChecked == true)
            {
                AttachTemplate(TemplateType.BlankDocument);
            }

            if (this.Letter.IsChecked == true)
            {
                AttachTemplate(TemplateType.Letter);
            }

            if (this.Memo.IsChecked == true)
            {
                AttachTemplate(TemplateType.Memo);
            }

            if (this.Other.IsChecked == true)
            {
                AttachTemplate(TemplateType.Custom);
            }

            this.Close();
        }

        private static void AttachTemplate(TemplateType templateType)
        {
            var currentAuthorSettings = ManageUsers.CurrentAuthorSettings;
            var templateList = AdminPanelWebApi.GetTemplateList(templateType, currentAuthorSettings.CompanyId, currentAuthorSettings.LanguageId, currentAuthorSettings.LocationId);

            TemplateListModel template = null;
            if (templateList.Count > 0)
            {
                if (templateList.Count == 1)
                {
                    template = templateList.FirstOrDefault();
                }
                else
                {
                    var templateChooser = new TemplateChooser(templateType);
                    templateChooser.PopulateTemplates(templateList);
                    templateChooser.ShowDialog();
                    if (templateChooser.ResultOk)
                    {
                        template = templateList.Where(x => x.Id == templateChooser.ResultId).FirstOrDefault();
                    }
                }
            }
            else
            {
                new InfowareInformationWindow("No templates of this type exists.").ShowDialog();
            }

            if (template != null)
            {
                var activeDocument = ThisAddIn.Instance.Application.ActiveDocument;
                TaskPaneUtil2.CloseTaskpanesOfDocument(activeDocument);

                var downloadedTemplate = Utils.DownloadAndGetTemplate(template, false);
                ThisAddIn.Instance.Application.ScreenUpdating = false;
                if (ThisAddIn.Instance.IsFunctionForLogging("ScreenUpdating"))
                    Logger.Log("Disabled Screen Updating (AttachTemplate.xaml.cs > AttachTemplate)");
                foreach (Word.Section section in activeDocument.Sections)
                {
                    var topMargin = downloadedTemplate.Document.Sections.First.PageSetup.TopMargin;
                    if (topMargin != CustomConstants.UNDEFINED_VALUE)
                    {
                        if (topMargin <= CustomConstants.MARGIN_MAX_IN_DP)
                        {
                            section.PageSetup.TopMargin = topMargin;
                        }
                        else
                        {
                            section.PageSetup.TopMargin = CustomConstants.MARGIN_MAX_IN_DP;
                        }
                    }

                    var bottomMargin = downloadedTemplate.Document.Sections.First.PageSetup.BottomMargin;
                    if (bottomMargin != CustomConstants.UNDEFINED_VALUE)
                    {
                        if (bottomMargin <= CustomConstants.MARGIN_MAX_IN_DP)
                        {
                            section.PageSetup.BottomMargin = bottomMargin;
                        }
                        else
                        {
                            section.PageSetup.BottomMargin = CustomConstants.MARGIN_MAX_IN_DP;
                        }
                    }

                    var leftMargin = downloadedTemplate.Document.Sections.First.PageSetup.LeftMargin;
                    if (leftMargin != CustomConstants.UNDEFINED_VALUE)
                    {
                        if (leftMargin <= CustomConstants.MARGIN_MAX_IN_DP)
                        {
                            section.PageSetup.LeftMargin = leftMargin;
                        }
                        else
                        {
                            section.PageSetup.LeftMargin = CustomConstants.MARGIN_MAX_IN_DP;
                        }
                    }

                    var rightMargin = downloadedTemplate.Document.Sections.First.PageSetup.RightMargin;
                    if (rightMargin != CustomConstants.UNDEFINED_VALUE)
                    {
                        if (rightMargin <= CustomConstants.MARGIN_MAX_IN_DP)
                        {
                            section.PageSetup.RightMargin = rightMargin;
                        }
                        else
                        {
                            section.PageSetup.RightMargin = CustomConstants.MARGIN_MAX_IN_DP;
                        }
                    }

                    var headerDistance = downloadedTemplate.Document.Sections.First.PageSetup.HeaderDistance;
                    if (headerDistance != CustomConstants.UNDEFINED_VALUE)
                    {
                        if (headerDistance <= CustomConstants.MARGIN_MAX_IN_DP)
                        {
                            section.PageSetup.HeaderDistance = headerDistance;
                        }
                        else
                        {
                            section.PageSetup.HeaderDistance = CustomConstants.MARGIN_MAX_IN_DP;
                        }
                    }

                    var footerDistance = downloadedTemplate.Document.Sections.First.PageSetup.FooterDistance;
                    if (footerDistance != CustomConstants.UNDEFINED_VALUE)
                    {
                        if (footerDistance <= CustomConstants.MARGIN_MAX_IN_DP)
                        {
                            section.PageSetup.FooterDistance = footerDistance;
                        }
                        else
                        {
                            section.PageSetup.FooterDistance = CustomConstants.MARGIN_MAX_IN_DP;
                        }
                    }

                    if (section.Headers[WdHeaderFooterIndex.wdHeaderFooterFirstPage].Exists)
                    {
                        section.Headers[WdHeaderFooterIndex.wdHeaderFooterFirstPage].Range.ParagraphFormat.LineSpacing = downloadedTemplate.Document.Sections.First.Headers[WdHeaderFooterIndex.wdHeaderFooterFirstPage].Range.ParagraphFormat.LineSpacing;
                        section.Headers[WdHeaderFooterIndex.wdHeaderFooterFirstPage].Range.ParagraphFormat.LineSpacingRule = downloadedTemplate.Document.Sections.First.Headers[WdHeaderFooterIndex.wdHeaderFooterFirstPage].Range.ParagraphFormat.LineSpacingRule;
                        section.Headers[WdHeaderFooterIndex.wdHeaderFooterFirstPage].Range.ParagraphFormat.SpaceAfter = downloadedTemplate.Document.Sections.First.Headers[WdHeaderFooterIndex.wdHeaderFooterFirstPage].Range.ParagraphFormat.SpaceAfter;
                        section.Headers[WdHeaderFooterIndex.wdHeaderFooterFirstPage].Range.ParagraphFormat.SpaceBefore = downloadedTemplate.Document.Sections.First.Headers[WdHeaderFooterIndex.wdHeaderFooterFirstPage].Range.ParagraphFormat.SpaceBefore;
                    }

                    if (section.Headers[WdHeaderFooterIndex.wdHeaderFooterEvenPages].Exists)
                    {
                        section.Headers[WdHeaderFooterIndex.wdHeaderFooterEvenPages].Range.ParagraphFormat.LineSpacing = downloadedTemplate.Document.Sections.First.Headers[WdHeaderFooterIndex.wdHeaderFooterFirstPage].Range.ParagraphFormat.LineSpacing;
                        section.Headers[WdHeaderFooterIndex.wdHeaderFooterEvenPages].Range.ParagraphFormat.LineSpacingRule = downloadedTemplate.Document.Sections.First.Headers[WdHeaderFooterIndex.wdHeaderFooterFirstPage].Range.ParagraphFormat.LineSpacingRule;
                        section.Headers[WdHeaderFooterIndex.wdHeaderFooterEvenPages].Range.ParagraphFormat.SpaceAfter = downloadedTemplate.Document.Sections.First.Headers[WdHeaderFooterIndex.wdHeaderFooterFirstPage].Range.ParagraphFormat.SpaceAfter;
                        section.Headers[WdHeaderFooterIndex.wdHeaderFooterEvenPages].Range.ParagraphFormat.SpaceBefore = downloadedTemplate.Document.Sections.First.Headers[WdHeaderFooterIndex.wdHeaderFooterFirstPage].Range.ParagraphFormat.SpaceBefore;
                    }

                    section.Headers[WdHeaderFooterIndex.wdHeaderFooterPrimary].Range.ParagraphFormat.LineSpacing = downloadedTemplate.Document.Sections.First.Headers[WdHeaderFooterIndex.wdHeaderFooterPrimary].Range.ParagraphFormat.LineSpacing;
                    section.Headers[WdHeaderFooterIndex.wdHeaderFooterPrimary].Range.ParagraphFormat.LineSpacingRule = downloadedTemplate.Document.Sections.First.Headers[WdHeaderFooterIndex.wdHeaderFooterPrimary].Range.ParagraphFormat.LineSpacingRule;
                    section.Headers[WdHeaderFooterIndex.wdHeaderFooterPrimary].Range.ParagraphFormat.SpaceAfter = downloadedTemplate.Document.Sections.First.Headers[WdHeaderFooterIndex.wdHeaderFooterPrimary].Range.ParagraphFormat.SpaceAfter;
                    section.Headers[WdHeaderFooterIndex.wdHeaderFooterPrimary].Range.ParagraphFormat.SpaceBefore = downloadedTemplate.Document.Sections.First.Headers[WdHeaderFooterIndex.wdHeaderFooterFirstPage].Range.ParagraphFormat.SpaceBefore;
                }
                ThisAddIn.Instance.Application.ScreenUpdating = true;
                if (ThisAddIn.Instance.IsFunctionForLogging("ScreenUpdating"))
                    Logger.Log("Enabled Screen Updating (AttachTemplate.xaml.cs > AttachTemplate)");
                activeDocument.set_AttachedTemplate(downloadedTemplate.TemplatePath);
                activeDocument.UpdateStyles();
                activeDocument.set_AttachedTemplate(string.Empty);

                DocumentPropertyUtil.SaveShortProperty(CustomConstants.TemplateTypeTags, ((int)templateType).ToString(), true);
                if (!downloadedTemplate.Document.Saved) { downloadedTemplate.Document.Saved = true; }
                downloadedTemplate.Document.Close(WdSaveOptions.wdDoNotSaveChanges);
            }
        }

        public class ListViewItem
        {
            public TemplateType Type { get; set; }

            public string Name { get; set; }
        }
    }
}
