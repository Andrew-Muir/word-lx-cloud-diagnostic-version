﻿using InfowareVSTO.Common.Language;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace InfowareVSTO.Windows
{
    /// <summary>
    /// Interaction logic for AboutWindow.xaml
    /// </summary>
    public partial class AboutWindow : InfowareWindow
    {
        private static AboutWindow aboutWindow;
        public static void ShowAboutWindow()
        {
            if (aboutWindow == null || aboutWindow.IsClosed)
            {
                aboutWindow = new AboutWindow();
            }
            aboutWindow.Show();
            aboutWindow.Activate();
        }
        public bool IsClosed { get; set; }

        public AboutWindow()
        {
            InitializeComponent();

            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
            FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(assembly.Location);
            string version = fvi.FileVersion;

            this.VersionNumber.Text = $"{LanguageManager.GetTranslation(LanguageConstants.Version, "Version")}: {version}";
        }

        private void OKBtn_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            IsClosed = true;
        }
    }
}
