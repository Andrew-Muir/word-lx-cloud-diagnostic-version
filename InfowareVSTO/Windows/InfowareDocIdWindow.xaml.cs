﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using InfowareVSTO.Common;
using InfowareVSTO.Exceptions;
using InfowareVSTO.IManage;
using InfowareVSTO.Util;
using Microsoft.Office.Interop.Word;
using Microsoft.Office.Tools.Word;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System.Linq;
using Microsoft.Office.Core;

namespace InfowareVSTO.Windows
{

    public enum DocIdSettings
    {
        EveryPage = 0b0,
        FirstPage = 0b1,
        LastPage = 0b10,
        SkipFirstPage = 0b11,
        IncludeAuthor = 0b100,
        IncludeDate = 0b1000
    }

    /// <summary>
    /// Interaction logic for InfowareDocIdWindow.xaml
    /// </summary>
    /// 
    public partial class InfowareDocIdWindow : InfowareWindow
    {
        public const string DOCUMENT_ID_CONTROL_TAG = "LX-DOCUMENTID";
        private const string DOCUMENT_ID_SETTING_PROP = "DOCUMENTID-SETTINGS";
        private const string DOCUMENT_ID_CONTROL_NAME_TEMPLATE = DOCUMENT_ID_CONTROL_TAG + "_{0}";
        private const string ENTERPRISE_DOCID_BOOKMARK = "DocsID";
        private const string ENTERPRISE_DOCID_AUTOUPDATE = "DocIDAutoUpdate";
        private const string ENTERPRISE_DOCID_BOOKMARK_LAST = "docsstamplast";
        private const string AUTHOR_NOT_SET_API_MESSAGE = "Current author was not yet set!";
        public const string IMANAGE_VAR_PREFIX = "IManageDocInfoCache";
        public const string NO_AUTO_UPDATE = "WordLXNoDocIdAutoUpdate";
        public const string FILENAME_REPLACEMENT = "[FILENAME]";
        public const string AUTHORID_REPLACEMENT = "[AUTHORID]";
        public const string LASTSAVEDDATE_REPLACEMENT = "[LASTSAVEDDATE]";
        private bool functionalityIsDisabled;
        private Microsoft.Office.Tools.Word.Document vstoDocument;
        private Microsoft.Office.Interop.Word.Document nativeDocument;
        private bool usesIManage = false;
        public bool UseDateTimeNow { get; set; }

        public bool HasSettingsDocumentProp { get; set; }

        public InfowareDocIdWindow()
        {
            InitializeComponent();
            nativeDocument = Globals.ThisAddIn.Application.ActiveDocument;
            vstoDocument = Globals.Factory.GetVstoObject(nativeDocument);
            Loaded += OnWindowLoaded;

            if (Globals.ThisAddIn.IsFunctionForLogging("DocID"))
                Logger.Log($"Enter InfowareDocIdWindow(): {nativeDocument?.Name}/{nativeDocument?.DocID}");

            if (SettingsManager.GetSettingAsBool("NoDocIDSupport", "Tools", false))
            {
                return;
            }

            UpdateFirstPageRadioButton();
            UpdateLastPageRadioButton();
            UpdateIncludeAuthorIdCheckBox();
            UpdateIncludeLastSavedDateCheckBox();
            ApplyPositionSetting();

            if (UsesIManage())
            {
                if (Globals.ThisAddIn.IsFunctionForLogging("DocID"))
                    Logger.Log($"This is a DMS document, load DMS options: {nativeDocument?.Name}/{nativeDocument?.DocID}");

                this.NoDM.Visibility = Visibility.Collapsed;
                this.IManage.Visibility = Visibility.Visible;

                LoadIManageSettings(IManageUtilities.LoadSettings());

                HideIManageCheckboxes();

                usesIManage = true;

                if (Globals.ThisAddIn.IsFunctionForLogging("DocID"))
                    Logger.Log($"Done loading DMS options: {nativeDocument?.Name}/{nativeDocument?.DocID}");
            }

            if (!SettingsManager.SETTINGS_ENABLED)
            {
                RbEveryPage.IsChecked = true;
            }

            if (Globals.ThisAddIn.IsFunctionForLogging("DocID"))
                Logger.Log($"Exit InfowareDocIdWindow(): {nativeDocument?.Name}/{nativeDocument?.DocID}");
        }

        private void HideIManageCheckboxes()
        {
            string hiddenFields = SettingsManager.GetSetting("HiddenFields", "DocumentManagement", SettingsManager.GetSetting("HiddenFields", "IManage", ""));
            foreach (string nrStr in SettingsManager.UnescapeCommaCharacter(hiddenFields.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries)))
            {
                if (int.TryParse(nrStr, out int nr))
                {
                    switch (nr)
                    {
                        case 0:
                            this.CbIMIncludeDocumentNumber.Visibility = Visibility.Collapsed;
                            this.CbIMIncludeDocumentNumber.IsChecked = false;
                            break;
                        case 1:
                            this.CbIMIncludeVersion.Visibility = Visibility.Collapsed;
                            this.CbIMIncludeVersion.IsChecked = false;
                            break;
                        case 2:
                            this.CbIMIncludeDatabaseName.Visibility = Visibility.Collapsed;
                            this.CbIMIncludeDatabaseName.IsChecked = false;
                            break;
                        case 3:
                            this.CbIMIncludeAuthorId.Visibility = Visibility.Collapsed;
                            this.CbIMIncludeAuthorId.IsChecked = false;

                            break;
                        case 4:
                            this.CbIMIncludeClientId.Visibility = Visibility.Collapsed;
                            this.CbIMIncludeClientId.IsChecked = false;
                            break;
                        case 5:
                            this.CbIMIncludeMatter.Visibility = Visibility.Collapsed;
                            this.CbIMIncludeMatter.IsChecked = false;
                            break;
                        case 6:
                            this.CbIMIncludeDocumentDescription.Visibility = Visibility.Collapsed;
                            this.CbIMIncludeDocumentDescription.IsChecked = false;
                            break;
                        case 7:
                            this.CbIMIncludeLastSavedDate.Visibility = Visibility.Collapsed;
                            this.CbIMIncludeLastSavedDate.IsChecked = false;
                            break;
                    }
                }
            }

            string mandatoryFields = SettingsManager.GetSetting("MandatoryFields", "DocumentManagement", SettingsManager.GetSetting("MandatoryFields", "IManage", "0"));
            foreach (string nrStr in SettingsManager.UnescapeCommaCharacter(mandatoryFields.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries)))
            {
                if (int.TryParse(nrStr, out int nr))
                {
                    switch (nr)
                    {
                        case 0:
                            this.CbIMIncludeDocumentNumber.Visibility = Visibility.Collapsed;
                            this.CbIMIncludeDocumentNumber.IsChecked = true;
                            break;
                        case 1:
                            this.CbIMIncludeVersion.Visibility = Visibility.Collapsed;
                            this.CbIMIncludeVersion.IsChecked = true;
                            break;
                        case 2:
                            this.CbIMIncludeDatabaseName.Visibility = Visibility.Collapsed;
                            this.CbIMIncludeDatabaseName.IsChecked = true;
                            break;
                        case 3:
                            this.CbIMIncludeAuthorId.Visibility = Visibility.Collapsed;
                            this.CbIMIncludeAuthorId.IsChecked = true;

                            break;
                        case 4:
                            this.CbIMIncludeClientId.Visibility = Visibility.Collapsed;
                            this.CbIMIncludeClientId.IsChecked = true;
                            break;
                        case 5:
                            this.CbIMIncludeMatter.Visibility = Visibility.Collapsed;
                            this.CbIMIncludeMatter.IsChecked = true;
                            break;
                        case 6:
                            this.CbIMIncludeDocumentDescription.Visibility = Visibility.Collapsed;
                            this.CbIMIncludeDocumentDescription.IsChecked = true;
                            break;
                        case 7:
                            this.CbIMIncludeLastSavedDate.Visibility = Visibility.Collapsed;
                            this.CbIMIncludeLastSavedDate.IsChecked = true;
                            break;
                    }
                }
            }
        }

        public static bool UsesIManage()
        {
            if (Globals.ThisAddIn.Application.ActiveDocument?.Variables != null)
            {
                foreach (Variable variable in Globals.ThisAddIn.Application.ActiveDocument.Variables)
                {
                    if (variable.Name.Contains(IMANAGE_VAR_PREFIX))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public void ApplyPositionSetting()
        {
            string docProp = DocumentPropertyUtil.ReadProperty(DOCUMENT_ID_SETTING_PROP);
            int docPropInt = 0;

            if (docProp != null && int.TryParse(docProp, out docPropInt))
            {
                ApplySettings(docPropInt);
            }
            else
            {
                int docIDDefaultLocation = SettingsManager.GetSettingAsInt("DocIDDefaultLocation", "Tools", 0);
                ApplySettings(docIDDefaultLocation);
            }

            HasSettingsDocumentProp = true;
        }

        public void UpdateDocIdOnOpen(bool save = false)
        {
            if (Globals.ThisAddIn.IsFunctionForLogging("DocID"))
                Logger.Log($"Enter UpdateDocIdOnOpen(): {nativeDocument?.Name}/{nativeDocument?.DocID}");

            bool docIDAutoUpdateAfterRemove = SettingsManager.GetSettingAsBool("DocIDAutoUpdateAfterRemove", "Tools", false);

            RefreshDocumentId(true, docIDAutoUpdateAfterRemove, true);

            if (save && SettingsManager.GetSettingAsBool("SaveDocIDUpdateOnOpen", "Tools", false))
            {
                ThisAddIn.Instance.Application.ActiveDocument.Save();
            }

            if (Globals.ThisAddIn.IsFunctionForLogging("DocID"))
                Logger.Log($"Exit UpdateDocIdOnOpen(): {nativeDocument?.Name}/{nativeDocument?.DocID}");
        }

        public void RefreshDocumentId(bool insertIfNotPresent, bool updateEvenIfHasNoAutoUpdate = false, bool alwaysUpdateIfFound = false)
        {
            if (Globals.ThisAddIn.IsFunctionForLogging("DocID"))
                Logger.Log($"Enter RefreshDocumentId(): {nativeDocument?.Name}/{nativeDocument?.DocID}");

            try
            {
                if (SkipDocIdAutoUpdateForThisDoc() && !updateEvenIfHasNoAutoUpdate && alwaysUpdateIfFound)
                {
                    updateEvenIfHasNoAutoUpdate = true;
                    insertIfNotPresent = false;
                }

                if (!SkipDocIdAutoUpdateForThisDoc() || updateEvenIfHasNoAutoUpdate)
                {
                    string docIdText = GetDocumentId();

                    bool found = false;

                    CallBeforeDocIDInsertionMethods(true);

                    if (Globals.ThisAddIn.IsFunctionForLogging("DocID"))
                        Logger.Log($"DocID already exists in document, update DocID: {nativeDocument?.Name}/{nativeDocument?.DocID}");
                    foreach (Microsoft.Office.Interop.Word.ContentControl cc in DocumentUtil.GetAllContentControls())
                    {
                        if (cc.Tag == DOCUMENT_ID_CONTROL_TAG)
                        {
                            cc.Range.Text = docIdText;
                            found = true;
                        }
                    }

                    if (found)
                    {
                        CallAfterDocIdInsertionMethods(true);
                    }

                    if (!found)
                    {
                        if (insertIfNotPresent)
                        {
                            if (Globals.ThisAddIn.IsFunctionForLogging("DocID"))
                                Logger.Log($"DocID not found, and autoinsert enabled: {nativeDocument?.Name}/{nativeDocument?.DocID}");

                            if (!HasSettingsDocumentProp)
                            {
                                ShowDialog();
                            }
                            else
                            {
                                OnOkButtonClick(null, null);
                            }
                        }
                    }
                }

                if (Globals.ThisAddIn.IsFunctionForLogging("DocID"))
                    Logger.Log($"Exit RefreshDocumentId(): {nativeDocument?.Name}/{nativeDocument?.DocID}");
            }
            catch (Exception ex)
            {
                new InfowareInformationWindow($"{ex.Message}\n\n{ex.StackTrace}", "InfowareDocIdWindow.RefreshDocumentId()", WordDefaultIconType.Error);
            }
        }

        private void RemoveWordLXEnterpriseDocId()
        {
            Microsoft.Office.Interop.Word.Document doc = ThisAddIn.Instance.Application.ActiveDocument;
            string docIdAutoUpdate = DocumentPropertyUtil.ReadProperty(ENTERPRISE_DOCID_AUTOUPDATE)?.Trim()?.ToUpper();
            if (docIdAutoUpdate != null)
            {
                switch (docIdAutoUpdate)
                {
                    case "LAST":
                        if (doc.Bookmarks.Exists(ENTERPRISE_DOCID_BOOKMARK_LAST))
                        {
                            Range lastRange = doc.Bookmarks[ENTERPRISE_DOCID_BOOKMARK_LAST].Range;
                            if (lastRange.Frames.Count > 0)
                            {
                                lastRange.Delete();
                            }
                            else
                            {
                                try
                                {
                                    lastRange.Paragraphs.First.Range.Delete();
                                }
                                catch { lastRange.Delete(); }
                            }
                            //doc.Bookmarks[ENTERPRISE_DOCID_BOOKMARK_LAST].Delete();
                        }

                        break;
                    case "FIRST":

                        Section section = doc.Sections.First;
                        foreach (HeaderFooter footer in section.Footers)
                        {
                            List<Microsoft.Office.Interop.Word.Shape> toDelete = new List<Microsoft.Office.Interop.Word.Shape>();
                            foreach (Microsoft.Office.Interop.Word.Shape shape in footer.Range.ShapeRange)
                            {
                                if (shape.Name.IndexOf("DocsID") == 0)
                                {
                                    toDelete.Add(shape);
                                }
                            }

                            foreach (Microsoft.Office.Interop.Word.Shape shape in toDelete)
                            {
                                shape.Delete();
                            }
                        }
                        if (doc.Bookmarks.Exists(ENTERPRISE_DOCID_BOOKMARK))
                        {
                            doc.Bookmarks[ENTERPRISE_DOCID_BOOKMARK].Delete();
                        }

                        break;
                    case "ALL":
                        ThisAddIn.Instance.Application.ScreenUpdating = false;
                        if (ThisAddIn.Instance.IsFunctionForLogging("ScreenUpdating"))
                            Logger.Log("Disabled Screen Updating (InfowareDocIdWindow.xaml.cs > RemoveWordLXEnterpriseDocId)");
                        foreach (Section sectionVar in doc.Sections)
                        {
                            foreach (HeaderFooter footer in sectionVar.Footers)
                            {
                                List<Microsoft.Office.Interop.Word.Shape> toDelete = new List<Microsoft.Office.Interop.Word.Shape>();
                                foreach (Microsoft.Office.Interop.Word.Shape shape in footer.Range.ShapeRange)
                                {
                                    if (shape.Name.IndexOf("DocsID") == 0)
                                    {
                                        toDelete.Add(shape);
                                    }
                                }

                                foreach (Microsoft.Office.Interop.Word.Shape shape in toDelete)
                                {
                                    shape.Delete();
                                }
                            }
                        }
                        ThisAddIn.Instance.Application.ScreenUpdating = true;
                        if (ThisAddIn.Instance.IsFunctionForLogging("ScreenUpdating"))
                            Logger.Log("Enabled Screen Updating (InfowareDocIdWindow.xaml.cs > RemoveWordLXEnterpriseDocId)");
                        if (doc.Bookmarks.Exists(ENTERPRISE_DOCID_BOOKMARK))
                        {
                            doc.Bookmarks[ENTERPRISE_DOCID_BOOKMARK].Delete();
                        }
                        break;
                    default:
                        break;
                }

            }

            DocumentPropertyUtil.RemoveShortProperty(ENTERPRISE_DOCID_AUTOUPDATE);
            DocumentPropertyUtil.RemoveShortProperty("DocsID");
        }

        private void ApplySettings(int settings)
        {
            if ((settings & 0b11) == (int)DocIdSettings.EveryPage)
            {
                this.RbEveryPage.IsChecked = true;
            }
            if ((settings & 0b11) == (int)DocIdSettings.LastPage)
            {
                this.RbLastPage.IsChecked = true;
            }
            if ((settings & 0b11) == (int)DocIdSettings.FirstPage)
            {
                this.RbFirstPage.IsChecked = true;
            }
            if ((settings & 0b11) == (int)DocIdSettings.SkipFirstPage)
            {
                this.RbSkipFirstPage.IsChecked = true;
            }
            if ((settings & (int)DocIdSettings.IncludeAuthor) > 0)
            {
                this.CbIncludeAuthorId.IsChecked = true;
            }
            if ((settings & (int)DocIdSettings.IncludeDate) > 0)
            {
                this.CbIncludeLastSavedDate.IsChecked = true;
            }
        }

        private void UpdateFirstPageRadioButton()
        {
            bool enableFirstPageRadioButton = SettingsManager.GetSettingAsBool("EnableDocsIDFirstPage", "Tools", true);
            RbFirstPage.IsChecked = enableFirstPageRadioButton;

            if (!enableFirstPageRadioButton)
            {
                RbFirstPage.Visibility = Visibility.Collapsed;
            }
        }

        private void UpdateLastPageRadioButton()
        {
            bool enableLastPageRadioButton = SettingsManager.GetSettingAsBool("EnableDocsIDLastPage", "Tools", true);
            RbLastPage.IsChecked = enableLastPageRadioButton;

            if (!enableLastPageRadioButton)
            {
                RbLastPage.Visibility = Visibility.Collapsed;
            }
        }

        private void UpdateIncludeAuthorIdCheckBox()
        {
            bool enableIncludeAuthorId = SettingsManager.GetSettingAsBool("EnableDocsIDIncludeAuthorID", "Tools", true);

            if (!enableIncludeAuthorId)
            {
                CbIncludeAuthorId.Visibility = Visibility.Collapsed;
            }
        }

        private void UpdateIncludeLastSavedDateCheckBox()
        {
            bool enableIncludeLastSavedDate = SettingsManager.GetSettingAsBool("EnableDocsIDIncludeLSaveDate", "Tools", true);

            if (!enableIncludeLastSavedDate)
            {
                CbIncludeLastSavedDate.Visibility = Visibility.Collapsed;
                CbIMIncludeLastSavedDate.Visibility = Visibility.Collapsed;
                CbIMIncludeLastSavedDate.IsChecked = false;
            }
        }

        private void OnWindowLoaded(object sender, RoutedEventArgs e)
        {
            if (functionalityIsDisabled)
            {
                Visibility = Visibility.Hidden;

                MessageBox.Show("The functionality is disabled via company settings", Title, MessageBoxButton.OK, MessageBoxImage.Exclamation);
                Close();
            }
        }

        public void OnOkButtonClick(object sender, RoutedEventArgs e)
        {
            if (Globals.ThisAddIn.IsFunctionForLogging("DocID"))
                Logger.Log($"Enter OnOkButtonClick(): {nativeDocument?.Name}/{nativeDocument?.DocID}");

            string documentId;
            try
            {
                documentId = GetDocumentId();
            }
            catch (WebServiceInteractionException)
            {
                new InfowareInformationWindow("Could not reach the web service").ShowDialog();
                return;
            }

            bool update = false;
            if (RbRemove.IsChecked != true)
            {
                update = CallBeforeDocIDInsertionMethods(false);
            }

            RemoveDocumentId();

            using (new DocumentUndoTransaction("DocumentId insertion"))
            {
                try
                {
                    if ((bool)RbEveryPage.IsChecked)
                    {
                        InsertDocumentIdOnEveryPage(documentId);
                    }
                    else if ((bool)RbFirstPage.IsChecked)
                    {
                        InsertDocumentIdOnFirstPage(documentId);
                    }
                    else if ((bool)RbSkipFirstPage.IsChecked)
                    {
                        InsertDocumentIdOnSkipFirstPage(documentId);
                    }
                    else if ((bool)RbLastPage.IsChecked)
                    {
                        InsertDocumentIdOnLastPage(documentId);
                    }
                    if (RbRemove.IsChecked != true)
                    {
                        CallAfterDocIdInsertionMethods(update);
                    }
                }
                catch (System.NotImplementedException)
                {
                    DocumentUtil.ShowCompatibilityModeError(new System.Threading.Tasks.Task(() => { OnOkButtonClick(sender, e); }));
                }
            }

            if ((bool)RbRemove.IsChecked)
            {
                RemoveDocumentId();
                SetSkipDocIdAutoUpdateForThisDoc(true);
            }
            else
            {
                int docProp = 0;
                if (RbEveryPage.IsChecked == true)
                {
                    docProp = (int)DocIdSettings.EveryPage;
                }
                if (RbFirstPage.IsChecked == true)
                {
                    docProp = (int)DocIdSettings.FirstPage;
                }
                if (RbLastPage.IsChecked == true)
                {
                    docProp = (int)DocIdSettings.LastPage;
                }
                if (RbSkipFirstPage.IsChecked == true)
                {
                    docProp = (int)DocIdSettings.SkipFirstPage;
                }

                if (CbIncludeAuthorId.IsChecked == true)
                {
                    docProp = docProp | (int)DocIdSettings.IncludeAuthor;
                }

                if (CbIncludeLastSavedDate.IsChecked == true)
                {
                    docProp = docProp | (int)DocIdSettings.IncludeDate;
                }

                DocumentPropertyUtil.SaveShortProperty(DOCUMENT_ID_SETTING_PROP, docProp.ToString(), true);

                if (SettingsManager.GetSettingAsBool("UpdateFieldsOnUpdateDocID", "Tools", false))
                {
                    ThisAddIn.Instance.Application.ActiveDocument.Fields.Update();
                }
            }

            if (Globals.ThisAddIn.IsFunctionForLogging("DocID"))
                Logger.Log($"Exit OnOkButtonClick(): {nativeDocument?.Name}/{nativeDocument?.DocID}");
            Close();
        }

        private void CallAfterDocIdInsertionMethods(bool update)
        {
            if (update)
            {
                bool callMethodAfterUpdate = SettingsManager.GetSettingAsBool("AfterDocIDUpdate", "DocumentManagement", false);
                if (callMethodAfterUpdate)
                {
                    try
                    {
                        ThisAddIn.Instance.Application.Run("WordLXAfterDocIDUpdate");
                    }
                    catch (Exception ex)
                    {
                    }
                }
            }
            else
            {
                bool callMethodAfterInsert = SettingsManager.GetSettingAsBool("AfterDocIDInsert", "DocumentManagement", false);
                if (callMethodAfterInsert)
                {
                    try
                    {
                        ThisAddIn.Instance.Application.Run("WordLXAfterDocIDInsert");
                    }
                    catch (Exception ex)
                    {
                    }
                }
            }
        }

        //return if docId is already present.
        private bool CallBeforeDocIDInsertionMethods(bool onlyOnUpdate)
        {
            bool update = RemoveDocumentId(true);

            if (update)
            {
                bool callMethodBeforeUpdate = SettingsManager.GetSettingAsBool("BeforeDocIDUpdate", "DocumentManagement", false);
                if (callMethodBeforeUpdate)
                {
                    try
                    {
                        ThisAddIn.Instance.Application.Run("WordLXBeforeDocIDUpdate");
                    }
                    catch (Exception ex)
                    {
                    }
                }
            }
            else if (!onlyOnUpdate)
            {
                bool callMethodBeforeInsert = SettingsManager.GetSettingAsBool("BeforeDocIDInsert", "DocumentManagement", false);
                if (callMethodBeforeInsert)
                {
                    try
                    {
                        ThisAddIn.Instance.Application.Run("WordLXBeforeDocIDInsert");
                    }
                    catch (Exception ex)
                    {
                    }
                }
            }

            return update;
        }

        public static bool SkipDocIdAutoUpdateForThisDoc()
        {
            string prop = DocumentPropertyUtil.ReadProperty(NO_AUTO_UPDATE);
            if (prop != null && prop == bool.TrueString)
            {
                return true;
            }

            return false;
        }

        private void SetSkipDocIdAutoUpdateForThisDoc(bool noAutoUpdate)
        {
            DocumentPropertyUtil.SaveShortProperty(NO_AUTO_UPDATE, noAutoUpdate.ToString(), true);
        }

        public IManageSettings GetIManageSettings()
        {
            try
            {
                IManageSettings result = new IManageSettings();
                result.DocumentNumber = this.CbIMIncludeDocumentNumber.IsChecked == true;
                result.AuthorId = this.CbIMIncludeAuthorId.IsChecked == true;
                result.ClientId = this.CbIMIncludeClientId.IsChecked == true;
                result.DatabaseName = this.CbIMIncludeDatabaseName.IsChecked == true;
                result.DocumentDescription = this.CbIMIncludeDocumentDescription.IsChecked == true;
                result.DocumentVersion = this.CbIMIncludeVersion.IsChecked == true;
                result.LastSavedDate = this.CbIMIncludeLastSavedDate.IsChecked == true;
                result.Matter = this.CbIMIncludeMatter.IsChecked == true;

                if (RbEveryPage.IsChecked == true)
                {
                    result.Position = DocIdSettings.EveryPage;
                }
                if (RbFirstPage.IsChecked == true)
                {
                    result.Position = DocIdSettings.FirstPage;
                }
                if (RbLastPage.IsChecked == true)
                {
                    result.Position = DocIdSettings.LastPage;
                }
                if (RbSkipFirstPage.IsChecked == true)
                {
                    result.Position = DocIdSettings.SkipFirstPage;
                }

                return result;
            }
            catch (Exception ex)
            {
                new InfowareInformationWindow($"{ex.Message}\n\n{ex.StackTrace}", "Get DMS Settings", WordDefaultIconType.Error);
                return null;
            }
        }

        private void LoadIManageSettings(IManageSettings settings)
        {
            if (settings != null)
            {
                this.CbIMIncludeDocumentNumber.IsChecked = settings.DocumentNumber;
                this.CbIMIncludeAuthorId.IsChecked = settings.AuthorId;
                this.CbIMIncludeClientId.IsChecked = settings.ClientId;
                this.CbIMIncludeDatabaseName.IsChecked = settings.DatabaseName;
                this.CbIMIncludeDocumentDescription.IsChecked = settings.DocumentDescription;
                this.CbIMIncludeVersion.IsChecked = settings.DocumentVersion;
                this.CbIMIncludeLastSavedDate.IsChecked = settings.LastSavedDate;
                this.CbIMIncludeMatter.IsChecked = settings.Matter;

                if (settings.Position == DocIdSettings.EveryPage)
                {
                    RbEveryPage.IsChecked = true;
                }
                if (settings.Position == DocIdSettings.FirstPage)
                {
                    RbFirstPage.IsChecked = true;
                }
                if (settings.Position == DocIdSettings.LastPage)
                {
                    RbLastPage.IsChecked = true;
                }
                if (settings.Position == DocIdSettings.SkipFirstPage)
                {
                    RbSkipFirstPage.IsChecked = true;
                }
            }
        }

        private void ApplyDocsIDStyle(Range range)
        {
            Microsoft.Office.Interop.Word.Style docsIDStyle = null;
            try
            {
                docsIDStyle = ThisAddIn.Instance.Application.ActiveDocument.Styles["DocsID"];
            }
            catch (Exception)
            {
            }

            if (docsIDStyle == null)
            {
                string fontName = SettingsManager.GetSetting("DocIDFontName", "Tools", null);
                float fontSize = SettingsManager.GetSettingAsFloat("DocIDFontSize", "Tools", 8);
                int color = SettingsManager.GetSettingAsInt("DocIDFontColour", "Tools", 8388608);

                docsIDStyle = ThisAddIn.Instance.Application.ActiveDocument.Styles.Add("DocsID", WdStyleType.wdStyleTypeParagraph);
                docsIDStyle.set_BaseStyle(WdBuiltinStyle.wdStyleNormal);
                if (!string.IsNullOrWhiteSpace(fontName))
                {
                    docsIDStyle.Font.Name = fontName;
                }

                docsIDStyle.Font.Size = fontSize;
                docsIDStyle.Font.Color = (WdColor)color;
            }
            range.set_Style(WdBuiltinStyle.wdStyleDefaultParagraphFont);
            RangeUtil.SetParagraphStyle(range, docsIDStyle);
        }

        private void InsertDocumentIdOnEveryPage(string documentId)
        {
            ThisAddIn.Instance.Application.ScreenUpdating = false;
            if (ThisAddIn.Instance.IsFunctionForLogging("ScreenUpdating"))
                Logger.Log("Disabled Screen Updating (InfowareDocIdWindow.xaml.cs > InsertDocumentIdOnEveryPage)");
            foreach (Section wordSection in GetDocumentSections())
            {
                if (Utils.IsSectionContinous(wordSection, nativeDocument))
                {
                    continue;
                }
                string controlName;
                Range range;
                HeaderFooter firstPageFooter = wordSection.Footers[WdHeaderFooterIndex.wdHeaderFooterFirstPage];
                if (firstPageFooter.Exists && !firstPageFooter.LinkToPrevious)
                {
                    controlName = GetNewDocumentIdControlName();
                    range = firstPageFooter.Range;
                    range.Collapse(WdCollapseDirection.wdCollapseEnd);
                    if (!RangeIsEmpty(firstPageFooter.Range))
                    {
                        range.InsertParagraphAfter();
                        range = range.Paragraphs.Last.Next().Range;
                    }

                    RichTextContentControl firstPageFooterDocumentIdControl = vstoDocument.Controls.AddRichTextContentControl(range, controlName);
                    firstPageFooterDocumentIdControl.Tag = DOCUMENT_ID_CONTROL_TAG;
                    firstPageFooterDocumentIdControl.Title = controlName;
                    firstPageFooterDocumentIdControl.Range.InsertAfter(documentId);
                    ApplyDocsIDStyle(firstPageFooterDocumentIdControl.Range);
                }

                HeaderFooter primaryFooter = wordSection.Footers[WdHeaderFooterIndex.wdHeaderFooterPrimary];
                if (!primaryFooter.LinkToPrevious)
                {
                    controlName = GetNewDocumentIdControlName();
                    range = primaryFooter.Range;
                    range.Collapse(WdCollapseDirection.wdCollapseEnd);
                    if (!RangeIsEmpty(primaryFooter.Range))
                    {
                        range.InsertParagraphAfter();
                        range = range.Paragraphs.Last.Next().Range;
                    }

                    RichTextContentControl primaryFooterDocumentIdControl = vstoDocument.Controls.AddRichTextContentControl(range, controlName);
                    primaryFooterDocumentIdControl.Tag = DOCUMENT_ID_CONTROL_TAG;
                    primaryFooterDocumentIdControl.Title = controlName;
                    primaryFooterDocumentIdControl.Range.InsertAfter(documentId);
                    ApplyDocsIDStyle(primaryFooterDocumentIdControl.Range);
                }

                HeaderFooter evenPageFooter = wordSection.Footers[WdHeaderFooterIndex.wdHeaderFooterEvenPages];
                if (evenPageFooter.Exists && !evenPageFooter.LinkToPrevious)
                {
                    controlName = GetNewDocumentIdControlName();
                    range = evenPageFooter.Range;
                    range.Collapse(WdCollapseDirection.wdCollapseEnd);
                    if (!RangeIsEmpty(evenPageFooter.Range))
                    {
                        range.InsertParagraphAfter();
                        range = range.Paragraphs.Last.Next().Range;
                    }

                    RichTextContentControl evenPageFooterDocumentControlId = vstoDocument.Controls.AddRichTextContentControl(range, controlName);
                    evenPageFooterDocumentControlId.Tag = DOCUMENT_ID_CONTROL_TAG;
                    evenPageFooterDocumentControlId.Title = controlName;
                    evenPageFooterDocumentControlId.Range.InsertAfter(documentId);
                    ApplyDocsIDStyle(evenPageFooterDocumentControlId.Range);
                }
            }
            ThisAddIn.Instance.Application.ScreenUpdating = true;
            if (ThisAddIn.Instance.IsFunctionForLogging("ScreenUpdating"))
                Logger.Log("Enabled Screen Updating (InfowareDocIdWindow.xaml.cs > InsertDocumentIdOnEveryPage)");
        }

        private void InsertDocumentIdOnFirstPage(string documentId)
        {
            DocumentUtil.GetActiveDocument().PageSetup.DifferentFirstPageHeaderFooter = -1;
            ThisAddIn.Instance.Application.ScreenUpdating = false;
            if (ThisAddIn.Instance.IsFunctionForLogging("ScreenUpdating"))
                Logger.Log("Disabled Screen Updating (InfowareDocIdWindow.xaml.cs > InsertDocumentIdOnFirstPage)");
            foreach (Section wordSection in GetDocumentSections())
            {
                if (Utils.IsSectionContinous(wordSection, nativeDocument))
                {
                    continue;
                }
                HeaderFooter firstPageFooter = wordSection.Footers[WdHeaderFooterIndex.wdHeaderFooterFirstPage];

                string controlName = GetNewDocumentIdControlName();
                Range range = firstPageFooter.Range;
                range.Collapse(WdCollapseDirection.wdCollapseEnd);
                if (!RangeIsEmpty(firstPageFooter.Range))
                {
                    range.InsertParagraphAfter();
                    range = range.Paragraphs.Last.Next().Range;
                }

                RichTextContentControl firstPageFooterDocumentIdControl = vstoDocument.Controls.AddRichTextContentControl(range, controlName);
                firstPageFooterDocumentIdControl.Tag = DOCUMENT_ID_CONTROL_TAG;
                firstPageFooterDocumentIdControl.Title = controlName;

                firstPageFooterDocumentIdControl.Range.InsertAfter(documentId);
                ApplyDocsIDStyle(firstPageFooterDocumentIdControl.Range);
                break;
            }
            ThisAddIn.Instance.Application.ScreenUpdating = true;
            if (ThisAddIn.Instance.IsFunctionForLogging("ScreenUpdating"))
                Logger.Log("Enabled Screen  Updating (InfowareDocIdWindow.xaml.cs > InsertDocumentIdOnFirstPage)");
        }

        private void InsertDocumentIdOnSkipFirstPage(string documentId)
        {
            bool firstSection = true;
            ThisAddIn.Instance.Application.ScreenUpdating = false;
            if (ThisAddIn.Instance.IsFunctionForLogging("ScreenUpdating"))
                Logger.Log("Disabled Screen Updating (InfowareDocIdWindow.xaml.cs > InsertDocumentIdOnSkipFirstPage)");
            foreach (Section wordSection in GetDocumentSections())
            {
                if (Utils.IsSectionContinous(wordSection, nativeDocument))
                {
                    continue;
                }

                HeaderFooter primaryFooter = wordSection.Footers[WdHeaderFooterIndex.wdHeaderFooterPrimary];
                string controlName;
                Range range;
                if (!primaryFooter.LinkToPrevious)
                {
                    controlName = GetNewDocumentIdControlName();
                    range = primaryFooter.Range;
                    range.Collapse(WdCollapseDirection.wdCollapseEnd);
                    if (!RangeIsEmpty(primaryFooter.Range))
                    {
                        range.InsertParagraphAfter();
                        range = range.Paragraphs.Last.Next().Range;
                    }

                    RichTextContentControl primaryFooterDocumentIdControl = vstoDocument.Controls.AddRichTextContentControl(range, controlName);
                    primaryFooterDocumentIdControl.Tag = DOCUMENT_ID_CONTROL_TAG;
                    primaryFooterDocumentIdControl.Title = controlName;

                    primaryFooterDocumentIdControl.Range.InsertAfter(documentId);
                    ApplyDocsIDStyle(primaryFooterDocumentIdControl.Range);
                }

                if (firstSection)
                {
                    wordSection.PageSetup.DifferentFirstPageHeaderFooter = -1;
                }
                else
                {
                    HeaderFooter firstPageFooter = wordSection.Footers[WdHeaderFooterIndex.wdHeaderFooterFirstPage];
                    if (firstPageFooter.Exists && !firstPageFooter.LinkToPrevious)
                    {
                        controlName = GetNewDocumentIdControlName();
                        range = firstPageFooter.Range;
                        range.Collapse(WdCollapseDirection.wdCollapseEnd);
                        if (!RangeIsEmpty(firstPageFooter.Range))
                        {
                            range.InsertParagraphAfter();
                            range = range.Paragraphs.Last.Next().Range;
                        }

                        RichTextContentControl firstPageFooterDocumentIdControl = vstoDocument.Controls.AddRichTextContentControl(range, controlName);
                        firstPageFooterDocumentIdControl.Tag = DOCUMENT_ID_CONTROL_TAG;
                        firstPageFooterDocumentIdControl.Title = controlName;

                        firstPageFooterDocumentIdControl.Range.InsertAfter(documentId);
                        ApplyDocsIDStyle(firstPageFooterDocumentIdControl.Range);
                    }
                }

                firstSection = false;
                HeaderFooter evenPageFooter = wordSection.Footers[WdHeaderFooterIndex.wdHeaderFooterEvenPages];
                if (evenPageFooter.Exists && !evenPageFooter.LinkToPrevious)
                {
                    controlName = GetNewDocumentIdControlName();
                    range = evenPageFooter.Range;
                    range.Collapse(WdCollapseDirection.wdCollapseEnd);
                    if (!RangeIsEmpty(evenPageFooter.Range))
                    {
                        range.InsertParagraphAfter();
                        range = range.Paragraphs.Last.Next().Range;
                    }

                    RichTextContentControl evenPageFooterDocumentControlId = vstoDocument.Controls.AddRichTextContentControl(range, controlName);
                    evenPageFooterDocumentControlId.Tag = DOCUMENT_ID_CONTROL_TAG;
                    evenPageFooterDocumentControlId.Title = controlName;
                    evenPageFooterDocumentControlId.Range.InsertAfter(documentId);
                    ApplyDocsIDStyle(evenPageFooterDocumentControlId.Range);
                }


            }
            ThisAddIn.Instance.Application.ScreenUpdating = true;
            if (ThisAddIn.Instance.IsFunctionForLogging("ScreenUpdating"))
                Logger.Log("Enabled Screen Updating (InfowareDocIdWindow.xaml.cs > InsertDocumentIdOnSkipFirstPage)");
        }

        private void InsertDocumentIdOnLastPage(string documentId)
        {
            Range documentRange = DocumentUtil.GetActiveDocument().Content;

            string controlName = GetNewDocumentIdControlName();
            Range range = documentRange;
            range.Collapse(WdCollapseDirection.wdCollapseEnd);
            range.InsertParagraphAfter();
            range = range.Paragraphs.Last.Next().Range;

            RichTextContentControl documentEndDocumentIdControl = vstoDocument.Controls.AddRichTextContentControl(range, controlName);
            documentEndDocumentIdControl.Tag = DOCUMENT_ID_CONTROL_TAG;
            documentEndDocumentIdControl.Title = controlName;

            documentEndDocumentIdControl.Range.InsertAfter(documentId);
            ApplyDocsIDStyle(documentEndDocumentIdControl.Range);
        }

        private bool RangeIsEmpty(Range range)
        {
            string text = range.Text;
            text = text.Trim('\r', '\n');
            return text.Length == 0;
        }

        // returns if any documentId was found.
        private bool RemoveDocumentId(bool detectOnly = false)
        {
            if (Globals.ThisAddIn.IsFunctionForLogging("DocID"))
                Logger.Log($"Enter RemoveDocumentId(): {nativeDocument?.Name}/{nativeDocument?.DocID}");

            Globals.ThisAddIn.Application.ScreenUpdating = false;
            if (ThisAddIn.Instance.IsFunctionForLogging("ScreenUpdating"))
                Logger.Log("Disabled Screen Updating (InfowareDocIdWindow.xaml.cs > RemoveDocId)");

            bool result = false;
            using (new DocumentUndoTransaction("DocumentId removal"))
            {
                RemoveOtherDocIds();

                var controlsToRemove = new List<Microsoft.Office.Interop.Word.ContentControl>();

                foreach (Microsoft.Office.Interop.Word.ContentControl genericControl in nativeDocument.ContentControls)
                {
                    if (genericControl.Type == WdContentControlType.wdContentControlRichText && genericControl.Tag == DOCUMENT_ID_CONTROL_TAG)
                    {
                        controlsToRemove.Add(genericControl);
                    }
                }

                foreach (HeaderFooter footer in GetFootersOfType(WdHeaderFooterIndex.wdHeaderFooterFirstPage))
                {
                    foreach (Microsoft.Office.Interop.Word.ContentControl control in footer.Range.ContentControls)
                    {
                        if (control.Type == WdContentControlType.wdContentControlRichText && control.Tag == DOCUMENT_ID_CONTROL_TAG)
                        {
                            controlsToRemove.Add(control);
                        }
                    }
                }

                foreach (HeaderFooter footer in GetFootersOfType(WdHeaderFooterIndex.wdHeaderFooterPrimary))
                {
                    foreach (Microsoft.Office.Interop.Word.ContentControl control in footer.Range.ContentControls)
                    {
                        if (control.Type == WdContentControlType.wdContentControlRichText && control.Tag == DOCUMENT_ID_CONTROL_TAG)
                        {
                            controlsToRemove.Add(control);
                        }
                    }
                }

                foreach (HeaderFooter footer in GetFootersOfType(WdHeaderFooterIndex.wdHeaderFooterEvenPages))
                {
                    foreach (Microsoft.Office.Interop.Word.ContentControl control in footer.Range.ContentControls)
                    {
                        if (control.Type == WdContentControlType.wdContentControlRichText && control.Tag == DOCUMENT_ID_CONTROL_TAG)
                        {
                            controlsToRemove.Add(control);
                        }
                    }
                }

                if (detectOnly)
                {
                    if (Globals.ThisAddIn.IsFunctionForLogging("DocID"))
                        Logger.Log($"Exit RemoveDocumentId() - detect only: {nativeDocument?.Name}/{nativeDocument?.DocID}");
                    Globals.ThisAddIn.Application.ScreenUpdating = true;
                    if (ThisAddIn.Instance.IsFunctionForLogging("ScreenUpdating"))
                        Logger.Log("Enabled Screen Updating (InfowareDocIdWindow.xaml.cs > RemoveDocId)");
                    return controlsToRemove.Count > 0;
                }

                foreach (Microsoft.Office.Interop.Word.ContentControl control in controlsToRemove)
                {
                    /*
                     * Simply removing a control doesn't remove its content
                     * https://social.msdn.microsoft.com/Forums/en-US/8d6060c6-7b46-45b9-811e-b64f8dad83f0/word-deleting-a-content-control-leaves-an-extra-paragraph-mark?forum=vsto
                     */

                    // Works when the content is set via control.Range.Text
                    //control.Range.Delete();

                    // Works every time, even for data inserted via control.Range.InnerXML
                    try
                    {
                        Range range = control.Range.Paragraphs.First.Range;

                        control.Delete(true);

                        vstoDocument.Controls.Remove(control);
                        range.Collapse(WdCollapseDirection.wdCollapseStart);
                        range.MoveStart(WdUnits.wdCharacter, -1);
                        range.Delete();
                        result = true;
                    }
                    catch { };
                }
            }

            Globals.ThisAddIn.Application.ScreenRefresh();
            Globals.ThisAddIn.Application.ScreenUpdating = true;
            if (ThisAddIn.Instance.IsFunctionForLogging("ScreenUpdating"))
                Logger.Log("Enabled Screen Updating (InfowareDocIdWindow.xaml.cs > RemoveDocId)");

            if (Globals.ThisAddIn.IsFunctionForLogging("DocID"))
                Logger.Log($"Exit RemoveDocumentId(): {nativeDocument?.Name}/{nativeDocument?.DocID}");

            return result;
        }

        private void RemoveOtherDocIds()
        {
            string all = string.Join(",", Enum.GetValues(typeof(DocIDRemovalProviders)).Cast<int>().Select(x => x.ToString()).ToArray());
            string remove = SettingsManager.GetSetting("DocIDRemoveProviders", "Tools", all);

            foreach (string nrStr in remove.Split(','))
            {
                if (int.TryParse(nrStr, out int nr))
                {
                    switch (nr)
                    {
                        case (int)DocIDRemovalProviders.Enterprise:
                            RemoveWordLXEnterpriseDocId();
                            break;
                        case (int)DocIDRemovalProviders.Fasken:
                            RemoveFaskenDocId();
                            break;
                        case (int)DocIDRemovalProviders.Wildeboer:
                            RemoveWildeboerDocId();
                            break;
                    }
                }
            }
        }

        private void RemoveFaskenDocId()
        {
            Microsoft.Office.Interop.Word.Document doc = ThisAddIn.Instance.Application.ActiveDocument;

            List<Field> toDelete = new List<Field>();
            foreach (Section section in doc.Sections)
            {
                foreach (HeaderFooter footer in section.Footers)
                {
                    foreach (Field field in footer.Range.Fields)
                    {
                        if (field.Code.Text.Trim() == "DOCPROPERTY DMDocID")
                        {
                            toDelete.Add(field);
                        }
                    }
                }
            }

            foreach (Field field in toDelete)
            {
                field.Delete();
            }
            DocumentPropertyUtil.RemoveShortProperty("DmAuthor");
            DocumentPropertyUtil.RemoveShortProperty("DmClientNum");
            DocumentPropertyUtil.RemoveShortProperty("DmDatabase");
            DocumentPropertyUtil.RemoveShortProperty("DmDocDescription");
            DocumentPropertyUtil.RemoveShortProperty("DmDocID");
            DocumentPropertyUtil.RemoveShortProperty("DmDocNum");
            DocumentPropertyUtil.RemoveShortProperty("DmDocType");
            DocumentPropertyUtil.RemoveShortProperty("DmMatterNum");
            DocumentPropertyUtil.RemoveShortProperty("DMSFooterStatus50");
            DocumentPropertyUtil.RemoveShortProperty("DmTypist");
            DocumentPropertyUtil.RemoveShortProperty("DmVersionNum");
            DocumentPropertyUtil.RemoveShortProperty("FrTStylesDone");
            DocumentPropertyUtil.RemoveShortProperty("TemplateCode50");
            DocumentPropertyUtil.RemoveShortProperty("TemplateVersion50");

        }

        private void RemoveWildeboerDocId()
        {
            Microsoft.Office.Interop.Word.Document doc = ThisAddIn.Instance.Application.ActiveDocument;
            var properties = (doc.BuiltInDocumentProperties as DocumentProperties);
            if (properties != null)
            {
                try
                {
                    string subject = properties["Subject"]?.Value as string;
                    if (subject != null)
                    {
                        bool found = false;
                        ThisAddIn.Instance.Application.ScreenUpdating = false;
                        if (ThisAddIn.Instance.IsFunctionForLogging("ScreenUpdating"))
                            Logger.Log("Disabled Screen Updating (InfowareDocIdWindow.xaml.cs > RemoveWildeboerDocId)");
                        foreach (Section section in doc.Sections)
                        {
                            foreach (HeaderFooter footer in section.Footers)
                            {
                                if (footer.Exists)
                                {
                                    Range range = footer.Range;
                                    Find find = range.Find;
                                    find.Forward = true;
                                    find.Format = false;
                                    List<Range> ranges = new List<Range>();

                                    bool doContinue = true;

                                    while (doContinue)
                                    {
                                        find.Execute(subject);
                                        doContinue = find.Found;
                                        if (find.Found)
                                        {
                                            found = true;
                                            ranges.Add(range.Duplicate);
                                        }
                                    }

                                    foreach (Range range2 in ranges)
                                    {
                                        if (range2.Text == subject)
                                        {
                                            try
                                            {
                                                if (range2.Tables.Count > 0 && range2.Tables[1].Rows.Count == 1 && range2.Tables[1].Rows[1].Cells.Count == 1)
                                                {
                                                    range2.Tables[1].Delete();
                                                }
                                                else
                                                {
                                                    range2.Text = "";
                                                }
                                            }
                                            catch { }
                                        }

                                    }
                                }
                            }
                        }
                        ThisAddIn.Instance.Application.ScreenUpdating = true;
                        if (ThisAddIn.Instance.IsFunctionForLogging("ScreenUpdating"))
                            Logger.Log("Enabled Screen Updating (InfowareDocIdWindow.xaml.cs > RemoveWildeboerDocId)");
                        if (found)
                        {
                            properties["Subject"].Value = string.Empty;
                        }
                    }
                }
                catch { }
            }
        }

        private Sections GetDocumentSections()
        {
            return DocumentUtil.GetActiveDocument().Sections;
        }

        private string GetDocumentId()
        {
            try
            {
                SetSkipDocIdAutoUpdateForThisDoc(false);
                if (!usesIManage)
                {

                    string fileName = null;
                    string authorId = null;
                    string lastSavedDate = null;
                    bool documentWasSaved = DocumentUtil.TryGetDateLastSaved(out DateTime dateLastSaved);
                    bool useFullName = SettingsManager.GetSettingAsBool("EnableDocIDFilePath", "Tools", false);
                    //get filename
                    if (documentWasSaved && !useFullName)
                    {
                        fileName = DocumentUtil.GetDocumentName();
                    }
                    else
                    {
                        fileName = DocumentUtil.GetActiveDocument().FullName;
                    }

                    //get authorId 
                    if ((bool)CbIncludeAuthorId.IsChecked)
                    {
                        bool gotAuthorInfo = TryGetAuthorInfo(out AuthorInfo authorInfo);

                        if (!gotAuthorInfo)
                        {
                            gotAuthorInfo = DocumentUtil.TryGetWordAuthor(out string author);
                            if (!string.IsNullOrWhiteSpace(author))
                            {
                                authorId = author;
                            }
                        }
                        else
                        {
                            authorId = authorInfo.ToString();
                        }
                    }

                    //get lastSavedDate
                    if ((bool)CbIncludeLastSavedDate.IsChecked && (documentWasSaved || UseDateTimeNow))
                    {
                        if (UseDateTimeNow)
                        {
                            dateLastSaved = DateTime.Now;
                        }

                        string lastSavedDateFormat = SettingsManager.GetSetting("DocIDLastSavedDateFormat", "Tools");
                        try
                        {
                            if (lastSavedDateFormat == null)
                            {
                                throw new FormatException();
                            }

                            lastSavedDate = dateLastSaved.ToString(lastSavedDateFormat);
                        }
                        catch
                        {
                            lastSavedDate = dateLastSaved.ToLongDateString() + " " + dateLastSaved.ToLongTimeString();
                        }
                    }

                    var language = MultiLanguage.MLanguageUtil.ActiveDocumentLanguage;
                    string docIDPositions = SettingsManager.GetSetting("DocIdFieldPositions", "Tools", FILENAME_REPLACEMENT + AUTHORID_REPLACEMENT + LASTSAVEDDATE_REPLACEMENT, language);
                    string authorIdPrefix = SettingsManager.GetSetting("AuthorIdPrefix", "Tools", " - ", language);
                    string lastSavedDatePrefix = SettingsManager.GetSetting("LastSavedDatePrefix", "Tools", " - ", language);
                    bool authorIdUseAsSuffix = SettingsManager.GetSettingAsBool("AuthorIdUseAsSuffix", "Tools", false, language);
                    bool lastSavedDateUseAsSuffix = SettingsManager.GetSettingAsBool("LastSavedDateUseAsSuffix", "Tools", false, language);


                    string fileNameStr = fileName ?? string.Empty;
                    string authorIdStr = authorId != null ? (authorIdUseAsSuffix ? authorId + authorIdPrefix : authorIdPrefix + authorId) : string.Empty;
                    string lastSavedDateStr = lastSavedDate != null ? (lastSavedDateUseAsSuffix ? lastSavedDate + lastSavedDatePrefix : lastSavedDatePrefix + lastSavedDate) : string.Empty;

                    return docIDPositions.Replace(FILENAME_REPLACEMENT, fileNameStr)
                        .Replace(AUTHORID_REPLACEMENT, authorIdStr)
                        .Replace(LASTSAVEDDATE_REPLACEMENT, lastSavedDateStr);
                }
                else
                {
                    var settings = GetIManageSettings();
                    IManageUtilities.SaveSettings(settings);
                    return IManageUtilities.GetDocumentID(settings);
                }
            }
            catch (Exception ex)
            {
                new InfowareInformationWindow($"{ex.Message}\n\n{ex.StackTrace}", "InfowareDocIdWindow.GetDocumentId()", WordDefaultIconType.Error);
                return null;
            }
        }

        private static string GetNewDocumentIdControlName()
        {
            return string.Format(DOCUMENT_ID_CONTROL_NAME_TEMPLATE, Guid.NewGuid().ToString());
        }

        private static bool TryGetAuthorInfo(out AuthorInfo authorInfo)
        {
            object authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);

            if (authKey != null)
            {
                var client = new RestClient(ConfigSettings.WebApiUrl);

                var request = new RestRequest("api/WebApi/GetCurrentAuthor?authKey=" + authKey, Method.GET);
                IRestResponse response = client.Execute(request);

                if (!response.IsSuccessful)
                {
                    authorInfo = null;
                    return false;
                }

                JObject content;

                try
                {
                    content = JsonConvert.DeserializeObject<JObject>(response.Content);
                }
                catch (JsonReaderException)
                {
                    // The response didn't contain valid JSON

                    authorInfo = null;
                    return false;
                }

                int statusCode = content["StatusCode"].ToObject<int>();

                if (statusCode == 200)
                {
                    JToken data = content["Data"];

                    string firstName = data["FirstName"].ToString();
                    string middleName = data["MiddleName"].ToString();
                    string lastName = data["LastName"].ToString();
                    string prefix = data["Prefix"].ToString();
                    string suffix = data["Suffix"].ToString();

                    authorInfo = new AuthorInfo(firstName, middleName, lastName, prefix, suffix);

                    return true;
                }
                else if (statusCode == 300 && content["Message"].ToString() == AUTHOR_NOT_SET_API_MESSAGE)
                {
                    authorInfo = null;
                    return false;
                }
            }

            authorInfo = null;
            return false;
        }

        private List<HeaderFooter> GetFootersOfType(WdHeaderFooterIndex footerId)
        {
            var footers = new List<HeaderFooter>();

            foreach (Section section in GetDocumentSections())
            {
                footers.Add(section.Footers[footerId]);
            }

            return footers;
        }

        private void OnCancelButtonClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private class AuthorInfo
        {
            public string FirstName { get; set; }

            public string MiddleName { get; set; }

            public string LastName { get; set; }

            public string Prefix { get; set; }

            public string Suffix { get; set; }

            public AuthorInfo(string firstName, string middleName, string lastName, string prefix = null, string suffix = null)
            {
                FirstName = firstName;
                MiddleName = middleName ?? "";
                LastName = lastName;
                Prefix = prefix ?? "";
                Suffix = suffix ?? "";
            }

            public override string ToString()
            {
                var authorInfo = new StringBuilder();

                if (!string.IsNullOrWhiteSpace(Prefix))
                {
                    authorInfo.Append(Prefix).Append(' ');
                }

                authorInfo.Append(FirstName);

                if (!string.IsNullOrWhiteSpace(MiddleName))
                {
                    authorInfo.Append(' ').Append(MiddleName);
                }

                authorInfo.Append(' ').Append(LastName);

                if (!string.IsNullOrWhiteSpace(Suffix))
                {
                    authorInfo.Append(' ').Append(Suffix);
                }

                return authorInfo.ToString();
            }
        }
    }
}
