﻿using InfowareVSTO.Common.Language;
using InfowareVSTO.Common.Models;
using InfowareVSTO.Common.OfflineMode;
using InfowareVSTO.VSTOAuthor;
using Microsoft.Office.Interop.Word;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace InfowareVSTO.Windows
{
    /// <summary>
    /// Interaction logic for TemplateFinder.xaml
    /// </summary>
    public partial class TemplateFinder : InfowareWindow
    {
        public TypedTemplateListModel Result { get; set; }

        public TemplateDownloadModel DownloadedTemplate { get; set; }

        private List<ExtendedTemplateListModel> favoriteTemplates;
        public TemplateFinder()
        {
            InitializeComponent();

            PopulateTemplateList();

            RefreshFavoriteTemplates();
        }

        public void PopulateTemplateList()
        {
            var currentAuthorSettings = ManageUsers.CurrentAuthorSettings;
            List<TypedTemplateListModel> templates = AdminPanelWebApi.GetAllTemplates(currentAuthorSettings.LanguageId, currentAuthorSettings.LocationId);
            this.TemplatesComboBox.ItemsSource = templates;
        }

        public void RefreshFavoriteTemplates()
        {
            favoriteTemplates = AdminPanelWebApi.GetFavoriteTemplates();
            this.FavoriteTemplatesDataGrid.ItemsSource = favoriteTemplates;
        }

        private void TemplatesComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            UpdateCheckboxValue();
        }

        private void UpdateCheckboxValue()
        {
            var selectedItem = TemplatesComboBox.SelectedItem as TemplateListModel;
            if (selectedItem == null)
            {
                AddFavorite.IsEnabled = false;
                AddFavorite.IsChecked = false;
                return;
            }
            else
            {
                AddFavorite.IsEnabled = true;
            }

            int id = (TemplatesComboBox.SelectedItem as TemplateListModel).Id;

            if (favoriteTemplates?.FirstOrDefault(x => x.Id == id) != null)
            {
                AddFavorite.IsChecked = true;
            }
            else
            {
                AddFavorite.IsChecked = false;
            }
        }

        private void Open1_Click(object sender, RoutedEventArgs e)
        {
            var selectedItem = TemplatesComboBox.SelectedItem as TypedTemplateListModel;
            if (selectedItem != null)
            {
                Result = selectedItem;
                this.Close();
                this.DownloadedTemplate = Utils.DownloadAndGetTemplate(selectedItem, true);
            }
        }

        private void Open2_Click(object sender, RoutedEventArgs e)
        {
            var selectedItem = FavoriteTemplatesDataGrid.SelectedItem as TypedTemplateListModel;
            if (selectedItem != null)
            {
                Result = selectedItem;
                try
                {
                    WebserviceCall.ThrowWhenForbidden = true;
                 
                    this.DownloadedTemplate = Utils.DownloadAndGetTemplate(selectedItem, true, taskBeforeOpen: ()=> { this.Close(); });
                 
                    WebserviceCall.ThrowWhenForbidden = false;
                }
                catch (UnauthorizedAccessException ex)
                {
                    WebserviceCall.ThrowWhenForbidden = false;
                    var dialog = new InfowareConfirmationWindow(LanguageManager.GetTranslation(LanguageConstants.ThisTemplateIsNoLongerAvailable, "This template is no longer available. Delete it from your favorites?"), LanguageManager.GetTranslation(LanguageConstants.TemplateNotAvailable, "Template not available"), MessageBoxButton.YesNo);
                    dialog.ShowDialog(this);
                    if (dialog.Result == MessageBoxResult.Yes)
                    {
                        AdminPanelWebApi.AddTemplateToFavorite(selectedItem.Id, false);
                        RefreshFavoriteTemplates();
                        UpdateCheckboxValue();
                    }
             
                    return;
                }
            }
        }

        private void TemplateFinder_CloseOnNeDoc(Document Doc)
        {
            this.Close();
        }

        private void FavoriteTemplatesDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var selectedItem = FavoriteTemplatesDataGrid.SelectedItem as ExtendedTemplateListModel;
            this.Description.Text = selectedItem?.Description ?? string.Empty;
        }

        private void CancelBtn_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void RemoveUserFromFavourite_Click(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;

            if (button != null)
            {
                TemplateListModel template = button.DataContext as TemplateListModel;
                if (template != null)
                {
                    AdminPanelWebApi.AddTemplateToFavorite(template.Id, false);
                    RefreshFavoriteTemplates();
                    UpdateCheckboxValue();
                }
            }
        }

        private void AddFavorite_Click(object sender, RoutedEventArgs e)
        {
            var selectedItem = TemplatesComboBox.SelectedItem as TemplateListModel;
            bool add = this.AddFavorite.IsChecked == true;
            if (selectedItem != null)
            {
                int id = (TemplatesComboBox.SelectedItem as TemplateListModel).Id;
                AdminPanelWebApi.AddTemplateToFavorite(id, add);
                RefreshFavoriteTemplates();
            }
        }

        private void DataGridRow_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Open2_Click(null, null);
        }
    }
}
