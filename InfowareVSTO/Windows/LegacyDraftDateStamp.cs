﻿using InfowareVSTO.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Word;
using InfowareVSTO.Util;

namespace InfowareVSTO.Windows
{
    public class LegacyDraftDateStamp
    {
        private const string DOC_PROPERTY = "Draft Number";
        private const string BOOKMARK_NAME = "draftnum";
        private const string DRAFT_DATE_PREFIX = "DraftDate";
        public bool HasLegacyDraftDateStamp { get; set; }
        public int? DraftNumber { get; set; }
        public DraftDateStampLocation Location
        {
            get
            {
                if (Shapes != null && Shapes.Count > 0)
                {
                    Shape shape = Shapes[0];
                    if (shape.Top == (float)WdShapePosition.wdShapeCenter)
                    {
                        if (shape.Left > 70)
                        {
                            return DraftDateStampLocation.Right;
                        }
                        else
                        {
                            return DraftDateStampLocation.Left;
                        }
                    }
                    else
                    {
                        return DraftDateStampLocation.Top;
                    }
                }

                return DraftDateStampLocation.Left;
            }
        }

        public DraftDateStampDateType DateType { get; set; }
        public bool IncludeTime { get; set; }
        public List<Shape> Shapes { get; set; }
        public Bookmark Bookmark { get; set; }

        public void Read()
        {
            Shapes = new List<Shape>();
            Document document = ThisAddIn.Instance.Application.ActiveDocument;
            try
            {
                Bookmark = document.Bookmarks[BOOKMARK_NAME];
            }
            catch { }
            string draftNumStr = DocumentPropertyUtil.ReadProperty(DOC_PROPERTY);
            if (draftNumStr != null && int.TryParse(draftNumStr, out int draftNum))
            {
                DraftNumber = draftNum;
            }

            ThisAddIn.Instance.Application.ScreenUpdating = false;
            if (ThisAddIn.Instance.IsFunctionForLogging("ScreenUpdating"))
                Logger.Log("Disabled Screen Updating (LegacyDraftDateStamp.xaml.cs > Read)");
            foreach (Section section in document.Sections)
            {
                foreach (HeadersFooters headersFooters in new List<HeadersFooters>() { section.Headers, section.Footers })
                {
                    foreach (HeaderFooter headerFooter in headersFooters)
                    {
                        if (headerFooter.Exists && headerFooter.Shapes.Count > 0)
                        {
                            foreach (Shape shape in headerFooter.Shapes)
                            {
                                if (IsDraftDateStamp(shape))
                                {
                                    Shapes.Add(shape);
                                }
                            }
                        }
                    }
                }
            }
            ThisAddIn.Instance.Application.ScreenUpdating = true;
            if (ThisAddIn.Instance.IsFunctionForLogging("ScreenUpdating"))
                Logger.Log("Enabled Screen Updating (LegacyDraftDateStamp.xaml.cs > Read)");

            if (Shapes.Count > 0 && DraftNumber.HasValue)
            {
                HasLegacyDraftDateStamp = true;
                Shape shape = Shapes[0];
                try
                {
                    DateType = DraftDateStampDateType.NoDate;
                    foreach (Field field in shape.TextFrame.TextRange.Fields)
                    {
                        bool checkForTime = false;
                        if (field.Type == WdFieldType.wdFieldDocProperty && field.Code.Text.Contains("LastSavedTime"))
                        {
                            this.DateType = DraftDateStampDateType.LastSavedDate;
                            checkForTime = true;
                        }
                        if (field.Type == WdFieldType.wdFieldDate)
                        {
                            this.DateType = DraftDateStampDateType.CurrentDate;
                            checkForTime = true;
                        }

                        if (checkForTime && field.Code.Text.Contains(":"))
                        {
                            this.IncludeTime = true;
                        }
                    }
                }
                catch { }
            }
        }

        private bool IsDraftDateStamp(Shape shape)
        {
            return shape?.Name != null && shape.Name.StartsWith(DRAFT_DATE_PREFIX);
        }

        public void Delete()
        {
            foreach (Shape shape in Shapes)
            {
                try
                {
                    shape.Delete();
                }
                catch { }
            }
        }

        public void RemoveMetaData()
        {
            DocumentPropertyUtil.RemoveShortProperty(DOC_PROPERTY);
            if (Bookmark != null)
            {
                Bookmark.Delete();
            }
        }
    }
}
