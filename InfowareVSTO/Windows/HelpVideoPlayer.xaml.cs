﻿using InfowareVSTO.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace InfowareVSTO.Windows
{
    /// <summary>
    /// Interaction logic for HelpVideoPlayer.xaml
    /// </summary>
    public partial class HelpVideoPlayer : InfowareWindow
    {
        public HelpVideoPlayer()
        {
            InitializeComponent();
        }

        public void OpenVideo(string folder)
        {
            if (folder != null)
            {
                string url = ConfigSettings.WebApiUrl + "VSTO/Help/videos/" + folder + "/index.html";
                this.Browser.Navigate(url);
                Browser.Navigating += Browser_Navigating;
            }
        }

        private void Browser_Navigating(object sender, NavigatingCancelEventArgs e)
        {
            this.Close();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            this.Browser.Navigating -= Browser_Navigating;
            this.Browser.Dispose();
        }
    }
}
