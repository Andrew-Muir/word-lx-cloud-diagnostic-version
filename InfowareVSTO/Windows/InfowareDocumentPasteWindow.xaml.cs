﻿using System.Windows;
using Microsoft.Office.Interop.Word;

namespace InfowareVSTO.Windows
{
    /// <summary>
    /// Interaction logic for InfowareDocumentPasteWindow.xaml
    /// </summary>
    public partial class InfowareDocumentPasteWindow : InfowareWindow
    {
        public InfowareDocumentPasteWindow()
        {
            InitializeComponent();
        }

        private void OnCancelButtonClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void OnOkButtonClick(object sender, RoutedEventArgs e)
        {
            if (Start.IsChecked == true)
            {
                OnStartButtonClick(null, null);
            }
            if (Cursor.IsChecked == true)
            {
                OnCursorButtonClick(null, null);
            }
            if (End.IsChecked == true)
            {
                OnEndButtonClick(null, null);
            }

            Close();
        }

        private void OnStartButtonClick(object sender, RoutedEventArgs e)
        {
            Globals.ThisAddIn.Application.ActiveDocument.Range(0, 0).Paste();
            Close();
        }

        private void OnEndButtonClick(object sender, RoutedEventArgs e)
        {
            Range documentRange = Globals.ThisAddIn.Application.ActiveDocument.Content;

            //documentRange.InsertParagraphAfter();

            Globals.ThisAddIn.Application.ActiveDocument.Range(documentRange.End - 1, documentRange.End - 1).Paste();

            Close();
        }

        private void OnCursorButtonClick(object sender, RoutedEventArgs e)
        {
            Globals.ThisAddIn.Application.Selection.Range.Paste();
            Close();
        }
    }
}
