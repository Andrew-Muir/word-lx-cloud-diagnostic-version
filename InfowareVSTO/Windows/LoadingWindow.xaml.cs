﻿using InfowareVSTO.Windows.ParagraphNumbering.Util;
using System;
using System.Threading;
using System.Windows.Threading;

namespace InfowareVSTO.Windows
{
    /// <summary>
    /// Interaction logic for LoadingWindow.xaml
    /// </summary>
    public partial class LoadingWindow : InfowareWindow
    {
        public static ParagraphNumberingSchemeManager ParagraphNumberingSchemeManager => ParagraphNumberingSchemeManager.Instance;
        
        public LoadingWindow()
        {
            InitializeComponent();
            DataContext = this;
        }
    }
}
