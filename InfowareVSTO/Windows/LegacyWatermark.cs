﻿using InfowareVSTO.Common;
using InfowareVSTO.Util;
using Microsoft.Office.Interop.Word;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfowareVSTO.Windows
{
    public class LegacyWatermark
    {
        public const string HAS_WATERMARK = "Watermark";
        public const string HAS_WATERMARK_YES = "Yes";
        public const string Has_WATERMARK_TRUE = "True";
        public const string WM_TYPE = "WatermarkType";
        public const string WM_TYPE_CUSTOM = "WatermarkCustom";
        public const string WM_CUSTOM = "Custom";
        public bool HasLegacyWatermark { get; set; }
        public string Type { get; set; }
        public string Custom { get; set; }
        public bool LeftMargin { get; set; }
        public bool RightMargin { get; set; }

        public string Name
        {
            get
            {
                if (Type == WM_CUSTOM)
                {
                    return this.Custom;
                }
                return this.Type;
            }
        }

        public List<Shape> Watermarks { get; set; }

        public void Read()
        {
            this.Watermarks = new List<Shape>();
            Document document = ThisAddIn.Instance.Application.ActiveDocument;
            string watermark = DocumentPropertyUtil.ReadProperty(HAS_WATERMARK);
            if (watermark == HAS_WATERMARK_YES || watermark == Has_WATERMARK_TRUE)
            {
                this.HasLegacyWatermark = true;
                this.Type = DocumentPropertyUtil.ReadProperty(WM_TYPE);
                if (Type == WM_CUSTOM)
                {
                    this.Custom = DocumentPropertyUtil.ReadProperty(WM_TYPE_CUSTOM);
                }
                ThisAddIn.Instance.Application.ScreenUpdating = false;
                if (ThisAddIn.Instance.IsFunctionForLogging("ScreenUpdating"))
                    Logger.Log("Disabled Screen Updating (LegacyWatermark.cs > Read)");
                foreach (Section section in document.Sections)
                {
                    foreach (HeaderFooter footer in section.Footers)
                    {
                        if (footer.Exists && footer.Range.ShapeRange.Count > 0)
                        {
                            foreach (Shape shape in footer.Range.ShapeRange)
                            {
                                if (shape.Name.StartsWith(this.Type))
                                {
                                    Watermarks.Add(shape);
                                    switch (shape.RelativeHorizontalPosition)
                                    {
                                        case WdRelativeHorizontalPosition.wdRelativeHorizontalPositionLeftMarginArea:
                                            this.LeftMargin = true;
                                            break;
                                        case WdRelativeHorizontalPosition.wdRelativeHorizontalPositionRightMarginArea:
                                            this.RightMargin = true;
                                            break;
                                    }
                                }
                            }
                        }
                    }
                }
                ThisAddIn.Instance.Application.ScreenUpdating = true;
                if (ThisAddIn.Instance.IsFunctionForLogging("ScreenUpdating"))
                    Logger.Log("Enabled Screen Updating (LegacyWatermark.cs > Read)");
            }
        }

        public void RemoveMetadata()
        {
            DocumentPropertyUtil.RemoveShortProperty(HAS_WATERMARK);
            DocumentPropertyUtil.RemoveShortProperty(WM_TYPE);
            DocumentPropertyUtil.RemoveShortProperty(WM_TYPE_CUSTOM);
        }

        public void DeleteShapes()
        {
            if (Watermarks != null)
            {
                foreach (Shape shape in Watermarks)
                {
                    shape.Delete();
                }
            }
        }
    }
}
