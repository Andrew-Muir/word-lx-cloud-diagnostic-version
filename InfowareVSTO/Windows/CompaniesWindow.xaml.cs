﻿using InfowareVSTO.Common.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace InfowareVSTO.Windows
{
    /// <summary>
    /// Interaction logic for CompaniesWindow.xaml
    /// </summary>
    public partial class CompaniesWindow : InfowareWindow
    {
        private List<ACompanyDTO> allCompanies;
        private ACompanyWithPathsDTO currentCompanyDetails;
        private AuthorAddressDTO currentOfficeLocation;
        private AuthorAddressDTO savedOfficeLocation;

        public CompaniesWindow()
        {
            InitializeComponent();

            PopulateCompaniesCombobox();

            SetCurrentCompany();
        }

        private void SetCurrentCompany()
        {
            ACompanySimpleDTO currentCompany = AdminPanelWebApi.GetCurrentCompanyDTO();

            if (currentCompany != null)
            {
                this.Companies.SelectedValue = currentCompany.Id;
                savedOfficeLocation = AdminPanelWebApi.GetCurrentOfficeLocation();
                SelectCompany(currentCompany.Id);
            }
        }

        private void PopulateCompaniesCombobox()
        {
            allCompanies = AdminPanelWebApi.GetCompanies();
            this.Companies.ItemsSource = allCompanies;
        }

        private void Companies_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.Companies.SelectedValue is int)
            {
                int id = (int)this.Companies.SelectedValue;
                SelectCompany(id);
            }
        }

        public void SelectCompany(int id)
        {
            this.currentCompanyDetails = AdminPanelWebApi.GetCompanyDetails(id);

            if (currentCompanyDetails != null)
            {
                this.CompanyName.Text = currentCompanyDetails.Name;
                this.Email.Text = currentCompanyDetails.ContactInformations.Where(x => x.Type == "Email")?.OrderBy(x => x.IsDefault)?.FirstOrDefault()?.Details;
                this.Website.Text = currentCompanyDetails.Website;

                this.Offices.ItemsSource = this.currentCompanyDetails.Addresses;

                if (savedOfficeLocation != null)
                {
                    this.Offices.SelectedValue = savedOfficeLocation.Id;
                    savedOfficeLocation = null;
                    Offices_SelectionChanged(null, null);
                }
                else if (this.Offices.SelectedItem == null && this.Offices.Items.Count > 0)
                {
                    this.Offices.SelectedIndex = 0;
                    Offices_SelectionChanged(null, null);
                }

                this.Offices.IsEnabled = currentCompanyDetails?.Addresses?.Count > 0;
            }
        }

        private void Offices_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (Offices.SelectedItem is AuthorAddressDTO)
            {
                this.currentOfficeLocation = Offices.SelectedItem as AuthorAddressDTO;
                this.Address.Text = currentOfficeLocation.Address1;
                this.City.Text = currentOfficeLocation.City;
                this.Province.Text = currentOfficeLocation.Province;
                this.PostalCode.Text = currentOfficeLocation.PostalCode;
                this.Country.Text = currentOfficeLocation.Country;
                this.Phone.Text = currentOfficeLocation.Phone;
                this.Fax.Text = currentOfficeLocation.Fax;
                this.OfficeGrid.Visibility = Visibility.Visible;
            }
            else
            {
                this.OfficeGrid.Visibility = Visibility.Hidden;
            }
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void BtnOk_Click(object sender, RoutedEventArgs e)
        {
            if (currentCompanyDetails != null)
            {
                AdminPanelWebApi.SetCurrentCompany(currentCompanyDetails.Id);
            }

            if (currentOfficeLocation != null)
            {
                AdminPanelWebApi.SetCurrentOfficeLocation(currentOfficeLocation.Id);
            }

            this.Close();
        }
    }
}
