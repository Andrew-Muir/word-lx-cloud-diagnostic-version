﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using InfowareVSTO.Common.Language;
using InfowareVSTO.Common.MultiLanguage;
using InfowareVSTO.MultiLanguage;
using InfowareVSTO.Util;
using Microsoft.Office.Interop.Word;

namespace InfowareVSTO.Windows
{
    /// <summary>
    /// Interaction logic for InfowareInsertCalendarWindow.xaml
    /// </summary>
    public partial class InfowareInsertCalendarWindow : InfowareWindow
    {
        public List<string> MonthNames { get; }

        public List<string> InsertMonthNames { get; }

        private CultureInfo caCultureInfo = CultureInfo.GetCultureInfo("en-CA");

        private int maxCalendarsPerRow = 3;

        private string[][] weekPlaceholders =
        {
            new string[] { "p_cell01", "p_cell02", "p_cell03", "p_cell04", "p_cell05", "p_cell06", "p_cell07" },
            new string[] { "p_cell08", "p_cell09", "p_cell10", "p_cell11", "p_cell12", "p_cell13", "p_cell14" },
            new string[] { "p_cell15", "p_cell16", "p_cell17", "p_cell18", "p_cell19", "p_cell20", "p_cell21"},
            new string[] { "p_cell22", "p_cell23", "p_cell24", "p_cell25", "p_cell26", "p_cell27", "p_cell28" },
            new string[] { "p_cell29", "p_cell30", "p_cell31", "p_cell32", "p_cell33", "p_cell34", "p_cell35" },
            new string[] { "p_cell36", "p_cell37", "p_cell38", "p_cell39", "p_cell40", "p_cell41", "p_cell42" }
        };

        private static string tableTemplate;

        static InfowareInsertCalendarWindow()
        {
            tableTemplate = Properties.Resources.CalendarTemplate;
        }

        public InfowareInsertCalendarWindow()
        {
            InitializeComponent();

            // Take the first 12 values, for some reason MonthNames contains 13 values, one of which is empty
            MonthNames = new List<string>()
            {
                LanguageManager.GetTranslation(LanguageConstants.January, "January"),
                LanguageManager.GetTranslation(LanguageConstants.February, "February"),
                LanguageManager.GetTranslation(LanguageConstants.March, "March"),
                LanguageManager.GetTranslation(LanguageConstants.April, "April"),
                LanguageManager.GetTranslation(LanguageConstants.May, "May"),
                LanguageManager.GetTranslation(LanguageConstants.June, "June"),
                LanguageManager.GetTranslation(LanguageConstants.July, "July"),
                LanguageManager.GetTranslation(LanguageConstants.August, "August"),
                LanguageManager.GetTranslation(LanguageConstants.September, "September"),
                LanguageManager.GetTranslation(LanguageConstants.October, "October"),
                LanguageManager.GetTranslation(LanguageConstants.November, "November"),
                LanguageManager.GetTranslation(LanguageConstants.December, "December")
            };

            var language = MLanguageUtil.ActiveDocumentLanguage;


            CultureInfo cultureInfo = CultureInfo.CreateSpecificCulture(MLanguageUtil.GetResource(MLanguageResourceEnum.DateCultureID));


            InsertMonthNames = new List<string>()
            {
                CapitalizeFirstLetter(cultureInfo.DateTimeFormat.GetMonthName(1)),
                CapitalizeFirstLetter(cultureInfo.DateTimeFormat.GetMonthName(2)),
                CapitalizeFirstLetter(cultureInfo.DateTimeFormat.GetMonthName(3)),
                CapitalizeFirstLetter(cultureInfo.DateTimeFormat.GetMonthName(4)),
                CapitalizeFirstLetter(cultureInfo.DateTimeFormat.GetMonthName(5)),
                CapitalizeFirstLetter(cultureInfo.DateTimeFormat.GetMonthName(6)),
                CapitalizeFirstLetter(cultureInfo.DateTimeFormat.GetMonthName(7)),
                CapitalizeFirstLetter(cultureInfo.DateTimeFormat.GetMonthName(8)),
                CapitalizeFirstLetter(cultureInfo.DateTimeFormat.GetMonthName(9)),
                CapitalizeFirstLetter(cultureInfo.DateTimeFormat.GetMonthName(10)),
                CapitalizeFirstLetter(cultureInfo.DateTimeFormat.GetMonthName(11)),
                CapitalizeFirstLetter(cultureInfo.DateTimeFormat.GetMonthName(12))
            };

            DataContext = this;

            UpdateUiWithCurrentDateInfo();
        }


        private string CapitalizeFirstLetter(string input)
        {
            if (!string.IsNullOrEmpty(input))
            {
                return string.Format("{0}{1}", input[0].ToString().ToUpper(), input.Substring(1));
            }

            return input;
        }

        private void UpdateUiWithCurrentDateInfo()
        {
            DateTime today = DateTime.Today;
            CbMonthNames.SelectedItem = caCultureInfo.DateTimeFormat.GetMonthName(today.Month);
            TbYear.Text = today.Year.ToString();
        }

        private void OnInsertButtonClick(object sender, System.Windows.RoutedEventArgs e)
        {
            Close();
            InsertCalendars();
        }

        private void OnCancelButtonClick(object sender, System.Windows.RoutedEventArgs e)
        {
            Close();
        }

        private void NumericTextBoxValidator(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            if (sender.Equals(this.TbYear))
            {
                if (this.TbYear.Text.Length - this.TbYear.SelectedText.Length + e.Text.Length > 4)
                {
                    e.Handled = true;
                    return;
                }
            }

            var regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void InsertCalendars()
        {
            List<YearMonthPair> yearToMonthPairs = GetYearMonthPairs();


            if (yearToMonthPairs.Count == 1)
            {
                AppendCallendar(yearToMonthPairs[0]);
                return;
            }

            Table containerTable = InsertContainerTable(yearToMonthPairs.Count);

            int rowIndex = 1;
            int columnIndex = 1;

            foreach (YearMonthPair yearMonthPair in yearToMonthPairs)
            {
                Cell destinationCell = containerTable.Cell(rowIndex, columnIndex);

                AppendCallendar(yearMonthPair, destinationCell);

                if (columnIndex == maxCalendarsPerRow)
                {
                    columnIndex = 1;
                    rowIndex++;
                    continue;
                }

                columnIndex++;
            }

            object o = Missing.Value;
            for (int i = 1; i <= containerTable.Rows.Count; i++)
            {
                for (int j = 1; j <= containerTable.Rows[i].Cells.Count; j++)
                {
                    Cell cell = containerTable.Rows[i].Cells[j];

                    cell.Range.Find.Execute("\r", ref o, ref o, ref o, ref o, ref o, ref o, ref o, ref o, "", Microsoft.Office.Interop.Word.WdReplace.wdReplaceAll, ref o, ref o, ref o, ref o);
                }
            }
        }

        private Table InsertContainerTable(int innerTableCount)
        {
            Range selectionRange = Globals.ThisAddIn.Application.Selection.Range;

            Table containerTable;

            if (innerTableCount == 2)
            {
                containerTable = selectionRange.Tables.Add(selectionRange, 1, 2);
            }
            else
            {
                int rowsToGenerate = (int)Math.Ceiling(innerTableCount / Convert.ToDouble(maxCalendarsPerRow));
                containerTable = selectionRange.Tables.Add(selectionRange, rowsToGenerate, maxCalendarsPerRow);
            }

            containerTable.Rows.Alignment = WdRowAlignment.wdAlignRowCenter;
            containerTable.Borders.Enable = 0;

            return containerTable;
        }

        private void AppendCallendar(YearMonthPair yearMonthPair, Cell destinationCell = null)
        {
            var tableStringBuilder = new StringBuilder(tableTemplate);

            string header = $"{yearMonthPair.Month} {yearMonthPair.Year}";

            tableStringBuilder = tableStringBuilder.Replace("p_month_and_year", header);

            var activeLanguage = MLanguageUtil.ActiveDocumentLanguage;


            CultureInfo cultureInfo = CultureInfo.CreateSpecificCulture(MLanguageUtil.GetResource(MLanguageResourceEnum.DateCultureID));

            tableStringBuilder = tableStringBuilder.Replace("_Su", cultureInfo.DateTimeFormat.GetDayName(DayOfWeek.Sunday)[0].ToString().ToUpper());
            tableStringBuilder = tableStringBuilder.Replace("_M", cultureInfo.DateTimeFormat.GetDayName(DayOfWeek.Monday)[0].ToString().ToUpper());
            tableStringBuilder = tableStringBuilder.Replace("_Tu", cultureInfo.DateTimeFormat.GetDayName(DayOfWeek.Tuesday)[0].ToString().ToUpper());
            tableStringBuilder = tableStringBuilder.Replace("_W", cultureInfo.DateTimeFormat.GetDayName(DayOfWeek.Wednesday)[0].ToString().ToUpper());
            tableStringBuilder = tableStringBuilder.Replace("_Th", cultureInfo.DateTimeFormat.GetDayName(DayOfWeek.Thursday)[0].ToString().ToUpper());
            tableStringBuilder = tableStringBuilder.Replace("_F", cultureInfo.DateTimeFormat.GetDayName(DayOfWeek.Friday)[0].ToString().ToUpper());
            tableStringBuilder = tableStringBuilder.Replace("_Sa", cultureInfo.DateTimeFormat.GetDayName(DayOfWeek.Saturday)[0].ToString().ToUpper());

            int month = InsertMonthNames.IndexOf(yearMonthPair.Month) + 1;
            int daysInMonth = DateTime.DaysInMonth(yearMonthPair.Year, month);

            int weekIndex = 0;
            int tableRowsToDelete = 0;

            for (int day = 1; day <= daysInMonth; day++)
            {
                var date = new DateTime(yearMonthPair.Year, month, day);
                DayOfWeek dayOfWeek = date.DayOfWeek;

                string[] placeholders = weekPlaceholders[weekIndex];

                int dayIndex = (int)dayOfWeek;
                string dayPlaceholder = placeholders[dayIndex];

                if (day == 1)
                {
                    // Clear all cells up until the one corresponding to the first day
                    for (int i = 0; i < dayIndex; i++)
                    {
                        tableStringBuilder = tableStringBuilder.Replace(placeholders[i], "");
                    }
                }

                tableStringBuilder = tableStringBuilder.Replace(dayPlaceholder, day.ToString());

                // Last iteration
                if (day == daysInMonth)
                {
                    // Clear all cells after the one corresponding to the last day
                    for (int i = dayIndex + 1; i < placeholders.Length; i++)
                    {
                        tableStringBuilder = tableStringBuilder.Replace(placeholders[i], "");
                    }

                    tableRowsToDelete = weekPlaceholders.Length - 1 - weekIndex;
                }

                if (dayOfWeek == DayOfWeek.Saturday)
                {
                    weekIndex++;
                }
            }

            string tableContent = tableStringBuilder.ToString();
            Tables containerTables;

            if (destinationCell == null)
            {
                Globals.ThisAddIn.Application.Selection.Range.InsertXML(tableContent);
                containerTables = Globals.ThisAddIn.Application.Selection.Tables;
            }
            else
            {
                destinationCell.Range.InsertXML(tableContent);
                containerTables = destinationCell.Tables;
            }

            if (tableRowsToDelete == 0)
            {
                return;
            }

            Table newTable = containerTables[1];

            for (int i = 0; i < tableRowsToDelete; i++)
            {
                int rowCount = newTable.Rows.Count;
                newTable.Rows[rowCount].Delete();
            }

            //for (int i = 1; i < newTable.Rows.Count; i++)
            //{
            //    newTable.Rows[i].Cells[1].Borders[WdBorderType.wdBorderLeft].;
            //}
        }

        private List<YearMonthPair> GetYearMonthPairs()
        {
            string selectedMonthName = (string)CbMonthNames.SelectedItem;
            int monthCount = int.Parse(TbMonthCount.Text);
            int insertedYear = int.Parse(TbYear.Text);

            var pairs = new List<YearMonthPair>();

            int year = insertedYear;
            int monthNameIndex = MonthNames.IndexOf(selectedMonthName);

            for (int i = 0; i < monthCount; i++)
            {
                pairs.Add(new YearMonthPair(year, InsertMonthNames[monthNameIndex]));

                // December
                if (monthNameIndex == 11)
                {
                    monthNameIndex = -1;
                    year++;
                }

                monthNameIndex++;
            }

            return pairs;
        }

        class YearMonthPair
        {
            public int Year { get; }

            public string Month { get; }

            public YearMonthPair(int year, string month)
            {
                Year = year;
                Month = month;
            }

            public override string ToString()
            {
                return $"{Month} {Year}";
            }
        }
    }
}
