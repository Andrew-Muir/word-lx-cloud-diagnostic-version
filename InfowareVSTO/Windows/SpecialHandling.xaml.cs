﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace InfowareVSTO.Windows
{
    /// <summary>
    /// Interaction logic for SpecialHandling.xaml
    /// </summary>
    public partial class SpecialHandling : InfowareWindow
    {
        public List<string> options = new List<string>()
        {
            "plain",
            "letterhead",
            "envelope",
            "label",
            "filecopy",
            "a4",
            "legal"
        };

        public SpecialHandling()
        {
            InitializeComponent();

            //populate combobox
            this.OptionsCombo.ItemsSource = options;

            //read variable 
            string paperType = Utils.GetDocumentVariable(ThisAddIn.Instance.Application.ActiveDocument, "PaperType");
            if (paperType!= null)
            {
                this.OptionsCombo.Text = paperType;
            }
        }

        private void Ok_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(this.OptionsCombo.Text))
            {
                Utils.SetDocumentVariable(ThisAddIn.Instance.Application.ActiveDocument, "PaperType", this.OptionsCombo.Text);
            }

            this.Close();
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
