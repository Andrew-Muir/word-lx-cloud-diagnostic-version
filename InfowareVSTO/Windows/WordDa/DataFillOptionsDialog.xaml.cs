﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using InfowareVSTO;

namespace InfowareVSTO.Windows.WordDa
{
    /// <summary>
    /// Interaction logic for DataFillOptionsDialog.xaml
    /// </summary>
    public partial class DataFillOptionsDialog : InfowareWindow
    {
        private const string DATA_FILL_OPTIONS_KEY = "DataFillOptions";
        private const int PROMPT_BIT = 0b1;
        private const int CHEVRON_BIT = 0b10;
        private const int SQUARE_BRACKETS_BIT = 0b100;
        public bool UsePrompt { get; set; }
        public bool UseChevrons { get; set; }
        public bool UseSquareBrackets { get; set; }

        public DataFillOptionsDialog()
        {
            InitializeComponent();
            LoadSettings();
            ApplySettings();
        }

        private void ApplySettings()
        {
            this.Prompt.IsChecked = UsePrompt;
            this.Chevron.IsChecked = UseChevrons;
            this.SquareBrackets.IsChecked = UseSquareBrackets;
        }

        private void LoadSettings()
        {
            int? saved =  RegistryManager.GetIntRegistryValue(DATA_FILL_OPTIONS_KEY);
            if (saved.HasValue)
            {
                UsePrompt = (saved.Value & PROMPT_BIT) > 0;
                UseChevrons = (saved.Value & CHEVRON_BIT) > 0;
                UseSquareBrackets = (saved.Value & SQUARE_BRACKETS_BIT) > 0;
            }
            else
            {
                UsePrompt = true;
                UseChevrons = true;
                UseSquareBrackets = true;
            }
        }

        private void OK_Click(object sender, RoutedEventArgs e)
        {
            int result = 0;
            if (this.Prompt.IsChecked == true)
            {
                result += PROMPT_BIT;
            }
            if (this.Chevron.IsChecked == true)
            {
                result += CHEVRON_BIT;
            }
            if(this.SquareBrackets.IsChecked == true)
            {
                result += SQUARE_BRACKETS_BIT;
            }

            RegistryManager.SaveToRegistry(DATA_FILL_OPTIONS_KEY, result);
            this.Close();
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
