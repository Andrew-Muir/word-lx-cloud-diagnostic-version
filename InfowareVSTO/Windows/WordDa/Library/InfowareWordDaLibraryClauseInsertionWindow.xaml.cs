﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Xml.Linq;
using InfowareVSTO.Common;
using InfowareVSTO.Common.General;
using InfowareVSTO.TaskPaneControls.WordDa;
using InfowareVSTO.Util;
using Word = Microsoft.Office.Interop.Word;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using InfowareVSTO.Common.Language;
using DocumentFormat.OpenXml.Bibliography;
using Microsoft.Office.Interop.Outlook;
using System.Windows.Data;
using System.Collections;
using InfowareVSTO.Common.Models;
using DocumentFormat.OpenXml.EMMA;

namespace InfowareVSTO.Windows.WordDa.Library
{
    /// <summary>
    /// Interaction logic for InfowareWordDaLibraryClauseInsertionUserControl.xaml
    /// </summary>
    public partial class InfowareWordDaLibraryClauseInsertionWindow : InfowareWindow
    {
        private static bool isUnformattedChecked = false;
        private readonly string clauseLibraryXml;

        public const string STRUCTURED_DOCUMENT_TAG_ELEMENT_NAME = "sdt";

        public const string STRUCTURED_DOCUMENT_TAG_CONTENT_ELEMENT_NAME = "sdtContent";

        public const string STRUCTURED_DOCUMENT_TAG_PROPERTIES_ELEMENT_NAME = "sdtPr";

        public const string VAL_ELEMENT_ATTRIBUTE_NAME = "val";

        public const string ALIAS_ELEMENT_NAME = "alias";

        public const string NAME_ELEMENT_NAME = "name";

        public const string TAG_ELEMENT_NAME = "tag";

        public const string PROPERTY_ELEMENT_NAME = "property";

        public const string STRING_PROPERTY_VALUE_ELEMENT_NAME = "lpwstr";

        public const string QUESTION_FILE_PROPERTY_NAME = "LX-WORDDA-QUESTIONFILE";

        public const string CITATION_CONTROL_TAG = "LX-WORDDA-CITATION";

        public const string CLAUSE_GROUP_TITLE_PREFIX = "Group";

        public const string CLAUSE_TITLE_PREFIX = "Citation";

        public const string OPENING_BODY_TAG = "<w:body>";

        public const string CLOSING_BODY_TAG = "</w:body>";

        public static readonly XNamespace wordNamespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main";

        private static readonly XNamespace propertyListNamespace = "http://schemas.openxmlformats.org/officeDocument/2006/custom-properties";

        private static readonly XNamespace propertyNamespace = "http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes";

        public List<ClauseGroup> ClauseGroups { get; set; }

        public ObservableCollection<Clause> SelectedClauses { get; set; }
        public ObservableCollection<ClauseGroup> SelectedClauseGroups { get; set; }
        private ClauseLibraryHelper clauseLibraryHelper;
        private bool fixedClauses;
        public bool FixedClauseGroups { get; set; }
        public bool FixedClauseGroupPopulation { get; set; }
        private string libraryName;
        private List<Word.ContentControl> InsertedFixedClauses = new List<Word.ContentControl>();

        public InfowareWordDaLibraryClauseInsertionWindow(string clauseLibraryXml, bool fixedClauses = false, string libraryName = null, bool fixedClauseGroups = false, List<string> groupNames = null)
        {
            InitializeComponent();

            this.clauseLibraryXml = clauseLibraryXml;

            InitLists();
            if (isUnformattedChecked)
            {
                Unformatted.IsChecked = true;
            }

            this.fixedClauses = fixedClauses;
            this.FixedClauseGroups = fixedClauseGroups;
            this.FixedClauseGroupPopulation = groupNames?.Count > 0;

            if (fixedClauses || FixedClauseGroupPopulation)
            {
                this.BtnMoveUp.IsEnabled = false;
                this.BtnMoveDown.IsEnabled = false;
                DialogTitle.Text = LanguageManager.GetTranslation(LanguageConstants.SelectClause, "Select Clause");
                this.libraryName = libraryName;
            }
            else if (fixedClauseGroups)
            {
                this.BtnMoveUp.IsEnabled = false;
                this.BtnMoveDown.IsEnabled = false;
                DialogTitle.Text = LanguageManager.GetTranslation(LanguageConstants.SelectClauseGroup, "Select Clause Group");
                this.libraryName = libraryName;
                ListSelectedClauses.ItemsSource = SelectedClauseGroups;
                ListSelectedClauses.DisplayMemberPath = "Id";
            }
            else
            {
                InsertedFixedClauses = DocumentUtils.GetAllContentControls(ThisAddIn.Instance.Application.ActiveDocument, CustomConstants.FIXED_CLAUSE_TAG, CustomConstants.FIXED_CLAUSE_GROUP_TAG).ToList();
                if (InsertedFixedClauses.Count > 0)
                {
                    this.InsertAtCursor.Visibility = Visibility.Visible;
                    this.InsertAtCursor.IsChecked = false;
                }
            }

            if (FixedClauseGroupPopulation)
            {
                RemoveOtherClauseGroups(groupNames);
            }

            DataContext = this;
        }

        private void RemoveOtherClauseGroups(List<string> groupNames)
        {
            List<ClauseGroup> groups = ClauseGroups;
            ClauseGroup parent = null;
            foreach(string groupName in groupNames)
            {
                parent?.Clauses?.Clear();
                var group = groups.Where(x => x.Id == groupName).FirstOrDefault();

                if (group != null)
                {
                    var toRemove = groups.Where(x => x.Id != groupName).ToList();
                    foreach (var groupToRemove in toRemove)
                    {
                        groups.Remove(groupToRemove);
                    }

                    parent = group;
                    groups = group.NestedClauseGroups;
                }
                else
                {
                    break;
                }
            }
        }

        private void InitLists()
        {
            this.clauseLibraryHelper = new ClauseLibraryHelper(this.clauseLibraryXml);
            List<ClauseGroup> clauseGroups = clauseLibraryHelper.GetAllClauseGroups();

            if (clauseGroups.Count == 0)
            {
                new InfowareInformationWindow($"The selected template has no clause groups").ShowDialog();
                return;
            }

            ClauseGroups = clauseGroups;

            SelectedClauses = new ObservableCollection<Clause>();
            SelectedClauseGroups = new ObservableCollection<ClauseGroup>();
        }

        private void OnInsertButtonClick(object sender, RoutedEventArgs e)
        {
            if (fixedClauses)
            {
                InsertFixedClause();
                return;
            }

            if (FixedClauseGroups)
            {
                InsertFixedClauseGroup();
                return;
            }

            if (FixedClauseGroupPopulation)
            {
                if (SelectedClauses.Count > 0)
                {
                    this.Close();
                }

                return;
            }

            if (InsertAtCursor.IsChecked != false)
            {
                var newBodyBuilder = new StringBuilder();

                foreach (Clause clause in SelectedClauses)
                {
                    newBodyBuilder.Append(clause.XmlContent);
                }

                string newBody = newBodyBuilder.ToString();

                string xmlDocument = clauseLibraryHelper.GetValidXmlDocument(newBody);

                Word.Range range = Globals.ThisAddIn.Application.Selection.Range;
                range.Collapse(Word.WdCollapseDirection.wdCollapseEnd);
                range.Select();
                Globals.ThisAddIn.Application.Selection.CopyFormat();

                isUnformattedChecked = this.Unformatted.IsChecked == true;
                if (this.Unformatted.IsChecked == true)
                {
                    Word.Range endRange = range.Duplicate;
                    endRange.MoveEnd(Word.WdUnits.wdCharacter, 1);
                    bool usePar = IsParagraphBreak(endRange.Text);
                    Word.Paragraph nextPar = range.Paragraphs.Last.Next();
                    try
                    {
                        range.InsertXML(xmlDocument);
                    }
                    catch (System.Exception ex)
                    {
                    }

                    if (!usePar)
                    {
                        endRange.MoveEnd(Word.WdUnits.wdCharacter, -1);
                        range = DocumentUtil.GetActiveDocument().Range(range.End, endRange.End);
                    }
                    else
                    {
                        if (nextPar != null)
                        {
                            range = DocumentUtil.GetActiveDocument().Range(range.End, nextPar.Previous().Range.End);
                        }
                        else
                        {
                            Word.Document document = DocumentUtil.GetActiveDocument();
                            range = document.Range(range.End, document.Content.End);
                        }
                    }
                    range.Select();
                    Globals.ThisAddIn.Application.Selection.PasteFormat();
                    range.Collapse(Word.WdCollapseDirection.wdCollapseEnd);
                    range.Select();

                }
                else
                {
                    try
                    {
                        range.InsertXML(xmlDocument);
                    }
                    catch (System.Exception ex)
                    {
                    }
                }
            }
            else
            {
                var fixedClauses = SelectedClauses.Where(x => x.IsFixedClause).ToList();
                int firstFixedClauseIndex = SelectedClauses.IndexOf(fixedClauses.First());
                if (firstFixedClauseIndex > 0)
                {
                    var newBodyBuilder = new StringBuilder();
                    for (int i = 0; i < firstFixedClauseIndex; i++)
                    {
                        newBodyBuilder.Append(SelectedClauses[i].XmlContent);
                    }

                    string newBody = newBodyBuilder.ToString();

                    string xmlDocument = clauseLibraryHelper.GetValidXmlDocument(newBody);
                    var cc = fixedClauses.First().ContentControl;
                    Word.Range tempRange = cc.Range;
                    tempRange.MoveStart(Word.WdUnits.wdCharacterFormatting, -1);
                    tempRange.Collapse(Word.WdCollapseDirection.wdCollapseStart);


                    tempRange.Select();
                    Globals.ThisAddIn.Application.Selection.CopyFormat();
                    try
                    {
                        tempRange.InsertXML(xmlDocument);
                    }
                    catch (System.Exception ex)
                    {
                    }

                    if (Unformatted.IsChecked == true)
                    {
                        Word.Range endRange = cc.Range;
                        endRange.MoveStart(Word.WdUnits.wdCharacterFormatting, -1);
                        endRange.Collapse(Word.WdCollapseDirection.wdCollapseStart);

                        tempRange = DocumentUtil.GetActiveDocument().Range(tempRange.Start, endRange.Start);
                        tempRange.Select();
                        Globals.ThisAddIn.Application.Selection.PasteFormat();
                    }

                }
                int j = firstFixedClauseIndex + 1;
                List<Clause> clauses = new List<Clause>();
                Clause lastFixedClause = SelectedClauses[firstFixedClauseIndex];
                while (j <= SelectedClauses.Count)
                {
                    if (j >= SelectedClauses.Count || SelectedClauses[j].IsFixedClause)
                    {
                        if (clauses.Count > 0)
                        {
                            var newBodyBuilder = new StringBuilder();

                            foreach (Clause clause in clauses)
                            {
                                newBodyBuilder.Append(clause.XmlContent);
                            }

                            string newBody = newBodyBuilder.ToString();

                            string xmlDocument = clauseLibraryHelper.GetValidXmlDocument(newBody);
                            var cc = lastFixedClause.ContentControl;

                            var tempRange = cc.Range;
                            tempRange.MoveEnd(Word.WdUnits.wdCharacterFormatting, 1);
                            tempRange.Collapse(Word.WdCollapseDirection.wdCollapseEnd);

                            if (tempRange.Paragraphs.Last.Next() == null)
                            {
                                tempRange.InsertParagraphAfter();
                            }
                            tempRange = tempRange.Paragraphs.Last.Next().Range;
                            tempRange.Collapse();
                            tempRange.Select();
                            Globals.ThisAddIn.Application.Selection.CopyFormat();
                            Word.Range endRange = tempRange.Duplicate;
                            endRange.MoveEnd(Word.WdUnits.wdCharacter, 1);
                            bool usePar = IsParagraphBreak(endRange.Text);
                            Word.Paragraph nextPar = tempRange.Paragraphs.Last.Next();
                            try
                            {
                                tempRange.InsertXML(xmlDocument);
                            }
                            catch (System.Exception ex)
                            {
                            }

                            if (Unformatted.IsChecked == true)
                            {
                                if (!usePar)
                                {
                                    endRange.MoveEnd(Word.WdUnits.wdCharacter, -1);
                                    tempRange = DocumentUtil.GetActiveDocument().Range(tempRange.End, endRange.End);
                                }
                                else
                                {
                                    if (nextPar != null)
                                    {
                                        tempRange = DocumentUtil.GetActiveDocument().Range(tempRange.End, nextPar.Previous().Range.End);
                                    }
                                    else
                                    {
                                        Word.Document document = DocumentUtil.GetActiveDocument();
                                        tempRange = document.Range(tempRange.End, document.Content.End);
                                    }
                                }
                                tempRange.Select();
                                Globals.ThisAddIn.Application.Selection.PasteFormat();
                                tempRange.Collapse(Word.WdCollapseDirection.wdCollapseEnd);
                                tempRange.Select();
                            }

                            clauses.Clear();
                        }

                        if (j < SelectedClauses.Count)
                        {
                            lastFixedClause = SelectedClauses[j];
                        }
                    }
                    else
                    {
                        clauses.Add(SelectedClauses[j]);
                    }

                    j++;
                }
            }

            UpdateActiveDocument();

            Close();
        }

        private void InsertFixedClauseGroup()
        {
            if (SelectedClauseGroups.Count > 0 && libraryName != null)
            {
                Word.Range insertionRange = ThisAddIn.Instance.Application.Selection.Range;
                insertionRange.Collapse(Word.WdCollapseDirection.wdCollapseEnd);

                insertionRange = insertionRange.Paragraphs.Last.Range;

                var vstoDocument = Globals.Factory.GetVstoObject(ThisAddIn.Instance.Application.ActiveDocument);
                try
                {
                    Microsoft.Office.Tools.Word.RichTextContentControl control = vstoDocument.Controls.AddRichTextContentControl(insertionRange, GetFixedClauseGroupTitle(SelectedClauseGroups.First()) + Guid.NewGuid());
                    control.Tag = CustomConstants.FIXED_CLAUSE_GROUP_TAG;
                    control.PlaceholderText = SelectedClauseGroups.First().Id;
                    control.Title = GetFixedClauseGroupTitle(SelectedClauseGroups.First());

                    Word.Range tempRange = control.Range;
                    tempRange.Collapse(Word.WdCollapseDirection.wdCollapseEnd);
                    tempRange.MoveEnd(Word.WdUnits.wdCharacterFormatting, 1);
                    tempRange.MoveStart(Word.WdUnits.wdCharacterFormatting, 1);
                    tempRange.Select();

                }
                catch (System.NotImplementedException)
                {
                    DocumentUtil.ShowCompatibilityModeError(new System.Threading.Tasks.Task(() => { InsertFixedClauseGroup(); }));
                }

                Close();
            }
        }

        private void InsertFixedClause()
        {
            if (SelectedClauses.Count > 0 && libraryName != null)
            {
                Word.Range insertionRange = ThisAddIn.Instance.Application.Selection.Range;
                insertionRange.Collapse(Word.WdCollapseDirection.wdCollapseEnd);

                insertionRange = insertionRange.Paragraphs.Last.Range;

                var vstoDocument = Globals.Factory.GetVstoObject(ThisAddIn.Instance.Application.ActiveDocument);
                try
                {
                    Microsoft.Office.Tools.Word.RichTextContentControl control = vstoDocument.Controls.AddRichTextContentControl(insertionRange, GetFixedClauseTitle(SelectedClauses.First()) + Guid.NewGuid());
                    control.Tag = CustomConstants.FIXED_CLAUSE_TAG;
                    control.PlaceholderText = SelectedClauses.First().Name;
                    control.Title = GetFixedClauseTitle(SelectedClauses.First());

                    Word.Range tempRange = control.Range;
                    tempRange.Collapse(Word.WdCollapseDirection.wdCollapseEnd);
                    tempRange.MoveEnd(Word.WdUnits.wdCharacterFormatting, 1);
                    tempRange.MoveStart(Word.WdUnits.wdCharacterFormatting, 1);
                    tempRange.Select();

                }
                catch (System.NotImplementedException)
                {
                    DocumentUtil.ShowCompatibilityModeError(new System.Threading.Tasks.Task(() => { InsertFixedClause(); }));
                }

                Close();
            }
        }


        private string GetFixedClauseGroupTitle(ClauseGroup clauseGroup)
        {
            Clause clause = clauseGroup.Clauses.FirstOrDefault();
            List<ClauseGroup> group = SearchClause(ClauseGroups, clause);

            if (clause == null || group.Count == 0)
            {
                return string.Empty;
            }

            string title = CustomConstants.FIXED_CLAUSE_LIBRARY + libraryName;

            for (int i = 0; i < group.Count; i++)
            {
                title += CustomConstants.FIXED_CLAUSE_GROUP + group[i].Id;
            }

            return title;
        }

        private string GetFixedClauseTitle(Clause clause)
        {
            List<ClauseGroup> group = SearchClause(ClauseGroups, clause);

            if (clause == null || group.Count == 0)
            {
                return string.Empty;
            }

            string title = CustomConstants.FIXED_CLAUSE_LIBRARY + libraryName;

            for (int i = 0; i < group.Count; i++)
            {
                title += CustomConstants.FIXED_CLAUSE_GROUP + group[i].Id;
            }

            title += CustomConstants.FIXED_CLAUSE_CLAUSE + clause.Name + CustomConstants.FIXED_CLAUSE_UNFORMATTED + (this.Unformatted.IsChecked == true ? 1 : 0);

            return title;
        }

        private List<ClauseGroup> SearchClause(List<ClauseGroup> clauseGroups, Clause clause)
        {
            var group = clauseGroups.Where(x => x.Clauses.Contains(clause)).FirstOrDefault();

            if (group != null)
            {
                return new List<ClauseGroup>() { group };
            }

            foreach (ClauseGroup cGroup in clauseGroups)
            {
                if (cGroup.NestedClauseGroups != null && cGroup.NestedClauseGroups.Count > 0)
                {
                    var searchResult = SearchClause(cGroup.NestedClauseGroups, clause);
                    if (searchResult != null)
                    {
                        List<ClauseGroup> result = new List<ClauseGroup>() { cGroup };
                        result.AddRange(searchResult);
                        return result;
                    }
                }
            }

            return null;
        }

        private bool IsParagraphBreak(string text)
        {
            return text == "\n" || text == "\r" || text == "\r\n";
        }

        public void UpdateActiveDocument()
        {
            MergeTemplateQuestionsWithDocumentQuestions();

            ClauseBuilderUserControl.LoadDataFields();
        }

        private void MergeTemplateQuestionsWithDocumentQuestions()
        {
            // Get questions from the selected template
            List<ClauseBuilderQuestion> existingQuestions = GetExistingQuestions();


            // Get questions from the current document's properties

            string serializedQuestions = DocumentPropertyUtil.ReadLongProperty(QUESTION_FILE_PROPERTY_NAME);

            if ((existingQuestions?.Count ?? 0) == 0)
            {
                // There are no questions in template.
                return;
            }

            if (serializedQuestions != null)
            {
                var rawQuestions = JObject.Parse(serializedQuestions);

                foreach (KeyValuePair<string, JToken> rawQuestion in rawQuestions)
                {
                    ClauseBuilderQuestion documentQuestion = rawQuestion.Value.ToObject<ClauseBuilderQuestion>();

                    // Overwrite document questions that exist under the same name in the template
                    if (existingQuestions.Any(existingQuestion => existingQuestion.Variable == documentQuestion.Variable))
                    {
                        continue;
                    }

                    existingQuestions.Add(documentQuestion);
                }
            }


            // Persist questions as properties

            var root = new JObject();

            foreach (ClauseBuilderQuestion question in existingQuestions)
            {
                root.Add(question.Variable, JObject.FromObject(question));
            }

            string questionsToSave = JsonConvert.SerializeObject(root);

            DocumentPropertyUtil.SaveLongProperty(QUESTION_FILE_PROPERTY_NAME, questionsToSave);

            // Load them in Clause Builder
            ClauseBuilderUserControl.LoadSerializedQuestions();
        }

        private void OnCancelButtonClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void OnBtnMoveUpClick(object sender, RoutedEventArgs e)
        {
            MoveSelectedClause(true);
        }

        private void OnBtnMoveDownClick(object sender, RoutedEventArgs e)
        {
            MoveSelectedClause(false);
        }

        private void OnBtnRemoveSelectedClick(object sender, RoutedEventArgs e)
        {
            if (FixedClauseGroups)
            {
                ClauseGroup selectedClauseGroup = SelectedClauseGroups.FirstOrDefault();

                if (selectedClauseGroup == null)
                {
                    return;
                }

                SelectedClauseGroups.Remove(selectedClauseGroup);

                selectedClauseGroup.IsSelected = false;
            }
            else
            {
                Clause selectedClause = GetSelectedClause();

                if (selectedClause == null)
                {
                    return;
                }

                SelectedClauses.Remove(selectedClause);

                selectedClause.IsSelected = false;
            }
        }

        private void MoveSelectedClause(bool moveUp)
        {
            Clause selectedClause = GetSelectedClause();
            int index = SelectedClauses.IndexOf(selectedClause);

            if (index == -1 || (moveUp && index == 0) || (!moveUp && index == SelectedClauses.Count - 1))
            {
                return;
            }

            SelectedClauses.RemoveAt(index);

            int newIndex;

            if (moveUp)
            {
                newIndex = index - 1;
            }
            else
            {
                newIndex = index + 1;
            }

            SelectedClauses.Insert(newIndex, selectedClause);
            ListSelectedClauses.SelectedIndex = newIndex;
        }

        private void OnClauseCheckboxChecked(object sender, RoutedEventArgs e)
        {
            var affectedClause = (Clause)((CheckBox)sender).DataContext;

            if (fixedClauses || FixedClauseGroupPopulation)
            {
                foreach (var clause in SelectedClauses.ToList())
                {
                    clause.IsSelected = false;
                }

                SelectedClauses.Clear();
            }

            SelectedClauses.Add(affectedClause);
            ListSelectedClauses.SelectedItem = affectedClause;

            affectedClause.IsSelected = true;
        }

        private void OnClauseCheckboxUnchecked(object sender, RoutedEventArgs e)
        {
            var affectedClause = (Clause)((CheckBox)sender).DataContext;

            SelectedClauses.Remove(affectedClause);

            affectedClause.IsSelected = false;
        }

        private void OnListClausesSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var selectedClause = (Clause)((ListBox)sender).SelectedItem;
            UpdateClauseContentWebView(selectedClause);
        }

        private Clause GetSelectedClause()
        {
            return (Clause)ListSelectedClauses.SelectedItem;
        }

        private void OnListSelectedClausesSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!FixedClauseGroups)
            {
                UpdateClauseContentWebView(GetSelectedClause());
            }
        }

        private void UpdateClauseContentWebView(Clause selectedClause)
        {
            string htmlContent;

            if (selectedClause == null)
            {
                htmlContent = "<html></html>";
            }
            else
            {
                if (selectedClause.HtmlContent == null)
                {
                    string xmlDocument = clauseLibraryHelper.GetValidXmlDocument(selectedClause.XmlContent);

                    selectedClause.HtmlContent = DocumentUtils.ConvertWordXmlDocumentToHtml(xmlDocument);
                }

                htmlContent = selectedClause.HtmlContent;
            }

            WbClauseContent.NavigateToString(htmlContent);
        }

        private List<ClauseBuilderQuestion> GetExistingQuestions()
        {
            var existingQuestions = new List<ClauseBuilderQuestion>();

            XElement questionSplitSizeElement = GetPropertyWithName(DocumentPropertyUtil.GetLongPropertySizePropertyName(QUESTION_FILE_PROPERTY_NAME));

            if (questionSplitSizeElement == null)
            {
                return existingQuestions;
            }

            var serializedQuestionStringBuilder = new StringBuilder();

            XElement questionSplitSizeValueElement = questionSplitSizeElement.Element(propertyNamespace + STRING_PROPERTY_VALUE_ELEMENT_NAME);

            int splitSize = int.Parse(questionSplitSizeValueElement.Value);

            for (int i = 0; i < splitSize; i++)
            {
                XElement questionPartElement = GetPropertyWithName(QUESTION_FILE_PROPERTY_NAME + i);
                string questionPartValue = questionPartElement.Element(propertyNamespace + STRING_PROPERTY_VALUE_ELEMENT_NAME).Value;

                serializedQuestionStringBuilder.Append(questionPartValue);
            }

            string serializedQuestions = serializedQuestionStringBuilder.ToString();

            var rawQuestions = JObject.Parse(serializedQuestions);

            foreach (KeyValuePair<string, JToken> question in rawQuestions)
            {
                existingQuestions.Add(question.Value.ToObject<ClauseBuilderQuestion>());
            }

            return existingQuestions;
        }

        private XElement GetPropertyWithName(string name)
        {
            var root = XDocument.Parse(clauseLibraryXml);

            foreach (XElement propertyElement in root.Descendants(propertyListNamespace + PROPERTY_ELEMENT_NAME))
            {
                string attribute = ClauseLibraryHelper.GetAttributeValue(propertyElement, NAME_ELEMENT_NAME);

                if (attribute != null && attribute == name)
                {
                    return propertyElement;
                }
            }

            return null;
        }

        public class ClausePreview
        {
            public Clause Clause { get; set; }
            public StackPanel StackPanel { get; set; }
            public string Base64 { get; set; }
        }

        private void InsertAtCursor_Checked(object sender, RoutedEventArgs e)
        {
            if (SelectedClauses != null)
            {
                var toRemove = SelectedClauses.Where(x => x.IsFixedClause).ToList();
                for (int i = toRemove.Count - 1; i >= 0; i--)
                {
                    Clause clause = toRemove[i];
                    SelectedClauses.Remove(clause);
                }
            }
        }

        private void InsertAtCursor_Unchecked(object sender, RoutedEventArgs e)
        {
            if (SelectedClauses != null && InsertedFixedClauses != null)
            {
                foreach (var cc in InsertedFixedClauses)
                {
                    if (cc.Tag == CustomConstants.FIXED_CLAUSE_TAG)
                    {
                        var reference = FixedClauseHelper.GetFixedClauseReferences(cc);

                        if (reference != null)
                        {
                            Clause clause = new Clause(reference.ClauseName, string.Empty);
                            clause.IsFixedClause = true;
                            clause.IsSelected = true;
                            clause.ContentControl = cc;
                            SelectedClauses.Add(clause);
                        }
                    }
                    else if (cc.Tag == CustomConstants.FIXED_CLAUSE_GROUP_TAG)
                    {
                        var reference = FixedClauseGroupHelper.GetFixedClauseGroupReferences(cc);

                        if (reference != null)
                        {
                            Clause clause = new Clause(reference.GroupNames.LastOrDefault(), string.Empty);
                            clause.IsFixedClause = true;
                            clause.IsSelected = true;
                            clause.ContentControl = cc;
                            SelectedClauses.Add(clause);
                        }
                    }
                }
            }
        }

        private void CbClauseGroup_Checked(object sender, RoutedEventArgs e)
        {
            var affectedClauseGroup = (ClauseGroup)((CheckBox)sender).DataContext;

            if (FixedClauseGroups)
            {
                foreach (var clauseGroup in SelectedClauseGroups.ToList())
                {
                    clauseGroup.IsSelected = false;
                }

                SelectedClauses.Clear();
            }

            SelectedClauseGroups.Add(affectedClauseGroup);
            ListSelectedClauses.SelectedItem = affectedClauseGroup;

            affectedClauseGroup.IsSelected = true;
        }

        private void CbClauseGroup_Unchecked(object sender, RoutedEventArgs e)
        {
            var affectedClauseGroup = (ClauseGroup)((CheckBox)sender).DataContext;

            SelectedClauseGroups.Remove(affectedClauseGroup);

            affectedClauseGroup.IsSelected = false;
        }

        private void ListGroups_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (FixedClauseGroups && ListGroups.SelectedItem is Clause)
            {
                UpdateClauseContentWebView(ListGroups.SelectedItem as Clause);
            }
        }
    }
    public class ClauseGroup : INotifyPropertyChanged
    {
        public string Id { get; set; }

        public List<Clause> Clauses { get; set; }

        public int Level { get; set; }

        public List<ClauseGroup> NestedClauseGroups { get; set; }

        private bool isSelected;

        public bool IsSelected
        {
            get => isSelected;

            set
            {
                isSelected = value;
                OnPropertyChanged();
            }
        }

        public ClauseGroup(string id, List<Clause> clauses)
        {
            Id = id;
            Clauses = clauses;
            Level = 1;
        }

        public ClauseGroup(string id)
        {
            Id = id;
            Clauses = new List<Clause>();
            Level = 1;
        }

        public ClauseGroup(string id, int level)
        {
            Id = id;
            Clauses = new List<Clause>();
            Level = level;
        }

        public IEnumerable Items
        {
            get
            {
                var items = new CompositeCollection();
                items.Add(new CollectionContainer { Collection = Clauses });
                items.Add(new CollectionContainer { Collection = NestedClauseGroups });
                return items;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class Clause : INotifyPropertyChanged
    {
        public string Name { get; set; }

        public string XmlContent { get; set; }

        public string HtmlContent { get; set; }

        private bool isSelected;

        public bool IsSelected
        {
            get => isSelected;

            set
            {
                isSelected = value;
                OnPropertyChanged();
            }
        }

        public bool IsFixedClause { get; set; }
        public bool IsEnabled
        {
            get
            {
                return !IsFixedClause;
            }
        }

        public Word.ContentControl ContentControl { get; set; }


        public Clause(string name, string xmlContent)
        {
            Name = name;

            XmlContent = xmlContent;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}