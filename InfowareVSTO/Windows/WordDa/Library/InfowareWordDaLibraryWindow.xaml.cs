﻿using System.Collections.Generic;
using System.Windows;
using InfowareVSTO.Common;
using InfowareVSTO.Common.Language;
using InfowareVSTO.Windows.WordDa.Library;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;

namespace InfowareVSTO.Windows
{
    /// <summary>
    /// Interaction logic for InfowareWordDaLibraryWindow.xaml
    /// </summary>
    public partial class InfowareWordDaLibraryWindow : InfowareWindow
    {
        /// <summary>
        /// As defined in the "Webservice parameters" document
        /// </summary>
        private const int TEMPLATE_TYPE = 7;
        public bool Open = true;
        private bool fixedClauses;
        private bool fixedClauseGroups;

        public List<ClauseLibrary> ClauseLibraries { get; private set; }

        public InfowareWordDaLibraryWindow(bool fixedClauses = false, bool fixedClauseGroups = false)
        {
            InitializeComponent();

            InitClauseLibraries();
            this.fixedClauses = fixedClauses;
            this.fixedClauseGroups = fixedClauseGroups;
            DataContext = this;
        }

        private void InitClauseLibraries()
        {
            List<ClauseLibrary> clauseLibraries = FetchClauseLibraries();

            if (clauseLibraries == null || clauseLibraries.Count == 0)
            {
                new InfowareInformationWindow(LanguageManager.GetTranslation(LanguageConstants.NoTemplatesClauseLibrary,  "There are no templates in the clause library")).ShowDialog();
                Open = false;
                return;
            }

            ClauseLibraries = clauseLibraries;
        }

        private void OnSelectButtonClick(object sender, RoutedEventArgs e)
        {
            var selectedClauseLibrary = (ClauseLibrary) ComboClauseLibraries.SelectedItem;

            string clauseLibraryXml = FetchClauseLibraryAsXml(selectedClauseLibrary.Id);

            if (clauseLibraryXml == null)
            {
                Close();

                new InfowareInformationWindow(LanguageManager.GetTranslation(LanguageConstants.CouldNotFetchTemplate, "Could not fetch template") + " '" + selectedClauseLibrary.FileName + "'").ShowDialog();
                return;
            }

            Close();

            new InfowareWordDaLibraryClauseInsertionWindow(clauseLibraryXml, fixedClauses, selectedClauseLibrary.FileName, fixedClauseGroups).ShowDialog();
        }

        private void OnCancelButtonClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        public static List<ClauseLibrary> FetchClauseLibraries()
        {
            object authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);

            if (authKey != null)
            {
                var client = new RestClient(ConfigSettings.WebApiUrl);

                string relativeUrl = $"api/WebApi/GetTemplates?authKey={authKey}&type={TEMPLATE_TYPE}";

                var request = new RestRequest(relativeUrl, Method.GET);
                IRestResponse response = client.Execute(request);

                if (!response.IsSuccessful)
                {
                    return null;
                }

                JObject content;

                try
                {
                    content = JsonConvert.DeserializeObject<JObject>(response.Content);
                }
                catch (JsonReaderException)
                {
                    // The response didn't contain valid JSON
                    return null;
                }

                int statusCode = content["StatusCode"].ToObject<int>();

                var clauseLibraries = new List<ClauseLibrary>();

                if (statusCode == 200)
                {
                    JToken rawFileNames = content["Data"];

                    foreach (JToken item in rawFileNames)
                    {
                        JProperty property = item.ToObject<JProperty>();

                        string id = property.Name;
                        string fileName = item.First.ToString();

                        clauseLibraries.Add(new ClauseLibrary(id, fileName));
                    }

                    return clauseLibraries;
                }
            }

            return null;
        }

        public static string FetchClauseLibraryAsXml(string id)
        {
            object authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);

            if (authKey != null)
            {
                var client = new RestClient(ConfigSettings.WebApiUrl);

                string relativeUrl = $"api/WebApi/ConvertTOOOXMLAndDownloadTemplate?authKey={authKey}&templateId={id}";

                var request = new RestRequest(relativeUrl, Method.GET);
                IRestResponse response = client.Execute(request);

                if (!response.IsSuccessful)
                {
                    return null;
                }

                var clauseLibraries = new List<ClauseLibrary>();

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    return response.Content;
                }
            }

            return null;
        }
    }

    public class ClauseLibrary
    {
        public string Id { get; set; }

        public string FileName { get; set; }

        public ClauseLibrary(string id, string fileName)
        {
            Id = id;
            FileName = fileName;
        }

        public override string ToString()
        {
            return FileName;
        }
    }
}
