﻿using InfowareVSTO.Common;
using Microsoft.Office.Interop.Word;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using static InfowareVSTO.Windows.WordDa.Library.InfowareWordDaLibraryClauseInsertionWindow;

namespace InfowareVSTO.Windows.WordDa.Library
{
    public class ClauseLibraryHelper
    {
        public const string BODY_TAG = "body";
        public string ClauseLibraryXml { get; private set; }
        private int bodyStartIndex;
        public int BodyStartIndex 
        { get
            {
                if (bodyStartIndex == 0)
                {
                    bodyStartIndex = ClauseLibraryXml.IndexOf(OPENING_BODY_TAG) + OPENING_BODY_TAG.Length;
                }
                return bodyStartIndex;
            }
        }
        private int bodyEndIndex;
        public int BodyEndIndex
        {
            get
            {
                if (bodyEndIndex == 0)
                {
                    bodyEndIndex = ClauseLibraryXml.IndexOf(CLOSING_BODY_TAG);
                }
                return bodyEndIndex;
            }
        }

        public ClauseLibraryHelper(string xml)
        {
            ClauseLibraryXml = xml;
        }

        public List<ClauseGroup> GetAllClauseGroups()
        {
            var clauseGroups = new List<ClauseGroup>();

            /*
             * Concatenating an XML namespace alias directly fails to work. We can fix the issue by sing XNamespace
             * https://stackoverflow.com/questions/2575546/the-character-hexadecimal-value-0x3a-cannot-be-included-in-a-name
             */

            var root = XDocument.Parse(ClauseLibraryXml);

            foreach (XElement sdtElement in root.Descendants(InfowareWordDaLibraryClauseInsertionWindow.wordNamespace + STRUCTURED_DOCUMENT_TAG_ELEMENT_NAME))
            {
                foreach (XElement groupOrClauseElement in GetGroupOrClauseElements(sdtElement))
                {
                    XElement aliasElement = groupOrClauseElement.Elements(wordNamespace + ALIAS_ELEMENT_NAME).FirstOrDefault();
                    string alias = GetAttributeValue(aliasElement, wordNamespace + VAL_ELEMENT_ATTRIBUTE_NAME);

                    int colonIndex = alias.IndexOf(':');

                    if (colonIndex == -1)
                    {
                        continue;
                    }

                    string itemName = alias.Substring(colonIndex + 1);

                    if (alias.StartsWith(CLAUSE_GROUP_TITLE_PREFIX))
                    {
                        int level = 1;
                        var style = sdtElement.Descendants(wordNamespace + "pStyle").FirstOrDefault();
                        if (style != null)
                        {
                            var match = System.Text.RegularExpressions.Regex.Match(GetAttributeValue(style, wordNamespace + VAL_ELEMENT_ATTRIBUTE_NAME), "\\d");

                            string levelStr = match.Value;

                            if (int.TryParse(levelStr, out int intResult))
                            {
                                level = intResult;
                            }
                        }

                        clauseGroups.Add(new ClauseGroup(itemName.Trim(), level));
                    }
                    else if (alias.StartsWith(CLAUSE_TITLE_PREFIX))
                    {
                        ClauseGroup lastClauseGroup = clauseGroups.LastOrDefault();

                        // Ignore clauses that don't immediately follow a group
                        if (lastClauseGroup == null)
                        {
                            continue;
                        }

                        XElement clauseElement = groupOrClauseElement;

                        lastClauseGroup.Clauses.Add(new Clause(itemName.Trim(), GetClauseXmlContent(sdtElement)));
                    }
                    else
                    {
                        continue;
                    }
                }
            }

            return ParseGroupLevels(clauseGroups);
        }

        private List<XElement> GetGroupOrClauseElements(XElement parentElement)
        {
            var elements = new List<XElement>();

            foreach (XElement element in parentElement.Elements(wordNamespace + STRUCTURED_DOCUMENT_TAG_PROPERTIES_ELEMENT_NAME))
            {
                XElement tagElement = element.Elements(wordNamespace + TAG_ELEMENT_NAME).FirstOrDefault();
                XElement aliasElement = element.Elements(wordNamespace + ALIAS_ELEMENT_NAME).FirstOrDefault();

                if (tagElement == null || aliasElement == null)
                {
                    continue;
                }

                string tag = GetAttributeValue(tagElement, wordNamespace + VAL_ELEMENT_ATTRIBUTE_NAME);
                string alias = GetAttributeValue(aliasElement, wordNamespace + VAL_ELEMENT_ATTRIBUTE_NAME);

                if (tag == null || alias == null)
                {
                    continue;
                }

                if (tag != CITATION_CONTROL_TAG)
                {
                    continue;
                }

                elements.Add(element);
            }

            return elements;
        }

        private string GetClauseXmlContent(XElement clauseElement)
        {
            var contentStringBuilder = new StringBuilder();

            XElement contentElement = clauseElement.Elements(wordNamespace + STRUCTURED_DOCUMENT_TAG_CONTENT_ELEMENT_NAME).FirstOrDefault();

            if (contentElement == null)
            {
                return string.Empty;
            }

            foreach (XElement contentPartElement in contentElement.Elements())
            {
                contentStringBuilder.Append(contentPartElement.ToString());
            }
            string result = contentStringBuilder.ToString();

            if(result.IndexOf("<w:p") == -1)
            {
                result = $"<w:p xmlns:w=\"{wordNamespace}\">{result}</w:p>";
            }

            return result;
        }

        public string GetValidXmlDocument(string xmlContent)
        {
            int bodyStartIndex = BodyStartIndex;

            int bodyEndIndex = BodyEndIndex;

            return ClauseLibraryXml.Substring(0, bodyStartIndex + 1) + xmlContent + ClauseLibraryXml.Substring(bodyEndIndex);
        }

        public static string GetAttributeValue(XElement element, XName attribute)
        {
            return element.Attribute(attribute)?.Value;
        }

        public string ConvertXmlDocumentToHtml(string xmlDocument)
        {
            var client = new RestClient(ConfigSettings.WebApiUrl);

            string relativeUrl = "api/WebApi/GetHTMLofOOXMLEncoded";

            var request = new RestRequest(relativeUrl, Method.POST);

            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            request.AddParameter("inputFile", xmlDocument);

            IRestResponse response = client.Execute(request);

            if (!response.IsSuccessful)
            {
                return null;
            }

            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return response.Content;
            }

            return null;
        }

        public List<ClauseGroup> ParseGroupLevels(List<ClauseGroup> input)
        {
            List<ClauseGroup> result = new List<ClauseGroup>();

            if (input != null)
            {
                foreach (ClauseGroup group in input)
                {
                    ClauseGroup addGroup = FindLastClauseGroupWithSmallerLevel(result, group.Level);
                    if (addGroup == null)
                    {
                        group.Level = 1;
                        result.Add(group);
                    }
                    else
                    {
                        if (addGroup.NestedClauseGroups == null)
                        {
                            addGroup.NestedClauseGroups = new List<ClauseGroup>();
                        }

                        group.Level = addGroup.Level + 1;
                        addGroup.NestedClauseGroups.Add(group);

                    }
                }
            }

            return result;
        }

        private ClauseGroup FindLastClauseGroupWithSmallerLevel(List<ClauseGroup> input, int level)
        {
            ClauseGroup result = null;

            if (input != null)
            {
                ClauseGroup last = input.LastOrDefault();

                if (last != null)
                {
                    if (last.Level < level - 1)
                    {
                        if (last.NestedClauseGroups == null)
                        {
                            return last;
                        }
                        else
                        {
                            return FindLastClauseGroupWithSmallerLevel(last.NestedClauseGroups, level) ?? last;
                        }
                    }
                    else if (last.Level == level - 1)
                    {
                        return last;
                    }
                    else
                    {
                        return null;
                    }
                }
            }

            return result;
        }

        public static bool HasDocumentContentControlDirectlyInBody(string xml)
        {
            if (xml != null)
            {
                var root = XDocument.Parse(xml);

                var body = root.Descendants(wordNamespace + BODY_TAG).First();

                foreach(var element in body.Elements())
                {
                    if (element.Name.LocalName == STRUCTURED_DOCUMENT_TAG_ELEMENT_NAME)
                    {
                        return true;
                    }
                }
            }

            return false;
        }
    }
}
