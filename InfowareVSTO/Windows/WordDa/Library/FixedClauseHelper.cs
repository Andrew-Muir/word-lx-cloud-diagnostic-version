﻿using InfowareVSTO.Common;
using InfowareVSTO.Common.General;
using InfowareVSTO.Util;
using Microsoft.Office.Interop.Word;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Formatting;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using System.Windows.Input;

namespace InfowareVSTO.Windows.WordDa.Library
{
    public class FixedClauseHelper
    {
        public List<ClauseLibrary> ClauseLibraries { get; set; }

        public Dictionary<string, ClauseLibraryHelper> clauseLibraryHelpers = new Dictionary<string, ClauseLibraryHelper>();

        public Dictionary<string, List<ClauseGroup>> clauseGroups = new Dictionary<string, List<ClauseGroup>>();

        public FixedClauseHelper()
        {
            ClauseLibraries = InfowareWordDaLibraryWindow.FetchClauseLibraries();
        }

        public void FillFixedClauseContentControl(ContentControl contentControl)
        {
            var reference = GetFixedClauseReferences(contentControl);

            if (reference == null)
            {
                return;
            }

            ClauseLibraryHelper clh = null;
            if (!clauseLibraryHelpers.TryGetValue(reference.LibraryName, out clh))
            {
                ClauseLibrary cl = ClauseLibraries.Where(x => x.FileName == reference.LibraryName).FirstOrDefault();
                if (cl == null)
                {
                    return;
                }

                string xml = InfowareWordDaLibraryWindow.FetchClauseLibraryAsXml(cl.Id);
                clh = new ClauseLibraryHelper(xml);
                clauseLibraryHelpers.Add(reference.LibraryName, clh);
            }

            if (!clauseGroups.TryGetValue(reference.LibraryName, out var groups))
            {
                groups = clh.GetAllClauseGroups();
                clauseGroups[reference.LibraryName] = groups;
            }

            Clause clause = null;
            for (int i = 0; i < reference.GroupNames.Count; i++)
            {
                if (i == reference.GroupNames.Count - 1)
                {
                    clause = groups?.Where(x => x.Id == reference.GroupNames[i])?.FirstOrDefault()?.Clauses?.Where(x => x.Name == reference.ClauseName)?.FirstOrDefault();
                }
                else
                {
                    groups = groups?.Where(x => x.Id == reference.GroupNames[i])?.FirstOrDefault()?.NestedClauseGroups;
                }
            }

            if (clause != null)
            {
                string xmlToInsert = clh.GetValidXmlDocument(clause.XmlContent);
             
                if (reference.Unformatted)
                {
                    Range range = contentControl.Range;
                    range.Collapse(WdCollapseDirection.wdCollapseStart);
                    range.MoveStart(WdUnits.wdCharacterFormatting, -1);
                    range.MoveEnd(WdUnits.wdCharacterFormatting, -1);
                    range.Select();
                    Globals.ThisAddIn.Application.Selection.CopyFormat();
                    DocumentUtils.InsertXMLWithoutExtraParagraphOriginal(contentControl, xmlToInsert);
                    range = contentControl.Range;
                    range.Select();
                    Globals.ThisAddIn.Application.Selection.PasteFormat();
                    range.Collapse(WdCollapseDirection.wdCollapseEnd);
                    range.Select();
                }
                else
                {
                    DocumentUtils.InsertXMLWithoutExtraParagraphOriginal(contentControl, xmlToInsert);
                }
            }
        }

        public void InsertQuestions()
        {
            foreach (ClauseLibraryHelper clh in clauseLibraryHelpers.Values)
            {
                InfowareWordDaLibraryClauseInsertionWindow clauseInsertionWindow = new InfowareWordDaLibraryClauseInsertionWindow(clh.ClauseLibraryXml);
                clauseInsertionWindow.UpdateActiveDocument();
            }
        }


        public static FixedClauseReference GetFixedClauseReferences(ContentControl cc)
        {
            string title = cc.Title;

            string[] strArr = title.Split(new string[] { CustomConstants.FIXED_CLAUSE_LIBRARY, CustomConstants.FIXED_CLAUSE_GROUP, CustomConstants.FIXED_CLAUSE_CLAUSE, CustomConstants.FIXED_CLAUSE_UNFORMATTED, }, StringSplitOptions.RemoveEmptyEntries);

            if (strArr.Length < 4)
            {
                return null;
            }
            string libraryName = strArr[0];
            List<string> groupNames = new List<string>();
            for (int i = 1; i < strArr.Length - 2; i++)
            {
                groupNames.Add(strArr[i]);
            }

            string clauseName = strArr[strArr.Length - 2];
            string unformattedStr = strArr[strArr.Length - 1];

            if (int.TryParse(unformattedStr, out int intResult))
            {
                bool unformatted = intResult == 1;
                return new FixedClauseReference()
                {
                    LibraryName = libraryName,
                    GroupNames = groupNames,
                    ClauseName = clauseName,
                    Unformatted = unformatted
                };
            }

            return null;
        }

        public class FixedClauseReference
        {
            public string LibraryName { get; set; }
            public List<string> GroupNames { get; set; }
            public string ClauseName { get; set; }
            public bool Unformatted { get; set; }
        }
    }
}
