﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DocumentFormat.OpenXml.Wordprocessing;
using InfowareVSTO.Common;
using InfowareVSTO.Common.General;
using Microsoft.Office.Interop.Word;

namespace InfowareVSTO.Windows.WordDa.Library
{
    public class FixedClauseGroupHelper
    {
        public List<ClauseLibrary> ClauseLibraries { get; set; }

        public Dictionary<string, ClauseLibraryHelper> clauseLibraryHelpers = new Dictionary<string, ClauseLibraryHelper>();
        
        public Dictionary<string, List<ClauseGroup>> clauseGroups = new Dictionary<string, List<ClauseGroup>>();

        public FixedClauseGroupHelper()
        {
            ClauseLibraries = InfowareWordDaLibraryWindow.FetchClauseLibraries();
        }

        public void FillFixedClauseContentControl(ContentControl contentControl)
        {
            var reference = GetFixedClauseGroupReferences(contentControl);

            if (reference == null)
            {
                return;
            }

            ClauseLibraryHelper clh = null;

            if (!clauseLibraryHelpers.TryGetValue(reference.LibraryName, out clh))
            {
                ClauseLibrary cl = ClauseLibraries.Where(x => x.FileName == reference.LibraryName).FirstOrDefault();
                if (cl == null)
                {
                    return;
                }

                string xml = InfowareWordDaLibraryWindow.FetchClauseLibraryAsXml(cl.Id);
                clh = new ClauseLibraryHelper(xml);
                clauseLibraryHelpers.Add(reference.LibraryName, clh);
            }

            if (!clauseGroups.TryGetValue(reference.LibraryName, out var groups))
            {
                groups = clh.GetAllClauseGroups();
                clauseGroups[reference.LibraryName] = groups;
            }

            var dialog = new InfowareWordDaLibraryClauseInsertionWindow(clh.ClauseLibraryXml, groupNames: reference.GroupNames);
            dialog.ShowDialog();
            Clause clause = dialog.SelectedClauses.FirstOrDefault();

            if (clause != null)
            {
                string xmlToInsert = clh.GetValidXmlDocument(clause.XmlContent);

                if (dialog.Unformatted.IsChecked == true)
                {
                    Range range = contentControl.Range;
                    range.Collapse(WdCollapseDirection.wdCollapseStart);
                    range.MoveStart(WdUnits.wdCharacterFormatting, -1);
                    range.MoveEnd(WdUnits.wdCharacterFormatting, -1);
                    range.Select();
                    Globals.ThisAddIn.Application.Selection.CopyFormat();
                    DocumentUtils.InsertXMLWithoutExtraParagraphOriginal(contentControl, xmlToInsert);
                    range = contentControl.Range;
                    range.Select();
                    Globals.ThisAddIn.Application.Selection.PasteFormat();
                    range.Collapse(WdCollapseDirection.wdCollapseEnd);
                    range.Select();
                }
                else
                {
                    DocumentUtils.InsertXMLWithoutExtraParagraphOriginal(contentControl, xmlToInsert);
                }
            }
        }

        public void InsertQuestions()
        {
            foreach (ClauseLibraryHelper clh in clauseLibraryHelpers.Values)
            {
                InfowareWordDaLibraryClauseInsertionWindow clauseInsertionWindow = new InfowareWordDaLibraryClauseInsertionWindow(clh.ClauseLibraryXml);
                clauseInsertionWindow.UpdateActiveDocument();
            }
        }

        public static FixedClauseGroupReference GetFixedClauseGroupReferences(ContentControl cc)
        {
            string title = cc.Title;

            string[] strArr = title.Split(new string[] { CustomConstants.FIXED_CLAUSE_LIBRARY, CustomConstants.FIXED_CLAUSE_GROUP }, StringSplitOptions.RemoveEmptyEntries);

            if (strArr.Length < 2)
            {
                return null;
            }
            string libraryName = strArr[0];
            List<string> groupNames = new List<string>();
            for (int i = 1; i < strArr.Length; i++)
            {
                groupNames.Add(strArr[i]);
            }

            return new FixedClauseGroupReference()
            {
                LibraryName = libraryName,
                GroupNames = groupNames
            };
        }

        public class FixedClauseGroupReference
        {
            public string LibraryName { get; set; }
            public List<string> GroupNames { get; set; }
        }
    }
}
