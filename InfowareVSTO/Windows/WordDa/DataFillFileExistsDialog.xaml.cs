﻿using DocumentFormat.OpenXml.Drawing.Charts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace InfowareVSTO.Windows.WordDa
{
    /// <summary>
    /// Interaction logic for DataFillFileExistsDialog.xaml
    /// </summary>
    public partial class DataFillFileExistsDialog : InfowareWindow
    {
        public DataFillFileExistsResult Result = DataFillFileExistsResult.Cancel;

        public DataFillFileExistsDialog()
        {
            InitializeComponent();
        }

        private void Overwrite_Click(object sender, RoutedEventArgs e)
        {
            Result = DataFillFileExistsResult.Overwrite;
            this.Close();
        }

        private void Add_Click(object sender, RoutedEventArgs e)
        {
            Result = DataFillFileExistsResult.Add;
            this.Close();
        }

        private void Rename_Click(object sender, RoutedEventArgs e)
        {
            Result = DataFillFileExistsResult.Rename;
            this.Close();
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            Result = DataFillFileExistsResult.Cancel;
            this.Close();
        }
    }

    public enum DataFillFileExistsResult
    {
        Overwrite,
        Add, 
        Rename,
        Cancel
    }
}
