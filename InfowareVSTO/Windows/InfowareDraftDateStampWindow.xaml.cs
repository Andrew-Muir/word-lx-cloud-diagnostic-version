﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using InfowareVSTO.Common;
using InfowareVSTO.Common.General;
using InfowareVSTO.Common.Language;
using InfowareVSTO.MultiLanguage;
using InfowareVSTO.Util;
using Microsoft.Office.Core;
using Microsoft.Office.Interop.Word;
using Shape = Microsoft.Office.Interop.Word.Shape;

namespace InfowareVSTO.Windows
{
    /// <summary>
    /// Interaction logic for InfowareDraftDateStampWindow.xaml
    /// </summary>
    public partial class InfowareDraftDateStampWindow : InfowareWindow
    {
        //private const string NO_DATE_OPTION = "No Date";

        //private const string LAST_SAVED_DATE_OPTION = "Last Saved Date";

        //private const string CURRENT_DATE_OPTION = "Current Date";

        private static string NO_DATE_OPTION = LanguageManager.GetTranslation(LanguageConstants.NoDate, "No date");

        private static string LAST_SAVED_DATE_OPTION = LanguageManager.GetTranslation(LanguageConstants.LastSavedDateAutoUpdate, "Last Saved Date - Auto Update");

        private static string CURRENT_DATE_OPTION = LanguageManager.GetTranslation(LanguageConstants.CurrentDateNoUpdate, "Current Date - No Update");

        private const string DRAFT_CONTROL_TAG = "LX-DRAFTDATESTAMP";
        private const string DRAFT_CONTROL_PROPERTY_TAG = "LX-DRAFTDATESTAMP-PROPERTY";

        private const string DRAFT_CONTROL_NAME = "LX-DRAFTDATESTAMP_{0}";

        private const string DRAFT_NUMBER_PLACEHOLDER = "##LX-DRAFTNR##";

        private const string DRAFT_DATE_PLACEHOLDER = "##LX-DRAFTDATE##";
        private const string DRAFT_TEXT_PLACEHOLDER = "##LX-DRAFTTEXT##";
        private const string FOR_DISCUSSION_PLACEHOLDER = "##LX-FORDISCUSSION##";
        private const string SAVED_BY_PLACEHOLDER = "##LX-SAVEDBY##";

        //private const string DATE_FORMAT = "MMMM d, yyyy";

        //private static readonly string DATE_WITH_TIME_FORMAT = $"{DATE_FORMAT} - h:mm tt";

        private string dateFormat;

        private string timeFormat;
        private string wordTimeFormat;

        private string dateTimeSeparator;

        private bool showUnsavedDocumentWarning;

        private LegacyDraftDateStamp legacyDraftDateStamp;

        public static List<string> DateOptions { get; private set; }

        private Document nativeDocument;

        private Microsoft.Office.Tools.Word.Document vstoDocument;

        //static InfowareDraftDateStampWindow()
        //{
        //    DateOptions = new List<string>()
        //    {
        //        NO_DATE_OPTION,
        //        LAST_SAVED_DATE_OPTION,
        //        CURRENT_DATE_OPTION
        //    };
        //}

        public InfowareDraftDateStampWindow()
        {
            InitializeComponent();

            nativeDocument = Globals.ThisAddIn.Application.ActiveDocument;

            vstoDocument = Globals.Factory.GetVstoObject(nativeDocument);

            InitUi();

            legacyDraftDateStamp = new LegacyDraftDateStamp();
            legacyDraftDateStamp.Read();
            if (legacyDraftDateStamp.HasLegacyDraftDateStamp)
            {
                ApplyLegacyDraftDateStampToUI();
            }

            DataContext = this;
        }

        private void ApplyLegacyDraftDateStampToUI()
        {
            this.TbDraftNumber.Text = legacyDraftDateStamp.DraftNumber.ToString();
            switch (legacyDraftDateStamp.Location)
            {
                case Common.DraftDateStampLocation.Left:
                    RbLeftMargin.IsChecked = true;
                    break;
                case Common.DraftDateStampLocation.Right:
                    RbRightMargin.IsChecked = true;
                    break;
                case Common.DraftDateStampLocation.Top:
                    RbTopMargin.IsChecked = true;
                    break;
            }
            ComboDateOptions.SelectedIndex = (int)legacyDraftDateStamp.DateType;
            CbIncludeTime.IsChecked = legacyDraftDateStamp.IncludeTime;
            this.BtnRemove.IsEnabled = true;
        }

        private void InitUi()
        {
            UpdateRadioButtonState();

            UpdateDateOptions();

            UpdateTimeFormat();

            UpdateDateFormat();

            UpdateTimeSeparator();

            UpdateShowUnsavedDocumentWarning();

            UpdateRemoveButtonState();
        }

        private void UpdateRadioButtonState()
        {
            string defaultLocationValue = SettingsManager.GetSetting("DraftDateDefaultLocation", "Tools");

            if (defaultLocationValue == null)
            {
                CheckTopMarginRadioButton();
                return;
            }

            if (int.TryParse(defaultLocationValue, out int defaultLocation))
            {
                if (defaultLocation == 0)
                {
                    CheckTopMarginRadioButton();
                }
                else if (defaultLocation == 1)
                {
                    CheckLeftMarginRadioButton();
                }
                else
                {
                    CheckRightMarginRadioButton();
                }
            }
            else
            {
                // Default to the Top radiobutton in case the service web returns a non-integer
                CheckTopMarginRadioButton();
            }
        }

        private void CheckTopMarginRadioButton()
        {
            RbTopMargin.IsChecked = true;
        }

        private void CheckLeftMarginRadioButton()
        {
            RbLeftMargin.IsChecked = true;
        }

        private void CheckRightMarginRadioButton()
        {
            RbRightMargin.IsChecked = true;
        }

        private void UpdateDateOptions()
        {
            string rawValue = SettingsManager.GetSetting("DraftDateDisplay", "Tools");

            if (rawValue == null)
            {
                DateOptions = new List<string>()
                {
                    NO_DATE_OPTION,
                    LAST_SAVED_DATE_OPTION,
                    CURRENT_DATE_OPTION
                };

                return;
            }

            string[] values = SettingsManager.UnescapeCommaCharacter(rawValue.Split(','));

            DateOptions = values.Select(it => it.Trim()).ToList();
        }

        private void UpdateDateFormat()
        {
            string dateFormat = SettingsManager.GetSetting("DraftDateFormat", "Tools", null, MLanguageUtil.ActiveDocumentLanguage);

            if (string.IsNullOrEmpty(dateFormat))
            {
                dateFormat = "MMMM d, yyyy";
            }

            this.dateFormat = dateFormat;
        }

        private void UpdateTimeFormat()
        {
            string timeFormat = SettingsManager.GetSetting("DraftTimeFormat", "Tools", null, MLanguageUtil.ActiveDocumentLanguage);

            if (string.IsNullOrEmpty(timeFormat))
            {
                timeFormat = "h:mm tt";
                wordTimeFormat = "h:mm am/pm";
            }
            else
            {
                var amPmRegex = new Regex("am[/-]pm", RegexOptions.IgnoreCase | RegexOptions.Compiled);
                wordTimeFormat = timeFormat;
                if (amPmRegex.IsMatch(timeFormat))
                {
                    // "tt" renders AM / PM, based on the hour
                    timeFormat = amPmRegex.Replace(timeFormat, "tt");
                }

            }

            this.timeFormat = timeFormat;
        }

        private void UpdateTimeSeparator()
        {
            string dateTimeSeparator = SettingsManager.GetSetting("DraftDateTimeSeparator", "Tools", null, MLanguageUtil.ActiveDocumentLanguage);

            if (string.IsNullOrEmpty(dateTimeSeparator))
            {
                dateTimeSeparator = "-";
            }

            this.dateTimeSeparator = dateTimeSeparator;
        }

        private void UpdateShowUnsavedDocumentWarning()
        {
            showUnsavedDocumentWarning = SettingsManager.GetSettingAsBool("DraftDateSaveWarning", "Tools", true);
        }

        private void UpdateRemoveButtonState()
        {
            BtnRemove.IsEnabled = DocumentContainsDraftTimeStamp();
        }

        private void OnRemoveButtonClick(object sender, RoutedEventArgs e)
        {
            RemoveDraftTimeStamp();
            Close();
        }

        private void OnOkButtonClick(object sender, RoutedEventArgs e)
        {
            if (!DocumentUtil.ShowCompatibilityModeErrorPreventive())
            {
                Close();
                return;
            }
            RemoveDraftTimeStamp();

            if ((bool)RbTopMargin.IsChecked)
            {
                InsertTopDraftTimeStamp();
            }
            else if ((bool)RbLeftMargin.IsChecked)
            {
                InsertLeftDraftTimeStamp();
            }
            else if ((bool)RbRightMargin.IsChecked)
            {
                InsertRightDraftTimeStamp();
            }

            Close();
        }

        private void InsertTopDraftTimeStamp()
        {
            string xml = AdminPanelWebApi.GetCustomTopDraftDateStamp(MLanguageUtil.ActiveDocumentLanguage);
            if (string.IsNullOrWhiteSpace(xml))
            {
                InsertDraftTimeStamp("Top");
            }
            else
            {
                InsertCustomDraftDateStamp(xml);
            }
        }


        private void InsertLeftDraftTimeStamp()
        {
            string xml = AdminPanelWebApi.GetCustomLeftDraftDateStamp(MLanguageUtil.ActiveDocumentLanguage);
            if (string.IsNullOrWhiteSpace(xml))
            {
                InsertDraftTimeStamp("Left");
            }
            else
            {
                InsertCustomDraftDateStamp(xml);
            }
        }

        private void InsertRightDraftTimeStamp()
        {
            string xml = AdminPanelWebApi.GetCustomRightDraftDateStamp(MLanguageUtil.ActiveDocumentLanguage);
            if (string.IsNullOrWhiteSpace(xml))
            {
                InsertDraftTimeStamp("Right");
            }
            else
            {
                InsertCustomDraftDateStamp(xml);
            }
        }

        private void InsertCustomDraftDateStamp(string xml)
        {
            ThisAddIn.Instance.Application.ScreenUpdating = false;
            if (ThisAddIn.Instance.IsFunctionForLogging("ScreenUpdating"))
                Logger.Log("Disabled Screen Updating (InfowareDraftDateStampWindow.xaml.cs > InsertCustomDraftDateStamp)");
            foreach (HeaderFooter header in GetExistingHeaders())
            {
                Range headerEndRange = header.Range.Duplicate;
                headerEndRange.Collapse(WdCollapseDirection.wdCollapseEnd);

                var control = nativeDocument.ContentControls.Add(WdContentControlType.wdContentControlRichText, headerEndRange);
                control.Title = GetNewDraftControlName();
                control.Tag = DRAFT_CONTROL_TAG;

                DocumentUtils.InsertXMLWithoutExtraParagraph(control, xml);

                FillCustomDraftDateStamp(control);
            }
            ThisAddIn.Instance.Application.ScreenUpdating = true;
            if (ThisAddIn.Instance.IsFunctionForLogging("ScreenUpdating"))
                Logger.Log("Enabled Screen Updating (InfowareDraftDateStampWindow.xaml.cs > InsertCustomDraftDateStamp)");
        }

        private void FillCustomDraftDateStamp(Microsoft.Office.Interop.Word.ContentControl control)
        {
            List<Microsoft.Office.Interop.Word.ContentControl> contentControls = new List<Microsoft.Office.Interop.Word.ContentControl>();
            contentControls.AddRange(control.Range.ContentControls.Cast<Microsoft.Office.Interop.Word.ContentControl>());

            foreach (Shape shape in control.Range.ShapeRange)
            {
                contentControls.AddRange(shape.TextFrame.TextRange.ContentControls.Cast<Microsoft.Office.Interop.Word.ContentControl>());
            }

            foreach (Microsoft.Office.Interop.Word.ContentControl cc in contentControls)
            {
                if (cc.Tag == DRAFT_CONTROL_PROPERTY_TAG)
                {

                    switch (cc.Title)
                    {
                        case "Number":
                            cc.Range.Text = TbDraftNumber.Text.SpaceIfEmpty();
                            break;
                        case "Date":
                            bool includeTime = CbIncludeTime.IsEnabled && (bool)CbIncludeTime.IsChecked;
                            int selectedDateOption = GetSelectedDateOption();

                            switch (selectedDateOption)
                            {
                                case 0:
                                    cc.Range.Text = " ";
                                    break;
                                case 1:
                                    if (includeTime)
                                    {
                                        cc.Range.Text = $" {dateTimeSeparator} ";

                                        var startRange = cc.Range.Duplicate;
                                        var endRange = cc.Range.Duplicate;

                                        startRange.Collapse(WdCollapseDirection.wdCollapseStart);
                                        endRange.Collapse(WdCollapseDirection.wdCollapseEnd);

                                        startRange.Fields.Add(startRange, WdFieldType.wdFieldSaveDate, $"\\@ \"{dateFormat}\"  \\* MERGEFORMAT ");
                                        endRange.Fields.Add(endRange, WdFieldType.wdFieldSaveDate, $"\\@ \"{wordTimeFormat}\"  \\* MERGEFORMAT ");
                                    }
                                    else
                                    {
                                        cc.Range.Fields.Add(cc.Range, WdFieldType.wdFieldSaveDate, $"\\@ \"{dateFormat}\"  \\* MERGEFORMAT ");
                                    }

                                    break;
                                case 2:
                                    cc.Range.Text = GetCurrentDate(includeTime);
                                    break;
                            }

                            break;
                    }

                }
            }
        }

        private void InsertDraftTimeStamp(string location)
        {

            if (!DocumentUtil.IsDocumentSaved())
            {
                string message = LanguageManager.GetTranslation(LanguageConstants.TheActiveDocumentHasNotBeenSaved, "The active document has not been saved. Press OK to continue the insertion operation anyway");
                MessageBoxResult result = MessageBox.Show(message, Title, MessageBoxButton.OKCancel, MessageBoxImage.Exclamation);

                if (result == MessageBoxResult.Cancel)
                {
                    return;
                }
            }

            string date = "";
            bool includeTime = CbIncludeTime.IsEnabled && (bool)CbIncludeTime.IsChecked;

            StringBuilder templateNameBuilder = new StringBuilder("Draft").Append(location);

            int selectedDateOption = GetSelectedDateOption();

            switch (selectedDateOption)
            {
                case 0:
                    templateNameBuilder.Append("NoDate");

                    break;
                case 1:
                    TryGetDocumentSaveDate(includeTime, out date);
                    templateNameBuilder.Append("Lsd");

                    break;
                case 2:
                    templateNameBuilder.Append("CurrentDate");
                    date = GetCurrentDate(includeTime);

                    break;
            }

            if (selectedDateOption != 0 && includeTime)
            {
                templateNameBuilder.Append("WithTime");
            }

            string template = Properties.Resources.ResourceManager.GetString(templateNameBuilder.ToString());

            template = template.Replace(DRAFT_NUMBER_PLACEHOLDER, TbDraftNumber.Text);

            template = template.Replace(DRAFT_DATE_PLACEHOLDER, date);

            template = template.Replace(DRAFT_TEXT_PLACEHOLDER, MLanguageUtil.GetResource(MLanguageResourceEnum.DDSDraft));
            template = template.Replace(FOR_DISCUSSION_PLACEHOLDER, MLanguageUtil.GetResource(MLanguageResourceEnum.DDSForDiscussionPurposesOnly));
            template = template.Replace(SAVED_BY_PLACEHOLDER, MLanguageUtil.GetResource(MLanguageResourceEnum.DDSSavedBy));
            ThisAddIn.Instance.Application.ScreenUpdating = false;
            if (ThisAddIn.Instance.IsFunctionForLogging("ScreenUpdating"))
                Logger.Log("Disabled Screen Updating (InfowareDraftDateStampWindow.xaml.cs > InsertDraftTimeStamp)");
            foreach (HeaderFooter header in GetExistingHeaders())
            {
                Range headerEndRange = header.Range.Duplicate;
                headerEndRange.Collapse(WdCollapseDirection.wdCollapseEnd);

                var control = nativeDocument.ContentControls.Add(WdContentControlType.wdContentControlRichText, headerEndRange);
                control.Title = GetNewDraftControlName();
                control.Tag = DRAFT_CONTROL_TAG;

                DocumentUtils.InsertXMLWithoutExtraParagraph(control, template);
            }
            ThisAddIn.Instance.Application.ScreenUpdating = true;
            if (ThisAddIn.Instance.IsFunctionForLogging("ScreenUpdating"))
                Logger.Log("Enabled Screen Updating (InfowareDraftDateStampWindow.xaml.cs > InsertDraftTimeStamp)");
        }

        private void RemoveDraftTimeStamp()
        {
            if (legacyDraftDateStamp != null && legacyDraftDateStamp.HasLegacyDraftDateStamp)
            {
                legacyDraftDateStamp.RemoveMetaData();
                legacyDraftDateStamp.Delete();
            }

            var controlsToRemove = new List<Microsoft.Office.Interop.Word.ContentControl>();
            ThisAddIn.Instance.Application.ScreenUpdating = false;
            if (ThisAddIn.Instance.IsFunctionForLogging("ScreenUpdating"))
                Logger.Log("Disabled Screen Updating (InfowareDraftDateStampWindow.xaml.cs > RemoveDraftTimeStamp)");
            foreach (HeaderFooter header in GetExistingHeaders())
            {
                foreach (Microsoft.Office.Interop.Word.ContentControl control in header.Range.ContentControls)
                {
                    if (control.Tag == DRAFT_CONTROL_TAG)
                    {
                        controlsToRemove.Add(control);
                    }
                }
            }
            ThisAddIn.Instance.Application.ScreenUpdating = true;
            if (ThisAddIn.Instance.IsFunctionForLogging("ScreenUpdating"))
                Logger.Log("Enabled Screen Updating (InfowareDraftDateStampWindow.xaml.cs > RemoveDraftTimeStamp)");

            foreach (Microsoft.Office.Interop.Word.ContentControl control in controlsToRemove)
            {
                try
                {
                    control.Delete(true);
                }
                catch (Exception ex)
                {
                }
            }
        }

        private bool TryGetDocumentSaveDate(bool includeTime, out string formattedDateLastSaved)
        {
            var properties = (DocumentProperties)Globals.ThisAddIn.Application.ActiveDocument.BuiltInDocumentProperties;
            try
            {
                var dateLastSaved = (DateTime)properties[WdBuiltInProperty.wdPropertyTimeLastSaved].Value;

                if (includeTime)
                {
                    formattedDateLastSaved = $"{dateLastSaved.ToString(dateFormat, CultureInfo.CreateSpecificCulture(MLanguageUtil.GetResource(MLanguageResourceEnum.DateCultureID)))} {dateTimeSeparator} {dateLastSaved.ToString(timeFormat, CultureInfo.CreateSpecificCulture(MLanguageUtil.GetResource(MLanguageResourceEnum.DateCultureID)))}";
                }
                else
                {
                    formattedDateLastSaved = dateLastSaved.ToString(dateFormat);
                }

                return true;
            }
            catch (COMException)
            {
                formattedDateLastSaved = null;
            }

            return false;
        }

        private string GetCurrentDate(bool includeTime)
        {
            DateTime currentDate = DateTime.Now;

            if (includeTime)
            {
                return $"{currentDate.ToString(dateFormat, CultureInfo.CreateSpecificCulture(MLanguageUtil.GetResource(MLanguageResourceEnum.DateCultureID)))} {dateTimeSeparator} {currentDate.ToString(timeFormat, CultureInfo.CreateSpecificCulture(MLanguageUtil.GetResource(MLanguageResourceEnum.DateCultureID)))}";
            }
            else
            {
                return currentDate.ToString(dateFormat, CultureInfo.CreateSpecificCulture(MLanguageUtil.GetResource(MLanguageResourceEnum.DateCultureID)));
            }
        }

        private string GetNewDraftControlName()
        {
            return string.Format(DRAFT_CONTROL_NAME, Guid.NewGuid());
        }

        private int GetSelectedDateOption()
        {
            return ComboDateOptions.SelectedIndex;
        }

        private void OnCancelButtonClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void OnComboDateOptionsSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (GetSelectedDateOption() == 0)
            {
                CbIncludeTime.IsChecked = CbIncludeTime.IsEnabled = false;
            }
            else
            {
                CbIncludeTime.IsChecked = CbIncludeTime.IsEnabled = true;
            }
        }

        private bool DocumentContainsDraftTimeStamp()
        {
            ThisAddIn.Instance.Application.ScreenUpdating = false;
            if (ThisAddIn.Instance.IsFunctionForLogging("ScreenUpdating"))
                Logger.Log("Disabled Screen Updating (InfowareDraftDateStampWindow.xaml.cs > DocumentContainsDraftTimeStamp)");
            foreach (HeaderFooter header in GetExistingHeaders())
            {
                foreach (Microsoft.Office.Interop.Word.ContentControl control in header.Range.ContentControls)
                {
                    if (control.Tag == DRAFT_CONTROL_TAG)
                    {
                        return true;
                    }
                }
            }
            ThisAddIn.Instance.Application.ScreenUpdating = true;
            if (ThisAddIn.Instance.IsFunctionForLogging("ScreenUpdating"))
                Logger.Log("Enabled Screen Updating (InfowareDraftDateStampWindow.xaml.cs > DocumentContainsDraftTimeStamp)");

            return false;
        }

        private List<HeaderFooter> GetExistingHeaders()
        {
            var headers = new List<HeaderFooter>();

            foreach (Section section in nativeDocument.Sections)
            {
                if (Utils.IsSectionContinous(section, nativeDocument))
                {
                    continue;
                }

                foreach (HeaderFooter header in section.Headers)
                {
                    if (header.Exists && !header.LinkToPrevious)
                    {
                        headers.Add(header);
                    }
                }
            }

            return headers;
        }

        private void NumericTextBoxValidator(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            var regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }
    }
}
