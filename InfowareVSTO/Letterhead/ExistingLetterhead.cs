﻿using InfowareVSTO.Common;
using InfowareVSTO.Common.General;
using InfowareVSTO.Common.Models;
using Microsoft.Office.Interop.Word;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfowareVSTO.Letterhead
{
    public class ExistingLetterhead
    {
        public List<ContentControl> FirstPageLetterheadContentControls { get; set; }

        public List<ContentControl> PrimaryLetterheadContentControls { get; set; }

        private void GetLetterheads()
        {
            LetterheadToggler letterheadToggler = new LetterheadToggler();
            PrimaryLetterheadContentControls = letterheadToggler.GetHeaderContentControls(WdHeaderFooterIndex.wdHeaderFooterPrimary).Where(x => x.Tag == CustomConstants.LETTERHEAD_TAG).ToList();
            FirstPageLetterheadContentControls = letterheadToggler.GetHeaderContentControls(WdHeaderFooterIndex.wdHeaderFooterFirstPage).Where(x => x.Tag == CustomConstants.LETTERHEAD_TAG).ToList();
        }

        public void ChangeLetterhead(int officeLocationId)
        {
            GetLetterheads();
            bool hasFirstPage = FirstPageLetterheadContentControls.Count > 0;
            bool hasPrimary = PrimaryLetterheadContentControls.Count > 0;
            if (hasFirstPage || hasPrimary)
            {
                List<TemplateListModel> templates = AdminPanelWebApi.GetTemplateList(TemplateType.Letterhead, null, (int)MultiLanguage.MLanguageUtil.ActiveDocumentLanguage, officeLocationId);

                if (templates.Count == 0)
                {
                    return;
                }
                else if (templates.Count == 1)
                {
                    string letterheadXml = AdminPanelWebApi.GetTemplateAsXml(templates[0].Id);
                    if (letterheadXml != null)
                    {
                        ReplaceLetterheadContent(FirstPageLetterheadContentControls, letterheadXml);
                        ReplaceLetterheadContent(PrimaryLetterheadContentControls, letterheadXml);
                    }
                }
                else
                {
                    LetterheadDialog dialog = new LetterheadDialog();
                    dialog.PopulateTemplates(templates);
                    dialog.DisableCheckBoxes(hasFirstPage, hasPrimary);
                    dialog.ShowDialog();
                    if (dialog.ResultOk)
                    {
                        if (hasFirstPage)
                        {
                            string firstPageXml = AdminPanelWebApi.GetTemplateAsXml(dialog.FirstPageId);
                            ReplaceLetterheadContent(FirstPageLetterheadContentControls, firstPageXml);
                        }
                        if (hasPrimary)
                        {
                            string primaryXml = AdminPanelWebApi.GetTemplateAsXml(dialog.PrimaryId);
                            ReplaceLetterheadContent(PrimaryLetterheadContentControls, primaryXml);
                        }
                    }
                }
            }
        }

        private void ReplaceLetterheadContent(List<ContentControl> letterheads, string xml)
        {
            if (letterheads != null)
            { 
                foreach (ContentControl cc in letterheads)
                {
                    try
                    {
                        for (int i = cc.Range.Tables.Count; i >= 1; i--)
                        {
                            cc.Range.Tables[i].ConvertToText();
                        }
                        for (int i = cc.Range.ShapeRange.Count; i >= 1; i--)
                        {
                            cc.Range.ShapeRange[i].Delete();
                        }

                        cc.Range.Text = "";

                        DocumentUtils.InsertXMLWithoutExtraParagraph(cc, xml);
                    }
                    catch { }
                }
            }
        }
    }
}
