﻿using InfowareVSTO.Util;
using Microsoft.Office.Interop.Word;
using Word = Microsoft.Office.Interop.Word;
using Microsoft.Office.Tools.Word;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InfowareVSTO.Common.General;
using InfowareVSTO.Common.Models;
using InfowareVSTO.VSTOAuthor;
using InfowareVSTO.Common;
using InfowareVSTO.Windows.ParagraphNumbering.Util;
using System.Runtime.InteropServices;
using InfowareVSTO.TaskPaneControls.WordDa;

namespace InfowareVSTO.Letterhead
{
    public class LetterheadToggler
    {
        private Word.Document document;
        public const string TRUE = "true";
        private const string FALSE = "false";

        private Dictionary<int, string> letterheadTemplates = new Dictionary<int, string>();

        public LetterheadToggler()
        {
            document = ThisAddIn.Instance.Application.ActiveDocument;
        }

        public void ToggleLetterhead(bool saveProperty = true)
        {
            string hasLetterhead = DocumentPropertyUtil.ReadProperty(CustomConstants.LETTERHEAD_ON);

            //if has letterhead removes all letterheads
            if (hasLetterhead == TRUE)
            {
                RemoveLetterhead(saveProperty);
            }
            else
            {
                AddLetterhead(saveProperty);
            }
        }

        private void AddLetterhead(bool saveProperty)
        {
            var currentAuthorSettings = ManageUsers.CurrentAuthorSettings;
            var templateList = AdminPanelWebApi.GetTemplateList(Common.TemplateType.Letterhead, currentAuthorSettings.CompanyId, (int)MultiLanguage.MLanguageUtil.ActiveDocumentLanguage);

            Windows.ParagraphNumbering.ParagraphNumberingToolbarManager.Instance.EnsureWindowIsHidden();
            var letterheadDialog = new LetterheadDialog();
            letterheadDialog.PopulateTemplates(templateList);

            if (templateList.Count > 1)
            {
                letterheadDialog.ShowDialog();
            }
            else if (templateList.Count == 1)
            {
                letterheadDialog.OnOkButtonClick(null, null);
            }

            Windows.ParagraphNumbering.ParagraphNumberingToolbarManager.Instance.RestoreWindowIfNecessary();

            if (letterheadDialog.ResultOk)
            {
                InsertLetterhead(saveProperty, letterheadDialog);
            }
        }

        private void InsertLetterhead(bool saveProperty, LetterheadDialog letterheadDialog)
        {
            try
            {
                string firstPageXml = AdminPanelWebApi.GetTemplateAsXml(letterheadDialog.FirstPageId);
                string primaryXml = AdminPanelWebApi.GetTemplateAsXml(letterheadDialog.PrimaryId);

                List<Word.ContentControl> containingContentControls = new List<Word.ContentControl>();
                ThisAddIn.Instance.Application.ScreenUpdating = false;
                if (ThisAddIn.Instance.IsFunctionForLogging("ScreenUpdating"))
                    Logger.Log("Disabled Screen Updating (LetterheadToggler.cs > InsertLetterhead)");
                foreach (Section section in document.Sections)
                {
                    if (Utils.IsSectionContinous(section, document))
                    {
                        continue;
                    }

                    Range range;
                    Word.ContentControl letterheadCc;

                    if (letterheadDialog.AddToFirstPage)
                    {
                        //add to first page header and set different first page header property
                        section.PageSetup.DifferentFirstPageHeaderFooter = -1;

                        if (!section.Headers[WdHeaderFooterIndex.wdHeaderFooterFirstPage].LinkToPrevious)
                        {
                            range = section.Headers[WdHeaderFooterIndex.wdHeaderFooterFirstPage].Range;

                            var paragraph = range.Paragraphs.First;
                            var letterheadParagraphFormat = new LetterheadParagraphFormat
                            {
                                LeftIndent = paragraph.Format.LeftIndent,
                                RightIndent = paragraph.Format.RightIndent,
                                SpaceBefore = paragraph.Format.SpaceBefore,
                                SpaceAfter = paragraph.Format.SpaceAfter,
                                Aligment = paragraph.Format.Alignment,
                                FirstLineIndent = paragraph.Format.FirstLineIndent,
                                TabStops = paragraph.Format.TabStops
                            };

                            range.Collapse(WdCollapseDirection.wdCollapseEnd);
                            letterheadCc = document.ContentControls.Add(WdContentControlType.wdContentControlRichText, range);
                            letterheadCc.Tag = CustomConstants.LETTERHEAD_TAG;

                            DocumentUtils.InsertXMLWithoutExtraParagraphOriginal(letterheadCc, firstPageXml);

                            containingContentControls.AddRange(GetContentControls(letterheadCc));


                            paragraph.Format.LeftIndent = letterheadParagraphFormat.LeftIndent;
                            paragraph.Format.RightIndent = letterheadParagraphFormat.RightIndent;
                            paragraph.Format.SpaceBefore = letterheadParagraphFormat.SpaceBefore;
                            paragraph.Format.SpaceAfter = letterheadParagraphFormat.SpaceAfter;
                            paragraph.Format.Alignment = letterheadParagraphFormat.Aligment;
                            paragraph.Format.FirstLineIndent = letterheadParagraphFormat.FirstLineIndent;
                            paragraph.Format.TabStops = letterheadParagraphFormat.TabStops;
                        }
                    }

                    if (letterheadDialog.AddToPrimary && !section.Headers[WdHeaderFooterIndex.wdHeaderFooterPrimary].LinkToPrevious)
                    {
                        range = section.Headers[WdHeaderFooterIndex.wdHeaderFooterPrimary].Range;

                        var paragraph = range.Paragraphs.First;
                        var letterheadParagraphFormat = new LetterheadParagraphFormat
                        {
                            LeftIndent = paragraph.Format.LeftIndent,
                            RightIndent = paragraph.Format.RightIndent,
                            SpaceBefore = paragraph.Format.SpaceBefore,
                            SpaceAfter = paragraph.Format.SpaceAfter,
                            Aligment = paragraph.Format.Alignment,
                            FirstLineIndent = paragraph.Format.FirstLineIndent,
                            TabStops = paragraph.Format.TabStops
                        };

                        range.Collapse(WdCollapseDirection.wdCollapseEnd);
                        letterheadCc = document.ContentControls.Add(WdContentControlType.wdContentControlRichText, range);
                        letterheadCc.Tag = CustomConstants.LETTERHEAD_TAG;

                        DocumentUtils.InsertXMLWithoutExtraParagraphOriginal(letterheadCc, primaryXml);

                        containingContentControls.AddRange(GetContentControls(letterheadCc));

                        paragraph.Format.LeftIndent = letterheadParagraphFormat.LeftIndent;
                        paragraph.Format.RightIndent = letterheadParagraphFormat.RightIndent;
                        paragraph.Format.SpaceBefore = letterheadParagraphFormat.SpaceBefore;
                        paragraph.Format.SpaceAfter = letterheadParagraphFormat.SpaceAfter;
                        paragraph.Format.Alignment = letterheadParagraphFormat.Aligment;
                        paragraph.Format.FirstLineIndent = letterheadParagraphFormat.FirstLineIndent;
                        paragraph.Format.TabStops = letterheadParagraphFormat.TabStops;
                    }
                }
                ThisAddIn.Instance.Application.ScreenUpdating = true;
                if (ThisAddIn.Instance.IsFunctionForLogging("ScreenUpdating"))
                    Logger.Log("Enabled Screen Updating (LetterheadToggler.cs > InsertLetterhead)");
                if (containingContentControls.Count > 0)
                {
                    TemplateLetterUserControl taskpane = new TemplateLetterUserControl();
                    PaneUtils.UsedContentControls = containingContentControls;
                    taskpane.OKHandler();
                    PaneUtils.UsedContentControls = null;
                }

                if (saveProperty)
                {
                    DocumentPropertyUtil.SaveShortProperty(CustomConstants.LETTERHEAD_ON, TRUE, true);
                }

                PaneUtils.PopulateLetterheadContentControls(ThisAddIn.Instance.Application.ActiveDocument);

                Utils.SetDocumentVariable(document, "PreOverlayPaperType", Utils.GetDocumentVariable(document, "PaperType") ?? "plain");
                Utils.SetDocumentVariable(document, "PaperType", "letterhead");
            }
            catch (NotImplementedException ex)
            {
                DocumentUtil.ShowCompatibilityModeError(new System.Threading.Tasks.Task(() => InsertLetterhead(saveProperty, letterheadDialog)));
            }
        }

        private List<Word.ContentControl> GetContentControls(Word.ContentControl letterheadCc)
        {
            List<Word.ContentControl> result = new List<Word.ContentControl>();

            result.AddRange(letterheadCc.Range.ContentControls.Cast<Word.ContentControl>());

            foreach (Shape shape in letterheadCc.Range.ShapeRange)
            {
                try
                {
                    result.AddRange(shape.TextFrame.TextRange.ContentControls.Cast<Word.ContentControl>());
                }
                catch (Exception ex)
                {
                }
            }

            return result;
        }

        private void RemoveLetterhead(bool saveProperty)
        {
            List<Word.ContentControl> controls = GetHeaderContentControls(WdHeaderFooterIndex.wdHeaderFooterPrimary);
            controls.AddRange(GetHeaderContentControls(WdHeaderFooterIndex.wdHeaderFooterFirstPage));

            if (controls.Count > 0)
            {
                foreach (Word.ContentControl contentControl in controls)
                {
                    if (contentControl.Tag == CustomConstants.LETTERHEAD_TAG)
                    {
                        contentControl.Delete(true);
                    }
                }
            }

            if (saveProperty)
            {
                DocumentPropertyUtil.SaveShortProperty(CustomConstants.LETTERHEAD_ON, FALSE, true);
            }

            Utils.SetDocumentVariable(document, "PaperType", Utils.GetDocumentVariable(document, "PreOverlayPaperType"));
        }

        public static void SetLetterheadProperty(bool? state)
        {
            if (state != null)
            {
                DocumentPropertyUtil.SaveShortProperty(CustomConstants.LETTERHEAD_ON, state.Value ? TRUE : FALSE, true);
            }
            else
            {
                DocumentPropertyUtil.RemoveShortProperty(CustomConstants.LETTERHEAD_ON);
            }
        }

        public static bool? GetLetterheadProperty()
        {
            string str = DocumentPropertyUtil.ReadProperty(CustomConstants.LETTERHEAD_ON);

            if (str == TRUE)
            {
                return true;
            }

            if (str == FALSE)
            {
                return false;
            }

            return null;
        }


        public List<Word.ContentControl> GetHeaderContentControls(WdHeaderFooterIndex headerIndex)
        {
            var controls = new List<Word.ContentControl>();

            foreach (Section section in document.Sections)
            {
                if (Utils.IsSectionContinous(section, document))
                {
                    continue;
                }

                HeaderFooter header = section.Headers[headerIndex];
                if (!header.LinkToPrevious)
                {
                    foreach (Word.ContentControl control in header.Range.ContentControls)
                    {
                        controls.Add(control);
                    }
                }
            }

            return controls;
        }

        private List<Word.ContentControl> GetHeaderContentControls(WdHeaderFooterIndex headerIndex, Word.Section section)
        {
            var controls = new List<Word.ContentControl>();

            HeaderFooter header = section.Headers[headerIndex];

            foreach (Word.ContentControl control in header.Range.ContentControls)
            {
                controls.Add(control);
            }

            return controls;
        }

        internal void AddSectionLetterhead(Word.Document document, int sectionIndex, int letterheadTemplateId)
        {
            var section = document.Sections[sectionIndex];

            if (!letterheadTemplates.TryGetValue(letterheadTemplateId, out string xml))
            {
                xml = AdminPanelWebApi.GetTemplateAsXml(letterheadTemplateId);
                letterheadTemplates[letterheadTemplateId] = xml;
            }
            section.Headers[WdHeaderFooterIndex.wdHeaderFooterFirstPage].LinkToPrevious = false;
            section.Headers[WdHeaderFooterIndex.wdHeaderFooterPrimary].LinkToPrevious = false;
            List<Word.ContentControl> controls = GetHeaderContentControls(WdHeaderFooterIndex.wdHeaderFooterPrimary, section);
            controls.AddRange(GetHeaderContentControls(WdHeaderFooterIndex.wdHeaderFooterFirstPage, section));

            if (controls.Count > 0)
            {
                foreach (Word.ContentControl contentControl in controls)
                {
                    if (contentControl.Tag == CustomConstants.LETTERHEAD_TAG)
                    {
                        contentControl.Delete(true);
                    }
                }
            }

            Range range;
            Word.ContentControl letterheadCc;
            //add to first page header and set different first page header property
            range = section.Headers[WdHeaderFooterIndex.wdHeaderFooterFirstPage].Range;

            var paragraph = range.Paragraphs.First;
            var letterheadParagraphFormat = new LetterheadParagraphFormat
            {
                LeftIndent = paragraph.Format.LeftIndent,
                RightIndent = paragraph.Format.RightIndent,
                SpaceBefore = paragraph.Format.SpaceBefore,
                SpaceAfter = paragraph.Format.SpaceAfter,
                Aligment = paragraph.Format.Alignment,
                FirstLineIndent = paragraph.Format.FirstLineIndent,
                TabStops = paragraph.Format.TabStops
            };

            range.Collapse(WdCollapseDirection.wdCollapseEnd);
            letterheadCc = document.ContentControls.Add(WdContentControlType.wdContentControlRichText, range);
            letterheadCc.Tag = CustomConstants.LETTERHEAD_TAG;

            DocumentUtils.InsertXMLWithoutExtraParagraph(letterheadCc, xml);

            paragraph.Format.LeftIndent = letterheadParagraphFormat.LeftIndent;
            paragraph.Format.RightIndent = letterheadParagraphFormat.RightIndent;
            paragraph.Format.SpaceBefore = letterheadParagraphFormat.SpaceBefore;
            paragraph.Format.SpaceAfter = letterheadParagraphFormat.SpaceAfter;
            paragraph.Format.Alignment = letterheadParagraphFormat.Aligment;
            paragraph.Format.FirstLineIndent = letterheadParagraphFormat.FirstLineIndent;
            paragraph.Format.TabStops = letterheadParagraphFormat.TabStops;

            range = section.Headers[WdHeaderFooterIndex.wdHeaderFooterPrimary].Range;

            paragraph = range.Paragraphs.First;
            letterheadParagraphFormat = new LetterheadParagraphFormat
            {
                LeftIndent = paragraph.Format.LeftIndent,
                RightIndent = paragraph.Format.RightIndent,
                SpaceBefore = paragraph.Format.SpaceBefore,
                SpaceAfter = paragraph.Format.SpaceAfter,
                Aligment = paragraph.Format.Alignment,
                FirstLineIndent = paragraph.Format.FirstLineIndent,
                TabStops = paragraph.Format.TabStops
            };

            range.Collapse(WdCollapseDirection.wdCollapseEnd);
            letterheadCc = document.ContentControls.Add(WdContentControlType.wdContentControlRichText, range);
            letterheadCc.Tag = CustomConstants.LETTERHEAD_TAG;

            DocumentUtils.InsertXMLWithoutExtraParagraph(letterheadCc, xml);

            paragraph.Format.LeftIndent = letterheadParagraphFormat.LeftIndent;
            paragraph.Format.RightIndent = letterheadParagraphFormat.RightIndent;
            paragraph.Format.SpaceBefore = letterheadParagraphFormat.SpaceBefore;
            paragraph.Format.SpaceAfter = letterheadParagraphFormat.SpaceAfter;
            paragraph.Format.Alignment = letterheadParagraphFormat.Aligment;
            paragraph.Format.FirstLineIndent = letterheadParagraphFormat.FirstLineIndent;
            paragraph.Format.TabStops = letterheadParagraphFormat.TabStops;

        }
    }
}
