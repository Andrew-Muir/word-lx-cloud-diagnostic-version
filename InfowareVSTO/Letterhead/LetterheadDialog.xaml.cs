﻿using DocumentFormat.OpenXml.Bibliography;
using InfowareVSTO.Common.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace InfowareVSTO.Letterhead
{
    /// <summary>
    /// Interaction logic for LetterheadDialog.xaml
    /// </summary>
    public partial class LetterheadDialog : InfowareWindow
    {
        public LetterheadDialog()
        {
            InitializeComponent();
			PrimaryCheckbox.IsChecked = SettingsManager.GetSettingAsBool("Has2ndPageLetterHead", "MultiPrint", false);
		}





        public int FirstPageId { get; set; }

        public int PrimaryId { get; set; }

        public bool AddToFirstPage { get; set; } = false;
        public bool AddToPrimary { get; set; } = false;

        public bool ResultOk { get; set; } = false;

        public void DisableCheckBoxes(bool firstPage, bool primary)
        {
            if (firstPage)
            {
                this.FirstPageCheckbox.IsChecked = true;
                this.FirstPageCheckbox.IsEnabled = false;
            }
            else
            {
                this.FirstPageCheckbox.IsChecked = false;
                this.FirstPageCheckbox.Visibility = Visibility.Collapsed;
                this.FirstPageGrid.Visibility = Visibility.Collapsed;
            }

            if (primary)
            {
                this.PrimaryCheckbox.IsChecked = true;
                this.PrimaryCheckbox.IsEnabled = false;
            }
            else
            {
                this.PrimaryCheckbox.IsChecked = false;
                this.PrimaryCheckbox.Visibility = Visibility.Collapsed;
                this.PrimaryGrid.Visibility = Visibility.Collapsed;
            }
        }

        public void PopulateTemplates(List<TemplateListModel> templateList)
        {
            if(templateList != null && templateList.Count > 0)
            {
                foreach(var template in templateList)
                {
                    var item = new ComboboxItem() { Text = template.DisplayName, Value = template.Id };

                    this.FirstPageTemplate.Items.Add(item);
                    this.PrimaryTemplate.Items.Add(item);
                    this.PrimaryHidden.Items.Add(item);
                    this.FirstPageHidden.Items.Add(item);
                }

                this.FirstPageTemplate.SelectedIndex = 0;
                this.PrimaryTemplate.SelectedIndex = 0;
            }
        }

        public void OnOkButtonClick(object sender, RoutedEventArgs e)
        {
            var selectedPrimaryId = (this.PrimaryTemplate.SelectedItem as ComboboxItem).Value;
            if(selectedPrimaryId != null)
            {
                this.PrimaryId = Convert.ToInt32(selectedPrimaryId.ToString());
            }

            var selectedFirstPageId = (this.FirstPageTemplate.SelectedItem as ComboboxItem).Value;
            if (selectedFirstPageId != null)
            {
                this.FirstPageId = Convert.ToInt32(selectedFirstPageId.ToString());
            }

            this.AddToFirstPage = this.FirstPageCheckbox.IsChecked == true;
            this.AddToPrimary = this.PrimaryCheckbox.IsChecked == true;

            this.ResultOk = true;
            this.Close();
        }

        private void OnCancelButtonClick(object sender, RoutedEventArgs e)
        {
            this.ResultOk = false;
            this.Close();
        }
    }
}
