﻿using InfowareVSTO.Common;
using InfowareVSTO.Common.Language;
using InfowareVSTO.Windows;
using InfowareVSTO.Common.Models;
using InfowareVSTO.VSTOAuthor;
using InfowareVSTO.Windows;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing.Printing;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Word = Microsoft.Office.Interop.Word;

namespace InfowareVSTO
{
    /// <summary>
    /// Interaction logic for MultiPrint_WPF.xaml
    /// </summary>
    public partial class MultiPrintSettings_WPF : InfowareWindow
    {
        public IMultiPrintSettings MPSettings { get; set; }
        private Word.Document document;
        public static string SelectedPrinterName;

        public MultiPrintSettings_WPF(IMultiPrintSettings multiPrintSettings, Word.Document document)
        {
            MPSettings = multiPrintSettings;
            this.document = document;
            InitializeComponent();
			Init();
		}

        private PrintDocument Pdocument = new PrintDocument();
        private List<PaperSource> ListaPS = new List<PaperSource>();
        private PaperSource General_PaperSource_AddresLabel = new PaperSource();
        private PaperSource General_PaperSource_A4 = new PaperSource();
        private PaperSource General_PaperSource_Envelopes = new PaperSource();
        private PaperSource General_PaperSource_FileCopy = new PaperSource();
        private PaperSource General_PaperSource_Letterhead1 = new PaperSource();
        private PaperSource General_PaperSource_Letterhead2 = new PaperSource();
        private PaperSource General_PaperSource_PlainPapper = new PaperSource();
        private PaperSource General_PaperSource_Legal = new PaperSource();

        public List<PaperSource> getBins()
        {
            List<PaperSource> Lista = new List<PaperSource>();
            Lista.Add(General_PaperSource_AddresLabel);
            Lista.Add(General_PaperSource_A4);
            Lista.Add(General_PaperSource_Envelopes);
            Lista.Add(General_PaperSource_FileCopy);
            Lista.Add(General_PaperSource_Letterhead1);
            Lista.Add(General_PaperSource_Letterhead2);
            Lista.Add(General_PaperSource_PlainPapper);
            Lista.Add(General_PaperSource_Legal);
            return Lista;
        }

        // GENERAL //
        public void General_OK_Click(object sender, RoutedEventArgs e)
        {
            for (int i = 0; i < ListaPS.Count; i++)
            {
                if (General_Combo_AddresLabel.Text == ListaPS[i].SourceName)
                {
                    General_PaperSource_AddresLabel = ListaPS[i];
                }
                if (General_Combo_A4.Text == ListaPS[i].SourceName)
                {
                    General_PaperSource_A4 = ListaPS[i];
                }
                if (General_Combo_Envelopes.Text == ListaPS[i].SourceName)
                {
                    General_PaperSource_Envelopes = ListaPS[i];
                }
                if (General_Combo_Legal.Text == ListaPS[i].SourceName)
                {
                    General_PaperSource_Legal = ListaPS[i];
                }
                if (General_Combo_FileCopy.Text == ListaPS[i].SourceName)
                {
                    General_PaperSource_FileCopy = ListaPS[i];
                }
                if (General_Combo_Letterhead1.Text == ListaPS[i].SourceName)
                {
                    General_PaperSource_Letterhead1 = ListaPS[i];
                }
                if (General_Combo_Letterhead2.Text == ListaPS[i].SourceName)
                {
                    General_PaperSource_Letterhead2 = ListaPS[i];
                }
                if (General_Combo_PlainPapper.Text == ListaPS[i].SourceName)
                {
                    General_PaperSource_PlainPapper = ListaPS[i];
                }
            }

            if (General_Combo_A4.Text == "")
            {
                General_PaperSource_A4 = null;
            }
            if (General_Combo_AddresLabel.Text == "")
            {
                General_PaperSource_AddresLabel = null;
            }
            if (General_Combo_Envelopes.Text == "")
            {
                General_PaperSource_Envelopes = null;
            }
            if (General_Combo_FileCopy.Text == "")
            {
                General_PaperSource_FileCopy = null;
            }
            if (General_Combo_Letterhead1.Text == "")
            {
                General_PaperSource_Letterhead1 = null;
            }
            if (General_Combo_Letterhead2.Text == "")
            {
                General_PaperSource_Letterhead2 = null;
            }
            if (General_Combo_PlainPapper.Text == "")
            {
                General_PaperSource_PlainPapper = null;
            }
            if (General_Combo_Legal.Text == "")
            {
                General_PaperSource_PlainPapper = null;
            }

            SaveCurrentPrinterSetup();
            if (SpecialPaperListbox.SelectedItem is string)
            {
                SaveSpecialPaper(SpecialPaperListbox.SelectedItem as string);
            }

            //save settings to webapi
            try
            {
                string multiPrintSettingsString = Newtonsoft.Json.JsonConvert.SerializeObject(MPSettings.RestoreToDefaultWhereSame());
                ThisAddIn.Instance.MPSettings = MPSettings;
                AdminPanelWebApi.UpdateCustomSetting(Utils.GetProcessorId(), "MultiPrintSettings", multiPrintSettingsString, ThisAddIn.DoNotOverWriteMPSettings);
            }
            catch
            {
                new InfowareInformationWindow(LanguageManager.GetTranslation(LanguageConstants.CouldNotSaveSettingsToWebApi, "Could not save settings to WebApi")).ShowDialog();
            }

            this.Close();
        }

		public void Init()
		{
            if (MPSettings is ComposedMultiPrintSettings)
            {
                this.RestoreToDefault.Visibility = Visibility.Visible;
            }

            Special_Combo_A4_Printer.Items.Add("Default Printer");
            Special_Combo_AddrLabel_Printer.Items.Add("Default Printer");
            Special_Combo_Envelope_Printer.Items.Add("Default Printer");
            Special_Combo_PDF_Printer.Items.Add("Create PDF document");
            Special_Combo_FileCopy_Printer.Items.Add("Default Printer");
            Special_Combo_Legal_Printer.Items.Add("Default Printer");
            Special_Combo_Letterhead_Printer.Items.Add("Default Printer");

            foreach (string printer in System.Drawing.Printing.PrinterSettings.InstalledPrinters)
            {
                Printer_Combo.Items.Add(printer);
                Special_Combo_A4_Printer.Items.Add(printer);
                Special_Combo_AddrLabel_Printer.Items.Add(printer);
                Special_Combo_Envelope_Printer.Items.Add(printer);
                Special_Combo_PDF_Printer.Items.Add(printer);
                Special_Combo_FileCopy_Printer.Items.Add(printer);
                Special_Combo_Legal_Printer.Items.Add(printer);
                Special_Combo_Letterhead_Printer.Items.Add(printer);
                SpecialPaperPrinter.Items.Add(printer);

            }

            Special_Combo_A4_Printer.Items.Add(LanguageManager.GetTranslation(LanguageConstants.DefaultPrinter, "Default Printer"));
            Special_Combo_AddrLabel_Printer.Items.Add(LanguageManager.GetTranslation(LanguageConstants.DefaultPrinter, "Default Printer"));
            Special_Combo_Envelope_Printer.Items.Add(LanguageManager.GetTranslation(LanguageConstants.DefaultPrinter, "Default Printer"));
            Special_Combo_PDF_Printer.Items.Add(LanguageManager.GetTranslation(LanguageConstants.CreatePDFDocument, "Create PDF document"));
            Special_Combo_PDF_Printer.Items.Add("");
            Special_Combo_FileCopy_Printer.Items.Add(LanguageManager.GetTranslation(LanguageConstants.DefaultPrinter, "Default Printer"));
            Special_Combo_Legal_Printer.Items.Add(LanguageManager.GetTranslation(LanguageConstants.DefaultPrinter, "Default Printer"));
            Special_Combo_Letterhead_Printer.Items.Add(LanguageManager.GetTranslation(LanguageConstants.DefaultPrinter, "Default Printer"));

            if (SelectedPrinterName != null)
            {
                Pdocument.PrinterSettings.DefaultPageSettings.PrinterSettings.PrinterName = SelectedPrinterName;
            }

            Printer_Combo.SelectedValue = Pdocument.PrinterSettings.DefaultPageSettings.PrinterSettings.PrinterName;

            //this is commented because these operations are done in the printer combo selection changed.
            //if (MPSettings.SpecialPrinterSettings == null || MPSettings.SpecialPrinterSettings.A4Printer == null)
            //{
            //	Special_Combo_A4_Printer.SelectedValue = Pdocument.PrinterSettings.DefaultPageSettings.PrinterSettings.PrinterName;
            //	Special_Combo_AddrLabel_Printer.SelectedValue = Pdocument.PrinterSettings.DefaultPageSettings.PrinterSettings.PrinterName;
            //	Special_Combo_Envelope_Printer.SelectedValue = Pdocument.PrinterSettings.DefaultPageSettings.PrinterSettings.PrinterName;
            //	Special_Combo_PDF_Printer.SelectedValue = Pdocument.PrinterSettings.DefaultPageSettings.PrinterSettings.PrinterName;
            //	Special_Combo_FileCopy_Printer.SelectedValue = Pdocument.PrinterSettings.DefaultPageSettings.PrinterSettings.PrinterName;
            //	Special_Combo_Legal_Printer.SelectedValue = Pdocument.PrinterSettings.DefaultPageSettings.PrinterSettings.PrinterName;
            //	Special_Combo_Letterhead_Printer.SelectedValue = Pdocument.PrinterSettings.DefaultPageSettings.PrinterSettings.PrinterName;
            //}
            //else
            //{

            Special_Combo_A4_Printer.SelectedValue = MPSettings.SpecialPrinterSettings?.A4Printer;
            Special_Combo_AddrLabel_Printer.SelectedValue = MPSettings.SpecialPrinterSettings?.AddressLabelPrinter;
            Special_Combo_Envelope_Printer.SelectedValue = MPSettings.SpecialPrinterSettings?.EnvelopePrinter;
            Special_Combo_PDF_Printer.SelectedValue = MPSettings.SpecialPrinterSettings?.PdfPrinter;
            Special_Combo_FileCopy_Printer.SelectedValue = MPSettings.SpecialPrinterSettings?.FileCopyPrinter;
            Special_Combo_Legal_Printer.SelectedValue = MPSettings.SpecialPrinterSettings?.LegalPrinter;
            Special_Combo_Letterhead_Printer.SelectedValue = MPSettings.SpecialPrinterSettings?.LetterheadPrinter;
            //}
            var envTemplateDocumentVariable = Utils.GetDocumentVariable(document, "EnvTemplate");
            var altLabTemplateDocumentVariable = Utils.GetDocumentVariable(document, "AltLabTemplate");
            var neverDuplexDocumentVariable = Utils.GetDocumentVariable(document, "NeverDuplex");
            var paperTypeDocumentVariable = Utils.GetDocumentVariable(document, "PaperType");
            var paperTypePerSectionDocumentVariable = Utils.GetDocumentVariable(document, "PaperTypePerSection");
            var preOverlayPaperTypeDocumentVariable = Utils.GetDocumentVariable(document, "PreOverlayPaperType");
            var infoRouteToSpecialPtrDocumentVariable = Utils.GetDocumentVariable(document, "InfoRouteToSpecialPtr");
            EnvTemplate_Box.Text = envTemplateDocumentVariable ?? string.Empty;
            LabTemplate_Box.Text = altLabTemplateDocumentVariable ?? string.Empty;
            NeverDuplex_Box.Text = neverDuplexDocumentVariable ?? string.Empty;
            PaperType_Box.Text = paperTypeDocumentVariable ?? "plain";
            PaperTypePerSection_Box.Text = paperTypePerSectionDocumentVariable ?? string.Empty;
            PreOverlayPaper_Box.Text = preOverlayPaperTypeDocumentVariable ?? string.Empty;
            InfoRouteToSpecialPtr_Box.Text = infoRouteToSpecialPtrDocumentVariable ?? string.Empty;

            var documentVariables = new List<string> { "EnvTemplate", "AltLabTemplate", "NeverDuplex", "PaperType", "PaperTypePerSection", "PreOverlayPaperType", "InfoRouteToSpecialPtr" };
            var restOfVariables = string.Empty;
			foreach (Word.Variable documentVariable in document.Variables)
			{
				if (!documentVariables.Contains(documentVariable.Name))
                {
                    restOfVariables += documentVariable.Name + ": " + documentVariable.Value + "; ";
                }					
			}
			OtherVariables_Box.Text = restOfVariables;


            var currentAuthorSettings = ManageUsers.CurrentAuthorSettings;
            var labelTemplateList = AdminPanelWebApi.GetTemplateList(TemplateType.Labels, currentAuthorSettings.CompanyId, currentAuthorSettings.LanguageId, currentAuthorSettings.LocationId);
            if (labelTemplateList != null && labelTemplateList.Any())
            {
                Utils.PopulateComboboxWithTemplateOptions(GeneralCombo_AddressLabelTemplateName, labelTemplateList);

                var pgSetting = MPSettings.PrinterBinSettings.Where(x => x.Name == Pdocument.PrinterSettings.DefaultPageSettings.PrinterSettings.PrinterName).FirstOrDefault();
                GeneralCombo_AddressLabelTemplateName.SelectedValue = Utils.GetAddressLabelComboBoxSelectedValue(pgSetting, labelTemplateList, SettingsManager.GetSetting("AddressLabelPrinterTemplate","MultiPrint"));
            }
            string specialPapersTypes = SettingsManager.GetSetting("SpecialPaperTypes", "MultiPrint");
            
            if (!string.IsNullOrWhiteSpace(specialPapersTypes))
            {
                this.SpecialPaperListbox.IsEnabled = true;

                string[] specialPapersTypesArr = specialPapersTypes.Split(',').Select(x => x.Trim()).ToArray();
                this.SpecialPaperListbox.ItemsSource = specialPapersTypesArr;
            }
        }

		private void SaveCurrentPrinterSetup()
		{
            MPSettings.InEditMode = true;
            //General
            var pgSetting = MPSettings.PrinterBinSettings.Where(x => x.Name == Pdocument.PrinterSettings.DefaultPageSettings.PrinterSettings.PrinterName).FirstOrDefault();
			if (pgSetting == null)
			{
				pgSetting = new PrinterGeneralSetting();
				pgSetting.Name = Pdocument.PrinterSettings.DefaultPageSettings.PrinterSettings.PrinterName;
				MPSettings.PrinterBinSettings.Add(pgSetting);
			}
			pgSetting.AddressLabelBin = General_Combo_AddresLabel.SelectedValue?.ToString();
			pgSetting.A4Bin = General_Combo_A4.SelectedValue?.ToString();
			pgSetting.A4IsDuplex = General_CheckBox_A4.IsChecked ?? false;
			pgSetting.EnvelopesBin = General_Combo_Envelopes.SelectedValue?.ToString();
			pgSetting.FileCopyBin = General_Combo_FileCopy.SelectedValue?.ToString();
			pgSetting.FileCopyIsDuplex = General_CheckBox_FileCopy.IsChecked ?? false;
			pgSetting.LegalBin = General_Combo_Legal.SelectedValue?.ToString();
			pgSetting.LegalIsDuplex = General_CheckBox_Legal.IsChecked ?? false;
			pgSetting.Letterhead1Bin = General_Combo_Letterhead1.SelectedValue?.ToString();
			pgSetting.Letterhead2Bin = General_Combo_Letterhead2.SelectedValue?.ToString();
			pgSetting.Letterhead2IsDuplex = General_CheckBox_Letterhead2.IsChecked ?? false;
			pgSetting.PlainPaperBin = General_Combo_PlainPapper.SelectedValue?.ToString();
			pgSetting.PlainPaperIsDuplex = General_CheckBox_PlainPapper.IsChecked ?? false;
			pgSetting.LetterHeadOnPlain = General_CheckBox_LetterheadOnPlain.IsChecked ?? false;
            pgSetting.AddressLabelTemplateId = (int)(GeneralCombo_AddressLabelTemplateName.SelectedValue ?? -1);
			pgSetting.LetterHeadPrePrinted = General_CheckBox_LetterHeadPrePrinted.IsChecked ?? false;
			pgSetting.ForcePromptsBlackAndWhite = General_CheckBox_ForcePrompts.IsChecked ?? false;

            //Special
            if (Special_Combo_A4_Bin.SelectedValue != null)
                MPSettings.SpecialPrinterSettings.A4Bin = Special_Combo_A4_Bin.SelectedValue.ToString();
            if (Special_Combo_A4_Printer.SelectedValue != null)
                MPSettings.SpecialPrinterSettings.A4Printer = Special_Combo_A4_Printer.SelectedValue.ToString();
            if (Special_Combo_AddrLabel_Bin.SelectedValue != null)
                MPSettings.SpecialPrinterSettings.AddressLabelBin = Special_Combo_AddrLabel_Bin.SelectedValue.ToString();
            if (Special_Combo_AddrLabel_Printer.SelectedValue != null)
                MPSettings.SpecialPrinterSettings.AddressLabelPrinter = Special_Combo_AddrLabel_Printer.SelectedValue.ToString();
            if (Special_Combo_Envelope_Bin.SelectedValue != null)
                MPSettings.SpecialPrinterSettings.EnvelopeBin = Special_Combo_Envelope_Bin.SelectedValue.ToString();
            if (Special_Combo_Envelope_Printer.SelectedValue != null)
                MPSettings.SpecialPrinterSettings.EnvelopePrinter = Special_Combo_Envelope_Printer.SelectedValue.ToString();
            if (Special_Combo_FileCopy_Bin.SelectedValue != null)
                MPSettings.SpecialPrinterSettings.FileCopyBin = Special_Combo_FileCopy_Bin.SelectedValue.ToString();
            if (Special_Combo_FileCopy_Printer.SelectedValue != null)
                MPSettings.SpecialPrinterSettings.FileCopyPrinter = Special_Combo_FileCopy_Printer.SelectedValue.ToString();
            if (Special_Combo_Legal_Bin.SelectedValue != null)
                MPSettings.SpecialPrinterSettings.LegalBin = Special_Combo_Legal_Bin.SelectedValue?.ToString();
            if (Special_Combo_Legal_Printer.SelectedValue != null)
                MPSettings.SpecialPrinterSettings.LegalPrinter = Special_Combo_Legal_Printer.SelectedValue.ToString();
            if (Special_Combo_LetterHead_1st_Bin.SelectedValue != null)
                MPSettings.SpecialPrinterSettings.Letterhead1Bin = Special_Combo_LetterHead_1st_Bin.SelectedValue.ToString();
            if (Special_Combo_LetterHead_2nd_Bin.SelectedValue != null)
                MPSettings.SpecialPrinterSettings.Letterhead2Bin = Special_Combo_LetterHead_2nd_Bin.SelectedValue.ToString();
            if (Special_Combo_Letterhead_Printer.SelectedValue != null)
                MPSettings.SpecialPrinterSettings.LetterheadPrinter = Special_Combo_Letterhead_Printer.SelectedValue.ToString();
            if (Special_Combo_PDF_Bin.SelectedValue != null)
                MPSettings.SpecialPrinterSettings.PdfBin = Special_Combo_PDF_Bin.SelectedValue.ToString();
            if (Special_Combo_PDF_Printer.SelectedValue != null)
                MPSettings.SpecialPrinterSettings.PdfPrinter = Special_Combo_PDF_Printer.SelectedValue.ToString();

            SelectedPrinterName = Printer_Combo.SelectedValue.ToString();
            MPSettings.InEditMode = false;
        }

		private void LoadCurrentPrinterSetup()
		{
			//General
			var pgSetting = MPSettings.PrinterBinSettings.Where(x => x.Name == Pdocument.PrinterSettings.DefaultPageSettings.PrinterSettings.PrinterName).FirstOrDefault();
			General_Combo_AddresLabel.SelectedIndex =
				General_Combo_A4.SelectedIndex =
				General_Combo_Envelopes.SelectedIndex =
				General_Combo_FileCopy.SelectedIndex =
				General_Combo_Legal.SelectedIndex =
				General_Combo_Letterhead1.SelectedIndex =
				General_Combo_Letterhead2.SelectedIndex =
				General_Combo_PlainPapper.SelectedIndex = 0;


            var labelTemplateList = this.GeneralCombo_AddressLabelTemplateName.Items.Cast<ComboboxItem>().Select(x => new TemplateListModel()
            {
                Name = x.Text,
                Id = (int) x.Value
            }).ToList();

            GeneralCombo_AddressLabelTemplateName.SelectedValue = Utils.GetAddressLabelComboBoxSelectedValue(pgSetting, labelTemplateList, SettingsManager.GetSetting("AddressLabelPrinterTemplate", "MultiPrint"));
            var letterHeadIsPrePrintedOnThisPrinter = SettingsManager.GetSettingAsBool("LetterheadIsPrePrintedOnThisPrinter");
            bool forcePromtsBlack = SettingsManager.GetSettingAsBool("ForcePromptsToBlackOnThisPrinter", "MultiPrint", true);
            if (pgSetting != null)
			{
				if (!string.IsNullOrEmpty(pgSetting.AddressLabelBin))
					General_Combo_AddresLabel.SelectedValue = pgSetting.AddressLabelBin;
				if (!string.IsNullOrEmpty(pgSetting.A4Bin))
					General_Combo_A4.SelectedValue = pgSetting.A4Bin;
				if (!string.IsNullOrEmpty(pgSetting.EnvelopesBin))
					General_Combo_Envelopes.SelectedValue = pgSetting.EnvelopesBin;
				if (!string.IsNullOrEmpty(pgSetting.FileCopyBin))
					General_Combo_FileCopy.SelectedValue = pgSetting.FileCopyBin;
				if (!string.IsNullOrEmpty(pgSetting.LegalBin))
					General_Combo_Legal.SelectedValue = pgSetting.LegalBin;
				if (!string.IsNullOrEmpty(pgSetting.Letterhead1Bin))
					General_Combo_Letterhead1.SelectedValue = pgSetting.Letterhead1Bin;
				if (!string.IsNullOrEmpty(pgSetting.Letterhead2Bin))
					General_Combo_Letterhead2.SelectedValue = pgSetting.Letterhead2Bin;
				if (!string.IsNullOrEmpty(pgSetting.PlainPaperBin))
					General_Combo_PlainPapper.SelectedValue = pgSetting.PlainPaperBin;
				if (!string.IsNullOrEmpty(pgSetting.AddressLabelBin))
					General_Combo_AddresLabel.SelectedValue = pgSetting.AddressLabelBin;
                if (pgSetting?.AddressLabelTemplateId != null && pgSetting.AddressLabelTemplateId != 0)
                    GeneralCombo_AddressLabelTemplateName.SelectedValue = pgSetting.AddressLabelTemplateId;
                
				General_CheckBox_A4.IsChecked = pgSetting.A4IsDuplex;
				General_CheckBox_FileCopy.IsChecked = pgSetting.FileCopyIsDuplex;
				General_CheckBox_ForcePrompts.IsChecked = pgSetting.ForcePromptsBlackAndWhite;
				General_CheckBox_Legal.IsChecked = pgSetting.LegalIsDuplex;
				General_CheckBox_LetterheadOnPlain.IsChecked = pgSetting.LetterHeadOnPlain;
				General_CheckBox_Letterhead2.IsChecked = pgSetting.Letterhead2IsDuplex;
				General_CheckBox_LetterHeadPrePrinted.IsChecked = pgSetting.LetterHeadPrePrinted;

				if (!pgSetting.LetterHeadPrePrinted.HasValue)
				{
					General_CheckBox_LetterHeadPrePrinted.IsChecked = letterHeadIsPrePrintedOnThisPrinter;
				}
				else
				{
					General_CheckBox_LetterHeadPrePrinted.IsChecked = pgSetting.LetterHeadPrePrinted;
				}
				General_CheckBox_PlainPapper.IsChecked = pgSetting.PlainPaperIsDuplex;
			}
			else
			{
				General_CheckBox_LetterHeadPrePrinted.IsChecked = letterHeadIsPrePrintedOnThisPrinter;
                General_CheckBox_ForcePrompts.IsChecked = forcePromtsBlack;
            }

            ////Special Printers
            //if (MPSettings.SpecialPrinterSettings == null || MPSettings.SpecialPrinterSettings.A4Printer == null)
            //{
            //	Special_Combo_A4_Printer.SelectedValue = Pdocument.PrinterSettings.DefaultPageSettings.PrinterSettings.PrinterName;
            //	Special_Combo_AddrLabel_Printer.SelectedValue = Pdocument.PrinterSettings.DefaultPageSettings.PrinterSettings.PrinterName;
            //	Special_Combo_Envelope_Printer.SelectedValue = Pdocument.PrinterSettings.DefaultPageSettings.PrinterSettings.PrinterName;
            //	Special_Combo_PDF_Printer.SelectedValue = Pdocument.PrinterSettings.DefaultPageSettings.PrinterSettings.PrinterName;
            //	Special_Combo_FileCopy_Printer.SelectedValue = Pdocument.PrinterSettings.DefaultPageSettings.PrinterSettings.PrinterName;
            //	Special_Combo_Legal_Printer.SelectedValue = Pdocument.PrinterSettings.DefaultPageSettings.PrinterSettings.PrinterName;
            //	Special_Combo_Letterhead_Printer.SelectedValue = Pdocument.PrinterSettings.DefaultPageSettings.PrinterSettings.PrinterName;
            //}
            //else
            //{
            //	Special_Combo_A4_Printer.SelectedValue = MPSettings.SpecialPrinterSettings.A4Printer;
            //	Special_Combo_AddrLabel_Printer.SelectedValue = MPSettings.SpecialPrinterSettings.AddressLabelPrinter;
            //	Special_Combo_Envelope_Printer.SelectedValue = MPSettings.SpecialPrinterSettings.EnvelopePrinter;
            //	Special_Combo_PDF_Printer.SelectedValue = MPSettings.SpecialPrinterSettings.PdfPrinter;
            //	Special_Combo_FileCopy_Printer.SelectedValue = MPSettings.SpecialPrinterSettings.FileCopyPrinter;
            //	Special_Combo_Legal_Printer.SelectedValue = MPSettings.SpecialPrinterSettings.LegalPrinter;
            //	Special_Combo_Letterhead_Printer.SelectedValue = MPSettings.SpecialPrinterSettings.LetterheadPrinter;
            //}
        }

        private void Printer_Combo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //save previous changes first
            if (General_Combo_AddresLabel.Items.Count > 0)//if it's not the first time we load them
                SaveCurrentPrinterSetup();

            Pdocument.PrinterSettings.PrinterName = Printer_Combo.SelectedItem.ToString();
            General_Combo_AddresLabel.Items.Clear();
            General_Combo_A4.Items.Clear();
            General_Combo_Envelopes.Items.Clear();
            General_Combo_FileCopy.Items.Clear();
            General_Combo_Legal.Items.Clear();
            General_Combo_Letterhead1.Items.Clear();
            General_Combo_Letterhead2.Items.Clear();
            General_Combo_PlainPapper.Items.Clear();

            List<string> paperSourceNames = new List<string>();
            ////paperSourceNames.Add(CustomConstants.DefaultPrinterBin);
            var paperSources = Pdocument.PrinterSettings.PaperSources;

            foreach (PaperSource paperSource in paperSources)
            {
                paperSourceNames.Add(paperSource.SourceName);
                ListaPS.Add(paperSource);
            }

            foreach (string paperSourceName in paperSourceNames)
            {
                General_Combo_AddresLabel.Items.Add(paperSourceName);
                General_Combo_A4.Items.Add(paperSourceName);
                General_Combo_Envelopes.Items.Add(paperSourceName);
                General_Combo_FileCopy.Items.Add(paperSourceName);
                General_Combo_Legal.Items.Add(paperSourceName);
                General_Combo_Letterhead1.Items.Add(paperSourceName);
                General_Combo_Letterhead2.Items.Add(paperSourceName);
                General_Combo_PlainPapper.Items.Add(paperSourceName);
            }

            //load default or saved values
            LoadCurrentPrinterSetup();
        }

        private void General_Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Special_Combo_A4_Printer_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Special_Combo_Custom_Printer_SelectionChanged(Special_Combo_A4_Bin, Special_Combo_A4_Printer.SelectedItem);
        }

        private void Special_Combo_AddrLabel_Printer_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Special_Combo_Custom_Printer_SelectionChanged(Special_Combo_AddrLabel_Bin, Special_Combo_AddrLabel_Printer.SelectedItem);
        }

        private void Special_Combo_Envelope_Printer_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Special_Combo_Custom_Printer_SelectionChanged(Special_Combo_Envelope_Bin, Special_Combo_Envelope_Printer.SelectedItem);
        }

        private void Special_Combo_FileCopy_Printer_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Special_Combo_Custom_Printer_SelectionChanged(Special_Combo_FileCopy_Bin, Special_Combo_FileCopy_Printer.SelectedItem);
        }

        private void Special_Combo_Legal_Printer_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Special_Combo_Custom_Printer_SelectionChanged(Special_Combo_Legal_Bin, Special_Combo_Legal_Printer.SelectedItem);
        }

        private void Special_Combo_PDF_Printer_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Special_Combo_Custom_Printer_SelectionChanged(Special_Combo_PDF_Bin, Special_Combo_PDF_Printer.SelectedItem);
        }

        private void Special_Combo_Letterhead_Printer_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Special_Combo_Custom_Printer_SelectionChanged(Special_Combo_LetterHead_1st_Bin, Special_Combo_Letterhead_Printer.SelectedItem);
            Special_Combo_Custom_Printer_SelectionChanged(Special_Combo_LetterHead_2nd_Bin, Special_Combo_Letterhead_Printer.SelectedItem);
        }
        private void SpecialPaperPrinter_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Special_Combo_Custom_Printer_SelectionChanged(SpecialPaperBin1, SpecialPaperPrinter.SelectedItem);
            Special_Combo_Custom_Printer_SelectionChanged(SpecialPaperBin2, SpecialPaperPrinter.SelectedItem);
            Special_Combo_Custom_Printer_SelectionChanged_Color(SpecialPaperColor, SpecialPaperPrinter.SelectedItem);
        }

        private void Special_Combo_Custom_Printer_SelectionChanged(ComboBox comboBoxBin, object printerName)
        {
            if (printerName == null)
                return;

            comboBoxBin.Items.Clear();

            List<string> paperSourceNames = new List<string>();
            paperSourceNames.Add(CustomConstants.DefaultPrinterBin);
            PrinterSettings ps = new PrinterSettings();
            ps.PrinterName = printerName.ToString();
            if (ps.IsValid)
            {
                var pss = ps.PaperSources;
                for (int i = 0; i < pss.Count; i++)
                {
                    paperSourceNames.Add(pss[i].SourceName);
                }
            }

            foreach (string paperSourceName in paperSourceNames)
            {
                comboBoxBin.Items.Add(paperSourceName);
            }

			//try to load selected value from saved setting first
			if (MPSettings.SpecialPrinterSettings != null)
			{
				switch (comboBoxBin.Name)
				{
					case "Special_Combo_A4_Bin":
						comboBoxBin.SelectedValue = MPSettings.SpecialPrinterSettings.A4Bin;
						break;
					case "Special_Combo_AddrLabel_Bin":
						comboBoxBin.SelectedValue = MPSettings.SpecialPrinterSettings.AddressLabelBin;
						break;
					case "Special_Combo_Envelope_Bin":
						comboBoxBin.SelectedValue = MPSettings.SpecialPrinterSettings.EnvelopeBin;
						break;
					case "Special_Combo_FileCopy_Bin":
						comboBoxBin.SelectedValue = MPSettings.SpecialPrinterSettings.FileCopyBin;
						break;
					case "Special_Combo_Legal_Bin":
						comboBoxBin.SelectedValue = MPSettings.SpecialPrinterSettings.LegalBin;
						break;
					case "Special_Combo_PDF_Bin":
						comboBoxBin.SelectedValue = MPSettings.SpecialPrinterSettings.PdfBin;
						break;
					case "Special_Combo_LetterHead_1st_Bin":
						comboBoxBin.SelectedValue = MPSettings.SpecialPrinterSettings.Letterhead1Bin;
						break;
					case "Special_Combo_LetterHead_2nd_Bin":
						comboBoxBin.SelectedValue = MPSettings.SpecialPrinterSettings.Letterhead2Bin;
						break;
				}
			}
			else
				comboBoxBin.SelectedValue = ps.DefaultPageSettings.PaperSource.SourceName;

            if (comboBoxBin.SelectedValue == null)
                comboBoxBin.SelectedIndex = 0;
        }

        private void Special_Combo_Custom_Printer_SelectionChanged_Color(CheckBox color, object printerName)
        {
            if (printerName == null)
                return;

            color.IsChecked = true;

            PrinterSettings ps = new PrinterSettings();
            ps.PrinterName = printerName.ToString();
            if (ps.IsValid)
            {
                color.IsChecked = ps.SupportsColor;
            }
        }

        private void SpecialPaperListbox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.RemovedItems.Count > 0)
            {
                string oldSpecialPaperType = e.RemovedItems.Cast<string>().First();

                SaveSpecialPaper(oldSpecialPaperType);
            }

            string specialPaperType = SpecialPaperListbox.SelectedItem as string;

            if (MPSettings.SpecialPaperSettingsDict != null)
            {
                if (MPSettings.SpecialPaperSettingsDict.TryGetValue(specialPaperType, out var value))
                {
                    this.SpecialPaperPrinter.SelectedItem = value.Printer;
                    this.SpecialPaperBin1.SelectedItem = value.Bin1;
                    this.SpecialPaperBin2.SelectedItem = value.Bin2;
                    this.SpecialPaperDuplex.SelectedIndex = value.Duplex ?? 0;
                    this.SpecialPaperColor.IsChecked = value.Color;
                }
                else
                {
                    this.SpecialPaperPrinter.SelectedIndex = -1;
                    this.SpecialPaperBin1.SelectedIndex = -1;
                    this.SpecialPaperBin2.SelectedIndex = -1;
                    this.SpecialPaperDuplex.SelectedIndex = -1;
                    this.SpecialPaperColor.IsChecked = true;
                }
            }
            else
            {
                MPSettings.InEditMode = true;
                MPSettings.SpecialPaperSettingsDict = new Dictionary<string, SpecialPaperSettings>();
                MPSettings.InEditMode = false;
            }
        }

        private void SaveSpecialPaper(string oldSpecialPaperType)
        {
            MPSettings.InEditMode = true;
            if (!string.IsNullOrWhiteSpace(oldSpecialPaperType))
            {
                if (MPSettings.SpecialPaperSettingsDict == null)
                {
                    MPSettings.SpecialPaperSettingsDict = new Dictionary<string, SpecialPaperSettings>();
                }

                SpecialPaperSettings sps = new SpecialPaperSettings()
                {
                    Printer = SpecialPaperPrinter.SelectedItem as string,
                    Bin1 = SpecialPaperBin1.SelectedItem as string,
                    Bin2 = SpecialPaperBin2.SelectedItem as string,
                    Duplex = SpecialPaperDuplex.SelectedIndex,
                    Color = SpecialPaperColor.IsChecked == true
                };

                MPSettings.SpecialPaperSettingsDict[oldSpecialPaperType] = sps;
            }

            MPSettings.InEditMode = false;
        }

        private void RestoreToDefault_Click(object sender, RoutedEventArgs e)
        {
            if (MPSettings is ComposedMultiPrintSettings)
            {
                InfowareConfirmationWindow icw = new InfowareConfirmationWindow(LanguageManager.GetTranslation(LanguageConstants.DeletePersonalMultiPrintSettings, "Do you want to delete the personal MultiPrint settings and restore to company default settings?"),
                    LanguageManager.GetTranslation(LanguageConstants.RestoreToCompanyDefaults, "Restore to Company Defaults"),
                    MessageBoxButton.YesNo);
                icw.ShowDialog();
                if (icw.Result == MessageBoxResult.Yes)
                {
                    (MPSettings as ComposedMultiPrintSettings).RestoreToCompanyDefault();
                    //save settings to webapi
                    try
                    {
                        string multiPrintSettingsString = Newtonsoft.Json.JsonConvert.SerializeObject(MPSettings.RestoreToDefaultWhereSame());
                        ThisAddIn.Instance.MPSettings = MPSettings;
                        AdminPanelWebApi.UpdateCustomSetting(Utils.GetProcessorId(), "MultiPrintSettings", multiPrintSettingsString);
                    }
                    catch
                    {
                        MessageBox.Show(LanguageManager.GetTranslation(LanguageConstants.CouldNotSaveSettingsToWebApi, "Could not save settings to WebApi"));
                    }
                    this.Close();
                }
            }
        }
    }
}
