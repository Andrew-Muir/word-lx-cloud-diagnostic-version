﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using InfowareVSTO.Common.DTO;
using InfowareVSTO.MultiLanguage;
using Microsoft.Office.Core;
using Microsoft.Office.Interop.Word;

namespace InfowareVSTO
{
    public class SettingsManager
    {
        private static SettingDTO settings;
        public const bool SETTINGS_ENABLED = true;
        private const string ESCAPED_COMMA_CHARACTER = "[WORDLX-ESCAPED-COMMA-CHARACTER]";

        private const string GENERAL_SECTION_NAME = "General";

        public static string GetGeneralSetting(string key)
        {
            return GetSetting(key, sectionName: GENERAL_SECTION_NAME);
        }

        public static string GetSetting(string key, string sectionName = null, string defaultValue = null, MsoLanguageID languageID = MsoLanguageID.msoLanguageIDNone)
        {
            if (SETTINGS_ENABLED)
            {
                if (settings == null)
                {
                    settings = AdminPanelWebApi.GetAllCompanySettings();
                }

                if (key != null && settings != null)
                {
                    if (sectionName != null && languageID != MsoLanguageID.msoLanguageIDNone)
                    {
                        Dictionary<string, string> keyValues = settings.Sections.Where(x => x.SectionName?.Split(' ').FirstOrDefault() == string.Format("{0}-{1}", sectionName, ((int)languageID).ToString())).FirstOrDefault()?.KeyValues;
                        if (keyValues != null)
                        {
                            if (keyValues.TryGetValue(key, out string value))
                            {
                                return EscapeCommaCharacter(TrimQuotes(value));
                            }
                        }
                    }

                    if (sectionName != null)
                    {
                        Dictionary<string, string> keyValues = settings.Sections.Where(x => x.SectionName == sectionName).FirstOrDefault()?.KeyValues;
                        if (keyValues != null)
                        {
                            if (keyValues.TryGetValue(key, out string value))
                            {
                                return EscapeCommaCharacter(TrimQuotes(value));
                            }
                        }
                    }
                    else
                    {
                        foreach (Common.DTO.Section section in settings.Sections)
                        {
                            if (section.KeyValues.TryGetValue(key, out string value))
                            {
                                return EscapeCommaCharacter(TrimQuotes(value));
                            }
                        }
                    }
                }
            }

            return defaultValue;
        }

        private static string EscapeCommaCharacter(string input)
        {
            if (input != null)
            {
                return input.Replace("\\,", ESCAPED_COMMA_CHARACTER);
            }

            return input;
        }

        public static string UnescapeCommaCharacter(string input)
        {
            if (input != null)
            {
                return input.Replace(ESCAPED_COMMA_CHARACTER, ",");
            }

            return input;
        }

        public static string[] UnescapeCommaCharacter(string[] input)
        {
            if (input!= null)
            {
                for (int i = 0; i< input.Length; i++)
                {
                    input[i] = UnescapeCommaCharacter(input[i]);
                }
            }

            return input;
        }

        private static string TrimQuotes(string input)
        {
            if (input != null && input.Length > 1 &&  input[0] == '"' && input[input.Length - 1] == '"')
            {
                return input.Substring(1, input.Length - 2);
            }

            return input;
        }

        public static bool GetGeneralSettingAsBool(string key, bool defaultValue = false)
        {
            return GetSettingAsBool(key, sectionName: GENERAL_SECTION_NAME, defaultValue: defaultValue);
        }

        public static bool GetSettingAsBool(string key, string sectionName = null, bool defaultValue = false, MsoLanguageID languageID = MsoLanguageID.msoLanguageIDNone)
        {
            string str = GetSetting(key, sectionName, null, languageID);

            if (str != null)
            {
                return str.Trim().ToLower() == "true";
            }
            else
            {
                return defaultValue;
            }
        }

        public static int GetGeneralSettingAsInt(string key, int defaultValue = 0)
        {
            return GetSettingAsInt(key, sectionName: GENERAL_SECTION_NAME, defaultValue: defaultValue);
        }

        public static int GetSettingAsInt(string key, string sectionName = null, int defaultValue = -1, MsoLanguageID languageId = MsoLanguageID.msoLanguageIDNone)
        {
            int result = defaultValue;
            string setting = GetSetting(key, sectionName, languageID: languageId);
            if (setting != null && int.TryParse(setting, out result))
            {
                return result;
            }

            return defaultValue;
        }

        public static float GetSettingAsFloat(string key, string sectionName = null, float defaultValue = -1, MsoLanguageID languageId = MsoLanguageID.msoLanguageIDNone)
        {
            float result = defaultValue;
            string setting = GetSetting(key, sectionName, languageID: languageId);
            if (setting != null && float.TryParse(setting, out result))
            {
                return result;
            }

            return defaultValue;
        }

        public static WdColor GetPromptColor()
        {
            string colourStr = GetSetting("Colour", "PromptBullets");
            int colorInt = 0;
            if (colourStr != null && int.TryParse(colourStr, out colorInt))
            {
                return (WdColor)colorInt;
            }

            return WdColor.wdColorBlue;
        }

        public static void PopulateComboboxWithHandlingOptions(ComboBox combobox)
        {
            string handlingPrintOptions = GetSetting("HandlingPrintOptions", "Letters", null, MLanguageUtil.ActiveDocumentLanguage);

            if (handlingPrintOptions != null)
            {

                List<ComboboxItem> items = new List<ComboboxItem>();

                List<string> handlingPrintList = SettingsManager.UnescapeCommaCharacter(handlingPrintOptions.Split(',')).ToList();

                if (string.IsNullOrWhiteSpace(handlingPrintList[0]))
                {
                    handlingPrintList.RemoveAt(0);
                }

                if (handlingPrintList.Count > 0)
                {
                    string handlingViewOptions = GetSetting("HandlingViewOptions", "Letters",null, MLanguageUtil.ActiveDocumentLanguage);
                    List<string> handlingViewList = new List<string>();
                    if (handlingViewOptions != null)
                    {
                        handlingViewList = SettingsManager.UnescapeCommaCharacter(handlingViewOptions.Split(',')).ToList();

                        if (string.IsNullOrWhiteSpace(handlingViewList[0]))
                        {
                            handlingViewList.RemoveAt(0);
                        }
                    }

                    for (int i = 0; i < handlingPrintList.Count; i++)
                    {

                        items.Add(new ComboboxItem() { Text = (i < handlingViewList.Count ? handlingViewList[i] : handlingPrintList[i]), Value = handlingPrintList[i] });
                    }

                    combobox.Items.Clear();
                    combobox.ItemsSource = items;
                    combobox.SelectedValuePath = "Value";
                    combobox.DisplayMemberPath = "Text";
                }
            }
        }

        public static void PopulateComboboxWithDeliveryOptions(ComboBox combobox)
        {
            string deliveryPrintOptions = GetSetting("DeliveryPrintOptions", "Letters", null, MLanguageUtil.ActiveDocumentLanguage);

            if (deliveryPrintOptions != null)
            {

                List<ComboboxItem> items = new List<ComboboxItem>();

                List<string> deliveryPrintList = SettingsManager.UnescapeCommaCharacter(deliveryPrintOptions.Split(',')).ToList();

                if (string.IsNullOrWhiteSpace(deliveryPrintList[0]))
                {
                    deliveryPrintList.RemoveAt(0);
                }

                if (deliveryPrintList.Count > 0)
                {
                    string deliveryViewOptions = GetSetting("DeliveryViewOptions", "Letters", null, MLanguageUtil.ActiveDocumentLanguage);
                    List<string> deliveryViewList = new List<string>();
                    if (deliveryViewOptions != null)
                    {
                        deliveryViewList = SettingsManager.UnescapeCommaCharacter(deliveryViewOptions.Split(',')).ToList();

                        if (string.IsNullOrWhiteSpace(deliveryViewList[0]))
                        {
                            deliveryViewList.RemoveAt(0);
                        }
                    }

                    for (int i = 0; i < deliveryPrintList.Count; i++)
                    {

                        items.Add(new ComboboxItem() { Text = (i < deliveryViewList.Count ? deliveryViewList[i] : deliveryPrintList[i]), Value = deliveryPrintList[i] });
                    }

                    combobox.Items.Clear();
                    combobox.ItemsSource = items;
                    combobox.SelectedValuePath = "Value";
                    combobox.DisplayMemberPath = "Text";
                }
            }
        }
    }
}
