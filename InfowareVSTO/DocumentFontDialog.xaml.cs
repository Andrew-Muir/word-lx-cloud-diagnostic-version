﻿using InfowareVSTO.Common;
using Microsoft.Office.Interop.Word;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace InfowareVSTO
{

    /// <summary>
    /// Interaction logic for DocumentFontDialog.xaml
    /// </summary>
    public partial class DocumentFontDialog : InfowareWindow
    {
        private const string DEFAULT_FONT = "Calibri";
        private const int DEFAULT_SIZE = 11;
        private const int MIN_SIZE = 6;
        private const int MAX_SIZE = 14;

        public bool Ok { get; set; }

        public DocumentFontDialog()
        {
            InitializeComponent();

            LoadRadioButtonValue();

            List<string> fontNames = (new List<string>(ThisAddIn.Instance.Application.FontNames.Cast<string>())).OrderBy(q => q).ToList();
            string fontToSelect = GetFontToSelect();
            int sizeToSelect = GetSizeToSelect();
        
            foreach (string fontName in fontNames)
            {
                ComboBoxItem cbi = new ComboBoxItem();
                cbi.Content = fontName;
                if (fontName == fontToSelect)
                {
                    cbi.IsSelected = true;
                }

                this.FontComboBox.Items.Add(cbi);
            }

            if (sizeToSelect > 0)
            {
                SizeComboBox.Text = sizeToSelect.ToString();
            }

            Ok = false;
        }

        private void OkButton_Click(object sender, RoutedEventArgs e)
        {
            Ok = true;
            SaveRadioButtonValue();
            this.Close();
        }

        private void SaveRadioButtonValue()
        {
            bool allStylesChecked = this.AllStyles.IsChecked == true;
            Microsoft.Win32.Registry.SetValue(ConfigSettings.InfowareKeyFolder, "AllStylesDocumentFont", allStylesChecked);
        }

        private void LoadRadioButtonValue()
        {
            object allStylesChecked = Microsoft.Win32.Registry.GetValue(Login.InfowareKeyFolder, "AllStylesDocumentFont", null);
            if (allStylesChecked is string)
            {
                if ((string)allStylesChecked == "False" || (string)allStylesChecked == "false")
                {
                    this.BasedOnNormal.IsChecked = true;
                }
            }
            else
            {
                if (!SettingsManager.GetSettingAsBool("SetDocumentFontAll", "Tools", true))
                {
                    this.BasedOnNormal.IsChecked = true;
                }
            }
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            Ok = false;
            this.Close();
        }

        private string GetFontToSelect()
        {
            try
            {
                Document doc = ThisAddIn.Instance.Application.ActiveDocument;
                if (doc != null)
                {
                    return doc.Styles[WdBuiltinStyle.wdStyleNormal].Font.Name;
                }
            }
            catch
            {
                return DEFAULT_FONT;
            }

            return DEFAULT_FONT;
        }

        private int GetSizeToSelect()
        {
            try
            {
                Document doc = ThisAddIn.Instance.Application.ActiveDocument;
                if (doc != null)
                {
                    int rounded = (int)Math.Round(doc.Styles[WdBuiltinStyle.wdStyleNormal].Font.Size);
                    if (rounded < MIN_SIZE)
                    {
                        rounded = MIN_SIZE;
                    }

                    if (rounded > MAX_SIZE)
                    {
                        rounded = MAX_SIZE;
                    }
                    return rounded;
                }
            }
            catch
            {
                return DEFAULT_SIZE;
            }

            return DEFAULT_SIZE;
        }
    }
}
