﻿using InfowareVSTO.Common;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfowareVSTO
{
    public class Login
    {       
		public static string WebserviceURL
		{
			get
			{
				return ConfigSettings.WebApiUrl;
			}
		}

		public static string InfowareKeyFolder
		{
			get
			{
				return ConfigSettings.InfowareKeyFolder;
			}
		}

        public static int? AssureLogin(int operationID)
        {
			int? actionID = AdminPanelWebApi.AssureLogin(operationID);

			if (actionID == null)
			{
				LoginDialog loginDialog = new LoginDialog();
				loginDialog.ShowDialog();
				if (loginDialog.Ok)
                {
                    return AdminPanelWebApi.AssureLogin(operationID);
                }
			}

		    return actionID;
        }
    }
}
