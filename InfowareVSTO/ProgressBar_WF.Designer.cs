﻿namespace InfowareVSTO
{
	partial class ProgressBar_WF
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.progressBarCustom = new InfowareVSTO.ProgressBarCustom();
			this.txtLabel = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// progressBarCustom
			// 
			this.progressBarCustom.Location = new System.Drawing.Point(12, 12);
			this.progressBarCustom.Name = "progressBarCustom";
			this.progressBarCustom.Size = new System.Drawing.Size(361, 23);
			this.progressBarCustom.TabIndex = 0;
			// 
			// txtLabel
			// 
			this.txtLabel.AutoSize = true;
			this.txtLabel.Location = new System.Drawing.Point(13, 44);
			this.txtLabel.Name = "txtLabel";
			this.txtLabel.Size = new System.Drawing.Size(54, 13);
			this.txtLabel.TabIndex = 1;
			this.txtLabel.Text = "Loading...";
			// 
			// ProgressBar_WF
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(385, 69);
			this.Controls.Add(this.txtLabel);
			this.Controls.Add(this.progressBarCustom);
			this.Name = "ProgressBar_WF";
			this.Text = "Progress bar";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		public ProgressBarCustom progressBarCustom;
		public System.Windows.Forms.Label txtLabel;
	}
}