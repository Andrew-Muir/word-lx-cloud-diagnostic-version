﻿using InfowareVSTO.Addenda.Model;
using InfowareVSTO.Common;
using Microsoft.Office.Interop.Word;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using ContentControl = Microsoft.Office.Interop.Word.ContentControl;

namespace InfowareVSTO.Addenda
{
    /// <summary>
    /// Interaction logic for AddendaMove.xaml
    /// </summary>
    public partial class AddendaMove : InfowareWindow
    {
        private List<AddendaSection> AddendaSections;
        private AddendaOptions AddendaOptions;

        public AddendaMove()
        {
            InitializeComponent();

            AddendaOptions = new AddendaOptions();
            AddendaOptions.LoadOptionsFromRegistry();

            AddendaSections = ReadAddendasFromPage();
        }

        public List<AddendaSection> ReadAddendasFromPage()
        {
            var addendasInPage = new List<AddendaSection>();

            var activeDocumentContentControls = ThisAddIn.Instance.Application.ActiveDocument?.ContentControls;
            if(activeDocumentContentControls != null && activeDocumentContentControls.Count > 0)
            {
                foreach (ContentControl contentControl in activeDocumentContentControls)
                {
                    if ((contentControl.Title == CustomConstants.ADDENDA_CONTENT_CONTROL_TITLE) && contentControl.Range.Paragraphs[1] != null)
                    {
                        Range paragraphRange = contentControl.Range.Paragraphs[1].Range;
                        string paragraphTitle = paragraphRange.Text.Replace("\r", "").Replace("\n", "").Replace("\t", "").Replace("\f", "").Trim();

                        addendasInPage.Add(new AddendaSection {
                            Name = paragraphTitle,
                            Section = paragraphRange.Sections[1]
                        });

                        ListOfAddendums.Items.Add(new ListBoxItem {
                            Content = paragraphTitle,
                            AllowDrop = true
                        });
                    }
                }
            }

            return addendasInPage;
        }

        private void OK_Click(object sender, RoutedEventArgs e)
        {
            var activeDocument = ThisAddIn.Instance.Application.ActiveDocument;
            var listOfAddendums = ListOfAddendums?.Items;
            
            if (activeDocument != null && (AddendaSections != null && AddendaSections.Count > 0) && (listOfAddendums != null && listOfAddendums.Count > 0))
            {
                var lastSection = AddendaSections.LastOrDefault()?.Section;
                if(lastSection != null)
                {

                    Range tempRange = lastSection.Range;
                    tempRange.Collapse(WdCollapseDirection.wdCollapseEnd);
                    tempRange.MoveStart(WdUnits.wdCharacter,-1);

                    if (tempRange.Text != "\f")
                    {
                        tempRange.Collapse(WdCollapseDirection.wdCollapseEnd);
                        tempRange.InsertBreak(WdBreakType.wdSectionBreakNextPage);
                    }

                    var targetSection = activeDocument.Sections[lastSection.Index + 1];
                    if(targetSection != null)
                    {
                        var index = 0;
                        foreach (ListBoxItem listItem in listOfAddendums)
                        {
                            try
                            {
                                var listForMoveListItemContent = listItem.Content.ToString();
                                if (!string.IsNullOrEmpty(listForMoveListItemContent))
                                {
                                    var listForMoveItem = AddendaSections.FirstOrDefault(x => x.Name == listForMoveListItemContent);

                                    listForMoveItem.Section.Range.Copy();
                                    targetSection.Range.Paste();

                                    activeDocument.Sections[targetSection.Index - 1].Range.Bookmarks.Add(Utils.GetAddendaBookmarkFromBookmarks(listForMoveItem.Section.Range.Bookmarks));

                                    if (listForMoveItem.Section.Headers[WdHeaderFooterIndex.wdHeaderFooterFirstPage].Exists)
                                    {
                                        listForMoveItem.Section.Headers[WdHeaderFooterIndex.wdHeaderFooterFirstPage].Range.Copy();
                                        activeDocument.Sections[targetSection.Index - 1].Headers[WdHeaderFooterIndex.wdHeaderFooterFirstPage].Range.Paste();
                                        activeDocument.Sections[targetSection.Index - 1].Headers[WdHeaderFooterIndex.wdHeaderFooterFirstPage].Range.Paragraphs.Last.Range.Delete(WdUnits.wdCharacter, -1);
                                    }

                                    if (listForMoveItem.Section.Footers[WdHeaderFooterIndex.wdHeaderFooterFirstPage].Exists)
                                    {
                                        listForMoveItem.Section.Footers[WdHeaderFooterIndex.wdHeaderFooterFirstPage].Range.Copy();
                                        activeDocument.Sections[targetSection.Index - 1].Footers[WdHeaderFooterIndex.wdHeaderFooterFirstPage].Range.Paste();
                                        activeDocument.Sections[targetSection.Index - 1].Footers[WdHeaderFooterIndex.wdHeaderFooterFirstPage].Range.Paragraphs.Last.Range.Delete(WdUnits.wdCharacter, -1);
                                    }

                                    if (listForMoveItem.Section.Headers[WdHeaderFooterIndex.wdHeaderFooterPrimary].Exists)
                                    {
                                        listForMoveItem.Section.Headers[WdHeaderFooterIndex.wdHeaderFooterPrimary].Range.Copy();
                                        activeDocument.Sections[targetSection.Index - 1].Headers[WdHeaderFooterIndex.wdHeaderFooterPrimary].Range.Paste();
                                        activeDocument.Sections[targetSection.Index - 1].Headers[WdHeaderFooterIndex.wdHeaderFooterPrimary].Range.Paragraphs.Last.Range.Delete(WdUnits.wdCharacter, -1);
                                    }

                                    if (listForMoveItem.Section.Footers[WdHeaderFooterIndex.wdHeaderFooterPrimary].Exists)
                                    {
                                        listForMoveItem.Section.Footers[WdHeaderFooterIndex.wdHeaderFooterPrimary].Range.Copy();
                                        activeDocument.Sections[targetSection.Index - 1].Footers[WdHeaderFooterIndex.wdHeaderFooterPrimary].Range.Paste();
                                        activeDocument.Sections[targetSection.Index - 1].Footers[WdHeaderFooterIndex.wdHeaderFooterPrimary].Range.Paragraphs.Last.Range.Delete(WdUnits.wdCharacter, -1);
                                    }

                                    listForMoveItem.Section.Range.Delete();

                                    if (index == listOfAddendums.Count - 1)
                                    {
                                        tempRange = ThisAddIn.Instance.Application.ActiveDocument.Sections[targetSection.Index - 1].Range;
                                        tempRange.Collapse(WdCollapseDirection.wdCollapseEnd);
                                        tempRange.MoveStart(WdUnits.wdCharacter, -1);
                                        tempRange.Delete();
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                Debug.WriteLine(ex);
                            }

                            ++index;
                        }

                        if (ReNumberAddenda.IsChecked == true)
                        {
                            Utils.UpdateAddendaIndexes();
                            Utils.UpdateAddendaCrossReferences();
                        }
                    }
                }
            }

            Close();
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void UpClick(object sender, RoutedEventArgs e)
        {
            // only if the first item isn't the current one
            int i = ListOfAddendums.SelectedIndex;
            if (i - 1 >= 0 && ListOfAddendums.Items.Count > i)
            {
                ListBoxItem val = (ListOfAddendums.SelectedValue as ListBoxItem);
                ListBoxItem aux = new ListBoxItem();
                aux.Content = val.Content;
                aux.IsSelected = val.IsSelected;
                ListOfAddendums.Items.RemoveAt(i);
                ListOfAddendums.Items.Insert(i - 1, aux);
            }
        }

        private void DownClick(object sender, RoutedEventArgs e)
        {
            int i = ListOfAddendums.SelectedIndex;
            if (i >= 0 && ListOfAddendums.Items.Count > i + 1)
            {
                ListBoxItem val = (ListOfAddendums.SelectedValue as ListBoxItem);
                ListBoxItem aux = new ListBoxItem();
                aux.Content = val.Content;
                aux.IsSelected = val.IsSelected;
                ListOfAddendums.Items.RemoveAt(i);
                ListOfAddendums.Items.Insert(i + 1, aux);
            }
        }
    }
}
