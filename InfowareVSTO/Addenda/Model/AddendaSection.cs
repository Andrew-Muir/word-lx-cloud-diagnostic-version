﻿using Microsoft.Office.Interop.Word;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfowareVSTO.Addenda.Model
{
    public class AddendaSection
    {
        public string Name { get; set; }
        public Section Section { get; set; }
    }
}
