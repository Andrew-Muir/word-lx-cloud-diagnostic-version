﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfowareVSTO.Addenda.Model
{
    public class AutoIndex
    {
        private List<AutoIndexItem> AutoIndexItems = new List<AutoIndexItem>();

        public int this[string item]
        {
            get
            {
                foreach (AutoIndexItem autoIndexItem in AutoIndexItems)
                {
                    if (item == autoIndexItem.Name)
                    {
                        return autoIndexItem.Index;
                    }
                }

                return 1;
            }
            set
            {
                foreach (AutoIndexItem autoIndexItem in AutoIndexItems)
                {
                    if (item == autoIndexItem.Name)
                    {
                        autoIndexItem.Index = value;
                        return;
                    }
                }

                AutoIndexItems.Add(new AutoIndexItem
                {
                    Name = item,
                    Index = value
                });
            }
        }
    }

    public class AutoIndexItem
    {
        public string Name { get; set; }
        public int Index { get; set; }

        public AutoIndexItem()
        {
            Index = 1;
        }
    }
}
