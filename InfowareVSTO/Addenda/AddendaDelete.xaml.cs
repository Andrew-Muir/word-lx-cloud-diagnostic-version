﻿using InfowareVSTO.Addenda.Model;
using InfowareVSTO.Common;
using Microsoft.Office.Interop.Word;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace InfowareVSTO.Addenda
{
    /// <summary>
    /// Interaction logic for AddendaDelete.xaml
    /// </summary>
    public partial class AddendaDelete : InfowareWindow
    {
        public class ForAllSectionClass
        {
            public string newtitle = null;
            public string name = null;
            public Microsoft.Office.Interop.Word.Range section = null;
        }
        public class AllOfAddendumClass
        {
            private List<ForAllSectionClass> all_sections = new List<ForAllSectionClass>();

            public ForAllSectionClass this[string i]
            {
                get
                {
                    foreach (ForAllSectionClass aux in all_sections)
                    {
                        if (i == aux.name)
                        {
                            return aux;
                        }
                    }
                    return null;
                }
                set
                {
                    value.name = i;
                    all_sections.Add(value);
                }
            }
        }

        public AllOfAddendumClass AllAddendums = new AllOfAddendumClass();
        public AddendaOptions AddendaOptions = new AddendaOptions();

        public AddendaDelete()
        {
            InitializeComponent();

            Microsoft.Office.Interop.Word.Document Document = ThisAddIn.Instance.Application.ActiveDocument;
            foreach (Microsoft.Office.Interop.Word.ContentControl contentControl in Document.ContentControls)
            {
                if ((contentControl.Title == CustomConstants.ADDENDA_CONTENT_CONTROL_TITLE) && contentControl.Range.Paragraphs[1] != null)
                {
                    Microsoft.Office.Interop.Word.Range par = contentControl.Range.Paragraphs[1].Range;
                    ForAllSectionClass forAllSectionClass = new ForAllSectionClass();
                    System.Windows.Controls.CheckBox title_Text = new System.Windows.Controls.CheckBox();
                    string title = par.Text.Replace("\r", "").Replace("\n", "").Replace("\t", "").Replace("\f", "");
                    title_Text.Content = title;
                    ListOfAddendums.Items.Add(title_Text);
                    forAllSectionClass.section = par.Sections[1].Range;
                    AllAddendums[title] = forAllSectionClass;
                }
            }

            AddendaOptions.LoadOptionsFromRegistry();
        }

        private void OK_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Office.Interop.Word.Document Document = ThisAddIn.Instance.Application.ActiveDocument;
            System.Collections.IList listForDelete = ListOfAddendums.Items;

            foreach (System.Windows.Controls.CheckBox checkBox in listForDelete)
            {
                if (checkBox.IsChecked != null && checkBox.IsChecked == true)
                {
                    Range par = AllAddendums[(string)checkBox.Content].section.Duplicate;
                    AllAddendums[(string)checkBox.Content].section.MoveEnd(WdUnits.wdCharacter, -1);
                    AllAddendums[(string)checkBox.Content].section.MoveStart(WdUnits.wdCharacter, -1);
                    if (AddendaOptions.DeleteHighlightsIsChecked == true)
                    {
                        foreach (Bookmark bm in par.Bookmarks)
                        {
                            if (bm.Name.StartsWith(CustomConstants.ADDENDA_BOOKMARK_PREFIX))
                            {
                                foreach (Hyperlink link in Document.Hyperlinks)
                                {
                                    if (link.SubAddress == bm.Name)
                                    {
                                        link.TextToDisplay = "[ The Following Addendum was Deleted: " + link.TextToDisplay + " ]";
                                        link.Range.HighlightColorIndex = WdColorIndex.wdDarkYellow;
                                    }
                                }
                            }
                        }
                    }


                    if (AllAddendums[(string)checkBox.Content].section.Sections.Count > 1)
                    {
                        ThisAddIn.Instance.Application.ScreenUpdating = false;
                        if (ThisAddIn.Instance.IsFunctionForLogging("ScreenUpdating"))
                            Logger.Log("Disabled Screen Updating (AddendaDelete.xaml.cs > OK_Click)");
                        Section previousSection = AllAddendums[(string)checkBox.Content].section.Sections.First;
                        for (int i = 2; i <= AllAddendums[(string)checkBox.Content].section.Sections.Count; i++)
                        {
                            Section section = AllAddendums[(string)checkBox.Content].section.Sections[i];
                            Utils.SetSectionHeaderFooterProperties(section.Headers, true, false);
                            Utils.SetSectionHeaderFooterProperties(section.Footers, true, false);
                            Utils.CopySectionPageSetup(previousSection, section);
                        }
                        ThisAddIn.Instance.Application.ScreenUpdating = true;
                        if (ThisAddIn.Instance.IsFunctionForLogging("ScreenUpdating"))
                            Logger.Log("Enabled Screen Updating (AddendaDelete.xaml.cs > OK_Click)");
                    }

                    AllAddendums[(string)checkBox.Content].section.Delete();
                }
            }

            if (ReNumberAddenda.IsChecked != null && ReNumberAddenda.IsChecked == true)
            {
                Utils.UpdateAddendaIndexes();
                Utils.UpdateAddendaCrossReferences();
            }
            this.Close();
        }

        private string ReadDocumentProperty(string propertyName)
        {
            Microsoft.Office.Core.DocumentProperties properties;
            properties = (Microsoft.Office.Core.DocumentProperties)ThisAddIn.Instance.Application.ActiveDocument.CustomDocumentProperties;

            foreach (Microsoft.Office.Core.DocumentProperty prop in properties)
            {
                if (prop.Name == propertyName)
                {
                    return prop.Value.ToString();
                }
            }
            return null;
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void TypesOfAddendum_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string text = (sender as ComboBox).SelectedItem as string;
        }
    }
}
