﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InfowareVSTO.Common;
using Microsoft.Office.Interop.Word;
using Microsoft.VisualBasic;

namespace InfowareVSTO.Addenda
{
    public class AddendaUtils
    {
        public static string GetAddendaNumber(Section section)
        {
            string number = null;
            foreach(ContentControl cc in section.Range.ContentControls)
            {
                if (cc.Title == CustomConstants.ADDENDA_CONTENT_CONTROL_TITLE)
                {
                    if (cc.Range.ContentControls.Count > 0)
                    {
                        number = cc.Range.ContentControls[1].Range.Text;
                    }
                }
            }

            if (number != null)
            {
                return number.Trim('"', '“', '”');
            }

            return null;
        }
    }
}
