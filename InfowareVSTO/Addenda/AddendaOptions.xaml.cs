﻿using InfowareVSTO.Common;
using System.Windows;

namespace InfowareVSTO.Addenda
{
    /// <summary>
    /// Interaction logic for AddendaOptions.xaml
    /// </summary>
    public partial class AddendaOptions : InfowareWindow
    {
        public bool ColorIsChecked { get; private set; }
        public bool QuotationIsChecked { get; private set; }
        public bool NrInPageIsChecked { get; private set; }
        public bool DeleteHighlightsIsChecked { get; private set; }
        
        public AddendaOptions()
        {
            InitializeComponent();
            LoadOptionsFromRegistry();
        }

        private void OK_Click(object sender, RoutedEventArgs e)
        {
            StoreOptionsInRegistry();
            Close();
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void StoreOptionsInRegistry()
        {
            Microsoft.Win32.Registry.SetValue(ConfigSettings.InfowareKeyFolder, "AddendumColorIsChecked", addendumColor?.IsChecked ?? false);
            Microsoft.Win32.Registry.SetValue(ConfigSettings.InfowareKeyFolder, "AddendumQuotationIsChecked", addendumQuotation?.IsChecked ?? false);
            Microsoft.Win32.Registry.SetValue(ConfigSettings.InfowareKeyFolder, "AddendumNrInPageIsChecked", addendumNrInPage?.IsChecked ?? false);
            Microsoft.Win32.Registry.SetValue(ConfigSettings.InfowareKeyFolder, "AddendumDeleteHighlightIsChecked", addendumDeleteHighlight?.IsChecked ?? false);
        }

        public void LoadOptionsFromRegistry()
        {
            var addendumColorValue = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AddendumColorIsChecked", null);
            if(addendumColorValue != null)
            {
                ColorIsChecked = bool.Parse(addendumColorValue.ToString());
            }
            else
            {
                ColorIsChecked = false;
            }

            var addendumQuotationValue = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AddendumQuotationIsChecked", null);
            if (addendumQuotationValue != null)
            {
                QuotationIsChecked = bool.Parse(addendumQuotationValue.ToString());
            }
            else
            {
                QuotationIsChecked = SettingsManager.GetSettingAsBool("UseQuotesAroundAppendix", "Sections", false);
            }

            var addendumNrInPageValue = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AddendumNrInPageIsChecked", null);
            if (addendumNrInPageValue != null)
            {
                NrInPageIsChecked = bool.Parse(addendumNrInPageValue.ToString());
            }
            else
            {
                NrInPageIsChecked = false;
            }

            var addendumDeleteHighlightValue = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AddendumDeleteHighlightIsChecked", null);
            if (addendumDeleteHighlightValue != null)
            {
                DeleteHighlightsIsChecked = bool.Parse(addendumDeleteHighlightValue.ToString());
            }
            else
            {
                DeleteHighlightsIsChecked = false;
            }

            addendumColor.IsChecked = ColorIsChecked;
            addendumQuotation.IsChecked = QuotationIsChecked;
            addendumNrInPage.IsChecked = NrInPageIsChecked;
            addendumDeleteHighlight.IsChecked = DeleteHighlightsIsChecked;
        }
    }
}
