﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace InfowareVSTO.Addenda
{
    /// <summary>
    /// Interaction logic for AddendaInsertSelectNumberingType.xaml
    /// </summary>
    public partial class AddendaInsertSelectNumberingType : InfowareWindow
    {
        public Boolean OK_Run = new Boolean();

        public AddendaInsertSelectNumberingType()
        {
            InitializeComponent();
            this.OK_Run = false;
        }

        private void OK_Click(object sender, RoutedEventArgs e)
        {
            //Selected_Numeric_Format
            this.OK_Run = true;
            this.Close();
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
