﻿using InfowareVSTO.Common;
using InfowareVSTO.Common.Language;
using Microsoft.Office.Interop.Word;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using InfowareVSTO.Windows.ParagraphNumbering;
using InfowareVSTO.Common.Models;
using InfowareVSTO.Util;
using System.Diagnostics;
using Style = Microsoft.Office.Interop.Word.Style;
using InfowareVSTO.MultiLanguage;
using Word = Microsoft.Office.Interop.Word;

namespace InfowareVSTO.Addenda
{
    /// <summary>
    /// Interaction logic for AddendaInsert.xaml
    /// </summary>
    public partial class AddendaInsert : InfowareWindow
    {
        public class ForAllSectionClass
        {
            public string newtitle = null;
            public string name = null;
            public Microsoft.Office.Interop.Word.Range section = null;
        }

        public class AllOfAddendumClass
        {
            private List<ForAllSectionClass> all_sections = new List<ForAllSectionClass>();

            public ForAllSectionClass this[string i]
            {
                get
                {
                    foreach (ForAllSectionClass aux in all_sections)
                    {
                        if (i == aux.name)
                        {
                            return aux;
                        }
                    }
                    return null;
                }
                set
                {
                    value.name = i;
                    all_sections.Add(value);
                }
            }
        }

        private const string INSERT_CROSS_REF_SETTING = "AddendaInsertCrossRef";
        private const string ALIGNMENT_LEFT = "left";
        private const string ALIGNMENT_CENTERED = "centered";
        private const string ALIGNMENT_RIGHT = "right";
        private const string POSITION_HEADER = "header";
        private const string POSITION_FOOTER = "footer";
        private AllOfAddendumClass AllAddendums = new AllOfAddendumClass();
        private AddendaOptions AddendaOptions = new AddendaOptions();
        private readonly AddendaOptionConfiguration addendaOptionConfiguration;
        private Style PromptStyle;

        public AddendaInsert()
        {
            InitializeComponent();

            List<KeyValuePair<AddendaType, string>> addendaOptions = new List<KeyValuePair<AddendaType, string>>();
            if (MLanguageUtil.ActiveDocumentLanguage != Microsoft.Office.Core.MsoLanguageID.msoLanguageIDFrenchCanadian)
            {
                addendaOptions.Add(new KeyValuePair<AddendaType, string>(AddendaType.Appendix, MLanguageUtil.GetResource(MLanguageResourceEnum.ADDAppendix)));
            }

            addendaOptions.Add(new KeyValuePair<AddendaType, string>(AddendaType.Exhibit, MLanguageUtil.GetResource(MLanguageResourceEnum.ADDExhibit)));
            addendaOptions.Add(new KeyValuePair<AddendaType, string>(AddendaType.Schedule, MLanguageUtil.GetResource(MLanguageResourceEnum.ADDSchedule)));

            this.TypesOfAddendum.ItemsSource = addendaOptions.OrderBy(x => x.Value).ToList();
            this.TypesOfAddendum.SelectedIndex = 0;

            addendaOptionConfiguration = GetOptionConfiguration();
            EnableDisableAddendaOptionsByConfig(addendaOptionConfiguration.ConfigurationId);

            AddendaInit();
            AddendaOptions.LoadOptionsFromRegistry();

            PromptStyle = Utils.GetOrCreatePromptStyle();
        }

        public void AddendaInit()
        {
            try
            {
                var response = AdminPanelWebApi.GetCustomSetting(Utils.STATIC_PROCESSOR_ID, INSERT_CROSS_REF_SETTING);
                if (response.Data == null)
                {
                    CrossReferenceCursor.IsChecked = true;
                }
                else
                {
                    CrossReferenceCursor.IsChecked = Convert.ToBoolean(response.Data);
                }
            }
            catch
            {
                CrossReferenceCursor.IsChecked = true;
            }

            Microsoft.Office.Interop.Word.Document Document = ThisAddIn.Instance.Application.ActiveDocument;
            foreach (Microsoft.Office.Interop.Word.ContentControl contentControl in Document.ContentControls)
            {
                if ((contentControl.Title == CustomConstants.ADDENDA_CONTENT_CONTROL_TITLE) && contentControl.Range.Paragraphs[1] != null)
                {
                    Microsoft.Office.Interop.Word.Range par = contentControl.Range.Paragraphs[1].Range;
                    ForAllSectionClass forAllSectionClass = new ForAllSectionClass();
                    ListBoxItem title_Text = new ListBoxItem();
                    string title = par.Text.Replace("\r", "").Replace("\n", "").Replace("\t", "").Replace("\f", "");
                    title_Text.Content = title;
                    LocationsOfAddendum.Items.Add(title_Text);
                    forAllSectionClass.section = par.Sections[1].Range;
                    AllAddendums[title] = forAllSectionClass;
                }
            }
            ListBoxItem EndOfDoc_Text = new ListBoxItem();
            EndOfDoc_Text.Content = LanguageManager.GetTranslation(LanguageConstants.EndOfDocument, "End of Document");
            EndOfDoc_Text.IsSelected = true;
            LocationsOfAddendum.Items.Add(EndOfDoc_Text);
        }

        private Microsoft.Office.Interop.Word.Style GetOrCreateAddendaStyle()
        {
            try
            {
                return ThisAddIn.Instance.Application.ActiveDocument.Styles[CustomConstants.ADDENDA_STYLE];
            }
            catch (Exception)
            {
                Microsoft.Office.Interop.Word.Style result = ThisAddIn.Instance.Application.ActiveDocument.Styles.Add(CustomConstants.ADDENDA_STYLE, WdStyleType.wdStyleTypeParagraph);

                result.set_BaseStyle(Utils.GetLocalizedStyleNameFromBuiltInStyle(WdBuiltinStyle.wdStyleNormal));
                result.Font.Bold = 1;
                result.Font.Size = 12;
                result.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphCenter;
                return result;
            }
        }

        private void OK_Click(object sender, RoutedEventArgs e)
        {

            bool close = true;
            if (AddendaManualRadioButton.IsChecked == true)
            {
                close = ManualAddendaInsert();
            }
            else
            {
                NumericAddendaInsert();
            }

            if (close)
            {
                Close();
            }
        }

        private void AddPageNumbering(Section section)
        {
            string position = POSITION_FOOTER;
            string alignment = ALIGNMENT_RIGHT;
            int pageNumberLocation = SettingsManager.GetSettingAsInt("AddendaPageNumberLocation", "Sections", 0);
            if (pageNumberLocation == 1)
            {
                string ccTitle = GetFirstPageNumberCC();

                if (ccTitle == null)
                {
                    ccTitle = GetLegacyPageNumberInfo();
                }

                if (ccTitle != null)
                {
                    string[] strArr = ccTitle.Split(';');

                    if (strArr.Length > 1)
                    {
                        alignment = strArr[0];
                        position = strArr[1];
                    }
                }

            }
            else
            {
                switch (pageNumberLocation)
                {
                    case 2:
                        position = POSITION_FOOTER;
                        alignment = ALIGNMENT_CENTERED;
                        break;
                    case 3:
                        position = POSITION_FOOTER;
                        alignment = ALIGNMENT_LEFT;
                        break;
                    case 4:
                        position = POSITION_HEADER;
                        alignment = ALIGNMENT_RIGHT;
                        break;
                    case 5:
                        position = POSITION_HEADER;
                        alignment = ALIGNMENT_CENTERED;
                        break;
                    case 6:
                        position = POSITION_HEADER;
                        alignment = ALIGNMENT_LEFT;
                        break;
                }
            }

            var footersList = position == POSITION_HEADER ?
                new List<HeaderFooter>() {
                section.Headers[WdHeaderFooterIndex.wdHeaderFooterFirstPage],
                section.Headers[WdHeaderFooterIndex.wdHeaderFooterPrimary],
                section.Headers[WdHeaderFooterIndex.wdHeaderFooterEvenPages]
            } : new List<HeaderFooter>() {
                section.Footers[WdHeaderFooterIndex.wdHeaderFooterFirstPage],
                section.Footers[WdHeaderFooterIndex.wdHeaderFooterPrimary],
                section.Footers[WdHeaderFooterIndex.wdHeaderFooterEvenPages]
            };

            var continuousPageNumbering = SettingsManager.GetSettingAsBool("ContinuousPageNumbering", "Tools", false);

            if (continuousPageNumbering == false)
            {
                var firstPageHasPageNumber = SettingsManager.GetSettingAsInt("FirstPageHasPageNumber", "Sections", -1);

                if (firstPageHasPageNumber == 0 || firstPageHasPageNumber == 1)
                {
                    footersList = position == POSITION_HEADER ?
                        new List<HeaderFooter>() {
                        section.Headers[WdHeaderFooterIndex.wdHeaderFooterPrimary],
                        section.Headers[WdHeaderFooterIndex.wdHeaderFooterEvenPages]
                    } : new List<HeaderFooter>() {
                        section.Footers[WdHeaderFooterIndex.wdHeaderFooterPrimary],
                        section.Footers[WdHeaderFooterIndex.wdHeaderFooterEvenPages]
                    };

                    section.PageSetup.DifferentFirstPageHeaderFooter = -1;
                }
            }

            bool includeAddendaNumber = new AddendaOptions().NrInPageIsChecked;

            if (footersList.Count > 0)
            {
                string addendaNumber = includeAddendaNumber ? AddendaUtils.GetAddendaNumber(section) : null;

                string prepostchars = SettingsManager.GetSetting("AddendaPageNumberOpenCloseChars", "Sections");
                string prechar = null;
                string postchar = null;

                if (!string.IsNullOrWhiteSpace(prepostchars))
                {
                    string[] strArr = prepostchars.Split('"');

                    if (strArr.Length > 2)
                    {
                        if (strArr.Length < 4)
                        {
                            prechar = strArr[0];
                            postchar = strArr[2];
                        }
                        else
                        {
                            prechar = strArr[1];
                            postchar = strArr[3];
                        }
                    }
                }

                foreach (HeaderFooter footer in footersList)
                {
                    if (footer.Exists)
                    {
                        var field = footer.Range.Fields.Add(footer.Range, WdFieldType.wdFieldPage);
                        Range startRange = footer.Range.Duplicate;
                        Range endRange = footer.Range.Duplicate;

                        startRange.Collapse(WdCollapseDirection.wdCollapseStart);
                        endRange.Collapse(WdCollapseDirection.wdCollapseEnd);



                        if (!string.IsNullOrWhiteSpace(prechar))
                        {
                            startRange.Text = prechar;
                        }

                        Range range = startRange.Duplicate;
                        range.Collapse(WdCollapseDirection.wdCollapseEnd);

                        if (!string.IsNullOrWhiteSpace(postchar))
                        {
                            endRange.Text = postchar;
                        }

                        switch (alignment)
                        {
                            case ALIGNMENT_LEFT:
                                footer.Range.Paragraphs[1].Alignment = WdParagraphAlignment.wdAlignParagraphLeft;
                                break;
                            case ALIGNMENT_CENTERED:
                                footer.Range.Paragraphs[1].Alignment = WdParagraphAlignment.wdAlignParagraphCenter;
                                break;
                            case ALIGNMENT_RIGHT:
                            default:
                                footer.Range.Paragraphs[1].Alignment = WdParagraphAlignment.wdAlignParagraphRight;
                                break;
                        }

                        if (addendaNumber != null)
                        {
                            range.Collapse(WdCollapseDirection.wdCollapseStart);
                            range.Text = "-";
                            range.Collapse(WdCollapseDirection.wdCollapseStart);
                            var cc = range.ContentControls.Add();
                            cc.Tag = CustomConstants.ADDENDA_NUMBER_IN_PAGE_NUMBER_TAG;
                            cc.Range.Text = addendaNumber;
                        }
                    }
                }
            }
        }

        private string GetLegacyPageNumberInfo()
        {
            string position = null;
            WdParagraphAlignment alignment = WdParagraphAlignment.wdAlignParagraphCenter;
            ThisAddIn.Instance.Application.ScreenUpdating = false;
            if (ThisAddIn.Instance.IsFunctionForLogging("ScreenUpdating"))
                Logger.Log("Disabled Screen Updating (AddendaInsert.xaml.cs > GetLegacyPageNumberInfo)");
            foreach (Section section in ThisAddIn.Instance.Application.ActiveDocument.Sections)
            {
                var header = section.Headers[WdHeaderFooterIndex.wdHeaderFooterFirstPage];
                var pageNumbers = header.Range.Fields.Cast<Field>().Where(x => x.Type == WdFieldType.wdFieldPage || x.Type == WdFieldType.wdFieldNumPages).ToList();
                if (Utils.GetPageNoTextboxes(header).Count > 0)
                {
                    alignment = WdParagraphAlignment.wdAlignParagraphCenter;
                    position = POSITION_HEADER;
                }
                foreach (Field pn in pageNumbers)
                {
                    alignment = pn.Code.Paragraphs.First.Alignment;
                    position = POSITION_HEADER;
                }

                header = section.Headers[WdHeaderFooterIndex.wdHeaderFooterPrimary];
                pageNumbers = header.Range.Fields.Cast<Field>().Where(x => x.Type == WdFieldType.wdFieldPage || x.Type == WdFieldType.wdFieldNumPages).ToList();
                if (Utils.GetPageNoTextboxes(header).Count > 0)
                {
                    alignment = WdParagraphAlignment.wdAlignParagraphCenter;
                    position = POSITION_HEADER;
                }
                foreach (Field pn in pageNumbers)
                {
                    alignment = pn.Code.Paragraphs.First.Alignment;
                    position = POSITION_HEADER;
                }

                header = section.Footers[WdHeaderFooterIndex.wdHeaderFooterFirstPage];
                pageNumbers = header.Range.Fields.Cast<Field>().Where(x => x.Type == WdFieldType.wdFieldPage || x.Type == WdFieldType.wdFieldNumPages).ToList();
                if (Utils.GetPageNoTextboxes(header).Count > 0)
                {
                    alignment = WdParagraphAlignment.wdAlignParagraphCenter;
                    position = POSITION_FOOTER;
                }
                foreach (Field pn in pageNumbers)
                {
                    alignment = pn.Code.Paragraphs.First.Alignment;
                    position = POSITION_FOOTER;
                }

                header = section.Footers[WdHeaderFooterIndex.wdHeaderFooterPrimary];
                pageNumbers = header.Range.Fields.Cast<Field>().Where(x => x.Type == WdFieldType.wdFieldPage || x.Type == WdFieldType.wdFieldNumPages).ToList();
                if (Utils.GetPageNoTextboxes(header).Count > 0)
                {
                    alignment = WdParagraphAlignment.wdAlignParagraphCenter;
                    position = POSITION_FOOTER;
                }
                foreach (Field pn in pageNumbers)
                {
                    alignment = pn.Code.Paragraphs.First.Alignment;
                    position = POSITION_FOOTER;
                }

                if (position != null)
                {
                    break;
                }
            }
            ThisAddIn.Instance.Application.ScreenUpdating = true;
            if (ThisAddIn.Instance.IsFunctionForLogging("ScreenUpdating"))
                Logger.Log("Enabled Screen Updating (AddendaInsert.xaml.cs > GetLegacyPageNumberInfo)");
            if (position != null)
            {
                string alignmentText = string.Empty;
                switch (alignment)
                {
                    case WdParagraphAlignment.wdAlignParagraphLeft:
                    case WdParagraphAlignment.wdAlignParagraphJustify:
                        alignmentText = ALIGNMENT_LEFT;
                        break;
                    case WdParagraphAlignment.wdAlignParagraphCenter:
                        alignmentText = ALIGNMENT_CENTERED;
                        break;
                    case WdParagraphAlignment.wdAlignParagraphRight:
                        alignmentText = ALIGNMENT_RIGHT;
                        break;
                }

                return $"{alignmentText};{position}";

            }

            return null;
        }

        private string GetFirstPageNumberCC()
        {
            Word.ContentControl foundCc = null;
            ThisAddIn.Instance.Application.ScreenUpdating = false;
            if (ThisAddIn.Instance.IsFunctionForLogging("ScreenUpdating"))
                Logger.Log("Disabled Screen Updating (AddendaInsert.xaml.cs > GetFirstPageNumberCC)");
            foreach (Section section in ThisAddIn.Instance.Application.ActiveDocument.Sections)
            {
                var headerFootersList = new List<HeaderFooter>() {
                    section.Footers[WdHeaderFooterIndex.wdHeaderFooterFirstPage],
                    section.Footers[WdHeaderFooterIndex.wdHeaderFooterPrimary],
                    section.Footers[WdHeaderFooterIndex.wdHeaderFooterEvenPages],

                    section.Headers[WdHeaderFooterIndex.wdHeaderFooterFirstPage],
                    section.Headers[WdHeaderFooterIndex.wdHeaderFooterPrimary],
                    section.Headers[WdHeaderFooterIndex.wdHeaderFooterEvenPages],
                };

                foreach (HeaderFooter hf in headerFootersList)
                {
                    foreach (Word.ContentControl cc in hf.Range.ContentControls)
                    {
                        if (cc.Tag == CustomConstants.PAGE_NUMBERING_TAG)
                        {
                            foundCc = cc;
                            break;
                        }
                    }

                    if (foundCc != null)
                    {
                        break;
                    }
                }

                if (foundCc != null)
                {
                    break;
                }
            }
            ThisAddIn.Instance.Application.ScreenUpdating = true;
            if (ThisAddIn.Instance.IsFunctionForLogging("ScreenUpdating"))
                Logger.Log("Enabled Screen Updating (AddendaInsert.xaml.cs > GetFirstPageNumberCC)");
            if (foundCc != null)
            {
                return foundCc.Title;
            }

            return null;
        }

        private void RestartPageNumberingIfNeeded(Section section)
        {
            bool restart = !SettingsManager.GetSettingAsBool("ContinuousPageNumbering", "Tools", false);

            if (restart)
            {
                section.Footers[WdHeaderFooterIndex.wdHeaderFooterPrimary].PageNumbers.RestartNumberingAtSection = true;
                section.Footers[WdHeaderFooterIndex.wdHeaderFooterPrimary].PageNumbers.StartingNumber = 1;
                section.Footers[WdHeaderFooterIndex.wdHeaderFooterFirstPage].PageNumbers.RestartNumberingAtSection = true;
                section.Footers[WdHeaderFooterIndex.wdHeaderFooterFirstPage].PageNumbers.StartingNumber = 1;
                section.Footers[WdHeaderFooterIndex.wdHeaderFooterEvenPages].PageNumbers.RestartNumberingAtSection = true;
                section.Footers[WdHeaderFooterIndex.wdHeaderFooterEvenPages].PageNumbers.StartingNumber = 1;
                section.Headers[WdHeaderFooterIndex.wdHeaderFooterEvenPages].PageNumbers.RestartNumberingAtSection = true;
                section.Headers[WdHeaderFooterIndex.wdHeaderFooterEvenPages].PageNumbers.StartingNumber = 1;
                section.Headers[WdHeaderFooterIndex.wdHeaderFooterPrimary].PageNumbers.RestartNumberingAtSection = true;
                section.Headers[WdHeaderFooterIndex.wdHeaderFooterPrimary].PageNumbers.StartingNumber = 1;
                section.Headers[WdHeaderFooterIndex.wdHeaderFooterFirstPage].PageNumbers.RestartNumberingAtSection = true;
                section.Headers[WdHeaderFooterIndex.wdHeaderFooterFirstPage].PageNumbers.StartingNumber = 1;
            }
            else
            {
                section.Footers[WdHeaderFooterIndex.wdHeaderFooterPrimary].PageNumbers.RestartNumberingAtSection = false;
                section.Footers[WdHeaderFooterIndex.wdHeaderFooterFirstPage].PageNumbers.RestartNumberingAtSection = false;
                section.Footers[WdHeaderFooterIndex.wdHeaderFooterEvenPages].PageNumbers.RestartNumberingAtSection = false;
                section.Headers[WdHeaderFooterIndex.wdHeaderFooterEvenPages].PageNumbers.RestartNumberingAtSection = false;
                section.Headers[WdHeaderFooterIndex.wdHeaderFooterPrimary].PageNumbers.RestartNumberingAtSection = false;
                section.Headers[WdHeaderFooterIndex.wdHeaderFooterFirstPage].PageNumbers.RestartNumberingAtSection = false;
            }
        }

        public void DisableLinkToPrevious(Section section)
        {
            section.Headers[WdHeaderFooterIndex.wdHeaderFooterPrimary].LinkToPrevious = false;
            section.Headers[WdHeaderFooterIndex.wdHeaderFooterPrimary].Range.Text = "";

            if (section.Headers[WdHeaderFooterIndex.wdHeaderFooterFirstPage].Exists)
            {
                section.Headers[WdHeaderFooterIndex.wdHeaderFooterFirstPage].LinkToPrevious = false;
                section.Headers[WdHeaderFooterIndex.wdHeaderFooterFirstPage].Range.Text = "";
            }

            if (section.Headers[WdHeaderFooterIndex.wdHeaderFooterEvenPages].Exists)
            {
                section.Headers[WdHeaderFooterIndex.wdHeaderFooterEvenPages].LinkToPrevious = false;
                section.Headers[WdHeaderFooterIndex.wdHeaderFooterEvenPages].Range.Text = "";
            }

            section.Footers[WdHeaderFooterIndex.wdHeaderFooterPrimary].LinkToPrevious = false;
            section.Footers[WdHeaderFooterIndex.wdHeaderFooterPrimary].Range.Text = "";

            if (section.Footers[WdHeaderFooterIndex.wdHeaderFooterFirstPage].Exists)
            {
                section.Footers[WdHeaderFooterIndex.wdHeaderFooterFirstPage].LinkToPrevious = false;
                section.Footers[WdHeaderFooterIndex.wdHeaderFooterFirstPage].Range.Text = "";
            }

            if (section.Footers[WdHeaderFooterIndex.wdHeaderFooterEvenPages].Exists)
            {
                section.Footers[WdHeaderFooterIndex.wdHeaderFooterEvenPages].LinkToPrevious = false;
                section.Footers[WdHeaderFooterIndex.wdHeaderFooterEvenPages].Range.Text = "";
            }
        }

        private void InsertXRef(string addendaType, string bookmarkName)
        {
            Range selection = ThisAddIn.Instance.Application.ActiveWindow.Selection.Range;
            selection.Collapse(WdCollapseDirection.wdCollapseEnd);

            bool insertSpaceAtStart = false;
            bool insertSpaceAtEnd = false;

            Range selectionDuplicate = selection.Duplicate;

            selectionDuplicate.Collapse(WdCollapseDirection.wdCollapseStart);
            selectionDuplicate.MoveStart(WdUnits.wdCharacter, -1);
            string firstCharacterMinusOne = selectionDuplicate.Text;
            if (!string.IsNullOrWhiteSpace(firstCharacterMinusOne))
            {
                insertSpaceAtStart = true;
            }

            selectionDuplicate.Collapse(WdCollapseDirection.wdCollapseEnd);
            selectionDuplicate.MoveEnd(WdUnits.wdCharacter, 1);
            string lastCharacterPlusOne = selectionDuplicate.Text;
            if (!string.IsNullOrWhiteSpace(lastCharacterPlusOne))
            {
                insertSpaceAtEnd = true;
            }

            Microsoft.Office.Interop.Word.ContentControl titleCC = null;
            Microsoft.Office.Interop.Word.ContentControl miniCC = null;
            Bookmark bm = ThisAddIn.Instance.Application.ActiveDocument.Bookmarks[bookmarkName];
            if (bm.Range.Sections[1].Range.ContentControls.Count > 0)
            {
                titleCC = bm.Range.Sections[1].Range.ContentControls[1];
                if (titleCC.Range.ContentControls.Count > 0)
                {
                    miniCC = titleCC.Range.ContentControls[1];

                    selection.Text = addendaType + " " + miniCC.Range.Text;
                }
            }

            ThisAddIn.Instance.Application.ActiveDocument.Hyperlinks.Add(selection, '#' + bookmarkName);
            selection.set_Style(Utils.GetOrCreateAddendaCrossReferenceCharacterStyle());

            if (insertSpaceAtStart)
            {
                selection.InsertBefore(" ");
            }

            if (insertSpaceAtEnd)
            {
                selection.Collapse(WdCollapseDirection.wdCollapseEnd);
                selection.MoveEnd(WdUnits.wdCharacter);
                selection.InsertAfter(" ");
            }
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private AddendaOptionConfiguration GetOptionConfiguration()
        {
            var optionConfiguration = new AddendaOptionConfiguration();
            var selection = DocumentUtil.GetSelection();

            if (selection != null)
            {
                var selectedParagraph = selection.Range?.Paragraphs[1];

                var paragraphLevelNumberAndRange = ListTemplateUtil.GetSelectionParagraphNumberAndRange(selectedParagraph);
                if (!string.IsNullOrEmpty(paragraphLevelNumberAndRange.LevelNumber))
                {
                    var rangeHasHyperlink = Utils.CheckIfRangeHasAddendaHyperlink(paragraphLevelNumberAndRange.Range);
                    if (rangeHasHyperlink || paragraphLevelNumberAndRange.LevelNumber == "")
                    {
                        optionConfiguration.ConfigurationId = 1;
                    }
                    else
                    {
                        optionConfiguration.ConfigurationId = 2;
                    }
                }
                else
                {
                    paragraphLevelNumberAndRange = ListTemplateUtil.GetSelectionParagraphNumberAndRange(selectedParagraph, 20);
                    if (!string.IsNullOrEmpty(paragraphLevelNumberAndRange.LevelNumber))
                    {
                        var previousRangeHasHyperlink = Utils.CheckIfRangeHasAddendaHyperlink(paragraphLevelNumberAndRange.Range);
                        var rangeHasHyperlink = Utils.CheckIfRangeHasAddendaHyperlink(selectedParagraph.Range);
                        if (previousRangeHasHyperlink || rangeHasHyperlink || paragraphLevelNumberAndRange.LevelNumber == "")
                        {
                            optionConfiguration.ConfigurationId = 1;
                        }
                        else
                        {
                            optionConfiguration.ConfigurationId = 3;
                        }
                    }
                    else
                    {
                        optionConfiguration.ConfigurationId = 1;
                    }
                }

                optionConfiguration.ParagraphNumberingLevel = Utils.TrimEndPunctuationMarks(paragraphLevelNumberAndRange.LevelNumber);
            }

            return optionConfiguration;
        }

        private void EnableDisableAddendaOptionsByConfig(int config)
        {
            if (config != -1)
            {
                switch (config)
                {
                    case 1:
                        AddendaNumericRadioButton.IsEnabled = false;
                        AddendaNumericLinkToPreceedingRadioButton.IsEnabled = false;
                        AddendaManualRadioButton.IsEnabled = true;
                        AddendaManualRadioButton.IsChecked = true;
                        break;
                    case 2:
                        AddendaNumericRadioButton.IsEnabled = true;
                        AddendaNumericLinkToPreceedingRadioButton.IsEnabled = false;
                        AddendaManualRadioButton.IsEnabled = true;
                        AddendaNumericRadioButton.IsChecked = true;
                        break;
                    case 3:
                        AddendaNumericRadioButton.IsEnabled = false;
                        AddendaNumericLinkToPreceedingRadioButton.IsEnabled = true;
                        AddendaManualRadioButton.IsEnabled = true;
                        AddendaNumericLinkToPreceedingRadioButton.IsChecked = true;
                        break;
                }
            }
        }

        private bool ManualAddendaInsert()
        {
            var activeDocument = ThisAddIn.Instance.Application.ActiveDocument;
            var properties = (Microsoft.Office.Core.DocumentProperties)activeDocument.CustomDocumentProperties;

            string addendumType = TypesOfAddendum.Text;
            AddendaType addendaTypeEnum = (AddendaType)TypesOfAddendum.SelectedValue;

            string location = (LocationsOfAddendum.SelectedItem as ListBoxItem)?.Content?.ToString();
            string numericFormat = Utils.ReadDocumentProperty(addendaTypeEnum.ToString());

            int wordNr = 2, wordStartColor = 3;
            if (numericFormat == null)
            {
                AddendaInsertSelectNumberingType dil = new AddendaInsertSelectNumberingType();
                dil.ShowDialog();
                if (dil.OK_Run == true)
                {
                    numericFormat = (dil.Selected_Numeric_Format.SelectedItem as ListBoxItem)?.Content?.ToString();
                    properties.Add(addendaTypeEnum.ToString(), false, Microsoft.Office.Core.MsoDocProperties.msoPropertyTypeString, numericFormat);
                }
                else
                {
                    return false;
                }
            }

            Range section = activeDocument.Range(0, 0);
            if (location != LanguageManager.GetTranslation(LanguageConstants.EndOfDocument, "End of Document"))
            {
                section = AllAddendums[location].section.Previous(WdUnits.wdCharacter);
                section.InsertBreak(WdBreakType.wdSectionBreakNextPage);
                //section.Paragraphs.Add();
            }
            else
            {
                try
                {
                    section = activeDocument.Sections.Add().Range;
                }
                catch (Exception ex)
                {
                    Range tempRange = activeDocument.Content.Duplicate;
                    tempRange.Collapse(WdCollapseDirection.wdCollapseEnd);

                    tempRange.InsertBreak(WdBreakType.wdSectionBreakNextPage);
                    section = activeDocument.Sections.Last.Range;
                }
            }

            DisableLinkToPrevious(section.Sections.First);

            RestartPageNumberingIfNeeded(section.Sections.First);

            section.Text = addendumType + " 1 [ Title ]";

            Random rand = new Random();
            string bookmarkName = CustomConstants.ADDENDA_BOOKMARK_PREFIX + Math.Abs(rand.Next());
            section.Bookmarks.Add(bookmarkName);

            try
            {
                section.Words[wordStartColor].set_Style(PromptStyle);
                section.Words[wordStartColor + 1].set_Style(PromptStyle);
                section.Words[wordStartColor + 2].set_Style(PromptStyle);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }

            Microsoft.Office.Interop.Word.ContentControl contentControl = section.Words[wordNr].ContentControls.Add();
            contentControl.Title = addendaTypeEnum.ToString();
            section.Paragraphs[1].set_Style(GetOrCreateAddendaStyle());

            section.Paragraphs.Add();
            if (section.Paragraphs.Count == 1)
            {
                section.Paragraphs.Add();
            }
            section.Paragraphs[2].set_Style(Utils.GetLocalizedStyleNameFromBuiltInStyle(WdBuiltinStyle.wdStyleNormal));

            var addendaContentControl = section.Paragraphs.First.Range.ContentControls.Add();
            addendaContentControl.Title = CustomConstants.ADDENDA_CONTENT_CONTROL_TITLE;
            addendaContentControl.Tag = addendaTypeEnum.ToString();

            if (addendaTypeEnum == AddendaType.Exhibit)
            {
                string customExhibitStamp = AdminPanelWebApi.GetCustomExhibitStamp(MultiLanguage.MLanguageUtil.ActiveDocumentLanguage);
                if (string.IsNullOrEmpty(customExhibitStamp))
                {
                    Word.Paragraph exhibit_text = section.Paragraphs.Add();
                    exhibit_text.Range.Text = "referred to in the Affidavit of * , sworn before me this * day of * , " + DateTime.Now.Year.ToString() + ".";
                    exhibit_text.Range.Font.Bold = 0;
                    exhibit_text.Range.ParagraphFormat.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphLeft;

                    Range range = exhibit_text.Range.Words[7];

                    range.MoveEnd(WdUnits.wdCharacter, -1);
                    range.InsertSymbol(108, "Wingdings");
                    range.Font.Color = WdColor.wdColorBlue;

                    range = exhibit_text.Range.Words[13];
                    range.MoveEnd(WdUnits.wdCharacter, -1);
                    range.InsertSymbol(108, "Wingdings");
                    range.Font.Color = WdColor.wdColorBlue;

                    range = exhibit_text.Range.Words[16];
                    range.MoveEnd(WdUnits.wdCharacter, -1);
                    range.InsertSymbol(108, "Wingdings");
                    range.Font.Color = WdColor.wdColorBlue;

                    exhibit_text.Range.InsertParagraphAfter();
                    exhibit_text.Range.InsertParagraphAfter();
                    exhibit_text.Range.InsertParagraphAfter();
                    exhibit_text.Range.InlineShapes.AddHorizontalLineStandard();
                    exhibit_text.Range.Text = "A Commissioner of taking affidavits.";
                    exhibit_text.Range.InsertParagraphAfter();
                    exhibit_text.Range.Text = "(Insert name if known)";
                    exhibit_text.Range.Font.Color = WdColor.wdColorGreen;
                    exhibit_text.Range.InsertParagraphAfter();
                    exhibit_text.Range.Font.Color = WdColor.wdColorBlack;
                    exhibit_text.Range.InsertParagraphAfter();
                    exhibit_text.Range.InsertParagraphAfter();
                    exhibit_text.Range.InsertParagraphAfter();
                    exhibit_text.Range.InsertParagraphAfter();
                    Word.Table table = exhibit_text.Range.Tables.Add(exhibit_text.Range, 1, 1);
                    table.Range.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphCenter;
                    table.Rows.Alignment = WdRowAlignment.wdAlignRowCenter;
                    Cell cell = table.Cell(1, 1);
                    cell.PreferredWidth = (float)200;
                    cell.Range.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphCenter;
                    cell.Range.Borders[WdBorderType.wdBorderTop].LineStyle = WdLineStyle.wdLineStyleDot;
                    cell.Range.Borders[WdBorderType.wdBorderBottom].LineStyle = WdLineStyle.wdLineStyleDot;
                    cell.Range.Borders[WdBorderType.wdBorderLeft].LineStyle = WdLineStyle.wdLineStyleDot;
                    cell.Range.Borders[WdBorderType.wdBorderRight].LineStyle = WdLineStyle.wdLineStyleDot;
                    cell.Range.Text = "This is EXHIBIT";
                    cell.Range.Bold = 1;
                }
                else
                {
                    Word.Paragraph exhibit_text = section.Paragraphs.Add();
                    exhibit_text.Range.InsertXML(customExhibitStamp);
                }
            }

            Utils.UpdateAddendaIndexes();
            AddPageNumbering(section.Sections.First);
            if (CrossReferenceCursor.IsChecked == true)
            {
                InsertXRef(addendumType, bookmarkName);
            }

            try
            {
                AdminPanelWebApi.UpdateCustomSetting(Utils.STATIC_PROCESSOR_ID, INSERT_CROSS_REF_SETTING, (CrossReferenceCursor.IsChecked == true).ToString());
            }
            catch { }

            Utils.UpdateAddendaCrossReferences();
            return true;
        }

        private void NumericAddendaInsert()
        {
            var activeDocument = ThisAddIn.Instance.Application.ActiveDocument;
            var location = (LocationsOfAddendum.SelectedItem as ListBoxItem)?.Content?.ToString();
            var format = addendaOptionConfiguration.ParagraphNumberingLevel.Trim();

            if (!string.IsNullOrEmpty(location) && !string.IsNullOrEmpty(format))
            {
                try
                {
                    var section = activeDocument.Range(0, 0);
                    if (location != LanguageManager.GetTranslation(LanguageConstants.EndOfDocument, "End of Document"))
                    {
                        section = AllAddendums[location].section.Previous(WdUnits.wdCharacter);
                        section.InsertBreak(WdBreakType.wdSectionBreakNextPage);
                    }
                    else
                    {
                        section = activeDocument.Sections.Add().Range;
                    }

                    DisableLinkToPrevious(section.Sections.First);

                    RestartPageNumberingIfNeeded(section.Sections.First);

                    section.Paragraphs.Add();

                    if (AddendaOptions.QuotationIsChecked == true)
                    {
                        format = "“" + format + "”";
                    }

                    var addendaContentControl = section.ContentControls.Add();
                    addendaContentControl.Title = CustomConstants.ADDENDA_CONTENT_CONTROL_TITLE;
                    addendaContentControl.Tag = CustomConstants.ADDENDA_TYPE_SCHEDULE_TITLE;
                    addendaContentControl.Range.Text = CustomConstants.ADDENDA_TYPE_SCHEDULE_TITLE + " 1 [ Title ]";

                    try
                    {
                        var addendaInnerContentControl = section.Words[3].ContentControls.Add();
                        addendaInnerContentControl.Title = CustomConstants.ADDENDA_TYPE_NUMBERING_SCHEDULE_TITLE;
                        addendaInnerContentControl.Range.Text = format;
                        section.Paragraphs[1].set_Style(GetOrCreateAddendaStyle());

                        addendaContentControl.Range.Words[addendaContentControl.Range.Words.Count - 2].set_Style(PromptStyle);
                        addendaContentControl.Range.Words[addendaContentControl.Range.Words.Count - 1].set_Style(PromptStyle);
                        addendaContentControl.Range.Words[addendaContentControl.Range.Words.Count].set_Style(PromptStyle);
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex);
                    }

                    if (section.Paragraphs.Count == 1)
                    {
                        section.Paragraphs.Add();
                    }

                    section.Paragraphs[2].set_Style(Utils.GetLocalizedStyleNameFromBuiltInStyle(WdBuiltinStyle.wdStyleNormal));

                    string bookmarkName = CustomConstants.ADDENDA_BOOKMARK_PREFIX + Math.Abs(new Random().Next());

                    section.Bookmarks.Add(bookmarkName);

                    AddPageNumbering(section.Sections.First);
                    if (CrossReferenceCursor.IsChecked == true)
                    {
                        InsertNumericXRef(format, bookmarkName);
                    }

                    try
                    {
                        AdminPanelWebApi.UpdateCustomSetting(Utils.STATIC_PROCESSOR_ID, INSERT_CROSS_REF_SETTING, (CrossReferenceCursor.IsChecked == true).ToString());
                    }
                    catch { }

                    Utils.UpdateAddendaCrossReferences();
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex);
                }
            }
        }

        private void InsertNumericXRef(string format, string bookmarkName)
        {
            if (!string.IsNullOrEmpty(format) && !string.IsNullOrEmpty(bookmarkName))
            {
                Range selection = ThisAddIn.Instance.Application.ActiveWindow.Selection.Range;
                selection.Collapse(WdCollapseDirection.wdCollapseEnd);

                bool insertSpaceAtStart = false;
                bool insertSpaceAtEnd = false;

                Range selectionDuplicate = selection.Duplicate;

                selectionDuplicate.Collapse(WdCollapseDirection.wdCollapseStart);
                selectionDuplicate.MoveStart(WdUnits.wdCharacter, -1);
                string firstCharacterMinusOne = selectionDuplicate.Text;
                if (!string.IsNullOrWhiteSpace(firstCharacterMinusOne))
                {
                    insertSpaceAtStart = true;
                }

                selectionDuplicate.Collapse(WdCollapseDirection.wdCollapseEnd);
                selectionDuplicate.MoveEnd(WdUnits.wdCharacter, 1);
                string lastCharacterPlusOne = selectionDuplicate.Text;
                if (!string.IsNullOrWhiteSpace(lastCharacterPlusOne))
                {
                    insertSpaceAtEnd = true;
                }

                ThisAddIn.Instance.Application.ActiveDocument.Bookmarks.Add(CustomConstants.ADDENDA_BOOKMARK_SOURCE_PARAGRAPH + bookmarkName, selection.Paragraphs.First.Range);

                selection.Text = CustomConstants.ADDENDA_TYPE_SCHEDULE_TITLE + " " + format;

                ThisAddIn.Instance.Application.ActiveDocument.Hyperlinks.Add(selection, '#' + bookmarkName);
                selection.set_Style(Utils.GetOrCreateAddendaCrossReferenceCharacterStyle());

                if (insertSpaceAtStart)
                {
                    selection.InsertBefore(" ");
                }

                if (insertSpaceAtEnd)
                {
                    selection.Collapse(WdCollapseDirection.wdCollapseEnd);
                    selection.MoveEnd(WdUnits.wdCharacter);
                    selection.InsertAfter(" ");
                }
            }
        }

    }
}
