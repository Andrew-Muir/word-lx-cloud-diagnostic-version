﻿using InfowareVSTO.Addenda.Model;
using InfowareVSTO.Common;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using Word = Microsoft.Office.Interop.Word;

namespace InfowareVSTO.Addenda
{
    /// <summary>
    /// Interaction logic for AddendaXRef.xaml
    /// </summary>
    public partial class AddendaXRef : InfowareWindow
    {
        public class ForAllSectionClass
        {
            public string newtitle = null;
            public string name = null;
            public Microsoft.Office.Interop.Word.Range range = null;

            public Word.Bookmark bookmark { get; set; }
        }
        public class AllOfAddendumClass
        {
            private List<ForAllSectionClass> all_sections = new List<ForAllSectionClass>();

            public ForAllSectionClass this[string i]
            {
                get
                {
                    foreach (ForAllSectionClass aux in all_sections)
                    {
                        if (i == aux.name)
                        {
                            return aux;
                        }
                    }
                    return null;
                }
                set
                {
                    value.name = i;
                    all_sections.Add(value);
                }
            }
        }

        public AllOfAddendumClass InitAddendums = new AllOfAddendumClass();

        public AddendaXRef()
        {
            InitializeComponent();
            Microsoft.Office.Interop.Word.Document Document = ThisAddIn.Instance.Application.ActiveDocument;

            foreach (Microsoft.Office.Interop.Word.ContentControl contentControl in Document.ContentControls)
            {
                if ((contentControl.Title == CustomConstants.ADDENDA_CONTENT_CONTROL_TITLE) && contentControl.Range.Paragraphs[1] != null)
                {
                    Microsoft.Office.Interop.Word.Range par = contentControl.Range.Paragraphs[1].Range;
                    Word.Range sectionRange = par.Sections[1].Range;
                    Word.Bookmark bm = null;
                    if (sectionRange.Bookmarks.Count > 0)
                    {
                        bm = sectionRange.Bookmarks[1];
                    }
                    
                    ForAllSectionClass forAllSectionClass = new ForAllSectionClass();
                    ListBoxItem title_Text = new ListBoxItem();
                    string title = par.Text.Replace("\r", "").Replace("\n", "").Replace("\t", "").Replace("\f", "");
                    title_Text.Content = title;
                    title_Text.AllowDrop = true;
                    ListOfAddendums.Items.Add(title_Text);
                    forAllSectionClass.range = par;
                    forAllSectionClass.bookmark = bm;
                    forAllSectionClass.newtitle = title;
                    if (contentControl.Range.ContentControls[1] != null)
                    {
                        Microsoft.Office.Interop.Word.ContentControl miniControl = contentControl.Range.ContentControls[1];
                        forAllSectionClass.newtitle = TrimNumberingEnd(miniControl.Title) +" "+ miniControl.Range.Text;
                    }
                    InitAddendums[title] = forAllSectionClass;
                }
            }
        }

        private string TrimNumberingEnd(string title)
        {
            if (title != null && title.EndsWith(" numbering"))
            {
                return title.Substring(0, title.Length - " numbering".Length);
            }

            return title;
        }

        private void OK_Click(object sender, RoutedEventArgs e)
        {
            if(ListOfAddendums.SelectedItem == null)
            {
                return;
            }
            object listForMove = (ListOfAddendums.SelectedItem as ListBoxItem).Content;

            Microsoft.Office.Interop.Word.Range range = ThisAddIn.Instance.Application.ActiveWindow.Selection.Range;
            range.Collapse(Microsoft.Office.Interop.Word.WdCollapseDirection.wdCollapseEnd);

            Microsoft.Office.Interop.Word.Range duplicate = range.Duplicate;
            duplicate.MoveStart(Word.WdUnits.wdCharacter, -1);
            if (string.IsNullOrWhiteSpace(duplicate.Text))
            {
                range.Text = InitAddendums[(string)listForMove].newtitle;
            }
            else
            {
                range.Text = ' ' + InitAddendums[(string)listForMove].newtitle;
            }

            if (InitAddendums[(string)listForMove].bookmark != null)
            {
                ThisAddIn.Instance.Application.ActiveDocument.Hyperlinks.Add(range, "#" + InitAddendums[(string)listForMove].bookmark.Name);
            }

            range.set_Style(Utils.GetOrCreateAddendaCrossReferenceCharacterStyle());
            range.Collapse(Microsoft.Office.Interop.Word.WdCollapseDirection.wdCollapseEnd);
            range.Select();
          
            range.Fields.Update();

            if (ReNumberAddenda.IsChecked != null && ReNumberAddenda.IsChecked == true)
            {
                Utils.UpdateAddendaIndexes();
                Utils.UpdateAddendaCrossReferences();
            }

            Close();
        }

        private string ReadDocumentProperty(string propertyName)
        {
            Microsoft.Office.Core.DocumentProperties properties;
            properties = (Microsoft.Office.Core.DocumentProperties)ThisAddIn.Instance.Application.ActiveDocument.CustomDocumentProperties;

            foreach (Microsoft.Office.Core.DocumentProperty prop in properties)
            {
                if (prop.Name == propertyName)
                {
                    return prop.Value.ToString();
                }
            }
            return null;
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void UpClick(object sender, RoutedEventArgs e)
        {
            // only if the first item isn't the current one
            int i = ListOfAddendums.SelectedIndex;
            if (i - 1 >= 0 && ListOfAddendums.Items.Count > i)
            {
                ListBoxItem val = (ListOfAddendums.SelectedValue as ListBoxItem);
                ListBoxItem aux = new ListBoxItem();
                aux.Content = val.Content;
                aux.IsSelected = val.IsSelected;
                ListOfAddendums.Items.RemoveAt(i);
                ListOfAddendums.Items.Insert(i - 1, aux);
            }
        }

        private void DownClick(object sender, RoutedEventArgs e)
        {
            int i = ListOfAddendums.SelectedIndex;
            if (i >= 0 && ListOfAddendums.Items.Count > i + 1)
            {
                ListBoxItem val = (ListOfAddendums.SelectedValue as ListBoxItem);
                ListBoxItem aux = new ListBoxItem();
                aux.Content = val.Content;
                aux.IsSelected = val.IsSelected;
                ListOfAddendums.Items.RemoveAt(i);
                ListOfAddendums.Items.Insert(i + 1, aux);
            }
        }
    }
}
