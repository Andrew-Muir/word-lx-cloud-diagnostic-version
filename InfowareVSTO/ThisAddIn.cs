﻿using System;
using System.Windows;
using System.Collections.Generic;
using System.Linq;
using Word = Microsoft.Office.Interop.Word;
using Office = Microsoft.Office.Core;
using InfowareVSTO.TaskPaneControls.WordDa;
using InfowareVSTO.Windows;
using InfowareVSTO.Windows.ParagraphNumbering;
using Gma.System.MouseKeyHook;
using System.Windows.Forms;
using System.Windows.Input;
using InfowareVSTO.Common.OfflineMode;
using Microsoft.Office.Interop.Word;
using System.IO;
using System.Diagnostics;
using InfowareVSTO.IManage;
using InfowareVSTO.Common;
using InfowareVSTO.Util;
using InfowareVSTO.ComVisibleObject;
using System.Reflection;
using System.Net;
using System.Runtime.InteropServices;

namespace InfowareVSTO
{
    public partial class ComboboxItem
    {
        public string Text
        {
            get;

            set;
        }

        public object Value
        {
            get;

            set;
        }

        public override string ToString()
        {
            return Text;
        }

    }
    public class HeaderPages
    {
        public string HeaderName { get; set; }
        public int Page { get; set; }
    }

    public partial class ThisAddIn
    {
        private const int STARTUP_FLAG_AVAILABILITY = 2000;
        public Ribbon Ribbon { get; set; }

        public static bool DoNotOverWriteMPSettings { get; set; } = false;
        public List<string> TmpTemplatesToDelete;
        public bool UnwantedUnfocus = false;
        private bool doNativeDocIdAutoUpdateForThisDoc = false; // flag for handling active document
        private bool startupOccurred = false;
        public string currentSelectedListTemplateName;

        // logging purposes
        private List<string> loggingFunctions;

        private List<string> enabledEvents;

        // return true to use native Word LX handling for DocID autoupdate
        // for DMS environments, use other methods
        private bool DoNativeDocIdAutoUpdateForThisDoc()
        {
            try
            { 
                if (IsFunctionForLogging("DocID"))
                    Logger.Log($"Enter DoNativeDocIdAutoUpdateForThisDoc(): {Application?.ActiveDocument?.Name}/{Application?.ActiveDocument?.DocID}");

                if (InfowareDocIdWindow.SkipDocIdAutoUpdateForThisDoc())
                {
                    if (IsFunctionForLogging("DocID"))
                        Logger.Log($"Skip DocID autoupdate for this doc - Exit DoNativeDocIdAutoUpdateForThisDoc(): {Application?.ActiveDocument?.Name}/{Application?.ActiveDocument?.DocID}");
                    return false;
                }

                if (InfowareDocIdWindow.UsesIManage())
                {
                    if (IsFunctionForLogging("DocID"))
                        Logger.Log($"This is a DMS doc - Exit DoNativeDocIdAutoUpdateForThisDoc(): {Application?.ActiveDocument?.Name}/{Application?.ActiveDocument?.DocID}");
                    return false;
                }

                if (!SettingsManager.GetSettingAsBool("DocIDFooterPathAutoAddOnSave", "EventClass", true))
                {
                    if (IsFunctionForLogging("DocID"))
                        Logger.Log($"Company autoinsert setting is disabled - Exit DoNativeDocIdAutoUpdateForThisDoc(): {Application?.ActiveDocument?.Name}/{Application?.ActiveDocument?.DocID}");
                    return false;
                }

                if (IsFunctionForLogging("DocID"))
                    Logger.Log($"Do DocID autoinsert for this doc - Exit DoNativeDocIdAutoUpdateForThisDoc(): {Application?.ActiveDocument?.Name}/{Application?.ActiveDocument?.DocID}");
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }


        private static ThisAddIn instance;
        public static Microsoft.Office.Interop.Word.Document FirmListDocument;

        public IMultiPrintSettings MPSettings { get; set; }
        
        // used for caching ActivePrinter prior to MultiPrint calls
        public string PreMultiPrintActivePrinterName;

        // private Microsoft.Office.Tools.CustomTaskPane CustomTaskPane1;

        //  private AddendaInsert usercontrol;
        private IIManageUtilities iManageUtilities;
        public IIManageUtilities IManageUtilities
        {
            get
            {
                if (iManageUtilities == null)
                {
                    iManageUtilities = new IManageUtilities();
                }

                return iManageUtilities;
            }
        }

        public IComVisibleObject ComVisibleObject { get; set; }

        protected override object RequestComAddInAutomationService()
        {
            if (ComVisibleObject == null)
            {
                ComVisibleObject = new ComVisibleObject.ComVisibleObject();
            }

            return ComVisibleObject;
        }

        public static ThisAddIn Instance
        {
            get
            {
                return instance;   // returns the called object
            }
        }

        protected override Office.IRibbonExtensibility CreateRibbonExtensibilityObject()
        {
            this.Ribbon = new Ribbon();
            return this.Ribbon;
        }

        private IKeyboardMouseEvents m_GlobalHook;
        public bool ControlPressed
        {
            get
            {
                return Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl);
            }
        }

        public bool ShiftPressed
        {
            get
            {
                return Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.RightShift);
            }
        }

        public bool AltPressed
        {
            get
            {
                return Keyboard.IsKeyDown(Key.LeftAlt) || Keyboard.IsKeyDown(Key.RightAlt);
            }
        }

        public void Subscribe()
        {
            // Note: for the application hook, use the Hook.AppEvents() instead
            m_GlobalHook = Hook.AppEvents();
            m_GlobalHook.KeyDown += GlobalHookKeyPress;
        }


        private void GlobalHookKeyPress(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            // languages where Ctrl+; are needed for special characters
            // so have to remap this shortcut for Contact Search
            List<WdLanguageID> alternateKeyboardLanguages = new List<WdLanguageID>()
            {
                WdLanguageID.wdDutch,
                WdLanguageID.wdFrench,
                WdLanguageID.wdGerman,
                WdLanguageID.wdItalian,
                WdLanguageID.wdPolish
            };

            if (ControlPressed || AltPressed)
            {
                //////////
                // Ctrl + Alt + key shortcuts
                //////////
                if (AltPressed)
                {
                    // Ctrl + Alt + Num 0 should launch Toggle Letterhead
                    if (e.KeyCode == Keys.NumPad0 && ControlPressed && AltPressed)
                    {
                        e.Handled = true;
                        this.Ribbon.ToggleLetterhead();
                        return;
                    }

                    // Ctrl + Alt + V calls Paste Unformatted
                    if (e.KeyCode == Keys.V && ControlPressed && AltPressed)
                    {
                        e.Handled = true;
                        this.Ribbon.PasteUnformattedText_Click(null);
                        return;
                    }
                }

                //////////
                // Ctrl + Shift + key shortcuts
                //////////
                if (ShiftPressed)
                {
                    // Ctrl + Shift + B should insert a prompt bullet
                    if (e.KeyCode == Keys.B && ControlPressed && ShiftPressed)
                    {
                        e.Handled = true;
                        this.Ribbon.InsertPrompt_Click(null);
                        return;
                    }

                    //Ctrl + Shift + P should Print the current page
                    if (e.KeyCode == Keys.P && ControlPressed && ShiftPressed)
                    {
                        e.Handled = true;
                        this.Ribbon.PrintCurrentPage_Click(null);
                    }

                    // Find Next Prompt - French (France) keyboard layout
                    // Ctrl + Shift + ; in French is the same as Ctrl + . in English
                    if (e.KeyCode == Keys.OemPeriod && ControlPressed && ShiftPressed)
                    {
                        if (this.Ribbon.HasDocumentOpen(null))
                        {
                            IntPtr foregroundWindow = InfowareWindow.GetForegroundWindow();
                            uint foregroundProcess = InfowareWindow.GetWindowThreadProcessId(foregroundWindow, IntPtr.Zero);
                            WdLanguageID keyboardLayout = (WdLanguageID)(InfowareWindow.GetKeyboardLayout(foregroundProcess).ToInt32() & 0xFFFF);
                            if (keyboardLayout == WdLanguageID.wdFrench)
                            {
                                e.Handled = true;
                                this.Ribbon.nextPrompt_Click(null);
                                return;
                            }
                        }
                    }

                    // Ctrl + Shift + ] launches Contact Search - Dutch/French/German/Italian keyboard layout
                    if (e.KeyCode == Keys.Oemcomma && ControlPressed && ShiftPressed)
                    {
                        if (this.Ribbon.HasDocumentOpen(null))
                        {
                            IntPtr foregroundWindow = InfowareWindow.GetForegroundWindow();
                            uint foregroundProcess = InfowareWindow.GetWindowThreadProcessId(foregroundWindow, IntPtr.Zero);
                            WdLanguageID keyboardLayout = (WdLanguageID)(InfowareWindow.GetKeyboardLayout(foregroundProcess).ToInt32() & 0xFFFF);
                            if (alternateKeyboardLanguages.Contains(keyboardLayout))
                            {
                                e.Handled = true;
                                this.Ribbon.OnBtnInsertContacts(null);
                                return;
                            }
                        }
                    }

                    // Ctrl + Shift + 4 launches Contact Search (Russian keyboard layout)
                    if (e.KeyCode == Keys.D4 && ControlPressed && ShiftPressed)
                    {
                        if (this.Ribbon.HasDocumentOpen(null))
                        {
                            IntPtr foregroundWindow = InfowareWindow.GetForegroundWindow();
                            uint foregroundProcess = InfowareWindow.GetWindowThreadProcessId(foregroundWindow, IntPtr.Zero);
                            WdLanguageID keyboardLayout = (WdLanguageID)(InfowareWindow.GetKeyboardLayout(foregroundProcess).ToInt32() & 0xFFFF);
                            if (keyboardLayout == WdLanguageID.wdRussian)
                            {
                                e.Handled = true;
                                this.Ribbon.OnBtnInsertContacts(null);
                                return;
                            }
                        }
                    }
                }

                //////////
                // Regular Ctrl + key shortcuts
                //////////

                // Ctrl + P should launch MultiPrint dialog box
                if (e.KeyCode == Keys.P && ControlPressed)
                {
                    e.Handled = true;
                    this.Ribbon.MultiPrint();
                    return;
                }

                // Ctrl + Num 0 should launch Numbering(Note: Num 0 = the 0 on the numeric keypad)
                if (e.KeyCode == Keys.NumPad0 && ControlPressed)
                {
                    e.Handled = true;
                    this.Ribbon.OnParagraphNumberingButtonClick(null);
                    return;
                }

                // Ctrl + Num 1 should launch Sections
                if (e.KeyCode == Keys.NumPad1 && ControlPressed)
                {
                    e.Handled = true;
                    this.Ribbon.sections_Click(null);
                    return;
                }

                // Ctrl + . should jump the cursor to the next Prompt
                // Contact Search - French (France) keyboard layout
                if (e.KeyCode == Keys.OemPeriod && ControlPressed)
                {
                    IntPtr foregroundWindow = InfowareWindow.GetForegroundWindow();
                    uint foregroundProcess = InfowareWindow.GetWindowThreadProcessId(foregroundWindow, IntPtr.Zero);
                    WdLanguageID keyboardLayout = (WdLanguageID)(InfowareWindow.GetKeyboardLayout(foregroundProcess).ToInt32() & 0xFFFF);
                    if (keyboardLayout == WdLanguageID.wdFrench)
                    {
                        e.Handled = true;
                        this.Ribbon.OnBtnInsertContacts(null);
                        return;
                    }
                    else
                    {
                        e.Handled = true;
                        this.Ribbon.nextPrompt_Click(null);
                        return;
                    }
                }

                // Ctrl + , should jump the cursor to the next Prompt
                if (e.KeyCode == Keys.Oemcomma && ControlPressed)
                {
                    e.Handled = true;
                    this.Ribbon.previousPrompt_Click(null);
                    return;
                }

                // Contact search - English keyboard layout
                if (e.KeyCode == Keys.OemSemicolon && ControlPressed)
                {
                    if (this.Ribbon.HasDocumentOpen(null))
                    {
                        IntPtr foregroundWindow = InfowareWindow.GetForegroundWindow();
                        uint foregroundProcess = InfowareWindow.GetWindowThreadProcessId(foregroundWindow, IntPtr.Zero);
                        WdLanguageID keyboardLayout = (WdLanguageID)(InfowareWindow.GetKeyboardLayout(foregroundProcess).ToInt32() & 0xFFFF);
                        if (!alternateKeyboardLanguages.Contains(keyboardLayout))
                        {
                            // not one of the languages that is trying to type in an actual character, so launch Contact Search
                            e.Handled = true;
                            this.Ribbon.OnBtnInsertContacts(null);
                            return;
                        }
                    }
                }
            }
        }

        public void Unsubscribe()
        {
            m_GlobalHook.KeyDown -= GlobalHookKeyPress;
            //It is recommened to dispose it
            m_GlobalHook.Dispose();
        }
        private void ThisAddIn_Startup(object sender, System.EventArgs e)
        {

            // check which product areas should be enabled.
            string EnabledEventsString = RegistryManager.GetStringRegistryValue("EnabledEvents");

            if (!String.IsNullOrEmpty(EnabledEventsString))
            {
                enabledEvents = EnabledEventsString.Split(',').ToList();
            }
            else 
            {
                //If the registry key is null or empty, we enable all events as this logging feature
                //is considered disabled.

                Logger.Log("EnabledEvents is null");
                
                enabledEvents.Add("Startup");
                enabledEvents.Add("OnDocumentNew");
                enabledEvents.Add("Shutdown");
                enabledEvents.Add("OnDocumentOpen");
                enabledEvents.Add("DocumentBeforeSave");
                enabledEvents.Add("DocumentAfterSave");
            }

            Logger.Log("Enabled Word Events : ");
            foreach(string Event in enabledEvents)
            {
                Logger.Log(Event);
            }


            if (enabledEvents.Contains("Startup"))
            {
                Logger.Log("Enter Startup");
                try
                {
                    instance = this;
                    Logger.Log("Application started");
                    try
                    {
                        if (this.Application.ActiveDocument != null && string.IsNullOrEmpty(this.Application.ActiveDocument.Path))
                        {
                            this.Application.ActiveDocument.Close();

                            bool openBlankOnStartup = SettingsManager.GetGeneralSettingAsBool("OpenBlankOnStartup", false);
                            if (openBlankOnStartup)
                            {
                                Utils.OpenTemplate(Common.TemplateType.BlankDocument, false, true);
                            }
                        }
                    }
                    catch (Exception)
                    {
                    }

                    try
                    {
                        WordSaveHandler wsh = new WordSaveHandler(Application);
                        wsh.AfterUiSaveEvent += new WordSaveHandler.AfterSaveDelegate(wsh_AfterUiSaveEvent);
                    }
                    catch (Exception ex)
                    {
                        Logger.LogException(ex);
                    }
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11;
                    FirmListDocument = null;
                    TmpTemplatesToDelete = new List<string>();
                    ((ApplicationEvents4_Event)Application).NewDocument += OnDocumentNew;
                    Application.DocumentBeforeClose += Application_DocumentBeforeClose;
                    Application.DocumentOpen += OnDocumentOpen;
                    Application.WindowActivate += OnWindowFocused;
                    Application.WindowDeactivate += OnWindowUnfocused;
                    Application.WindowSize += OnApplicationWindowSizeChanged;
                    Application.DocumentBeforeSave += Application_DocumentBeforeSave;
                    Application.DocumentChange += Application_DocumentChange;
                    Subscribe();
                    PreMultiPrintActivePrinterName = Application.ActivePrinter;
                    try
                    {
                        if (SettingsManager.GetSettingAsBool("PreCacheEnabled", "General", true))
                        {
                            PreCacheManager.Instance.UpdateIfNeeded();
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.LogException(ex);
                    }
                    NonWordTemplateManager.CreateTempFolderAndEmpty();

                    // check what function should have logging enabled
                    string loggingFunctionsString = RegistryManager.GetStringRegistryValue("LoggingFunctions");
                    if (loggingFunctionsString != null)
                    {
                        loggingFunctions = loggingFunctionsString.Split(',').ToList();
                    }

                    startupOccurred = true;
                    System.Threading.Tasks.Task.Delay(STARTUP_FLAG_AVAILABILITY).ContinueWith(t =>
                    {
                        startupOccurred = false;
                    });
                }
                catch (Exception ex)
                {
                    Logger.LogException(ex);
                }
                Logger.Log("Exit Startup");
            }
            
        }

        private void Application_DocumentChange()
        {
            if (enabledEvents.Contains("DocumentChange"))
            {
                Logger.Log("Enter Application_DocumentChange");
                //this is used for double click on word file.
                if (startupOccurred)
                {
                    try
                    {
                        OnDocumentOpen(ThisAddIn.Instance.Application.ActiveDocument);
                    }
                    catch { }
                    startupOccurred = false;

                    this.currentSelectedListTemplateName = null;
                }
                Logger.Log("Exit Application_DocumentChange");
            }
        }

        private void wsh_AfterUiSaveEvent(Word.Document doc, bool isClosed)
        {
            if (doc != null && !isClosed && doNativeDocIdAutoUpdateForThisDoc)
            {
                doc.Save();
                doNativeDocIdAutoUpdateForThisDoc = false; // reset flag
            }
        }

        private void Application_DocumentBeforeSave(Document Doc, ref bool SaveAsUI, ref bool Cancel)
        {
            if (enabledEvents.Contains("DocumentBeforeSave"))
            {
                Logger.Log("Enter Application_DocumentBeforeSave");
                if (IsFunctionForLogging("DocID"))
                    Logger.Log($"Enter Application_DocumentBeforeSave(): {Doc?.Name}/{Doc?.DocID}");

                // only do DocID handling if enabled
                if (SettingsManager.GetSettingAsBool("NoDocIDSupport", "Tools", false))
                {
                    if (Globals.ThisAddIn.IsFunctionForLogging("DocID"))
                        Logger.Log($"DocID support disabled: {Doc?.Name}/{Doc?.DocID}");
                    return;
                }

                object oBasic = Application.WordBasic;
                // this is where we invoke the object and
                // get the property. But we get an "object"
                // back so be careful casting it.
                object fIsAutoSave =
                    oBasic.GetType().InvokeMember(
                        "IsAutosaveEvent",
                        BindingFlags.GetProperty,
                        null, oBasic, null);
                if (int.Parse(fIsAutoSave.ToString()) != 1)
                {
                    doNativeDocIdAutoUpdateForThisDoc = DoNativeDocIdAutoUpdateForThisDoc();
                    if (doNativeDocIdAutoUpdateForThisDoc && !Doc.ReadOnly)
                    {
                        if (!SaveAsUI)
                        {
                            string applications = SettingsManager.GetSetting("AppsToSkipCompatibilityCheck", "EventClass", null);

                            if (applications == null || !Utils.GetIsAnyProcessRunning(applications.Split(',').Select(x => x.Trim()).ToArray()))
                            {
                                var protectionManager = new ProtectionManager(Doc);
                                protectionManager.UnprotectIfNeeded();
                                InfowareDocIdWindow infowareDocIdWindow = new InfowareDocIdWindow();
                                infowareDocIdWindow.UseDateTimeNow = true;
                                infowareDocIdWindow.RefreshDocumentId(true);
                                protectionManager.ReProtectIfNeeded();
                            }
                        }
                    }
                }

                if (IsFunctionForLogging("DocID"))
                    Logger.Log($"Exit Application_DocumentBeforeSave(): {Doc?.Name}/{Doc?.DocID}");

                Logger.Log("Exit Application_DocumentBeforeSave");
            }
        }

        private void Application_DocumentBeforeClose(Word.Document Doc, ref bool Cancel)
        {
            if (enabledEvents.Contains("DocumentBeforeClose"))
            {

                Logger.Log("Enter Application_DocumentBeforeClose");
                
                if (ThisAddIn.Instance.Application.Documents != null)
                {
                    try
                    {
                        if (ThisAddIn.Instance.Application.Documents.Count == 2 && FirmListDocument != null)
                        {
                            if (!FirmListDocument.Saved) { FirmListDocument.Saved = true; }
                            FirmListDocument.Close(WdSaveOptions.wdDoNotSaveChanges);
                            FirmListDocument = null;
                        }

                        TaskPaneUtil2.CloseTaskpanesOfDocument(Doc);
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex);
                    }
                }

                this.currentSelectedListTemplateName = null;
                this.Ribbon.Invalidate();

                Logger.Log("Exit Application_DocumentBeforeClose");
            }
        }

        private void OnApplicationWindowSizeChanged(Word.Document doc, Word.Window window)
        {
            ParagraphNumberingToolbarManager.Instance.OnWordWindowSizeChanged(doc, window);
            ParagraphNumberingToolbarManager.Instance.RestoreWindowIfNecessary();
        }

        private void OnWindowFocused(Word.Document doc, Word.Window window)
        {

            ParagraphNumberingToolbarManager.Instance.SetToolbarOwner(window);
            ParagraphNumberingToolbarManager.Instance.RestoreWindowIfNecessary();
            ListTemplateUtil.ExtractParagraphNumberingSchemesFromDocument(doc);
        }

        private void OnWindowUnfocused(Word.Document doc, Word.Window window)
        {
            if (UnwantedUnfocus == true)
                UnwantedUnfocus = false;
            else
                ParagraphNumberingToolbarManager.Instance.EnsureWindowIsHidden();
        }

        public void OnDocumentNew(Word.Document document)
        {
            if (enabledEvents.Contains("OnDocumentNew"))
            {
                Logger.Log("Enter OnDocumentNew");

                string applications = SettingsManager.GetSetting("AppsToAllowUseOfNormalTemplate", "EventClass", null);

                if (!(applications != null && Utils.GetIsAnyProcessRunning(applications.Split(',').Select(x => x.Trim()).ToArray())))
                {
                    if (AdminPanelWebApi.AssureLogin(2) != null)
                    {
                        bool documentDeleted = false;
                        try
                        {
                            var temp = document.Content;
                        }
                        catch
                        {
                            documentDeleted = true;
                        }

                        if ((documentDeleted || string.IsNullOrWhiteSpace(document.Content.Text)) && Ribbon.OpenTemplateCalled == false)
                        {
                            if (!documentDeleted)
                            {
                                document.Close();
                            }

                            Utils.OpenTemplate(Common.TemplateType.BlankDocument, false);
                        }

                        Ribbon.OpenTemplateCalled = false;
                    }
                }

                this.Ribbon.Invalidate();
                Logger.Log("Exit OnDocumentNew");
            }
        }

        private void OnDocumentOpen(Word.Document document)
        {
            if (enabledEvents.Contains("OnDocumentOpen"))
            {
                Logger.Log("Enter OnDocumentOpen");

                if (IsFunctionForLogging("DocID"))
                    Logger.Log($"Enter OnDocumentOpen(): {document?.Name}/{document?.DocID}");

                string applications = SettingsManager.GetSetting("AppsToSkipCompatibilityCheck", "EventClass", null);

                if (applications == null || !Utils.GetIsAnyProcessRunning(applications.Split(',').Select(x => x.Trim()).ToArray()))
                {
                    try
                    {
                        ClauseBuilderUserControl.OnDocumentOpen(document);
                    }
                    catch { }
                    if (!OutlookSourceFinder.DocumentIsOutlookAttachment(document.Path) && !document.ReadOnly && SettingsManager.GetSettingAsBool("UpdateDocIDOnOpen", "EventClass", false))
                    {
                        var protectionManager = new ProtectionManager(document);
                        protectionManager.UnprotectIfNeeded();

                        if (IsFunctionForLogging("DocID"))
                            Logger.Log($"Launch InfowareDocIdWindow: {document?.Name}/{document?.DocID}");
                        InfowareDocIdWindow infowareDocIdWindow = new InfowareDocIdWindow();
                        infowareDocIdWindow.UpdateDocIdOnOpen(true);
                        if (IsFunctionForLogging("DocID"))
                            Logger.Log($"Return from UpdateDocIdOnOpen: {document?.Name}/{document?.DocID}");

                        protectionManager.ReProtectIfNeeded();
                    }
                }
                ListTemplateUtil.ExtractParagraphNumberingSchemesFromDocument(document);
                this.Ribbon.Invalidate();

                if (IsFunctionForLogging("DocID"))
                    Logger.Log($"Exit OnDocumentOpen(): {document?.Name}/{document?.DocID}");

                Logger.Log("Exit OnDocumentOpen");
            }
        }

        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {
            if (enabledEvents.Contains("Shutdown"))
            {
                Logger.Log("Enter Shutdown");
                try
                {
                    Unsubscribe();
                    DeleteTmpTemplates();
                }
                catch (Exception ex)
                {
                    Logger.LogException(ex);
                }
                Logger.Log("Exit Shutdown");
            }
        }

        private void DeleteTmpTemplates()
        {
            try
            {
                if (TmpTemplatesToDelete.Count() > 0)
                {
                    foreach (var tmpTemplatePath in TmpTemplatesToDelete)
                    {
                        if (File.Exists(tmpTemplatePath))
                        {
                            File.Delete(tmpTemplatePath);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        internal bool IsFunctionForLogging(string function)
        {
            return (loggingFunctions?.Contains(function) == true);
        }

        internal bool IsEventEnabled(string function)
        {
            return (enabledEvents?.Contains(function) == true);
        }

        #region VSTO generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
        }

        #endregion
    }
}
