﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Runtime.InteropServices;
using Word = Microsoft.Office.Interop.Word;
using Newtonsoft.Json.Linq;
using InfowareVSTO.Common;
using InfowareVSTO.Util;
using Microsoft.Office.Interop.Word;
using InfowareVSTO.Windows;
using System.Windows.Forms;

namespace InfowareVSTO.IManage
{
    public interface IIManageUtilities
    {
        void AddDocumentID(bool skipSave);
        void AddDocumentIdOnOpen();
    }

    public class IManageUtilities : IIManageUtilities
    {
        // This method tries to write a string to cell A1 in the active worksheet. 
        public void AddDocumentID(bool skipSave)
        {
            Document document = ThisAddIn.Instance.Application.ActiveDocument;

            if (document.ReadOnly)
            {
                return;
            }

            var protectionManager = new ProtectionManager(document);
            try
            {
                protectionManager.UnprotectIfNeeded();
                RefreshDocumentId();
                protectionManager.ReProtectIfNeeded();
            }
            catch (Exception ex)
            {
                new InfowareInformationWindow($"{ex.Message}\n\n{ex.StackTrace}", "DMS AddDocumentId()", WordDefaultIconType.Error).ShowDialog();
            }
        }

        private void RefreshDocumentId()
        {
            try
            {
                InfowareDocIdWindow docIdWindow = new InfowareDocIdWindow();
                bool autoAdd = SettingsManager.GetSettingAsBool("AutoAddDocIdOnSave", "DocumentManagement", SettingsManager.GetSettingAsBool("AutoAddDocIdOnSave", "IManage", true));
                string docId = GetDocumentID(docIdWindow.GetIManageSettings());
                docIdWindow.RefreshDocumentId(autoAdd);
            }
            catch (Exception ex)
            {
                new InfowareInformationWindow($"{ex.Message}\n\n{ex.StackTrace}", "DMS RefreshDocumentId()", WordDefaultIconType.Error).ShowDialog();
            }
        }

        public void AddDocumentIdOnOpen()
        {
            Document document = ThisAddIn.Instance.Application.ActiveDocument;
            if (document.ReadOnly)
            {
                return;
            }

            try
            {
                var protectionManager = new ProtectionManager(document);
                protectionManager.UnprotectIfNeeded();

                //MessageBox.Show("It works (open).");
                bool updateDocId = SettingsManager.GetSettingAsBool("UpdateDocumentIdOnOpen", "DocumentManagement", SettingsManager.GetSettingAsBool("UpdateDocumentIdOnOpen", "IManage", false));
                if (updateDocId)
                {
                    RefreshDocumentId();
                }
                protectionManager.ReProtectIfNeeded();
            }
            catch (Exception ex)
            {
                new InfowareInformationWindow($"{ex.Message}\n\n{ex.StackTrace}", "DMS AddDocumentIdOnOpen()", WordDefaultIconType.Error).ShowDialog();
            }
        }

        public static void SaveSettings(IManageSettings settings)
        {
            string json = JObject.FromObject(settings).ToString();

            Microsoft.Win32.Registry.SetValue(ConfigSettings.InfowareKeyFolder, "IManageSettings", json);
        }

        public static IManageSettings LoadSettings()
        {
            string str = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "IManageSettings", null) as string;

            if (str != null)
            {
                try
                {
                    return JObject.Parse(str).ToObject<IManageSettings>();
                }
                catch
                {
                }
            }

            return null;
        }

        public static string GetDocumentID(IManageSettings settings)
        {
            if (settings == null)
            {
                settings = new IManageSettings();
            }

            string varName, number = null, version = null, databaseName = null, authorId = null, clientId = null, matter = null, description = null, lastSavedDate = null;
            string numberPrefix = null, versionPrefix = null, databaseNamePrefix = null, authorIdPrefix = null, clientIdPrefix = null, matterPrefix = null, descriptionPrefix = null, lastSavedDatePrefix = null;
            bool numberUseAsSuffix = false, versionUseAsSuffix = false, databaseNameUseAsSuffix = false, authorIdUseAsSuffix = false, clientIdUseAsSuffix = false, matterUseAsSuffix = false, descriptionUseAsSuffix = false, lastSavedDateUseAsSuffix = false;

            try
            {
                numberPrefix = SettingsManager.GetSetting("DocumentNumberPrefix", "DocumentManagement", SettingsManager.GetSetting("DocumentNumberPrefix", "IManage", ""));
                versionPrefix = SettingsManager.GetSetting("DocumentVersionPrefix", "DocumentManagement", SettingsManager.GetSetting("DocumentVersionPrefix", "IManage", ""));
                databaseNamePrefix = SettingsManager.GetSetting("DatabaseNamePrefix", "DocumentManagement", SettingsManager.GetSetting("DatabaseNamePrefix", "IManage", ""));
                authorIdPrefix = SettingsManager.GetSetting("AuthorIdPrefix", "DocumentManagement", SettingsManager.GetSetting("AuthorIdPrefix", "IManage", ""));
                clientIdPrefix = SettingsManager.GetSetting("ClientIdPrefix", "DocumentManagement", SettingsManager.GetSetting("ClientIdPrefix", "IManage", ""));
                matterPrefix = SettingsManager.GetSetting("MatterPrefix", "DocumentManagement", SettingsManager.GetSetting("MatterPrefix", "IManage", ""));
                descriptionPrefix = SettingsManager.GetSetting("DocumentDescriptionPrefix", "DocumentManagement", SettingsManager.GetSetting("DocumentDescriptionPrefix", "IManage", ""));
                lastSavedDatePrefix = SettingsManager.GetSetting("LastSavedDatePrefix", "DocumentManagement", SettingsManager.GetSetting("LastSavedDatePrefix", "IManage", ""));

                numberUseAsSuffix = SettingsManager.GetSettingAsBool("DocumentNumberUseAsSuffix", "DocumentManagement", SettingsManager.GetSettingAsBool("DocumentNumberUseAsSuffix", "IManage", false));
                versionUseAsSuffix = SettingsManager.GetSettingAsBool("DocumentVersionUseAsSuffix", "DocumentManagement", SettingsManager.GetSettingAsBool("DocumentVersionUseAsSuffix", "IManage", false));
                databaseNameUseAsSuffix = SettingsManager.GetSettingAsBool("DatabaseNameUseAsSuffix", "DocumentManagement", SettingsManager.GetSettingAsBool("DatabaseNameUseAsSuffix", "IManage", false));
                authorIdUseAsSuffix = SettingsManager.GetSettingAsBool("AuthorIdUseAsSuffix", "DocumentManagement", SettingsManager.GetSettingAsBool("AuthorIdUseAsSuffix", "IManage", false));
                clientIdUseAsSuffix = SettingsManager.GetSettingAsBool("ClientIdUseAsSuffix", "DocumentManagement", SettingsManager.GetSettingAsBool("ClientIdUseAsSuffix", "IManage", false));
                matterUseAsSuffix = SettingsManager.GetSettingAsBool("MatterUseAsSuffix", "DocumentManagement", SettingsManager.GetSettingAsBool("MatterUseAsSuffix", "IManage", false));
                descriptionUseAsSuffix = SettingsManager.GetSettingAsBool("DocumentDescriptionUseAsSuffix", "DocumentManagement", SettingsManager.GetSettingAsBool("DocumentDescriptionUseAsSuffix", "IManage", false));
                lastSavedDateUseAsSuffix = SettingsManager.GetSettingAsBool("LastSavedDateUseAsSuffix", "DocumentManagement", SettingsManager.GetSettingAsBool("LastSavedDateUseAsSuffix", "IManage", false));

                bool documentWasSaved = DocumentUtil.TryGetDateLastSaved(out DateTime dateLastSaved);
                if (documentWasSaved)
                {
                    string lastSavedDateFormat = SettingsManager.GetSetting("DocIDLastSavedDateFormat", "Tools");
                    try
                    {
                        if (lastSavedDateFormat == null)
                        {
                            throw new FormatException();
                        }

                        lastSavedDate = dateLastSaved.ToString(lastSavedDateFormat);
                    }
                    catch
                    {
                        lastSavedDate = dateLastSaved.ToLongDateString();
                    }
                }
            }
            catch (Exception ex)
            {
                new InfowareInformationWindow($"Error loading DocID settings\n\n{ex.Message}\n\n{ex.StackTrace}", "DMS GetDocmentID()", WordDefaultIconType.Error).ShowDialog();
            }

            try
            {
                if (Globals.ThisAddIn.Application.ActiveDocument?.Variables != null)
                {
                    foreach (Variable variable in Globals.ThisAddIn.Application.ActiveDocument.Variables)
                    {
                        if (variable.Name.StartsWith(InfowareDocIdWindow.IMANAGE_VAR_PREFIX))
                        {
                            varName = variable.Name.Substring(InfowareDocIdWindow.IMANAGE_VAR_PREFIX.Length + 1, variable.Name.Length - InfowareDocIdWindow.IMANAGE_VAR_PREFIX.Length - 2);
                            switch (varName)
                            {
                                case "DocumentNumber":
                                    number = variable.Value;
                                    break;
                                case "DocumentVersion":
                                    version = variable.Value;
                                    break;
                                case "DatabaseName":
                                    databaseName = variable.Value;
                                    break;
                                case "AuthorId":
                                    authorId = variable.Value;
                                    break;
                                case "ClientId":
                                    clientId = variable.Value;
                                    break;
                                case "Matter":
                                    matter = variable.Value;
                                    break;
                                case "DocumentDescription":
                                    description = variable.Value;
                                    break;
                            }
                        }
                    }

                    if (clientId != null && matter != null && matter.StartsWith(clientId) && matter.Length > clientId.Length + 1 && matter[clientId.Length] == '.')
                    {
                        matter = matter.Substring(clientId.Length + 1);
                    }

                    string defaultPositions = "[DOCUMENTNUMBER][DOCUMENTVERSION][DATABASENAME][AUTHORID][CLIENTID][MATTER][DOCUMENTDESCRIPTION][LASTSAVEDDATE]";
                    string numberStr = numberUseAsSuffix ? number + numberPrefix : numberPrefix + number;
                    string versionStr = versionUseAsSuffix ? version + versionPrefix : versionPrefix + version;
                    string databaseNameStr = databaseNameUseAsSuffix ? databaseName + databaseNamePrefix : databaseNamePrefix + databaseName;
                    string authorIdStr = authorIdUseAsSuffix ? authorId + authorIdPrefix : authorIdPrefix + authorId;
                    string clientIdStr = clientIdUseAsSuffix ? clientId + clientIdPrefix : clientIdPrefix + clientId;
                    string matterStr = matterUseAsSuffix ? matter + matterPrefix : matterPrefix + matter;
                    string descriptionStr = descriptionUseAsSuffix ? description + descriptionPrefix : descriptionPrefix + description;
                    string lastSavedDateStr = lastSavedDateUseAsSuffix ? lastSavedDate + lastSavedDatePrefix : lastSavedDatePrefix + lastSavedDate;

                    string positions = SettingsManager.GetSetting("DocIdFieldPositions", "DocumentManagement", SettingsManager.GetSetting("DocIdFieldPositions", "IManage", defaultPositions));

                    return positions.Replace("[DOCUMENTNUMBER]".ToUpper(), settings.DocumentNumber ? numberStr : string.Empty)
                           .Replace("[DOCUMENTVERSION]".ToUpper(), settings.DocumentVersion ? versionStr : string.Empty)
                           .Replace("[DATABASENAME]".ToUpper(), settings.DatabaseName ? databaseNameStr : string.Empty)
                           .Replace("[AUTHORID]".ToUpper(), settings.AuthorId ? authorIdStr : string.Empty)
                           .Replace("[CLIENTID]".ToUpper(), settings.ClientId ? clientIdStr : string.Empty)
                           .Replace("[MATTER]".ToUpper(), settings.Matter ? matterStr : string.Empty)
                           .Replace("[DOCUMENTDESCRIPTION]".ToUpper(), settings.DocumentDescription ? descriptionStr : string.Empty)
                           .Replace("[LASTSAVEDDATE]".ToUpper(), settings.LastSavedDate ? lastSavedDateStr : string.Empty);
                }
            }
            catch (Exception ex)
            {
                new InfowareInformationWindow($"Error forming DocID\n\n{ex.Message}\n\n{ex.StackTrace}", "DMS GetDocumentID()", WordDefaultIconType.Error).ShowDialog();
            }

            return null;
        }
    }
}
