﻿using InfowareVSTO.Windows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfowareVSTO.IManage
{
    public class IManageSettings
    {
        public bool DocumentNumber { get; set; } = true;
        public bool DocumentVersion { get; set; }
        public bool DatabaseName { get; set; }
        public bool AuthorId { get; set; }
        public bool ClientId { get; set; }
        public bool Matter { get; set; }
        public bool DocumentDescription { get; set; }
        public bool LastSavedDate { get; set; }
        public DocIdSettings Position { get; set; }

        public IManageSettings()
        {
            DocumentNumber = true;
            DocumentVersion = true;
            DocumentDescription = true;
        }
    }
}
