﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfowareVSTO
{
    public class TOCSerializer
    {
        public bool SameTabLeader { get; set; }
        public List<int> TabLeaders { get; set; }
        public List<bool> IncludePageNumber { get; set; }
        public int OutlineLevels { get; set; }
        public bool PreserLineBreaks { get; set; }
        public bool ExcludeSectionBreaks { get; set; }
        public bool AddendaTable { get; set; }
        public bool IncludePageNumberingFirstPage { get; set; }
        public bool RunInHeadings { get; set; }
        public int LineSpacing { get; set; }
        public bool LeftAlignment { get; set; }
        public bool CenterAlignment { get; set; }
        public bool TwoColumn { get; set; }
        public bool Toc1Centered { get; set; }
        public bool NoSpaceBetweenEntriesOfSameLevel { get; set; }
        public List<bool> Bold { get; set; }
        public List<bool> AllCaps { get; set; }
        public List<bool> SpaceAfter12 { get; set; }
        public List<bool> SpaceBefore12 { get; set; }
        public int BeforeEachLevel { get; set; }
        public int AfterEachLevel { get; set; }
        public int RightTab { get; set; }

        public byte[] byteArr { get; set; }

        public TOCSerializer()
        {
            TabLeaders = new List<int>();
            IncludePageNumber = new List<bool>();
            Bold = new List<bool>();
            AllCaps = new List<bool>();
            SpaceAfter12 = new List<bool>();
            SpaceBefore12 = new List<bool>();
        }

        public string Serialize()
        {
            byteArr = new byte[14];
            int position = 0;
            AddBool(SameTabLeader, position);
            position++;
            AddBool(PreserLineBreaks, position);
            position++;
            AddBool(ExcludeSectionBreaks, position);
            position++;
            AddBool(AddendaTable, position);
            position++;
            AddBool(IncludePageNumberingFirstPage, position);
            position++;
            AddBool(RunInHeadings, position);
            position++;
            AddBool(LeftAlignment, position);
            position++;
            AddBool(CenterAlignment, position);
            position++;
            AddBool(TwoColumn, position);
            position++;
            AddBool(Toc1Centered, position);
            position++;
            AddBool(NoSpaceBetweenEntriesOfSameLevel, position);
            position++;

            foreach (bool boolean in IncludePageNumber)
            {
                AddBool(boolean, position);
                position++;
            }

            foreach (bool boolean in Bold)
            {
                AddBool(boolean, position);
                position++;
            }

            foreach (bool boolean in AllCaps)
            {
                AddBool(boolean, position);
                position++;
            }

            foreach (bool boolean in SpaceAfter12)
            {
                AddBool(boolean, position);
                position++;
            }

            foreach (bool boolean in SpaceBefore12)
            {
                AddBool(boolean, position);
                position++;
            }

            AddInt(OutlineLevels, position, 4);
            position += 4;
            AddInt(LineSpacing, position, 2);
            position += 2;
            AddInt(BeforeEachLevel, position, 8);
            position += 8;
            AddInt(AfterEachLevel, position, 8);
            position += 8;
            AddInt(RightTab, position, 8);
            position += 8;

            foreach (int value in TabLeaders)
            {
                AddInt(value, position, 2);
                position+=2;
            }

            return Convert.ToBase64String(byteArr);
        }

        private void AddInt(int value, int position, int nrOfBits)
        {
            List<bool> booleans = new List<bool>();

            var bit = 0b1;
            for (int i = 0; i < nrOfBits; i++)
            {
                booleans.Add((value & bit) > 0);
                bit = bit * 2;
            }

            foreach (bool boolean in booleans)
            {
                AddBool(boolean, position);
                position++;
            }
        }

        private void AddBool(bool value, int position)
        {
            int byteNr = (int)(position / 8);
            int bitNr = (position - byteNr * 8);
            if (value)
            {
                byteArr[byteNr] = (byte)(byteArr[byteNr] + ((int)Math.Pow(2, bitNr)));
            }
        }

        private bool ReadBool(int position)
        {
            int byteNr = (int)(position / 8);
            int bitNr = (position - byteNr * 8);

            return (((ushort)byteArr[byteNr]) & (int)(Math.Pow(2 , bitNr))) > 0;
        }

        private int ReadInt(int position, int nrOfBits)
        {
            List<bool> booleans = new List<bool>();

            for (int i = 0; i < nrOfBits; i++)
            {
                booleans.Add(ReadBool(position));
                position++;
            }

            int result = 0;
            int bit = 0b1;
            foreach(bool boolean in booleans)
            {
                if (boolean)
                {
                    result += bit;
                }

                bit = bit * 2;
            }

            return result;
        }

        public void Deserialize(string input)
        {
            try
            {
                byteArr = Convert.FromBase64String(input);

                int position = 0;
                SameTabLeader = ReadBool(position);
                position++;
                PreserLineBreaks = ReadBool(position);
                position++;
                ExcludeSectionBreaks = ReadBool(position);
                position++;
                AddendaTable = ReadBool(position);
                position++;
                IncludePageNumberingFirstPage = ReadBool(position);
                position++;
                RunInHeadings = ReadBool(position);
                position++;
                LeftAlignment =  ReadBool(position);
                position++;
                CenterAlignment = ReadBool(position);
                position++;
                TwoColumn = ReadBool(position);
                position++;
                Toc1Centered = ReadBool(position);
                position++;
                NoSpaceBetweenEntriesOfSameLevel = ReadBool(position);
                position++;

                for(int i=0; i < 9; i++ )
                {
                    IncludePageNumber.Add(ReadBool(position));
                    position++;
                }

                for (int i = 0; i < 9; i++)
                {
                    Bold.Add(ReadBool(position));
                    position++;
                }

                for (int i = 0; i < 9; i++)
                {
                    AllCaps.Add(ReadBool(position));
                    position++;
                }

                for (int i = 0; i < 9; i++)
                {
                    SpaceAfter12.Add(ReadBool(position));
                    position++;
                }

                for (int i = 0; i < 9; i++)
                {
                    SpaceBefore12.Add(ReadBool(position));
                    position++;
                }

                OutlineLevels = ReadInt(position, 4);
                position += 4;
                LineSpacing=  ReadInt( position, 2);
                position += 2;
                BeforeEachLevel = ReadInt(position, 8);
                position += 8;
                AfterEachLevel = ReadInt(position, 8);
                position += 8;
                RightTab = ReadInt(position, 8);
                position += 8;

                for (int i = 0; i < 9; i++)
                {
                    TabLeaders.Add(ReadInt(position, 2));
                    position+=2;
                }
            }
            catch { }
        }

    }
}
