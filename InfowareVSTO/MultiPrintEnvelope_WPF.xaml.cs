﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Controls;
using InfowareVSTO.Common;
using InfowareVSTO.Common.DTOs;
using InfowareVSTO.Common.General;
using InfowareVSTO.Common.Models;
using InfowareVSTO.MultiLanguage;
using InfowareVSTO.Util;
using InfowareVSTO.VSTOAuthor;
using Microsoft.Office.Interop.Word;

namespace InfowareVSTO
{
    public partial class MultiPrintEnvelope_WPF : InfowareWindow
    {
        private List<SelectedContact> AddedRecipients { get; set; }
        public Document EnvelopeDocument { get; set; }
        public List<TemplateListModel> EnvelopeTemplateList { get; set; }

        public MultiPrintEnvelope_WPF()
        {
            InitializeComponent();

            AddedRecipients = new List<SelectedContact>();

            DataContext = this;
            var currentAuthorSettings = ManageUsers.CurrentAuthorSettings;
            EnvelopeTemplateList = AdminPanelWebApi.GetTemplateList(TemplateType.Envelope, currentAuthorSettings.CompanyId, currentAuthorSettings.LanguageId, currentAuthorSettings.LocationId);
            comboBoxOfficeLocation.ItemsSource = Utils.GetLocationsForCombobox();
            int defaultLocationId = -1;
   
            if (currentAuthorSettings.LocationId.HasValue)
            {
                defaultLocationId = currentAuthorSettings.LocationId.Value;
            }
            else
            {
                int? companyId = currentAuthorSettings.CompanyId ?? DocumentPropertyUtil.GetTemplateCompanyID();

                if (companyId.HasValue)
                {
                    defaultLocationId = AdminPanelWebApi.GetCompanyDetails(companyId.Value).Id;
                }
            }

            if (defaultLocationId >= 0)
            {
                comboBoxOfficeLocation.SelectedValue = defaultLocationId;
            }
            Utils.PopulateComboboxWithTemplateOptions(comboBoxTemplateEnvelopeMultiPrint, EnvelopeTemplateList);
            SettingsManager.PopulateComboboxWithHandlingOptions(comboBoxHandlingEnvelopeMultiPrint);
            SettingsManager.PopulateComboboxWithDeliveryOptions(comboBoxDeliveryEnvelopeMultiPrint);
            LoadAnswers();

            SetDefaultSelectedTemplate();
        }
                
        public void LoadAnswers(PaneDTO paneDTO = null)
        {
            var envelopeDTO = TaskPaneUtil.LoadAnswers(paneDTO);
            if (envelopeDTO != null)
            {
                textBoxFileNumberEnvelopeMultiPrint.Text = envelopeDTO.FileNumber;
                textBoxClientFileNumberEnvelopeMultiPrint.Text = envelopeDTO.ClientFileNumber;
                textBoxInitialsEnvelopeMultiPrint.Text = envelopeDTO.Initials ?? PaneUtils.GetAuthorInitials_Letter(envelopeDTO.Author);
                textBoxRecipientsEnvelopeMultiPrint.Text = envelopeDTO.Recipients;
                comboBoxHandlingEnvelopeMultiPrint.Text = envelopeDTO.Handling;
                comboBoxDeliveryEnvelopeMultiPrint.Text = envelopeDTO.Delivery;
                textBoxAttentionEnvelopeMultiPrint.Text = envelopeDTO.Attention;
                AddedRecipients = envelopeDTO.AddedRecipients ?? AddedRecipients;
                if (envelopeDTO.OfficeLocationId.HasValue)
                {
                    this.comboBoxOfficeLocation.SelectedValue = envelopeDTO.OfficeLocationId.Value;
                }
            }
        }

        public void ButtonOK_Click(object sender, EventArgs e)
        {
            Int32.TryParse(comboBoxTemplateEnvelopeMultiPrint.SelectedValue?.ToString(), out int templateId);
            var templateName = (comboBoxTemplateEnvelopeMultiPrint.SelectedItem as ComboboxItem)?.Text;

            if (templateId != 0 && templateName != null)
            {
                var downloadedTemplate = Utils.DownloadAndGetTemplate(new TemplateListModel { Id = templateId, Name = templateName }, createNew: false);

                if (downloadedTemplate?.Document != null)
                {
                    EnvelopeDocument = downloadedTemplate.Document;

                    DocumentPropertyUtil.SaveShortProperty(CustomConstants.TemplateTypeTags, ((int)TemplateType.Envelope).ToString(), true);
                }
            }

            if (EnvelopeDocument != null)
            {
                var envelopeDTO = new PaneDTO
                {
                    Type = TemplateType.Envelope,
                    FileNumber = textBoxFileNumberEnvelopeMultiPrint.Text,
                    ClientFileNumber = textBoxClientFileNumberEnvelopeMultiPrint.Text,
                    Initials = textBoxInitialsEnvelopeMultiPrint.Text,
                    CompanyId = DocumentPropertyUtil.GetTemplateCompanyID(),
                    Delivery = comboBoxDeliveryEnvelopeMultiPrint.SelectedValue is string ? comboBoxDeliveryEnvelopeMultiPrint.SelectedValue as string : comboBoxDeliveryEnvelopeMultiPrint.Text,
                    Handling = comboBoxHandlingEnvelopeMultiPrint.SelectedValue is string ? comboBoxHandlingEnvelopeMultiPrint.SelectedValue as string : comboBoxHandlingEnvelopeMultiPrint.Text,
                    Attention = textBoxAttentionEnvelopeMultiPrint.Text,
                    Recipients = textBoxRecipientsEnvelopeMultiPrint.Text.Replace("\r\n", "\v"),
                    AddedRecipients = AddedRecipients,
                    OfficeLocationId = comboBoxOfficeLocation.SelectedValue as int?,
                    IncludeContactInformationDeliverySetting = SettingsManager.GetSetting("IncludeContactInformationDelivery", "Letters"),
                    AttentionTranslation = MLanguageUtil.GetResource(MLanguageResourceEnum.Attention)
                };

                PaneUtils.InsertGeneralTemplateDTO(EnvelopeDocument, envelopeDTO, TemplateType.Envelope);
                //TaskPaneUtil.SaveAnswers(envelopeDTO);
                if (this.IsVisible)
                {
                    this.DialogResult = true;
                }
            }
            this.Close();
        }

        private void ButtonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonFindRecipients_Click(object sender, EventArgs e)
        {
            var importContacts_WPF = new ImportContacts_WPF(RecipientType.MultiRow, true);
            importContacts_WPF.ShowDialog();

            var recipients = new List<string>();
            if (importContacts_WPF.ResultOk)
            {
                foreach (var recipient in importContacts_WPF.SelectedContacts)
                {
                    var grouppedLine = new List<string>();
                    var fullName = new List<string>();
                    if (!recipient.FirstName.IsEmpty())
                    {
                        fullName.Add(recipient.FirstName);
                    }
                    if (!recipient.LastName.IsEmpty())
                    {
                        fullName.Add(recipient.LastName);
                    }
                    string fullNameStr = string.Join(" ", fullName.ToArray());
                    if (importContacts_WPF.JobTitleVisibility == JobTitleVisibility.SameRow && !string.IsNullOrEmpty(recipient.JobTitle))
                    {
                        fullNameStr += ", " + recipient.JobTitle;
                    }

                    if (!recipient.IsSelected && fullName.Count > 0)
                    {
                        grouppedLine.Add(fullNameStr);
                    }
                    if (!recipient.IsSelected && !recipient.JobTitle.IsEmpty() && importContacts_WPF.JobTitleVisibility == JobTitleVisibility.NextRow)
                    {
                        grouppedLine.Add(recipient.JobTitle);
                    }

                    var companyName = string.Empty;
                    var companies = recipient.Companies;
                    if (!recipient.IsSelected && companies != null)
                    {
                        var company = companies.OrderBy(x => x.IsDefault).FirstOrDefault();
                        if (company != null && !company.Name.IsEmpty())
                        {
                            grouppedLine.Add(company.Name);
                            companyName = company.Name;
                        }
                    }

                    var address = BaseUtils.GetAuthorAddress(recipient);
                    if (address != null && !string.IsNullOrWhiteSpace(address.Address1))                          
						grouppedLine.Add(string.Join("\r\n", address.LinesFormat));

                    if (recipient.IsSelected)
                    {
                        if (grouppedLine.Count > 0)
                        {
                            textBoxAttentionEnvelopeMultiPrint.Text += BaseUtils.GetAttentionLine(recipient, importContacts_WPF.JobTitleVisibility) + "\n";
                        }
                        else
                        {
                            if (fullName.Count > 0)
                            {
                                grouppedLine.Add(fullNameStr);
                            }

                            if (!recipient.JobTitle.IsEmpty() && importContacts_WPF.JobTitleVisibility == JobTitleVisibility.NextRow)
                            {
                                grouppedLine.Add(recipient.JobTitle);
                            }
                        }
                    }
                    if (grouppedLine.Count > 0)
                    {
                        recipients.Add(string.Join("\r\n", grouppedLine));
                        AddedRecipients.Add(recipient);
                    }
                }
            }
            var recipientsStr = string.Join("\r\n\r\n", recipients);
            if (!string.IsNullOrEmpty(recipientsStr))
            {
                if (textBoxRecipientsEnvelopeMultiPrint.Text.Trim().IsEmpty())
                {
                    textBoxRecipientsEnvelopeMultiPrint.Text = recipientsStr;
                }
                else
                {
                    textBoxRecipientsEnvelopeMultiPrint.Text = textBoxRecipientsEnvelopeMultiPrint.Text + "\r\n\r\n" + recipientsStr;
                }
            }
        }

        private void TextBoxRecipients_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (AddedRecipients.Count > 0)
            {
                var firstRecipientsRow = BaseUtils.GetRecipientTextFirstRow(textBoxRecipientsEnvelopeMultiPrint.Text.Replace("\r\n", "\v"));
                if (firstRecipientsRow != null)
                {
                    var firstRecipient = AddedRecipients.FirstOrDefault();
                    if (firstRecipient != null)
                    {
                        var firstRecipientFullName = firstRecipient.FirstName + " " + firstRecipient.LastName;
                        if (firstRecipientsRow != firstRecipientFullName)
                        {
                            AddedRecipients.Clear();
                        }
                    }
                }
                else
                {
                    AddedRecipients.Clear();
                }
            }
        }

        private void SetDefaultSelectedTemplate()
        {
            var selectedValue = 0;
            var envelopeTemplateName = SettingsManager.GetGeneralSetting("EnvelopePrinterTemplate");
            if(!string.IsNullOrEmpty(envelopeTemplateName) && comboBoxTemplateEnvelopeMultiPrint != null && comboBoxTemplateEnvelopeMultiPrint.ItemsSource != null)
            {
                foreach(ComboboxItem item in comboBoxTemplateEnvelopeMultiPrint.ItemsSource)
                {
                    if(item.Text == envelopeTemplateName)
                    {
                        selectedValue = Convert.ToInt32(item.Value);
                        break;
                    }
                }
            }

            if(selectedValue != 0)
            {
                comboBoxTemplateEnvelopeMultiPrint.SelectedValue = selectedValue;
            }
            else
            {
                comboBoxTemplateEnvelopeMultiPrint.SelectedIndex = 0;
            }
        }

    }
}
