﻿using InfowareVSTO.Windows;
using PrinterApis.WinNative;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace InfowareVSTO
{
	public partial class TestForm : Form
	{
		public TestForm()
		{
			InitializeComponent();
		}
		
		[DllImport("winspool.Drv", EntryPoint = "DocumentPropertiesW", SetLastError = true,
                     ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        private static extern int DocumentProperties(IntPtr hwnd, IntPtr hPrinter,
            [MarshalAs(UnmanagedType.LPWStr)] string pDeviceName,
            IntPtr pDevModeOutput, ref IntPtr pDevModeInput, int fMode);

        [DllImport("kernel32.dll")]
        private static extern bool GlobalFree(IntPtr hMem);

        [DllImport("kernel32.dll")]
        private static extern IntPtr GlobalLock(IntPtr hMem);

        [DllImport("kernel32.dll")]
        private static extern bool GlobalUnlock(IntPtr hMem);

        private void btnInvoke_Click(object sender, EventArgs e)
        {
            PrinterSettings psSettings = new PrinterSettings { PrinterName = cbPrinters.SelectedItem.ToString() + "" };
            IntPtr ipDevMode = psSettings.GetHdevmode(psSettings.DefaultPageSettings);
            IntPtr pDevMode = GlobalLock(ipDevMode);
            int nSize = DocumentProperties(this.Handle, IntPtr.Zero, psSettings.PrinterName, IntPtr.Zero//pDevMode
				, ref pDevMode, 0);
            IntPtr ipDevModeData = Marshal.AllocHGlobal(nSize);
            DocumentProperties(this.Handle, IntPtr.Zero, psSettings.PrinterName, ipDevModeData, ref pDevMode, DEVMODEfModes.DM_IN_BUFFER | DEVMODEfModes.DM_IN_PROMPT | DEVMODEfModes.DM_OUT_BUFFER);
            GlobalUnlock(ipDevMode);
            psSettings.SetHdevmode(ipDevModeData);
            psSettings.DefaultPageSettings.SetHdevmode(ipDevModeData);
            GlobalFree(ipDevMode);
            Marshal.FreeHGlobal(ipDevModeData);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string errorMessage;
            if (!PrinterApis.PrinterSettingsApis.SetPrinterProperty(cbPrinters.SelectedItem.ToString(), PrinterApis.WinNative.DM.Orientation, 2, out errorMessage))
                new InfowareInformationWindow(errorMessage, @"Failed").ShowDialog();
            if (!PrinterApis.PrinterSettingsApis.SetPrinterProperty(cbPrinters.SelectedItem.ToString(), PrinterApis.WinNative.DM.PaperSize, 8, out errorMessage))
                new InfowareInformationWindow(errorMessage, @"Failed").ShowDialog();
            if (!PrinterApis.PrinterSettingsApis.SetPrinterProperty(cbPrinters.SelectedItem.ToString(), PrinterApis.WinNative.DM.NUP, 1, out errorMessage))
                new InfowareInformationWindow(errorMessage, @"Failed").ShowDialog();
        }

        private void TestForm_Load(object sender, EventArgs e)
        {
            foreach (string strPrinterName in PrinterSettings.InstalledPrinters)
            {
                cbPrinters.Items.Add(strPrinterName);
            }

            cbPrinters.SelectedItem = cbPrinters.Items[0];
        }

		private void btnStartProgress_Click(object sender, EventArgs e)
		{
			progressBarCustom.Minimum = 0;
			progressBarCustom.Maximum = 100;
			for (int i = 0; i <= 100; i++)
			{
				progressBarCustom.Value = i;
				System.Threading.Thread.Sleep(20);
			}
		}
	}
}
