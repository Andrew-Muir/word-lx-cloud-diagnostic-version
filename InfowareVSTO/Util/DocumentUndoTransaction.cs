﻿using System;

namespace InfowareVSTO.Util
{
    class DocumentUndoTransaction : IDisposable
    {
        private readonly string undoTransactionLabel;

        private bool transactionIsStopped;

        public DocumentUndoTransaction(string undoTransactionLabel)
        {
            this.undoTransactionLabel = undoTransactionLabel;

            StartTransaction();
        }

        private void StartTransaction()
        {
            Globals.ThisAddIn.Application.UndoRecord.StartCustomRecord(undoTransactionLabel);
        }

        private void StopTransaction()
        {
            Globals.ThisAddIn.Application.UndoRecord.EndCustomRecord();
            transactionIsStopped = true;
        }

        public void Dispose()
        {
            if (transactionIsStopped)
            {
                return;
            }

            StopTransaction();
        }
    }
}
