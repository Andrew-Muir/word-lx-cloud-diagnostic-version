﻿using System;
using System.Windows.Data;

namespace InfowareVSTO.Util.Converters
{
    public class BoolInverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return Swap((bool) value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return Swap((bool) value);
        }

        public bool Swap(bool value)
        {
            return !value;
        }
    }
}
