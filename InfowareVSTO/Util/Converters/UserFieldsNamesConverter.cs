﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace InfowareVSTO.Util.Converters
{
    public class UserFieldsNamesConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is string)
            {
                switch (value)
                {
                    case "User1":
                        return GetUserFieldName(0);
                    case "User2":
                        return GetUserFieldName(1);
                    case "User3":
                        return GetUserFieldName(2);
                }
            }

            return value;
        }

        private string GetUserFieldName(int index)
        {
            string userFieldNames = SettingsManager.GetSetting("AuthorUserFieldsNames", "Glossary");
            if(userFieldNames != null)
            {
                string[] userFieldsArr = userFieldNames.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);

                if (userFieldsArr.Length > index)
                {
                    return userFieldsArr[index];
                }
            }
            return $"User{index + 1}";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
