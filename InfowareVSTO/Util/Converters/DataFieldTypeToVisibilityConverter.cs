﻿using InfowareVSTO.Common;
using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace InfowareVSTO.Util.Converters
{
    public class DataFieldDateTypeToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null && value != DependencyProperty.UnsetValue)
            {
                if((DataFieldType)value == DataFieldType.Date)
                {
                    return true;
                }
            }
            return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
