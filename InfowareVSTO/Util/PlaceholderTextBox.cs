﻿using DocumentFormat.OpenXml.Office2010.Excel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace InfowareVSTO.Util
{
    public class PlaceholderTextBox : TextBox
    {
        public static readonly DependencyProperty PlaceholderProperty =
            DependencyProperty.Register(
            "Placeholder", typeof(string),
            typeof(PlaceholderTextBox)
            );

        public bool ShowingPlaceholder { get; private set; }
        private Brush originalBrush;
        private bool suppressTextChanged;

        protected override void OnInitialized(EventArgs e)
        {
            base.OnInitialized(e);
            ShowPlaceholderIfNeeded();
        }

        protected override void OnGotKeyboardFocus(KeyboardFocusChangedEventArgs e)
        {
            base.OnGotKeyboardFocus(e);
            if (ShowingPlaceholder)
            {
                this.Foreground = originalBrush;
                suppressTextChanged = true;
                this.Text = "";
                suppressTextChanged = false;
                ShowingPlaceholder = false;
            }
        }

        protected override void OnTextChanged(TextChangedEventArgs e)
        {
            if (!suppressTextChanged)
            {
                base.OnTextChanged(e);
            }
        }

        protected override void OnLostKeyboardFocus(KeyboardFocusChangedEventArgs e)
        {
            base.OnLostKeyboardFocus(e);
            ShowPlaceholderIfNeeded();
        }

        public void ShowPlaceholderIfNeeded()
        {
            if (string.IsNullOrWhiteSpace(this.Text))
            {
                suppressTextChanged = true;
                this.Text = Placeholder;
                suppressTextChanged = false;
                originalBrush = this.Foreground;
                this.Foreground = new SolidColorBrush(Colors.Gray);
                this.ShowingPlaceholder = true;
            }
        }

        public string Placeholder
        {
            get { return (string)GetValue(PlaceholderProperty); }
            set { SetValue(PlaceholderProperty, value); }
        }
    }
}
