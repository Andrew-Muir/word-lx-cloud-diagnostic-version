﻿using DocumentFormat.OpenXml.Drawing;
using InfowareVSTO.Common.Language;
using InfowareVSTO.Windows;
using InfowareVSTO.Windows.ParagraphNumbering;
using Microsoft.Office.Core;
using Microsoft.Office.Interop.Word;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using COM = System.Runtime.InteropServices.ComTypes;
using Path = System.IO.Path;

namespace InfowareVSTO.Util
{
    /// <summary>
    /// Contains <see cref="Document"/> related helper methods
    /// </summary>
    internal static class DocumentUtil
    {


        /// <summary>
        /// Gets the total number of pages in the active document
        /// </summary>
        /// <returns>the total number of pages in the active document</returns>
        public static int GetTotalPageCount()
        {
            return (int)GetActiveDocument().Content.Information[WdInformation.wdNumberOfPagesInDocument];
        }

        public static Document GetActiveDocument()
        {
            try
            {
                return Globals.ThisAddIn.Application.ActiveDocument;
            }
            catch
            {
                return null;
            }
        }

        public static Microsoft.Office.Tools.Word.Document GetActiveVstoDocument()
        {
            return Globals.Factory.GetVstoObject(GetActiveDocument());
        }

        public static CommandBarControl GetCommandBarControlWithId(int controlBarControlId)
        {
            return GetActiveDocument().CommandBars.FindControl(Id: controlBarControlId);
        }

        public static void ReplaceStyle(string originalStyleName, string replacementStyleName)
        {
            if (!ListTemplateUtil.CheckStyleExists(originalStyleName) || !ListTemplateUtil.CheckStyleExists(replacementStyleName))
            {
                return;
            }

            StaticCustomUndoRecord.Stop();
            object originalStyleNameObject = originalStyleName;
            object replacementStyleNameObject = replacementStyleName;

            Range documentContentRange = GetActiveDocument().Content;
            Find find = documentContentRange.Find;

            find.ClearFormatting();
            find.set_Style(ref originalStyleNameObject);

            find.Replacement.ClearFormatting();
            find.Replacement.set_Style(ref replacementStyleNameObject);

            find.Format = true;
            find.Forward = true;
            object replaceAll = WdReplace.wdReplaceAll;

            documentContentRange.Find.Execute(Replace: ref replaceAll);
            StaticCustomUndoRecord.Start();
        }

        public static bool IsZeroLengthSelection()
        {
            Range selectionRange = GetApplication().Selection.Range.Duplicate;

            return selectionRange.Start == selectionRange.End;
        }

        public static List GetFirstListInSelection()
        {
            Range selectionRange = GetApplication().Selection.Range.Duplicate;
            int selectionStart = selectionRange.Start;
            int selectionEnd = selectionRange.End;

            foreach (List list in GetApplication().ActiveDocument.Lists)
            {
                if (list.Range.Start == selectionStart // cursor is behind the first character in the list
                    || (list.Range.Start < selectionStart && list.Range.End >= selectionStart) // cursor is somewhere inside any of the list's paragraphs, or on the last character
                    || (list.Range.Start < selectionEnd && list.Range.End >= selectionEnd)) // user did a selection that includes non-list paragraphs before an actual list
                {
                    return list;
                }
            }

            return null;
        }

        public static string SaveCopyAs(string fullDocumentPath = null)
        {
            var nativeDocument = Globals.ThisAddIn.Application.ActiveDocument;
            COM.IPersistFile pp = (COM.IPersistFile)nativeDocument;

            if (string.IsNullOrWhiteSpace(fullDocumentPath))
            {
                string tempDirectoryPath = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
                Directory.CreateDirectory(tempDirectoryPath);

                fullDocumentPath = Path.Combine(tempDirectoryPath, Path.GetRandomFileName());
            }

            pp.Save(fullDocumentPath, false);
            pp.SaveCompleted(fullDocumentPath);
            return fullDocumentPath;
        }

        public static bool DocumentContainsLists()
        {
            return GetActiveDocument().Lists.Count > 0;
        }

        public static Selection GetSelection()
        {
            return Globals.ThisAddIn.Application.Selection;
        }

        public static Range GetCollapsedSelectionRange()
        {
            Range collapsedSelectionRange = Globals.ThisAddIn.Application.Selection.Range.Duplicate;
            collapsedSelectionRange.Collapse();

            return collapsedSelectionRange;
        }

        public static bool TryGetDateLastSaved(out DateTime dateLastSaved)
        {
            try
            {
                dateLastSaved = (DateTime)GetDocumentProperties()[WdBuiltInProperty.wdPropertyTimeLastSaved].Value;
                return true;
            }
            catch (COMException)
            {
                dateLastSaved = new DateTime();
                return false;
            }
        }

        public static bool TryGetWordAuthor(out string author)
        {
            author = string.Empty;
            try
            {
                author = (string)GetDocumentProperties()[WdBuiltInProperty.wdPropertyLastAuthor].Value;
                return true;
            }
            catch (COMException)
            {
                return false;
            }
        }

        public static DocumentProperties GetDocumentProperties()
        {
            return (DocumentProperties)GetActiveDocument().BuiltInDocumentProperties;
        }

        public static string GetDocumentName()
        {
            return GetActiveDocument().Name;
        }

        public static string GetFullDocumentPath()
        {
            return GetActiveDocument().FullName;
        }

        public static List<ContentControl> GetAllContentControls()
        {
            var result = new List<ContentControl>();
            result.AddRange(GetHeaderFooterContentControls(WdHeaderFooterIndex.wdHeaderFooterPrimary, true));
            result.AddRange(GetHeaderFooterContentControls(WdHeaderFooterIndex.wdHeaderFooterFirstPage, true));
            result.AddRange(GetHeaderFooterContentControls(WdHeaderFooterIndex.wdHeaderFooterEvenPages, true));
            result.AddRange(GetHeaderFooterContentControls(WdHeaderFooterIndex.wdHeaderFooterPrimary, false));
            result.AddRange(GetHeaderFooterContentControls(WdHeaderFooterIndex.wdHeaderFooterFirstPage, false));
            result.AddRange(GetHeaderFooterContentControls(WdHeaderFooterIndex.wdHeaderFooterEvenPages, false));

            foreach (ContentControl CC in GetActiveDocument().ContentControls)
            {
                result.Add(CC);
            }

            return result;
        }

        public static bool IsDocumentSaved()
        {
            try
            {
                object value = GetDocumentProperties()[WdBuiltInProperty.wdPropertyTimeLastSaved].Value;
                return true;
            }
            catch (COMException)
            {
                return false;
            }
        }

        private static List<ContentControl> GetHeaderFooterContentControls(WdHeaderFooterIndex headerIndex, bool fromHeader = true)
        {
            var controls = new List<ContentControl>();

            foreach (Section section in GetActiveDocument().Sections)
            {
                HeaderFooter headerFooter = fromHeader ? section.Headers[headerIndex] : section.Footers[headerIndex];

                foreach (ContentControl control in headerFooter.Range.ContentControls)
                {
                    controls.Add(control);
                }
            }

            return controls;
        }

        public static bool IsCursorInsideTable()
        {
            return GetSelection().Range.Tables.Count > 0;
        }

        public static void ApplyStyleToSelection(string paragraphStyleName, bool characterStyle = false)
        {
            if (!string.IsNullOrEmpty(paragraphStyleName))
            {
                dynamic styleDialog = Globals.ThisAddIn.Application.Dialogs[WdWordDialog.wdDialogFormatStyle];
                styleDialog.Name = paragraphStyleName;

                styleDialog.Execute();
                
                if (!characterStyle)
                {
                    Range range = Globals.ThisAddIn.Application.ActiveWindow.Selection.Range;
                    try
                    {
                        range.Paragraphs.First.set_Style(paragraphStyleName);
                    }
                    catch
                    { 
                    }
                }
            }
        }

        //retrurns false if shown and no has been clicked,
        // else true
        public static bool ShowCompatibilityModeErrorPreventive()
        {
            Document document = GetActiveDocument();
            if (document != null)
            {
                if (document.CompatibilityMode < (int)WdCompatibilityMode.wdWord2010 ||
                    document.SaveFormat < (int)WdSaveFormat.wdFormatXMLDocument ||
                            document.SaveFormat == (int)WdSaveFormat.wdFormatPDF ||
                            document.SaveFormat == (int)WdSaveFormat.wdFormatXPS)
                {
                    bool yes = false;
                    ShowCompatibilityModeError(new System.Threading.Tasks.Task(() =>{ yes = true; }));
                    return yes;
                }
            }
            return true;
        }

        public static void ShowCompatibilityModeError(System.Threading.Tasks.Task task = null)
        {
            if (SettingsManager.GetGeneralSettingAsBool("WarnCompatibilityMode", true))
            {
                if (!SettingsManager.GetGeneralSettingAsBool("AskConvertOldDocs", false))
                {
                    Message_WPF messageDialog = new Message_WPF("Content controls cannot be added in Compatibility Mode.");
                    messageDialog.ShowDialog();
                }
                else
                {
                    InfowareConfirmationWindow dialog = new InfowareConfirmationWindow(LanguageManager.GetTranslation(LanguageConstants.ThisToolIsNotCompatible, "This tool is not compatible with the current document. Do you want to convert the document format?"),
                        messageBoxButton: System.Windows.MessageBoxButton.YesNo);
                    dialog.ShowDialog();
                    if (dialog.Result == System.Windows.MessageBoxResult.Yes)
                    {
                        object missing = System.Reflection.Missing.Value;

                        bool convertTables = false;
                        Document document = GetActiveDocument();
                        if (document != null)
                        {
                            //compatibility
                            if (document.CompatibilityMode < (int)WdCompatibilityMode.wdWord2010)
                            {
                                if (document.CompatibilityMode < (int)WdCompatibilityMode.wdWord2007)
                                {
                                    convertTables = true;
                                }

                                document.Convert();
                            }

                            //docx
                            if (document.SaveFormat < (int)WdSaveFormat.wdFormatXMLDocument ||
                                document.SaveFormat == (int)WdSaveFormat.wdFormatPDF ||
                                document.SaveFormat == (int)WdSaveFormat.wdFormatXPS)
                            {
                                string filename = Path.ChangeExtension(document.FullName, ".docx");
                                document.SaveAs(filename,
                         WdSaveFormat.wdFormatXMLDocument, ref missing, ref missing, ref missing, ref missing,
                        ref missing, ref missing, ref missing, ref missing, ref missing,
                        ref missing, ref missing, ref missing, ref missing, ref missing);

                            }

                            //tables
                            if (convertTables)
                            {
                                foreach (Microsoft.Office.Interop.Word.Table table in document.Tables)
                                {
                                    table.Rows.LeftIndent = table.LeftPadding * -1;
                                }
                            }

                            if (task != null)
                            {
                                task.RunSynchronously();
                            }
                        }
                    }
                }
            }
        }

        public class StyleNameEqualityComparer : IEqualityComparer<Style>
        {
            public bool Equals(Style x, Style y)
            {
                return x.NameLocal == y.NameLocal;
            }

            public int GetHashCode(Style obj)
            {
                return obj.GetHashCode();
            }
        }

        private static void CopyStyle(Style from, Style to)
        {
            try
            {
                to.Borders = from.Borders;
            }
            catch (Exception)
            {
            }
            to.Font = from.Font;
            to.Hidden = from.Hidden;
            to.Locked = from.Locked;
            to.NameLocal = from.NameLocal;
            try
            {
                to.NoSpaceBetweenParagraphsOfSameStyle = from.NoSpaceBetweenParagraphsOfSameStyle;
            }
            catch (Exception)
            {
            }
            try
            {
                to.ParagraphFormat = from.ParagraphFormat;
            }
            catch (Exception)
            {
            }
            to.Priority = from.Priority;
            to.UnhideWhenUsed = from.UnhideWhenUsed;
            to.Visibility = from.Visibility;
        }

        public static void CopyStyles(Document from, Document to)
        {
            var fromStyles = new List<Style>(from.Styles.Cast<Style>());
            var toStyles = new List<Style>(to.Styles.Cast<Style>());
            List<string> builtInToCopy = GetBuiltInStylesToCopy();
            foreach (Style style in fromStyles)
            {
                if (!toStyles.Contains(style, new StyleNameEqualityComparer()))
                {
                    if (!style.BuiltIn)
                    {
                        from.Application.OrganizerCopy(from.Name, to.Name, style.NameLocal, WdOrganizerObject.wdOrganizerObjectStyles);
                    }
                }
                else
                {
                    Style toStyle = toStyles.FirstOrDefault(x => x.NameLocal == style.NameLocal);
                    if (toStyle != null && builtInToCopy.Contains(toStyle.NameLocal))
                    {
                        CopyStyle(style, toStyle);
                    }
                }
            }
        }

        private static List<string> GetBuiltInStylesToCopy()
        {
            List<string> result = new List<string>();
            result.Add("Normal");
            result.Add("Heading 1");
            result.Add("Heading 2");
            result.Add("Heading 3");
            result.Add("Heading 4");
            result.Add("Heading 5");
            result.Add("Heading 6");
            result.Add("Heading 7");
            result.Add("Heading 8");
            result.Add("Heading 9");
            return result;
        }

        private static Application GetApplication()
        {
            return Globals.ThisAddIn.Application;
        }

        public static List<string> GetAllListTemplatesNames()
        {
            List<string> result = new List<string>();

            foreach (ListTemplate lt in ThisAddIn.Instance.Application.ActiveDocument.ListTemplates)
            {
                result.Add(lt.Name);
            }

            return result;
        }
    }
}
