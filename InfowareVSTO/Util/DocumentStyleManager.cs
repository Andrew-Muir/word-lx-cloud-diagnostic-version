﻿using System;
using Microsoft.Office.Interop.Word;

namespace InfowareVSTO.Util
{
    internal class DocumentStyleManager
    {
        private const string FIRST_LINE_INDENT_STYLE_NAME_TEMPLATE = "Indent First Line Indent {0}\"";

        private const string HANGING_INDENT_STYLE_NAME_TEMPLATE = "Indent Hanging";

        private const string INTENT_STYLE_NAME_TEMPLATE = "Indent {0}\"";

        private const string BLOCK_INDENT_STYLE_NAME_TEMPLATE = "Indent Block {0}\"";

        private const string LEFT_ALIGNED_STYLE_NAME = "Left-Aligned";

        private const string RIGHT_ALIGNED_STYLE_NAME = "Right-Aligned";

        private const string CENTER_ALIGNED_STYLE_NAME = "Centre";

        private const string CENTER_ALIGNED_BOLDED_STYLE_NAME = "Centre Bold";

        private const string PLAIN_STYLE_NAME = "Plain";

        private const string CENTER_HEADING_STYLE_NAME = "Heading1Centre";

        private const string REFERENCE_STYLE_NAME = "Reference";

        public static DocumentStyleManager Instance { get; private set; }

        public static Application app;

        static DocumentStyleManager()
        {
            app = Globals.ThisAddIn.Application;
            Instance = new DocumentStyleManager();
        }

        #region Indentation

        public void SetFirstLineIndent(float valueInPoints)
        {
            string styleName;

            if (valueInPoints > 0)
            {
                styleName = GetStyleName(FIRST_LINE_INDENT_STYLE_NAME_TEMPLATE, Math.Abs(valueInPoints));
            }
            else if (valueInPoints < 0)
            {
                styleName = HANGING_INDENT_STYLE_NAME_TEMPLATE;
            }
            else
            {
                throw new ArgumentOutOfRangeException($"{nameof(valueInPoints)} should be either negative or greater than zero");
            }

            if (!CheckStyleExists(styleName))
            {
                Style style = CreateStyle(styleName);
                style.ParagraphFormat.FirstLineIndent = valueInPoints;

                if (valueInPoints < 0)
                {
                    // Hanging indent
                    style.ParagraphFormat.LeftIndent = Math.Abs(valueInPoints);
                }
            }

            ApplyStyleToSelection(styleName);
        }

        public void SetLeftIndent(float valueInPoints)
        {
            string styleName = GetStyleName(INTENT_STYLE_NAME_TEMPLATE, valueInPoints);

            if (!CheckStyleExists(styleName))
            {
                Style style = CreateStyle(styleName);
                style.ParagraphFormat.LeftIndent = valueInPoints;
                style.ParagraphFormat.RightIndent = style.ParagraphFormat.FirstLineIndent = 0;
            }

            ApplyStyleToSelection(styleName);
        }

        public void SetLeftAndRightIndent(float valueInPoints)
        {
            string styleName = GetStyleName(BLOCK_INDENT_STYLE_NAME_TEMPLATE, valueInPoints);

            if (!CheckStyleExists(styleName))
            {
                Style style = CreateStyle(styleName);
                style.ParagraphFormat.RightIndent = style.ParagraphFormat.LeftIndent = valueInPoints;
                style.ParagraphFormat.FirstLineIndent = 0;
            }

            ApplyStyleToSelection(styleName);
        }

        public void ClearIndentation()
        {
            ApplyStyleToSelection(Utils.GetLocalizedStyleNameFromBuiltInStyle(WdBuiltinStyle.wdStyleNormal));
        }

        #endregion

        #region Alignment

        public void SetLeftAlignedStyle()
        {
            if (!CheckStyleExists(LEFT_ALIGNED_STYLE_NAME))
            {
                Style style = CreateStyle(LEFT_ALIGNED_STYLE_NAME);
                style.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphLeft;
            }

            ApplyStyleToSelection(LEFT_ALIGNED_STYLE_NAME);
        }

        public void SetRightAlignedStyle()
        {
            if (!CheckStyleExists(RIGHT_ALIGNED_STYLE_NAME))
            {
                Style style = CreateStyle(RIGHT_ALIGNED_STYLE_NAME);
                style.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphRight;
            }

            ApplyStyleToSelection(RIGHT_ALIGNED_STYLE_NAME);
        }

        public void SetCenterAlignedStyle(bool bolded = false)
        {
            string styleName = bolded ? CENTER_ALIGNED_BOLDED_STYLE_NAME : CENTER_ALIGNED_STYLE_NAME;

            if (!CheckStyleExists(styleName))
            {
                Style style = CreateStyle(styleName);
                style.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphCenter;

                if (bolded)
                {
                    style.Font.Bold = -1;
                }
            }

            ApplyStyleToSelection(styleName);
        }

        public void SetPlainStyle()
        {
            if (!CheckStyleExists(PLAIN_STYLE_NAME))
            {
                Style style = CreateStyle(PLAIN_STYLE_NAME);
                style.ParagraphFormat.SpaceAfter = 0;
            }

            ApplyStyleToSelection(PLAIN_STYLE_NAME);
        }

        public void SetCenteredHeadingStyle()
        {
            if (!CheckStyleExists(CENTER_HEADING_STYLE_NAME))
            {
                Style style = CreateStyle(CENTER_HEADING_STYLE_NAME);
                style.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphCenter;
            }

            ApplyStyleToSelection(CENTER_HEADING_STYLE_NAME);
        }

        public void SetReferenceStyle()
        {
            if (!CheckStyleExists(REFERENCE_STYLE_NAME))
            {
                Style style = CreateStyle(REFERENCE_STYLE_NAME);
                style.ParagraphFormat.LeftIndent = 72;
                style.Font.Bold = -1;
            }

            ApplyStyleToSelection(REFERENCE_STYLE_NAME);
        }

        #endregion

        private bool CheckStyleExists(string styleName)
        {
            foreach (Style style in app.ActiveDocument.Styles)
            {
                if (style.NameLocal == styleName)
                {
                    return true;
                }
            }

            return false;
        }

        private Style CreateStyle(string styleName, string baseStyleName = null)
        {
            object baseStyleNameObject = baseStyleName ?? Utils.GetLocalizedStyleNameFromBuiltInStyle(WdBuiltinStyle.wdStyleNormal);
            object paragraphStyleType = WdStyleType.wdStyleTypeParagraph;

            Style style = app.ActiveDocument.Styles.Add(styleName, ref paragraphStyleType);
            style.set_BaseStyle(ref baseStyleNameObject);

            return style;
        }

        public static void ApplyStyleToSelection(string styleName, bool character = false)
        {
            dynamic styleDialog = app.Dialogs[WdWordDialog.wdDialogFormatStyle];
            styleDialog.Name = styleName;
            styleDialog.Execute();

            if (!character)
            {
                Range range = Globals.ThisAddIn.Application.ActiveWindow.Selection.Range;
                try
                {
                    range.Paragraphs.First.set_Style(styleName);
                }
                catch
                {
                }
            }
        }

        private string GetStyleName(string styleNameTemplate, float valueInPoints)
        {
            return string.Format(styleNameTemplate, app.PointsToInches(valueInPoints));
        }
    }
}
