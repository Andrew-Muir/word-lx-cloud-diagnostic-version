﻿using Microsoft.Office.Interop.Word;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfowareVSTO.Util
{
    public class StaticCustomUndoRecord
    {
        public static string ActionName { get; set; }
        public static UndoRecord undoRecord;
        public static void Start()
        {
            if (ActionName != null)
            {
                undoRecord = ThisAddIn.Instance.Application.UndoRecord;
                undoRecord.StartCustomRecord(ActionName ?? "Custom Undo Record");
            }
        }

        public static void Start(string actionName)
        {
            ActionName = actionName;
            Start();
        }

        public static void Stop(bool final = false)
        {
            if (undoRecord != null)
            {
                undoRecord.EndCustomRecord();
            }
            if (final)
            {
                ActionName = null;
            }
        }
    }
}
