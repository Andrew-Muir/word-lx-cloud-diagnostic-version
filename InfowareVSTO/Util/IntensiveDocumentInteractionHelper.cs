﻿using System;
using InfowareVSTO.Common;
using Microsoft.Office.Interop.Word;

namespace InfowareVSTO.Util
{
    internal class IntensiveDocumentInteractionWrapper : IDisposable
    {
        private bool isDisposed;

        private DocumentUndoTransaction documentUndoTransaction;

        private static readonly Application app;

        static IntensiveDocumentInteractionWrapper()
        {
            app = Globals.ThisAddIn.Application;
        }

        public IntensiveDocumentInteractionWrapper(bool runInUndoTransaction = true, string undoTransactionName = "Add-in operation")
        {
            app.ScreenUpdating = false;
            if (ThisAddIn.Instance.IsFunctionForLogging("ScreenUpdating"))
                Logger.Log("Disabled Screen Updating (IntensiveDocumentInteractionWrapper.cs)");

            if (runInUndoTransaction)
            {
                documentUndoTransaction = new DocumentUndoTransaction(undoTransactionName);
            }
        }

        public void Dispose()
        {
            if (isDisposed)
            {
                return;
            }

            app.ScreenUpdating = true;
            if (ThisAddIn.Instance.IsFunctionForLogging("ScreenUpdating"))
                Logger.Log("Enabled Screen Updating (IntensiveDocumentInteractionWrapper.cs)");
            documentUndoTransaction?.Dispose();

            isDisposed = true;
        }
    }
}
