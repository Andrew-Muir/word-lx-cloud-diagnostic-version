﻿using InfowareVSTO.Common.Language;
using InfowareVSTO.Windows;
using Microsoft.Office.Interop.Word;
using System;
using System.Runtime.InteropServices;

namespace InfowareVSTO.Util
{
    /// <summary>
    /// Contains <see cref="Range"/> related helper methods
    /// </summary>
    internal static class RangeUtil
    {
        /// <summary>
        /// Clears manual formatting (<see cref="Paragraph.Reset()"/>) and sets the given <see cref="WdBuiltinStyle"/> 
        /// on all of the <see cref="Paragraph"/> instances in the given <see cref="Range"/>
        /// </summary>
        /// <param name="range"></param>
        /// <param name="style"></param>
        internal static void SetParagraphStyle(Range range, WdBuiltinStyle style)
        {
            foreach (Paragraph paragraph in range.Paragraphs)
            {
                paragraph.Reset();
                paragraph.set_Style(style);
            }
        }

        /// <summary>
        /// Clears manual formatting (<see cref="Paragraph.Reset()"/>) and sets the given style on all of the 
        /// <see cref="Paragraph"/> instances in the given <see cref="Range"/>
        /// </summary>
        /// <param name="range"></param>
        /// <param name="style"></param>
        internal static void SetParagraphStyle(Range range, object style)
        {
            try
            {
                foreach (Paragraph paragraph in range.Paragraphs)
                {
                    paragraph.Reset();
                    paragraph.set_Style(ref style);
                }
            }
            catch (COMException)
            {
                string styleName = string.Empty;
                if (style is string)
                {
                    styleName = (style as string) + ' ';
                }

                InfowareInformationWindow messageDialog = new InfowareInformationWindow(LanguageManager.GetTranslation(LanguageConstants.Style,"Style") + " " + styleName + LanguageManager.GetTranslation(LanguageConstants.WasNotFound, "was not found."));
                messageDialog.ShowDialog();
            }
        }

        /// <summary>
        /// Sets the given <see cref="WdParagraphAlignment"/> on all of the <see cref="Paragraph"/> instances in the
        /// given <see cref="Range"/>
        /// </summary>
        /// <param name="range"></param>
        /// <param name="alignment"></param>
        internal static void SetParagraphAlignment(Range range, WdParagraphAlignment alignment)
        {
            foreach (Paragraph paragraph in range.Paragraphs)
            {
                paragraph.Format.Alignment = alignment;
            }
        }

        public static Style CreateHeadingCentreStyle()
        {
            string styleName = "Heading1Centre";
            try
            {
                return ThisAddIn.Instance.Application.ActiveDocument.Styles[styleName];
            }
            catch (Exception)
            {
                Style heading1Centre = ThisAddIn.Instance.Application.ActiveDocument.Styles.Add(styleName, WdStyleType.wdStyleTypeParagraph);
                heading1Centre.set_BaseStyle(Utils.GetLocalizedStyleNameFromBuiltInStyle(WdBuiltinStyle.wdStyleHeading1));
                heading1Centre.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphCenter;
                return heading1Centre;
            }
        }

        public static void SetSelectionStyle(object style, bool character = false)
        {
            try
            {
                Style styleObj = ThisAddIn.Instance.Application.ActiveDocument.Styles[style];
                DocumentStyleManager.ApplyStyleToSelection(styleObj.NameLocal, character);
            }
            catch
            {
                string styleName = string.Empty;
                if (style is string)
                {
                    styleName = (style as string) + ' ';
                }

                InfowareInformationWindow messageDialog = new InfowareInformationWindow(LanguageManager.GetTranslation(LanguageConstants.Style, "Style") + " " + styleName + LanguageManager.GetTranslation(LanguageConstants.WasNotFound, "was not found."));
                messageDialog.ShowDialog();
            }
        }
    }
}
