﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace InfowareVSTO.Util
{
    public class TaskpaneFieldOrderer
    {
        public void OrderFields(string json, StackPanel parent)
        {
            if (!string.IsNullOrWhiteSpace(json))
            {
                var fieldOrder = JsonConvert.DeserializeObject<FieldOrder>(json);

                if (fieldOrder != null)
                {
                    int i = parent.Children.Cast<UIElement>().ToList().FindIndex(x => x is StackPanel);

                    foreach (var field in fieldOrder.Lists)
                    {
                        field.TranslateFieldID();

                        if (!string.IsNullOrWhiteSpace(field.FieldStackPanelName))
                        {
                            var element = parent.Children.Cast<UIElement>().Where(x => (x as FrameworkElement)?.Name == field.FieldStackPanelName).FirstOrDefault();

                            if (element != null)
                            {
                                parent.Children.Remove(element);
                                parent.Children.Insert(i, element);

                                i++;
                            }
                        }
                    }
                }
            }
        }
    }

    public class FieldItem
    {
        public string FieldID { get; set; }
        public string FieldStackPanelName { get; set; }
        
        public void TranslateFieldID()
        {
            var dictionary = GetFieldDictionary();

            if (dictionary.TryGetValue(FieldID, out string fieldStackPanelName))
            {
                this.FieldStackPanelName = fieldStackPanelName;
            }
        }

        private Dictionary<string, string> GetFieldDictionary()
        {
            Dictionary<string, string> result = new Dictionary<string, string>() 
            {
                { "Author", "AuthorField" },
                { "Assistant", "AssistantField" },
                { "Date", "DateField" },
                { "Recipient(s)", "RecipientsField" },
                { "Salutation", "SalutationField" },
                { "CC", "CCField" },
                { "BCC", "BCCField" },
                { "Our File Number", "FileNumberField" },
                { "Their File Number", "ClientFileNumberField" },
                { "Delivery", "DeliveryField" },
                { "Handling", "HandlingField" },
                { "Attention", "AttentionField" },
                { "Office Location", "OfficeLocationField" },
                { "Re Line", "ReLineField" },
                { "Diary Date", "diaryDateStackPanel" },
                { "Closing", "ClosingField" },
                { "Enclosure(s)", "EnclosuresField" },
                { "#", "EnclosureNoField" },
                { "Include Firm Name in Closing", "IncludeFirmNameField" },
                { "Include Per in Closing", "IncludePerField" },
                { "Start at Label Number", "StartAtField" },
                { "Use First Recipient to fit All Positions", "UseFirstField" },
                { "To", "ToField" },
                { "Firm", "FirmField" },
                { "Fax Number", "FaxNumberField" },
                { "Phone Number", "PhoneNumberField" },
                { "No.of pages", "NoOfPagesField" }
            };
            return result;
        }
    }

    public class FieldOrder
    {
        public List<FieldItem> Lists { get; set; }
    }
}
