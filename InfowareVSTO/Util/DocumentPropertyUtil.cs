﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Office.Core;
using Microsoft.Office.Interop.Word;

namespace InfowareVSTO.Util
{
    internal class DocumentPropertyUtil
    {
        private const int MAX_PROPERTY_VALUE_SIZE = 255;

        private const string PROPERTY_VALUE_SIZE_SUFFIX = "-SIZE";

        public static void SaveLongProperty(string propertyName, string propertyValue)
        {
            RemovePropertiesByNameSuffix(propertyName);

            int splitCount = (int) Math.Ceiling((double) propertyValue.Length / MAX_PROPERTY_VALUE_SIZE);

            SaveShortProperty(propertyName + PROPERTY_VALUE_SIZE_SUFFIX, splitCount.ToString());

            for (int splitIndex = 0; splitIndex < splitCount; splitIndex++)
            {
                char[] characters = propertyValue.Skip(splitIndex * MAX_PROPERTY_VALUE_SIZE).Take(MAX_PROPERTY_VALUE_SIZE).ToArray();

                string value = new string(characters);
                SaveShortProperty($"{propertyName}{splitIndex}", value);
            }
        }

        public static string ReadLongProperty(string propertyName, Document document = null)
        {
            string splitCountProperty = ReadProperty(propertyName + PROPERTY_VALUE_SIZE_SUFFIX, document);

            if (splitCountProperty == null)
            {
                // No split count property is defined, there can't be any property that contains the actual values
                return null;
            }

            int splitCount = int.Parse(splitCountProperty);

            var propertyBuilder = new StringBuilder();

            for (int splitIndex = 0; splitIndex < splitCount; splitIndex++)
            {
                propertyBuilder.Append(ReadProperty(propertyName + splitIndex, document));
            }

            return propertyBuilder.ToString();
        }

        public static string GetLongPropertySizePropertyName(string propertyName)
        {
            return propertyName + PROPERTY_VALUE_SIZE_SUFFIX;
        }

        public static void RemovePropertiesByNameSuffix(string propertyNamePrefix, Document fromDocument = null)
        {
            var propertiesToDelete = new List<DocumentProperty>();
            DocumentProperties documentProperties = GetCustomProperties(fromDocument ?? Globals.ThisAddIn.Application.ActiveDocument);

            if(documentProperties != null)
            {
                foreach (DocumentProperty property in documentProperties)
                {
                    if (property.Name.StartsWith(propertyNamePrefix))
                    {
                        propertiesToDelete.Add(property);
                    }
                }
            }

            propertiesToDelete.ForEach(it => it.Delete());
        }

        public static void CopyDocumentProperties(Document sourceDocument, Document destinationDocument)
        {
            DocumentProperties sourceDocumentProperties = GetCustomProperties(sourceDocument);
            DocumentProperties destinationDocumentProperties = GetCustomProperties(destinationDocument);

            Dictionary<string, DocumentProperty> existingDestinationProperties = GetAllCustomProperties(destinationDocument);

            foreach (DocumentProperty documentProperty in sourceDocumentProperties)
            {
                if (existingDestinationProperties.ContainsKey(documentProperty.Name))
                {
                    existingDestinationProperties[documentProperty.Name].Delete();
                }

                destinationDocumentProperties.Add(documentProperty.Name, documentProperty.LinkToContent, documentProperty.Type,
                    documentProperty.Value, documentProperty.LinkSource);
            }
        }

        private static Dictionary<string, DocumentProperty> GetAllCustomProperties(Document document)
        {
            var existingProperties = new Dictionary<string, DocumentProperty>();

            foreach (DocumentProperty documentProperty in GetCustomProperties(document))
            {
                existingProperties.Add(documentProperty.Name, documentProperty);
            }

            return existingProperties;
        }

        public static void SaveShortProperty(string propertyName, string propertyValue, bool removeBefore = false, Document fromDocument = null)
        {
            if (removeBefore)
            {
                RemoveShortProperty(propertyName, fromDocument ?? Globals.ThisAddIn.Application.ActiveDocument);
            }

            DocumentProperties documentProperties = GetCustomProperties(fromDocument ?? Globals.ThisAddIn.Application.ActiveDocument);
            if(documentProperties != null)
            {
                documentProperties.Add(propertyName, LinkToContent: false, Type: MsoDocProperties.msoPropertyTypeString, Value: propertyValue);
            }
        }

        public static void RemoveShortProperty(string propertyName, Document fromDocument = null)
        {
            RemovePropertiesByNameSuffix(propertyName, fromDocument ?? Globals.ThisAddIn.Application.ActiveDocument);
        }

        public static string ReadProperty(string propertyName, Document fromDocument = null)
        {
            DocumentProperties properties = fromDocument == null ? GetCustomProperties() : GetCustomProperties(fromDocument);

            if(properties != null)
            {
                foreach (DocumentProperty property in properties)
                {
                    if (property.Name.Equals(propertyName))
                    {
                        return property.Value.ToString();
                    }
                }
            }

            return null;
        }

        private static DocumentProperties GetCustomProperties()
        {
            DocumentProperties documentProperties = null;
            if (Globals.ThisAddIn.Application.Documents.Count >= 1)
            {
                documentProperties = GetCustomProperties(Globals.ThisAddIn.Application.ActiveDocument);
            }
            return documentProperties;
        }

        private static DocumentProperties GetCustomProperties(Document document)
        {
            return (DocumentProperties) document.CustomDocumentProperties;
        }

        public static int? GetTemplateCompanyID()
        {
            int companyID = 0;
            if(int.TryParse(DocumentPropertyUtil.ReadProperty(Ribbon.COMPANY_ID_PROP), out companyID))
            {
                return companyID;
            }

            return null;
        }
    }
}