﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InfowareVSTO.Windows;
using Microsoft.Office.Interop.Word;

namespace InfowareVSTO.Util
{
    public class ProtectionManager
    {
        private Document document;
        private WdProtectionType originalProtectionType;
        private string password;
        private bool unprotected = false;

        public ProtectionManager(Document document)
        {
            this.document = document;
            originalProtectionType = document.ProtectionType;
        }

        public void UnprotectIfNeeded()
        {
            if (originalProtectionType != WdProtectionType.wdNoProtection)
            {
                try
                {
                    document.Unprotect();
                    unprotected = true;
                }
                catch (Exception ex)
                {
                    PasswordWindow dialog = new PasswordWindow();
                    dialog.ShowDialog();
                    if (dialog.Result != null)
                    {
                        try
                        {
                            document.Unprotect(dialog.Result);
                            unprotected = true;
                            password = dialog.Result;
                        }
                        catch 
                        {
                        }
                    }
                }
            }
        }

        public void ReProtectIfNeeded()
        {
            try
            {
                if (originalProtectionType != WdProtectionType.wdNoProtection && unprotected)
                {
                    if (password == null)
                    {
                        document.Protect(originalProtectionType, true);
                    }
                    else
                    {
                        document.Protect(originalProtectionType, true, Password: password);
                    }
                }
            }
            catch
            {
            }
        }
    }
}
