﻿using Microsoft.Office.Interop.Word;

namespace InfowareVSTO.Util
{
    /// <summary>
    /// Containts <see cref="Table"/> related helper methods
    /// </summary>
    internal static class TableUtil
    {
        public static void CenterRowCells(Row tableRow)
        {
            tableRow.Range.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphCenter;
        }

        /// <summary>
        /// Hides the borders that are not shared with adjacent row. That is, the method hides all borders but the
        /// bottom one
        /// </summary>
        /// <param name="tableRow"></param>
        public static void HidePrivateBorders(Row tableRow)
        {
            tableRow.Borders[WdBorderType.wdBorderLeft].Visible = false;
            tableRow.Borders[WdBorderType.wdBorderTop].Visible = false;
            tableRow.Borders[WdBorderType.wdBorderRight].Visible = false;
        }

        public static void SetRowFont(Row tableRow, string fontName, int fontSize)
        {
            Font font = tableRow.Range.Font;

            font.Size = fontSize;
            font.Name = font.NameAscii = font.NameBi = fontName;
        }
    }
}
