﻿using Microsoft.Office.Interop.Word;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfowareVSTO.Util
{
    public class LineSpacingUtil
    {
        public static void ApplyLineSpacing(Paragraphs paragraphs, WdLineSpacing lineSpacing)
        {
            float value = 12;
            switch (lineSpacing)
            {
                case WdLineSpacing.wdLineSpaceSingle:
                    value = 12;
                    break;
                case WdLineSpacing.wdLineSpace1pt5:
                    value = 18;
                    break;
                case WdLineSpacing.wdLineSpaceDouble:
                    value = 24;
                    break;
                default:
                    break;

            }

            if (paragraphs != null && paragraphs.Count > 0)
            {
                //Key-Section in Admin Panel
                string[,] styleList = {
                    { "KeepStandardSpacing","DocLineSpacing" },
                    { "KeepStandardSpacingBaseStyles","DocLineSpacing" }
                 };

                //Building List
                string[][] exceptedStyles = new string[2][];
                for (int i = 0; i < 2; i++)
                {

                    //Only One per Setting to avoid collision
                    string exceptedStyleList = SettingsManager.GetSetting(
                        styleList[i, 0], //ID
                        styleList[i, 1],  //Section
                        string.Empty
                    );

                    exceptedStyles[i] = exceptedStyleList.Split(
                        new string[] { "," },
                        StringSplitOptions.RemoveEmptyEntries
                     );
                }

                //Separate Items
             
                Style user_style;
                Style parent_style;

                foreach (Paragraph paragraph in paragraphs)
                {
                    user_style = paragraph.get_Style() as Style;
                    parent_style = user_style.get_BaseStyle() as Style; //Null parrent does not affect code execution

                    //Check ruling for all settings
                    for (int i = 0; i < 2; i++)
                    {
                        if (
                            (styleList[i, 0].Equals("KeepStandardSpacingBaseStyles") && exceptedStyles[i].Contains(parent_style.NameLocal)) //Inherited Styles
                            || (exceptedStyles[i].Contains(user_style.NameLocal)) //Base Styles
                           )
                        {
                            break;
                        }
                        else
                        {
                            if (i == 1)
                            {
                                paragraph.Format.LineSpacing = value;
                                break;
                            }
                        }
                    }
                }

            }
            else
            {
                try
                {
                    Style normalStyle = ThisAddIn.Instance.Application.ActiveDocument.Styles[Utils.GetStandardStyleName()];
                    if (normalStyle != null)
                    {
                        normalStyle.ParagraphFormat.LineSpacing = value;
                    }
                }
                catch(Exception ex)
                {
                    Debug.WriteLine(ex);
                }
            }
        }
    }
}
