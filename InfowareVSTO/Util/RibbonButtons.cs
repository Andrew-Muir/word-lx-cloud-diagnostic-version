﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfowareVSTO.Util
{
    public class RibbonButtons
    {
        public List<RibbonButton> Buttons { get; set; }
    }

    public class RibbonButton
    {
        public string Name { get; set; }
        public int Id { get; set; }
        public string ButtonId { get; set; }
        public List<RibbonButton> SubItems { get; set; }
    }

}
