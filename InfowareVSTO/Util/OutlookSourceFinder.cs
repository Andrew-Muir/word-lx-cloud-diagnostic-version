﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InfowareVSTO.Util
{
    public class OutlookSourceFinder
    {
        private static string tempAttachmentPath = null;

        public static bool DocumentIsOutlookAttachment(string path)
        {
            if (!string.IsNullOrEmpty(path))
            {
                if (tempAttachmentPath == null)
                {
                    GetTempAttachmentPath();
                }

                if (tempAttachmentPath != null)
                {
                    return tempAttachmentPath.TrimEnd('\\', '/') == path.TrimEnd('\\', '/');
                }
            }

            return false;

        }

        private static void GetTempAttachmentPath()
        {
            string version = GetOutlookVersion();
        
            if (!string.IsNullOrWhiteSpace(version))
            {
               tempAttachmentPath = Microsoft.Win32.Registry.GetValue($@"HKEY_CURRENT_USER\Software\Microsoft\Office\{version}.0\Outlook\Security", "OutlookSecureTempFolder", null) as string;
            }
        }

        private static string GetOutlookVersion()
        {
            string str = Microsoft.Win32.Registry.GetValue($@"HKEY_CLASSES_ROOT\Outlook.Application\CurVer", "", null) as string;
        
            if (str != null && str.StartsWith("Outlook.Application."))
            {
                return str.Trim().Substring("Outlook.Application.".Length);
            }

            return null;
        }
    }
}
