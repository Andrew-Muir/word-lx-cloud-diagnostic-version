﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using DocumentFormat.OpenXml.Office2010.PowerPoint;
using DocumentFormat.OpenXml.Spreadsheet;
using InfowareVSTO.Common.DTOs;
using InfowareVSTO.Common.General;
using InfowareVSTO.Common.Models;
using InfowareVSTO.DataSource;
using InfowareVSTO.TaskPaneControls;
using Microsoft.Office.Tools;
using Newtonsoft.Json.Linq;

namespace InfowareVSTO.Util
{
    internal static class TaskPaneUtil
    {
        private const string SAVE_DATA_PROP = "LX-SAVEDATA";
        public static bool AnswersFound;

        public static CustomTaskPane FindOrCreateCustomTaskPane(string taskPaneTitle)
        {
            var customTaskPanes = Globals.ThisAddIn.CustomTaskPanes;

            var taskPane = customTaskPanes.Where(it => it.Title == taskPaneTitle).FirstOrDefault();

            if (taskPane != null)
            {
                return taskPane;
            }

            return customTaskPanes.Add(new TaskPaneContainer(), taskPaneTitle);
        }

        public static void SaveAnswers(PaneDTO paneDTO)
        {
            if (paneDTO != null)
            {
                DocumentPropertyUtil.SaveLongProperty(SAVE_DATA_PROP, JObject.FromObject(paneDTO).ToString());
            }
        }

        public static PaneDTO LoadAnswers(PaneDTO initial = null, Microsoft.Office.Interop.Word.Document document = null)
        {
            PaneDTO result = initial ?? new PaneDTO();
            AnswersFound = false;
            try
            {
                string serialized = DocumentPropertyUtil.ReadLongProperty(SAVE_DATA_PROP, document);
                if (serialized != null)
                {

                    JObject jObj = JObject.Parse(serialized);
                    if (jObj != null)
                    {
                        result = jObj.ToObject<PaneDTO>();
                        AnswersFound = true;
                    }
                }
            }
            catch (Exception)
            {
            }

            return PaneUtils.ReadAnswersFromDocument(document == null ? ThisAddIn.Instance.Application.ActiveDocument : document, result);
        }

        public static DataSourceValues GetDataSourceAnsweresDTO(PaneDTO paneDTO = null)
        {
            if (DataSourceManager.DataSourceType != Common.DataSourceType.None && DataSourceManager.LoadedMatter != null)
            {
                return DataSourceManager.GetValues(paneDTO);
            }

            return null;
        }

        public static PaneDTO GetNewPaneDTO(DataSourceValues values)
        {
            return values?.PaneDTO ?? new PaneDTO();
        }
    }
}
