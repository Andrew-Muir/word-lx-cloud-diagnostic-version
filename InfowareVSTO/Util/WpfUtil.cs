﻿using System.Windows.Input;
using System.Windows.Media;

namespace InfowareVSTO.Util
{
    internal static class WpfUtil
    {
        public static SolidColorBrush GetColorFromHex(string colorHex)
        {
            return new SolidColorBrush((Color) ColorConverter.ConvertFromString(colorHex ?? "#000000"));
        }

        public static bool IsShiftKeyDown()
        {
            return Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.RightShift);
        }
    }
}
