﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Data;

namespace InfowareVSTO.MyControls
{
    public class FilterCombobox : ComboBox
    {
        ICollectionView view;
        private string filterText;
        TextBox editableTextBox;
        private bool IsTextChanging = false;
    
        protected override void OnItemsSourceChanged(IEnumerable oldValue, IEnumerable newValue)
        {
            if (newValue != null)
            {
                view = CollectionViewSource.GetDefaultView(newValue);
                view.Filter += FilterItem;
            }

            this.DropDownClosed += FilterCombobox_DropDownClosed;
            try
            {
                AttachTextBoxEvents();
            }
            catch
            {
                this.Loaded += FilterCombobox_Loaded;
            }
            base.OnItemsSourceChanged(oldValue, newValue);
        }

        private void AttachTextBoxEvents()
        {
            TextBox textBox = GetTemplateChild("PART_EditableTextBox") as TextBox;
            textBox.Padding = new System.Windows.Thickness(5, 0, 0, 0);
            textBox.VerticalAlignment = System.Windows.VerticalAlignment.Center;
         
            editableTextBox = textBox;
      
            textBox.PreviewTextInput += TextBox_PreviewTextInput;
            textBox.PreviewKeyDown += TextBox_PreviewKeyUp;
            textBox.TextChanged += TextBox_TextChanged;
            textBox.PreviewMouseLeftButtonDown += TextBox_PreviewMouseLeftButtonDown;
        }

        //private void TextBox_SelectionChanged(object sender, System.Windows.RoutedEventArgs e)
        //{
        //   if (filterText!= null && editableTextBox.SelectionStart < editableTextBox.Text.Length)
        //    {
        //        editableTextBox.SelectionStart = editableTextBox.Text.Length;
        //    }
        //}

        private void TextBox_PreviewMouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (filterText == null)
            {
                editableTextBox.Text = string.Empty;
                filterText = string.Empty;
                IsDropDownOpen = true;
            }
        }

        private void FilterCombobox_DropDownClosed(object sender, EventArgs e)
        {
            this.filterText = null;
            view.Refresh();
            if (SelectedItem is KeyValuePair<int, string>)
                this.Text = ((KeyValuePair<int, string>)SelectedItem).Value;
            editableTextBox.SelectionStart = editableTextBox.Text.Length;
        }

        private void FilterCombobox_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            AttachTextBoxEvents();
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (filterText != null && !IsTextChanging && editableTextBox.Text.Length == 1 && filterText.Length > 1)
            {
                IsTextChanging = true;
                editableTextBox.Text = filterText;
                editableTextBox.SelectionStart = editableTextBox.Text.Length;
                IsTextChanging = false;
            }
        }

        private void TextBox_PreviewKeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            TextBox textBox = sender as TextBox;
            switch (e.Key)
            {
                case System.Windows.Input.Key.Delete:
                    if (filterText == null)
                    {
                        filterText = string.Empty;
                    }
                    else if (textBox.SelectionLength > 0)
                    {
                        filterText = textBox.Text.Substring(0, textBox.SelectionStart) + textBox.Text.Substring(textBox.SelectionStart + textBox.SelectionLength);
                    }
                    else if (textBox.SelectionStart < textBox.Text.Length)
                    {
                        filterText = textBox.Text.Substring(0, textBox.SelectionStart) + textBox.Text.Substring(textBox.SelectionStart + 1);  
                    }
                    view.Refresh();
                    IsDropDownOpen = true;
                    break;
                case System.Windows.Input.Key.Back:
                    if (filterText == null)
                    {
                        filterText = string.Empty;
                        textBox.Text = string.Empty;
                    }
                    else if (textBox.SelectionLength > 0)
                    {
                        filterText = textBox.Text.Substring(0, textBox.SelectionStart) + textBox.Text.Substring(textBox.SelectionStart + textBox.SelectionLength);
                    }
                    else if (textBox.SelectionStart >0)
                    {
                        filterText = textBox.Text.Substring(0, textBox.SelectionStart - 1) + textBox.Text.Substring(textBox.SelectionStart);
                    }
                    view.Refresh();
                    IsDropDownOpen = true;
                    break;
            }
        }

        private void TextBox_PreviewTextInput(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            TextBox textBox = sender as TextBox;

            filterText = filterText == null ? e.Text : getComposedText(textBox.Text, textBox.SelectionStart, textBox.SelectionLength, e.Text);
            IsDropDownOpen = true;
            view.Refresh();
        }

        private string getComposedText(string text, int selectionStart, int selectionLength, string input)
        {
            return text.Substring(0, selectionStart) + input + text.Substring(selectionStart + selectionLength);
        }

        private bool FilterItem(object obj)
        {
            if (obj == null) return false;
            if (filterText == null || filterText.Length == 0) return true;

          var filterArr  = filterText.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

            foreach (string str in filterArr)
            {
                if (!(obj.ToString().ToLower().RemoveDiacritics().Contains(str.Trim().ToLower())) 
                    && !(obj.ToString().ToLower().Contains(str.Trim().ToLower())))
                {
                    return false;
                }
            }

            return true;
        }
    }
}
