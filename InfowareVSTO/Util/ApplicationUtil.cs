﻿using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Media;
using Microsoft.Office.Core;

namespace InfowareVSTO.Util
{
    internal static class ApplicationUtil
    {
        private static ColorConverter colorConverter;

        static ApplicationUtil()
        {
            colorConverter = new ColorConverter();
        }

        public static Color GetWpfColor(int msWordColor)
        {
            // http://blogs.microsoft.co.il/dorr/2008/11/16/converting-word-2007s-wdcolor-to-net-color-class/

            int transparency = (int) (msWordColor & 0xFF000000) >> 24;

            transparency = transparency == 0 ? 255 : transparency;

            int red = (msWordColor & 0x000000FF);

            int green = ((msWordColor & 0x0000FF00) >> 8);

            int blue = ((msWordColor & 0x00FF0000) >> 16);

            return System.Windows.Media.Color.FromArgb((byte) transparency, (byte) red, (byte) green, (byte) blue);
        }

        public static string GetHexColor(int msWordColor)
        {
            Color color = GetWpfColor(msWordColor);

            return colorConverter.ConvertToString(color);
        }

        /// <summary>
        /// Gets the name application's (MS Word) UI language
        /// </summary>
        /// <returns>the name application's (MS Word) UI language</returns>
        public static string GetUiLanguageName()
        {
            int wordUiLanguageId = Globals.ThisAddIn.Application.LanguageSettings.LanguageID[MsoAppLanguageID.msoLanguageIDUI];

            // More info about this: https://www.pinvoke.net/default.aspx/kernel32.getlocaleinfo
            int LOCALE_SENGLISHLANGUAGENAME = 0x00001001;

            /**
             * As per https://docs.microsoft.com/en-us/windows/desktop/api/winnls/nf-winnls-getlocaleinfoa, calling
             * the method with 0 for the last parameter, returns the amount of characters the language name will take
             */
            int requiredBufferSize = GetLocaleInfoA(wordUiLanguageId, LOCALE_SENGLISHLANGUAGENAME, null, 0);

            var resultBuffer = new StringBuilder(requiredBufferSize);

            int result = GetLocaleInfoA(wordUiLanguageId, LOCALE_SENGLISHLANGUAGENAME, resultBuffer, requiredBufferSize);

            string languageName = null;

            if (result == 0)
            {
                languageName = "English";
            }
            else
            {
                languageName = resultBuffer.ToString();
            }

            return languageName;
        }

        #region Native methods

        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern int GetLocaleInfoA(int localeId, int lcType, StringBuilder lpLcData, int cchData);

        #endregion
    }
}
