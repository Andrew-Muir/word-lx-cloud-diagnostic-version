﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfowareVSTO.Util
{
    public class CustomStyle : ICustomStyle
    {
        public string ButtonID { get; set; }
        public string Label { get; set; }
        public string StyleName { get; set; }

        public CustomStyle(string buttonID, string styleName, string label)
        {
            ButtonID = buttonID;
            Label = label;
            StyleName = styleName;
        }
    }

    public class CustomStyleSeparator : ICustomStyle { }

    public interface ICustomStyle
    {
    }
}
