﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Controls;
using InfowareVSTO.Common;
using InfowareVSTO.Common.General;
using InfowareVSTO.TaskPaneControls.WordDa;
using Microsoft.Office.Interop.Word;
using Microsoft.Office.Tools;

namespace InfowareVSTO.Util
{
    /// <summary>
    /// TODO: merge with TaskPaneUtil, replace WordDaTaskPaneContainer with TaskPaneContainer, then dynamically populate
    /// the container's element host with the WPF UserControls
    /// </summary>
    internal static class TaskPaneUtil2
    {
        public static CustomTaskPane CreateCustomTaskPane(string taskPaneTitle, UserControl content)
        {
            CustomTaskPaneCollection customTaskPanes = Globals.ThisAddIn.CustomTaskPanes;
            bool alreadyExists = false;
            CustomTaskPane existingTaskPane = null;

            List<CustomTaskPane> toRemove = new List<CustomTaskPane>();
            foreach (var element in customTaskPanes)
            {
                try
                {
                    if (element.Visible && (element.Window as Microsoft.Office.Interop.Word.Window)?.Document == ThisAddIn.Instance.Application.ActiveWindow.Document && (element.Control as WordDaTaskPaneContainer)?.MainElementHost?.Child?.GetType() == content.GetType())
                    {
                        alreadyExists = true;
                        existingTaskPane = element;
                    }
                }
                catch (COMException ex)
                {
                    toRemove.Add(element);
                }
            }

            foreach (var element in toRemove)
            {
                customTaskPanes.Remove(element);
            }

            if (!alreadyExists)
            {
                var container = new WordDaTaskPaneContainer();
                container.MainElementHost.Child = content;
                container.MainElementHost.Dock = System.Windows.Forms.DockStyle.Fill;
                return customTaskPanes.Add(container, taskPaneTitle);
            }
            else
            {
                return existingTaskPane;
            }
        }

        public static void CloseTaskpanesOfDocument(Document document)
        {
            CustomTaskPaneCollection customTaskPanes = Globals.ThisAddIn.CustomTaskPanes;
            List<CustomTaskPane> toRemove = new List<CustomTaskPane>();
            foreach (var element in customTaskPanes)
            {
                try
                {
                    if ((element.Window as Microsoft.Office.Interop.Word.Window)?.Document == ThisAddIn.Instance.Application.ActiveWindow.Document)
                    {
                        toRemove.Add(element);
                    }
                }
                catch (COMException ex)
                {
                    toRemove.Add(element);
                }
            }

            foreach (var element in toRemove)
            {
                customTaskPanes.Remove(element);
            }
        }

        public static void TryRestoreSelection(IRefreshData taskpane)
        {
            try
            {
                taskpane.Selection?.Select();
            }
            catch { }
        }

        public static void HideUnusedFields(IRefreshData taskpane)
        {
            if (taskpane != null && ThisAddIn.Instance.Application.ActiveDocument != null)
            {
                var contentControls = DocumentUtils.GetAllContentControls(ThisAddIn.Instance.Application.ActiveDocument, CustomConstants.TemplateTags)?.ToList();
                var fieldCC = GetFieldContentControls();

                UserControl userControl = taskpane as UserControl;
                bool anyFieldVisible = false;
                foreach (var field in fieldCC)
                {
                    FrameworkElement element = userControl.FindName(field.Key) as FrameworkElement;
                    bool thisFieldVisible = false;
                    if (element != null && field.Value.Intersect(contentControls.Select(x => x.Title)).ToList().Count == 0)
                    {
                        element.Visibility = Visibility.Collapsed;
                    }
                    else if (element != null)
                    {
                        thisFieldVisible = true;
                    }

                    if (element != null && field.Key == "OfficeLocationField")
                    {
                        ComboBox officeLocation = DocumentUtils.FindVisualChild<ComboBox>(element);
                        if (officeLocation != null && officeLocation.Items.Count < 2)
                        {
                            thisFieldVisible = false;
                            element.Visibility = Visibility.Collapsed;
                            if (officeLocation.Items.Count == 1)
                            {
                                officeLocation.SelectedIndex = 0;
                            }

                            if (taskpane is DataFillUserControl)
                            {
                                (taskpane as DataFillUserControl).OfficeLocationFound = true;
                            }
                        }
                    }

                    anyFieldVisible = anyFieldVisible || thisFieldVisible;
                }

                if (anyFieldVisible && taskpane is DataFillUserControl)
                {
                    (taskpane as DataFillUserControl).AnyFieldFound = true;
                }
            }
        }

        public static Dictionary<string, List<string>> GetFieldContentControls()
        {
            Dictionary<string, List<string>> dictionary = new Dictionary<string, List<string>>();
            dictionary["AuthorField"] = new List<string>(){
                "Author's Initials",
                "Full Name",
                "Degrees",
                "Certifications",
                "Direct Telephone",
                "Direct Fax",
                "Email",
                "Title",
                "Law Society Number Label",
                "Lawyer ID",
                "Mobile Phone",
                "User1",
                "User2",
                "User3"
            };
            dictionary["AssistantField"] = new List<string>(){
                "Assistant's Initials",
                "Assistant Title",
                "Assistant Name",
                "Assistant Phone",
                "Assistant Email"
            };
            dictionary["DateField"] = new List<string>() { "Date" };
            dictionary["SalutationField"] = new List<string>() { "Salutation" };
            dictionary["CCField"] = new List<string>() { "Courtesy Copy" };
            dictionary["BCCField"] = new List<string>() { "Blind Copy" };
            dictionary["DeliveryField"] = new List<string>() { "Delivery" };
            dictionary["HandlingField"] = new List<string>() { "Handling" };
            dictionary["AttentionField"] = new List<string>()
            {
                "Attention",
                "Address(es)",
                "Recipient(s)",
                "Recipient",
                "To (Full Name)"
            };
            dictionary["ReLineField"] = new List<string>() { "Re Line" };
            dictionary["diaryDateStackPanel"] = new List<string>() { "Diary Date" };
            dictionary["ClosingField"] = new List<string>() { "Closing" };
            dictionary["EnclosuresField"] = new List<string>() { "Enclosure" };
            dictionary["EnclosureNoField"] = new List<string>() { "Enclosure" };
            dictionary["IncludePerField"] = new List<string>() { "Include Per" };
            dictionary["StartAtField"] = new List<string>() { "Recipient" };
            dictionary["UseFirstField"] = new List<string>() { "Recipient" };
            dictionary["InitialsField"] = new List<string>() { "Author's Initials" };
            dictionary["NoOfPagesField"] = new List<string>() { "No. of Pages" };
            dictionary["PhoneNumberField"] = new List<string>() { "To (Phone Number)" };
            dictionary["FaxNumberField"] = new List<string>() { "To (Fax Number)" };
            dictionary["FirmField"] = new List<string>() { "To (Company)" };
            dictionary["ToField"] = new List<string>() { "To", "To (Full Name)" };
            dictionary["RecipientsField"] = new List<string>() {
                "Recipient",
                "Recipient(s)",
                "Address(es)",
                "To (Full Name)"
            };
            dictionary["FileNumberField"] = new List<string>() {
                "Our File Number",
                "File Number"
            };
            dictionary["ClientFileNumberField"] = new List<string>() {
                "Their File Number",
                "Client File Number"
            };
            dictionary["IncludeFirmNameField"] = new List<string>() {
                "Company Name",
                "Company Name Rich Text"
            };
            dictionary["IncludeAssistantBlockCheckbox"] = new List<string>() {
                "Assistant Block"
            };
            dictionary["OfficeLocationField"] = new List<string>() {
                "Company Address",
                "Company Address (Graphic)",
                "Company City",
                "Company Province",
                "Company Postal Code",
                "Company Phone",
                "Company Fax",
                "Direct Telephone",
                "Direct Fax",
                "Email",
                "Mobile Phone",
                "Assistant Phone",
                "Assistant Email"
            };

            return dictionary;
        }

        public static void AutoPopulateIfNeeded(IRefreshData taskpane)
        {
            if (ThisAddIn.Instance.Application.ActiveDocument != null && taskpane != null)
            {
                string property = DocumentPropertyUtil.ReadProperty("AutoPopulateTemplate");
                if (property != null && property.Trim().ToLower() == "true")
                {
                    taskpane.OKHandler();
                }
            }
        }
    }
}