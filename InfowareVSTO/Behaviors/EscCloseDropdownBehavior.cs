﻿using Microsoft.Xaml.Behaviors;
using System.Windows.Controls;

namespace InfowareVSTO.Behaviors
{
    public class EscCloseDropdownBehavior : Behavior<ComboBox>
    {
        protected override void OnAttached()
        {
            AssociatedObject.KeyUp += AssociatedObject_KeyUp;
        }

        private void AssociatedObject_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Escape)
            {
                AssociatedObject.IsDropDownOpen = false;
                e.Handled = true;
            }
        }

        protected override void OnDetaching()
        {
            AssociatedObject.KeyUp -= AssociatedObject_KeyUp;
        }
    }
}
