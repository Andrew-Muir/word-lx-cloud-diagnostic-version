﻿using InfowareVSTO.Windows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace InfowareVSTO
{
    /// <summary>
    /// Interaction logic for MultiPartyAgreement.xaml
    /// </summary>
    public partial class MultiPartyAgreement : InfowareWindow
    {
        public MultiPartyAgreement()
        {
            InitializeComponent();
        }

        public bool ResultOk { get; set; }
        public int NrOfParties { get; set; }
        public string ResultTitle { get; set; }
        
        private void Ok_Click(object sender, RoutedEventArgs e)
        {
            int result = 0;
            if (!int.TryParse(this.NrOfPartiesTB.Text, out result) || result < 1)
            {
                new InfowareInformationWindow("Please enter a number greater than 0.", "Number of Parties").ShowDialog();
                return;
            }

            ResultOk = true;
            NrOfParties = result;
            ResultTitle = this.TitleTB.Text;
            this.Close();
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            ResultOk = false;
            this.Close();
        }
    }
}
