﻿using InfowareVSTO.Common.DTOs;
using InfowareVSTO.Common.General;
using InfowareVSTO.Util;
using Microsoft.Office.Core;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace InfowareVSTO.MultiLanguage
{
    public class MLanguageUtil
    {
        private static Dictionary<MsoLanguageID, Dictionary<MLanguageResourceEnum, string>> resources;
        public static Dictionary<MsoLanguageID, Dictionary<MLanguageResourceEnum, string>> Resources
        {
            get
            {
                if (resources == null)
                {
                    resources = JsonConvert.DeserializeObject<Dictionary<MsoLanguageID, Dictionary<MLanguageResourceEnum, string>>>(ReadProjectTextFile("MultiLanguage\\MLanguageResourceFile.json"));
                }

                return resources;
            }
        }

        public static MsoLanguageID ActiveDocumentLanguage
        {
            get
            {
                try
                {
                    string languageStr = DocumentPropertyUtil.ReadProperty("WordLXDocumentLanguage");
                    if (int.TryParse(languageStr, out int languageInt))
                    {
                        return (MsoLanguageID)languageInt;
                    }

                    return DocumentUtils.GetDocumentLanguage(DocumentUtil.GetActiveDocument());
                }
                catch
                {
                    return MsoLanguageID.msoLanguageIDNone;
               }
            }
        }

        public static string ReadProjectTextFile(string path)
        {
            // the file needds to be embedded resource.
            Assembly assembly = Assembly.GetExecutingAssembly();

            var test = assembly.GetManifestResourceNames();

            string fullTemplatePath = test.Where(it => it.EndsWith(path.Replace(@"\", "."))).FirstOrDefault();

            if (fullTemplatePath != null)
            {
                using (var streamReader = new StreamReader(assembly.GetManifestResourceStream(fullTemplatePath)))
                {
                    return streamReader?.ReadToEnd();
                }
            }

            return null;
        }

        public static string GetResource(MLanguageResourceEnum resourceId, MsoLanguageID? languageID = null)
        {
            if (!languageID.HasValue)
            {
                languageID = ActiveDocumentLanguage;
            }

            string returnValue = null;
            Dictionary<MLanguageResourceEnum, string> tempDict = null;
            if (Resources.TryGetValue(languageID.Value, out tempDict) && tempDict != null)
            {
                if (tempDict.TryGetValue(resourceId, out returnValue))
                {
                    return returnValue;
                }
            }

            if (Resources.TryGetValue(MsoLanguageID.msoLanguageIDEnglishCanadian, out tempDict) && tempDict != null)
            {
                if (tempDict.TryGetValue(resourceId, out returnValue))
                {
                    return returnValue;
                }
            }

            return null;
        }

        public static PaneDTO ApplyDateSettings(PaneDTO letterDTO)
        {
            letterDTO.DateFormat = Utils.GetDefaultDateFormat();
            letterDTO.CultureId = GetResource(MLanguageResourceEnum.DateCultureID);
            return letterDTO;
        }
    }
}
