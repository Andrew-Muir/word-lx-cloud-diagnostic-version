﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace InfowareVSTO.MultiLanguage
{
    public enum MLanguageResourceEnum
    {
        TOCTitle = 0,
        TOCPage = 1,
        MPFileCopy = 2,
        MPCopy = 3,
        MPDraft = 4,
        DateCultureID = 5,
        PNPage = 6,
        PNOf = 7,
        DDSDraft = 8,
        DDSForDiscussionPurposesOnly = 9,
        DDSSavedBy = 10,
        ADDExhibit = 11,
        ADDSchedule = 12,
        ADDAppendix = 13,
        Attention = 14
    }
}