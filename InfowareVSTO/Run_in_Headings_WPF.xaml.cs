﻿using InfowareVSTO.Util;
using InfowareVSTO.Windows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace InfowareVSTO
{
    /// <summary>
    /// Interaction logic for Run_in_Headings_WPF.xaml
    /// </summary>
    public partial class Run_in_Headings_WPF : InfowareWindow
    {

        public Run_in_Headings_WPF()
        {
            InitializeComponent();

            this.Text_Box.Text = Globals.ThisAddIn.Application.Selection.Range.Text?.Trim();
            this.Text_Box.PreviewKeyDown += Text_Box_PreviewKeyDown;

        }

        private void Text_Box_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (ThisAddIn.Instance.ControlPressed && e.Key == Key.V)
            {
                string clipboardText = Clipboard.GetText();
                var originalSelection = DocumentUtil.GetSelection();
                var paragraph = originalSelection.Range.Paragraphs.First.Range;
                string paragraphListFormat = paragraph.ListFormat.ListString;

                if (!string.IsNullOrEmpty(paragraphListFormat))
                {
                    if (clipboardText.StartsWith(paragraphListFormat))
                    {
                        clipboardText = clipboardText.Substring(paragraphListFormat.Length).TrimStart();
                    }
                }

                this.Text_Box.SelectedText = clipboardText;
                this.Text_Box.SelectionStart = this.Text_Box.SelectionStart + this.Text_Box.SelectionLength;
                this.Text_Box.SelectionLength = 0;
                e.Handled = true;
            }
        }

        private string text, level;
        private bool multiple, page;

        public bool Page
        {
            get
            {
                return page;
            }
        }

        public bool Multiple
        {
            get
            {
                return multiple;
            }
        }

        public string Text
        {
            get
            {
                return text;
            }
          
        }

        public string Level
        {
            get
            {
                return level;
            }
        }

        private void OkButton_Click(object sender, RoutedEventArgs e)
        {
            text = this.Text_Box.Text.ToString();
            level = this.Level_Box.Text.ToString();
            int intlevel;

            if (text == "" && level == "")
            {
                this.Close();
                Null();
                return;

            }
           else if (text != "" && level == "")
            {
                level = "1";
            }

            if (!Int32.TryParse(level, out intlevel)) 
            { 
                new InfowareInformationWindow("In OutlineLevel must be a number between 1-8.").ShowDialog();
                this.Level_Box.Text = null;
                Null();
                return;
            }

            if(intlevel > 8)
            {
                new InfowareInformationWindow("In OutlineLevel must be a number between 1-8.").ShowDialog();
                this.Level_Box.Text = null;
                Null();
                return;
            }

            if(multiple_tables.IsChecked == true)
            {
                multiple = true;
            }
            else
            {
                multiple = false;
            }

            if(page_number.IsChecked == true)
            {
                page = true;
            }
            else
            {
                page = false;
            }
            
            this.Close();
        }

        private void CancelButton_Click_1(object sender, RoutedEventArgs e)
        {
            Null();
            this.Close();
        }

        public void Null()
        {
            level = "";
            text = "";
            page = false;
            multiple = false;
        }
    }
}
