﻿
using InfowareVSTO.Common;
using InfowareVSTO.Letterhead;
using InfowareVSTO.Windows;
using InfowareVSTO.MultiLanguage;
using InfowareVSTO.Util;
using Microsoft.Office.Interop.Word;
using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Net;
using System.Printing;
using System.Printing.Interop;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Word = Microsoft.Office.Interop.Word;
using Task = System.Threading.Tasks.Task;
using Window = System.Windows.Window;
using InfowareVSTO.Common.DTOs;
using InfowareVSTO.Common.Language;
using PrinterApis;

namespace InfowareVSTO
{
    /// <summary>
    /// Interaction logic for MultiPrint_Settings.xaml
    /// </summary>

    public partial class MultiPrint : InfowareWindow
    {
        //const string ProgramDataTemplatesPath = @"C:\ProgramData\Infoware\Templates\4105\";
        const int DMDUP_SIMPLEX = 1;
        const int DMDUP_VERTICAL = 2;
        const int DMDUP_HORIZONTAL = 3;
        const int DMCOLOR_MONOCHROME = 1;
        const int DMCOLOR_COLOR = 2;
      
        public MultiPrint(IMultiPrintSettings _mPSettings)
        {
            MPSettings = _mPSettings;
            InitializeComponent();
            Initialize(MPSettings);

            if (SettingsManager.GetSettingAsBool("InsertDocIDOnPlain", "MultiPrint", false))
            {
                PlainPaperDocID.Visibility = Visibility.Visible;
            }
            if (SettingsManager.GetSettingAsBool("InsertDocIDOnFileCopy", "MultiPrint", false))
            {
                FileCopyDocID.Visibility = Visibility.Visible;
            }
        }
        Word.Document document = ThisAddIn.Instance.Application.ActiveDocument;
        List<int> index_printer = new List<int>();
        List<PrinterDescription> PrinterTickets = new List<PrinterDescription>();

        [DllImport("winspool.Drv", EntryPoint = "DocumentPropertiesW", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        private static extern int DocumentProperties(IntPtr hwnd, IntPtr hPrinter, [MarshalAs(UnmanagedType.LPWStr)] string pDeviceName, IntPtr pDevModeOutput, IntPtr pDevModeInput, int fMode);

        [DllImport("kernel32.dll")]
        private static extern IntPtr GlobalLock(IntPtr hMem);

        [DllImport("kernel32.dll")]
        private static extern bool GlobalUnlock(IntPtr hMem);

        [DllImport("kernel32.dll")]
        private static extern bool GlobalFree(IntPtr hMem);

        [DllImport("winspool.drv", CharSet = CharSet.Ansi, SetLastError = true)]
        private static extern bool SetPrinter(IntPtr hPrinter, int Level, IntPtr pPrinter, int Command);

        public class PrinterDescription
        {
            public string FullName { get; set; }
            public int ClientPrintSchemaVersion { get; set; }
            public PrintTicket DefaultPrintTicket { get; set; }
        }

        public IMultiPrintSettings MPSettings { get; set; }

        public string SelectedPrinterName { get; set; }

        private void Initialize(IMultiPrintSettings MPSettings)
        {
            PrinterSettings ps = new PrinterSettings();
            ThisAddIn.Instance.PreMultiPrintActivePrinterName = ps.PrinterName;
            foreach (string printer in PrinterSettings.InstalledPrinters)
            {
                PrinterCB.Items.Add(printer);
            }
            PrinterCB.SelectedValue = ps.PrinterName;
            for (int i = 0; i < 8; i++)
            {
                PaperSource_List.Add(null);
            }
            Letterhead_Check.IsEnabled = true;
            Letterhead_Text.IsEnabled = true;
            var specialPapersTypes = SettingsManager.GetSetting("SpecialPaperTypes", "MultiPrint");
            if (!string.IsNullOrWhiteSpace(specialPapersTypes))
            {
                this.SpecialPaper_Checkbox.IsEnabled = true;
                this.SpecialPaperCB.Visibility = Visibility.Visible;
                string[] specialPapersTypesArr = specialPapersTypes.Split(',').Select(x => x.Trim()).ToArray();
                this.SpecialPaperCB.ItemsSource = specialPapersTypesArr;
                this.SpecialPaperCB.SelectedIndex = 0;
            }
            //check if PDF Special printer is set
            if (string.IsNullOrEmpty(MPSettings.SpecialPrinterSettings.PdfPrinter) || string.IsNullOrEmpty(MPSettings.SpecialPrinterSettings.PdfBin))
                PDF_Checkbox.IsEnabled = false;
            //AddKeyBindings();
        }

        private void FillInPrinterProperties(string printerName)
        {
            Duplex_Combo.IsEnabled = PrinterSettingsApis.GetPrinterHasDuplex(printerName);
            Color_Check.IsEnabled = PrinterSettingsApis.GetPrinterHasColor(printerName);

            if (!Duplex_Combo.IsEnabled)
            {
                Duplex_Combo.SelectedIndex = 0;
            }
            else
            {
                int selectedDuplex = PrinterSettingsApis.GetPrinterDuplex(printerName, out string error);
                if (string.IsNullOrWhiteSpace(error))
                {
                    switch (selectedDuplex)
                    {
                        case DMDUP_SIMPLEX:
                            Duplex_Combo.SelectedIndex = 0;
                            break;
                        case DMDUP_HORIZONTAL:
                            Duplex_Combo.SelectedIndex = 2;
                            break;
                        case DMDUP_VERTICAL:
                            Duplex_Combo.SelectedIndex = 1;
                            break;
                    }
                }
            }

            if (!Color_Check.IsEnabled)
            {
                Color_Check.IsChecked = false;
            }
            else
            {
                int selectedColor = PrinterSettingsApis.GetPrinterColor(printerName, out string error);
                if (string.IsNullOrWhiteSpace(error))
                {
                    switch (selectedColor)
                    {
                        case DMCOLOR_COLOR:
                            Color_Check.IsChecked = true;
                            break;
                        case DMCOLOR_MONOCHROME:
                            Color_Check.IsChecked = false;
                            break;
                    }
                }
            }
        }

        int count;
        object missing = System.Reflection.Missing.Value;
        object copies = "1";
        object pages = "";
        object range;
        object items = System.Reflection.Missing.Value;
        object pageType = Word.WdPrintOutPages.wdPrintAllPages;
        object oTrue = true;
        object oFalse = false;
        object manualDuplex = false;
        private bool watermarkStyleCreated = false;
        List<PaperSource> PaperSource_List = new List<PaperSource>();

        private void Ok_Click(object sender, RoutedEventArgs e)
        {
            count = 0;
            int checkedCount = GetCheckboxCount();
            int cancelCount = 0;
            bool? originalLetterheadProperty = LetterheadToggler.GetLetterheadProperty();
            Word.Range initialSelection = ThisAddIn.Instance.Application.Selection.Range;

            //////////
            // INITIALIZATION
            // Validate inputs in the dialog box - printer range, number of copies
            //////////
            using (new DocumentUndoTransaction("MultiPrint Init"))
            {
                count++;
                Word.Document document = ThisAddIn.Instance.Application.ActiveDocument;
                CreateNullAction(document);
                PrintDocument Pdoc = new PrintDocument();
                document.Application.ActivePrinter = PrinterCB.Text;
                MultiPrintSettings_WPF.SelectedPrinterName = PrinterCB.Text;

                if (CurrentPage_Button.IsChecked == true)
                {
                    range = Word.WdPrintOutRange.wdPrintCurrentPage;
                    pages = "1";
                }
                else if (Selection_Button.IsChecked == true)
                {
                    range = Word.WdPrintOutRange.wdPrintSelection;
                }
                else
                {
                    if (range_text.Text == "," || range_text.Text == "")
                    {
                        range = Word.WdPrintOutRange.wdPrintAllDocument;
                    }
                    else
                    {
                        range = Word.WdPrintOutRange.wdPrintRangeOfPages;
                        string regex = @"^(\s*((\d{1,3})|(\d{1,3}\s*-\s*\d{1,3})|(s\d{1,3})|(s\d{1,3}\s*-\s*s\d{1,3})|(p\d{1,3}s\d{1,3})|(p\d{1,3}s\d{1,3}\s*-\s*p\d{1,3}s\d{1,3}))\s*)(,\s*((\d{1,3})|(\d{1,3}\s*-\s*\d{1,3})|(s\d{1,3})|(s\d{1,3}\s*-\s*s\d{1,3})|(p\d{1,3}s\d{1,3})|(p\d{1,3}s\d{1,3}\s*-\s*p\d{1,3}s\d{1,3}))\s*)*$";

                        if (Regex.IsMatch(range_text.Text, regex))
                        {
                            pages = range_text.Text;
                        }
                        else
                        {
                            new InfowareInformationWindow("The print range has incorrect format!").ShowDialog();
                            return;
                        }
                    }
                }

                // validate user input for # of copies, for each paper type
                if (IsValidNumberOfCopies(Final_Check, Final_Text) == false)
                    return;
                if (IsValidNumberOfCopies(PlainPaper_Check, PlainPaper_Text) == false)
                    return;
                if (IsValidNumberOfCopies(Letterhead_Check, Letterhead_Text) == false)
                    return;
                if (IsValidNumberOfCopies(FileCopy_Check, FileCopy_Text) == false)
                    return;
                if (IsValidNumberOfCopies(TrayOverride_Check, TrayOverride_Text) == false)
                    return;
                if (IsValidNumberOfCopies(Envelope_Checkbox, Envelope_Textbox) == false)
                    return;
                if (IsValidNumberOfCopies(AddressLabel_Checkbox, AddressLabel_Textbox) == false)
                    return;

                // if none of the paper types are checked, treat it as Final
                if (checkedCount == 0)
                {
                    Final_Check.IsChecked = true;
                    Final_Text.Text = "1";
                    checkedCount++;
                }
            }

            //////////
            // COLOUR PRINTING
            //////////
            if (Color_Check.IsChecked != true)
            {
                using (new DocumentUndoTransaction("MultiPrint Color"))
                {
                    count++;
                    CreateNullAction(document);
                    SetPrinterColor(PrinterCB.Text, false);
                }
            }

            //////////
            // STAMPS
            //////////
            using (new DocumentUndoTransaction("MultiPrint Stamps"))
            {
                count++;
                CreateNullAction(document);

                Word.Shape shape;

                if (FileCopyStamp_Checkbox.IsChecked == true)
                {
                    ThisAddIn.Instance.Application.ScreenUpdating = false;
                    if (ThisAddIn.Instance.IsFunctionForLogging("ScreenUpdating"))
                        Logger.Log("Disabled Screen Updating (MultiPrint.xaml.cs > Ok_CLick> Stamps)");
                    foreach (Microsoft.Office.Interop.Word.Section section in document.Sections)
                    {
                        shape = section.Headers[Microsoft.Office.Interop.Word.WdHeaderFooterIndex.wdHeaderFooterPrimary].Shapes.AddTextbox(Microsoft.Office.Core.MsoTextOrientation.msoTextOrientationHorizontal, 0, 0, 240, 48);
                        shape.TextFrame.TextRange.Text = MLanguageUtil.GetResource(MLanguageResourceEnum.MPFileCopy);
                        shape.TextFrame.TextRange.Paragraphs.set_Style(GetOrCreateWatermarkStyle().NameLocal);
                        PositionStamp(shape);

                        if (section.Headers[Microsoft.Office.Interop.Word.WdHeaderFooterIndex.wdHeaderFooterFirstPage].Exists)
                        {
                            shape = section.Headers[Microsoft.Office.Interop.Word.WdHeaderFooterIndex.wdHeaderFooterFirstPage].Shapes.AddTextbox(Microsoft.Office.Core.MsoTextOrientation.msoTextOrientationHorizontal, 0, 0, 240, 48);
                            shape.TextFrame.TextRange.Text = MLanguageUtil.GetResource(MLanguageResourceEnum.MPFileCopy);
                            shape.TextFrame.TextRange.Paragraphs.set_Style(GetOrCreateWatermarkStyle().NameLocal);
                            PositionStamp(shape);
                        }

                        if (section.Headers[Microsoft.Office.Interop.Word.WdHeaderFooterIndex.wdHeaderFooterEvenPages].Exists)
                        {
                            shape = section.Headers[Microsoft.Office.Interop.Word.WdHeaderFooterIndex.wdHeaderFooterEvenPages].Shapes.AddTextbox(Microsoft.Office.Core.MsoTextOrientation.msoTextOrientationHorizontal, 0, 0, 240, 48);
                            shape.TextFrame.TextRange.Text = MLanguageUtil.GetResource(MLanguageResourceEnum.MPFileCopy);
                            shape.TextFrame.TextRange.Paragraphs.set_Style(GetOrCreateWatermarkStyle().NameLocal);
                            PositionStamp(shape);
                        }
                    }
                    ThisAddIn.Instance.Application.ScreenUpdating = true;
                    if (ThisAddIn.Instance.IsFunctionForLogging("ScreenUpdating"))
                        Logger.Log("Enabled Screen Updating (MultiPrint.xaml.cs >  Ok_CLick> Stamps)");
                }

                if (CopyStamp_Checkbox.IsChecked == true)
                {
                    ThisAddIn.Instance.Application.ScreenUpdating = false;
                    if (ThisAddIn.Instance.IsFunctionForLogging("ScreenUpdating"))
                        Logger.Log("Disabled Screen Updating (MultiPrint.xaml.cs > Ok_CLick> Stamps)");
                    foreach (Microsoft.Office.Interop.Word.Section section in document.Sections)
                    {
                        shape = section.Headers[Microsoft.Office.Interop.Word.WdHeaderFooterIndex.wdHeaderFooterPrimary].Shapes.AddTextbox(Microsoft.Office.Core.MsoTextOrientation.msoTextOrientationHorizontal, 0, 0, 240, 48);
                        shape.TextFrame.TextRange.Text = MLanguageUtil.GetResource(MLanguageResourceEnum.MPCopy);
                        shape.TextFrame.TextRange.Paragraphs.set_Style(GetOrCreateWatermarkStyle().NameLocal);
                        PositionStamp(shape);

                        if (section.Headers[Microsoft.Office.Interop.Word.WdHeaderFooterIndex.wdHeaderFooterFirstPage].Exists)
                        {
                            shape = section.Headers[Microsoft.Office.Interop.Word.WdHeaderFooterIndex.wdHeaderFooterFirstPage].Shapes.AddTextbox(Microsoft.Office.Core.MsoTextOrientation.msoTextOrientationHorizontal, 0, 0, 240, 48);
                            shape.TextFrame.TextRange.Text = MLanguageUtil.GetResource(MLanguageResourceEnum.MPCopy);
                            shape.TextFrame.TextRange.Paragraphs.set_Style(GetOrCreateWatermarkStyle().NameLocal);
                            PositionStamp(shape);
                        }

                        if (section.Headers[Microsoft.Office.Interop.Word.WdHeaderFooterIndex.wdHeaderFooterEvenPages].Exists)
                        {
                            shape = section.Headers[Microsoft.Office.Interop.Word.WdHeaderFooterIndex.wdHeaderFooterEvenPages].Shapes.AddTextbox(Microsoft.Office.Core.MsoTextOrientation.msoTextOrientationHorizontal, 0, 0, 240, 48);
                            shape.TextFrame.TextRange.Text = MLanguageUtil.GetResource(MLanguageResourceEnum.MPCopy);
                            shape.TextFrame.TextRange.Paragraphs.set_Style(GetOrCreateWatermarkStyle().NameLocal);
                            PositionStamp(shape);
                        }
                    }
                    ThisAddIn.Instance.Application.ScreenUpdating = true;
                    if (ThisAddIn.Instance.IsFunctionForLogging("ScreenUpdating"))
                        Logger.Log("Enabled Screen Updating (MultiPrint.xaml.cs >  Ok_CLick> Stamps)");
                }

                if (DraftStamp_Checkbox.IsChecked == true)
                {
                    ThisAddIn.Instance.Application.ScreenUpdating = false;
                    if (ThisAddIn.Instance.IsFunctionForLogging("ScreenUpdating"))
                        Logger.Log("Disabled Screen Updating (MultiPrint.xaml.cs > Ok_CLick> Stamps)");
                    foreach (Microsoft.Office.Interop.Word.Section section in document.Sections)
                    {
                        shape = section.Headers[Microsoft.Office.Interop.Word.WdHeaderFooterIndex.wdHeaderFooterPrimary].Shapes.AddTextbox(Microsoft.Office.Core.MsoTextOrientation.msoTextOrientationHorizontal, 0, 0, 240, 48);
                        shape.TextFrame.TextRange.Text = MLanguageUtil.GetResource(MLanguageResourceEnum.MPDraft);
                        shape.TextFrame.TextRange.Paragraphs.set_Style(GetOrCreateWatermarkStyle().NameLocal);
                        PositionStamp(shape);

                        if (section.Headers[Microsoft.Office.Interop.Word.WdHeaderFooterIndex.wdHeaderFooterFirstPage].Exists)
                        {
                            shape = section.Headers[Microsoft.Office.Interop.Word.WdHeaderFooterIndex.wdHeaderFooterFirstPage].Shapes.AddTextbox(Microsoft.Office.Core.MsoTextOrientation.msoTextOrientationHorizontal, 0, 0, 240, 48);
                            shape.TextFrame.TextRange.Text = MLanguageUtil.GetResource(MLanguageResourceEnum.MPDraft);
                            shape.TextFrame.TextRange.Paragraphs.set_Style(GetOrCreateWatermarkStyle().NameLocal);
                            PositionStamp(shape);
                        }

                        if (section.Headers[Microsoft.Office.Interop.Word.WdHeaderFooterIndex.wdHeaderFooterEvenPages].Exists)
                        {
                            shape = section.Headers[Microsoft.Office.Interop.Word.WdHeaderFooterIndex.wdHeaderFooterEvenPages].Shapes.AddTextbox(Microsoft.Office.Core.MsoTextOrientation.msoTextOrientationHorizontal, 0, 0, 240, 48);
                            shape.TextFrame.TextRange.Text = MLanguageUtil.GetResource(MLanguageResourceEnum.MPDraft);
                            shape.TextFrame.TextRange.Paragraphs.set_Style(GetOrCreateWatermarkStyle().NameLocal);
                            PositionStamp(shape);
                        }
                    }
                    ThisAddIn.Instance.Application.ScreenUpdating = true;
                    if (ThisAddIn.Instance.IsFunctionForLogging("ScreenUpdating"))
                        Logger.Log("Enabled Screen Updating (MultiPrint.xaml.cs > Ok_CLick> Stamps)");
                }
            }

            //////////
            // Set paper trays based on page size
            //////////
            using (new DocumentUndoTransaction("MultiPrint Page Setup"))
            {
                count++;
                CreateNullAction(document);
                if (document.PageSetup.PaperSize == Microsoft.Office.Interop.Word.WdPaperSize.wdPaperA4 || document.PageSetup.PaperSize == Microsoft.Office.Interop.Word.WdPaperSize.wdPaperA4Small)
                {
                    if (PaperSource_List[1] != null)
                    {
                        document.PageSetup.OtherPagesTray = (Microsoft.Office.Interop.Word.WdPaperTray)PaperSource_List[1].RawKind;
                        document.PageSetup.FirstPageTray = (Microsoft.Office.Interop.Word.WdPaperTray)PaperSource_List[1].RawKind;
                    }
                    else
                    {
                        document.PageSetup.OtherPagesTray = Microsoft.Office.Interop.Word.WdPaperTray.wdPrinterDefaultBin;
                        document.PageSetup.FirstPageTray = Microsoft.Office.Interop.Word.WdPaperTray.wdPrinterDefaultBin;
                    }
                }

                if (document.PageSetup.PaperSize == Microsoft.Office.Interop.Word.WdPaperSize.wdPaperEnvelope10 ||
                    document.PageSetup.PaperSize == Microsoft.Office.Interop.Word.WdPaperSize.wdPaperEnvelope11 ||
                    document.PageSetup.PaperSize == Microsoft.Office.Interop.Word.WdPaperSize.wdPaperEnvelope12 ||
                    document.PageSetup.PaperSize == Microsoft.Office.Interop.Word.WdPaperSize.wdPaperEnvelope14 ||
                    document.PageSetup.PaperSize == Microsoft.Office.Interop.Word.WdPaperSize.wdPaperEnvelope9 ||
                    document.PageSetup.PaperSize == Microsoft.Office.Interop.Word.WdPaperSize.wdPaperEnvelopeB4 ||
                    document.PageSetup.PaperSize == Microsoft.Office.Interop.Word.WdPaperSize.wdPaperEnvelopeB5 ||
                    document.PageSetup.PaperSize == Microsoft.Office.Interop.Word.WdPaperSize.wdPaperEnvelopeB6 ||
                    document.PageSetup.PaperSize == Microsoft.Office.Interop.Word.WdPaperSize.wdPaperEnvelopeC3 ||
                    document.PageSetup.PaperSize == Microsoft.Office.Interop.Word.WdPaperSize.wdPaperEnvelopeC4 ||
                    document.PageSetup.PaperSize == Microsoft.Office.Interop.Word.WdPaperSize.wdPaperEnvelopeC5 ||
                    document.PageSetup.PaperSize == Microsoft.Office.Interop.Word.WdPaperSize.wdPaperEnvelopeC6 ||
                    document.PageSetup.PaperSize == Microsoft.Office.Interop.Word.WdPaperSize.wdPaperEnvelopeC65 ||
                    document.PageSetup.PaperSize == Microsoft.Office.Interop.Word.WdPaperSize.wdPaperEnvelopeDL ||
                    document.PageSetup.PaperSize == Microsoft.Office.Interop.Word.WdPaperSize.wdPaperEnvelopeItaly ||
                    document.PageSetup.PaperSize == Microsoft.Office.Interop.Word.WdPaperSize.wdPaperEnvelopeMonarch ||
                    document.PageSetup.PaperSize == Microsoft.Office.Interop.Word.WdPaperSize.wdPaperEnvelopePersonal
                  )
                {
                    if (PaperSource_List[2] != null)
                    {
                        document.PageSetup.OtherPagesTray = (Microsoft.Office.Interop.Word.WdPaperTray)PaperSource_List[2].RawKind;
                        document.PageSetup.FirstPageTray = (Microsoft.Office.Interop.Word.WdPaperTray)PaperSource_List[2].RawKind;
                    }
                    else
                    {
                        document.PageSetup.OtherPagesTray = Microsoft.Office.Interop.Word.WdPaperTray.wdPrinterDefaultBin;
                        document.PageSetup.FirstPageTray = Microsoft.Office.Interop.Word.WdPaperTray.wdPrinterDefaultBin;
                    }
                }

                if (document.PageSetup.PaperSize == Microsoft.Office.Interop.Word.WdPaperSize.wdPaperLegal)
                {
                    if (PaperSource_List[7] != null)
                    {
                        document.PageSetup.OtherPagesTray = (Microsoft.Office.Interop.Word.WdPaperTray)PaperSource_List[7].RawKind;
                        document.PageSetup.FirstPageTray = (Microsoft.Office.Interop.Word.WdPaperTray)PaperSource_List[7].RawKind;
                    }
                    else
                    {
                        document.PageSetup.OtherPagesTray = Microsoft.Office.Interop.Word.WdPaperTray.wdPrinterDefaultBin;
                        document.PageSetup.FirstPageTray = Microsoft.Office.Interop.Word.WdPaperTray.wdPrinterDefaultBin;
                    }
                }
            }

            //////////
            // ENVELOPES
            //////////
            string envelopeLabelsSelectedRecipient = null;
            string envelopeLabelsSelectedAttention = string.Empty;

            if (Envelope_Checkbox.IsChecked == true || AddressLabel_Checkbox.IsChecked == true)
            {
                Word.Paragraph envelopeLabelsSelectionRecipients = DocumentUtil.GetSelection().Range.Paragraphs.First;
                if (((Word.Style)envelopeLabelsSelectionRecipients.get_Style()).NameLocal == "Address")
                {
                    envelopeLabelsSelectedRecipient = envelopeLabelsSelectionRecipients.Range.Text?.Trim();
                }

                Word.Paragraph envelopeLabelsSelectionAttention = envelopeLabelsSelectionRecipients.Next();
                if (envelopeLabelsSelectionAttention != null && ((Word.Style)envelopeLabelsSelectionAttention.get_Style()).NameLocal == "AttnLine" && envelopeLabelsSelectionAttention.Range.Text?.Contains("\t") == true)
                {
                    envelopeLabelsSelectedAttention = string.Join("\t", envelopeLabelsSelectionAttention.Range.Text.Split('\t').Skip(1));
                }
            }

            if (Envelope_Checkbox.IsChecked == true)
            {
                using (new DocumentUndoTransaction("MultiPrint Envelope"))
                {
                    CreateNullAction(document);
                    var multiPrintEnvelope = new MultiPrintEnvelope_WPF();

                    if (Utils.ActiveDocumentIsLetter(document))
                    {
                        PaneDTO paneDTO = TaskPaneUtil.LoadAnswers(document: document);
                        multiPrintEnvelope.LoadAnswers(paneDTO);
                    }
                    if (!string.IsNullOrWhiteSpace(envelopeLabelsSelectedRecipient))
                    {
                        multiPrintEnvelope.textBoxRecipientsEnvelopeMultiPrint.Text = envelopeLabelsSelectedRecipient;
                        multiPrintEnvelope.textBoxAttentionEnvelopeMultiPrint.Text = envelopeLabelsSelectedAttention;
                        multiPrintEnvelope.ButtonOK_Click(null, null);
                    }
                    else
                    {
                        multiPrintEnvelope.ShowDialog();
                    }

                    if (multiPrintEnvelope.DialogResult == true || !string.IsNullOrWhiteSpace(envelopeLabelsSelectedRecipient))
                    {
                        PrinterSettings ps = new PrinterSettings();
                        if (MPSettings.SpecialPrinterSettings != null && !string.IsNullOrWhiteSpace(MPSettings.SpecialPrinterSettings.EnvelopePrinter) && MPSettings.SpecialPrinterSettings.EnvelopePrinter != CustomConstants.DefaultPrinter)
                        {
                            ps.PrinterName = MPSettings.SpecialPrinterSettings.EnvelopePrinter;
                            multiPrintEnvelope.EnvelopeDocument.Application.ActivePrinter = MPSettings.SpecialPrinterSettings.EnvelopePrinter;
                            try
                            {
                                multiPrintEnvelope.EnvelopeDocument.PageSetup.FirstPageTray = Utils.GetPaperTray(MPSettings.SpecialPrinterSettings.EnvelopePrinter, MPSettings.SpecialPrinterSettings.EnvelopeBin);
                            }
                            catch
                            {
                                SetPrinterTrayOnSections(multiPrintEnvelope.EnvelopeDocument, Utils.GetPaperTray(MPSettings.SpecialPrinterSettings.EnvelopePrinter, MPSettings.SpecialPrinterSettings.EnvelopeBin));
                            }
                        }
                        else if (MPSettings.PrinterBinSettings != null && MPSettings.PrinterBinSettings.Where(x => x.Name == PrinterCB.Text).Count() > 0 && !string.IsNullOrWhiteSpace(MPSettings.PrinterBinSettings.Where(x => x.Name == PrinterCB.Text).First().EnvelopesBin))
                        {
                            ps.PrinterName = PrinterCB.Text;
                            multiPrintEnvelope.EnvelopeDocument.Application.ActivePrinter = PrinterCB.Text;
                            try
                            {
                                multiPrintEnvelope.EnvelopeDocument.PageSetup.FirstPageTray = Utils.GetPaperTray(PrinterCB.Text, MPSettings.PrinterBinSettings.Where(x => x.Name == PrinterCB.Text).First().EnvelopesBin);
                            }
                            catch
                            {
                                SetPrinterTrayOnSections(multiPrintEnvelope.EnvelopeDocument, Utils.GetPaperTray(PrinterCB.Text, MPSettings.PrinterBinSettings.Where(x => x.Name == PrinterCB.Text).First().EnvelopesBin));
                            }
                        }

                        int.TryParse(Envelope_Textbox.Text, out int nrCopies);

                        multiPrintEnvelope.EnvelopeDocument.PrintOut(Background: true, Range: range, Item: items, Copies: nrCopies.ToString(), Pages: pages, PageType: pageType, PrintToFile: false, Collate: true, ManualDuplexPrint: manualDuplex);
                        multiPrintEnvelope.EnvelopeDocument.Saved = true;
                        multiPrintEnvelope.EnvelopeDocument.Close(Word.WdSaveOptions.wdDoNotSaveChanges);
                        this.Close();
                    }
                    else
                    {
                        cancelCount++;
                    }
                }

                document.Undo(1);
            }

            //////////
            // ADDRESS LABEL
            //////////
            if (AddressLabel_Checkbox.IsChecked == true)
            {
                using (new DocumentUndoTransaction("MultiPrint Address Labels"))
                {
                    CreateNullAction(document);


                    var multiPrintLabel = new MultiPrintLabel_WPF(MPSettings.GetMultiPrintSettingsObject(), SelectedPrinterName);

                    if (Utils.ActiveDocumentIsLetter(document))
                    {
                        PaneDTO paneDTO = TaskPaneUtil.LoadAnswers(document: document);
                        multiPrintLabel.LoadAnswers(paneDTO);
                    }

                    if (!string.IsNullOrWhiteSpace(envelopeLabelsSelectedRecipient))
                    {
                        multiPrintLabel.textBoxRecipientsLabelMultiPrint.Text = envelopeLabelsSelectedRecipient;
                        multiPrintLabel.textBoxAttentionLabelMultiPrint.Text = envelopeLabelsSelectedAttention;
                        multiPrintLabel.ButtonOK_Click(null, null);
                    }
                    else
                    {
                        multiPrintLabel.ShowDialog();
                    }

                    if (multiPrintLabel.DialogResult == true || !string.IsNullOrWhiteSpace(envelopeLabelsSelectedRecipient))
                    {
                        PrinterSettings ps = new PrinterSettings();
                        if (MPSettings.SpecialPrinterSettings != null && !string.IsNullOrWhiteSpace(MPSettings.SpecialPrinterSettings.AddressLabelPrinter))
                        {
                            ps.PrinterName = MPSettings.SpecialPrinterSettings.AddressLabelPrinter;
                            multiPrintLabel.LabelDocument.Application.ActivePrinter = MPSettings.SpecialPrinterSettings.AddressLabelPrinter;
                            try
                            {
                                multiPrintLabel.LabelDocument.PageSetup.FirstPageTray = Utils.GetPaperTray(MPSettings.SpecialPrinterSettings.AddressLabelPrinter, MPSettings.SpecialPrinterSettings.AddressLabelBin);
                            }
                            catch
                            {
                                SetPrinterTrayOnSections(multiPrintLabel.LabelDocument, Utils.GetPaperTray(MPSettings.SpecialPrinterSettings.AddressLabelPrinter, MPSettings.SpecialPrinterSettings.AddressLabelBin));
                            }
                        }
                        else if (MPSettings.PrinterBinSettings != null && MPSettings.PrinterBinSettings.Where(x => x.Name == PrinterCB.Text).Count() > 0 && !string.IsNullOrWhiteSpace(MPSettings.PrinterBinSettings.Where(x => x.Name == PrinterCB.Text).First().AddressLabelBin))
                        {
                            ps.PrinterName = PrinterCB.Text;
                            multiPrintLabel.LabelDocument.Application.ActivePrinter = PrinterCB.Text;
                            try
                            {
                                multiPrintLabel.LabelDocument.PageSetup.FirstPageTray = Utils.GetPaperTray(PrinterCB.Text, MPSettings.PrinterBinSettings.Where(x => x.Name == PrinterCB.Text).First().AddressLabelBin);
                            }
                            catch
                            {
                                SetPrinterTrayOnSections(multiPrintLabel.LabelDocument, Utils.GetPaperTray(PrinterCB.Text, MPSettings.PrinterBinSettings.Where(x => x.Name == PrinterCB.Text).First().AddressLabelBin));
                            }
                        }

                        int.TryParse(AddressLabel_Textbox.Text, out int nrCopies);

                        multiPrintLabel.LabelDocument.PrintOut(Background: true, Range: range, Item: items, Copies: nrCopies.ToString(), Pages: pages, PageType: pageType, PrintToFile: false, Collate: true, ManualDuplexPrint: false);
                        multiPrintLabel.LabelDocument.Saved = true;
                        multiPrintLabel.LabelDocument.Close(Word.WdSaveOptions.wdDoNotSaveChanges);
                        this.Close();
                    }
                    else
                    {
                        cancelCount++;
                    }
                }

                document.Undo(1);
            }

            var initialDuplexValue = -1;
            var currentPrintJobsCount = ThisAddIn.Instance.Application.BackgroundPrintingStatus;

            //////////
            // DUPLEX
            //////////
            if (Duplex_Combo.IsEnabled && (checkedCount == 0 || checkedCount - cancelCount > 0))
            {
                using (new DocumentUndoTransaction("MultiPrint Duplex"))
                {
                    CreateNullAction(document);
                    count++;

                    initialDuplexValue = SetPrinterDuplex(PrinterCB.Text, initialDuplexValue, Duplex_Combo.SelectedIndex);
                }
            }



            var selectedPrinterBinSettings = MPSettings.PrinterBinSettings.Where(x => x.Name == PrinterCB.Text).FirstOrDefault();
            if (selectedPrinterBinSettings == null)
            {
                MultiPrintSettings_WPF.SelectedPrinterName = PrinterCB.Text;
                var optionsDialog = new MultiPrintSettings_WPF(MPSettings, document);
                optionsDialog.General_OK_Click(null, null);
                MPSettings = optionsDialog.MPSettings;
                selectedPrinterBinSettings = MPSettings.PrinterBinSettings.Where(x => x.Name == PrinterCB.Text).FirstOrDefault();
            }

            //////////
            // FORCE PROMPTS TO BLACK
            //////////
            if (selectedPrinterBinSettings != null && selectedPrinterBinSettings.ForcePromptsBlackAndWhite == true)
            {
                using (new DocumentUndoTransaction("MultiPrint Prompts"))
                {
                    CreateNullAction(document);
                    count++;
                    try
                    {
                        document.Styles["Prompt"].Font.Color = Word.WdColor.wdColorBlack;

                    }
                    // style does not exist
                    catch { }
                }
            }

            //////////
            // FINAL
            //////////
            if (Final_Check.IsChecked == true)
            {
                using (new DocumentUndoTransaction("MultiPrint Final"))
                {
                    CreateNullAction(document);

                    var letterHeadOn = DocumentPropertyUtil.ReadProperty(CustomConstants.LETTERHEAD_ON);
                    if (letterHeadOn != null && bool.Parse(letterHeadOn))
                    {
                        if ((selectedPrinterBinSettings?.LetterHeadPrePrinted ?? false) == true)
                        {
                            HideLetterhead(document);
                        }
                    }
                    else
                    {
                        if ((selectedPrinterBinSettings?.LetterHeadPrePrinted ?? false) == false)
                        {
                            ShowLetterhead(document);
                        }
                    }

                    string printerName = PrinterCB.Text, printerBin = CustomConstants.DefaultPrinterBin, printerBin2nd = null;
                    bool? isDuplex;
                    bool currentPrinterDuplex = Duplex_Combo.IsEnabled && Duplex_Combo.SelectedIndex > 0;
                    string paperType = Utils.GetDocumentVariable(document, "PaperType");
                    string letterheadTemplateIdStr = Utils.GetDocumentVariable(document, "LetterheadTemplateId");

                    int.TryParse(letterheadTemplateIdStr, out int letterheadTemplateId);

                    bool specialPaperFound = false;
                    if (string.IsNullOrEmpty(paperType))
                        paperType = "plain";

                    Dictionary<int, SpecialHandlingSetting> specialSectionHandlingSettings = GetSectionSpecialHandlingSettings();


                    string infoRouteToSpecialPrinter = Utils.GetDocumentVariable(document, "InfoRouteToSpecialPtr");
                    if (!string.IsNullOrEmpty(infoRouteToSpecialPrinter))
                    {
                        // support for legacy document variable that
                        // directs a document to the label printer
                        if (infoRouteToSpecialPrinter.ToLower() == "labelptr")
                            paperType = "label";
                    }

                    if (MPSettings.SpecialPaperSettingsDict != null && MPSettings.SpecialPaperSettingsDict.TryGetValue(paperType, out var value) && !string.IsNullOrWhiteSpace(value.Printer))
                    {
                        // Handling for Special Paper type
                        printerName = value.Printer;
                        printerBin = value.Bin1 ?? CustomConstants.DefaultPrinterBin;
                        printerBin2nd = value.Bin2 ?? CustomConstants.DefaultPrinterBin;

                        SetPrinterDuplex(value.Printer, initialDuplexValue, value.Duplex ?? 0);
                        SetPrinterColor(value.Printer, value.Color == true);
                        specialPaperFound = true;
                    }
                    else if (paperType.ToLower() == "plain")
                    {
                        printerName = PrinterCB.Text;
                        if (MPSettings.PrinterBinSettings != null && MPSettings.PrinterBinSettings.Where(x => x.Name == PrinterCB.Text).Count() > 0 && !string.IsNullOrWhiteSpace(MPSettings.PrinterBinSettings.Where(x => x.Name == PrinterCB.Text).First().PlainPaperBin))
                        {
                            printerBin = MPSettings.PrinterBinSettings.Where(x => x.Name == PrinterCB.Text).First().PlainPaperBin;
                            isDuplex = MPSettings.PrinterBinSettings.Where(x => x.Name == PrinterCB.Text).First().PlainPaperIsDuplex;
                        }
                        else
                        {
                            isDuplex = currentPrinterDuplex;
                        }
                    }
                    else if (paperType.ToLower() == "letterhead")
                    {
                        if (MPSettings.SpecialPrinterSettings != null && !string.IsNullOrEmpty(MPSettings.SpecialPrinterSettings.LetterheadPrinter))
                        {
                            printerName = MPSettings.SpecialPrinterSettings.LetterheadPrinter;
                            printerBin = MPSettings.SpecialPrinterSettings.Letterhead1Bin;
                            printerBin2nd = MPSettings.SpecialPrinterSettings.Letterhead2Bin;
                            isDuplex = false;
                        }
                        else if (MPSettings.PrinterBinSettings != null && MPSettings.PrinterBinSettings.Where(x => x.Name == PrinterCB.Text).Count() > 0 && !string.IsNullOrWhiteSpace(MPSettings.PrinterBinSettings.Where(x => x.Name == PrinterCB.Text).First().Letterhead1Bin))
                        {
                            printerName = PrinterCB.Text;
                            printerBin = MPSettings.PrinterBinSettings.Where(x => x.Name == PrinterCB.Text).First().Letterhead1Bin;
                            printerBin2nd = MPSettings.PrinterBinSettings.Where(x => x.Name == PrinterCB.Text).First().Letterhead2Bin;
                            isDuplex = MPSettings.PrinterBinSettings.Where(x => x.Name == PrinterCB.Text).First().Letterhead2IsDuplex;
                        }
                        else
                        {
                            printerName = PrinterCB.Text;
                            isDuplex = currentPrinterDuplex;
                        }
                    }
                    else if (paperType.ToLower() == "envelope")
                    {
                        if (MPSettings.SpecialPrinterSettings != null && !string.IsNullOrEmpty(MPSettings.SpecialPrinterSettings.EnvelopePrinter))
                        {
                            printerName = MPSettings.SpecialPrinterSettings.EnvelopePrinter;
                            printerBin = MPSettings.SpecialPrinterSettings.EnvelopeBin;
                            isDuplex = false;
                        }
                        else if (MPSettings.PrinterBinSettings != null && MPSettings.PrinterBinSettings.Where(x => x.Name == PrinterCB.Text).Count() > 0 && !string.IsNullOrWhiteSpace(MPSettings.PrinterBinSettings.Where(x => x.Name == PrinterCB.Text).First().EnvelopesBin))
                        {
                            printerName = PrinterCB.Text;
                            printerBin = MPSettings.PrinterBinSettings.Where(x => x.Name == PrinterCB.Text).First().EnvelopesBin;
                            isDuplex = false;
                        }
                        else
                        {
                            printerName = PrinterCB.Text;
                            isDuplex = false;
                        }
                    }
                    else if (paperType.ToLower() == "label")
                    {
                        if (MPSettings.SpecialPrinterSettings != null && !string.IsNullOrEmpty(MPSettings.SpecialPrinterSettings.AddressLabelPrinter))
                        {
                            printerName = MPSettings.SpecialPrinterSettings.AddressLabelPrinter;
                            printerBin = MPSettings.SpecialPrinterSettings.AddressLabelBin;
                            isDuplex = false;
                        }
                        else if (MPSettings.PrinterBinSettings != null && MPSettings.PrinterBinSettings.Where(x => x.Name == PrinterCB.Text).Count() > 0 && !string.IsNullOrWhiteSpace(MPSettings.PrinterBinSettings.Where(x => x.Name == PrinterCB.Text).First().AddressLabelBin))
                        {
                            printerName = PrinterCB.Text;
                            printerBin = MPSettings.PrinterBinSettings.Where(x => x.Name == PrinterCB.Text).First().AddressLabelBin;
                            isDuplex = false;
                        }
                        else
                        {
                            printerName = PrinterCB.Text;
                            isDuplex = currentPrinterDuplex;
                        }
                    }
                    else if (paperType.ToLower() == "filecopy")
                    {
                        if (MPSettings.SpecialPrinterSettings != null && !string.IsNullOrEmpty(MPSettings.SpecialPrinterSettings.FileCopyPrinter))
                        {
                            printerName = MPSettings.SpecialPrinterSettings.FileCopyPrinter;
                            printerBin = MPSettings.SpecialPrinterSettings.FileCopyBin;
                            isDuplex = false;
                        }
                        else if (MPSettings.PrinterBinSettings != null && MPSettings.PrinterBinSettings.Where(x => x.Name == PrinterCB.Text).Count() > 0 && !string.IsNullOrWhiteSpace(MPSettings.PrinterBinSettings.Where(x => x.Name == PrinterCB.Text).First().FileCopyBin))
                        {
                            printerName = PrinterCB.Text;
                            printerBin = MPSettings.PrinterBinSettings.Where(x => x.Name == PrinterCB.Text).First().FileCopyBin;
                            isDuplex = MPSettings.PrinterBinSettings.Where(x => x.Name == PrinterCB.Text).First().FileCopyIsDuplex;
                        }
                        else
                        {
                            printerName = PrinterCB.Text;
                            isDuplex = currentPrinterDuplex;
                        }
                    }
                    else if (paperType.ToLower() == "a4")
                    {
                        if (MPSettings.SpecialPrinterSettings != null && !string.IsNullOrEmpty(MPSettings.SpecialPrinterSettings.A4Printer))
                        {
                            printerName = MPSettings.SpecialPrinterSettings.A4Printer;
                            printerBin = MPSettings.SpecialPrinterSettings.A4Bin;
                            isDuplex = false;
                        }
                        else if (MPSettings.PrinterBinSettings != null && MPSettings.PrinterBinSettings.Where(x => x.Name == PrinterCB.Text).Count() > 0 && !string.IsNullOrWhiteSpace(MPSettings.PrinterBinSettings.Where(x => x.Name == PrinterCB.Text).First().A4Bin))
                        {
                            printerName = PrinterCB.Text;
                            printerBin = MPSettings.PrinterBinSettings.Where(x => x.Name == PrinterCB.Text).First().A4Bin;
                            isDuplex = MPSettings.PrinterBinSettings.Where(x => x.Name == PrinterCB.Text).First().A4IsDuplex;
                        }
                        else
                        {
                            printerName = PrinterCB.Text;
                            isDuplex = currentPrinterDuplex;
                        }
                    }
                    else if (paperType.ToLower() == "legal")
                    {
                        if (MPSettings.SpecialPrinterSettings != null && !string.IsNullOrEmpty(MPSettings.SpecialPrinterSettings.LegalPrinter))
                        {
                            printerName = MPSettings.SpecialPrinterSettings.LegalPrinter;
                            printerBin = MPSettings.SpecialPrinterSettings.LegalBin;
                            isDuplex = false;
                        }
                        else if (MPSettings.PrinterBinSettings != null && MPSettings.PrinterBinSettings.Where(x => x.Name == PrinterCB.Text).Count() > 0 && !string.IsNullOrWhiteSpace(MPSettings.PrinterBinSettings.Where(x => x.Name == PrinterCB.Text).First().LegalBin))
                        {
                            printerName = PrinterCB.Text;
                            printerBin = MPSettings.PrinterBinSettings.Where(x => x.Name == PrinterCB.Text).First().LegalBin;
                            isDuplex = MPSettings.PrinterBinSettings.Where(x => x.Name == PrinterCB.Text).First().LegalIsDuplex;
                        }
                        else
                        {
                            printerName = PrinterCB.Text;
                            isDuplex = currentPrinterDuplex;
                        }
                    }
                    else
                    {
                        printerName = PrinterCB.Text;
                        if (MPSettings.PrinterBinSettings != null && MPSettings.PrinterBinSettings.Where(x => x.Name == PrinterCB.Text).Count() > 0 && !string.IsNullOrWhiteSpace(MPSettings.PrinterBinSettings.Where(x => x.Name == PrinterCB.Text).First().PlainPaperBin))
                        {
                            printerBin = MPSettings.PrinterBinSettings.Where(x => x.Name == PrinterCB.Text).First().PlainPaperBin;
                            isDuplex = MPSettings.PrinterBinSettings.Where(x => x.Name == PrinterCB.Text).First().PlainPaperIsDuplex;
                        }
                        else
                        {
                            isDuplex = currentPrinterDuplex;
                        }
                    }

                    Utils.SetPrinterAndBin(document, printerName, printerBin, printerBin2nd);

                    foreach (var sectionSettings in specialSectionHandlingSettings)
                    {
                        string sPrinterBin = null, sPrinterBin2nd = null;
                        if (sectionSettings.Value.PaperType.ToLower() == "plain")
                        {
                            if (MPSettings.PrinterBinSettings != null && MPSettings.PrinterBinSettings.Where(x => x.Name == printerName).Count() > 0 && !string.IsNullOrWhiteSpace(MPSettings.PrinterBinSettings.Where(x => x.Name == printerName).First().PlainPaperBin))
                            {
                                sPrinterBin = MPSettings.PrinterBinSettings.Where(x => x.Name == printerName).First().PlainPaperBin;
                            }
                        }
                        else if (sectionSettings.Value.PaperType.ToLower() == "letterhead")
                        {
                            if (MPSettings.PrinterBinSettings != null && MPSettings.PrinterBinSettings.Where(x => x.Name == printerName).Count() > 0 && !string.IsNullOrWhiteSpace(MPSettings.PrinterBinSettings.Where(x => x.Name == printerName).First().Letterhead1Bin))
                            {
                                sPrinterBin = MPSettings.PrinterBinSettings.Where(x => x.Name == printerName).First().Letterhead1Bin;
                                sPrinterBin2nd = MPSettings.PrinterBinSettings.Where(x => x.Name == printerName).First().Letterhead2Bin;
                            }
                        }
                        else if (sectionSettings.Value.PaperType.ToLower() == "2ndpageletterhead")
                        {
                            if (MPSettings.PrinterBinSettings != null && MPSettings.PrinterBinSettings.Where(x => x.Name == printerName).Count() > 0 && !string.IsNullOrWhiteSpace(MPSettings.PrinterBinSettings.Where(x => x.Name == printerName).First().Letterhead2Bin))
                            {
                                sPrinterBin = MPSettings.PrinterBinSettings.Where(x => x.Name == printerName).First().Letterhead2Bin;
                                sPrinterBin2nd = MPSettings.PrinterBinSettings.Where(x => x.Name == printerName).First().Letterhead2Bin;
                            }
                        }
                        else if (sectionSettings.Value.PaperType.ToLower() == "envelope")
                        {
                            if (MPSettings.PrinterBinSettings != null && MPSettings.PrinterBinSettings.Where(x => x.Name == printerName).Count() > 0 && !string.IsNullOrWhiteSpace(MPSettings.PrinterBinSettings.Where(x => x.Name == printerName).First().EnvelopesBin))
                            {
                                sPrinterBin = MPSettings.PrinterBinSettings.Where(x => x.Name == printerName).First().EnvelopesBin;
                            }
                        }
                        else if (sectionSettings.Value.PaperType.ToLower() == "label")
                        {
                            if (MPSettings.PrinterBinSettings != null && MPSettings.PrinterBinSettings.Where(x => x.Name == printerName).Count() > 0 && !string.IsNullOrWhiteSpace(MPSettings.PrinterBinSettings.Where(x => x.Name == printerName).First().AddressLabelBin))
                            {
                                sPrinterBin = MPSettings.PrinterBinSettings.Where(x => x.Name == printerName).First().AddressLabelBin;
                            }
                        }
                        else if (sectionSettings.Value.PaperType.ToLower() == "filecopy")
                        {
                            if (MPSettings.PrinterBinSettings != null && MPSettings.PrinterBinSettings.Where(x => x.Name == printerName).Count() > 0 && !string.IsNullOrWhiteSpace(MPSettings.PrinterBinSettings.Where(x => x.Name == printerName).First().FileCopyBin))
                            {
                                sPrinterBin = MPSettings.PrinterBinSettings.Where(x => x.Name == printerName).First().FileCopyBin;
                            }
                        }
                        else if (sectionSettings.Value.PaperType.ToLower() == "a4")
                        {
                            if (MPSettings.PrinterBinSettings != null && MPSettings.PrinterBinSettings.Where(x => x.Name == printerName).Count() > 0 && !string.IsNullOrWhiteSpace(MPSettings.PrinterBinSettings.Where(x => x.Name == printerName).First().A4Bin))
                            {
                                sPrinterBin = MPSettings.PrinterBinSettings.Where(x => x.Name == printerName).First().A4Bin;
                            }
                        }
                        else if (sectionSettings.Value.PaperType.ToLower() == "legal")
                        {
                            if (MPSettings.PrinterBinSettings != null && MPSettings.PrinterBinSettings.Where(x => x.Name == printerName).Count() > 0 && !string.IsNullOrWhiteSpace(MPSettings.PrinterBinSettings.Where(x => x.Name == printerName).First().LegalBin))
                            {
                                sPrinterBin = MPSettings.PrinterBinSettings.Where(x => x.Name == printerName).First().LegalBin;
                            }
                        }

                        if (sPrinterBin != null)
                        {
                            document.Sections[sectionSettings.Key].PageSetup.FirstPageTray = Utils.GetPaperTray(printerName, sPrinterBin);
                        }
                        if (sPrinterBin2nd != null)
                        {
                            document.Sections[sectionSettings.Key].PageSetup.OtherPagesTray = Utils.GetPaperTray(printerName, sPrinterBin2nd);
                        }

                        if (MPSettings.SpecialPaperSettingsDict != null && MPSettings.SpecialPaperSettingsDict.TryGetValue(sectionSettings.Value.PaperType, out value) && !string.IsNullOrWhiteSpace(value.Printer) && printerName == value.Printer)
                        {
                            if (value.Bin1 != null)
                            {
                                document.Sections[sectionSettings.Key].PageSetup.FirstPageTray = Utils.GetPaperTray(printerName, value.Bin1);
                            }
                            if (value.Bin2 != null)
                            {
                                document.Sections[sectionSettings.Key].PageSetup.OtherPagesTray = Utils.GetPaperTray(printerName, value.Bin2);
                            }
                        }
                    }


                    Dictionary<int, int> letterheadSections = new Dictionary<int, int>();

                    if (letterheadTemplateId > 0)
                    {
                        foreach (Word.Section section in document.Sections)
                        {
                            letterheadSections[section.Index] = letterheadTemplateId;
                        }
                    }

                    foreach (var sectionSetting in specialSectionHandlingSettings)
                    {
                        if ((sectionSetting.Value.TemplateId ?? 0) > 0)
                        {
                            letterheadSections[sectionSetting.Key] = sectionSetting.Value.TemplateId.Value;
                        }
                    }

                    LetterheadToggler lt = new LetterheadToggler();
                    foreach (var sectionSetting in letterheadSections)
                    {
                        lt.AddSectionLetterhead(document, sectionSetting.Key, sectionSetting.Value);
                    }

                    bool hideDocID = SettingsManager.GetSettingAsBool("HideDocIDOnFinal", "MultiPrint", false);
                    Word.WdColor? docIdColor = null;
                    if (hideDocID)
                    {
                        try
                        {
                            docIdColor = document.Styles["DocsID"].Font.Color;
                            document.Styles["DocsID"].Font.Color = Word.WdColor.wdColorWhite;
                        }
                        catch { }
                    }

                    int nrCopies = 1;
                    int.TryParse(Final_Text.Text, out nrCopies);
                    document.PrintOut(Background: true, Range: range, Item: items, Copies: nrCopies.ToString(), Pages: pages, PageType: pageType, PrintToFile: false, Collate: true, ManualDuplexPrint: manualDuplex);

                    if (specialPaperFound)
                    {
                        SetPrinterDuplex(printerName, initialDuplexValue, Duplex_Combo.IsEnabled ? Duplex_Combo.SelectedIndex : 0);
                        SetPrinterColor(printerName, Color_Check.IsChecked == true);
                    }
                }

                document.Undo(1);
            }

            //////////
            // PLAIN PAPER
            //////////
            if (PlainPaper_Check.IsChecked == true)
            {
                using (new DocumentUndoTransaction("MultiPrint Plain Paper"))
                {
                    CreateNullAction(document);
                    if (selectedPrinterBinSettings != null)
                        Utils.SetPrinterAndBin(document, PrinterCB.Text, selectedPrinterBinSettings.PlainPaperBin);
                    else
                        Utils.SetPrinterAndBin(document, PrinterCB.Text, CustomConstants.DefaultPrinterBin);

                    bool hideDocID = this.PlainPaperDocID.Visibility == Visibility.Visible && PlainPaperDocID.IsChecked == false;
                    Word.WdColor? docIdColor = null;
                    if (hideDocID)
                    {
                        try
                        {
                            docIdColor = document.Styles["DocsID"].Font.Color;
                            document.Styles["DocsID"].Font.Color = Word.WdColor.wdColorWhite;
                        }
                        catch { }
                    }

                    int nrCopies = 1;
                    int.TryParse(PlainPaper_Text.Text, out nrCopies);
                    document.PrintOut(Background: true, Range: range, Item: items, Copies: nrCopies.ToString(), Pages: pages, PageType: pageType, PrintToFile: false, Collate: true, ManualDuplexPrint: manualDuplex);
                }
                document.Undo(1);
            }

            //////////
            // LETTERHEAD ON PLAIN
            //////////
            if (Letterhead_Check.IsEnabled && Letterhead_Check.IsChecked == true)
            {
                using (new DocumentUndoTransaction("MultiPrint Letterhead"))
                {
                    CreateNullAction(document);
                    string hasLetterhead = Util.DocumentPropertyUtil.ReadProperty(CustomConstants.LETTERHEAD_ON);
                    Letterhead.LetterheadToggler letterheadToggler = null;
                    if (hasLetterhead != Letterhead.LetterheadToggler.TRUE)
                    {
                        letterheadToggler = new Letterhead.LetterheadToggler();
                        letterheadToggler.ToggleLetterhead(true); //we need to save shortProperty, otherwise we will not know we have to hide it as undo doesn't seem to work
                    }

                    int nrCopies = 1;
                    int.TryParse(Letterhead_Text.Text, out nrCopies);
                    document.PrintOut(Background: true, Range: range, Item: items, Copies: nrCopies.ToString(), Pages: pages, PageType: pageType, PrintToFile: false, Collate: true, ManualDuplexPrint: manualDuplex);
                }
                document.Undo(1);
            }

            //////////
            // FILE COPY
            //////////
            if (FileCopy_Check.IsChecked == true)
            {
                //////////
                // HIDDEN TEXT
                //////////
                string texts = "";
                if (IncludeHiddenText_Checkbox.IsChecked == true)
                {
                    using (new DocumentUndoTransaction("MultiPrint Hidden Text"))
                    {
                        CreateNullAction(document);
                        string hiddenWhiteListStylesStr = SettingsManager.GetSetting("HiddenStylesToPrint", "MultiPrint", "bcclist", MLanguageUtil.ActiveDocumentLanguage);
                        List<string> hiddenWhiteListStylesList = hiddenWhiteListStylesStr.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(x => SettingsManager.UnescapeCommaCharacter(x.Trim())).ToList();

                        foreach (string styleName in hiddenWhiteListStylesList)
                        {
                            Word.Style style = Utils.GetStyleByName(styleName);

                            if (style != null)
                            {
                                style.Font.Hidden = 0;
                            }
                        }

                        var rangeAll = document.Range();
                        rangeAll.TextRetrievalMode.IncludeHiddenText = true;
                        foreach (Microsoft.Office.Interop.Word.Range p in rangeAll.Words)
                        {



                            texts += p.Text;
                            if (p.Font.Hidden != 0 && hiddenWhiteListStylesList.Contains((p.Paragraphs[1].get_Style() as Word.Style)?.NameLocal))
                            {
                                p.Font.Hidden = 0;
                            }
                        }
                    }
                }

                using (new DocumentUndoTransaction("MultiPrint File Copy"))
                {
                    CreateNullAction(document);
                    string printerName, printerBin;
                    bool? isDuplex;
                    if (MPSettings.SpecialPrinterSettings != null && !string.IsNullOrWhiteSpace(MPSettings.SpecialPrinterSettings.FileCopyPrinter) && !string.IsNullOrWhiteSpace(MPSettings.SpecialPrinterSettings.FileCopyBin))
                    {
                        printerName = MPSettings.SpecialPrinterSettings.FileCopyPrinter;
                        printerBin = MPSettings.SpecialPrinterSettings.FileCopyBin;
                        isDuplex = null;
                    }
                    else if (selectedPrinterBinSettings != null)
                    {
                        printerName = selectedPrinterBinSettings.Name;
                        printerBin = selectedPrinterBinSettings.FileCopyBin;
                        isDuplex = selectedPrinterBinSettings.FileCopyIsDuplex;
                    }
                    else
                    {
                        printerName = PrinterCB.Text;
                        printerBin = CustomConstants.DefaultPrinterBin;
                        isDuplex = Duplex_Combo.IsEnabled && Duplex_Combo.SelectedIndex > 0;
                    }
                    Utils.SetPrinterAndBin(document, printerName, printerBin);
                    ThisAddIn.Instance.Application.ScreenUpdating = false;
                    if (ThisAddIn.Instance.IsFunctionForLogging("ScreenUpdating"))
                        Logger.Log("Disabled Screen Updating (MultiPrint.xaml.cs > Ok_Click> File Copy)");
                    foreach (Microsoft.Office.Interop.Word.Section section in document.Sections)
                    {
                        if (section.Headers[Microsoft.Office.Interop.Word.WdHeaderFooterIndex.wdHeaderFooterFirstPage].Exists)
                        {
                            Word.Shape shape = section.Headers[Microsoft.Office.Interop.Word.WdHeaderFooterIndex.wdHeaderFooterFirstPage].Shapes.AddTextbox(Microsoft.Office.Core.MsoTextOrientation.msoTextOrientationHorizontal, 0, 0, 240, 48);
                            shape.TextFrame.TextRange.Text = MLanguageUtil.GetResource(MLanguageResourceEnum.MPFileCopy);
                            shape.TextFrame.TextRange.Paragraphs.set_Style(GetOrCreateWatermarkStyle().NameLocal);
                            PositionStamp(shape);
                        }
                        if (section.Headers[Microsoft.Office.Interop.Word.WdHeaderFooterIndex.wdHeaderFooterEvenPages].Exists)
                        {
                            Word.Shape shape = section.Headers[Microsoft.Office.Interop.Word.WdHeaderFooterIndex.wdHeaderFooterEvenPages].Shapes.AddTextbox(Microsoft.Office.Core.MsoTextOrientation.msoTextOrientationHorizontal, 0, 0, 240, 48);
                            shape.TextFrame.TextRange.Text = MLanguageUtil.GetResource(MLanguageResourceEnum.MPFileCopy);
                            shape.TextFrame.TextRange.Paragraphs.set_Style(GetOrCreateWatermarkStyle().NameLocal);
                            PositionStamp(shape);
                        }

                        {
                            Word.Shape shape = section.Headers[Microsoft.Office.Interop.Word.WdHeaderFooterIndex.wdHeaderFooterPrimary].Shapes.AddTextbox(Microsoft.Office.Core.MsoTextOrientation.msoTextOrientationHorizontal, 0, 0, 240, 48);
                            shape.TextFrame.TextRange.Text = MLanguageUtil.GetResource(MLanguageResourceEnum.MPFileCopy);
                            shape.TextFrame.TextRange.Paragraphs.set_Style(GetOrCreateWatermarkStyle().NameLocal);
                            PositionStamp(shape);
                        }
                    }
                    ThisAddIn.Instance.Application.ScreenUpdating = true;
                    if (ThisAddIn.Instance.IsFunctionForLogging("ScreenUpdating"))
                        Logger.Log("Enabled Screen Updating (MultiPrint.xaml.cs > Ok_Click> File Copy)");
                    bool hideDocID = FileCopyDocID.Visibility == Visibility.Visible && FileCopyDocID.IsChecked == false;
                    Word.WdColor? docIdColor = null;
                    if (hideDocID)
                    {
                        try
                        {
                            docIdColor = document.Styles["DocsID"].Font.Color;
                            document.Styles["DocsID"].Font.Color = Word.WdColor.wdColorWhite;
                        }
                        catch { }
                    }

                    int nrCopies = 1;
                    int.TryParse(FileCopy_Text.Text, out nrCopies);
                    document.PrintOut(Background: true, Range: range, Item: items, Copies: nrCopies.ToString(), Pages: pages, PageType: pageType, PrintToFile: false, Collate: true, ManualDuplexPrint: manualDuplex);
                }
                document.Undo(1);

                if (IncludeHiddenText_Checkbox.IsChecked == true)
                {
                    document.Undo(1);
                }
            }

            //////////
            // TRAY OVERRIDE
            //////////
            if (TrayOverride_Check.IsChecked == true)
            {
                using (new DocumentUndoTransaction("MultiPrint Tray Override"))
                {
                    CreateNullAction(document);
                    count++;
                    string printerName, printerBin;
                    bool? isDuplex;
                    if (MPSettings.SpecialPrinterSettings != null && !string.IsNullOrWhiteSpace(MPSettings.SpecialPrinterSettings.FileCopyPrinter) && !string.IsNullOrWhiteSpace(MPSettings.SpecialPrinterSettings.FileCopyBin))
                    {
                        printerName = MPSettings.SpecialPrinterSettings.FileCopyPrinter;
                        printerBin = MPSettings.SpecialPrinterSettings.FileCopyBin;
                        isDuplex = null;
                    }
                    else if (selectedPrinterBinSettings != null)
                    {
                        printerName = selectedPrinterBinSettings.Name;
                        printerBin = selectedPrinterBinSettings.FileCopyBin;
                        isDuplex = selectedPrinterBinSettings.FileCopyIsDuplex;
                    }
                    else
                    {
                        printerName = PrinterCB.Text;
                        printerBin = CustomConstants.DefaultPrinterBin;
                        isDuplex = Duplex_Combo.IsEnabled && Duplex_Combo.SelectedIndex > 0;
                    }
                    Utils.SetPrinterAndBin(document, PrinterCB.Text, TrayOverrideBin_Combobox.SelectedValue.ToString());

                    int nrCopies = 1;
                    int.TryParse(TrayOverride_Text.Text, out nrCopies);
                    document.PrintOut(Background: true, Range: range, Item: items, Copies: nrCopies.ToString(), Pages: pages, PageType: pageType, PrintToFile: false, Collate: true, ManualDuplexPrint: manualDuplex);
                }
            }

            //////////
            // PDF
            //////////
            if (PDF_Checkbox.IsChecked == true)
            {
                using (new DocumentUndoTransaction("MultiPrint PDF"))
                {
                    CreateNullAction(document);
                    if (MPSettings.SpecialPrinterSettings.PdfPrinter == "Create PDF document")
                    {
                        string pdfPath = string.Format("{0}\\{1}.pdf", document.Path, document.Name);
                        if ((Word.WdPrintOutRange)range == Word.WdPrintOutRange.wdPrintRangeOfPages)
                        {
                            string[] pagesArray = pages.ToString().Split(',');
                            if (pagesArray.Count() >= 2)
                                document.ExportAsFixedFormat(pdfPath, Word.WdExportFormat.wdExportFormatPDF, OpenAfterExport: true, Range: Word.WdExportRange.wdExportFromTo, From: Convert.ToInt32(pagesArray[0]), To: Convert.ToInt32(pagesArray[1]));
                            else
                                document.ExportAsFixedFormat(pdfPath, Word.WdExportFormat.wdExportFormatPDF, OpenAfterExport: true, Range: Word.WdExportRange.wdExportFromTo, From: Convert.ToInt32(pagesArray[0]), To: Convert.ToInt32(pagesArray[0]));

                        }
                        else if ((Word.WdPrintOutRange)range == Word.WdPrintOutRange.wdPrintCurrentPage)
                        {
                            document.ExportAsFixedFormat(pdfPath, Word.WdExportFormat.wdExportFormatPDF, OpenAfterExport: true, Range: Word.WdExportRange.wdExportCurrentPage);
                        }
                        else if ((Word.WdPrintOutRange)range == Word.WdPrintOutRange.wdPrintSelection)
                        {
                            document.ExportAsFixedFormat(pdfPath, Word.WdExportFormat.wdExportFormatPDF, OpenAfterExport: true, Range: Word.WdExportRange.wdExportSelection);
                        }
                        else
                        {
                            document.ExportAsFixedFormat(pdfPath, Word.WdExportFormat.wdExportFormatPDF, OpenAfterExport: true);
                        }
                    }
                    else
                    {
                        Utils.SetPrinterAndBin(document, MPSettings.SpecialPrinterSettings.PdfPrinter, MPSettings.SpecialPrinterSettings.PdfBin, null);
                        document.PrintOut(Background: true, Range: range, Item: items, Copies: copies, Pages: pages, PageType: pageType, PrintToFile: false, ManualDuplexPrint: false);
                    }
                }
                document.Undo(1);
            }

            //////////
            // SPECIAL PAPER OVERRIDE
            //////////
            if (SpecialPaper_Checkbox.IsChecked == true)
            {
                using (new DocumentUndoTransaction("MultiPrint Special Paper Print"))
                {
                    count++;
                    CreateNullAction(document);

                    string specialPaperType = SpecialPaperCB.SelectedItem as string;

                    Utils.SetPrinterAndBin(document, PrinterCB.Text, CustomConstants.DefaultPrinterBin);

                    if (MPSettings.SpecialPaperSettingsDict != null && MPSettings.SpecialPaperSettingsDict.TryGetValue(specialPaperType, out var value) && value?.Printer != null)
                    {

                        Utils.SetPrinterAndBin(document, value.Printer, value.Bin1 ?? CustomConstants.DefaultPrinterBin, value.Bin2 ?? CustomConstants.DefaultPrinterBin);
                        if (value.Duplex >= 0)
                        {
                            SetPrinterDuplex(value.Printer, 0, value.Duplex ?? 0);
                        }

                        SetPrinterColor(value.Printer, value.Color == true);

                    }

                    document.PrintOut(Background: true, Range: range, Item: items, Copies: copies, Pages: pages, PageType: pageType, PrintToFile: false, Collate: true, ManualDuplexPrint: manualDuplex);

                    SetPrinterDuplex(PrinterCB.Text, initialDuplexValue, Duplex_Combo.SelectedIndex);
                    SetPrinterColor(PrinterCB.Text, Color_Check.IsChecked == true);
                }
            }

            //////////
            // SET LETTERHEAD BACK TO ORIGINAL STATE
            //////////
            document.Undo(count);
            try
            {
                initialSelection.Select();
            }
            catch (Exception ex)
            {
            }
            LetterheadToggler.SetLetterheadProperty(originalLetterheadProperty);

            Utils.SetBackDefaultPrinter();
            SetBackMultiPrintValues(PrinterCB.Text, initialDuplexValue);

            if (checkedCount == 0 || checkedCount - cancelCount > 0)
            {
                this.Close();
            }
        }

        private Dictionary<int, SpecialHandlingSetting> GetSectionSpecialHandlingSettings()
        {
            Dictionary<int, SpecialHandlingSetting> result = new Dictionary<int, SpecialHandlingSetting>();

            int i = 1;
            foreach (Word.Section section in document.Sections)
            {
                var settings = SpecialSectionHandling.ReadCurrentSectionSettings(section);

                if (settings != null)
                {
                    result[i] = settings;
                }

                i++;
            }

            return result;
        }

        private int SetPrinterColor(string printer, bool color, int initialColorValue = 2)
        {
            PrinterSettings ps = new PrinterSettings();
            ps.PrinterName = printer;
            if (ps.IsValid)
            {
                if (ps.SupportsColor)
                {
                    try
                    {

                        string colorErrorMessage = string.Empty;
                        initialColorValue = PrinterApis.PrinterSettingsApis.GetPrinterColor(printer, out string error);
                        if (color)
                        {
                            PrinterApis.PrinterSettingsApis.
                            SetPrinterProperty(printer, PrinterApis.WinNative.DM.Color, DMCOLOR_COLOR, out error);
                        }
                        else
                        {
                            PrinterApis.PrinterSettingsApis.
                            SetPrinterProperty(printer, PrinterApis.WinNative.DM.Color, DMCOLOR_MONOCHROME, out error);
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex);
                    }
                }
            }
            return initialColorValue;
        }

        private int SetPrinterDuplex(string printerName, int initialDuplexValue, int duplexIndex)
        {
            PrinterSettings ps = new PrinterSettings();
            ps.PrinterName = printerName;
            if (ps.IsValid)
            {
                if (ps.CanDuplex)
                {
                    try
                    {
                        var selectedIndex = duplexIndex;
                        string duplexErrorMessage = string.Empty;
                        initialDuplexValue = PrinterApis.PrinterSettingsApis.GetPrinterDuplex(printerName, out string printerIsDuplexError);
                        if (selectedIndex == 0)
                        {
                            PrinterApis.PrinterSettingsApis.
                            SetPrinterProperty(printerName, PrinterApis.WinNative.DM.Duplex, 1, out duplexErrorMessage);
                        }
                        else if (selectedIndex == 1)
                        {
                            PrinterApis.PrinterSettingsApis.
                            SetPrinterProperty(printerName, PrinterApis.WinNative.DM.Duplex, 2, out duplexErrorMessage);

                        }
                        else if (selectedIndex == 2)
                        {
                            PrinterApis.PrinterSettingsApis.
                            SetPrinterProperty(printerName, PrinterApis.WinNative.DM.Duplex, 3, out duplexErrorMessage);
                        }
                        if (!string.IsNullOrEmpty(duplexErrorMessage))
                        {
                            // MessageBox.Show(duplexErrorMessage);
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex);
                    }
                }
            }
            return initialDuplexValue;
        }

        public int GetCheckboxCount()
        {
            int result = 0;
            result = Final_Check.IsChecked == false ? result : result + 1;
            result = PlainPaper_Check.IsChecked == false ? result : result + 1;
            result = Letterhead_Check.IsChecked == false ? result : result + 1;
            result = FileCopy_Check.IsChecked == false ? result : result + 1;
            result = AddressLabel_Checkbox.IsChecked == false ? result : result + 1;
            result = Envelope_Checkbox.IsChecked == false ? result : result + 1;
            result = TrayOverride_Check.IsChecked == false ? result : result + 1;
            result = PDF_Checkbox.IsChecked == false ? result : result + 1;
            result = SpecialPaper_Checkbox.IsChecked == false ? result : result + 1;
            return result;
        }

        private Word.Style GetOrCreateWatermarkStyle()
        {
            Word.Style style = null;
            try
            {
                style = ThisAddIn.Instance.Application.ActiveDocument.Styles["Watermark"];
            }
            catch
            {
                watermarkStyleCreated = true;
                style = ThisAddIn.Instance.Application.ActiveDocument.Styles.Add("Watermark", Word.WdStyleType.wdStyleTypeParagraphOnly);

                style.set_BaseStyle("");
                style.Font.Name = "Arial";
                style.Font.Size = 36;
                style.Font.Outline = -1;
                style.Font.AllCaps = -1;
                style.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphCenter;
                style.ParagraphFormat.SpaceAfter = 0;
            }

            return style;
        }

        // Helper function to validate # of copies to print
        // entered by user in MultiPrint dialog box
        private bool? IsValidNumberOfCopies(System.Windows.Controls.
        CheckBox paperTypeCheckbox, TextBox paperTypeTextbox)
        {
            if (paperTypeCheckbox.IsChecked == true)
            {
                int result;
                bool isNumeric = Int32.TryParse(paperTypeTextbox.Text, out result);
                if (isNumeric && result > 0)
                {
                    return true;
                }
                else
                {
                    new InfowareInformationWindow($"You must enter a valid number for { paperTypeCheckbox.Content.ToString()} copies.").ShowDialog();
                    return false;
                }
            }
            return null;
        }

        private void PositionStamp(Word.Shape shape)
        {
            shape.Application.ActiveWindow.ScrollIntoView(shape.Anchor);
            int position = SettingsManager.GetSettingAsInt("StampLocation", "MultiPrint", 0);
            shape.Width = 300;
            if (position == (int)StampLocation.Right)
            {
                shape.RelativeHorizontalPosition = Word.WdRelativeHorizontalPosition.wdRelativeHorizontalPositionRightMarginArea;
                shape.Left = (float)Word.WdShapePosition.wdShapeCenter;
                shape.Rotation = 90;
            }
            else
            {
                shape.RelativeHorizontalPosition = Word.WdRelativeHorizontalPosition.wdRelativeHorizontalPositionLeftMarginArea;
                shape.Left = (float)Word.WdShapePosition.wdShapeCenter;
                shape.Rotation = -90;
            }

            if (watermarkStyleCreated)
            {
                try
                {
                    shape.TextFrame.TextRange.Font.Line.Visible = Microsoft.Office.Core.MsoTriState.msoTrue;
                    shape.TextFrame.TextRange.Font.Line.Weight = (float)0.75;
                    shape.TextFrame.TextRange.Font.TextColor.RGB = (int)WdColor.wdColorWhite;
                }
                catch { }
            }

            shape.Line.Visible = Microsoft.Office.Core.MsoTriState.msoFalse;
            shape.RelativeVerticalPosition = Word.WdRelativeVerticalPosition.wdRelativeVerticalPositionPage;
            shape.Top = (float)Word.WdShapePosition.wdShapeCenter;
        }

        private void CreateNullAction(Word.Document document)
        {
            try
            {
                float bottomMargin = document.Sections.First.PageSetup.BottomMargin;

                document.Sections.First.PageSetup.BottomMargin = bottomMargin + 1;
                document.Sections.First.PageSetup.BottomMargin = bottomMargin;
            }
            catch (Exception ex)
            {
                try
                {
                    WdColor normalColor = document.Styles[WdBuiltinStyle.wdStyleHtmlNormal].Font.Color;
                    document.Styles[WdBuiltinStyle.wdStyleHtmlNormal].Font.Color = WdColor.wdColorBlack;
                    document.Styles[WdBuiltinStyle.wdStyleHtmlNormal].Font.Color = normalColor;

                }
                catch (Exception ex2)
                {
                }

                Debug.WriteLine(ex);
            }
        }

        public void Options_Click(object sender, RoutedEventArgs e)
        {
            MultiPrintSettings_WPF.SelectedPrinterName = this.PrinterCB.SelectedItem.ToString();
            MultiPrintSettings_WPF mpsForm = new MultiPrintSettings_WPF(MPSettings, document);
            this.Hide();
            mpsForm.ShowDialog();
            this.Show();
            PaperSource_List = mpsForm.getBins();
            MPSettings = mpsForm.MPSettings;

            if (MultiPrintSettings_WPF.SelectedPrinterName != null)
            {
                PrinterCB.SelectedValue = MultiPrintSettings_WPF.SelectedPrinterName;
            }

            //check if PDF Special printer is set
            if (string.IsNullOrEmpty(MPSettings.SpecialPrinterSettings.PdfPrinter) || string.IsNullOrEmpty(MPSettings.SpecialPrinterSettings.PdfBin))
                PDF_Checkbox.IsEnabled = false;
            else
                PDF_Checkbox.IsEnabled = true;
        }

        private void CopyStamp_Checkbox_Checked(object sender, RoutedEventArgs e)
        {
            if (DraftStamp_Checkbox.IsChecked == true)
            {
                DraftStamp_Checkbox.IsChecked = false;
            }
            if (FileCopyStamp_Checkbox.IsChecked == true)
            {
                FileCopyStamp_Checkbox.IsChecked = false;
            }
        }

        private void DraftStamp_Checkbox_Checked(object sender, RoutedEventArgs e)
        {
            if (CopyStamp_Checkbox.IsChecked == true)
            {
                CopyStamp_Checkbox.IsChecked = false;
            }
            if (FileCopyStamp_Checkbox.IsChecked == true)
            {
                FileCopyStamp_Checkbox.IsChecked = false;
            }
        }

        private void FileCopyStamp_Checkbox_Checked(object sender, RoutedEventArgs e)
        {
            if (DraftStamp_Checkbox.IsChecked == true)
            {
                DraftStamp_Checkbox.IsChecked = false;
            }
            if (CopyStamp_Checkbox.IsChecked == true)
            {
                CopyStamp_Checkbox.IsChecked = false;
            }
        }

        private void btnPrinterProperty_Click(object sender, RoutedEventArgs e)
        {
            PrinterDescription selectedPrinter;
            if (PrinterTickets.Where(x => x.FullName == PrinterCB.Text).Count() > 0)
                selectedPrinter = PrinterTickets.Where(x => x.FullName == PrinterCB.Text).FirstOrDefault();
            else
            {
                selectedPrinter = new PrinterDescription() { FullName = PrinterCB.Text, ClientPrintSchemaVersion = 1 };
                PrinterTickets.Add(selectedPrinter);
            }

            bool showPrintingPreferencesDialog = true;

            int modeGetSizeOfBuffer = 0;
            int modeCopy = 2;
            int modeOutBuffer = 14;

            IntPtr pointerHDevMode = new IntPtr();
            IntPtr pointerDevModeData = new IntPtr();
            GCHandle pinnedDevMode = new GCHandle();

            try
            {
                //PrinterSettings psSettings = new PrinterSettings { PrinterName = cbPrinters.SelectedItem.ToString() };
                //psSettings.DefaultPageSettings.PrinterSettings.Print

                PrintTicketConverter printTicketConverter = new PrintTicketConverter(selectedPrinter.FullName, selectedPrinter.ClientPrintSchemaVersion);
                IntPtr mainWindowPtr = IntPtr.Zero;//this.Handle;				

                if (selectedPrinter.DefaultPrintTicket == null)
                {
                    PrinterSettings printerSettings = new PrinterSettings();
                    //printerSettings.DefaultPageSettings.Color = false;
                    pointerHDevMode = printerSettings.GetHdevmode(printerSettings.DefaultPageSettings);
                }
                else
                {
                    byte[] myDevMode = printTicketConverter.ConvertPrintTicketToDevMode(selectedPrinter.DefaultPrintTicket, BaseDevModeType.UserDefault);
                    pinnedDevMode = GCHandle.Alloc(myDevMode, GCHandleType.Pinned);
                    pointerHDevMode = pinnedDevMode.AddrOfPinnedObject();
                }
                IntPtr pointerDevMode = GlobalLock(pointerHDevMode);

                int sizeNeeded = DocumentProperties(mainWindowPtr, IntPtr.Zero, selectedPrinter.FullName, IntPtr.Zero, pointerDevMode, modeGetSizeOfBuffer);
                pointerDevModeData = Marshal.AllocHGlobal(sizeNeeded);

                int result = -1;

                if (!showPrintingPreferencesDialog)
                {
                    result = DocumentProperties(mainWindowPtr, IntPtr.Zero, selectedPrinter.FullName, pointerDevModeData, pointerDevMode, modeCopy);
                }
                else
                {
                    result = DocumentProperties(mainWindowPtr, IntPtr.Zero, selectedPrinter.FullName, pointerDevModeData, pointerDevMode, modeOutBuffer);
                }

                GlobalUnlock(pointerHDevMode);

                if (result == 1)
                {
                    byte[] devMode = new byte[sizeNeeded];
                    Marshal.Copy(pointerDevModeData, devMode, 0, sizeNeeded);

                    // set back new printing settings to selected printer.
                    selectedPrinter.DefaultPrintTicket = printTicketConverter.ConvertDevModeToPrintTicket(devMode);
                }
            }
            finally
            {
                //GlobalFree(pointerHDevMode);
                Marshal.FreeHGlobal(pointerDevModeData);
                if (pinnedDevMode.IsAllocated)
                    pinnedDevMode.Free();
            }

            //Printer.PrinterProperties(IntPtr.Zero, IntPtr.Zero);

            //PrinterHelper106DotNet helper = new PrinterHelper106DotNet();
            //helper.OpenPrinterPropertiesDialog(IntPtr.Zero);

            //PrinterSettings ps = new PrinterSettings();
            //Printer.OpenPrinterPropertiesDialog(ps);

            //PrintDialog PrintDialog1 = new PrintDialog();
            //PrintDialog1.ShowDialog();			
        }

        private void PrinterCB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string printerName = (string)e.AddedItems[0];
            ThisAddIn.Instance.PreMultiPrintActivePrinterName = printerName;
            FillInPrinterProperties(printerName);
            SelectedPrinterName = printerName;
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            Utils.SetBackDefaultPrinter();
            this.Close();
        }

        private void SetPrinterTrayOnSections(Document document, WdPaperTray paperTray)
        {
            if (document != null)
            {
                ThisAddIn.Instance.Application.ScreenUpdating = false;
                if (ThisAddIn.Instance.IsFunctionForLogging("ScreenUpdating"))
                    Logger.Log("Disabled Screen Updating (MultiPrint.xaml.cs > SetPrinterTrayOnSections)");
                foreach (Word.Section section in document.Sections)
                {
                    section.PageSetup.FirstPageTray = paperTray;
                    section.PageSetup.OtherPagesTray = paperTray;
                }
                ThisAddIn.Instance.Application.ScreenUpdating = true;
                if (ThisAddIn.Instance.IsFunctionForLogging("ScreenUpdating"))
                    Logger.Log("Enabled Screen Updating (MultiPrint.xaml.cs > SetPrinterTrayOnSections)");
            }
        }

        public enum StampLocation
        {
            Left = 0,
            Right = 1
        }

        public static class Printer
        {
            [System.Runtime.InteropServices.DllImport("winspool.drv", CharSet = System.Runtime.InteropServices.CharSet.Auto, SetLastError = true)]
            public static extern bool SetDefaultPrinter(string Printr);

            [System.Runtime.InteropServices.DllImport("winspool.drv", CharSet = System.Runtime.InteropServices.CharSet.Auto, SetLastError = true)]
            public static extern bool PrinterProperties(IntPtr hwnd, IntPtr hPrinter);

            [DllImport("winspool.Drv", EntryPoint = "DocumentPropertiesW", SetLastError = true,
                ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
            static extern int DocumentProperties(IntPtr hwnd, IntPtr hPrinter, [MarshalAs(UnmanagedType.LPWStr)] string pDeviceName,
                IntPtr pDevModeOutput, ref IntPtr pDevModeInput, int fMode);
            [DllImport("kernel32.dll")]
            static extern IntPtr GlobalLock(IntPtr hMem);
            [DllImport("kernel32.dll")]
            static extern bool GlobalUnlock(IntPtr hMem);
            [DllImport("kernel32.dll")]
            static extern bool GlobalFree(IntPtr hMem);

            public static void OpenPrinterPropertiesDialog(PrinterSettings printerSettings)
            {
                try
                {
                    IntPtr hDevMode = printerSettings.GetHdevmode(printerSettings.DefaultPageSettings);
                    IntPtr pDevMode = GlobalLock(hDevMode);
                    //int sizeNeeded = DocumentProperties(this.Handle, IntPtr.Zero, printerSettings.PrinterName, pDevMode, ref pDevMode, 0);
                    int sizeNeeded = DocumentProperties(IntPtr.Zero,
                                        IntPtr.Zero,
                                        printerSettings.PrinterName,
                                        IntPtr.Zero, // This solves it
                                        ref pDevMode,
                                        0);
                    IntPtr devModeData = Marshal.AllocHGlobal(sizeNeeded);
                    DocumentProperties(IntPtr.Zero//this.Handle
                        , IntPtr.Zero, printerSettings.PrinterName, devModeData
                        , ref pDevMode
                        , 14);
                    GlobalUnlock(hDevMode);
                    printerSettings.SetHdevmode(pDevMode);
                    printerSettings.DefaultPageSettings.SetHdevmode(pDevMode);
                    GlobalFree(hDevMode);
                    Marshal.FreeHGlobal(devModeData);
                }
                catch (Exception ex)
                {
                    new InfowareInformationWindow(ex.Message).ShowDialog();
                }
            }
        }

        private void TrayOverride_Check_Unchecked(object sender, RoutedEventArgs e)
        {
            TrayOverride_Check.Content = "Tray Override";
            TrayOverride_Check.Width = 150;
            TrayOverrideBin_Combobox.Visibility = Visibility.Hidden;
        }

        private void TrayOverride_Check_Checked(object sender, RoutedEventArgs e)
        {
            TrayOverride_Check.Content = "";
            TrayOverride_Check.Width = 18;
            TrayOverrideBin_Combobox.Visibility = Visibility.Visible;

            TrayOverrideBin_Combobox.Items.Clear();

            List<string> paperSourceNames = new List<string>();
            paperSourceNames.Add(CustomConstants.DefaultPrinterBin);
            PrinterSettings ps = new PrinterSettings();
            for (int i = 0; i < ps.PaperSources.Count; i++)
            {
                paperSourceNames.Add(ps.PaperSources[i].SourceName);
            }

            foreach (string paperSourceName in paperSourceNames)
            {
                TrayOverrideBin_Combobox.Items.Add(paperSourceName);
            }
            TrayOverrideBin_Combobox.SelectedIndex = 0;
        }

        private void TextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (e != null)
            {
                Regex regex = new Regex("^[0-9]*$");
                e.Handled = !regex.IsMatch(e.Text);
            }
        }

        private void SetBackMultiPrintValues(string printerName, int initialDuplexValue)
        {
            if (!string.IsNullOrEmpty(printerName) && initialDuplexValue != -1)
            {
                var cacelationTokenSource = new CancellationTokenSource();
                cacelationTokenSource.CancelAfter(60000);

                try
                {
                    System.Threading.Tasks.Task.Run(async () =>
                    {
                        while (ThisAddIn.Instance.Application.BackgroundPrintingStatus > 0)
                        {
                            await System.Threading.Tasks.Task.Delay(5000);
                        }

                        PrinterApis.PrinterSettingsApis.SetPrinterProperty(printerName, PrinterApis.WinNative.DM.Duplex, initialDuplexValue, out string resetDuplexInitialValueErrorMessage);
                    }, cacelationTokenSource.Token);
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex);
                }
            }
        }

        private void SetBackPreviousPaperType()
        {
            var letterHeadOn = DocumentPropertyUtil.ReadProperty(CustomConstants.LETTERHEAD_ON);
            if (letterHeadOn != null && bool.Parse(letterHeadOn))
            {
                Task.Run(async () =>
                {
                    await Task.Delay(5000);

                    try
                    {
                        Utils.SetDocumentVariable(document, "PaperType", Utils.GetDocumentVariable(document, "PreOverlayPaperType"));
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex);
                    }
                });
            }
        }

        private void HideLetterhead(Document document)
        {
            if (document != null)
            {
                var letterheads = Utils.GetAllContentControls(document, CustomConstants.LETTERHEAD_TAG);

                if (letterheads.Count > 0)
                {
                    foreach (Word.ContentControl cc in letterheads)
                    {
                        cc.Range.ShapeRange.Visible = Microsoft.Office.Core.MsoTriState.msoFalse;
                        cc.Range.Font.Hidden = -1;
                    }
                }
            }
        }

        private void ShowLetterhead(Document document)
        {
            if (document != null)
            {
                int countShapesRemoved = 0;
                bool foundByTag = false;

                foreach (Word.ContentControl cc in Utils.GetAllContentControls(document, CustomConstants.LETTERHEAD_TAG))
                {
                    cc.Range.ShapeRange.Visible = Microsoft.Office.Core.MsoTriState.msoTrue;
                    cc.Range.Font.Hidden = 0;
                    foundByTag = true;
                }

                if (!foundByTag)
                {
                    foreach (Word.Shape shape in document.Sections[1].Headers[Word.WdHeaderFooterIndex.wdHeaderFooterPrimary].Shapes)
                    {
                        if (countShapesRemoved++ >= 2)
                        {
                            break;
                        }

                        shape.Visible = Microsoft.Office.Core.MsoTriState.msoTrue;
                    }
                }
            }
        }

        private bool fileCopyStampState;
        private bool includeHiddeState;

        private void FileCopy_Check_Checked(object sender, RoutedEventArgs e)
        {
            bool alwaysIncludeHidden = SettingsManager.GetSettingAsBool("FileCopyAlwaysIncludeHiddenText", "MultiPrint", false);
            bool alwaysIncludeStamp = SettingsManager.GetSettingAsBool("FileCopyAlwaysIncludeStamp", "MultiPrint", false);

            if (alwaysIncludeHidden)
            {
                includeHiddeState = IncludeHiddenText_Checkbox.IsChecked == true;
                IncludeHiddenText_Checkbox.IsChecked = true;
                IncludeHiddenText_Checkbox.IsEnabled = false;
            }

            if (alwaysIncludeStamp)
            {
                fileCopyStampState = FileCopyStamp_Checkbox.IsChecked == true;
                FileCopyStamp_Checkbox.IsChecked = true;
                FileCopyStamp_Checkbox.IsEnabled = false;
            }
        }

        private void FileCopy_Check_Unchecked(object sender, RoutedEventArgs e)
        {
            bool alwaysIncludeHidden = SettingsManager.GetSettingAsBool("FileCopyAlwaysIncludeHiddenText", "MultiPrint", false);
            bool alwaysIncludeStamp = SettingsManager.GetSettingAsBool("FileCopyAlwaysIncludeStamp", "MultiPrint", false);

            if (alwaysIncludeHidden)
            {
                IncludeHiddenText_Checkbox.IsChecked = includeHiddeState;
                IncludeHiddenText_Checkbox.IsEnabled = true;
            }

            if (alwaysIncludeStamp)
            {
                FileCopyStamp_Checkbox.IsChecked = fileCopyStampState;
                FileCopyStamp_Checkbox.IsEnabled = true;
            }
        }
    }
}
