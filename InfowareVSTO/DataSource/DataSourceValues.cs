﻿using InfowareVSTO.Common.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfowareVSTO.DataSource
{
    public class DataSourceValues
    {
        public PaneDTO PaneDTO { get; set; }

        public Dictionary<string, string> CustomFields { get; set; }
    }
}
