﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfowareVSTO.DataSource
{
    public class BaseConnector
    {
        public string ReplaceParameters(string condition, Dictionary<string, string> parameters)
        {
            if (parameters != null)
            {
                foreach (var kvPair in parameters)
                {
                    condition = condition.Replace("@" + kvPair.Key, $"'{kvPair.Value}'");
                }
            }

            return condition;
        }
    }
}
