﻿using InfowareVSTO.Common.DTOs;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfowareVSTO.DataSource
{
    public interface IDataSource
    {
        List<Matter> LoadMatters(string filter);
        DataSourceValues GetValues(Matter matter, PaneDTO paneDTO);
        DataTable GetData(string table, string condition, string columns, string joinTable = null, string joinCondition = null, string joinTable2 = null, string joinCondition2 = null, string joinTable3 = null, string joinCondition3 = null, string joinTable4 = null, string joinCondition4 = null, Dictionary<string, string> parameters = null, bool join4left = false, bool noCache = false);
        void StartCaching();
        void StopCaching();
    }
}
