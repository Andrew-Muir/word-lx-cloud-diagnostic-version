﻿using InfowareVSTO.Common;
using InfowareVSTO.Common.Language;
using InfowareVSTO.Common.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace InfowareVSTO.DataSource
{
    /// <summary>
    /// Interaction logic for SelectEntitiesDialog.xaml
    /// </summary>
    public partial class SelectEntitiesDialog : InfowareWindow
    {
        private static JobTitleVisibility LastJobTitleVisibility { get; set; } = JobTitleVisibility.NoJobTitle;
        private static bool LastCompanyNameVisibility { get; set; } = true;
        private static bool LastUseAttentionLineVisibility { get; set; } = true;
        private bool forInsertEntities;
        public bool ResultOK { get; set; }
        public List<SelectedEntityModel> AllEntities { get; set; }
        public List<SelectedEntityModel> SelectedEntities
        {
            get
            {
                List<SelectedEntityModel> result = new List<SelectedEntityModel>();

                foreach (object item in EntityGrid.Items)
                {
                    if (item is SelectedEntityModel)
                    {
                        var entity = item as SelectedEntityModel;
                        if (entity.Used)
                        {
                            result.Add(entity);
                        }
                    }
                }

                return result;
            }
        }

        public JobTitleVisibility JobTitleVisibility
        {
            get
            {
                if (UseJobTitle.IsChecked == true)
                {
                    if (JobTitleOnSameRow.IsChecked == true)
                    {
                        return JobTitleVisibility.SameRow;
                    }
                    else
                    {
                        return JobTitleVisibility.NextRow;
                    }
                }
                else
                {
                    return JobTitleVisibility.NextRow;
                }
            }
        }

        public bool CompanyNameVisibility
        {
            get
            {
                return this.UseCompanyName.IsChecked == true;
            }
        }

        public List<SelectedContact> SelectedContacts
        {
            get
            {
                List<SelectedContact> result = new List<SelectedContact>();

                foreach(var entity in SelectedEntities)
                {
                    string suffix = null, prefix = null, first = null, last = null, middle = null, jobtitle = null;
                    string street = null, city = null, prov = null, postal = null, country = null;
                    DataTable table = DataSourceManager.GetData("Entities", $"EN_Key = {entity.Key}", "EN_Name_Title,EN_Name_First,EN_Name_Middle,EN_Name_Last,EN_Name_Suffix,EN_JobTitle");
                    if (table.Rows.Count > 0)
                    {
                        prefix = table.Rows[0].ItemArray[0].ToString();
                        first = table.Rows[0].ItemArray[1].ToString();
                        middle = table.Rows[0].ItemArray[2].ToString();
                        last = table.Rows[0].ItemArray[3].ToString();
                        suffix = table.Rows[0].ItemArray[4].ToString();
                        jobtitle = table.Rows[0].ItemArray[5].ToString();
                    }
                
                    table = DataSourceManager.GetData("EntityXref", $"EX_EN_Key = {entity.Key} and EX_MM_Key = {DataSourceManager.LoadedMatter.Id}", "AD_Street, AD_City, AD_Prov, AD_Postal, AD_Country", "Addresses", "EX_AD_Key = AD_Key");
                    if (table.Rows.Count > 0)
                    {
                        street = table.Rows[0].ItemArray[0].ToString();
                        city = table.Rows[0].ItemArray[1].ToString();
                        prov = table.Rows[0].ItemArray[2].ToString();
                        postal = table.Rows[0].ItemArray[3].ToString();
                        country = table.Rows[0].ItemArray[4].ToString();
                    }

                    result.Add(new SelectedContact()
                    {
                        CompanyName = entity.Company,
                        Prefix = prefix,
                        FirstName = first,
                        LastName = last,
                        Suffix = suffix,
                        JobTitle = jobtitle,
                        IsSelected = UseAttentionLine.IsChecked == true,
                        Addresses = new List<Common.DTOs.AuthorAddressDTO>()
                        {
                            new Common.DTOs.AuthorAddressDTO()
                            {
                                Address1 = street,
                                City = city,
                                Province = prov,
                                Country = country,
                                PostalCode = postal
                            }
                        }
                    }) ;
                }

                return result;
            }
        }


        public SelectEntitiesDialog(string matterId, bool forInsertEntities = false)
        {
            InitializeComponent();

            PopulateEntities(matterId);

            ShowAndPopulateFilter();


            if (forInsertEntities)
            {
                this.forInsertEntities = true;
                HideShowInsertEntitySpecificControls();
            }
        }

        private void ShowAndPopulateFilter()
        {
            string filters = SettingsManager.GetSetting("SelectEntityFilters", "MatterMaster");
            List<KeyValuePair<string, List<string>>> itemSource = new List<KeyValuePair<string, List<string>>>();

            if (filters != null)
            {
                itemSource.Add(new KeyValuePair<string, List<string>>(LanguageManager.GetTranslation(LanguageConstants.None, "None"), new List<string>()));

                string[] filtersArr = filters.Split(';').Select(x=>x.Trim()).ToArray();

                foreach (string filter in filtersArr)
                {
                    string[] filterArr = filter.Split(',').Select(x => x.Trim()).ToArray();

                    if (filterArr.Length > 1)
                    {
                        itemSource.Add(new KeyValuePair<string, List<string>>(filterArr[0], filterArr.Skip(1).ToList()));
                    }
                }

                this.Filter.DisplayMemberPath = "Key";
                this.FilterStackPanel.Visibility = Visibility.Visible;
                this.Filter.ItemsSource = itemSource;
                this.Filter.SelectedIndex = 0;
            }
        }

        private void HideShowInsertEntitySpecificControls()
        {
            this.EntityGrid.Columns[4].Visibility = Visibility.Collapsed;
            this.EntityGrid.Columns[5].Visibility = Visibility.Collapsed;
            this.JobTitleStackPanel.Visibility = Visibility.Visible;
            this.CompanyNameStackPanel.Visibility = Visibility.Visible;
            LoadJobTitleVisibility();
            LoadCompanyNameVisibility();
            LoadUseAttentionLineVisibility();
        }

        private void LoadJobTitleVisibility()
        {
            switch (LastJobTitleVisibility)
            {
                case JobTitleVisibility.SameRow:
                    this.JobTitleOnSameRow.IsChecked = true;
                    this.UseJobTitle.IsChecked = true;
                    break;
                case JobTitleVisibility.NextRow:
                    this.UseJobTitle.IsChecked = true;
                    break;
            }
        }

        private void LoadCompanyNameVisibility()
        {
            if (LastCompanyNameVisibility == true)
            {
                UseCompanyName.IsChecked = true;
            }
            else
            {
                UseCompanyName.IsChecked = false;
            }
        }

        private void LoadUseAttentionLineVisibility()
        {
            if (LastUseAttentionLineVisibility == true)
            {
                UseAttentionLine.IsChecked = true;
            }
            else
            {
                UseAttentionLine.IsChecked = false;
            }
        }

        private void PopulateEntities(string matterId)
        {
            List<SelectedEntityModel> result = new List<SelectedEntityModel>();

            var dataTable = DataSourceManager.GetData("Entities", $"EX_MM_Key = {matterId}",
                "EN_Key, EN_Name_First, EN_Name_Middle, EN_Name_Last, EN_Name_Suffix, EN_Is_Company, EN_Company_Name, ER_Code, ER_Description, EX_CCList, EX_BCCList",
                "EntityXref", "EN_Key = EX_EN_Key",
                "EntityRoles", "EX_ER_Code = ER_Code");

            foreach (DataRow row in dataTable.Rows)
            {
                try
                {
                    var entity = new SelectedEntityModel()
                    {
                        FullName = MatterMasterPaneUtils.GetEntityFullName(row, 1, 2, 3, 4, 5, 6),
                        Company = row.ItemArray[6]?.ToString(),
                        Key = (int)row.ItemArray[0],
                        RoleCode = row.ItemArray[7]?.ToString(),
                        RoleName = row.ItemArray[8]?.ToString(),
                        CC = row.ItemArray[9]?.ToString() == "1",
                        BCC = row.ItemArray[10]?.ToString() == "1",
                    };

                    result.Add(entity);
                }
                catch (Exception ex)
                {
                }
            }

            EntityGrid.ItemsSource = result;
            AllEntities = result;
        }

        private void OKBtn_Click(object sender, RoutedEventArgs e)
        {
            if (forInsertEntities)
            {
                LastCompanyNameVisibility = CompanyNameVisibility;
                LastJobTitleVisibility = JobTitleVisibility;
                LastUseAttentionLineVisibility = this.UseAttentionLine.IsChecked == true;
            }
            ResultOK = true;
            this.Close();
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Filter_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if ((sender as ComboBox).SelectedItem is KeyValuePair<string, List<string>>)
            {
                List<string> roles = ((KeyValuePair<string, List<string>>)(sender as ComboBox).SelectedItem).Value;
                foreach (var entity in AllEntities)
                {
                    entity.Used = roles.Contains(entity.RoleCode);
                }
            }
        }
    }
}
