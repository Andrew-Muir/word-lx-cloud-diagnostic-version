﻿using InfowareVSTO.Common;
using InfowareVSTO.Common.DTOs;
using InfowareVSTO.Common.Models;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace InfowareVSTO.DataSource
{
    public class MySQLConnector : BaseConnector, IDataSource
    {
        private string connectionString;
        private DataSourceMappingDTO mapping;
        private Dictionary<string, Dictionary<string, DataTable>> cacheMemory;
        private bool cacheEnabled = false;

        public MySQLConnector(string connectionString, DataSourceMappingDTO mapping)
        {
            this.connectionString = connectionString;
            this.mapping = mapping;
        }

        DataSourceValues IDataSource.GetValues(Matter matter, PaneDTO paneDTO)
        {
            if (paneDTO == null)
            {
                paneDTO = new PaneDTO();
            }
            DataSourceValues result = new DataSourceValues();

            result.CustomFields = new Dictionary<string, string>();
            if (mapping != null && mapping.Groups != null && matter?.Id != null)
            {
                foreach (var group in mapping.Groups)
                {
                    if (!string.IsNullOrWhiteSpace(group.Sql))
                    {
                        try
                        {
                            MySqlConnection cnn;
                            cnn = new MySqlConnection(connectionString);
                            cnn.Open();
                            string sql = group.Sql.Replace(DataSourceManager.MATTER_ID_PLACEHOLDER, matter.Id);
                            MySqlCommand sqlCommand = new MySqlCommand(sql, cnn);
                            var reader = sqlCommand.ExecuteReader();

                            if (string.IsNullOrWhiteSpace(group.Type) || group.Type == DSGroupTypes.Other.ToString())
                            {
                                if (reader.Read())
                                {
                                    foreach (var column in group.FieldColumns)
                                    {
                                        try
                                        {
                                            string text = reader.GetString(reader.GetOrdinal(column.Value));
                                            result.CustomFields[column.Key] = text;
                                            if (Enum.TryParse(column.Key, out DSPaneDTOFields field))
                                            {
                                                switch (field)
                                                {
                                                    case DSPaneDTOFields.FileNumber:
                                                        paneDTO.FileNumber = text;
                                                        break;
                                                    case DSPaneDTOFields.Attention:
                                                        paneDTO.Attention = text;
                                                        break;
                                                    case DSPaneDTOFields.ClientFileNumber:
                                                        paneDTO.ClientFileNumber = text;
                                                        break;
                                                    case DSPaneDTOFields.Closing:
                                                        paneDTO.Closing = text;
                                                        break;
                                                    case DSPaneDTOFields.Delivery:
                                                        paneDTO.Delivery = text;
                                                        break;
                                                    case DSPaneDTOFields.Handling:
                                                        paneDTO.Handling = text;
                                                        break;
                                                    case DSPaneDTOFields.NoOfPages:
                                                        paneDTO.NoOfPages = text;
                                                        break;
                                                    case DSPaneDTOFields.ReLine:
                                                        paneDTO.ReLine = text;
                                                        break;
                                                }
                                            }
                                        }
                                        catch { }
                                    }
                                }
                            }
                            else if (group.Type == DSGroupTypes.Recipient.ToString())
                            {
                                List<SelectedContact> selectedContacts = new List<SelectedContact>();
                                while (reader.Read())
                                {
                                    SelectedContact contact = new SelectedContact();
                                    foreach (var column in group.FieldColumns)
                                    {

                                        try
                                        {
                                            string text = reader.GetString(reader.GetOrdinal(column.Value));
                                            if (Enum.TryParse(column.Key, out DSPaneDTOFieldsRecipient field))
                                            {
                                                switch (field)
                                                {
                                                    case DSPaneDTOFieldsRecipient.AddressLine1:
                                                        EnsureAddressExists(contact);
                                                        contact.Addresses[0].Address1 = text;
                                                        break;
                                                    case DSPaneDTOFieldsRecipient.AddressLine2:
                                                        EnsureAddressExists(contact);
                                                        contact.Addresses[0].Address2 = text;
                                                        break;
                                                    case DSPaneDTOFieldsRecipient.City:
                                                        EnsureAddressExists(contact);
                                                        contact.Addresses[0].City = text;
                                                        break;
                                                    case DSPaneDTOFieldsRecipient.Company:
                                                        if (contact.Companies == null)
                                                            contact.Companies = new List<ACompanyDTO>();
                                                        if (contact.Companies.Count == 0)
                                                            contact.Companies.Add(new ACompanyDTO());
                                                        contact.Companies[0].Name = text;
                                                        contact.CompanyName = text;
                                                        break;
                                                    case DSPaneDTOFieldsRecipient.Country:
                                                        EnsureAddressExists(contact);
                                                        contact.Addresses[0].Country = text;
                                                        break;
                                                    case DSPaneDTOFieldsRecipient.Email:
                                                        EnsureContactInformationsExists(contact);

                                                        contact.ContactInformations.Add(new AContactInformationDTO()
                                                        {
                                                            Details = text,
                                                            Type = "Email"
                                                        });
                                                        break;
                                                    case DSPaneDTOFieldsRecipient.Fax:
                                                        EnsureAddressExists(contact);
                                                        contact.Addresses[0].Fax = text;
                                                        EnsureContactInformationsExists(contact);

                                                        contact.ContactInformations.Add(new AContactInformationDTO()
                                                        {
                                                            Details = text,
                                                            Type = "Fax"
                                                        });
                                                        break;
                                                    case DSPaneDTOFieldsRecipient.FirstName:
                                                        contact.FirstName = text;
                                                        break;
                                                    case DSPaneDTOFieldsRecipient.JobTitle:
                                                        contact.JobTitle = text;
                                                        break;
                                                    case DSPaneDTOFieldsRecipient.LastName:
                                                        contact.LastName = text;
                                                        break;
                                                    case DSPaneDTOFieldsRecipient.Mobile:
                                                        EnsureContactInformationsExists(contact);

                                                        contact.ContactInformations.Add(new AContactInformationDTO()
                                                        {
                                                            Details = text,
                                                            Type = "Mobile"
                                                        });
                                                        break;
                                                    case DSPaneDTOFieldsRecipient.Name:
                                                        if (text != null)
                                                        {
                                                            string[] arr = text.Split(' ');
                                                            if (arr.Length > 0)
                                                            {
                                                                contact.LastName = arr[arr.Length - 1];

                                                                List<string> list = arr.ToList();
                                                                if (list.Count > 0)
                                                                {
                                                                    list.RemoveAt(list.Count - 1);
                                                                    contact.FirstName = string.Join(" ", list);
                                                                }
                                                            }
                                                        }
                                                        break;
                                                    case DSPaneDTOFieldsRecipient.Phone:
                                                        EnsureAddressExists(contact);
                                                        contact.Addresses[0].Phone = text;
                                                        EnsureContactInformationsExists(contact);

                                                        contact.ContactInformations.Add(new AContactInformationDTO()
                                                        {
                                                            Details = text,
                                                            Type = "Phone"
                                                        });
                                                        break;
                                                    case DSPaneDTOFieldsRecipient.PostalCode:
                                                        EnsureAddressExists(contact);
                                                        contact.Addresses[0].PostalCode = text;
                                                        break;
                                                    case DSPaneDTOFieldsRecipient.Province:
                                                        EnsureAddressExists(contact);
                                                        contact.Addresses[0].Province = text;
                                                        break;
                                                }
                                            }
                                        }
                                        catch { }


                                    }
                                    selectedContacts.Add(contact);
                                }
                                paneDTO.AddedRecipients = selectedContacts;
                                paneDTO.Tos = selectedContacts.ToArray();
                                paneDTO.Recipients = null;
                            }
                            else if (group.Type == DSGroupTypes.CC.ToString() || group.Type == DSGroupTypes.BCC.ToString())
                            {
                                List<string> selectedContacts = new List<string>();
                                while (reader.Read())
                                {
                                    SelectedContact contact = new SelectedContact();
                                    foreach (var column in group.FieldColumns)
                                    {

                                        try
                                        {
                                            string text = reader.GetString(reader.GetOrdinal(column.Value));
                                            if (Enum.TryParse(column.Key, out DSPaneDTOFieldsCC field))
                                            {
                                                switch (field)
                                                {
                                                    case DSPaneDTOFieldsCC.FirstName:
                                                        contact.FirstName = text;
                                                        break;

                                                    case DSPaneDTOFieldsCC.LastName:
                                                        contact.LastName = text;
                                                        break;
                                                    case DSPaneDTOFieldsCC.Name:
                                                        if (text != null)
                                                        {
                                                            string[] arr = text.Split(' ');
                                                            if (arr.Length > 0)
                                                            {
                                                                contact.LastName = arr[arr.Length - 1];

                                                                List<string> list = arr.ToList();
                                                                if (list.Count > 0)
                                                                {
                                                                    list.RemoveAt(list.Count - 1);
                                                                    contact.FirstName = string.Join(" ", list);
                                                                }
                                                            }
                                                        }
                                                        break;
                                                }
                                            }
                                        }
                                        catch { }

                                    }
                                    selectedContacts.Add(string.Join(" ", new List<string>() { contact.FirstName, contact.LastName }.Where(x => !string.IsNullOrWhiteSpace(x))));
                                }

                                string ccResult = string.Join("\r\n", selectedContacts);
                                if (group.Type == DSGroupTypes.CC.ToString())
                                {
                                    paneDTO.CC = ccResult;
                                }
                                else
                                {
                                    paneDTO.BCC = ccResult;
                                }
                            }

                            cnn.Close();
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                }
            }

            result.PaneDTO = paneDTO;

            return result;
        }

        private static void EnsureContactInformationsExists(SelectedContact contact)
        {
            if (contact.ContactInformations == null)
                contact.ContactInformations = new List<AContactInformationDTO>();
        }

        private static void EnsureAddressExists(SelectedContact contact)
        {
            if (contact.Addresses == null)
            {
                contact.Addresses = new List<AuthorAddressDTO>();
            }
            if (contact.Addresses.Count == 0)
            {
                contact.Addresses.Add(new AuthorAddressDTO());
            }
        }

        List<Matter> IDataSource.LoadMatters(string filter)
        {
            if (mapping != null && !string.IsNullOrWhiteSpace(mapping.MatterListSql) && !string.IsNullOrWhiteSpace(mapping.MatterCodeColumn))
            {
                try
                {
                    MySqlConnection cnn;
                    cnn = new MySqlConnection(connectionString);
                    cnn.Open();
                    string sql = mapping.MatterListSql;

                    sql = sql.Replace(DataSourceManager.MATTER_FILTER_PLACEHOLDER, "@filter");

                    MySqlCommand sqlCommand = new MySqlCommand(sql, cnn);
                    sqlCommand.Parameters.Add(new MySqlParameter("filter", filter));
                    var reader = sqlCommand.ExecuteReader();
                    List<Matter> result = new List<Matter>();
                    int matterIdOrdinal = -1;
                    int matterNameOrdinal = -1;
                    int matterDescriptionOrdinal = -1;
                    int matterClientNameOrdinal = -1;
                    int matterCodeOrdinal = -1;
                    while (reader.Read())
                    {

                        if (result.Count == 0)
                        {
                            try
                            {
                                matterIdOrdinal = reader.GetOrdinal(mapping.MatterIdColumn);
                            }
                            catch { }
                            try
                            {
                                matterNameOrdinal = reader.GetOrdinal(mapping.MatterNameColumn);
                            }
                            catch { }
                            try
                            {
                                matterDescriptionOrdinal = reader.GetOrdinal(mapping.MatterDescriptionColumn);
                            }
                            catch { }
                            try
                            {
                                matterClientNameOrdinal = reader.GetOrdinal(mapping.MatterClientNameColumn);
                            }
                            catch { }
                            try
                            {
                                matterCodeOrdinal = reader.GetOrdinal(mapping.MatterCodeColumn);
                            }
                            catch { }
                        }

                        var m = new Matter();

                        try
                        {
                            m.Id = reader.GetString(matterIdOrdinal);
                        }
                        catch
                        {
                            try
                            {
                                m.Id = reader.GetInt64(matterIdOrdinal).ToString();
                            }
                            catch { }
                        }
                        try
                        {
                            m.Name = reader.GetString(matterNameOrdinal);
                        }
                        catch { }

                        if (matterDescriptionOrdinal >= 0)
                        {
                            try
                            {
                                m.Description = reader.GetString(matterDescriptionOrdinal);
                            }
                            catch { }
                        }
                        if (matterClientNameOrdinal >= 0)
                        {
                            try
                            {
                                m.ClientName = reader.GetString(matterClientNameOrdinal);
                            }
                            catch { }
                        }

                        if (matterCodeOrdinal >= 0)
                        {
                            try
                            {
                                m.Code = reader.GetString(matterCodeOrdinal);
                            }
                            catch { }
                        }

                        result.Add(m);
                    }

                    cnn.Close();
                    return result;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
            return null;
        }

        public DataTable GetData(string table, string condition, string columns, string joinTable = null, string joinCondition = null, string joinTable2 = null, string joinCondition2 = null, string joinTable3 = null, string joinCondition3 = null, string joinTable4 = null, string joinCondition4 = null, Dictionary<string, string> parameters = null, bool join4left = false, bool noCache = false)
        {
            DataTable result = new DataTable();

            if (!noCache && cacheEnabled && cacheMemory != null && joinTable == null)
            {
                return GetChachedData(table, condition, columns, parameters);
            }

            if (table != null && condition != null && columns != null)
            {
                string sql = null;

                if (joinTable == null || joinCondition == null)
                {
                    sql = $"select {columns} from {table} where {condition}";
                }
                else if (joinTable2 == null || joinCondition2 == null)
                {
                    sql = $"select {columns} from {table} join {joinTable} on {joinCondition} where {condition}";
                }
                else if (joinTable3 == null || joinCondition3 == null)
                {
                    sql = $"select {columns} from {table} join {joinTable} on {joinCondition} join {joinTable2} on {joinCondition2} where {condition}";
                }
                else if (joinTable4 == null || joinCondition4 == null)
                {
                    sql = $"select {columns} from {table} join {joinTable} on {joinCondition} join {joinTable2} on {joinCondition2} join {joinTable3} on {joinCondition3} where {condition}";
                }
                else
                {
                    sql = $"select {columns} from {table} join {joinTable} on {joinCondition} join {joinTable2} on {joinCondition2} join {joinTable3} on {joinCondition3} {(join4left ? "left join" : "join")} {joinTable4} on {joinCondition4} where {condition}";
                }
                try
                {
                    MySqlConnection cnn;
                    cnn = new MySqlConnection(connectionString);
                    cnn.Open();

                    MySqlCommand sqlCommand = new MySqlCommand(sql, cnn);

                    if (parameters != null)
                    {
                        foreach (var keyValue in parameters)
                        {
                            sqlCommand.Parameters.AddWithValue(keyValue.Key, keyValue.Value);
                        }
                    }

                    MySqlDataAdapter da = new MySqlDataAdapter(sqlCommand);
                    // this will query your database and return the result to your datatable
                    da.Fill(result);
                    cnn.Close();
                }
                catch (Exception ex)
                {
                    return null;
                }
            }

            return result;
        }

        private DataTable GetChachedData(string table, string condition, string columns, Dictionary<string, string> parameters)
        {
            string tableClean = table.Replace("[", "").Replace("]", "").Replace(" ", "");
            string conditionClean = ReplaceParameters(condition.Replace("[", "").Replace("]", "").Replace(" ", ""), parameters);
            string columnsClean = columns.Replace("[", "").Replace("]", "").Replace(" ", "");

            if (!cacheMemory.ContainsKey(tableClean))
            {
                cacheMemory[tableClean] = new Dictionary<string, DataTable>();
            }

            if (!cacheMemory[tableClean].ContainsKey(conditionClean))
            {
                DataTable sqlResult = GetData(table, condition, "*", parameters: parameters, noCache: true);
                if (sqlResult != null)
                {
                    cacheMemory[tableClean][conditionClean] = sqlResult;
                }
            }

            if (cacheMemory[tableClean].TryGetValue(conditionClean, out var sqlDataTable))
            {
                string[] columnsArr = columnsClean.Split(',');

                DataTable result = new DataTable();

                foreach (string column in columnsArr)
                {
                    result.Columns.Add(column);
                }
                var dataColumns = sqlDataTable.Columns.Cast<DataColumn>().ToList();
                foreach (DataRow row in sqlDataTable.Rows)
                {
                    result.Rows.Add();
                    for (int i = 0; i < columnsArr.Length; i++)
                    {
                        DataColumn column = dataColumns.Where(x => x.ColumnName == columnsArr[i]).FirstOrDefault();

                        if (column != null)
                        {
                            result.Rows[result.Rows.Count - 1][i] = row.ItemArray[dataColumns.IndexOf(column)];
                        }
                    }
                }

                return result;
            }

            return null;
        }

        public void StartCaching()
        {
            cacheMemory = new Dictionary<string, Dictionary<string, DataTable>>();
            cacheEnabled = true;
        }

        public void StopCaching()
        {
            cacheMemory = null;
            cacheEnabled = false;
        }
    }
}


