﻿
using DocumentFormat.OpenXml.Office2013.PowerPoint.Roaming;
using InfowareVSTO.Common;
using InfowareVSTO.Common.DTOs;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace InfowareVSTO.DataSource
{
    public class DataSourceManager
    {
        public const string MATTER_ID_PLACEHOLDER = "##MATTERID##";
        public const string MATTER_FILTER_PLACEHOLDER = "##FILTER##";
        public const string LOADED_MATTER_KEY = "LoadedMatterId";
        private static Matter loadedMatter;
        public static Matter LoadedMatter 
        { 
            get
            {
                if (loadedMatter == null)
                {
                    string savedValue = RegistryManager.GetStringRegistryValue(LOADED_MATTER_KEY);
                    if (savedValue != null)
                    {
                       loadedMatter = JsonConvert.DeserializeObject<Matter>(savedValue);
                    }
                }

                return loadedMatter;
            }
            set
            {
                loadedMatter = value;
                if (loadedMatter != null)
                {
                    string json = JsonConvert.SerializeObject(loadedMatter);

                    RegistryManager.SaveToRegistry(LOADED_MATTER_KEY, json);
                }
                else
                {
                    RegistryManager.RemoveKey(LOADED_MATTER_KEY);
                }
            }
        }
        public static bool noConnectionString;
        public static DataSourceType? dataSourceType;
        public static DataSourceType DataSourceType
        {
            get
            {
                if (!dataSourceType.HasValue)
                {
                    dataSourceType = AdminPanelWebApi.GetDataSourceType();
                }

                return dataSourceType.Value;
            }
        }

        public static IDataSource dataSource;
        public static IDataSource DataSource
        {
            get
            {
                if (dataSource == null && DataSourceType != DataSourceType.None && !noConnectionString)
                {
                    dataSource = CreateDataSource(DataSourceType);
                }

                return dataSource;
            }
        }

        public static DataSourceMappingDTO mappings;
        public static DataSourceMappingDTO Mappings
        {
            get
            {
                if (mappings == null)
                {
                    mappings = AdminPanelWebApi.GetDataSourceFieldMappings();
                }

                return mappings;
            }
        }

        public static void Invalidate()
        {
            dataSourceType = null;
        }

        private static IDataSource CreateDataSource(DataSourceType dataSourceType)
        {
            string connectionString = SettingsManager.GetSetting("DataSourceConnectionString", "General");
            if (connectionString != null)
            {
                switch (dataSourceType)
                {
                    case DataSourceType.MatterSphere:
                    case DataSourceType.MatterMasterMSSQL:
                        return new MSSQLConnector(connectionString, Mappings);
                    case DataSourceType.MatterMasterMySql:
                        return new MySQLConnector(connectionString, Mappings);
                }
            }
            else
            {
                noConnectionString = true;
            }

            return null;
        }

        public static List<Matter> LoadMatters(string filter)
        {
            return DataSource?.LoadMatters(filter);
        }

        public static DataSourceValues GetValues(PaneDTO paneDTO = null)
        {
            if (LoadedMatter != null)
            {
                return DataSource?.GetValues(LoadedMatter, paneDTO);
            }
            return null;
        }

        public static DataTable GetData(string table, string condition, string columns, string joinTable = null, string joinCondition = null, string joinTable2 = null, string joinCondition2 = null, string joinTable3 = null, string joinCondition3 = null, string joinTable4 = null, string joinCondition4 = null, Dictionary<string, string> parameters = null, bool join4left = false)
        {
            return DataSource?.GetData(table, condition, columns, joinTable, joinCondition, joinTable2, joinCondition2, joinTable3, joinCondition3, joinTable4, joinCondition4, parameters, join4left) ?? new DataTable();
        }

        public static void StartCaching()
        {
            DataSource?.StartCaching();
        }

        public static void StopCaching()
        {
            DataSource?.StopCaching();
        }
    }
}
