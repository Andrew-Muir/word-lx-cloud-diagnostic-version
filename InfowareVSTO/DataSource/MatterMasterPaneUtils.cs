﻿using DocumentFormat.OpenXml.Bibliography;
using DocumentFormat.OpenXml.Drawing;
using DocumentFormat.OpenXml.ExtendedProperties;
using DocumentFormat.OpenXml.Office.CoverPageProps;
using InfowareVSTO.Common;
using InfowareVSTO.Common.DTOs;
using InfowareVSTO.Common.General;
using InfowareVSTO.MultiLanguage;
using InfowareVSTO.Util;
using InfowareVSTO.Windows;
using Microsoft.Office.Core;
using Microsoft.Office.Interop.Word;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Windows.Forms;
using Word = Microsoft.Office.Interop.Word;

namespace InfowareVSTO.DataSource
{
    public class MatterMasterPaneUtils
    {
        public static string MATTER_MASTER_TAG
        {
            get
            {
                return CustomConstants.MATTER_MASTER_TAG;
            }
        }

      
        public const string DELIFEMPTY_SWITCH = "<\\DelIfEmpty>";
        public const string DELLINEIFNOVALUE_SWITCH = "<\\DelLineIfNoValue>";
        public const string OTHER_LAWYERS_FIELD = "«name, address of other lawyers»";
        private static string matterId;
        private static Document document;
        private static bool shortStyleOnlastSection;

        public static bool HasMatterMasterMatterLoaded
        {
            get
            {
                return DataSourceManager.LoadedMatter != null && (DataSourceManager.DataSourceType == DataSourceType.MatterMasterMSSQL || DataSourceManager.DataSourceType == DataSourceType.MatterMasterMySql);
            }
        }

        public static void FillMatterMasterPlaceholders(Document document, bool shortStyleOnlastSection = true, InformOptions informOptions = null, string lawSocietyNumberLabel = "LS#:")
        {
            if (document != null && HasMatterMasterMatterLoaded)
            {
                DataSourceManager.StartCaching();
                MatterMasterPaneUtils.document = document;
                MatterMasterPaneUtils.shortStyleOnlastSection = shortStyleOnlastSection;
                var contentControls = DocumentUtils.GetAllContentControls(document, MATTER_MASTER_TAG)?.ToList();

                matterId = DataSourceManager.LoadedMatter.Id;

                contentControls = MergeSSOC(contentControls);

                if (contentControls != null && contentControls.Count > 0)
                {
                    List<SelectedEntityModel> entities = null;
                    if (contentControls.Where(x => x.Title?.StartsWith("@") == true).FirstOrDefault() != null)
                    {
                        bool hasWildcards = contentControls.Where(x => x.Title?.StartsWith("@*") == true).FirstOrDefault() != null;

                        var dialog = new SelectEntitiesDialog(matterId);
                        if (hasWildcards)
                        {
                            dialog.ShowDialog();
                            entities = dialog.SelectedEntities;
                        }
                        else
                        {
                            entities = dialog.AllEntities;
                        }
                    }

                    if (entities != null)
                    {
                        List<Bookmark> erbs = FindERBs(document);

                        List<ContentControl> toDelete = FillERBs(erbs, contentControls, entities);

                        foreach (ContentControl cc in toDelete)
                        {
                            contentControls.Remove(cc);
                        }
                    }

                    foreach (ContentControl cc in contentControls)
                    {
                        FillNonEntityFields(cc);

                        if (cc.Title?.StartsWith("@") == true)
                        {
                            FillEntityField(cc, entities);
                        }

                        //TODO
                        ApplySwitches(cc);
                    }
                }

                if (informOptions != null)
                {
                    FillOtherLawyersPlaceholder(informOptions, lawSocietyNumberLabel);
                }
                DataSourceManager.StopCaching();
            }
        }

        private static List<ContentControl> MergeSSOC(List<ContentControl> contentControls)
        {
            // Get has SSOC
            bool hasSSOC = false;
            System.Data.DataTable dataTable = DataSourceManager.GetData("InForm", $"IN_MM_Key = {matterId}", "IN_ShortStyle");
            if (dataTable.Rows.Count > 0)
            {
                var row = dataTable.Rows[0];

                hasSSOC = !string.IsNullOrWhiteSpace(row.ItemArray[0]?.ToString());
            }
            if (hasSSOC)
            {
                // get the SSOC Party 1 and Party 2 content controls from backpage.
                List<string> ccNames = new List<string>()
                {
                    "shortstyleofcause",
                    "party1",
                    "party2"
                };
                var ccs = contentControls.Where(x => ccNames.Where(y => x.Title?.ToLower().StartsWith(y) == true).Count() > 0 && IsInBackPage(x)).ToList();

                if (ccNames.Where(x => ccs.Where(y => y.Title?.ToLower().StartsWith(x) == true).Count() == 0).Count() == 0)
                {
                    // test to see if they are on the same row
                    var firstCell = GetCell(ccs.FirstOrDefault());

                    bool onTheSameRow = true;

                    if (firstCell != null)
                    {
                        foreach (var cc in ccs.Skip(1))
                        {
                            if (firstCell.Row.Range.Start != GetCell(cc)?.Row.Range.Start)
                            {
                                onTheSameRow = false;
                            }
                        }
                    }

                    if (onTheSameRow)
                    {
                        // merge cells
                        firstCell.Merge(GetCell(ccs.Last()));

                        ContentControl ssocCC = null;

                        // fill SSOC and empty Party 1 and Party 2
                        foreach (var cc in ccs)
                        {
                            string titleFullCase = TrimSwitch(cc.Title);
                            string title = titleFullCase?.ToLower();

                            switch (title)
                            {
                                case "party1":
                                case "party2":
                                    cc.Range.Text = cc.Range.Text = " ";
                                    break;
                                case "shortstyleofcause":
                                    cc.Range.Text = ReplaceParagraphMarks(dataTable.Rows[0].ItemArray[0].ToString().SpaceIfEmpty());
                                    ssocCC = cc;
                                    break;
                            }

                            ApplySwitches(cc);
                            contentControls.Remove(cc);
                        }

                        //delete all content except SSOC
                        try
                        {
                            Range range = document.Range(GetCell(ssocCC).Range.Start, ssocCC.Range.Start - 1);
                            if (range.End - range.Start > 0)
                            {
                                range.Delete();
                            }
                            range = document.Range(ssocCC.Range.End, GetCell(ssocCC).Range.End - 1);
                            if (range.End - range.Start > 0)
                            {
                                range.Delete();
                            }
                            range = document.Range(ssocCC.Range.End, GetCell(ssocCC).Range.End - 1);
                            if (range.End - range.Start > 0)
                            {
                                range.Delete();
                            }
                        }
                        catch (Exception ex)
                        {
                        }

                        //apply BoilerPlateCentred style
                        try
                        {
                            ssocCC.Range.Paragraphs.set_Style("BoilerPlateCentre");
                        }
                        catch (Exception ex)
                        {

                        }

                        DocumentPropertyUtil.SaveShortProperty(CustomConstants.ROW_EXPANDED, "True", true, document);
                        ThisAddIn.Instance.Ribbon.Invalidate();
                    }
                }
            }

            return contentControls;
        }

        private static Cell GetCell(ContentControl cc)
        {
            try
            {
                return cc.Range.Cells[1];
            }
            catch (Exception ex)
            {
            }

            return null;
        }

        private static bool IsInBackPage(ContentControl cc)
        {
            return cc.Range.Sections.First.Range.Start == document.Sections.Last.Range.Start;
        }

        private static void FillOtherLawyersPlaceholder(InformOptions options, string lawSocietyNumberLabel)
        {
            List<LawyerCompany> companies = GetAllLawyersAndParties(options);
            Range otherLawyersRange = GetOtherLawyersRange(document.Content);
            List<KeyValuePair<LawyerCompany, Word.Paragraph>> paragraphs = new List<KeyValuePair<LawyerCompany, Word.Paragraph>>();

            if (otherLawyersRange != null && companies.Count > 0)
            {
                paragraphs.Add(new KeyValuePair<LawyerCompany, Word.Paragraph>(companies.First(), otherLawyersRange.Paragraphs.First));
                otherLawyersRange.Paragraphs.First.Range.Copy();
                Range copyTarget = otherLawyersRange.Paragraphs.First.Range;
                copyTarget.Collapse(WdCollapseDirection.wdCollapseEnd);

                foreach (LawyerCompany company in companies)
                {
                    if (!company.IsThisCompany)
                    {
                        if (company == companies.FirstOrDefault())
                        {
                            continue;
                        }
                        copyTarget.Paste();

                        Range tempRange = copyTarget.Duplicate;

                        tempRange.Collapse(WdCollapseDirection.wdCollapseStart);
                        tempRange.Text = "AND ";

                        paragraphs.Add(new KeyValuePair<LawyerCompany, Word.Paragraph>(company, copyTarget.Paragraphs.First));

                        copyTarget.Collapse(WdCollapseDirection.wdCollapseEnd);
                    }
                }

                foreach (var keyValuePair in paragraphs)
                {
                    Word.Paragraph paragraph = keyValuePair.Value;
                    LawyerCompany company = keyValuePair.Key;

                    Range fieldRange = GetOtherLawyersRange(paragraph.Range);

                    fieldRange.Text = company.CompanyName;
                    fieldRange.Font.Reset();

                    if (options.CompanyNameBoldface)
                    {
                        fieldRange.Font.Bold = -1;
                    }

                    if (options.CompanyNameAllCaps)
                    {
                        fieldRange.Font.AllCaps = -1;
                    }

                    fieldRange.Collapse(WdCollapseDirection.wdCollapseEnd);

                    fieldRange.Text = GetAddressBlock(company);
                    fieldRange.Font.Bold = 0;
                    fieldRange.Font.AllCaps = 0;
                    fieldRange.Collapse(WdCollapseDirection.wdCollapseEnd);

                    fieldRange.Text = "\v";

                    if (options.IncludeBlankLineCompanyAddressLawyer)
                    {
                        fieldRange.Collapse(WdCollapseDirection.wdCollapseEnd);
                        fieldRange.Text = "\v";
                    }

                    fieldRange.Collapse(WdCollapseDirection.wdCollapseEnd);

                    foreach (Lawyer lawyer in company.Lawyers)
                    {
                        if (lawyer != null)
                        {
                            fieldRange.Text = lawyer.FullName + " ";
                            if (options.LawyerNameBoldface)
                            {
                                fieldRange.Font.Bold = -1;
                            }

                            fieldRange.Collapse(WdCollapseDirection.wdCollapseEnd);
                            if (!string.IsNullOrWhiteSpace(lawyer.LSNr))
                            {
                                fieldRange.Text = lawSocietyNumberLabel + lawyer.LSNr;

                                fieldRange.Font.Bold = 0;
                                fieldRange.Collapse(WdCollapseDirection.wdCollapseEnd);
                            }

                            fieldRange.Text = "\v";

                            if (options.IncludeBlankLineLawyerPhoneNumber && (!string.IsNullOrWhiteSpace(lawyer.Phone) || !string.IsNullOrWhiteSpace(lawyer.Fax)))
                            {
                                fieldRange.Collapse(WdCollapseDirection.wdCollapseEnd);
                                fieldRange.Text = "\v";
                            }

                            fieldRange.Font.Bold = 0;
                            fieldRange.Font.AllCaps = 0;
                            fieldRange.Collapse(WdCollapseDirection.wdCollapseEnd);

                            if (!string.IsNullOrWhiteSpace(lawyer.Phone))
                            {
                                string separator = "\v";
                                if (!string.IsNullOrWhiteSpace(lawyer.Fax) && options.PhoneAndFaxNumberOnOneLine)
                                {
                                    separator = " ";
                                }

                                fieldRange.Text = "Tel: " + lawyer.Phone + separator;
                                fieldRange.Font.Bold = 0;
                                fieldRange.Font.AllCaps = 0;
                                fieldRange.Collapse(WdCollapseDirection.wdCollapseEnd);
                            }
                            if (!string.IsNullOrWhiteSpace(lawyer.Fax))
                            {
                                fieldRange.Text = "Fax: " + lawyer.Fax + "\v";
                                fieldRange.Font.Bold = 0;
                                fieldRange.Font.AllCaps = 0;
                                fieldRange.Collapse(WdCollapseDirection.wdCollapseEnd);
                            }

                            if (!string.IsNullOrWhiteSpace(lawyer.Email) && options.IncludeEmailAddress)
                            {
                                fieldRange.Text = "Email: " + lawyer.Email + "\v";
                                fieldRange.Font.Bold = 0;
                                fieldRange.Font.AllCaps = 0;
                                fieldRange.Collapse(WdCollapseDirection.wdCollapseEnd);
                            }

                            if (lawyer != company.Lawyers.Last() && options.IncludeBlankLinesSameFirmLawyer)
                            {
                                fieldRange.Text = "\v";
                                fieldRange.Collapse(WdCollapseDirection.wdCollapseEnd);
                            }
                        }

                        if (company.RepresentedNames?.Count > 0)
                        {
                            fieldRange.Collapse(WdCollapseDirection.wdCollapseEnd);
                            fieldRange.Text = "\v";

                            fieldRange.Collapse(WdCollapseDirection.wdCollapseEnd);
                            if (company.SelfRepresented)
                            {
                                fieldRange.Text = "Self represented " + company.RepresentedNames.First().Key;
                            }
                            else
                            {
                                string text = "Lawyer for the ";
                                if (company.Lawyers.Count > 1)
                                {
                                    text = "Lawyers for the ";
                                }

                                text += CommaAnd(company.RepresentedNames.Select(x => (x.Value.Count > 1 ? GetPluralPartyTitle(x.Key) : x.Key) + (HasOnlyOneOfThisType(x.Key, companies) ? "" : (" " + CommaAnd(x.Value)))));
                                fieldRange.Text = text;
                            }
                        }
                    }
                }
            }
        }

        private static string GetPluralPartyTitle(string key)
        {
            Dictionary<string, string> plurals = GetPluralPartyTitleDictionary();

            if (key != null && plurals.TryGetValue(key, out string value))
            {
                return value;
            }

            return key;
        }

        private static Dictionary<string, string> GetPluralPartyTitleDictionary()
        {
            Dictionary<string, string> result = new Dictionary<string, string>();

            result["Appellant"] = "Appellants";
            result["Respondent"] = "Respondents";
            result["Plaintiff"] = "Plaintiffs";
            result["Defendant"] = "Defendants";
            result["Third Party"] = "Third Parties";
            result["Fourth Party"] = "Fourth Parties";
            result["Intervener"] = "Interveners";
            result["Plaintiff by Counterclaim"] = "Plaintiffs by Counterclaim";
            result["Defendant by Counterclaim"] = "Defendants by Counterclaim";
            return result;
        }

        private static bool HasOnlyOneOfThisType(string title, List<LawyerCompany> companies)
        {
            int titleCount = 0;

            foreach (var company in companies)
            {
                if (company.RepresentedNames.TryGetValue(title, out var value))
                {
                    titleCount += value.Count;
                }
            }

            return titleCount < 2;
        }

        private static string CommaAnd(IEnumerable<string> input)
        {
            List<string> temp = input.ToList();
            string last = temp.LastOrDefault();

            temp.RemoveAt(temp.Count - 1);
            if (temp.Count > 0)
            {
                string previous = string.Join(", ", temp);

                if (!string.IsNullOrWhiteSpace(previous))
                {
                    return previous + " and " + last;
                }
            }

            return last;

        }

        private static string GetAddressBlock(LawyerCompany company)
        {
            List<string> lines = new List<string>();

            if (!string.IsNullOrWhiteSpace(company.Street))
            {
                lines.Add(ReplaceParagraphMarks(company.Street));
            }

            List<string> secondLine = new List<string>();

            if (!string.IsNullOrWhiteSpace(company.City))
            {
                secondLine.Add(company.City);
            }

            if (!string.IsNullOrWhiteSpace(company.Province))
            {
                secondLine.Add(company.Province);
            }

            if (!string.IsNullOrWhiteSpace(company.PostalCode))
            {
                secondLine.Add(company.PostalCode);
            }

            string secondLineStr = string.Join(" ", secondLine);

            if (!string.IsNullOrWhiteSpace(secondLineStr))
            {
                lines.Add(secondLineStr);
            }

            string allLines = string.Join("\v", lines);

            if (!string.IsNullOrWhiteSpace(allLines))
            {
                return "\v" + allLines;
            }
            else
            {
                return "";
            }
        }

        private static Range GetOtherLawyersRange(Range range)
        {
            Find find = range.Find;

            find.Text = OTHER_LAWYERS_FIELD;

            find.Execute();

            if (find.Found)
            {
                return range;
            }

            return null;
        }

        private static List<LawyerCompany> GetAllLawyersAndParties(InformOptions options)
        {
            var result = new List<LawyerCompany>();
            List<KeyValuePair<LawyerCompany, Lawyer>> entries = new List<KeyValuePair<LawyerCompany, Lawyer>>();
            string formCollection = InformDialog.CurrentFormCollection;
            Dictionary<int, string> partyTitles = GetPartyTitles(formCollection);
            System.Data.DataTable dataTable = DataSourceManager.GetData("EntityXref as Client", $"Client.EX_MM_Key = {matterId}", "Client.EX_EN_Key,EX_PartyNo," +
            "ClientEntity.EN_Name_First, ClientEntity.EN_Name_Middle, ClientEntity.EN_Name_Last, ClientEntity.EN_Company_Name, ClientEntity.EN_Is_Company," + //2
            "Lawyer.EN_Key, Lawyer.EN_Is_Company, Lawyer.EN_Name_First, Lawyer.EN_Name_Middle, Lawyer.EN_Name_Last, Lawyer.EN_Company_Name, Lawyer.EN_Is_Company, Lawyer.EN_DirectPhone, Lawyer.EN_DirectFax, Lawyer.EN_Email, Lawyer.EN_LSN," + //7
            "Company.EN_Company_Name," + //18
            "Addresses.AD_Street, Addresses.AD_City, Addresses.AD_Prov, Addresses.AD_Postal," +  //19
            "Company.EN_DirectPhone, Company.EN_DirectFax," + //23
            "Client.EX_IsClient", //25
            "Entities as ClientEntity", " (Client.EX_EN_Key = ClientEntity.EN_Key)",
            "Entities as Lawyer", "(Client.EX_EN_Rep = Lawyer.EN_Key or Client.EX_Rep1 = Lawyer.EN_Key or Client.EX_Rep2 = Lawyer.EN_Key or Client.EX_Rep3 = Lawyer.EN_Key or Client.EX_Rep4 = Lawyer.EN_Key)",
            "Entities as Company", "(Lawyer.EN_Company_Key = Company.EN_Key or (Lawyer.EN_Is_Company = 1 and Lawyer.EN_Key = Company.En_Key))",
            "Addresses", "Company.EN_MailingAddress = Addresses.AD_Key", join4left: true);
            if (dataTable.Rows.Count > 0)
            {
                foreach (DataRow row in dataTable.Rows)
                {
                    LawyerCompany company = new LawyerCompany();
                    Lawyer lawyer = new Lawyer();

                    company.CompanyName = row.ItemArray[18]?.ToString();
                    company.SelfRepresented = row.ItemArray[0]?.ToString() == row.ItemArray[7]?.ToString();
                    company.RepresentedName = GetEntityFullName(row, 2, 3, 4, null, 5, 6);
                    company.Street = row.ItemArray[19]?.ToString();
                    company.City = row.ItemArray[20]?.ToString();
                    company.Province = row.ItemArray[21]?.ToString();
                    company.PostalCode = row.ItemArray[22].ToString();

                    if (int.TryParse(row.ItemArray[1]?.ToString(), out int partyNo) && partyTitles.TryGetValue(partyNo, out string partyTitle))
                    {
                        company.PartyTitle = partyTitle;
                    }

                    // is this our client?
                    company.IsThisCompany = (row.ItemArray[25]?.ToString().ToLower() == bool.TrueString.ToLower());

                    // is this a lawyer?
                    if (row.ItemArray[8]?.ToString() == "1")
                    {
                        lawyer = null;
                    }
                    else
                    {
                        lawyer.FullName = GetEntityFullName(row, 9, 10, 11, null, 12, 13);
                        lawyer.Email = row.ItemArray[16]?.ToString();
                        lawyer.LSNr = row.ItemArray[17]?.ToString();
                        if (options.UseCompanyPhoneAndFax && !options.UseOnlyCompanyFax)
                        {
                            lawyer.Phone = row.ItemArray[23]?.ToString();
                            lawyer.Fax = row.ItemArray[24]?.ToString();
                        }
                        else if (!options.UseOnlyCompanyFax)
                        {
                            lawyer.Phone = row.ItemArray[14]?.ToString();
                            lawyer.Fax = row.ItemArray[15]?.ToString();
                        }
                        else
                        {
                            lawyer.Fax = row.ItemArray[24]?.ToString();
                        }
                    }

                    entries.Add(new KeyValuePair<LawyerCompany, Lawyer>(company, lawyer));
                }
            }

            Dictionary<LawyerCompany, List<Lawyer>> tempResult = new Dictionary<LawyerCompany, List<Lawyer>>();
            foreach (var entry in entries)
            {
                var company = tempResult.Keys.Where(x => x.Equals(entry.Key)).FirstOrDefault();

                if (company == null)
                {
                    company = entry.Key;
                }

                if (tempResult.TryGetValue(company, out var list))
                {
                    list.Add(entry.Value);
                }
                else
                {
                    tempResult[company] = new List<Lawyer>() { entry.Value };
                }
            }

            foreach (KeyValuePair<LawyerCompany, List<Lawyer>> keyValuePair in tempResult)
            {
                keyValuePair.Key.Lawyers = keyValuePair.Value;
                result.Add(keyValuePair.Key);
            }


            // Group by same lawyer
            var tempResult2 = result;
            result = new List<LawyerCompany>();
            for (int i = 0; i < tempResult2.Count; i++)
            {
                var company = tempResult2[i];
                if (company.Lawyers.Count > 0)
                {
                    Dictionary<string, List<string>> representedNames = new Dictionary<string, List<string>>();
                    representedNames[company.PartyTitle] = new List<string>() { company.RepresentedName };

                    for (int j = i + 1; j < tempResult2.Count; j++)
                    {
                        for (int k = tempResult2[j].Lawyers.Count - 1; k >= 0; k--)
                        {
                            if (company.Lawyers.Where(x => x.Equals(tempResult2[j].Lawyers[k])).Count() > 0)
                            {
                                string pt = tempResult2[j].PartyTitle;
                                string rn = tempResult2[j].RepresentedName;
                                if (!representedNames.ContainsKey(pt))
                                {
                                    representedNames[pt] = new List<string>();
                                }
                                representedNames[pt].Add(rn);
                                tempResult2[j].Lawyers.RemoveAt(k);
                            }
                        }
                    }

                    company.RepresentedNames = representedNames;
                    result.Add(company);
                }
            }

            return result;
        }

        private static Dictionary<int, string> GetPartyTitles(string formCollection)
        {
            Dictionary<int, string> result = new Dictionary<int, string>();

            if (formCollection == "Federal Tax Court" || formCollection == "Federal Appeal Court")
            {
                result[1] = "Appellant";
                result[2] = "Respondent";
            }
            else
            {
                result[1] = "Plaintiff";
                result[2] = "Defendant";
            }

            result[3] = "Third Party";
            result[4] = "Fourth Party";
            result[5] = "Intervener";

            result[8] = "Plaintiff by Counterclaim";
            result[9] = "Defendant by Counterclaim";
            return result;
        }

        private static void FillNonEntityFields(ContentControl cc)
        {
            string titleFullCase = TrimSwitch(cc.Title);
            string title = titleFullCase?.ToLower();
            System.Data.DataTable dataTable;

            switch (title)
            {
                case "fileno":
                    dataTable = DataSourceManager.GetData("MatterMaster", $"MM_KEY = {matterId}", "MM_ClientMatterCode");
                    if (dataTable.Rows.Count > 0)
                    {
                        cc.Range.Text = dataTable.Rows[0].ItemArray.FirstOrDefault()?.ToString().SpaceIfEmpty();
                    }
                    else
                    {
                        cc.Range.Text = " ";
                    }
                    break;
                case "clientiscompany":
                    dataTable = DataSourceManager.GetData("MatterMaster", $"MM_KEY = {matterId}", "MM_ClientIsCompany");
                    if (dataTable.Rows.Count > 0)
                    {
                        cc.Range.Text = dataTable.Rows[0].ItemArray.FirstOrDefault()?.ToString().SpaceIfEmpty();
                    }
                    else
                    {
                        cc.Range.Text = " ";
                    }
                    break;
                case "mattername":
                    dataTable = DataSourceManager.GetData("MatterMaster", $"MM_KEY = {matterId}", "MM_MatterName");
                    if (dataTable.Rows.Count > 0)
                    {
                        cc.Range.Text = dataTable.Rows[0].ItemArray.FirstOrDefault()?.ToString().SpaceIfEmpty();
                    }
                    else
                    {
                        cc.Range.Text = " ";
                    }
                    break;
                case "reline":
                    dataTable = DataSourceManager.GetData("MatterMaster", $"MM_KEY = {matterId}", "MM_MatterReLine");
                    if (dataTable.Rows.Count > 0)
                    {
                        cc.Range.Text = dataTable.Rows[0].ItemArray.FirstOrDefault()?.ToString().SpaceIfEmpty();
                    }
                    else
                    {
                        cc.Range.Text = " ";
                    }
                    break;
                case "dateopened":
                    dataTable = DataSourceManager.GetData("MatterMaster", $"MM_KEY = {matterId}", "MM_DateOpened");
                    if (dataTable.Rows.Count > 0)
                    {
                        string dateFormat = Utils.GetDefaultDateFormat();
                        string cultureId = MLanguageUtil.GetResource(MLanguageResourceEnum.DateCultureID);
                        DateTime? dateTime = (dataTable.Rows[0].ItemArray.FirstOrDefault() as DateTime?);
                        string dateValue = null;
                        if (dateTime.HasValue)
                        {
                            if (cultureId.IsEmpty())
                            {
                                dateValue = dateTime.Value.ToString(dateFormat);
                            }
                            else
                            {
                                dateValue = dateTime.Value.ToString(dateFormat, CultureInfo.CreateSpecificCulture(cultureId));
                            }
                        }

                        cc.Range.Text = dateValue.SpaceIfEmpty();
                    }
                    break;
                case "client_name":
                    dataTable = DataSourceManager.GetData("MatterMaster", $"MM_KEY = {matterId}", "EN_Name_First, EN_Name_Middle, EN_Name_Last, EN_Name_Suffix, EN_Is_Company, EN_Company_Name", "Entities", "MM_ClientCode = EN_ClientID");
                    if (dataTable.Rows.Count > 0)
                    {
                        var row = dataTable.Rows[0];

                        cc.Range.Text = GetEntityFullName(row, 0, 1, 2, 3, 4, 5);
                    }
                    else
                    {
                        cc.Range.Text = " ";
                    }

                    break;
                case "client_fileno":
                    dataTable = DataSourceManager.GetData("MatterMaster", $"MM_KEY = {matterId}", "MM_ClientFileNumber");
                    if (dataTable.Rows.Count > 0)
                    {
                        cc.Range.Text = dataTable.Rows[0].ItemArray.FirstOrDefault()?.ToString().SpaceIfEmpty();
                    }
                    else
                    {
                        cc.Range.Text = " ";
                    }
                    break;

                case "clientnamesall":
                    dataTable = DataSourceManager.GetData("MatterMaster", $"MM_KEY = {matterId} and EX_IsClient = 1", "EN_Name_First, EN_Name_Middle, EN_Name_Last, EN_Name_Suffix, EN_Is_Company, EN_Company_Name", "EntityXref", "MM_Key = EX_MM_Key", "Entities", "EX_EN_Key = EN_Key");
                    List<string> fullNames = new List<string>();
                    foreach (DataRow row in dataTable.Rows)
                    {
                        string fullName = string.Empty;
                        fullName = GetEntityFullName(row, 0, 1, 2, 3, 4, 5);

                        fullNames.Add(fullName);
                    }
                    cc.Range.Text = ConcatenateAllNames(fullNames).SpaceIfEmpty();
                    break;
                case "party1":
                    cc.Range.Text = GetPartyXNames(1, matterId).SpaceIfEmpty();
                    break;
                case "party2":
                    cc.Range.Text = GetPartyXNames(2, matterId).SpaceIfEmpty();
                    break;
                case "party3":
                    cc.Range.Text = GetPartyXNames(3, matterId).SpaceIfEmpty();
                    break;
                case "party4":
                    cc.Range.Text = GetPartyXNames(4, matterId).SpaceIfEmpty();
                    break;
                case "party5":
                    cc.Range.Text = GetPartyXNames(5, matterId).SpaceIfEmpty();
                    break;
                case "party6":
                    cc.Range.Text = GetPartyXNames(6, matterId).SpaceIfEmpty();
                    break;
                case "party7":
                    cc.Range.Text = GetPartyXNames(7, matterId).SpaceIfEmpty();
                    break;
                case "party8":
                    cc.Range.Text = GetPartyXNames(8, matterId).SpaceIfEmpty();
                    break;
                case "party9":
                    cc.Range.Text = GetPartyXNames(9, matterId).SpaceIfEmpty();
                    break;
                case "matterlawyername":
                case "matterlawyerphone":
                case "matterlawyerfax":
                case "matterlawyeremail":
                case "matterlawyerlsn":
                    dataTable = DataSourceManager.GetData("Entities", $"XrefClient.EX_MM_Key = {matterId} and XrefLawyer.EX_MM_Key = {matterId} and EntityRoles.ER_IsRepRole = 1",
                        "EN_DirectPhone, EN_HomePhone, EN_DirectFax, EN_Email, EN_Name_First, EN_Name_Middle, EN_Name_Last, EN_Name_Suffix, EN_Is_Company, EN_Company_Name, EN_LSN",
                        "EntityXref as XrefClient", "XrefClient.EX_EN_Rep = EN_Key AND XrefClient.EX_IsClient = 1",
                        "EntityXref as XrefLawyer", "Entities.EN_Key =  XrefLawyer.EX_EN_Key",
                        "EntityRoles", "XrefLawyer.EX_ER_Code = ER_Code");
                    if (dataTable.Rows.Count > 0)
                    {
                        var row = dataTable.Rows[0];
                        switch (title)
                        {
                            case "matterlawyername":
                                cc.Range.Text = GetEntityFullName(row, 4, 5, 6, 7, 8, 9);
                                break;
                            case "matterlawyerphone":
                                cc.Range.Text = GetEntityPhone(row, 0, 1).SpaceIfEmpty();
                                break;
                            case "matterlawyerfax":
                                cc.Range.Text = row.ItemArray[2]?.ToString().SpaceIfEmpty();
                                break;
                            case "matterlawyeremail":
                                cc.Range.Text = row.ItemArray[3]?.ToString().SpaceIfEmpty();
                                break;
                            case "matterlawyerlsn":
                                cc.Range.Text = row.ItemArray[10]?.ToString().SpaceIfEmpty();
                                break;
                        }
                    }
                    else
                    {
                        cc.Range.Text = " ";
                    }

                    break;
                case "shortstyleofcause":
                    dataTable = DataSourceManager.GetData("InForm", $"IN_MM_Key = {matterId}", "IN_ShortStyle");
                    if (dataTable.Rows.Count > 0 && (cc.Range.Sections.First.Range.Start != document.Sections.Last.Range.Start || shortStyleOnlastSection))
                    {
                        var row = dataTable.Rows[0];

                        cc.Range.Text = ReplaceParagraphMarks(row.ItemArray[0]?.ToString()).SpaceIfEmpty();
                    }
                    else
                    {
                        cc.Range.Text = " ";
                    }

                    break;
                case "courtfilenumber":
                    dataTable = DataSourceManager.GetData("InForm", $"IN_MM_Key = {matterId}", "IN_CourtFileNo");
                    if (dataTable.Rows.Count > 0)
                    {
                        var row = dataTable.Rows[0];

                        cc.Range.Text = ReplaceParagraphMarks(row.ItemArray[0]?.ToString()).SpaceIfEmpty();
                    }
                    else
                    {
                        cc.Range.Text = " ";
                    }

                    break;
                case "courtname":
                    dataTable = DataSourceManager.GetData("InForm", $"IN_MM_Key = {matterId}", "IN_CourtName");
                    if (dataTable.Rows.Count > 0)
                    {
                        var row = dataTable.Rows[0];

                        string courtName = row.ItemArray[0]?.ToString().SpaceIfEmpty();
                        InsertCourtName(cc, courtName);
                    }
                    else
                    {
                        cc.Range.Text = " ";
                    }

                    break;
                case "placecommenced":
                    dataTable = DataSourceManager.GetData("InForm", $"IN_MM_Key = {matterId}", "IN_PlaceCommenced");
                    if (dataTable.Rows.Count > 0)
                    {
                        var row = dataTable.Rows[0];

                        cc.Range.Text = ReplaceParagraphMarks(row.ItemArray[0]?.ToString()).SpaceIfEmpty();
                    }
                    else
                    {
                        cc.Range.Text = " ";
                    }

                    break;
                case "actforparty":
                    GetActForParty(out string actForParty, out bool actForAllParties);
                    cc.Range.Text = actForParty;

                    break;
                case "actfornames":
                    dataTable = DataSourceManager.GetData("InForm", $"IN_MM_Key = {matterId}", "IN_ActForNames");
                    if (dataTable.Rows.Count > 0)
                    {
                        var row = dataTable.Rows[0];

                        cc.Range.Text = ReplaceParagraphMarks(row.ItemArray[0]?.ToString()).SpaceIfEmpty();
                    }
                    else
                    {
                        cc.Range.Text = " ";
                    }

                    break;
                case "actforpartyandnames":
                    // first get the party titles
                    GetActForParty(out actForParty, out actForAllParties);

                    // if we don't act for all the parties, append the party names
                    if (!actForAllParties)
                    {
                        dataTable = DataSourceManager.GetData("InForm", $"IN_MM_Key = {matterId}", "IN_ActForNames");
                        if (dataTable.Rows.Count > 0)
                        {
                            var row = dataTable.Rows[0];

                            actForParty += "\v" + ReplaceParagraphMarks(row.ItemArray[0]?.ToString()).SpaceIfEmpty();
                        }
                        else
                        {
                            cc.Range.Text = " ";
                        }
                    }

                    cc.Range.Text = actForParty;

                    break;
                case "longstyleofcause":
                    dataTable = DataSourceManager.GetData("InFormLSOC", $"IL_IN_Key = {matterId}", "IL_LSOC");
                    if (dataTable.Rows.Count > 0)
                    {
                        var row = dataTable.Rows[0];

                        string rtfText = row.ItemArray[0]?.ToString().SpaceIfEmpty();

                        if (string.IsNullOrWhiteSpace(rtfText))
                        {
                            cc.Range.Text = " ";
                        }
                        else
                        {
                            Range rng = cc.Range;
                            Clipboard.SetData(System.Windows.DataFormats.Rtf, rtfText);
                            rng.PasteSpecial(DataType: Microsoft.Office.Interop.Word.WdPasteDataType.wdPasteRTF);
                        }
                    }
                    else
                    {
                        cc.Range.Text = " ";
                    }
                    break;
            }

            if (IsCustomField(titleFullCase))
            {
                cc.Range.Text = TryGetCustomFieldValue(titleFullCase, matterId).SpaceIfEmpty();
            }
        }

        public static void InsertCourtName(ContentControl cc, string courtName)
        {
            List<KeyValuePair<string, string>> lines = new List<KeyValuePair<string, string>>();
            Range range = cc.Range;
            switch (courtName)
            {
                case "ON - Superior Court":
                    lines.Add(new KeyValuePair<string, string>("ONTARIO\v", "ib"));
                    lines.Add(new KeyValuePair<string, string>("SUPERIOR COURT OF JUSTICE", "b"));
                    InsertCourtText(range, lines);
                    break;
                case "CA - Federal Court":
                    lines.Add(new KeyValuePair<string, string>("FEDERAL COURT", "b"));
                    InsertCourtText(range, lines);
                    break;
                case "CA - Federal Court of Appeal":
                    lines.Add(new KeyValuePair<string, string>("FEDERAL COURT OF APPEAL", "b"));
                    InsertCourtText(range, lines);
                    break;
                case "ON - Commercial List":
                    lines.Add(new KeyValuePair<string, string>("ONTARIO\v", "ib"));
                    lines.Add(new KeyValuePair<string, string>("SUPERIOR COURT OF JUSTICE - COMMERCIAL LIST", "b"));
                    InsertCourtText(range, lines);
                    break;
                case "ON - Court of Appeal":
                    lines.Add(new KeyValuePair<string, string>("COURT OF APPEAL FOR ONTARIO", "b"));
                    InsertCourtText(range, lines);
                    break;
                case "ON - Divisional Court":
                    lines.Add(new KeyValuePair<string, string>("ONTARIO\v", "ib"));
                    lines.Add(new KeyValuePair<string, string>("SUPERIOR COURT OF JUSTICE\v", "b"));
                    lines.Add(new KeyValuePair<string, string>("(Divisional Court)", "b"));
                    InsertCourtText(range, lines);
                    break;
                case "ON - Family Court":
                    lines.Add(new KeyValuePair<string, string>("Superior Court of Justice - Family Court", ""));
                    InsertCourtText(range, lines);
                    break;
                case "ON - Ontario Court":
                    lines.Add(new KeyValuePair<string, string>("Ontario Court of Justice", ""));
                    InsertCourtText(range, lines);
                    break;
                default:
                    cc.Range.Text = courtName.SpaceIfEmpty();
                    break;
            }

            cc.Range.Paragraphs.First.Format.Alignment = WdParagraphAlignment.wdAlignParagraphCenter;
        }

        // Helper function to retrieve the Act For Party/Parties
        private static void GetActForParty(out string actForParty, out bool actForAllParties)
        {
            // initial values
            actForParty = " ";
            actForAllParties = false;

            System.Data.DataTable dataTable = DataSourceManager.GetData("InForm", $"IN_MM_Key = {matterId}", "IN_ActForParty");

            if (dataTable.Rows.Count == 0 || String.IsNullOrEmpty(dataTable.Rows[0].ItemArray[0]?.ToString()))
            {
                // didn't find anything in the InFORM table, check the EntityXref table
                dataTable = DataSourceManager.GetData("Entities", $"XrefClient.EX_MM_Key = {matterId} and XrefLawyer.EX_MM_Key = {matterId}",
                    "XrefClient.EX_PartyNo, XrefClient.EX_IsClient",
                    "EntityXref as XrefLawyer", "Entities.EN_Key =  XrefLawyer.EX_EN_Key",
                    "EntityXref as XrefClient", "XrefClient.EX_EN_Rep = EN_Key",
                    "EntityRoles", "XrefLawyer.EX_ER_Code = ER_Code");
            }

            try
            {
                // we have data, so figure out the titles of the parties we represent
                if (dataTable.Rows.Count > 0)
                {
                    List<int> weActForPartyNumbers = new List<int>();
                    List<int> othersActForPartyNumbers = new List<int>();
                    List<int> distinctWeActForPartyNumbers;

                    List<string> weActForPartyTitles = new List<string>();
                    Dictionary<int, string> partyTitlesForThisCollection = GetPartyTitles(InformDialog.CurrentFormCollection);

                    // categorize the parties that are being represented, do we act for or does somebody else act for
                    foreach (DataRow row in dataTable.Rows)
                    {
                        int partyNumber = 0;
                        bool weActForThisParty = false;

                        if (int.TryParse(ReplaceParagraphMarks(row.ItemArray[0]?.ToString()).SpaceIfEmpty(), out partyNumber))
                        {
                            if (bool.TryParse(ReplaceParagraphMarks(row.ItemArray[1]?.ToString()).SpaceIfEmpty(), out weActForThisParty))
                            {
                                if (weActForThisParty)
                                {
                                    // we rep this party
                                    weActForPartyNumbers.Add(partyNumber);
                                }
                                else
                                {
                                    // we don't rep this party
                                    othersActForPartyNumbers.Add(partyNumber);
                                }
                            }
                        }
                    }

                    distinctWeActForPartyNumbers = weActForPartyNumbers.Distinct().ToList();
                    distinctWeActForPartyNumbers.Sort();

                    foreach (int partyNumber in distinctWeActForPartyNumbers)
                    {
                        // do we rep all the parties of this type?
                        actForAllParties = (!othersActForPartyNumbers.Contains(partyNumber));

                        // get party titles and pluralize if needed
                        if (partyTitlesForThisCollection.TryGetValue(partyNumber, out string partyTitle))
                        {
                            if (weActForPartyNumbers.Where(x => x == partyNumber).Count() > 1)
                            {
                                weActForPartyTitles.Add(GetPluralPartyTitle(partyTitle));
                            }
                            else
                            {
                                weActForPartyTitles.Add(partyTitle);
                            }
                        }
                    }

                    // fill the placeholder
                    actForParty = String.Join(" and the ", weActForPartyTitles);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n\n" + ex.StackTrace, "Getting Act For Party from Matter", MessageBoxButtons.OK);
            }

            // if we didn't find anything, return the standard empty string for empty content controls
        }

        private static void InsertCourtText(Range range, List<KeyValuePair<string, string>> lines)
        {
            foreach (var line in lines)
            {
                range.Text = line.Key;
                range.Font.Bold = line.Value.Contains("b") ? -1 : 0;
                range.Font.Italic = line.Value.Contains("i") ? -1 : 0;
                range.Collapse(WdCollapseDirection.wdCollapseEnd);
            }
        }

        internal static bool FillInformLawyerFields(Document document)
        {
            if (document != null && HasMatterMasterMatterLoaded)
            {
                matterId = DataSourceManager.LoadedMatter.Id;
                var contentControls = DocumentUtils.GetAllContentControls(document, InformDialog.INFORM_LAWYER_TAG)?.ToList();

                List<int> lawyerKeys = new List<int>();

                // get primary lawyer, add it to the top of the list
                var lawyerDataTable = DataSourceManager.GetData("EntityXref as Client",
                                                                $"Client.EX_MM_Key = {matterId} and Client.EX_IsClient = 1", "Lawyer.EX_EN_Key",
                                                                "EntityXref as Lawyer", "(Client.EX_EN_Rep = Lawyer.EX_EN_Key and Client.EX_MM_Key = Lawyer.EX_MM_Key)");
                int primaryLawyerKey = 0;
                if (lawyerDataTable.Rows.Count > 0)
                {
                    foreach (DataRow row in lawyerDataTable.Rows)
                    {
                        int? lawyerKeyNullable = lawyerDataTable.Rows[0].ItemArray[0] as int?;
                        if (lawyerKeyNullable.HasValue)
                        {
                            lawyerKeys.Add(lawyerKeyNullable.Value);
                            primaryLawyerKey = lawyerKeyNullable.Value;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
                else
                {
                    return false;
                }

                // get other lawyers, if any, add them after the primary
                lawyerDataTable = DataSourceManager.GetData("EntityXref as Client",
                                                                $"Client.EX_MM_Key = {matterId} and Client.EX_IsClient = 1", "Lawyer.EX_EN_Key",
                                                                "EntityXref as Lawyer", "((Client.EX_Rep1 = Lawyer.EX_EN_Key OR Client.EX_Rep2 = Lawyer.EX_EN_Key OR Client.EX_Rep3 = Lawyer.EX_EN_Key OR Client.EX_Rep4 = Lawyer.EX_EN_Key) and Client.EX_MM_Key = Lawyer.EX_MM_Key)");
                if (lawyerDataTable.Rows.Count > 0)
                {
                    foreach (DataRow row in lawyerDataTable.Rows)
                    {
                        int? lawyerKeyNullable = lawyerDataTable.Rows[0].ItemArray[0] as int?;
                        if (lawyerKeyNullable.HasValue)
                        {
                            lawyerKeys.Add(lawyerKeyNullable.Value);
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
                else
                {
                    return false;
                }

                // need to grab InFORM options for processing address blocks
                InformOptions options = InformOptionsDialog.GetOptions();

                foreach (var cc in contentControls)
                {
                    string titleToLower = cc.Title?.ToLower();

                    System.Data.DataTable dataTable = null;
                    bool usesCompanyAddress = false;
                    dataTable = DataSourceManager.GetData("Entities", $"EN_Key = {primaryLawyerKey}", "EN_Use_CompAddr");
                    if (dataTable.Rows.Count > 0)
                    {
                        var row = dataTable.Rows[0];

                        usesCompanyAddress = row.ItemArray[0]?.ToString() == "1";
                    }
                    DataRow addressRow = null;
                    if (usesCompanyAddress)
                    {
                        dataTable = DataSourceManager.GetData("Entities as Lawyer", $"Lawyer.EN_Key = {primaryLawyerKey}", "AD_Street, AD_City, AD_Prov, AD_Postal, AD_Country",
                            "Entities as Company", "(Lawyer.EN_Company_Key = Company.EN_Key)",
                            "Addresses", "Company.EN_MailingAddress = Addresses.AD_Key");
                        if (dataTable.Rows.Count > 0)
                        {
                            addressRow = dataTable.Rows[0];
                        }
                    }
                    else
                    {
                        dataTable = DataSourceManager.GetData("Entities", $"EN_Key = {primaryLawyerKey}", "AD_Street, AD_City, AD_Prov, AD_Postal, AD_Country",
                            "Addresses", "Entities.EN_MailingAddress = Addresses.AD_Key");
                        if (dataTable.Rows.Count > 0)
                        {
                            addressRow = dataTable.Rows[0];
                        }
                    }

                    if (titleToLower != null)
                    {
                        switch (titleToLower)
                        {
                            case "company name":
                                dataTable = DataSourceManager.GetData("Entities", $"EN_Key = {primaryLawyerKey}", "EN_Company_Name");
                                if (dataTable.Rows.Count > 0)
                                {
                                    var row = dataTable.Rows[0];

                                    cc.Range.Text = ReplaceParagraphMarks(row.ItemArray[0]?.ToString()).SpaceIfEmpty();

                                    // formatting options for company name
                                    if (options.CompanyNameBoldface)
                                    {
                                        cc.Range.Font.Bold = -1;
                                    }

                                    if (options.CompanyNameAllCaps)
                                    {
                                        cc.Range.Font.AllCaps = -1;
                                    }
                                }
                                else
                                {
                                    cc.Range.Text = " ";
                                }
                                break;
                            case "company address":
                                if (addressRow != null)
                                {
                                    cc.Range.Text = ReplaceParagraphMarks(addressRow.ItemArray[0]?.ToString()).SpaceIfEmpty();
                                }
                                else
                                {
                                    cc.Range.Text = " ";
                                }
                                break;
                            case "company city":
                                if (addressRow != null)
                                {
                                    cc.Range.Text = ReplaceParagraphMarks(addressRow.ItemArray[1]?.ToString()).SpaceIfEmpty();
                                }
                                else
                                {
                                    cc.Range.Text = " ";
                                }
                                break;
                            case "company province":
                                if (addressRow != null)
                                {
                                    cc.Range.Text = ReplaceParagraphMarks(addressRow.ItemArray[2]?.ToString()).SpaceIfEmpty();
                                }
                                else
                                {
                                    cc.Range.Text = " ";
                                }
                                break;
                            case "company postal code":
                                if (addressRow != null)
                                {
                                    cc.Range.Text = ReplaceParagraphMarks(addressRow.ItemArray[3]?.ToString()).SpaceIfEmpty();
                                }
                                else
                                {
                                    cc.Range.Text = " ";
                                }
                                break;
                            case "company country":
                                if (addressRow != null)
                                {
                                    cc.Range.Text = ReplaceParagraphMarks(addressRow.ItemArray[4]?.ToString()).SpaceIfEmpty();
                                }
                                else
                                {
                                    cc.Range.Text = " ";
                                }
                                break;
                            case "our lawyers":
                                if (lawyerDataTable.Rows.Count > 0)
                                {
                                    cc.Range.Text = " ";
                                    Range fieldRange = cc.Range;

                                    foreach (int lawyerKey in lawyerKeys)
                                    {
                                        // get the data for this lawyer
                                        dataTable = DataSourceManager.GetData("Entities", $"EN_Key = {lawyerKey}", "EN_Name_First, EN_Name_Middle, EN_Name_Last, EN_Name_Suffix, EN_Is_Company, EN_Company_Name, EN_DirectPhone, EN_DirectFax, EN_Email, EN_LSN");

                                        if (dataTable.Rows.Count > 0)
                                        {
                                            DataRow row = dataTable.Rows[0];

                                            // lawyer name
                                            fieldRange.Text = GetEntityFullName(row, 0, 1, 2, 3, 4, 5);
                                            if (options.LawyerNameBoldface)
                                            {
                                                fieldRange.Font.Bold = -1;
                                            }
                                            fieldRange.Collapse(WdCollapseDirection.wdCollapseEnd);

                                            // law society #
                                            fieldRange.Text = $"  LSO#: {row.ItemArray[9]?.ToString()}\v";
                                            if (options.LawyerNameBoldface)
                                            {
                                                fieldRange.Font.Reset();
                                            }
                                            fieldRange.Collapse(WdCollapseDirection.wdCollapseEnd);

                                            // email
                                            if (options.IncludeEmailAddress)
                                            {
                                                fieldRange.Text = $"{row.ItemArray[8]?.ToString()}\v";
                                                fieldRange.Collapse(WdCollapseDirection.wdCollapseEnd);
                                            }

                                            // phone number
                                            if (options.IncludeBlankLineLawyerPhoneNumber)
                                            {
                                                fieldRange.Text = "\v";
                                                fieldRange.Collapse(WdCollapseDirection.wdCollapseEnd);
                                            }
                                            if (options.UseCompanyPhoneAndFax)
                                            {
                                                // TO DO: get actual company phone #
                                                fieldRange.Text = $"Tel: {row.ItemArray[6]?.ToString()}";
                                            }
                                            else
                                            {
                                                fieldRange.Text = $"Tel: {row.ItemArray[6]?.ToString()}";
                                            }
                                            fieldRange.Collapse(WdCollapseDirection.wdCollapseEnd);
                                        }

                                        // do we need a new paragraph for the next lawyer?
                                        if (lawyerKey != lawyerKeys.Last())
                                        {
                                            if (options.IncludeBlankLinesSameFirmLawyer)
                                            {
                                                fieldRange.Text = "\v\v";
                                            }
                                            else
                                            {
                                                fieldRange.Text = "\v";
                                            }
                                            fieldRange.Collapse(WdCollapseDirection.wdCollapseEnd);
                                        }
                                    }
                                }
                                else
                                {
                                    cc.Range.Text = " ";
                                }
                                break;
                            case "full name":
                                dataTable = DataSourceManager.GetData("Entities", $"EN_Key = {primaryLawyerKey}", "EN_Name_First, EN_Name_Middle, EN_Name_Last, EN_Name_Suffix, EN_Is_Company, EN_Company_Name");
                                if (dataTable.Rows.Count > 0)
                                {
                                    var row = dataTable.Rows[0];

                                    cc.Range.Text = GetEntityFullName(row, 0, 1, 2, 3, 4, 5);
                                    if (options.LawyerNameBoldface)
                                    {
                                        cc.Range.Font.Bold = -1;
                                    }
                                }
                                else
                                {
                                    cc.Range.Text = " ";
                                }
                                break;
                            case "direct phone":
                                dataTable = DataSourceManager.GetData("Entities", $"EN_Key = {primaryLawyerKey}", "EN_DirectPhone");
                                if (dataTable.Rows.Count > 0)
                                {
                                    var row = dataTable.Rows[0];

                                    cc.Range.Text = ReplaceParagraphMarks(row.ItemArray[0]?.ToString()).SpaceIfEmpty();
                                }
                                else
                                {
                                    cc.Range.Text = " ";
                                }
                                break;
                            case "direct fax":
                                dataTable = DataSourceManager.GetData("Entities", $"EN_Key = {primaryLawyerKey}", "EN_DirectFax");
                                if (dataTable.Rows.Count > 0)
                                {
                                    var row = dataTable.Rows[0];

                                    cc.Range.Text = ReplaceParagraphMarks(row.ItemArray[0]?.ToString()).SpaceIfEmpty();
                                }
                                else
                                {
                                    cc.Range.Text = " ";
                                }
                                break;
                            case "email":
                                dataTable = DataSourceManager.GetData("Entities", $"EN_Key = {primaryLawyerKey}", "EN_Email");
                                if (dataTable.Rows.Count > 0)
                                {
                                    var row = dataTable.Rows[0];

                                    cc.Range.Text = ReplaceParagraphMarks(row.ItemArray[0]?.ToString()).SpaceIfEmpty();
                                }
                                else
                                {
                                    cc.Range.Text = " ";
                                }
                                break;
                            case "lawyer id":
                                dataTable = DataSourceManager.GetData("Entities", $"EN_Key = {primaryLawyerKey}", "EN_LSN");
                                if (dataTable.Rows.Count > 0)
                                {
                                    var row = dataTable.Rows[0];

                                    cc.Range.Text = ReplaceParagraphMarks(row.ItemArray[0]?.ToString()).SpaceIfEmpty();
                                }
                                else
                                {
                                    cc.Range.Text = " ";
                                }
                                break;
                        }
                    }
                }
            }

            return true;
        }

        private static void FillEntityField(ContentControl cc, SelectedEntityModel entity)
        {
            string title = TrimSwitch(cc.Title)?.ToLower();
            if (title != null)
            {
                int index = title.IndexOf("_");

                if (title.Length > index + 1)
                {
                    title = title.Substring(index + 1);

                    System.Data.DataTable dataTable = null;
                    switch (title)
                    {
                        case "role":
                            cc.Range.Text = entity.RoleName.SpaceIfEmpty();
                            break;
                        case "fullname":
                            dataTable = DataSourceManager.GetData("Entities", $"EN_Key = {entity.Key}", "EN_Name_First, EN_Name_Middle, EN_Name_Last, EN_Name_Suffix, EN_Is_Company, EN_Company_Name");
                            if (dataTable.Rows.Count > 0)
                            {
                                var row = dataTable.Rows[0];

                                cc.Range.Text = GetEntityFullName(row, 0, 1, 2, 3, 4, 5);
                            }
                            else
                            {
                                cc.Range.Text = " ";
                            }
                            break;
                        case "title":
                            dataTable = DataSourceManager.GetData("Entities", $"EN_Key = {entity.Key}", "EN_Name_Title");
                            if (dataTable.Rows.Count > 0)
                            {
                                var row = dataTable.Rows[0];

                                cc.Range.Text = ReplaceParagraphMarks(row.ItemArray[0]?.ToString()).SpaceIfEmpty();
                            }
                            else
                            {
                                cc.Range.Text = " ";
                            }
                            break;
                        case "firstname":
                            dataTable = DataSourceManager.GetData("Entities", $"EN_Key = {entity.Key}", "EN_Name_First");
                            if (dataTable.Rows.Count > 0)
                            {
                                var row = dataTable.Rows[0];

                                cc.Range.Text = ReplaceParagraphMarks(row.ItemArray[0]?.ToString()).SpaceIfEmpty();
                            }
                            else
                            {
                                cc.Range.Text = " ";
                            }
                            break;
                        case "middlename":
                            dataTable = DataSourceManager.GetData("Entities", $"EN_Key = {entity.Key}", "EN_Name_Middle");
                            if (dataTable.Rows.Count > 0)
                            {
                                var row = dataTable.Rows[0];

                                cc.Range.Text = ReplaceParagraphMarks(row.ItemArray[0]?.ToString()).SpaceIfEmpty();
                            }
                            else
                            {
                                cc.Range.Text = " ";
                            }
                            break;
                        case "lastname":
                            dataTable = DataSourceManager.GetData("Entities", $"EN_Key = {entity.Key}", "EN_Name_Last");
                            if (dataTable.Rows.Count > 0)
                            {
                                var row = dataTable.Rows[0];

                                cc.Range.Text = ReplaceParagraphMarks(row.ItemArray[0]?.ToString()).SpaceIfEmpty();
                            }
                            else
                            {
                                cc.Range.Text = " ";
                            }
                            break;
                        case "suffix":
                            dataTable = DataSourceManager.GetData("Entities", $"EN_Key = {entity.Key}", "EN_Name_Suffix");
                            if (dataTable.Rows.Count > 0)
                            {
                                var row = dataTable.Rows[0];

                                cc.Range.Text = ReplaceParagraphMarks(row.ItemArray[0]?.ToString()).SpaceIfEmpty();
                            }
                            else
                            {
                                cc.Range.Text = " ";
                            }
                            break;
                        case "alias":
                            dataTable = DataSourceManager.GetData("Entities", $"EN_Key = {entity.Key}", "EN_Name_Alias");
                            if (dataTable.Rows.Count > 0)
                            {
                                var row = dataTable.Rows[0];

                                cc.Range.Text = ReplaceParagraphMarks(row.ItemArray[0]?.ToString()).SpaceIfEmpty();
                            }
                            else
                            {
                                cc.Range.Text = " ";
                            }
                            break;
                        case "salutation":
                            dataTable = DataSourceManager.GetData("Entities", $"EN_Key = {entity.Key}", "EN_Salutation");
                            if (dataTable.Rows.Count > 0)
                            {
                                var row = dataTable.Rows[0];

                                cc.Range.Text = ReplaceParagraphMarks(row.ItemArray[0]?.ToString()).SpaceIfEmpty();
                            }
                            else
                            {
                                cc.Range.Text = " ";
                            }
                            break;
                        case "jobtitle":
                            dataTable = DataSourceManager.GetData("Entities", $"EN_Key = {entity.Key}", "EN_JobTitle");
                            if (dataTable.Rows.Count > 0)
                            {
                                var row = dataTable.Rows[0];

                                cc.Range.Text = ReplaceParagraphMarks(row.ItemArray[0]?.ToString()).SpaceIfEmpty();
                            }
                            else
                            {
                                cc.Range.Text = " ";
                            }
                            break;
                        case "company":
                            dataTable = DataSourceManager.GetData("Entities", $"EN_Key = {entity.Key}", "EN_Company_Name");
                            if (dataTable.Rows.Count > 0)
                            {
                                var row = dataTable.Rows[0];

                                cc.Range.Text = ReplaceParagraphMarks(row.ItemArray[0]?.ToString()).SpaceIfEmpty();
                            }
                            else
                            {
                                cc.Range.Text = " ";
                            }
                            break;
                        case "division":
                            dataTable = DataSourceManager.GetData("Entities", $"EN_Key = {entity.Key}", "EN_Company_Division");
                            if (dataTable.Rows.Count > 0)
                            {
                                var row = dataTable.Rows[0];

                                cc.Range.Text = ReplaceParagraphMarks(row.ItemArray[0]?.ToString()).SpaceIfEmpty();
                            }
                            else
                            {
                                cc.Range.Text = " ";
                            }
                            break;
                        case "phone":
                            dataTable = DataSourceManager.GetData("Entities", $"EN_Key = {entity.Key}", "EN_DirectPhone, EN_HomePhone");
                            if (dataTable.Rows.Count > 0)
                            {
                                var row = dataTable.Rows[0];

                                cc.Range.Text = GetEntityPhone(row, 0, 1).SpaceIfEmpty();
                            }
                            else
                            {
                                cc.Range.Text = " ";
                            }
                            break;
                        case "directphone":
                            dataTable = DataSourceManager.GetData("Entities", $"EN_Key = {entity.Key}", "EN_DirectPhone");
                            if (dataTable.Rows.Count > 0)
                            {
                                var row = dataTable.Rows[0];

                                cc.Range.Text = ReplaceParagraphMarks(row.ItemArray[0]?.ToString()).SpaceIfEmpty();
                            }
                            else
                            {
                                cc.Range.Text = " ";
                            }
                            break;
                        case "directfax":
                        case "fax":
                            dataTable = DataSourceManager.GetData("Entities", $"EN_Key = {entity.Key}", "EN_DirectFax");
                            if (dataTable.Rows.Count > 0)
                            {
                                var row = dataTable.Rows[0];

                                cc.Range.Text = ReplaceParagraphMarks(row.ItemArray[0]?.ToString()).SpaceIfEmpty();
                            }
                            else
                            {
                                cc.Range.Text = " ";
                            }
                            break;
                        case "mobilephone":
                            dataTable = DataSourceManager.GetData("Entities", $"EN_Key = {entity.Key}", "EN_Mobile");
                            if (dataTable.Rows.Count > 0)
                            {
                                var row = dataTable.Rows[0];

                                cc.Range.Text = ReplaceParagraphMarks(row.ItemArray[0]?.ToString()).SpaceIfEmpty();
                            }
                            else
                            {
                                cc.Range.Text = " ";
                            }
                            break;
                        case "homephone":
                            dataTable = DataSourceManager.GetData("Entities", $"EN_Key = {entity.Key}", "EN_HomePhone");
                            if (dataTable.Rows.Count > 0)
                            {
                                var row = dataTable.Rows[0];

                                cc.Range.Text = ReplaceParagraphMarks(row.ItemArray[0]?.ToString()).SpaceIfEmpty();
                            }
                            else
                            {
                                cc.Range.Text = " ";
                            }
                            break;
                        case "email":
                            dataTable = DataSourceManager.GetData("Entities", $"EN_Key = {entity.Key}", "EN_Email");
                            if (dataTable.Rows.Count > 0)
                            {
                                var row = dataTable.Rows[0];

                                cc.Range.Text = ReplaceParagraphMarks(row.ItemArray[0]?.ToString()).SpaceIfEmpty();
                            }
                            else
                            {
                                cc.Range.Text = " ";
                            }
                            break;
                        case "streetaddress":
                            dataTable = DataSourceManager.GetData("EntityXref", $"EX_EN_Key = {entity.Key} and EX_MM_Key = {matterId}", "AD_Street", "Addresses", "EX_AD_Key = AD_Key");
                            if (dataTable.Rows.Count > 0)
                            {
                                var row = dataTable.Rows[0];

                                cc.Range.Text = ReplaceParagraphMarks(row.ItemArray[0]?.ToString()).SpaceIfEmpty();
                            }
                            else
                            {
                                cc.Range.Text = " ";
                            }
                            break;
                        case "city":
                            dataTable = DataSourceManager.GetData("EntityXref", $"EX_EN_Key = {entity.Key} and EX_MM_Key = {matterId}", "AD_City", "Addresses", "EX_AD_Key = AD_Key");
                            if (dataTable.Rows.Count > 0)
                            {
                                var row = dataTable.Rows[0];

                                cc.Range.Text = ReplaceParagraphMarks(row.ItemArray[0]?.ToString()).SpaceIfEmpty();
                            }
                            else
                            {
                                cc.Range.Text = " ";
                            }
                            break;
                        case "prov":
                            dataTable = DataSourceManager.GetData("EntityXref", $"EX_EN_Key = {entity.Key} and EX_MM_Key = {matterId}", "AD_Prov", "Addresses", "EX_AD_Key = AD_Key");
                            if (dataTable.Rows.Count > 0)
                            {
                                var row = dataTable.Rows[0];

                                cc.Range.Text = ReplaceParagraphMarks(row.ItemArray[0]?.ToString()).SpaceIfEmpty();
                            }
                            else
                            {
                                cc.Range.Text = " ";
                            }
                            break;
                        case "postal":
                            dataTable = DataSourceManager.GetData("EntityXref", $"EX_EN_Key = {entity.Key} and EX_MM_Key = {matterId}", "AD_Postal", "Addresses", "EX_AD_Key = AD_Key");
                            if (dataTable.Rows.Count > 0)
                            {
                                var row = dataTable.Rows[0];

                                cc.Range.Text = ReplaceParagraphMarks(row.ItemArray[0]?.ToString()).SpaceIfEmpty();
                            }
                            else
                            {
                                cc.Range.Text = " ";
                            }
                            break;
                        case "country":
                            dataTable = DataSourceManager.GetData("EntityXref", $"EX_EN_Key = {entity.Key} and EX_MM_Key = {matterId}", "AD_Country", "Addresses", "EX_AD_Key = AD_Key");
                            if (dataTable.Rows.Count > 0)
                            {
                                var row = dataTable.Rows[0];

                                cc.Range.Text = ReplaceParagraphMarks(row.ItemArray[0]?.ToString()).SpaceIfEmpty();
                            }
                            else
                            {
                                cc.Range.Text = " ";

                            }
                            break;
                        case "fulladdress":
                            dataTable = DataSourceManager.GetData("EntityXref", $"EX_EN_Key = {entity.Key} and EX_MM_Key = {matterId}", "AD_Street, AD_City, AD_Prov, AD_Postal, AD_Country", "Addresses", "EX_AD_Key = AD_Key");
                            if (dataTable.Rows.Count > 0)
                            {
                                var row = dataTable.Rows[0];

                                cc.Range.Text = GetFullAddress(row, 0, 1, 2, 3, 4);
                            }
                            else
                            {
                                cc.Range.Text = " ";

                            }
                            break;
                        case "lawsocietynumber":
                            dataTable = DataSourceManager.GetData("Entities", $"EN_Key = {entity.Key}", "EN_LSN");
                            if (dataTable.Rows.Count > 0)
                            {
                                var row = dataTable.Rows[0];

                                cc.Range.Text = ReplaceParagraphMarks(row.ItemArray[0]?.ToString()).SpaceIfEmpty();
                            }
                            else
                            {
                                cc.Range.Text = " ";
                            }
                            break;
                        case "reline":
                            dataTable = DataSourceManager.GetData("EntityXref", $"EX_MM_Key = {matterId} and EX_EN_Key = {entity.Key}", "EX_ReLine");
                            if (dataTable.Rows.Count > 0)
                            {
                                var row = dataTable.Rows[0];

                                cc.Range.Text = ReplaceParagraphMarks(row.ItemArray[0]?.ToString()).SpaceIfEmpty();
                            }
                            else
                            {
                                cc.Range.Text = " ";
                            }

                            if (cc.Range.Text == " ")
                            {
                                dataTable = DataSourceManager.GetData("MatterMaster", $"MM_KEY = {matterId}", "MM_MatterReLine");
                                if (dataTable.Rows.Count > 0)
                                {
                                    cc.Range.Text = ReplaceParagraphMarks(dataTable.Rows[0].ItemArray.FirstOrDefault()?.ToString()).SpaceIfEmpty();
                                }
                                else
                                {
                                    cc.Range.Text = " ";
                                }
                            }
                            break;
                        case "notes":
                            dataTable = DataSourceManager.GetData("EntityXref", $"EX_MM_Key = {matterId} and EX_EN_Key = {entity.Key}", "EX_Notes");
                            if (dataTable.Rows.Count > 0)
                            {
                                var row = dataTable.Rows[0];

                                cc.Range.Text = ReplaceParagraphMarks(row.ItemArray[0]?.ToString()).SpaceIfEmpty();
                            }
                            else
                            {
                                cc.Range.Text = " ";
                            }
                            break;
                        case "user1":
                            dataTable = DataSourceManager.GetData("Entities", $"EN_Key = {entity.Key}", "EN_User1");
                            if (dataTable.Rows.Count > 0)
                            {
                                var row = dataTable.Rows[0];

                                cc.Range.Text = ReplaceParagraphMarks(row.ItemArray[0]?.ToString()).SpaceIfEmpty();
                            }
                            else
                            {
                                cc.Range.Text = " ";
                            }
                            break;
                        case "user2":
                            dataTable = DataSourceManager.GetData("Entities", $"EN_Key = {entity.Key}", "EN_User2");
                            if (dataTable.Rows.Count > 0)
                            {
                                var row = dataTable.Rows[0];

                                cc.Range.Text = ReplaceParagraphMarks(row.ItemArray[0]?.ToString()).SpaceIfEmpty();
                            }
                            else
                            {
                                cc.Range.Text = " ";
                            }
                            break;
                        case "user3":
                            dataTable = DataSourceManager.GetData("Entities", $"EN_Key = {entity.Key}", "EN_User3");
                            if (dataTable.Rows.Count > 0)
                            {
                                var row = dataTable.Rows[0];

                                cc.Range.Text = ReplaceParagraphMarks(row.ItemArray[0]?.ToString()).SpaceIfEmpty();
                            }
                            else
                            {
                                cc.Range.Text = " ";
                            }
                            break;
                        case "user4":
                            dataTable = DataSourceManager.GetData("Entities", $"EN_Key = {entity.Key}", "EN_User4");
                            if (dataTable.Rows.Count > 0)
                            {
                                var row = dataTable.Rows[0];

                                cc.Range.Text = ReplaceParagraphMarks(row.ItemArray[0]?.ToString()).SpaceIfEmpty();
                            }
                            else
                            {
                                cc.Range.Text = " ";
                            }
                            break;
                        case "user5":
                            dataTable = DataSourceManager.GetData("Entities", $"EN_Key = {entity.Key}", "EN_User5");
                            if (dataTable.Rows.Count > 0)
                            {
                                var row = dataTable.Rows[0];

                                cc.Range.Text = ReplaceParagraphMarks(row.ItemArray[0]?.ToString()).SpaceIfEmpty();
                            }
                            else
                            {
                                cc.Range.Text = " ";
                            }
                            break;
                        case "ref1":
                            dataTable = DataSourceManager.GetData("EntityXref", $"EX_MM_Key = {matterId} and EX_EN_Key = {entity.Key}", "EX_Ref1");
                            if (dataTable.Rows.Count > 0)
                            {
                                var row = dataTable.Rows[0];

                                cc.Range.Text = ReplaceParagraphMarks(row.ItemArray[0]?.ToString()).SpaceIfEmpty();
                            }
                            else
                            {
                                cc.Range.Text = " ";
                            }
                            break;
                        case "ref2":
                            dataTable = DataSourceManager.GetData("EntityXref", $"EX_MM_Key = {matterId} and EX_EN_Key = {entity.Key}", "EX_Ref2");
                            if (dataTable.Rows.Count > 0)
                            {
                                var row = dataTable.Rows[0];

                                cc.Range.Text = ReplaceParagraphMarks(row.ItemArray[0]?.ToString()).SpaceIfEmpty();
                            }
                            else
                            {
                                cc.Range.Text = " ";
                            }
                            break;
                        case "partynumber":
                            dataTable = DataSourceManager.GetData("EntityXref", $"EX_MM_Key = {matterId} and EX_EN_Key = {entity.Key}", "EX_PartyNo");
                            if (dataTable.Rows.Count > 0)
                            {
                                var row = dataTable.Rows[0];

                                cc.Range.Text = ReplaceParagraphMarks(row.ItemArray[0]?.ToString()).SpaceIfEmpty();
                            }
                            else
                            {
                                cc.Range.Text = " ";
                            }
                            break;
                        case "isminor":
                            dataTable = DataSourceManager.GetData("EntityXref", $"EX_MM_Key = {matterId} and EX_EN_Key = {entity.Key}", "EX_IsMinor");
                            if (dataTable.Rows.Count > 0)
                            {
                                var row = dataTable.Rows[0];

                                cc.Range.Text = ReplaceParagraphMarks(row.ItemArray[0]?.ToString()).SpaceIfEmpty();
                            }
                            else
                            {
                                cc.Range.Text = " ";
                            }
                            break;
                        case "lawyername":
                            dataTable = DataSourceManager.GetData("EntityXref", $"EX_MM_Key = {matterId} and EX_EN_Key = {entity.Key}", "EN_Name_First, EN_Name_Middle, EN_Name_Last, EN_Name_Suffix, EN_Is_Company, EN_Company_Name", "Entities", "EX_EN_Rep = EN_Key");
                            if (dataTable.Rows.Count > 0)
                            {
                                var row = dataTable.Rows[0];

                                cc.Range.Text = GetEntityFullName(row, 0, 1, 2, 3, 4, 5);
                            }
                            else
                            {
                                cc.Range.Text = " ";
                            }
                            break;
                        case "lawyerlsn":
                            dataTable = DataSourceManager.GetData("EntityXref", $"EX_MM_Key = {matterId} and EX_EN_Key = {entity.Key}", "EN_LSN", "Entities", "EX_EN_Rep = EN_Key");
                            if (dataTable.Rows.Count > 0)
                            {
                                var row = dataTable.Rows[0];

                                cc.Range.Text = ReplaceParagraphMarks(row.ItemArray[0]?.ToString()).SpaceIfEmpty();
                            }
                            else
                            {
                                cc.Range.Text = " ";
                            }
                            break;
                        case "lawyerfirm":
                            dataTable = DataSourceManager.GetData("EntityXref", $"EX_MM_Key = {matterId} and EX_EN_Key = {entity.Key}", "EN_Company_Name", "Entities", "EX_EN_Rep = EN_Key");
                            if (dataTable.Rows.Count > 0)
                            {
                                var row = dataTable.Rows[0];

                                cc.Range.Text = ReplaceParagraphMarks(row.ItemArray[0]?.ToString()).SpaceIfEmpty();
                            }
                            else
                            {
                                cc.Range.Text = " ";
                            }
                            break;
                        case "lawyerphone":
                            dataTable = DataSourceManager.GetData("EntityXref", $"EX_MM_Key = {matterId} and EX_EN_Key = {entity.Key}", "EN_DirectPhone, EN_HomePhone", "Entities", "EX_EN_Rep = EN_Key");
                            if (dataTable.Rows.Count > 0)
                            {
                                var row = dataTable.Rows[0];

                                cc.Range.Text = GetEntityPhone(row, 0, 1).SpaceIfEmpty();
                            }
                            else
                            {
                                cc.Range.Text = " ";
                            }
                            break;
                        case "lawyerfax":
                            dataTable = DataSourceManager.GetData("EntityXref", $"EX_MM_Key = {matterId} and EX_EN_Key = {entity.Key}", "EN_DirectFax", "Entities", "EX_EN_Rep = EN_Key");
                            if (dataTable.Rows.Count > 0)
                            {
                                var row = dataTable.Rows[0];

                                cc.Range.Text = ReplaceParagraphMarks(row.ItemArray[0]?.ToString()).SpaceIfEmpty();
                            }
                            else
                            {
                                cc.Range.Text = " ";
                            }
                            break;
                        case "lawyerfulladdress":
                            dataTable = DataSourceManager.GetData("EntityXref as Client", $"Client.EX_MM_Key = {matterId} and Lawyer.EX_MM_Key = {matterId} and Client.EX_EN_Key = {entity.Key}",
                                "AD_Street, AD_City, AD_Prov, AD_Postal, AD_Country",
                                "EntityXref as Lawyer ", "Client.EX_EN_Rep = Lawyer.EX_EN_Key",
                                "Addresses", "AD_Key = Lawyer.EX_AD_Key");
                            if (dataTable.Rows.Count > 0)
                            {
                                var row = dataTable.Rows[0];

                                cc.Range.Text = GetFullAddress(row, 0, 1, 2, 3, 4);
                            }
                            else
                            {
                                cc.Range.Text = " ";
                            }
                            break;
                        case "lawyerclientsall":
                            dataTable = DataSourceManager.GetData("EntityXref as a", $"a.EX_MM_Key = {matterId} and a.EX_EN_Key = {entity.Key} and a.EX_EN_Rep > 0", "DISTINCT EN_Name_First, EN_Name_Middle, EN_Name_Last, EN_Name_Suffix, EN_Is_Company, EN_Company_Name",
                                "EntityXref as b", "a.EX_EN_Rep = b.EX_EN_Rep",
                                "Entities", "b.EX_EN_Key = EN_Key");
                            List<string> fullNames = new List<string>();
                            foreach (DataRow row in dataTable.Rows)
                            {
                                string fullName = string.Empty;
                                fullName = GetEntityFullName(row, 0, 1, 2, 3, 4, 5);

                                fullNames.Add(fullName);
                            }
                            cc.Range.Text = ConcatenateAllNames(fullNames).SpaceIfEmpty();
                            break;
                        case "iscompany":
                            dataTable = DataSourceManager.GetData("Entities", $"EN_Key = {entity.Key}", "EN_Is_Company");
                            if (dataTable.Rows.Count > 0)
                            {
                                var row = dataTable.Rows[0];

                                cc.Range.Text = ReplaceParagraphMarks(row.ItemArray[0]?.ToString()).SpaceIfEmpty();
                            }
                            else
                            {
                                cc.Range.Text = " ";
                            }
                            break;
                        case "fileno":
                            // entity is a lawyer, return their file number
                            dataTable = DataSourceManager.GetData("EntityXRef", $"EX_MM_Key = {matterId} and EX_EN_Key = {entity.Key}", "EX_YourFile");
                            if (dataTable.Rows.Count > 0)
                            {
                                var row = dataTable.Rows[0];
                                cc.Range.Text = ReplaceParagraphMarks(row.ItemArray[0]?.ToString()).SpaceIfEmpty();
                            }
                            else
                            {
                                cc.Range.Text = "";
                            }
                            break;
                        case "repfornames":
                            // entity is a lawyer, return a list of their clients
                            dataTable = DataSourceManager.GetData("Entities", $"EX_MM_Key = {matterId} and EX_EN_Rep = {entity.Key}",
                                "EN_Name_First, EN_Name_Middle, EN_Name_Last, EN_Name_Suffix, EN_Is_Company, EN_Company_Name",
                                "EntityXRef", "EX_EN_Key = EN_Key", "MatterMaster", "EX_MM_Key = MM_Key");
                            if (dataTable.Rows.Count > 0)
                            {
                                List<string> repForNames = new List<string>();
                                foreach (DataRow row in dataTable.Rows)
                                {
                                    // convert each row to an appropriate entity name
                                    repForNames.Add(GetEntityFullName(row, 0, 1, 2, 3, 4, 5));
                                }
                                // format as single string
                                string repForNamesToCC = "";
                                switch (repForNames.Count)
                                {
                                    case 1:
                                        repForNamesToCC = repForNames[0];
                                        break;
                                    case 2:
                                        repForNamesToCC = String.Join(" and ", repForNames);
                                        break;
                                    default:
                                        repForNamesToCC = $"{String.Join(", ", repForNames.GetRange(0, repForNames.Count - 1))} and { repForNames.Last()}";
                                        break;
                                }
                                cc.Range.Text = repForNamesToCC;
                            }
                            else
                            {
                                cc.Range.Text = "";
                            }
                            break;
                    }
                }
            }
        }

        private static string ReplaceParagraphMarks(string input)
        {
            if (input != null)
            {
                input = input.Trim().Replace("\r\n", "\v").Replace("\r", "\v").Replace("\n", "\v");
            }

            return input;
        }

        private static string GetFullAddress(DataRow row, int streetIdx, int cityIdx, int provIdx, int postalIdx, int countryIdx)
        {
            if (row == null)
            {
                return null;
            }
            List<string> fullAddress = new List<string>()
               {
                   ReplaceParagraphMarks(row.ItemArray[streetIdx]?.ToString())
               };

            List<string> secondLine = new List<string>()
            {
                ReplaceParagraphMarks(row.ItemArray[cityIdx]?.ToString()),
                ReplaceParagraphMarks(row.ItemArray[provIdx]?.ToString()),
                ReplaceParagraphMarks(row.ItemArray[postalIdx]?.ToString())
            };
            fullAddress.Add(string.Join(" ", secondLine.Where(x => !string.IsNullOrWhiteSpace(x))));
            fullAddress.Add(ReplaceParagraphMarks(row.ItemArray[countryIdx]?.ToString()));
            return string.Join("\v", fullAddress.Where(x => !string.IsNullOrWhiteSpace(x)));
        }

        private static void FillEntityField(ContentControl cc, List<SelectedEntityModel> entities)
        {
            if (cc != null)
            {
                if (entities?.Count > 0)
                {
                    string title = TrimSwitch(cc.Title);
                    if (title != null)
                    {
                        int index = title.IndexOf("_");
                        if (index > 0)
                        {
                            string role = title.Substring(1, index - 1);
                            SelectedEntityModel entity = null;
                            if (role == "*")
                            {
                                entity = entities.First();
                            }
                            else
                            {
                                entity = entities.Where(x => x.RoleCode == role).FirstOrDefault();
                            }

                            if (entity != null)
                            {
                                FillEntityField(cc, entity);
                            }
                        }
                    }
                }
                else
                {
                    cc.Range.Text = " ";
                }
            }
        }

        private static string GetEntityPhone(DataRow row, int directPhoneIdx, int homePhoneIdx)
        {
            if (string.IsNullOrWhiteSpace(row.ItemArray[directPhoneIdx]?.ToString()))
            {
                return ReplaceParagraphMarks(row.ItemArray[homePhoneIdx]?.ToString());
            }
            else
            {
                return ReplaceParagraphMarks(row.ItemArray[directPhoneIdx]?.ToString());
            }
        }

        public static string GetEntityFullName(DataRow row, int firstNameIdx, int middleNameIdx, int lastNameIdx, int? suffixIdx, int isCompanyIdx, int companyNameIdx)
        {
            string fullName;
            if (row.ItemArray[isCompanyIdx]?.ToString() == "True")
            {
                fullName = ReplaceParagraphMarks(row.ItemArray[companyNameIdx]?.ToString()).SpaceIfEmpty();
            }
            else
            {
                List<string> nameComponents = new List<string>()
                                        {
                                            ReplaceParagraphMarks(row.ItemArray[firstNameIdx]?.ToString()),
                                            ReplaceParagraphMarks(row.ItemArray[middleNameIdx]?.ToString()),
                                            ReplaceParagraphMarks(row.ItemArray[lastNameIdx]?.ToString()),
                                            (suffixIdx.HasValue ?ReplaceParagraphMarks( row.ItemArray[suffixIdx.Value]?.ToString()): null),
                                        };

                fullName = string.Join(" ", nameComponents.Where(x => !string.IsNullOrWhiteSpace(x)));
            }

            return fullName;
        }

        private static string TryGetCustomFieldValue(string title, string matterId)
        {
            var dataTable = DataSourceManager.GetData("MatterCustomFields", $"MF_MM_Key = {matterId} and MF_FieldName = @title", "MF_Value", parameters: new Dictionary<string, string>() { { "title", title } });

            if (dataTable.Rows.Count > 0)
            {
                return dataTable.Rows[0].ItemArray.FirstOrDefault()?.ToString();
            }

            return null;
        }

        private static bool IsCustomField(string title)
        {
            var dataTable = DataSourceManager.GetData("CustomFieldDefs", $"FD_FieldName = @title", "FD_FieldName", parameters: new Dictionary<string, string>() { { "title", title } });
            return dataTable.Rows.Count > 0;
        }

        private static string TrimSwitch(string title)
        {
            if (title != null)
            {
                int index = title.IndexOf("<");

                if (index > -1)
                {
                    return title.Substring(0, index).Trim();
                }
            }

            return title;
        }

        private static void ApplySwitches(ContentControl cc)
        {
            string title = cc.Title;

            if (title != null)
            {
                if (title.Contains(DELIFEMPTY_SWITCH) || title.Contains(DELLINEIFNOVALUE_SWITCH))
                {
                    var row = BaseUtils.GetLineOfRange(cc.Range);

                    if (title.Contains(DELLINEIFNOVALUE_SWITCH) || string.IsNullOrWhiteSpace(row.Text))
                    {
                        if (!string.IsNullOrWhiteSpace(cc.Range.Text))
                        {
                            row.Font.Hidden = 0;
                        }
                        else
                        {
                            row.Font.Hidden = -1;
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrWhiteSpace(cc.Range.Text))
                        {
                            cc.Range.Font.Hidden = 0;
                        }
                        else
                        {
                            cc.Range.Font.Hidden = -1;
                        }
                    }
                }

                string regex = @"<\\Boolean:.*[^\\]\|.*[^\\]>";
                if (Regex.IsMatch(title, regex))
                {
                    var match = Regex.Matches(title, regex)[0];

                    string switchText = match.Value;

                    var pipeMatch = Regex.Matches(switchText, @"[^\\]\|")[0];
                    string falseValue = switchText.Substring(@"<\Boolean:".Length, pipeMatch.Index - @"<\Boolean:".Length + 1);
                    string trueValue = switchText.Substring(pipeMatch.Index + 2, switchText.Length - pipeMatch.Index - 3);

                    if (cc.Range.Text == "True")
                    {
                        cc.Range.Text = UnescapeBooleanSwitch(trueValue);
                    }
                    else
                    {
                        cc.Range.Text = UnescapeBooleanSwitch(falseValue);
                    }
                }
            }
        }

        private static string UnescapeBooleanSwitch(string input)
        {
            if (input == null)
            {
                return null;
            }

            return input.Replace("\\|", "|").Replace("\\>", ">");
        }

        public static string EscapeBooleanSwitch(string input)
        {
            if (input == null)
            {
                return null;
            }

            return input.Replace("|", "\\|").Replace(">", "\\>");
        }

        private static List<ContentControl> FillERBs(List<Bookmark> erbs, List<ContentControl> contentControls, List<SelectedEntityModel> entities)
        {
            List<ContentControl> result = new List<ContentControl>();

            foreach (Bookmark bookmark in erbs)
            {
                try
                {
                    string bookMakrName = bookmark.Name;
                }
                catch(Exception ex)
                {
                    continue;
                }
                List<ContentControl> thisBookmarkCCs = FindContentControlsInBookMark(bookmark, contentControls).ToList();

                var firstCC = thisBookmarkCCs.Where(x => x.Title.StartsWith("@")).FirstOrDefault();
                List<SelectedEntityModel> entitiesToUse = entities;

                if (firstCC != null)
                {
                    string title = TrimSwitch(firstCC.Title);
                    if (title != null)
                    {
                        int index = title.IndexOf("_");
                        if (index > 0)
                        {
                            string role = title.Substring(1, index - 1);
                            if (role != "*")
                            {
                                entitiesToUse = entitiesToUse.Where(x => x.RoleCode == role).ToList();
                            }
                        }
                    }
                }

                if (bookmark.Name.Contains("CC") && !bookmark.Name.Contains("BCC"))
                {
                    entitiesToUse = entitiesToUse.Where(x => x.CC).ToList();
                }
                else if (bookmark.Name.Contains("BCC"))
                {
                    entitiesToUse = entitiesToUse.Where(x => x.BCC).ToList();
                }
                else
                {
                    entitiesToUse = entitiesToUse.Where(x => !x.CC && !x.BCC).ToList();
                }

                result.AddRange(thisBookmarkCCs);

                if (entitiesToUse.Count == 0)
                {
                    bookmark.Range.Delete();
                    bookmark.Delete();
                }
                else
                {
                    ContentControl recipientContentControl = GetContainingRecipientContentControl(bookmark);
                    if (recipientContentControl != null)
                    {
                        recipientContentControl.Delete(false);
                    }

                    Range startRange = bookmark.Range;
                    if (entitiesToUse.Count > 1)
                    {
                        for (int i = 1; i < entitiesToUse.Count; i++)
                        {
                            startRange.Copy();
                            Range endRange = startRange.Duplicate;
                            endRange.Collapse(WdCollapseDirection.wdCollapseEnd);

                            endRange.Paste();

                            Range addEnterRange = endRange.Duplicate;
                            addEnterRange.Collapse();
                            Random rand = new Random();
                            document.Bookmarks.Add(CustomConstants.ERB_BLANK_LINE + rand.Next(),addEnterRange);

                            startRange = endRange;
                        }
                    }
                    if (recipientContentControl != null)
                    {
                        AddRecipientContentControl(bookmark.Range.Start, startRange.End);
                    }

                    List<Word.ContentControl> newContentControls = new List<ContentControl>();

                    foreach (Word.ContentControl cc in bookmark.Range.Document.Range(bookmark.Range.Start, startRange.End).ContentControls)
                    {
                        newContentControls.Add(cc);
                    }

                    StringCounter stringCounter = new StringCounter();

                    foreach (ContentControl cc in newContentControls)
                    {
                        int count = stringCounter.GetStringCount(cc.Title);

                        if (cc.Title?.StartsWith("@") == true)
                        {
                            if (count < entitiesToUse.Count)
                            {
                                FillEntityField(cc, entitiesToUse[count]);
                            }
                        }
                        else
                        {
                            FillNonEntityFields(cc);
                        }

                        ApplySwitches(cc);
                        stringCounter.AddString(cc.Title);
                    }
                }
            }

            return result;
        }

        private static void AddRecipientContentControl(int start, int end)
        {
            try
            {
                Range range = document.Range(start, end);
                ContentControl cc = document.ContentControls.Add(WdContentControlType.wdContentControlRichText, range);
                cc.Title = "Recipient(s)";
                cc.Tag = CustomConstants.TemplateTags;
            }
            catch (NotImplementedException ex)
            {
                DocumentUtil.ShowCompatibilityModeError(new System.Threading.Tasks.Task(() => AddRecipientContentControl(start, end)));
            }
        }

        private static ContentControl GetContainingRecipientContentControl(Bookmark bookmark)
        {
            Range range = bookmark.Range;

            foreach (ContentControl cc in range.ContentControls)
            {
                if (cc.Title == "Recipient(s)" && cc.Tag == CustomConstants.TemplateTags && cc.Range.Start - range.Start < 2 && range.End - cc.Range.End < 3)
                {
                    return cc;
                }
            }

            return null;
        }

        private static List<ContentControl> FindContentControlsInBookMark(Bookmark bookmark, List<ContentControl> contentControls)
        {
            List<ContentControl> result = new List<ContentControl>();
            foreach (ContentControl cc in contentControls)
            {
                try
                {
                    if (cc.Range.InRange(bookmark.Range))
                    {
                        result.Add(cc);
                    }
                }
                catch(Exception Ex)
                {
                }
            }

            return result;
        }

        private static List<Bookmark> FindERBs(Document document)
        {
            List<Bookmark> result = new List<Bookmark>();

            foreach (Bookmark bookmark in document.Bookmarks)
            {
                if (bookmark.Name.StartsWith(CustomConstants.ERB_NAME))
                {
                    result.Add(bookmark);
                }
            }

            return result;
        }

        private static string ConcatenateAllNames(List<string> input)
        {
            if (input != null && input.Count > 0)
            {
                string last = input.Last();

                input.RemoveAt(input.Count - 1);


                string beforeLast = null;
                if (input.Count > 0)
                {
                    beforeLast = string.Join(", ", input);
                }

                return (beforeLast != null ? beforeLast + " and " : "") + last;
            }
            return null;
        }

        private static string GetPartyXNames(int nr, string matterId)
        {
            System.Data.DataTable dataTable = DataSourceManager.GetData("MatterMaster", $"MM_KEY = {matterId} and EX_PartyNo = {nr}", "EN_Name_First, EN_Name_Middle, EN_Name_Last, EN_Name_Suffix, EN_Is_Company, EN_Company_Name", "EntityXref", "MM_Key = EX_MM_Key", "Entities", "EX_EN_Key = EN_Key");
            List<string> fullNames = new List<string>();
            foreach (DataRow row in dataTable.Rows)
            {
                string fullName = GetEntityFullName(row, 0, 1, 2, 3, 4, 5);

                fullNames.Add(fullName);
            }

            return ConcatenateAllNames(fullNames);
        }
    }

    public class Lawyer
    {
        public string FullName { get; set; }
        public string LSNr { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Fax { get; set; }

        public override bool Equals(Object obj)
        {
            //Check for null and compare run-time types.
            if ((obj == null) || !this.GetType().Equals(obj.GetType()))
            {
                return false;
            }
            else
            {
                Lawyer p = (Lawyer)obj;
                return FullName == p.FullName && LSNr == p.LSNr && Phone == p.Phone && Email == p.Email && Fax == p.Fax;
            }
        }
    }

    public class LawyerCompany
    {
        public string CompanyName { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string Province { get; set; }
        public string PostalCode { get; set; }
        public Dictionary<string, List<string>> RepresentedNames { get; set; }
        public List<Lawyer> Lawyers { get; set; }
        public bool SelfRepresented { get; set; }
        public string RepresentedName { get; internal set; }
        public string PartyTitle { get; internal set; }
        public bool IsThisCompany { get; set; }

        public override bool Equals(Object obj)
        {
            //Check for null and compare run-time types.
            if ((obj == null) || !this.GetType().Equals(obj.GetType()))
            {
                return false;
            }
            else
            {
                LawyerCompany p = (LawyerCompany)obj;
                return (CompanyName == p.CompanyName) && (Street == p.Street) && (RepresentedName == p.RepresentedName);
            }
        }
    }
}
