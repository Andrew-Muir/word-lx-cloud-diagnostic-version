﻿using InfowareVSTO.Common.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfowareVSTO.DataSource
{
    public class SelectedEntityModel : ViewModelBase
    {
        private bool used;
        public bool Used
        {
            get
            {
                return used;
            }
            set
            {
                used = value;
                OnPropertyRaised("Used");
            }
        }
        public int Key { get; set; }
        public string FullName { get; set; }
        public string Company { get; set; }
        public string RoleName { get; set; }
        public string RoleCode { get; set; }
        public bool CC { get; set; }
        public bool BCC { get; set; }
    }
}
