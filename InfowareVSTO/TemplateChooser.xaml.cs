﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using InfowareVSTO.Common;
using InfowareVSTO.Common.Language;
using InfowareVSTO.Common.Models;
using Newtonsoft.Json.Linq;

namespace InfowareVSTO
{
    /// <summary>
    /// Interaction logic for TemplateChooser.xaml
    /// </summary>
    public partial class TemplateChooser : InfowareWindow
    {
        public TemplateChooser()
        {
            InitializeComponent();
           
            this.Loaded += TemplateChooser_Loaded;
        }

        private void TemplateChooser_Loaded(object sender, RoutedEventArgs e)
        {
            this.Activate();
            this.Focus();
        }
     
        public TemplateChooser(TemplateType type) : this()
        {
            SetTitle(type);
        }

        public int ResultId { get; set; }

        public bool ResultOk { get; set; }

        private void SetTitle(TemplateType type)
        {
            string title = LanguageManager.GetTranslation(LanguageConstants.SelectTemplate, "Choose Template");

            switch (type)
            {
                case TemplateType.BlankDocument:
                    title = LanguageManager.GetTranslation(LanguageConstants.BlankDocument, "Blank Document");
                    break;
                case TemplateType.Envelope:
                    title = LanguageManager.GetTranslation(LanguageConstants.Envelope, "Envelope");
                    break;
                case TemplateType.Fax:
                    title = LanguageManager.GetTranslation(LanguageConstants.Fax, "Fax");
                    break;
                case TemplateType.Labels:
                    title = LanguageManager.GetTranslation(LanguageConstants.Labels, "Labels");
                    break;
                case TemplateType.Letter:
                    title = LanguageManager.GetTranslation(LanguageConstants.Letter, "Letter");
                    break;
                case TemplateType.Memo:
                    title = LanguageManager.GetTranslation(LanguageConstants.Memo, "Memo"); 
                    break;
                default:
                    break;
            }

            this.Title = title;
        }

        public void PopulateTemplates(List<TemplateListModel> templateList)
        {
            if(templateList != null && templateList.Count > 0)
            {
                foreach (var template in templateList)
                {
                    this.Templates.Items.Add(new ComboboxItem { Value = template.Id, Text = template.DisplayName });
                    this.HiddenListbox.Items.Add(new ComboboxItem { Value = template.Id, Text = template.DisplayName });
                }

                this.Templates.SelectedIndex = 0;
            }
        }

        private void OK_Click(object sender, RoutedEventArgs e)
        {
            this.ResultOk = true;
            this.ResultId = int.Parse(this.Templates.SelectedValue.ToString());
            this.Close();
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.ResultOk = false;
            this.Close();
        }
    }
}
