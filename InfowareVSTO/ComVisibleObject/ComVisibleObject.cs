﻿using InfowareVSTO.Windows.ParagraphNumbering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace InfowareVSTO.ComVisibleObject
{
    [ComVisible(true)]
    public interface IComVisibleObject
    {
        void AddDocumentID(bool skipSave);
        void AddDocumentIdOnOpen();
        void OpenNumberingDialog();
        void ChangeListLevelInSelection(int levelNumber, bool isContinuationParagraphStyle);
    }

    [ComVisible(true)]
    [ClassInterface(ClassInterfaceType.None)]
    public class ComVisibleObject : IComVisibleObject
    {
        public void AddDocumentID(bool skipSave)
        {
            Globals.ThisAddIn.IManageUtilities.AddDocumentID(skipSave);
        }

        public void AddDocumentIdOnOpen()
        {
            Globals.ThisAddIn.IManageUtilities.AddDocumentIdOnOpen();
        }

        public void OpenNumberingDialog()
        {
           Globals.ThisAddIn.Ribbon.ShowParagraphNumberingMainWindow();
        }

        public void ChangeListLevelInSelection(int levelNumber, bool isContinuationParagraphStyle)
        {
            ListTemplateUtil.ChangeListLevelInSelection(levelNumber, isContinuationParagraphStyle, Globals.ThisAddIn.currentSelectedListTemplateName);
        }
    }
}
