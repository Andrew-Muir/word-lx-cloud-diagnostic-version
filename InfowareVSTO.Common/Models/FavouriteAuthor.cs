﻿using InfowareVSTO.Common.Models.ViewModels;
using Microsoft.Office.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfowareVSTO.Common.Models
{
    public class FavouriteAuthor : ViewModelBase
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string JobTitle { get; set; }
        public string Company { get; set; }
        
        private int _aCompanyId;
        public int ACompanyId
        {
            get
            {
                return _aCompanyId;
            }
            set
            {
                _aCompanyId = value;
                OnPropertyRaised("ACompanyId");
            }
        }

        private int _officeLocationId;
        public int OfficeLocationId
        {
            get
            {
                return _officeLocationId;
            }
            set
            {
                _officeLocationId = value;
                OnPropertyRaised("OfficeLocationId");
            }
        }

        private UserAssignedRole _role;
        public UserAssignedRole Role
        {
            get
            {
                return _role;
            }
            set
            {
                _role = value;
                OnPropertyRaised("Role");
            }
        }

        private MsoLanguageID languageID;
        public MsoLanguageID LanguageID
        {
            get
            {
                return languageID;
            }
            set
            {
                languageID = value;
                OnPropertyRaised("LanguageID");
            }
        }

        public bool CurrentlyAdded { get; set; }
        public bool HasChanged { get; set; }

        public FavouriteAuthor()
        {
            Role = UserAssignedRole.Unnasigned;
            CurrentlyAdded = false;
            HasChanged = false;
        }
    }
}
