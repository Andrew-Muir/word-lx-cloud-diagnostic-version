﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfowareVSTO.Common.Models
{
    public class UserFullName
    {
		public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
