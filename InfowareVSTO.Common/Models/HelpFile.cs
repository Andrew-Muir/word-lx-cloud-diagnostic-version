﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfowareVSTO.Common.Models
{
    public class HelpFile
    {
        public string FileName { get; set; }
        public string Name { get; set; }
    }

    public class HelpFileList
    {
        public List<HelpFile> List { get; set; }
    }
}
