﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfowareVSTO.Common.Models
{
    public class Enclosure
    {
        public string SingularValue { get; set; }
        public string PluralValue { get; set; }
    }
}
