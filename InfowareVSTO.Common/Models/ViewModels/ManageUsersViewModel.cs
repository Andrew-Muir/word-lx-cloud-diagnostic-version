﻿using InfowareVSTO.Common.DTOs;
using InfowareVSTO.Common.Models.ViewModels;
using InfowareVSTO.Common.MultiLanguage;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace InfowareVSTO.Common.Models
{
    public class ManageUsersViewModel : ViewModelBase
    {
        private List<KeyValuePair<int, string>> _authors;
        public List<KeyValuePair<int, string>> Authors
        {
            get
            {
                return _authors;
            }
            set
            {
                _authors = value;
                OnPropertyRaised("Authors");
            }
        }

        private ObservableCollection<FavouriteAuthor> _favouriteAuthors;
        public ObservableCollection<FavouriteAuthor> FavouriteAuthors
        {
            get
            {
                return _favouriteAuthors;
            }
            set
            {
                _favouriteAuthors = value;
                OnPropertyRaised("FavouriteAuthors");
            }
        }

        private ObservableCollection<KeyValuePair<int, string>> _aCompanies;
        public ObservableCollection<KeyValuePair<int, string>> ACompanies
        {
            get
            {
                return _aCompanies;
            }
            set
            {
                _aCompanies = value;
                OnPropertyRaised("ACompanies");
            }
        }

        private ObservableCollection<KeyValuePair<int, string>> _officeLocations;
        public ObservableCollection<KeyValuePair<int, string>> OfficeLocations
        {
            get
            {
                return _officeLocations;
            }
            set
            {
                _officeLocations = value;
                OnPropertyRaised("OfficeLocations");
            }
        }

        private ObservableCollection<KeyValuePair<UserAssignedRole, string>> _assignedRoles;
        public ObservableCollection<KeyValuePair<UserAssignedRole, string>> AssignedRoles
        {
            get
            {
                return _assignedRoles;
            }
            set
            {
                _assignedRoles = value;
                OnPropertyRaised("AssignedRoles");
            }
        }

        private ObservableCollection<MLanguage> languages;
        public ObservableCollection<MLanguage> Languages
        {
            get
            {
                return languages;
            }
            set
            {
                languages = value;
                OnPropertyRaised("Languages");
            }
        }
    }
}
