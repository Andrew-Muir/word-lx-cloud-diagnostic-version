﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfowareVSTO.Common.Models.ViewModels
{
    public class DataFillViewModel : ViewModelBase
    {
        private ObservableCollection<SimpleDataField> _dataFields;
        public ObservableCollection<SimpleDataField> DataFields
        {
            get
            {
                return _dataFields;
            }
            set
            {
                _dataFields = value;
                OnPropertyRaised("DataFields");
            }
        }

        private ObservableCollection<ConditionalDataField> _questions;
        public ObservableCollection<ConditionalDataField> Questions
        {
            get
            {
                return _questions;
            }
            set
            {
                _questions = value;
                OnPropertyRaised("Questions");
            }
        }

        private ObservableCollection<PromptOrSpecialCharacterDataField> _promptAndSpecialCharacterFields;
        public ObservableCollection<PromptOrSpecialCharacterDataField> PromptAndSpecialCharacterFields
        {
            get
            {
                return _promptAndSpecialCharacterFields;
            }
            set
            {
                _promptAndSpecialCharacterFields = value;
                OnPropertyRaised("PromptAndSpecialCharacterFields");
            }
        }

        public DataFieldType DataFieldType { get; set; }

        public DataFillViewModel()
        {
            DataFields = new ObservableCollection<SimpleDataField>();
            Questions = new ObservableCollection<ConditionalDataField>();
            PromptAndSpecialCharacterFields = new ObservableCollection<PromptOrSpecialCharacterDataField>();
        }
    }
}
