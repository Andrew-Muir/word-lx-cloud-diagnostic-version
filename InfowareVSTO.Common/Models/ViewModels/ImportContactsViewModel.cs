﻿using InfowareVSTO.Common.Language;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace InfowareVSTO.Common.Models.ViewModels
{
    public class ImportContactsViewModel : ViewModelBase
    {
        private ObservableCollection<LocalContact> _localContactsCollection;
        public ObservableCollection<LocalContact> LocalContactsCollection
        {
            get
            {
                return _localContactsCollection;
            }
            set
            {
                _localContactsCollection = value;
                OnPropertyRaised("ContactsLocalView");
            }
        }

        private ObservableCollection<OutlookContact> _outlookContactsCollection;
        public ObservableCollection<OutlookContact> OutlookContactsCollection
        {
            get
            {
                return _outlookContactsCollection;
            }
            set
            {
                _outlookContactsCollection = value;
                OnPropertyRaised("ContactsOutlookView");
            }
        }

        public ICollectionView ContactsOutlookView
        {
            get { return CollectionViewSource.GetDefaultView(OutlookContactsCollection); }
        }

        public ICollectionView ContactsLocalView
        {
            get { return CollectionViewSource.GetDefaultView(LocalContactsCollection); }
        }

        private string searchOutlook;
        public string SearchOutlook
        {
            get { return searchOutlook; }
            set
            {
                searchOutlook = value;
                ContactsOutlookView.Filter = OutlookFilterExecute;
                ContactsOutlookView.Refresh();
            }
        }

        private string searchLocal;
        public string SearchLocal
        {
            get { return searchLocal; }
            set
            {
                searchLocal = value;
                ContactsLocalView.Filter = LocalFilterExecute;
                ContactsLocalView.Refresh();
            }
        }

        private ObservableCollection<SelectedContact> _selectedContactsView;
        public ObservableCollection<SelectedContact> SelectedContactsView
        {
            get
            {
                return _selectedContactsView;
            }
            set
            {
                _selectedContactsView = value;
                OnPropertyRaised("SelectedContactsView");
            }
        }

        public bool OutlookFilterExecute(object item)
        {
            return FilterExecute(item, true);
        }

        public bool LocalFilterExecute(object item)
        {
            return FilterExecute(item, false);
        }

        public bool FilterExecute(object item, bool fromOutlook)
        {
            if (item is IContact)
            {
                IContact author = item as IContact;
                string searchWords = fromOutlook ? searchOutlook : searchLocal;
                searchWords = searchWords.ToLower();
                string[] words = searchWords.Split(' ');

                foreach (string word in words)
                {
                    if ((author.FirstName?.ToLower().RemoveDiacritics().IndexOf(word) ?? -1) < 0 && (author.LastName?.ToLower().RemoveDiacritics().IndexOf(word) ?? -1) < 0 && (author.JobTitle?.ToLower().RemoveDiacritics().IndexOf(word) ?? -1) < 0
                        && (author.FirstName?.ToLower().IndexOf(word) ?? -1) < 0 && (author.LastName?.ToLower().IndexOf(word) ?? -1) < 0 && (author.JobTitle?.ToLower().IndexOf(word) ?? -1) < 0)
                    {
                        return false;
                    }
                }

                return true;
            }

            return false;
        }

        public List<KeyValuePair<string, OutlookContactType>> OutlookContactTypes { get; set; }

        private DataGridSelectionMode _selectionMode;
        public DataGridSelectionMode SelectionMode
        {
            get
            {
                return _selectionMode;
            }
            set
            {
                _selectionMode = value;
                OnPropertyRaised("SelectionMode");
            }
        }

        public ImportContactsViewModel()
        {
            LocalContactsCollection = new ObservableCollection<LocalContact>();
            OutlookContactsCollection = new ObservableCollection<OutlookContact>();
            SelectedContactsView = new ObservableCollection<SelectedContact>();

            OutlookContactTypes = new List<KeyValuePair<string, OutlookContactType>>()
            {
                new KeyValuePair<string, OutlookContactType>(LanguageManager.GetTranslation(LanguageConstants.Own, "Own"), OutlookContactType.Own),
                   new KeyValuePair<string, OutlookContactType>(LanguageManager.GetTranslation(LanguageConstants.Shared, "Shared"), OutlookContactType.Shared)
            };

            SelectionMode = DataGridSelectionMode.Extended;
        }
    }
}
