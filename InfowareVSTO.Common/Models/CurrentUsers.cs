﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfowareVSTO.Common.Models
{
    public class CurrentUsers
    {
        public int AuthorId { get; set; }
        public int AssistantId { get; set; }
        public int? AuthorOfficeLocationId { get; set; }
        public int? AssistantOfficeLocationId { get; set; }
    }
}
