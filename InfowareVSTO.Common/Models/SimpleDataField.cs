﻿using InfowareVSTO.Common.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfowareVSTO.Common.Models
{
    public class SimpleDataField : ViewModelBase
    {
        public string Name { get; set; }
        private string _value;
        public string Value
        {
            get
            {
                return _value;
            }
            set
            {
                _value = value;
                OnPropertyRaised("Value");
            }
        }
        public DataFieldType Type { get; set; }

        public SimpleDataField(string name, string value, DataFieldType type = DataFieldType.Text)
        {
            Name = name;
            Value = value;
            Type = type;
        }
    }
}
