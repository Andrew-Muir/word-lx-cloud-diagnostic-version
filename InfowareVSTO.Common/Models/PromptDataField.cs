﻿using InfowareVSTO.Common.Models.ViewModels;
using Microsoft.Office.Interop.Word;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfowareVSTO.Common.Models
{
    public class PromptOrSpecialCharacterDataField : ViewModelBase
    {
        public string Name { get; set; }
        public bool MultiLine { get; set; }

        private string _value;
        public string Value
        {
            get
            {
                return _value;
            }
            set
            {
                _value = value;
                OnPropertyRaised("Value");
            }
        }

        public PromptOrSpecialCharacterDataField(string name, bool multiLine = false)
        {
            Name = name;
            Value = string.Empty;
            MultiLine = multiLine;
        }
    }
}
