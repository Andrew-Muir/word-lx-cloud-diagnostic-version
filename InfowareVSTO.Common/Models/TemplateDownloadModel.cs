﻿using InfowareVSTO.Common.DTOs;
using Microsoft.Office.Interop.Word;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfowareVSTO.Common.Models
{
    public class TemplateDownloadModel
    {
        public Document Document { get; set; }
        public PaneDTO PaneDTO { get; set; }
        public string TemplatePath { get; set; }
    }
}
