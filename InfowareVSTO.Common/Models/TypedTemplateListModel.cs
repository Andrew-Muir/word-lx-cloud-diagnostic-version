﻿using InfowareVSTO.Common.Language;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfowareVSTO.Common.Models
{
    public class TypedTemplateListModel : TemplateListModel
    {
        public TemplateType Type { get; set; }

        public override string ToString()
        {
            string prefix = string.Empty;
            switch (Type)
            {
                case TemplateType.BlankDocument:
                    prefix = LanguageManager.GetTranslation(LanguageConstants.BlankDocument, "Blank Document");
                    break;
                case TemplateType.Letter:
                    prefix = LanguageManager.GetTranslation(LanguageConstants.Letter, "Letter");
                    break;
                case TemplateType.Fax:
                    prefix = LanguageManager.GetTranslation(LanguageConstants.Fax, "Fax");
                    break;
                case TemplateType.Memo:
                    prefix = LanguageManager.GetTranslation(LanguageConstants.Memo, "Memo");
                    break;
                case TemplateType.Envelope:
                    prefix = LanguageManager.GetTranslation(LanguageConstants.Envelope, "Envelope");
                    break;
                case TemplateType.Labels:
                    prefix = LanguageManager.GetTranslation(LanguageConstants.Labels, "Labels");
                    break;
                case TemplateType.Custom:
                    prefix = LanguageManager.GetTranslation(LanguageConstants.Other, "Other");
                    break;
                default:
                    return DisplayName;
            }

            return prefix + ": " + DisplayName;
        }
    }
}
