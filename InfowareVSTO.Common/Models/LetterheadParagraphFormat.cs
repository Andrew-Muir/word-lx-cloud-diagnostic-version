﻿using Microsoft.Office.Interop.Word;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfowareVSTO.Common.Models
{
    public class LetterheadParagraphFormat
    {
        public float LeftIndent { get; set; }
        public float RightIndent { get; set; }
        public float SpaceBefore { get; set; }
        public float SpaceAfter { get; set; }
        public WdParagraphAlignment Aligment { get; set; }
        public float FirstLineIndent { get; set; }
        public TabStops TabStops { get; set; }
    }
}
