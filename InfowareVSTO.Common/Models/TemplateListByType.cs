﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfowareVSTO.Common.Models
{
    public class TemplateListByType
    {
        public TemplateListModel Template { get; set; }
        public bool ResultOk { get; set; }

        public TemplateListByType()
        {
            ResultOk = true;
        }
    }
}
