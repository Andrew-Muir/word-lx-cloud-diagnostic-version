﻿using InfowareVSTO.Common.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfowareVSTO.Common.Models
{
    public class SpecialCharacterField : ViewModelBase
    {
        public string Name { get; set; }

        private string _value;
        public string Value
        {
            get
            {
                return _value;
            }
            set
            {
                _value = value;
                OnPropertyRaised("Value");
            }
        }

        public SpecialCharacterField(string name)
        {
            Name = name;
            Value = string.Empty;
        }
    }
}
