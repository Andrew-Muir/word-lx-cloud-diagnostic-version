﻿using Microsoft.Office.Interop.Word;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfowareVSTO.Common.Models
{
    public class AddendaOptionConfiguration
    {
        public int ConfigurationId { get; set; }
        public string ParagraphNumberingLevel { get; set; }
        
        public AddendaOptionConfiguration()
        {
            ConfigurationId = -1;
        }
    }
}
