﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfowareVSTO.Common.Models
{
    public class ConditionalDataField
    {
        public string Name { get; set; }
        public bool Value { get; set; }
        public string Question { get; set; }
        public string Answer1 { get; set; }
        public string Answer2 { get; set; }
        public List<string> AdditionalAnswers { get; set; }

        public List<KeyValuePair<int, string>> AdditionalAnswersWithNr
        {
            get
            {
                List<KeyValuePair<int, string>> result = new List<KeyValuePair<int, string>>();
                if (AdditionalAnswers != null)
                {
                    for (int i = 0; i < AdditionalAnswers.Count; i++)
                    {
                        result.Add(new KeyValuePair<int, string>(i + 3, AdditionalAnswers[i]));
                    }
                }

                return result;
            }
        }

        public ConditionalDataField(string name, string question, string answer1, string answer2,List<string> additionalAnswers, bool value = false)
        {
            Name = name;
            Question = question;
            Answer1 = answer1;
            Answer2 = answer2;
            Value = value;
            AdditionalAnswers = additionalAnswers;
        }
    }
}
