﻿using Microsoft.Office.Interop.Word;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfowareVSTO.Common.Models
{
    public class SelectedParagraphModel
    {
        public string LevelNumber { get; set; }
        public WdListType Type { get; set; }
        public Range Range { get; set; }
    }
}
