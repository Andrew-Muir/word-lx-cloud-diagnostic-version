﻿namespace InfowareVSTO.Common.Models
{
    public interface IContact
    {
        string FirstName { get; set; }
        string LastName { get; set; }
        string JobTitle { get; set; }
    }
}