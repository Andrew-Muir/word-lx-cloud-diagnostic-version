﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfowareVSTO.Common.Models
{
    public class ExtendedTemplateListModel : TypedTemplateListModel
    {
        public string Category { get; set; }

        public string Description { get; set; }
    }
}
