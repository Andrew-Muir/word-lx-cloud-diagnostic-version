﻿using InfowareVSTO.Common.DTOs;
using Microsoft.Office.Interop.Outlook;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfowareVSTO.Common.Models
{
    public class OutlookContact : IContact
    {
        public int Id { get; set; }
		public string Prefix { get; set; }
        public string Suffix { get; set; }
        public string CompanyName { get; set; }
        public string FullName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string JobTitle { get; set; }
        public string Department { get; set; }
        public List<AuthorAddressDTO> Addresses { get; set; }
        public List<AContactInformationDTO> ContactInformations { get; set; }
        public List<ACompanyDTO> Companies { get; set; }
        public ContactItem ContactItem { get; set; }
        public List<AuthorAddressDTO> AlternateAddresses { get; set; }
    }
}
