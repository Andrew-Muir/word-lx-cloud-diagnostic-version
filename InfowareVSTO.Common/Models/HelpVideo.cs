﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfowareVSTO.Common.Models
{
    public class HelpVideo
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public string Folder { get; set; }
    }

    public class HelpVideoCategory
    {
        public string Name { get; set; }

        public List<HelpVideo> Videos { get; set; }
    }

    public class HelpVideoList
    {
        public List<HelpVideoCategory> Categories;
    }

}
