﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace InfowareVSTO.Common.Models
{
    public class TemplateListModel
    {
        public int Id { get; set; }
        private string name;
        public string DisplayName { get; set; }
        public string Extension { get; set; }
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
                try
                {
                    DisplayName = Path.GetFileNameWithoutExtension(value);
                    Extension = Path.GetExtension(value);
                }
                catch
                {
                    DisplayName = value;
                    Extension = string.Empty;
                }
            }
        }
        public int FormCollectionId { get; set; }
        public string FormCollectioName { get; set; }
    }
}
