﻿using InfowareVSTO.Common.DTOs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace InfowareVSTO.Common.Models
{
    public class SelectedContact : INotifyPropertyChanged
    {
        public int Id { get; set; }
        public string CompanyName { get; set; }
        public string FullName { get; set; }
        public string Prefix { get; set; }
        public string Suffix { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string JobTitle { get; set; }
        public string Department { get; set; }
        public List<AuthorAddressDTO> Addresses { get; set; }
        public List<ACompanyDTO> Companies { get; set; }
        public List<AContactInformationDTO> ContactInformations { get; set; }
        private string displayName;
        public string DispayName 
        {
            get
            {
                return displayName ?? (FirstName + " " + LastName);
            }
            set
            {
                displayName = value;
            }
        }

        private bool isSelected { get; set; }
        public bool IsSelected
        {
            get => isSelected;

            set
            {
                isSelected = value;

                OnPropertyChanged();
            }
        }

        public string UniqueId { get; set; }

        public SelectedContact()
        {
            Addresses = new List<AuthorAddressDTO>();
            Companies = new List<ACompanyDTO>();
            ContactInformations = new List<AContactInformationDTO>();
            UniqueId = Guid.NewGuid().ToString();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public override string ToString()
        {
            var selectedContact = new List<string>();
            var fullName = new List<string>();
            if (!string.IsNullOrEmpty(Prefix))
            {
                fullName.Add(Prefix);
            }
            if (!string.IsNullOrEmpty(FirstName))
            {
                fullName.Add(FirstName);
            }
            if (!string.IsNullOrEmpty(LastName))
            {
                fullName.Add(LastName);
            }
            if (!string.IsNullOrEmpty(Suffix))
            {
                fullName.Add(Suffix);
            }

            if (fullName.Count > 0)
            {
                selectedContact.Add(string.Join(" ", fullName));
            }
            if (string.IsNullOrEmpty(JobTitle))
            {
                selectedContact.Add(JobTitle);
            }

            var address = Addresses.OrderBy(x => x.IsDefault).FirstOrDefault();
            if (address != null && !string.IsNullOrWhiteSpace(address.Address1))
                selectedContact.Add(string.Join("\r\n", address.LinesFormat));

            return string.Join("\r\n", selectedContact);
        }

        protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
