﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfowareVSTO.Common.DTOs
{
	public class Field
	{
		public string SubGroup { get; set; }
		public List<string> Fields { get; set; }
	}

	public class Group
	{
		public string Name { get; set; }
		public List<Field> Fields { get; set; }
	}

	public class StandardFieldsDTO
	{
		public List<Group> Groups { get; set; }
	}

}
