﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InfowareVSTO.Common.Models.ViewModels;

namespace InfowareVSTO.Common.DTOs
{
    public class InformOptions
    {
        public bool CompanyNameAllCaps { get; set; }
        public bool CompanyNameBoldface { get; set; }
        public bool LawyerNameBoldface { get; set; }
        public bool UseCompanyPhoneAndFax { get; set; }
        public bool UseOnlyCompanyFax { get; set; }
        public bool PhoneAndFaxNumberOnOneLine { get; set; }
        public bool IncludeEmailAddress { get; set; }
        public bool IncludeBlankLinesSameFirmLawyer { get; set; }
        public bool IncludeBlankLineCompanyAddressLawyer { get; set; }
        public bool IncludeBlankLineLawyerPhoneNumber { get; set; }
        public bool CompactSpacingLSOC { get; set; }
        public bool AlwaysUseRealDouble { get; set; }
        public bool AlwaysSetView100 { get; set; }
        public BackPageOption BackPageOption { get; set; }
        public bool UseShortStyleOfCause { get; set; } 
    }

    public enum BackPageOption
    {
        Append = 0,
        SeparateDoc = 1,
        Ask = 2,
        NeverInsert = 3
    }
}
