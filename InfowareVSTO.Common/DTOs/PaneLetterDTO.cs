﻿using InfowareVSTO.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;

namespace InfowareVSTO.Common.DTOs
{
    public class PaneDTO
    {
        public TemplateType Type { get; set; }
        public AuthorWithPathsDTO Author { get; set; }
        public AuthorWithPathsDTO Assistant { get; set; }
        public SelectedContact[] Tos { get; set; }
        public DateTime Date { get; set; }
        public string Recipients { get; set; }
        public string[] RecipientsArr { get; set; }
        public string Initials { get; set; }
        public string FaxNumber { get; set; }
        public string PhoneNumber { get; set; }
        public string Salutation { get; set; }
        public string CC { get; set; }
        public string BCC { get; set; }
        public string FileNumber { get; set; }
        public string ClientFileNumber { get; set; }
        public string Delivery { get; set; }
        public string Handling { get; set; }
        public string Attention { get; set; }
        public string OfficeLocation { get; set; }
        public int? OfficeLocationId { get; set; }
        public string ReLine { get; set; }
        public string Closing { get; set; }
        public string Enclosures { get; set; }
        public int Number { get; set; }
        public bool IncludeFirmName { get; set; }
        public bool IncludePer { get; set; }
        public string NoOfPages { get; set; }
        public int StartAtLabelNumber { get; set; }
        public bool UseFirstRecipient { get; set; }
        public string IncludeContactInformationDeliverySetting { get; set; }
        public List<SelectedContact> AddedRecipients { get; set; }
        public string FileNumberTagSetting { get; set; }
        public bool HandlingAllCaps { get; set; }
        public bool DeliveryAllCaps { get; set; }
        public string Firm { get; set; }
        public int? CompanyId { get; set; }
        public string ClosingPerText { get; set; }
        public string ClosingEndChar { get; set; }
        public string DateFormat { get; set; }
        public string CultureId { get; set; }
        public DateTime? DiaryDate { get; set; }
        public string AttentionTranslation { get; set; }
        public string LawSocietyNumberLabel { get; set; }
        public InformOptions InformOptions { get; set; }
        public bool? IncludeAssistantBlock { get; set; }

        public PaneDTO()
        {
            Date = DateTime.Now;
            AddedRecipients = new List<SelectedContact>();
            StartAtLabelNumber = 1;
        }
    }
}
