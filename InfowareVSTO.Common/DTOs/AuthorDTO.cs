﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using DocumentFormat.OpenXml.ExtendedProperties;
using Microsoft.Office.Interop.Outlook;

namespace InfowareVSTO.Common.DTOs
{
	public class AuthorDTO
	{
		public int Id { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string JobTitle { get; set; }
		public bool IsPublic { get; set; }
		public bool IsEditable { get; set; }
		public bool IsFavorite { get; set; }

		//custom
		public bool IsFromOutlook { get; set; }
		public string Company { get; set; }
		public string Phone { get; set; }
		public string Fax { get; set; }
	}

	public class AuthorWithPathsDTO
	{
		public int Id { get; set; }
		public string CompanyName { get; set; }
		public string FirstName { get; set; }
		public string MiddleName { get; set; }
		public string LastName { get; set; }
		public string JobTitle { get; set; }
		public bool IsPublic { get; set; }
		public bool IsEditable { get; set; }
		public string Prefix { get; set; }
		public string Suffix { get; set; }
		public string Credentials { get; set; }
		public string LawyerNumber { get; set; }
		public string Certification { get; set; }
		public string EmployeeId { get; set; }
		public string DisplayName { get; set; }
		public string Initials { get; set; }
		public string DefaultClosing { get; set; }
		public string User1 { get; set; }
		public string User2 { get; set; }
		public string User3 { get; set; }
		public string Department { get; set; }

		public List<ACompanyDTO> Companies { get; set; }
		public List<AuthorAddressDTO> Addresses { get; set; }
		public List<AuthorAddressDTO> AlternateAddresses { get; set; }
		public List<AContactInformationDTO> ContactInformations { get; set; }
		public ContactItem ContactItem { get; set; }

		public AuthorDTO ToAuthorDTO()
		{
			AuthorDTO result = new AuthorDTO() { FirstName = FirstName, LastName = LastName, JobTitle = JobTitle, Id = Id, IsEditable = IsEditable };
			return result;
		}
	}

	/// <summary>
	/// AAddress for Authors. Is used for AAdress for ACompanies
	/// </summary>
	public class AuthorAddressDTO
	{
		public int Id { get; set; }
		public string Address1 { get; set; }
		public string Address2 { get; set; }
		public string Name { get; set; }
		public string City { get; set; }
		public string Province { get; set; }
		public string PostalCode { get; set; }
		public string Country { get; set; }
		public string Phone { get; set; }
		public string Fax { get; set; }
		public bool IsDefault { get; set; }

		static readonly string[] countriesWithPostalCodeOnSeparateLine = { "Brazil", "Hungary", "Iraq", "Ireland", "Latvia", "Pakistan", "Russia", "Sri Lanka", "Thailand", "Ukraine" };

		public string[] LinesFormat
		{
			get
			{
				var fullAddress = new List<string>();
				if (!string.IsNullOrWhiteSpace(Address1))
				{
					fullAddress.Add(Address1);
					if (string.IsNullOrWhiteSpace(City) || Address1.IndexOf(City) < 0 || Address1.IndexOf('\n') < 0)
					{
						var location = new List<string>();

						CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
						RegionInfo regionInfo = new RegionInfo(cultureInfo.LCID);
						string currentCountry = regionInfo.EnglishName;
						string countryToCheck;

						string postalCodeRow = null;

						// city
						if (!string.IsNullOrWhiteSpace(City))
						{
							location.Add(City);
						}

						// province
						if (!string.IsNullOrWhiteSpace(Province))
						{
							location.Add(Province);
						}

						// postal code
						if (!string.IsNullOrWhiteSpace(PostalCode))
						{
							if (!string.IsNullOrWhiteSpace(Country))
							{
								// country found in contact, use it
								countryToCheck = Country;
							}
							else
							{
								// no country found in contact, assume recipient is in the same country
								countryToCheck = currentCountry;
							}

							// is country one that uses separate line for postal code?
							bool countryUsesSeparateLineForPostalCode = false;
							for (int i = 0; i < countriesWithPostalCodeOnSeparateLine.Length; i++)
							{
								if (countryToCheck.Contains(countriesWithPostalCodeOnSeparateLine[i]))
								{
									countryUsesSeparateLineForPostalCode = true;
									break;
								}
							}

							if (countryUsesSeparateLineForPostalCode)
							{
								postalCodeRow = PostalCode;
							}
							else
							{
								location.Add(PostalCode);
							}
						}

						var fullLocation = string.Join(" ", location.ToArray());
						if (!string.IsNullOrWhiteSpace(fullLocation))
						{
							fullAddress.Add(fullLocation);
						}

						if (!string.IsNullOrWhiteSpace(postalCodeRow))
						{
							fullAddress.Add(postalCodeRow);
						}

						// if the recipient's country is different from "our" country, add it to the address
						// if no country is in the Outlook contact, assume they are in the same country
						if (!string.IsNullOrWhiteSpace(Country) && (Country != currentCountry))
						{
							fullAddress.Add(Country);
						}
					}

				}
				return fullAddress.ToArray();
			}
		}
	}

	/// <summary>
	/// AContactInformation for Authors. Is used for AContactInformation for ACompanies
	/// </summary>
	public class AContactInformationDTO
	{
		public int Id { get; set; }
		public string Type { get; set; }
		public string Details { get; set; }
		public bool IsDefault { get; set; }
	}

	public class AccessTokenDTO
	{
		public string access_token { get; set; }
	}
}
