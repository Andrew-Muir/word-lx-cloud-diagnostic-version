﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfowareVSTO.Common.DTOs
{
	//This is the ACompany
	public class ACompanyDTO
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public string Phone { get; set; }
		public string Email { get; set; }
		//public string Website { get; set; }
		//public string Description { get; set; }
		public bool IsDefault { get; set; }		
	}

	public class ACompanyWithPathsDTO
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public string Website { get; set; }
		public string Description { get; set; }
		public bool IsDefault { get; set; }
		public List<AuthorAddressDTO> Addresses { get; set; }
		public List<AContactInformationDTO> ContactInformations { get; set; }
		public List<ACompanyAContactInformationDTO> Contacts { get; set; }
	}

	/// <summary>
	/// Company's Authors are listed here - AuthorACompany records
	/// This only reads from database, it does not update it
	/// </summary>
	public class ACompanyAContactInformationDTO
	{
		public int Id { get; set; }//Author.Id
		public string FirstName { get; set; }//Author.FirstName
		public string LastName { get; set; }//Author.LastName	
		public string Phone { get; set; }//Author.AContactInformation.Where(x => x.Type == 'Phone').OrderByDesc(x => x.IsDefault).FirstOrDefault()
		public string Email { get; set; }//Author.AContactInformation.Where(x => x.Type == 'Email').OrderByDesc(x => x.IsDefault).FirstOrDefault()		
		public bool IsDefault { get; set; }
	}

	public class ACompanySimpleDTO
	{
		public int Id { get; set; }
		public string CompanyName { get; set; }
		public string Description { get; set; }
		public int? DefaultLocationId { get; set; }
		public string Address { get; set; }
		public string Address2 { get; set; }		
		public string City { get; set; }
		public string Province { get; set; }
		public string PostalCode { get; set; }
		public string Country { get; set; }		
		public string CompanyPhone { get; set; }
		public string CompanyFax { get; set; }
		public string CompanyWebsite { get; set; }
	}
}
