﻿using InfowareVSTO.Common.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfowareVSTO.Common.Models
{
    public class FavouriteAuthorDTO
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string JobTitle { get; set; }
        public string Company { get; set; }
        public int? CurrentACompanyId { get; set; }
        public int? OfficeLocationId { get; set; }
        public int? LanguageId { get; set; }
        public bool IsPublic { get; set; }
        public bool IsEditable { get; set; }
        public bool IsFavorite { get; set; }
    }
}
