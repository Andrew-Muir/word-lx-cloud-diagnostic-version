﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfowareVSTO.Common.DTOs
{
    public class DataSourceMappingDTO
    {
        public string MatterListSql { get; set; }
        public string MatterNameColumn { get; set; }
        public string MatterIdColumn { get; set; }
        public string MatterDescriptionColumn { get; set; }
        public string MatterClientNameColumn { get; set; }
        public string MatterCodeColumn { get; set; }

        public List<DataSourceMappingGroupDTO> Groups { get; set; }
    }

    public class DataSourceMappingGroupDTO
    {
        public string Type { get; set; }
        public string Sql { get; set; }

        public Dictionary<string, string> FieldColumns { get; set; }
    }
}
