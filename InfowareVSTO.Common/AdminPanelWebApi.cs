﻿using InfowareVSTO.Common;
using InfowareVSTO.Common.DTO;
using InfowareVSTO.Common.DTOs;
using InfowareVSTO.Common.Models;
using InfowareVSTO.Common.MultiLanguage;
using InfowareVSTO.Common.OfflineMode;
using Microsoft.Office.Core;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace InfowareVSTO
{
    public class AdminPanelWebApi
    {
        //public static string WebApiUrl = "https://infoware-stg.vmtinternal.net:4432/";
        //public static string WebApiUrl = "https://wordlx.infowaregroup.net:4439/"; // Master
        //private static string WebApiUrl = "http://localhost:61770/";

        public static readonly HttpClient client = new HttpClient();

        private static List<WSParameter> AddLanguageIdParameter(List<WSParameter> list, MsoLanguageID languageID)
        {
            if (OfflineModeManager.IsOffline == false && list != null && languageID != MsoLanguageID.msoLanguageIDNone)
            {
                list.Add(new WSParameter("languageId", ((int)languageID).ToString()));
            }

            return list;
        }

        public static ApiResponseDTO GetUserInfo(string authKey)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(ConfigSettings.WebApiUrl);

            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));

            // List data response.
            HttpResponseMessage response = client.GetAsync(string.Format("api/WebApi/GetUserInfo?authKey={0}", authKey)).Result;  // Blocking call! Program will wait here until a response is received or a timeout occurs.
            ApiResponseDTO apiResponse;
            if (response.IsSuccessStatusCode)
            {
                // Parse the response body.
                apiResponse = response.Content.ReadAsAsync<ApiResponseDTO>().Result;  //Make sure to add a reference to System.Net.Http.Formatting.dll                
            }
            else
            {
                apiResponse = new ApiResponseDTO
                {
                    StatusCode = (int)response.StatusCode,
                    Message = response.ReasonPhrase
                };
            }

            //Make any other calls using HttpClient here.

            //Dispose once all HttpClient calls are complete. This is not necessary if the containing object will be disposed of; for example in this case the HttpClient instance will be disposed automatically when the application terminates so the following call is superfluous.
            client.Dispose();
            return apiResponse;
        }

        public static DataSourceMappingDTO GetDataSourceFieldMappings()
        {
            object authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
            if (authKey is string)
            {
                WebserviceCall wsCall = new WebserviceCall("GetCompanyDataSourceFieldMappings", new List<WSParameter>() { new WSParameter("authKey", authKey.ToString()) });
                wsCall.Execute();

                JObject content;
                if (wsCall?.Response?.Content != null && wsCall.Response.Content.ToString() != "null")
                {
                    content = JsonConvert.DeserializeObject<JObject>(wsCall.Response.Content.ToString());
                    if (content != null)
                    {
                        int statusCode = content["StatusCode"]?.ToObject<int>() ?? -1;

                        if (statusCode == 200)
                        {
                            return content["Data"].ToObject<DataSourceMappingDTO>();
                        }
                    }
                }
            }

            return null;
        }

        public static List<MLanguage> GetMLanguages()
        {
            object authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
            if (authKey is string)
            {
                WebserviceCall wsCall = new WebserviceCall("GetMLanguages", new List<WSParameter>() { new WSParameter("authKey", authKey.ToString()) });
                wsCall.Execute();

                if (wsCall.Response != null)
                {
                    return JsonConvert.DeserializeObject<List<MLanguage>>(wsCall.Response.Content as string);
                }
            }

            return null;
        }

        public static List<TypedTemplateListModel> GetAllTemplates(int? languageId, int? locationId)
        {
            object authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
            if (authKey is string)
            {
                var parameters = new List<WSParameter>() { new WSParameter("authKey", authKey.ToString()) };

                var cachedParameters = parameters.ToList();

                if (locationId.HasValue)
                {
                    parameters.Add(new WSParameter("locationId", locationId.Value.ToString()));
                }

                if (languageId.HasValue)
                {
                    parameters.Add(new WSParameter("languageId", languageId.Value.ToString()));
                }

                WebserviceCall wsCall = new WebserviceCall("GetAllTemplates", parameters);
                wsCall.CacheParameters = cachedParameters;
                wsCall.Execute();

                if (wsCall.Response != null)
                {
                    return JsonConvert.DeserializeObject<List<TypedTemplateListModel>>(wsCall.Response.Content as string);
                }
            }

            return null;
        }

        public static List<KeyValuePair<int,string>> GetCompanyLanguages()
        {
            object authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
            if (authKey is string)
            {
                WebserviceCall wsCall = new WebserviceCall("GetCompanyLanguages", new List<WSParameter>() { new WSParameter("authKey", authKey.ToString()) });
                wsCall.Execute();

                if (wsCall.Response != null)
                {
                    try
                    {
                        return JsonConvert.DeserializeObject<List<KeyValuePair<int, string>>>(wsCall.Response.Content as string);
                    }
                    catch(Exception ex) 
                    {
                    }
                }
            }

            return null;
        }

        public static int GetBackPageId(int formId)
        {
            object authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
            if (authKey is string)
            {
                WebserviceCall wsCall = new WebserviceCall("GetFormBackPageId", new List<WSParameter>() 
                { 
                    new WSParameter("authKey", authKey.ToString()),
                    new WSParameter("formId", formId.ToString())
                });
                wsCall.Execute();

                ApiResponseDTO content;
                if (wsCall?.Response?.Content != null && wsCall.Response.Content.ToString() != "null")
                {
                    content = JsonConvert.DeserializeObject<ApiResponseDTO>(wsCall.Response.Content.ToString());
                    if (content != null)
                    {
                        int statusCode = content.StatusCode;

                        if (statusCode == 200)
                        {
                            if (content.Data is int || content.Data is long)
                            {
                                return (int)(long)content.Data;
                            }
                        }
                    }
                }
            }

            return 0;
        }

        public static void AddTemplateToFavorite(int id, bool add)
        {
            object authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
            if (authKey is string)
            {
                WebserviceCall wsCall = new WebserviceCall("GetSetFavoriteTemplate", new List<WSParameter>() {
                    new WSParameter("authKey", authKey.ToString()),
                    new WSParameter("templateId", id.ToString()),
                    new WSParameter("add", add.ToString())
                });
                wsCall.Execute();
            }
        }

        public static List<ExtendedTemplateListModel> GetFavoriteTemplates()
        {
            object authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
            if (authKey is string)
            {
                WebserviceCall wsCall = new WebserviceCall("GetFavoriteTemplates", new List<WSParameter>() { new WSParameter("authKey", authKey.ToString()) });
                wsCall.Execute();

                if (wsCall.Response != null)
                {
                    return JsonConvert.DeserializeObject<List<ExtendedTemplateListModel>>(wsCall.Response.Content as string);
                }
            }

            return null;
        }

        public static ApiResponseDTO PushMultiPrintSettingsToCompany(string mPSettings, int officeLocation, bool overwrite)
        {
            object authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
            if (authKey is string)
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(ConfigSettings.WebApiUrl);

                // Add an Accept header for JSON format.
                client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));

                //HttpResponseMessage response = await client.PostAsJsonAsync(
                //             "api/products", product);
                //         response.EnsureSuccessStatusCode();

                var buffer = System.Text.Encoding.UTF8.GetBytes(mPSettings);
                var byteContent = new ByteArrayContent(buffer);
                byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                HttpResponseMessage response = client.PostAsync(string.Format("api/WebApi/PushMultiPrintSettingToCompany?authKey={0}&officeLocation={1}&overwrite={2}", authKey, officeLocation, overwrite),
                    byteContent).Result;  // Blocking call! Program will wait here until a response is received or a timeout occurs.
                ApiResponseDTO apiResponse;
                if (response.IsSuccessStatusCode)
                {
                    // Parse the response body.
                    apiResponse = response.Content.ReadAsAsync<ApiResponseDTO>().Result;  //Make sure to add a reference to System.Net.Http.Formatting.dll                
                }
                else
                {
                    apiResponse = new ApiResponseDTO
                    {
                        StatusCode = (int)response.StatusCode,
                        Message = response.ReasonPhrase
                    };
                }

                //Make any other calls using HttpClient here.

                //Dispose once all HttpClient calls are complete. This is not necessary if the containing object will be disposed of; for example in this case the HttpClient instance will be disposed automatically when the application terminates so the following call is superfluous.
                client.Dispose();

                return apiResponse;
            }

            return null;
        }

        public static void PreCacheSimpleWebMethod(string webMethod, bool useByteArr = false, bool useApiResponse = false, bool noAuth = false)
        {
            object authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
            if (authKey is string && webMethod != null)
            {
                WebserviceCall wsCall = new WebserviceCall(webMethod, new List<WSParameter>() { new WSParameter("authKey", authKey.ToString()) });

                if (noAuth)
                {
                    wsCall.Parameters = new List<WSParameter>();
                }

                wsCall.Execute(useByteArr, useApiResponse);
            }
        }

        public static string PreCacheWebMethod(string webMethod, DateTime? lastUpdate)
        {
            webMethod = webMethod + "PreCache";
            object authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
            if (authKey is string && webMethod != null)
            {
                RestClient client = new RestClient(ConfigSettings.WebApiUrl);
                RestRequest request;
                IRestResponse response;
                string parameterString = "?authKey=" + authKey;
                if (lastUpdate.HasValue)
                {
                    parameterString += "&lastPreCache=" + lastUpdate.Value.Ticks.ToString();
                }

                request = new RestRequest("api/WebApi/" + webMethod + parameterString, Method.GET);

                response = client.Execute(request);

                if (response.StatusCode == HttpStatusCode.OK || response.StatusCode == HttpStatusCode.Accepted)
                {
                    return response.Content;
                }
            }

            return null;
        }

        public static string GetCustomSigningLine(MsoLanguageID languageID = MsoLanguageID.msoLanguageIDNone)
        {
            object authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
            if (authKey is string)
            {
                WebserviceCall wsCall = new WebserviceCall("DownloadSigningLineTemplateAsXml", AddLanguageIdParameter(new List<WSParameter>() { new WSParameter("authKey", authKey.ToString()) }, languageID));
                wsCall.Execute();

                if (wsCall.Response != null)
                {
                    JObject content = JsonConvert.DeserializeObject<JObject>(wsCall.Response.Content as string);

                    int statusCode = content["StatusCode"].ToObject<int>();

                    if (statusCode == 200)
                    {
                        return content["Data"].ToString();
                    }
                }
            }

            return null;
        }

        public static string GetCustomTopDraftDateStamp(MsoLanguageID languageID = MsoLanguageID.msoLanguageIDNone)
        {
            object authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
            if (authKey is string)
            {
                WebserviceCall wsCall = new WebserviceCall("DownloadTopDraftDateStampAsXml", AddLanguageIdParameter(new List<WSParameter>() { new WSParameter("authKey", authKey.ToString()) }, languageID));
                wsCall.Execute();

                if (wsCall.Response != null)
                {
                    JObject content = JsonConvert.DeserializeObject<JObject>(wsCall.Response.Content as string);

                    int statusCode = content["StatusCode"].ToObject<int>();

                    if (statusCode == 200)
                    {
                        return content["Data"].ToString();
                    }
                }
            }

            return null;
        }
        public static string GetCustomLeftDraftDateStamp(MsoLanguageID languageID = MsoLanguageID.msoLanguageIDNone)
        {
            object authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
            if (authKey is string)
            {
                WebserviceCall wsCall = new WebserviceCall("DownloadLeftDraftDateStampAsXml", AddLanguageIdParameter(new List<WSParameter>() { new WSParameter("authKey", authKey.ToString()) }, languageID));
                wsCall.Execute();

                if (wsCall.Response != null)
                {
                    JObject content = JsonConvert.DeserializeObject<JObject>(wsCall.Response.Content as string);

                    int statusCode = content["StatusCode"].ToObject<int>();

                    if (statusCode == 200)
                    {
                        return content["Data"].ToString();
                    }
                }
            }

            return null;
        }
        public static string GetCustomRightDraftDateStamp(MsoLanguageID languageID = MsoLanguageID.msoLanguageIDNone)
        {
            object authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
            if (authKey is string)
            {
                WebserviceCall wsCall = new WebserviceCall("DownloadRightDraftDateStampAsXml", AddLanguageIdParameter(new List<WSParameter>() { new WSParameter("authKey", authKey.ToString()) }, languageID));
                wsCall.Execute();

                if (wsCall.Response != null)
                {
                    JObject content = JsonConvert.DeserializeObject<JObject>(wsCall.Response.Content as string);

                    int statusCode = content["StatusCode"].ToObject<int>();

                    if (statusCode == 200)
                    {
                        return content["Data"].ToString();
                    }
                }
            }

            return null;
        }

        // precache
        public static ApiResponseDTO GetCustomSetting(string browserId, string settingName)
        {
            object authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
             if (authKey is string)
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(ConfigSettings.WebApiUrl);

                // Add an Accept header for JSON format.
                client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));

                // List data response.
                HttpResponseMessage response = client.GetAsync(string.Format("api/WebApi/GetCustomSetting?authKey={0}&browserId={1}&settingName={2}", authKey, browserId, settingName)).Result;  // Blocking call! Program will wait here until a response is received or a timeout occurs.
                ApiResponseDTO apiResponse;
                if (response.IsSuccessStatusCode)
                {
                    // Parse the response body.
                    apiResponse = response.Content.ReadAsAsync<ApiResponseDTO>().Result;  //Make sure to add a reference to System.Net.Http.Formatting.dll  
                    if (apiResponse.StatusCode != 200)
                    {
                        Logger.Log($"GetCustomSetting: Webservice returned ApiResponseDTO with status {apiResponse.StatusCode} ({apiResponse.Message}), settingName is {settingName}.");
                    }
                }
                else
                {
                    Logger.Log($"GetCustomSetting: Webservice call returned status {(int) response.StatusCode} ({response.ReasonPhrase}), settingName is {settingName}.");

                    apiResponse = new ApiResponseDTO
                    {
                        StatusCode = (int)response.StatusCode,
                        Message = response.ReasonPhrase
                    };
                }

                //Make any other calls using HttpClient here.

                //Dispose once all HttpClient calls are complete. This is not necessary if the containing object will be disposed of; for example in this case the HttpClient instance will be disposed automatically when the application terminates so the following call is superfluous.
                client.Dispose();

                return apiResponse;
            }

            return null;
        }

        //precache
        public static SettingDTO GetAllCompanySettings()
        {
            object authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
             if (authKey is string)
            {

                WebserviceCall wsCall = new WebserviceCall("GetCompanySettings", new List<WSParameter>() { new WSParameter("authKey", authKey.ToString()) });
                wsCall.Execute();

                JObject content;
                if (wsCall?.Response?.Content != null && wsCall.Response.Content.ToString() != "null")
                {
                    content = JsonConvert.DeserializeObject<JObject>(wsCall.Response.Content.ToString());
                    if (content != null)
                    {
                        int statusCode = content["StatusCode"]?.ToObject<int>() ?? -1;

                        if (statusCode == 200)
                        {
                            return content["Data"].ToObject<SettingDTO>();
                        }
                        else
                        {
                            Logger.Log($"GetAllCompanySettings: Webservice returned ApiResponseDTO with status {statusCode} ({content["Message"]}).");
                        }
                    }
                }
                else
                {
                    Logger.Log($"GetAllCompanySettings: Webservice returned NULL content.");
                }
            }

            return null;
        }

        public static ApiResponseDTO UpdateCustomSetting(string browserId, string settingName, string settingValue, bool? doNotOverWrite = null)
        {
            object authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
             if (authKey is string)
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(ConfigSettings.WebApiUrl);

                // Add an Accept header for JSON format.
                client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));

                //HttpResponseMessage response = await client.PostAsJsonAsync(
                //             "api/products", product);
                //         response.EnsureSuccessStatusCode();

                var buffer = System.Text.Encoding.UTF8.GetBytes(settingValue);
                var byteContent = new ByteArrayContent(buffer);
                byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                HttpResponseMessage response = client.PostAsync(string.Format("api/WebApi/UpdateCustomSetting?authKey={0}&browserId={1}&settingName={2}{3}", authKey, browserId, settingName, doNotOverWrite.HasValue ? $"&doNotOverWrite={doNotOverWrite.Value}": ""),
                    byteContent).Result;  // Blocking call! Program will wait here until a response is received or a timeout occurs.
                ApiResponseDTO apiResponse;
                if (response.IsSuccessStatusCode)
                {
                    // Parse the response body.
                    apiResponse = response.Content.ReadAsAsync<ApiResponseDTO>().Result;  //Make sure to add a reference to System.Net.Http.Formatting.dll                
                }
                else
                {
                    apiResponse = new ApiResponseDTO
                    {
                        StatusCode = (int)response.StatusCode,
                        Message = response.ReasonPhrase
                    };
                }

                //Make any other calls using HttpClient here.

                //Dispose once all HttpClient calls are complete. This is not necessary if the containing object will be disposed of; for example in this case the HttpClient instance will be disposed automatically when the application terminates so the following call is superfluous.
                client.Dispose();

                return apiResponse;
            }

            return null;
        }

        public static bool DeleteParagraphNumberingScheme(string browserId)
        {
            object authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
            if (authKey is string && !string.IsNullOrEmpty(browserId))
            {
                try
                {
                    var client = new RestClient(ConfigSettings.WebApiUrl);

                    var request = new RestRequest("api/WebApi/GetDeleteParagraphNumberingScheme?authKey=" + authKey + "&browserId=" + browserId, Method.GET);
                    var response = client.Execute(request);

                    var content = JsonConvert.DeserializeObject<ApiResponseDTO>(response.Content);
                    if (response.StatusCode == HttpStatusCode.OK && content.StatusCode == (int)HttpStatusCode.OK)
                    {
                        return Convert.ToBoolean(content.Data);
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex);
                }
            }
            return false;
        }

        static async Task<ApiResponseDTO> GetUserInfoAsync(string authKey)
        {
            ApiResponseDTO userInfo = null;
            HttpResponseMessage response = await client.GetAsync(string.Format("api/WebApi/GetUserInfo?authKey={1}", ConfigSettings.WebApiUrl, authKey));
            if (response.IsSuccessStatusCode)
            {
                userInfo = await response.Content.ReadAsAsync<ApiResponseDTO>();
            }
            return userInfo;
        }
        public static JToken GetJTokenTemplateList(TemplateType type, int? locationId = null, int? languageId = null)
        {
            object authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
             if (authKey is string)
            {
                var parameters = new List<WSParameter>()
                {
                    new WSParameter("authKey", authKey.ToString()),
                    new WSParameter("type", ((int)type).ToString())
                };

                if (locationId.HasValue)
                {
                    parameters.Add(new WSParameter("locationId", locationId.Value.ToString()));
                }

                if (languageId.HasValue)
                {
                    parameters.Add(new WSParameter("languageId", languageId.Value.ToString()));
                }

                WebserviceCall wsCall = new WebserviceCall("GetTemplates", parameters);

                JObject content;

                wsCall.Execute();

                if (wsCall.Response != null)
                {
                    content = JsonConvert.DeserializeObject<JObject>(wsCall.Response.Content.ToString());
                    int statusCode = content["StatusCode"].ToObject<int>();

                    if (statusCode == 200)
                    {
                        return content["Data"];
                    }
                }
            }

            return null;
        }
        public static string GetCompanyNameRichText(int companyId, MsoLanguageID languageID = MsoLanguageID.msoLanguageIDNone)
        {
            object authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
            if (authKey is string)
            {
                WebserviceCall wsCall = new WebserviceCall("DownloadCompanyNameTemplateAsXml", AddLanguageIdParameter(new List<WSParameter>()
                {
                    new WSParameter("authKey", authKey as string),
                    new WSParameter("aCompanyId", companyId.ToString())
                }, languageID));
                wsCall.Execute();

                if (wsCall.Response != null)
                {
                    var content = JsonConvert.DeserializeObject<JObject>(wsCall.Response.Content as string);
                    if (content["StatusCode"].ToObject<int>() == (int)HttpStatusCode.OK)
                    {
                        return content["Data"].ToString();
                    }
                }
            }

            return null;
        }

        public static List<TemplateListModel> GetTemplateList(TemplateType type, int? companyId = null, int? languageId = null, int? locationId = null)
        {
            var templateList = new List<TemplateListModel>();

            var authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
             if (authKey is string)
            {
                try
                {
                    var parameters = new List<WSParameter>()
                    {
                        new WSParameter("authKey", authKey.ToString()),
                        new WSParameter("type", ((int)type).ToString())
                    };

                    var cachedParameters = parameters.Select(x => x).ToList();

                    if (companyId.HasValue)
                    {
                        parameters.Add(new WSParameter("companyId", companyId.Value.ToString()));
                    }

                    if (languageId.HasValue && languageId.Value != 0)
                    {
                        parameters.Add(new WSParameter("languageId", languageId.Value.ToString()));
                    }

                    if (locationId.HasValue && locationId.Value != 0)
                    {
                        parameters.Add(new WSParameter("locationId", locationId.Value.ToString()));
                    }

                    WebserviceCall wsCall = new WebserviceCall("GetTemplates", parameters);
                    wsCall.CacheParameters = cachedParameters;

                    wsCall.Execute();

                    if (wsCall.Response != null)
                    {
                        var apiResponse = JsonConvert.DeserializeObject<ApiResponseDTO>(wsCall.Response.Content as string);
                        if (apiResponse != null && apiResponse.Data != null)
                        {
                            var results = JObject.Parse(apiResponse.Data.ToString());
                            if (results != null)
                            {
                                foreach (var result in results)
                                {
                                    templateList.Add(new TemplateListModel
                                    {
                                        Id = Convert.ToInt32(result.Key),
                                        Name = result.Value.ToString()
                                    });
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                }
            }

            return templateList;
        }

        public static List<ACompanyDTO> GetCompanies()
        {
            object authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
            if (authKey is string)
            {
                WebserviceCall wsCall = new WebserviceCall("GetACompanies", new List<WSParameter>()
                    {
                        new WSParameter("authKey", authKey as string)
                    });
                wsCall.Execute();

                if (wsCall.Response != null)
                {
                    JObject content = JsonConvert.DeserializeObject<JObject>(wsCall.Response.Content as string);
                    int statusCode = content["StatusCode"].ToObject<int>();

                    if (statusCode == 200)
                    {
                        return content["Data"].ToObject<List<ACompanyDTO>>();
                    }
                }
            }

            return null;
        }

        public static ACompanySimpleDTO GetCurrentCompanyDTO()
        {
            object authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
            if (authKey is string)
            {
                WebserviceCall wsCall = new WebserviceCall("GetCurrentACompany", new List<WSParameter>()
                    {
                        new WSParameter("authKey", authKey as string)
                    });

                wsCall.Execute();

                if (wsCall.Response != null)
                {
                    JObject content = JsonConvert.DeserializeObject<JObject>(wsCall.Response.Content as string);
                    int statusCode = content["StatusCode"].ToObject<int>();

                    if (statusCode == 200)
                    {
                        return content["Data"].ToObject<ACompanySimpleDTO>();
                    }
                }
            }

            return null;
        }

        public static void SetFavouriteAuthorLanguage(int id, int languageID)
        {
            object authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
            if (authKey is string)
            {
                WebserviceCall wsCall = new WebserviceCall("SetFavoriteAuthorLanguage", new List<WSParameter>()
                {
                        new WSParameter("authKey", authKey as string),
                        new WSParameter("authorId", id.ToString()),
                        new WSParameter("languageId", languageID.ToString())
                });

                wsCall.Execute();
            }
        }

        public static ACompanyWithPathsDTO GetCompanyDetails(int id, MsoLanguageID languageID = MsoLanguageID.msoLanguageIDNone)
        {
            object authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
            if (authKey is string)
            {
                WebserviceCall wsCall = new WebserviceCall("GetACompany", AddLanguageIdParameter(new List<WSParameter>()
                    {
                        new WSParameter("authKey", authKey as string),
                        new WSParameter("companyId", id.ToString())
                    }, languageID));
                wsCall.Execute();

                if (wsCall.Response != null)
                {
                    JObject content = JsonConvert.DeserializeObject<JObject>(wsCall.Response.Content as string);
                    int statusCode = content["StatusCode"].ToObject<int>();

                    if (statusCode == 200)
                    {
                        return content["Data"].ToObject<ACompanyWithPathsDTO>();
                    }
                }
            }

            return null;
        }

        public static void SetCurrentCompany(int id)
        {
            object authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
            if (authKey is string)
            {
                RestClient client = new RestClient(ConfigSettings.WebApiUrl);
                RestRequest request;
                IRestResponse response;

                request = new RestRequest("api/WebApi/GetSetCurrentCompanyId?authKey=" + (authKey as string) + "&companyId=" + id, Method.GET);
                response = client.Execute(request);
            }
        }

        public static void SetCurrentOfficeLocation(int id)
        {
            object authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
            if (authKey is string)
            {
                RestClient client = new RestClient(ConfigSettings.WebApiUrl);
                RestRequest request;
                IRestResponse response;

                request = new RestRequest("api/WebApi/GetSetCurrentLocationId?authKey=" + (authKey as string) + "&locationId=" + id, Method.GET);
                response = client.Execute(request);
            }
        }

        public static AuthorAddressDTO GetCurrentOfficeLocation()
        {
            object authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
            if (authKey is string)
            {
                WebserviceCall wsCall = new WebserviceCall("GetCurrentLocation", new List<WSParameter>()
                    {
                        new WSParameter("authKey", authKey as string)
                    });
                wsCall.Execute();

                if (wsCall.Response != null)
                {
                    JObject content = JsonConvert.DeserializeObject<JObject>(wsCall.Response.Content as string);
                    int statusCode = content["StatusCode"].ToObject<int>();

                    if (statusCode == 200)
                    {
                        return content["Data"].ToObject<AuthorAddressDTO>();
                    }
                }
            }

            return null;
        }

        public static bool IsCurrentUserAdmin()
        {
            object authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
             if (authKey is string)
            {
                WebserviceCall wsCall = new WebserviceCall("GetCurrentUserIsAdmin", new List<WSParameter>() { new WSParameter("authKey", authKey.ToString()) });
                wsCall.Execute();
                JObject content;

                if (wsCall.Response != null)
                {
                    content = JsonConvert.DeserializeObject<JObject>(wsCall.Response.Content.ToString());

                    int statusCode = content["StatusCode"].ToObject<int>();

                    if (statusCode == 200)
                    {
                        return (bool)content["Data"];
                    }
                }
            }

            return false;
        }

        public static string GetCustomExhibitStamp(MsoLanguageID languageID)
        {
            object authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
            if (authKey is string)
            {
                WebserviceCall wsCall = new WebserviceCall("DownloadCompanyAddendaExhibitTemplateAsXml", AddLanguageIdParameter(new List<WSParameter>() { new WSParameter("authKey", authKey.ToString()) }, languageID));
                wsCall.Execute();
                JObject content;

                if (wsCall.Response != null)
                {
                    content = JsonConvert.DeserializeObject<JObject>(wsCall.Response.Content as string);

                    int statusCode = content["StatusCode"].ToObject<int>();

                    if (statusCode == 200)
                    {
                        return content["Data"].ToString();
                    }

                }
            }

            return null;
        }

        public static string GetCustomCoverPage(MsoLanguageID languageID)
        {
            object authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
            if (authKey is string)
            {
                WebserviceCall wsCall = new WebserviceCall("DownloadCoverPageTemplateAsXml", AddLanguageIdParameter(new List<WSParameter>() { new WSParameter("authKey", authKey.ToString()) }, languageID));

                JObject content;
                wsCall.Execute();

                if (wsCall.Response != null)
                {
                    content = JsonConvert.DeserializeObject<JObject>(wsCall.Response.Content as string);

                    int statusCode = content["StatusCode"].ToObject<int>();

                    if (statusCode == 200)
                    {
                        return content["Data"].ToString();
                    }
                }
            }

            return null;
        }

        public static byte[] GetMultiPartyAgreementCoverPage(MsoLanguageID languageID)
        {
            object authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
            if (authKey is string)
            {
                WebserviceCall wsCall = new WebserviceCall("DownloadMultiPartyAgreementCoverPageTemplate", AddLanguageIdParameter(new List<WSParameter>() { new WSParameter("authKey", authKey.ToString()) }, languageID));
                wsCall.Execute(true);

                if (wsCall.Response != null)
                {
                    return wsCall.Response?.RawBytes;
                }
            }

            return null;
        }

        public static int? AssureLogin(int operationID)
        {
            object authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);

            if (authKey is string)
            {
                int? actionID = IsValidAuthKey(authKey as string, operationID);
                if (actionID != null)
                {
                    return actionID;
                }
                else
                {
                    object email = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "Email", null);
                    object licenseKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "LicenseKey", null);

                    if (email is string && licenseKey is string)
                    {
                        if (GetAuthKey(email as string, licenseKey as string, BaseUtils.GetProcessorId()))
                        {
                            authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
                            if (authKey is string)
                            {
                                return IsValidAuthKey(authKey as string, operationID);
                            }
                        }
                    }
                }
            }

            return null;
        }

        private static bool GetAuthKey(string email, string licenseKey, string deviceID)
        {
            RestClient client = new RestClient(ConfigSettings.WebApiUrl);
            RestRequest request;
            IRestResponse response;
            JObject content;
            request = new RestRequest("api/WebApi/GetActivateLicense?email=" + email + "&licenseKey=" + licenseKey + "&browserId=" + BaseUtils.GetProcessorId(), Method.GET);
            response = client.Execute(request);

            content = JsonConvert.DeserializeObject<JObject>(response.Content);
            int statusCode = content["StatusCode"].ToObject<int>();
            if (statusCode == 200 || statusCode == 201 || statusCode == 202)
            {
                Microsoft.Win32.Registry.SetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", content["Data"].ToObject<string>());
                return true;
            }

            return false;
        }

        private static int? IsValidAuthKey(string authKey, int operationID)
        {
            JObject content;
            List<WSParameter> parameters = new List<WSParameter>()
                {
                    new WSParameter("authKey", authKey)
                };
            if (operationID >= 0)
            {
                parameters.Add(new WSParameter("operationId", operationID.ToString()));
            }

            List<WSParameter> cacheParameters = new List<WSParameter>()
                {
                    new WSParameter("authKey", authKey)
                };
            WebserviceCall wsCall = new WebserviceCall("GetIsValidAuthKeyWithTest", parameters);
            wsCall.CacheParameters = cacheParameters;
            wsCall.Execute();

            if (wsCall.Response != null)
            {
                content = JsonConvert.DeserializeObject<JObject>(wsCall.Response.Content as string);
                int statusCode = content["StatusCode"].ToObject<int>();

                if (statusCode == 200)
                {
                    try
                    {
                        int? actionID = content["Data"].ToObject<int?>();
                        return actionID;
                    }
                    catch
                    {
                        return null;
                    }
                }
            }

            return null;
        }

        public static JToken GetCurrentCompany()
        {
            object authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
            if (authKey is string)
            {
                WebserviceCall wsCall = new WebserviceCall("GetCurrentACompany", new List<WSParameter>() { new WSParameter("authKey", authKey.ToString()) });

                JObject content;

                wsCall.Execute();

                if (wsCall.Response != null)
                {
                    content = JsonConvert.DeserializeObject<JObject>(wsCall.Response.Content as string);
                    int statusCode = content["StatusCode"].ToObject<int>();

                    if (statusCode == 200)
                    {
                        int id = content["Data"]["Id"].ToObject<int>();
                        WebserviceCall wsCall2 = new WebserviceCall("GetACompany", new List<WSParameter>() {
                            new WSParameter("authKey", authKey.ToString()),
                            new WSParameter("companyId", id.ToString())
                        });
                        wsCall2.Execute();

                        if (wsCall2.Response != null)
                        {
                            content = JsonConvert.DeserializeObject<JObject>(wsCall2.Response.Content.ToString());
                            statusCode = content["StatusCode"].ToObject<int>();

                            if (statusCode == 200)
                            {
                                return content["Data"];
                            }
                        }
                    }
                }
            }

            return null;
        }

        public static JToken GetCurrentAuthorACompany()
        {
            object authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
             if (authKey is string)
            {
                WebserviceCall wsCall = new WebserviceCall("GetCurrentAuthorACompany", new List<WSParameter>() { new WSParameter("authKey", authKey.ToString()) });

                JObject content;

                wsCall.Execute();

                if (wsCall.Response != null)
                {
                    content = JsonConvert.DeserializeObject<JObject>(wsCall.Response.Content.ToString());
                    int statusCode = content["StatusCode"].ToObject<int>();

                    if (statusCode == 200)
                    {
                        return content["Data"];
                    }
                }
            }

            return null;
        }

        public static string GetTemplateAsXml(int id)
        {
            object authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
            if (authKey is string)
            {
                WebserviceCall wsCall = new WebserviceCall("ConvertTOOOXMLAndDownloadTemplate", new List<WSParameter>()
                    {
                        new WSParameter("authKey", authKey as string),
                        new WSParameter("templateId", id.ToString())
                    });
                wsCall.Execute();

                if (wsCall.Response != null)
                {
                    return wsCall.Response.Content as string;
                }
            }

            return null;
        }

    

        public static ACompanySimpleDTO GetCurrentCompanySimpleDTO()
        {
            object authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
            if (authKey is string)
            {
                WebserviceCall wsCall = new WebserviceCall("GetCurrentACompany", new List<WSParameter>()
                    {
                        new WSParameter("authKey", authKey as string)
                    });
                wsCall.Execute();
                if (wsCall.Response != null)
                {
                    ApiResponseDTO apiResponse = JsonConvert.DeserializeObject<ApiResponseDTO>(wsCall.Response.Content as string);
                    if (apiResponse.StatusCode == 200)
                    {
                        return JsonConvert.DeserializeObject<ACompanySimpleDTO>(apiResponse.Data.ToString());
                    }
                }
            }

            return null;
        }
        //offline mode
        public static string GetCompanyLogoUrl(int companyId, bool isSmall = false)
        {
            object authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
            if (authKey is string)
            {
                WebserviceCall wsCall = new WebserviceCall("GetCompanyLogo", new List<WSParameter>()
                    {
                        new WSParameter("authKey", authKey as string),
                        new WSParameter("companyId",companyId.ToString()),
                        new WSParameter("isSmall",  isSmall.ToString())
                    });
                wsCall.Execute(true);

                if (wsCall.Response != null)
                {
                    string path = Path.GetTempFileName();
                    if (wsCall.Response.RawBytes != null)
                    {
                        File.WriteAllBytes(path, wsCall.Response.RawBytes);
                        return path;
                    }
                }
            }

            return null;
        }

        public static string GetCompanyLocationAddressGraphicUrl(int locationId, int acompanyId)
        {
            object authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
            if (authKey is string)
            {
                WebserviceCall wsCall = new WebserviceCall("DownloadCompanyLocationAddressGraphic", new List<WSParameter>()
                    {
                        new WSParameter("authKey", authKey as string),
                        new WSParameter("locationId", locationId.ToString()),
                        new WSParameter("isInline",  (true).ToString()),
                        new WSParameter("acompanyId", acompanyId.ToString())
                    });
                wsCall.Execute(true);

                if (wsCall.Response != null && wsCall.Response.RawBytes.Count() != 0)
                {
                    string path = Path.GetTempFileName();
                    if (wsCall.Response.RawBytes != null)
                    {
                        File.WriteAllBytes(path, wsCall.Response.RawBytes);
                        return path;
                    }
                }
            }

            return null;
        }

        public static AuthorWithPathsDTO GetCurrentAuthor()
        {
            object authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
            if (authKey is string)
            {
                WebserviceCall wsCall = new WebserviceCall("GetCurrentAuthor", new List<WSParameter>()
                    {
                        new WSParameter("authKey", authKey as string)
                    });
                wsCall.Execute();

                if (wsCall.Response != null)
                {
                    ApiResponseDTO apiResponse = JsonConvert.DeserializeObject<ApiResponseDTO>(wsCall.Response.Content as string);
                    if (apiResponse.StatusCode == 200)
                    {
                        return JsonConvert.DeserializeObject<AuthorWithPathsDTO>(apiResponse.Data.ToString());
                    }
                }
            }

            return null;
        }

        public static int GetCurrentAsistantId()
        {
            object authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
            if (authKey is string)
            {
                WebserviceCall wsCall = new WebserviceCall("GetCurrentAssistantId", new List<WSParameter>()
                    {
                        new WSParameter("authKey", authKey as string)
                    });
                wsCall.Execute();

                if (wsCall.Response != null)
                {
                    ApiResponseDTO apiResponse = JsonConvert.DeserializeObject<ApiResponseDTO>(wsCall.Response.Content as string);
                    if (apiResponse.StatusCode == 200)
                    {
                        return JsonConvert.DeserializeObject<int>(apiResponse.Data.ToString());
                    }
                }
            }

            return 0;
        }

        public static AuthorWithPathsDTO GetAuthor(int id, MsoLanguageID languageID = MsoLanguageID.msoLanguageIDNone)
        {
            object authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
            AuthorWithPathsDTO author = new AuthorWithPathsDTO();

            if (authKey is string)
            {
                WebserviceCall wsCall = new WebserviceCall("GetAuthor", AddLanguageIdParameter(new List<WSParameter>()
                    {
                        new WSParameter("authKey", authKey as string),
                        new WSParameter("authorId", id.ToString())
                    }, languageID));
                wsCall.Execute();

                if (wsCall.Response != null)
                {
                    ApiResponseDTO apiResponse = JsonConvert.DeserializeObject<ApiResponseDTO>(wsCall.Response.Content as string);

                    if (apiResponse.StatusCode == 200)
                    {
                        apiResponse.DeArrayData();
                        try
                        {
                            author = Newtonsoft.Json.JsonConvert.DeserializeObject<AuthorWithPathsDTO>(apiResponse.Data.ToString());
                        }
                        catch { }
                    }
                }
            }

            return author;
        }

        public static List<AuthorDTO> GetAuthors(int? languageId = null)
        {
            object authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
            List<AuthorDTO> authors = new List<AuthorDTO>();

            if (authKey is string)
            {
                WebserviceCall wsCall = new WebserviceCall("GetAuthors", new List<WSParameter>()
                    {
                        new WSParameter("authKey", authKey as string),
                        new WSParameter("type", 2.ToString())
                    });

                if (languageId != null)
                {
                    wsCall.Parameters.Add(new WSParameter("languageId", languageId.Value.ToString()));
                }

                wsCall.Execute();

                if (wsCall.Response != null)
                {
                    ApiResponseDTO apiResponse = JsonConvert.DeserializeObject<ApiResponseDTO>(wsCall.Response.Content as string);

                    if (apiResponse.StatusCode == 200)
                    {
                        authors = Newtonsoft.Json.JsonConvert.DeserializeObject<List<AuthorDTO>>(apiResponse.Data.ToString());
                    }

                }
            }

            return authors;
        }

        public static string GetCompanyMultiPrintSettings()
        {
            object authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
            if (authKey is string)
            {
                WebserviceCall wsCall = new WebserviceCall("GetCompanyMultiPrintSettings", new List<WSParameter>()
                    {
                        new WSParameter("authKey", authKey as string)
                    });
                wsCall.Execute();

                if (wsCall.Response != null)
                {
                    var content = JsonConvert.DeserializeObject<ApiResponseDTO>(wsCall.Response.Content.ToString());

                    return content.Data?.ToString();
                }
            }

            return null;
        }

        public static ApiResponseDTO UpdateParagraphNumberingScheme(string schemeName, string schemeValue)
        {
            object authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
            if (authKey is string)
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(ConfigSettings.WebApiUrl);

                // Add an Accept header for JSON format.
                client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));

                var buffer = System.Text.Encoding.UTF8.GetBytes(schemeValue);
                var byteContent = new ByteArrayContent(buffer);
                byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                HttpResponseMessage response = client.PostAsync(string.Format("api/WebApi/UpdateParagraphNumberingScheme?authKey={0}&schemeName={1}", authKey, schemeName),
                    byteContent).Result;  // Blocking call! Program will wait here until a response is received or a timeout occurs.
                ApiResponseDTO apiResponse;
                if (response.IsSuccessStatusCode)
                {
                    // Parse the response body.
                    apiResponse = response.Content.ReadAsAsync<ApiResponseDTO>().Result;  //Make sure to add a reference to System.Net.Http.Formatting.dll                
                }
                else
                {
                    apiResponse = new ApiResponseDTO
                    {
                        StatusCode = (int)response.StatusCode,
                        Message = response.ReasonPhrase
                    };
                }

                //Make any other calls using HttpClient here.

                //Dispose once all HttpClient calls are complete. This is not necessary if the containing object will be disposed of; for example in this case the HttpClient instance will be disposed automatically when the application terminates so the following call is superfluous.
                client.Dispose();
                return apiResponse;
            }
            else
                return null;
        }

        //precache
        public static ApiResponseDTO GetParagraphNumberingSchemes()
        {
            object authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
            if (authKey is string)
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(ConfigSettings.WebApiUrl);

                // Add an Accept header for JSON format.
                client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));

                List<WSParameter> parameters = new List<WSParameter>()
                {
                    new WSParameter("authKey", authKey.ToString())
                };
                WebserviceResponse webserviceResponse = null;
                // List data response.
                HttpResponseMessage response = null;
                try
                {
                    response = client.GetAsync(string.Format("api/WebApi/GetParagraphNumberingSchemes?authKey={0}", authKey)).Result;  // Blocking call! Program will wait here until a response is received or a timeout occurs.
                }
                catch { }
                ApiResponseDTO apiResponse;
                if (response != null && response.IsSuccessStatusCode)
                {
                    // Parse the response body.
                    apiResponse = response.Content.ReadAsAsync<ApiResponseDTO>().Result;  //Make sure to add a reference to System.Net.Http.Formatting.dll                
                    webserviceResponse = new WebserviceResponse("GetParagraphNumberingSchemes", parameters, apiResponse);
                    OfflineModeManager.SaveWebserviceResponse(webserviceResponse);
                }
                else
                {
                    webserviceResponse = OfflineModeManager.GetWebserviceResponse("GetParagraphNumberingSchemes", parameters);
                    if (webserviceResponse.Content is ApiResponseDTO)
                    {
                        apiResponse = webserviceResponse.Content as ApiResponseDTO;
                    }
                    else if (webserviceResponse.Content is JObject)
                    {
                        apiResponse = (webserviceResponse.Content as JObject).ToObject<ApiResponseDTO>();
                    }
                    else
                    {
                        apiResponse = new ApiResponseDTO
                        {
                            StatusCode = (int)response.StatusCode,
                            Message = response?.ReasonPhrase
                        };
                    }
                }

                //Make any other calls using HttpClient here.

                //Dispose once all HttpClient calls are complete. This is not necessary if the containing object will be disposed of; for example in this case the HttpClient instance will be disposed automatically when the application terminates so the following call is superfluous.
                client.Dispose();
                return apiResponse;
            }
            else
                return null;
        }


        public static byte[] GetTemplate(int id)
        {
            object authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
             if (authKey is string)
            {
                WebserviceCall wsCall = new WebserviceCall("DownloadTemplate", new List<WSParameter>()
                {
                    new WSParameter("authKey", authKey.ToString()),
                    new WSParameter("templateId", id.ToString()),
                    new WSParameter("forWebAddin", "false")
                });

                wsCall.CacheParameters = new List<WSParameter>()
                {
                    new WSParameter("authKey", authKey.ToString()),
                    new WSParameter("templateId", id.ToString()),
                };

                wsCall.Execute(true);

                return wsCall.Response?.RawBytes;
            }

            return null;
        }

        //static async Task<ApiResponseDTO> GetProductAsync(string path)
        //      {
        //          ApiResponseDTO product = null;
        //          HttpResponseMessage response = await client.GetAsync(path);
        //          if (response.IsSuccessStatusCode)
        //          {
        //              product = await response.Content.ReadAsAsync<ApiResponseDTO>();
        //          }
        //          return product;
        //      }

        //static async Task<Uri> CreateProductAsync(Product product)
        //      {
        //          HttpResponseMessage response = await client.PostAsJsonAsync(
        //              "api/products", product);
        //          response.EnsureSuccessStatusCode();

        //          // return URI of the created resource.
        //          return response.Headers.Location;
        //      }

        //static async Task<ApiResponseDTO> UpdateProductAsync(Product product)
        //      {
        //          HttpResponseMessage response = await client.PutAsJsonAsync(
        //              $"api/products/{product.Id}", product);
        //          response.EnsureSuccessStatusCode();

        //          // Deserialize the updated product from the response body.
        //          ApiResponseDTO apiResponse = await response.Content.ReadAsAsync<ApiResponseDTO>();
        //          return apiResponse;
        //      }

        //      static async Task<HttpStatusCode> DeleteProductAsync(string id)
        //      {
        //          HttpResponseMessage response = await client.DeleteAsync(
        //              $"api/products/{id}");
        //          return response.StatusCode;
        //      }

        public static async Task RunAsync()
        {
            // Update port # in the following line.
            client.BaseAddress = new Uri(ConfigSettings.WebApiUrl);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));

            try
            {
                var userInfo = await GetUserInfoAsync("AD65F9D6-1488-443B-A058-413559CAB2C2");

                // Create a new product
                //Product product = new Product
                //{
                //    Name = "Gizmo",
                //    Price = 100,
                //    Category = "Widgets"
                //};

                //var url = await CreateProductAsync(product);
                //Console.WriteLine($"Created at {url}");

                // Get the product
                //product = await GetProductAsync(url.PathAndQuery);
                ////ShowProduct(product);

                //// Update the product
                //Console.WriteLine("Updating price...");
                //product.Price = 80;
                //await UpdateProductAsync(product);

                //// Get the updated product
                //product = await GetProductAsync(url.PathAndQuery);
                //ShowProduct(product);

                // Delete the product
                //var statusCode = await DeleteProductAsync(product.Id);
                //Console.WriteLine($"Deleted (HTTP Status = {(int)statusCode})");

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public static byte[] GetFirmListsDocument()
        {
            object authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
             if (authKey is string)
            {
                WebserviceCall wsCall = new WebserviceCall("DownloadCompanyFirmListDocument", new List<WSParameter>()
                {
                    new WSParameter("authKey", authKey.ToString())
                });
                wsCall.Execute(true);

                if (wsCall.Response != null)
                {
                    byte[] result = wsCall.Response.RawBytes;
                    if (result?.Length > 0)
                    {
                        return result;
                    }
                }
            }

            return null;
        }

        public static JToken GetFirmListStructure()
        {
            object authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
             if (authKey is string)
            {
                WebserviceCall wsCall = new WebserviceCall("DownloadCompanyFirmListStructure", new List<WSParameter>()
                {
                    new WSParameter("authKey", authKey.ToString())
                });

                wsCall.Execute();
                if (wsCall.Response != null)
                {
                    try
                    {
                        return JsonConvert.DeserializeObject<JObject>(wsCall.Response.Content as string);
                    }
                    catch
                    {
                    }
                }
            }

            return null;
        }

        public static bool SetFavouriteAuthor(int authorId, bool isFavourite = true)
        {
            var authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
            if (authKey is string)
            {
                try
                {
                    var client = new RestClient(ConfigSettings.WebApiUrl);
                    var request = new RestRequest("api/WebApi/GetSetIsFavorite?authKey=" + (authKey as string) + "&authorId=" + authorId + "&isFav=" + isFavourite, Method.GET);

                    var response = client.Execute(request);
                    if (response.StatusCode == HttpStatusCode.OK || response.StatusCode == HttpStatusCode.Accepted)
                    {
                        return true;
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                }
            }
            return false;
        }

        public static List<FavouriteAuthorDTO> GetFavouriteAuthors()
        {
            var authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
            if (authKey is string)
            {
                try
                {
                    WebserviceCall wsCall = new WebserviceCall("GetFavouriteAuthors", new List<WSParameter>()
                    {
                        new WSParameter("authKey", authKey as string)
                    });
                    wsCall.Execute();

                    if (wsCall.Response != null)
                    {
                        var apiResponse = JsonConvert.DeserializeObject<ApiResponseDTO>(wsCall.Response.Content as string);
                        if (apiResponse != null && apiResponse.Data != null)
                        {
                            return JsonConvert.DeserializeObject<List<FavouriteAuthorDTO>>(apiResponse.Data.ToString()).OrderBy(x => x.Id).ToList();
                        }
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                }
            }
            return null;
        }

        public static bool GetSetCurrentAuthorId(int authorId = 0)
        {
            var authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
            if (authKey is string)
            {
                try
                {
                    var requestUrl = "api/WebApi/GetSetCurrentAuthorId?authKey=" + (authKey as string);
                    requestUrl += "&authorId=" + authorId;

                    var client = new RestClient(ConfigSettings.WebApiUrl);
                    var request = new RestRequest(requestUrl, Method.GET);

                    var response = client.Execute(request);
                    if (response.StatusCode == HttpStatusCode.OK || response.StatusCode == HttpStatusCode.Accepted)
                    {
                        var apiResponse = JsonConvert.DeserializeObject<ApiResponseDTO>(response.Content);
                        if (apiResponse != null && apiResponse.StatusCode == 200)
                        {
                            return true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                }
            }
            return false;
        }

        public static bool GetSetCurrentAssistantId(int assistantId = 0)
        {
            var authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
            if (authKey is string)
            {
                try
                {
                    var requestUrl = "api/WebApi/GetSetCurrentAssistantId?authKey=" + (authKey as string);
              
                        requestUrl += "&assistantId=" + assistantId;
                    var client = new RestClient(ConfigSettings.WebApiUrl);
                    var request = new RestRequest(requestUrl, Method.GET);

                    var response = client.Execute(request);
                    if (response.StatusCode == HttpStatusCode.OK || response.StatusCode == HttpStatusCode.Accepted)
                    {
                        var apiResponse = JsonConvert.DeserializeObject<ApiResponseDTO>(response.Content);
                        if (apiResponse != null && apiResponse.Data != null && apiResponse.StatusCode == 200)
                        {
                            return true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                }
            }
            return false;
        }

        public static int GetCurrentAuthorId()
        {
            var authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
            if (authKey is string)
            {
                try
                {
                    WebserviceCall wsCall = new WebserviceCall("GetCurrentAuthor", new List<WSParameter>()
                    {
                        new WSParameter("authKey", authKey as string)
                    });
                    wsCall.Execute();

                    if (wsCall.Response != null)
                    {
                        var apiResponse = JsonConvert.DeserializeObject<ApiResponseDTO>(wsCall.Response.Content as string);
                        if (apiResponse != null && apiResponse.Data != null && apiResponse.StatusCode == 200)
                        {
                            var currentAuthor = JsonConvert.DeserializeObject<AuthorWithPathsDTO>(apiResponse.Data.ToString());
                            if (currentAuthor != null)
                            {
                                return currentAuthor.Id;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                }
            }
            return 0;
        }

        public static int GetCurrentAssistantId()
        {
            var authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
            if (authKey is string)
            {
                try
                {
                    WebserviceCall wsCall = new WebserviceCall("GetCurrentAssistantId", new List<WSParameter>()
                    {
                        new WSParameter("authKey", authKey as string)
                    });
                    wsCall.Execute();

                    if (wsCall.Response != null)
                    {
                        var apiResponse = JsonConvert.DeserializeObject<ApiResponseDTO>(wsCall.Response.Content as string);
                        if (apiResponse != null && apiResponse.Data != null && apiResponse.StatusCode == 200)
                        {
                            return int.Parse(apiResponse.Data.ToString());
                        }
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                }
            }
            return 0;
        }

        public static AuthorAddressDTO GetAddress(int? locationId, MsoLanguageID languageID = MsoLanguageID.msoLanguageIDNone)
        {
            var authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
            if (authKey is string && locationId != null && locationId != 0)
            {
                try
                {
                    WebserviceCall wsCall = new WebserviceCall("GetAddress", AddLanguageIdParameter(new List<WSParameter>()
                    {
                        new WSParameter("authKey", authKey as string),
                        new WSParameter("locationId", locationId.Value.ToString())
                    }, languageID));

                    wsCall.Execute();
                    if (wsCall.Response != null)
                    {
                        var apiResponse = JsonConvert.DeserializeObject<ApiResponseDTO>(wsCall.Response.Content as string);
                        if (apiResponse != null && apiResponse.Data != null && apiResponse.StatusCode == 200)
                        {
                            var authorAddress = JsonConvert.DeserializeObject<AuthorAddressDTO>(apiResponse.Data.ToString());
                            if (authorAddress != null)
                            {
                                return authorAddress;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                }
            }
            return null;
        }

        //TODO support for MultiLanguage
        public static List<AuthorAddressDTO> GetAddresses(int type = 0)
        {
            var authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
            if (authKey is string)
            {
                try
                {
                    WebserviceCall wsCall = new WebserviceCall("GetAddresses", new List<WSParameter>()
                    {
                        new WSParameter("authKey", authKey as string),
                        new WSParameter("type", type.ToString())
                    });
                    wsCall.Execute();

                    if (wsCall.Response != null)
                    {
                        var apiResponse = JsonConvert.DeserializeObject<ApiResponseDTO>(wsCall.Response.Content as string);
                        if (apiResponse != null && apiResponse.Data != null && apiResponse.StatusCode == 200)
                        {
                            var authorAddress = JsonConvert.DeserializeObject<List<AuthorAddressDTO>>(apiResponse.Data.ToString());
                            if (authorAddress != null)
                            {
                                return authorAddress;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                }
            }
            return null;
        }

        public static List<AContactInformationDTO> GetAuthorOfficeSpecificContactInformation(int authorId, int locationId)
        {
            var authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
            if (authKey is string)
            {
                try
                {
                    WebserviceCall wsCall = new WebserviceCall("GetAuthorOfficeSpecificContactInformation", new List<WSParameter>()
                    {
                        new WSParameter("authKey", authKey as string),
                        new WSParameter("authorId", authorId.ToString()),
                        new WSParameter("locationId", locationId.ToString())
                    });
                    wsCall.Execute();

                    if (wsCall.Response != null)
                    {
                        var apiResponse = JsonConvert.DeserializeObject<ApiResponseDTO>(wsCall.Response.Content as string);
                        if (apiResponse != null && apiResponse.Data != null && apiResponse.StatusCode == 200)
                        {
                            var result = JsonConvert.DeserializeObject<List<AContactInformationDTO>>(apiResponse.Data.ToString());
                            if (result != null)
                            {
                                return result;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                }
            }
            return null;
        }


        public static bool SetFavouriteAuthorOfficeLocation(int authorId, int locationId)
        {
            var authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
            if (authKey is string && authorId != 0)
            {
                try
                {
                    var requestUrl = "api/WebApi/GetSetFavouriteAuthorOfficeLocationId?authKey=" + (authKey as string) + "&authorId=" + authorId + "&locationId=" + locationId;
                    var client = new RestClient(ConfigSettings.WebApiUrl);
                    var request = new RestRequest(requestUrl, Method.GET);

                    var response = client.Execute(request);
                    if (response.StatusCode == HttpStatusCode.OK || response.StatusCode == HttpStatusCode.Accepted)
                    {
                        var apiResponse = JsonConvert.DeserializeObject<ApiResponseDTO>(response.Content);
                        if (apiResponse != null && apiResponse.StatusCode == 200)
                        {
                            return true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                }
            }
            return false;
        }

        public static bool SetFavouriteAuthorACompany(int favouriteAuthorId, int aCompanyId)
        {
            var authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
            if (authKey is string && favouriteAuthorId != 0)
            {
                try
                {
                    var requestUrl = "api/WebApi/GetSetFavouriteAuthorCurrentCompanyId?authKey=" + (authKey as string) + "&authorId=" + favouriteAuthorId + "&companyId=" + aCompanyId;
                    var client = new RestClient(ConfigSettings.WebApiUrl);
                    var request = new RestRequest(requestUrl, Method.GET);

                    var response = client.Execute(request);
                    if (response.StatusCode == HttpStatusCode.OK || response.StatusCode == HttpStatusCode.Accepted)
                    {
                        var apiResponse = JsonConvert.DeserializeObject<ApiResponseDTO>(response.Content);
                        if (apiResponse != null && apiResponse.StatusCode == 200)
                        {
                            return true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                }
            }
            return false;
        }

        public static int? LogEvent(int? actionID, ButtonEventType buttonEventType, List<KeyValuePair<string, string>> options = null)
        {
            if (actionID > 0)
            {
                object authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
                 if (authKey is string)
                {
                    try
                    {
                        var client = new RestClient(ConfigSettings.WebApiUrl);
                        var request = new RestRequest("api/WebApi/LogEvent?authKey=" + authKey + "&actionId=" + actionID + "&buttonEventType=" + (int)buttonEventType, Method.POST);
                        if (options != null)
                        {
                            request.AddParameter("options", JsonConvert.SerializeObject(options));
                        }

                        var response = client.Execute(request);

                        var content = JsonConvert.DeserializeObject<ApiResponseDTO>(response.Content);
                        if (response.StatusCode == HttpStatusCode.OK && content.StatusCode == (int)HttpStatusCode.OK)
                        {
                            if (content.Data is Int64)
                            {
                                return Convert.ToInt32(content.Data);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex);
                    }
                }
            }
            return null;
        }

        public static void LogEventResult(int? eventID, bool ok, string message)
        {
            if (eventID > 0)
            {
                object authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
                 if (authKey is string)
                {
                    try
                    {
                        var client = new RestClient(ConfigSettings.WebApiUrl);
                        var request = new RestRequest("api/WebApi/LogEventResult?authKey=" + authKey + "&eventId=" + eventID + "&success=" + ok, Method.POST);
                        if (message != null)
                        {
                            request.AddParameter("message", message);
                        }

                        var response = client.Execute(request);
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex);
                    }
                }
            }
        }

        public static List<AuthorAddressDTO> GetAuthorLocations(int authorId)
        {
            var authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
            if (authKey is string && authorId != 0)
            {
                try
                {
                    WebserviceCall wsCall = new WebserviceCall("GetAuthorLocations", new List<WSParameter>()
                    {
                        new WSParameter("authKey", authKey as string),
                        new WSParameter("authorId", authorId.ToString())
                    });
                    wsCall.Execute();

                    if (wsCall.Response != null)
                    {
                        var apiResponse = JsonConvert.DeserializeObject<ApiResponseDTO>(wsCall.Response.Content as string);
                        if (apiResponse != null && apiResponse.Data != null && apiResponse.StatusCode == 200)
                        {
                            return JsonConvert.DeserializeObject<List<AuthorAddressDTO>>(apiResponse.Data.ToString());
                        }
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                }
            }
            return null;
        }

        public static int GetCompanyIdOfTemplate(int templateId)
        {
            var authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
            if (authKey is string && templateId != 0)
            {
                try
                {
                    WebserviceCall wsCall = new WebserviceCall("GetTemplateACompany", new List<WSParameter>()
                    {
                        new WSParameter("authKey", authKey as string),
                        new WSParameter("templateId", templateId.ToString())
                    });
                    wsCall.Execute();

                    if (wsCall.Response != null)
                    {
                        var apiResponse = JsonConvert.DeserializeObject<ApiResponseDTO>(wsCall.Response.Content as string);
                        if (apiResponse != null && apiResponse.Data != null && apiResponse.StatusCode == 200)
                        {
                            ACompanySimpleDTO company = JsonConvert.DeserializeObject<ACompanySimpleDTO>(apiResponse.Data.ToString());
                            if (company != null)
                            {
                                return company.Id;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                }
            }

            return -1;
        }

        public static HelpFileList GetHelpDownloadList()
        {
            try
            {
                string requestUrl = "VSTO/Help/downloads/list.json";
                var client = new RestClient(ConfigSettings.WebApiUrl);
                var request = new RestRequest(requestUrl, Method.GET);

                var response = client.Execute(request);
                if (response.StatusCode == HttpStatusCode.OK || response.StatusCode == HttpStatusCode.Accepted)
                {
                    var apiResponse = JsonConvert.DeserializeObject<HelpFileList>(response.Content);
                    return apiResponse;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }

            return null;
        }

        public static byte[] DownloadHelpFile(string fileName)
        {
            try
            {
                string requestUrl = "VSTO/Help/downloads/" + fileName;
                var client = new RestClient(ConfigSettings.WebApiUrl);

                var request = new RestRequest(requestUrl, Method.GET);
                request.AddHeader("Accept", "application/pdf");
                var response = client.Execute(request);
                if (response.StatusCode == HttpStatusCode.OK || response.StatusCode == HttpStatusCode.Accepted)
                {
                    return response.RawBytes;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }

            return null;
        }

        public static bool TestConnection()
        {
            object authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
             if (authKey is string)
            {
                RestClient client = new RestClient(ConfigSettings.WebApiUrl);
                RestRequest request;
                IRestResponse response;
                JObject content;

                request = new RestRequest("api/WebApi/GetIsValidAuthKeyWithTest?authKey=" + authKey, Method.GET);
                response = client.Execute(request);

                if (response.StatusCode == HttpStatusCode.OK || response.StatusCode == HttpStatusCode.Accepted)
                {
                    content = JsonConvert.DeserializeObject<JObject>(response.Content);
                    int statusCode = content["StatusCode"].ToObject<int>();

                    if (statusCode == 200)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public static Dictionary<int, Dictionary<int, string>> DownLoadTranslations()
        {
            WebserviceCall wsCall = new WebserviceCall("DownloadTranslations", new List<WSParameter>());
            wsCall.Execute(false);

            if (wsCall.Response != null && wsCall.Response.Content != null)
            {
                try
                {
                    return JsonConvert.DeserializeObject<Dictionary<int, Dictionary<int, string>>>(wsCall.Response.Content.ToString());
                }
                catch { }
            }

            return null;
        }

        public static HelpVideoList GetHelpVideos()
        {
            try
            {
                string requestUrl = "VSTO/Help/videos/list.json";
                var client = new RestClient(ConfigSettings.WebApiUrl);
                var request = new RestRequest(requestUrl, Method.GET);

                var response = client.Execute(request);
                if (response.StatusCode == HttpStatusCode.OK || response.StatusCode == HttpStatusCode.Accepted)
                {
                    var apiResponse = JsonConvert.DeserializeObject<HelpVideoList>(BaseUtils.PrepareJSONForDeserialization(response.Content));
                    return apiResponse;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }

            return null;
        }

        public static bool GetHasInform()
        {
            var authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
            if (authKey is string)
            {
                WebserviceCall wsCall = new WebserviceCall("GetHasInform", new List<WSParameter>()
                {
                    new WSParameter("authKey", authKey as string)
                });
                wsCall.Execute(useApiResponse: true);

                if (wsCall.Response?.Content is ApiResponseDTO)
                {
                    try
                    {
                        var apiResponse = wsCall.Response?.Content as ApiResponseDTO;

                        if (bool.TryParse(apiResponse.Data.ToString(), out bool boolType))
                        {
                            return boolType;
                        }
                    }
                    catch { }
                }
            }
            return false;
        }
        public static Dictionary<int, string> GetFormCollectionList()
        {
            var authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
            if (authKey is string)
            {
                WebserviceCall wsCall = new WebserviceCall("GetFormCollectionList", new List<WSParameter>()
                {
                    new WSParameter("authKey", authKey as string)
                });
                wsCall.Execute(useApiResponse: true);

                if (wsCall.Response?.Content is ApiResponseDTO)
                {
                    try
                    {
                        var apiResponse = wsCall.Response?.Content as ApiResponseDTO;
                        if (apiResponse.Data is JToken)
                        {
                            return (apiResponse.Data as JToken).ToObject<Dictionary<int, string>>();
                        }
                    }
                    catch { }
                }
            }
            return null;
        }
        public static List<TemplateListModel> GetFormList(int id)
        {
            List<TemplateListModel> templateList = new List<TemplateListModel>(); 
            var authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
            if (authKey is string)
            {
                WebserviceCall wsCall = new WebserviceCall("GetFormList", new List<WSParameter>()
                {
                    new WSParameter("authKey", authKey as string),
                    new WSParameter("formCollectionId", id.ToString())
                });
                wsCall.Execute(useApiResponse: true);

                if (wsCall.Response?.Content is ApiResponseDTO)
                {
                    try
                    {
                        var apiResponse = wsCall.Response?.Content as ApiResponseDTO;
                        if (apiResponse != null && apiResponse.Data != null)
                        {
                            var results = JObject.Parse(apiResponse.Data.ToString());
                            if (results != null)
                            {
                                foreach (var result in results)
                                {
                                    templateList.Add(new TemplateListModel
                                    {
                                        Id = Convert.ToInt32(result.Key),
                                        Name = result.Value.ToString(),
                                        FormCollectionId = id,
                                    });
                                }
                            }
                        }
                    }
                    catch { }
                }
            }
            return templateList;
        }

        public static byte[] GetInformTemplate(int id)
        {
            object authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
            if (authKey is string)
            {
                WebserviceCall wsCall = new WebserviceCall("DownloadForm", new List<WSParameter>()
                {
                    new WSParameter("authKey", authKey.ToString()),
                    new WSParameter("formId", id.ToString())
                });

                wsCall.Execute(true);

                return wsCall.Response?.RawBytes;
            }

            return null;
        }

        public static string GetSigningLineHash(MsoLanguageID languageId = MsoLanguageID.msoLanguageIDNone)
        {
            object authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
            if (authKey is string)
            {
                WebserviceCall wsCall = new WebserviceCall("GetSigningLineHash", AddLanguageIdParameter(new List<WSParameter>()
                {
                    new WSParameter("authKey", authKey.ToString()),
                }, languageId));

                wsCall.Execute(useApiResponse: true);

                if (wsCall.Response?.Content is ApiResponseDTO)
                {
                    try
                    {
                        var apiResponse = wsCall.Response?.Content as ApiResponseDTO;
                        if (apiResponse != null && apiResponse.Data != null)
                        {
                            return apiResponse.Data.ToString();
                        }
                    }
                    catch { }
                }
            }

            return null;
        }

        public static byte[] GetClosingAgendaRecordBookIndex()
        {
            object authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
             if (authKey is string)
            {
                WebserviceCall wsCall = new WebserviceCall("DownloadClosingAgendaRecordBookIndex", new List<WSParameter>()
                {
                    new WSParameter("authKey", authKey.ToString())
                });

                wsCall.Execute(true);

                return wsCall.Response?.RawBytes;
            }

            return null;
        }

        public static byte[] GetClosingAgendaFileLabels()
        {
            object authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
             if (authKey is string)
            {
                WebserviceCall wsCall = new WebserviceCall("DownloadClosingAgendaFileLabels", new List<WSParameter>()
                {
                    new WSParameter("authKey", authKey.ToString())
                });

                wsCall.Execute(true);

                return wsCall.Response?.RawBytes;
            }

            return null;
        }

        public static Dictionary<string, string> GetSigningLinePreviews(MsoLanguageID languageId = MsoLanguageID.msoLanguageIDNone)
        {
            var authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
             if (authKey is string)
            {
                WebserviceCall wsCall = new WebserviceCall("GetSigningLinePreviews", AddLanguageIdParameter(new List<WSParameter>()
                {
                    new WSParameter("authKey", authKey.ToString()),
                }, languageId));

                wsCall.Execute(useApiResponse: true);

                if (wsCall.Response?.Content is ApiResponseDTO)
                {
                    try
                    {
                        var apiResponse = wsCall.Response?.Content as ApiResponseDTO;
                        if (apiResponse != null && apiResponse.Data is JToken)
                        {
                             return (apiResponse.Data as JToken).ToObject<Dictionary<string,string>>();
                        }
                    }
                    catch { }
                }
            }

            return null;
        }

        public static DataSourceType GetDataSourceType()
        {
            var authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
            if (authKey is string)
            {
                WebserviceCall wsCall = new WebserviceCall("GetCompanyDataSourceType", new List<WSParameter>()
                {
                    new WSParameter("authKey", authKey as string)
                });
                wsCall.Execute(useApiResponse: true);

                if (wsCall.Response?.Content is ApiResponseDTO)
                {
                    try
                    {
                        var apiResponse = wsCall.Response?.Content as ApiResponseDTO;

                        if (int.TryParse(apiResponse.Data.ToString(), out int intType)){
                            return (DataSourceType)intType;
                        }
                    }
                    catch { }
                }
            }
            return DataSourceType.None;
        }

        public static string GetHiddenRibbonButtons()
        {
            var authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
            if (authKey is string)
            {
                WebserviceCall wsCall = new WebserviceCall("GetHiddenRibbonButtons", new List<WSParameter>()
                {
                    new WSParameter("authKey", authKey as string)
                });
                wsCall.Execute(useApiResponse: true);

                if (wsCall.Response?.Content is ApiResponseDTO)
                {
                    try
                    {
                        var apiResponse = wsCall.Response?.Content as ApiResponseDTO;
                        return apiResponse.Data.ToString() ?? string.Empty;
                    }
                    catch { }
                }
            }
            return string.Empty;
        }
    }
}
