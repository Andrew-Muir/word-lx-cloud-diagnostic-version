﻿using InfowareVSTO.Common.DTO;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Office.Interop.Word;

namespace InfowareVSTO
{
    public class SettingsManager
    {
        private static SettingDTO settings;
        private const bool SETTINGS_ENABLED = true;

        public static string GetSetting(string key, string sectionName = null)
        {
            if (SETTINGS_ENABLED)
            {
                if (settings == null)
                {
                    settings = AdminPanelWebApi.GetAllCompanySettings();
                }

                if (key != null && settings != null)
                {
                    if (sectionName != null)
                    {
                        Dictionary<string, string> keyValues = settings.Sections.Where(x => x.SectionName == sectionName).FirstOrDefault()?.KeyValues;
                        if (keyValues != null)
                        {
                            string value = null;
                            if (keyValues.TryGetValue(key, out value))
                            {
                                return value;
                            }
                        }

                    }
                    else
                    {
                        foreach (Common.DTO.Section section in settings.Sections)
                        {
                            string value = null;
                            if (section.KeyValues.TryGetValue(key, out value))
                            {
                                return value;
                            }
                        }
                    }
                }
            }

            return null;
        }

        public static bool GetSettingAsBool(string key, string sectionName = null, bool defaultValue = false)
        {
            string str = GetSetting(key, sectionName);

            if (str != null)
            {
                return str.Trim().ToLower() == "true";
            }
            else
            {
                return defaultValue;
            }
        }

        public static int GetSettingAsInt(string key, string sectionName = null, int defaultValue = -1)
        {
            int result = defaultValue;
            string setting = GetSetting(key, sectionName);
            if (setting != null && int.TryParse(setting, out result))
            {
                return result;
            }

            return defaultValue;
        }

        public static WdColor GetPromptColor()
        {
            string colourStr = GetSetting("Colour", "General");
            int colorInt = 0;
            if (colourStr != null && int.TryParse(colourStr, out colorInt))
            {
                return  (WdColor)colorInt;
            }
        
            return WdColor.wdColorBlue;
        }
    }
}
