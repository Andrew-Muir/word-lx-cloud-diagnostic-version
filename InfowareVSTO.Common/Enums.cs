﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfowareVSTO.Common
{
    public enum TemplateType
    {
        BlankDocument = 0,
        Letter = 1,
        Memo = 2,
        Fax = 3,
        Labels = 4,
        Envelope = 5,
        Custom = 6,
        WordDA = 7,
        Letterhead = 8
    }

    public enum WordDefaultIconType
    {
        Information = 0,
        Question,
        Warning,
        Error
    }

    public enum UserAssignedRole
    {
        [Display(Name = "Unassigned")]
        Unnasigned = 0,
        [Display(Name = "Current Author")]
        CurrentAuthor,
        [Display(Name = "Current Assistant")]
        CurrentAssistant
    }

    public enum DocIDRemovalProviders
    {
        Enterprise = 0,
        Fasken = 1,
        Wildeboer = 2
    }

    public enum OutlookContactType
    {
        Own = 0,
        Shared
    }

    public enum DataFieldType
    {
        Text = 0,
        Date,
        Company,
        Firm,
        Client,
        Contact,
        CourtName
    }

    public enum RecipientType
    {
        SingleRow = 0,
        MultiRow = 1,
        NoJobTitle
    }

    public enum JobTitleVisibility
    {
        NoJobTitle = 0,
        SameRow = 1,
        NextRow = 2
    }

    public enum ButtonEventType
    {
        OK = 0,
        Cancel = 1
    }

    public enum TocLevelType
    {
        All = 0,
        Toc1,
        Toc2to9,
        None
    }

    public enum DraftDateStampLocation
    {
        Top = 0,
        Left = 1,
        Right = 2
    }

    public enum DraftDateStampDateType
    {
        NoDate = 0,
        LastSavedDate = 1,
        CurrentDate = 2
    }

    public enum OutlookSourceType
    {
        Personal = 0,
        CurrentAuthor = 1,
        OtherAccounts = 2,
        Shared = 3
    }

    public enum DataSourceType
    {
        None = 0,
        MatterSphere = 1,
        MatterMasterMSSQL = 2,
        MatterMasterMySql = 3
    }

    public enum DSPaneDTOFields
    {
        FileNumber,
        ClientFileNumber,
        Delivery,
        Handling,
        Attention,
        ReLine,
        Closing,
        NoOfPages,
    }

    public enum DSPaneDTOFieldsRecipient
    {
        Name,
        FirstName,
        LastName,
        Company,
        JobTitle,
        AddressLine1,
        AddressLine2,
        City,
        Province,
        PostalCode,
        Country,
        Fax,
        Email,
        Phone,
        Mobile
    }

    public enum DSPaneDTOFieldsCC
    {
        Name,
        FirstName,
        LastName
    }

    public enum DSGroupTypes
    {
        Other,
        Recipient,
        CC,
        BCC
    }
}
