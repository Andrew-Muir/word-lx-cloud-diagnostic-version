﻿using DocumentFormat.OpenXml.Office2013.PowerPoint.Roaming;
using InfowareVSTO.Common.DTOs;
using InfowareVSTO.Common.Models;
using Microsoft.Office.Core;
using Microsoft.Office.Interop.Word;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using Word = Microsoft.Office.Interop.Word;

namespace InfowareVSTO.Common.General
{
    public static class PaneUtils
    {
        private static List<Row> AddedRows = new List<Row>();
        private static List<string> FillAndHideGeneralContentControlTitles = new List<string> { "File Number", "Our File Number", "Client File Number", "Their File Number", "Delivery", "Handling", "Enclosure", "Courtesy Copy", "Blind Copy", "Attention" };
        private static List<string> FillAndHideLabelContentControlTitles = new List<string> { "Attention" };
        private static List<ContentControl> ContentControlsToHideWhenLineEmpty;
        private const string RECIP_TABLE_WIDTHS_PROP = "RecipTableWidth";
        private const string ENVELOPE_COPY_PAGE = "LX_Envelope_Page_Copy_";
        private const string LABELS_COPY_PAGE = "LX_Labels_Page_Copy_";

        public static object UsedContentControls { get; set; }

        public static void InsertGeneralTemplateDTO(Word.Document document, PaneDTO letterDTO, TemplateType? templateType = null, PaneSettings paneSettings = null, Range seletctionToRestore = null, string specialTag = null)
        {
            if (paneSettings == null)
            {
                paneSettings = new PaneSettings();
            }

            if (templateType == TemplateType.Envelope)
            {
                CopyEnvelopePage(document, letterDTO.Recipients);
            }
            int envelopeRecipientCount = 0;
            int envelopeNonAttention = 0;

            ContentControlsToHideWhenLineEmpty = new List<ContentControl>();
            bool showHiddenText = document.ActiveWindow.View.ShowHiddenText;
            document.ActiveWindow.View.ShowHiddenText = true;
            ACompanySimpleDTO currentCompany = GetCompoundCompany(letterDTO.Author, letterDTO.CompanyId, DocumentUtils.GetDocumentLanguage(document), letterDTO.OfficeLocationId);

            if (currentCompany != null && currentCompany.DefaultLocationId.HasValue)
            {
                if (letterDTO.Author != null)
                {
                    letterDTO.Author = MergeContactInformation(letterDTO.Author, currentCompany.DefaultLocationId.Value);
                }

                if (letterDTO.Assistant != null)
                {
                    letterDTO.Assistant = MergeContactInformation(letterDTO.Assistant, currentCompany.DefaultLocationId.Value);
                }
                letterDTO.OfficeLocation = currentCompany.City;
            }

            var contentControls = UsedContentControls as List<ContentControl> ?? DocumentUtils.GetAllContentControls(document, specialTag == null ? CustomConstants.TemplateTags : specialTag)?.ToList();
            string tags = "";

            bool hasAttentionLineCC = contentControls.Where(x => x.Title == "Attention").Count() > 0;
            var currentColumnLevels = new List<int>();

            if (AddedRows.Count > 0)
            {
                foreach (Row row in AddedRows)
                {
                    try
                    {
                        row.Delete();
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex);
                    }
                }
                AddedRows = new List<Row>();
            }

            string[] attentions = letterDTO?.Attention?.Trim()?.Split(new string[] { "\r\n", "\r", "\n"}, StringSplitOptions.RemoveEmptyEntries);

            if (letterDTO.IncludeAssistantBlock == true)
            {
                ShowAssistantBlock(contentControls);
            }

            bool fusesFxCustomRecipientBlock = false;
            if (templateType == TemplateType.Fax && contentControls?.Where(x => x.Title == CustomConstants.CUSTOM_RECIPIENT_BLOCK)?.FirstOrDefault() != null)
            {
                var list = contentControls?.Where(x => x.Title == CustomConstants.CUSTOM_RECIPIENT_BLOCK_COPY).ToList();

                foreach (ContentControl cc in list)
                {
                    cc.Delete(true);
                }

                var customRecipientBlock = contentControls?.Where(x => x.Title == CustomConstants.CUSTOM_RECIPIENT_BLOCK)?.FirstOrDefault();
                Range range = customRecipientBlock.Range;
                if (letterDTO.Tos?.Length > 1)
                {
                    range = CopyCustomRecipientBlock(document, customRecipientBlock, letterDTO.Tos.Length - 1);
                }

                FillFaxCustomRecipientBlock(range, letterDTO);
                fusesFxCustomRecipientBlock = true;
            }

            if (contentControls != null && contentControls.Count > 0)
            {
                foreach (Word.ContentControl cc in contentControls)
                {
                    tags += cc.Title + ";";
                    switch (cc.Title)
                    {
                        case "Company Name Rich Text":
                            bool isInClosing = false;
                            try
                            {
                                isInClosing = cc.Range.Paragraphs[1].get_Style()?.NameLocal == document.Styles[WdBuiltinStyle.wdStyleClosing].NameLocal;
                            }
                            catch { }

                            bool hide = isInClosing && !letterDTO.IncludeFirmName;

                            if (hide)
                            {
                                FillAndHide(cc, "");
                            }
                            else
                            {
                                FillAndHide(cc, "CN");
                                if (currentCompany != null)
                                {
                                    string xml = AdminPanelWebApi.GetCompanyNameRichText(currentCompany.Id, DocumentUtils.GetDocumentLanguage(document));
                                    cc.Range.Text = "";
                                    if (!string.IsNullOrWhiteSpace(xml))
                                    {
                                        DocumentUtils.InsertXMLWithoutExtraParagraph(cc, xml);
                                    }
                                    else
                                    {
                                        cc.Range.Text = currentCompany.CompanyName;
                                    }
                                }
                                else
                                {
                                    cc.Range.Text = " ";
                                }
                            }
                            break;
                        case "Company Logo":
                            if (cc.Range.InlineShapes.Count == 0)
                                try
                                {
                                    cc.Range.Text = " ";

                                    bool isSmall = letterDTO.Type == TemplateType.Envelope;
                                    var logoUrl = AdminPanelWebApi.GetCompanyLogoUrl(currentCompany.Id, isSmall);
                                    var pic = cc.Range.InlineShapes.AddPicture(logoUrl);

                                    if (pic != null)
                                    {
                                        float width = pic.Width;
                                        float height = pic.Height;
                                        float newWidth = isSmall ? 80 : 140;

                                        pic.Height = (float)Math.Round(newWidth * height / width);
                                        pic.Width = newWidth;
                                    }
                                    try
                                    {
                                        File.Delete(logoUrl);
                                    }
                                    catch { }
                                }
                                catch { }//do nothing, probably no logo uploaded yet
                            break;
                        case "Date":

                            string dateFormat = string.IsNullOrWhiteSpace(letterDTO.DateFormat) ? "MMMM d, yyyy" : letterDTO.DateFormat;
                            if (letterDTO.CultureId.IsEmpty())
                            {
                                cc.Range.Text = letterDTO.Date.ToString(dateFormat);
                            }
                            else
                            {
                                cc.Range.Text = letterDTO.Date.ToString(dateFormat, CultureInfo.CreateSpecificCulture(letterDTO.CultureId));
                            }

                            break;
                        case "Diary Date":
                            string diaryDateValue = "";
                            if (letterDTO?.DiaryDate != null)
                            {

                                dateFormat = string.IsNullOrWhiteSpace(letterDTO.DateFormat) ? "MMMM d, yyyy" : letterDTO.DateFormat;
                                if (letterDTO.CultureId.IsEmpty())
                                {
                                    diaryDateValue = letterDTO.DiaryDate.Value.ToString(dateFormat);
                                }
                                else
                                {
                                    diaryDateValue = letterDTO.DiaryDate.Value.ToString(dateFormat, CultureInfo.CreateSpecificCulture(letterDTO.CultureId));
                                }
                            }

                            FillAndHide(cc, diaryDateValue);
                            break;
                        case "Our File Number Tag":
                        case "File No. Tag":
                        case "File Number Tag":
                            var fileNumberTag = CustomConstants.FILE_NUMBER_TAG;
                            if (!string.IsNullOrEmpty(letterDTO.FileNumberTagSetting))
                            {
                                fileNumberTag = letterDTO.FileNumberTagSetting;
                            }
                            cc.Range.Text = fileNumberTag;
                            break;
                        case "Our File Number":
                        case "File Number":
                            FillAndHide(cc, letterDTO.FileNumber, false, templateType == TemplateType.Memo);
                            break;
                        case "Their File Number":
                        case "Client File Number":
                            FillAndHide(cc, letterDTO.ClientFileNumber, false, templateType == TemplateType.Memo);
                            break;
                        case "No. of Pages":
                            cc.Range.Text = letterDTO.NoOfPages?.ToString().SpaceIfEmpty();
                            break;

                        case "Delivery":
                            var delivery = letterDTO.Delivery;
                            if (!string.IsNullOrEmpty(letterDTO.IncludeContactInformationDeliverySetting) && !string.IsNullOrWhiteSpace(delivery) && letterDTO.AddedRecipients.Count > 0)
                            {
                                var firstRecipient = letterDTO.AddedRecipients.FirstOrDefault();
                                if (firstRecipient != null)
                                {
                                    switch (letterDTO.IncludeContactInformationDeliverySetting)
                                    {
                                        case "E":
                                            var email = BaseUtils.GetContactFirstInformation(firstRecipient.ContactInformations, "Email");
                                            if (email != null && !string.IsNullOrWhiteSpace(letterDTO.Delivery))
                                            {
                                                delivery += " " + email;
                                            }
                                            break;
                                        case "F":
                                            var fax = BaseUtils.GetContactFirstInformation(firstRecipient.ContactInformations, "Fax");
                                            if (fax != null && !string.IsNullOrWhiteSpace(letterDTO.Delivery))
                                            {
                                                delivery += " " + fax;
                                            }
                                            break;
                                    }
                                }
                            }
                            if (!string.IsNullOrWhiteSpace(delivery) && letterDTO.DeliveryAllCaps)
                            {
                                delivery = delivery.ToUpper();
                            }
                            FillAndHide(cc, delivery);
                            break;
                        case "Handling":
                            var handling = letterDTO.Handling;
                            if (!string.IsNullOrWhiteSpace(handling) && letterDTO.HandlingAllCaps)
                            {
                                handling = handling.ToUpper();
                            }
                            FillAndHide(cc, handling);
                            break;
                        case "Address(es)":
                        case "Recipient(s)":
                            if (templateType == TemplateType.Envelope)
                            {
                                var recipentEnvelopeArr = letterDTO.Recipients.Trim().Split(new string[] { "\r\n\r\n", "\r\r", "\n\n", "\v\v" }, StringSplitOptions.RemoveEmptyEntries);
                                if (recipentEnvelopeArr.Length > envelopeRecipientCount)
                                {
                                    cc.Range.Text = recipentEnvelopeArr[envelopeRecipientCount];

                                    List<SelectedContact> selectedContacts = new List<SelectedContact>(letterDTO.AddedRecipients);
                                    BaseUtils.RemoveRecipientsNotInTextBox(selectedContacts, recipentEnvelopeArr[envelopeRecipientCount]);
                                    if (selectedContacts.Count > 0)
                                    {
                                        envelopeNonAttention++;
                                    }
                                    else if (attentions != null && (attentions.Length != 1 || !hasAttentionLineCC) && envelopeRecipientCount < attentions.Length + envelopeNonAttention)
                                    {
                                        AddAttentionLineParagraphToEndOfRange(cc.Range, attentions[envelopeRecipientCount - envelopeNonAttention], false, letterDTO.AttentionTranslation);
                                    }
                                }
                                else
                                {
                                    cc.Range.Text = " ";
                                }
                                envelopeRecipientCount++;

                                // AddAttentionLineToNonTableRecipients(letterDTO, attentions, cc, hasAttentionLineCC, "\f", "\v\f");
                            }
                            else if (templateType == TemplateType.Letter || templateType == TemplateType.Custom)
                            {
                                InsertLetterRecipients(document, letterDTO, paneSettings, hasAttentionLineCC, attentions, cc);
                            }
                            else
                            {
                                if (letterDTO.RecipientsArr == null || letterDTO.RecipientsArr.Length == 0)
                                    cc.Range.Text = letterDTO.Recipients.Trim().SpaceIfEmpty();
                                else
                                    cc.Range.Text = string.Join("\f", letterDTO.RecipientsArr).Trim();
                            }
                            break;

                        case "To (Full Name)":
                            string valueToInsert = string.Empty;
                            if (templateType != TemplateType.Fax || letterDTO.Tos == null || letterDTO.Tos.Length == 0)
                            {
                                string firstLineRecipients = letterDTO.Recipients.Trim().Split(new string[] { "\r\n", "\r", "\n", "\v" }, StringSplitOptions.RemoveEmptyEntries).FirstOrDefault();
                                List<SelectedContact> selectedContacts = new List<SelectedContact>(letterDTO.AddedRecipients);
                                BaseUtils.RemoveRecipientsNotInTextBox(selectedContacts, firstLineRecipients);
                                if (selectedContacts.Count == 0 && attentions?.FirstOrDefault() != null)
                                {
                                    firstLineRecipients = attentions.FirstOrDefault() ?? firstLineRecipients;
                                }

                                valueToInsert = firstLineRecipients?.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).FirstOrDefault();
                            }
                            else
                            {
                                valueToInsert = letterDTO.Tos[0].DispayName;
                            }

                            FillAndHide(cc, valueToInsert);

                            break;

                        case "Salutation":
                            cc.Range.Text = letterDTO.Salutation.SpaceIfEmpty();
                            break;
                        case "Re Line":
                            cc.Range.Text = ReplaceParagraphBreaksWithLineBreaks(letterDTO.ReLine?.SpaceIfEmpty()) ?? " ";
                            break;
                        case "Closing":
                            var closing = letterDTO.Closing;
                            if (!string.IsNullOrWhiteSpace(letterDTO.Closing))
                            {
                                closing += letterDTO.ClosingEndChar;
                            }
                            cc.Range.Text = closing.SpaceIfEmpty();
                            break;
                        case "Full Name":
                            try
                            {


                                if (letterDTO.InformOptions?.IncludeBlankLineCompanyAddressLawyer == true)
                                {
                                    List<string> companyFields = new List<string>();
                                    companyFields.Add("Company Logo");
                                    companyFields.Add("Company Name Rich Text");
                                    companyFields.Add("Company Name");
                                    companyFields.Add("Company Description");
                                    companyFields.Add("Company Address");
                                    companyFields.Add("Company Address (Graphic)");
                                    companyFields.Add("Company City");
                                    companyFields.Add("Company Province");
                                    companyFields.Add("Company Postal Code");
                                    companyFields.Add("Company Phone");
                                    companyFields.Add("Company Fax");
                                    companyFields.Add("Company Website");

                                    Range previousLine = GetPreviousLine(cc.Range);
                                    if (previousLine != null && ContainsContentControl(previousLine, companyFields))
                                    {
                                        previousLine.Collapse(WdCollapseDirection.wdCollapseEnd);
                                        previousLine.Text = "\v";
                                    }
                                }
                                cc.Range.Text = (string.IsNullOrEmpty(letterDTO.Author.Prefix) ? "" : (letterDTO.Author.Prefix + " ")) + letterDTO.Author.FirstName + " "
                                    + letterDTO.Author.MiddleName + (string.IsNullOrEmpty(letterDTO.Author.MiddleName) ? "" : " ")
                                    + letterDTO.Author.LastName + (string.IsNullOrEmpty(letterDTO.Author.Suffix) ? "" : (" " + letterDTO.Author.Suffix));
                                if (letterDTO.InformOptions?.LawyerNameBoldface == true)
                                {
                                    cc.Range.Font.Bold = -1;
                                }
                            }
                            catch (Exception ex)
                            {
                            }
                            break;
                        case "Title":
                            FillAndHideWhenLineEmpty(cc, letterDTO.Author.JobTitle.SpaceIfEmpty());
                            break;
                        case "Initials":
                            if (letterDTO.Initials?.IsEmpty() == false)
                                cc.Range.Text = letterDTO.Initials;
                            else
                                cc.Range.Text = GetAuthorInitials_Letter(letterDTO.Author).SpaceIfEmpty();
                            break;
                        case "Assistant Name":
                            try
                            {
                                cc.Range.Text = letterDTO.Assistant.FirstName + " "
                                    + letterDTO.Assistant.MiddleName + (string.IsNullOrEmpty(letterDTO.Assistant.MiddleName) ? "" : " ")
                                    + letterDTO.Assistant.LastName;
                            }
                            catch (Exception ex)
                            {
                            }
                            break;
                        case "Assistant's Initials":
                            string assistantInitials = GetAuthorInitials_Letter(letterDTO.Assistant)?.ToLower();
                            Range slashRange = GetAssistantIntialsSlashRange(cc, document);
                            if (!string.IsNullOrEmpty(assistantInitials))
                            {
                                cc.Range.Text = assistantInitials;
                                if (slashRange != null)
                                {
                                    slashRange.Font.Hidden = 0;
                                }
                            }
                            else
                            {
                                cc.Range.Text = " ";
                                if (slashRange != null)
                                {
                                    slashRange.Font.Hidden = -1;
                                }
                            }
                            break;
                        case "Enclosure":
                            var enclosure = "";
                            if (!string.IsNullOrEmpty(letterDTO.Enclosures))
                            {
                                if (letterDTO.Number > 0)
                                {
                                    enclosure = string.Format("{0} ({1})", letterDTO.Enclosures, letterDTO.Number);
                                }
                                else
                                {
                                    enclosure = letterDTO.Enclosures;
                                }
                            }

                            FillAndHide(cc, enclosure);
                            break;
                        case "Courtesy Copy":
                            FillAndHide(cc, letterDTO.CC, false, true);
                            break;
                        case "Blind Copy":
                            FillAndHide(cc, letterDTO.BCC, true, true);
                            break;
                        case "Company Description":
                            cc.Range.Text = currentCompany?.Description?.SpaceIfEmpty() ?? " ";
                            break;
                        case "Company":
                            cc.Range.Text = (currentCompany != null && currentCompany?.CompanyName != null) ? currentCompany.CompanyName.SpaceIfEmpty() : " ";
                            break;
                        case "Company Name":
                            isInClosing = false;
                            try
                            {
                                isInClosing = cc.Range.Paragraphs[1].get_Style()?.NameLocal == document.Styles[WdBuiltinStyle.wdStyleClosing].NameLocal;
                            }
                            catch { }
                            string companyNameValue = "";
                            if ((!isInClosing || letterDTO.IncludeFirmName) && currentCompany != null)
                                companyNameValue = currentCompany.CompanyName.SpaceIfEmpty();
                            if (letterDTO.InformOptions?.CompanyNameAllCaps == true)
                            {
                                companyNameValue = companyNameValue.ToUpper();
                            }
                            FillAndHideWhenLineEmpty(cc, companyNameValue);
                            if (letterDTO.InformOptions?.CompanyNameBoldface == true)
                            {
                                cc.Range.Font.Bold = -1;
                            }
                            break;
                        case "Company Address":
                            cc.Range.Text = currentCompany?.Address?.SpaceIfEmpty() ?? " ";
                            break;
                        case "Company Address 2":
                            FillAndHide(cc, currentCompany?.Address2);
                            break;
                        case "Company Address (Graphic)":
                            try
                            {
                                if (cc.Range.InlineShapes.Count > 0)
                                {
                                    DeleteInlineShapes(cc.Range);
                                }
                                if (cc.Range.InlineShapes.Count == 0 && currentCompany.DefaultLocationId.HasValue)
                                {
                                    cc.Range.Text = " ";

                                    bool isSmall = letterDTO.Type == TemplateType.Envelope;
                                    var addressGraphicUrl = AdminPanelWebApi.GetCompanyLocationAddressGraphicUrl(currentCompany.DefaultLocationId.Value, currentCompany.Id);
                                    if (!addressGraphicUrl.IsEmpty())
                                    {
                                        var pic = cc.Range.InlineShapes.AddPicture(addressGraphicUrl);
                                        try
                                        {
                                            File.Delete(addressGraphicUrl);
                                        }
                                        catch { }
                                    }
                                }
                            }
                            catch { }//do nothing, probably no logo uploaded yet
                            break;
                        case "Company City":
                            cc.Range.Text = currentCompany?.City?.SpaceIfEmpty() ?? " ";
                            break;
                        case "Company Province":
                            cc.Range.Text = currentCompany?.Province?.SpaceIfEmpty() ?? " ";
                            break;
                        case "Company Postal Code":
                            cc.Range.Text = currentCompany?.PostalCode?.SpaceIfEmpty() ?? " ";
                            break;
                        case "Company Phone":
                            cc.Range.Text = currentCompany?.CompanyPhone?.SpaceIfEmpty() ?? " ";
                            break;
                        case "Company Fax":
                            cc.Range.Text = currentCompany?.CompanyFax?.SpaceIfEmpty() ?? " ";
                            break;
                        case "Company Website":
                            cc.Range.Text = currentCompany?.CompanyWebsite?.SpaceIfEmpty() ?? " ";
                            break;
                        case "Degrees":
                            FillAndHideWhenLineEmpty(cc, ReplaceParagraphBreaksWithLineBreaks(letterDTO.Author.Credentials.SpaceIfEmpty()));
                            break;
                        case "User1":
                            FillAndHideWhenLineEmpty(cc, ReplaceParagraphBreaksWithLineBreaks(letterDTO.Author.User1.SpaceIfEmpty()));
                            break;
                        case "User2":
                            FillAndHideWhenLineEmpty(cc, ReplaceParagraphBreaksWithLineBreaks(letterDTO.Author.User2.SpaceIfEmpty()));
                            break;
                        case "User3":
                            FillAndHideWhenLineEmpty(cc, ReplaceParagraphBreaksWithLineBreaks(letterDTO.Author.User3.SpaceIfEmpty()));
                            break;
                        case "Certifications":
                            FillAndHideWhenLineEmpty(cc, ReplaceParagraphBreaksWithLineBreaks(letterDTO.Author.Certification.SpaceIfEmpty()));
                            break;
                        case "Direct Telephone":
                            if (letterDTO.InformOptions?.IncludeBlankLineLawyerPhoneNumber == true)
                            {
                                Range previousLine = GetPreviousLine(cc.Range);
                                if (previousLine != null && ContainsContentControl(previousLine, "Full Name")) { }
                                {
                                    previousLine.Collapse(WdCollapseDirection.wdCollapseEnd);
                                    previousLine.Text = "\v";
                                }
                            }

                            if (letterDTO.InformOptions?.UseCompanyPhoneAndFax == true)
                            {
                                cc.Range.Text = currentCompany?.CompanyPhone?.SpaceIfEmpty() ?? " ";
                            }
                            else
                            {
                                cc.Range.Text = BaseUtils.GetContactFirstInformation(letterDTO.Author?.ContactInformations, "Phone").SpaceIfEmpty();
                            }
                            break;
                        case "Mobile Phone":
                            cc.Range.Text = BaseUtils.GetContactFirstInformation(letterDTO.Author?.ContactInformations, "Mobile").SpaceIfEmpty();
                            break;
                        case "Direct Fax":
                            if (letterDTO.InformOptions?.PhoneAndFaxNumberOnOneLine == true)
                            {
                                Range previousLine = GetPreviousLine(cc.Range);
                                if (previousLine != null && ContainsContentControl(previousLine, "Direct Telephone")) { }
                                {
                                    previousLine.Collapse(WdCollapseDirection.wdCollapseEnd);
                                    previousLine.MoveStart(WdUnits.wdCharacter, -1);
                                    previousLine.Text = "\t";
                                }
                            }

                            string directFax = BaseUtils.GetContactFirstInformation(letterDTO.Author?.ContactInformations, "Fax").SpaceIfEmpty();
                            if (letterDTO.InformOptions?.UseCompanyPhoneAndFax == true || (letterDTO.InformOptions != null && string.IsNullOrWhiteSpace(directFax))
                                || letterDTO.InformOptions?.UseOnlyCompanyFax == true)
                            {
                                cc.Range.Text = currentCompany?.CompanyFax?.SpaceIfEmpty() ?? " ";
                            }
                            else
                            {
                                cc.Range.Text = directFax?.SpaceIfEmpty() ?? " ";
                            }
                            break;
                        case "Email":

                            cc.Range.Text = BaseUtils.GetContactFirstInformation(letterDTO.Author?.ContactInformations, "Email").SpaceIfEmpty();
                            if (letterDTO.InformOptions?.IncludeEmailAddress == false)
                            {
                                FillAndHide(cc, "");
                            }
                            break;
                        case "Lawyer ID":
                            cc.Range.Text = letterDTO.Author?.LawyerNumber?.SpaceIfEmpty();
                            break;
                        case "Assistant Phone":
                            cc.Range.Text = BaseUtils.GetContactFirstInformation(letterDTO.Assistant?.ContactInformations, "Phone").SpaceIfEmpty();
                            break;
                        case "Assistant Email":
                            cc.Range.Text = BaseUtils.GetContactFirstInformation(letterDTO.Assistant?.ContactInformations, "Email").SpaceIfEmpty();
                            break;
                        case "Assistant Title":
                            FillAndHideWhenLineEmpty(cc, letterDTO.Assistant?.JobTitle.SpaceIfEmpty());
                            break;
                        case "Office Location":
                            cc.Range.Text = letterDTO.OfficeLocation.SpaceIfEmpty();
                            break;
                        case "Include Per":
                            cc.Range.Text = letterDTO.IncludePer ? (string.IsNullOrWhiteSpace(letterDTO.ClosingPerText) ? "Per:" : letterDTO.ClosingPerText) : " ";
                            break;
                        case "To":
                        case "To (Company)":
                        case "To (Fax Number)":
                        case "To (Phone Number)":
                            if (!fusesFxCustomRecipientBlock)
                            {
                                if (letterDTO.Tos.Length > 0)
                                {
                                    if (cc.Range.Tables.Count == 1 && cc.Range.Tables[1] != null)
                                    {
                                        if (!currentColumnLevels.Contains(cc.Range.Cells[1].RowIndex))
                                        {
                                            for (var i = 1; i < letterDTO.Tos.Length; ++i)
                                            {
                                                var row = cc.Range.Tables[1].Rows.Add();
                                                AddedRows.Add(row);
                                            }

                                            currentColumnLevels.Add(cc.Range.Cells[1].RowIndex);
                                        }

                                        if (letterDTO.Tos.Length > 1)
                                        {
                                            if (cc.Title == "To")
                                            {
                                                cc.Range.Text = letterDTO.Tos[0].DispayName;
                                            }
                                            else if (cc.Title == "To (Company)")
                                            {
                                                cc.Range.Text = GetCompanyNameOfRecipient(letterDTO.Tos[0]);
                                            }
                                            else if (cc.Title == "To (Fax Number)")
                                            {
                                                cc.Range.Text = GetFaxOfRecipient(letterDTO.Tos[0]);
                                            }
                                            else if (cc.Title == "To (Phone Number)")
                                            {
                                                cc.Range.Text = GetPhoneOfRecipient(letterDTO.Tos[0]);
                                            }

                                            for (var i = 1; i < letterDTO.Tos.Length; ++i)
                                            {
                                                var text = string.Empty;
                                                if (cc.Title == "To")
                                                {
                                                    text = letterDTO.Tos[i].DispayName;
                                                }
                                                else if (cc.Title == "To (Company)")
                                                {
                                                    text = GetCompanyNameOfRecipient(letterDTO.Tos[i]);
                                                }
                                                else if (cc.Title == "To (Fax Number)")
                                                {
                                                    text = GetFaxOfRecipient(letterDTO.Tos[i]);
                                                }
                                                else if (cc.Title == "To (Phone Number)")
                                                {
                                                    text = GetPhoneOfRecipient(letterDTO.Tos[i]);
                                                }

                                                if (!string.IsNullOrEmpty(text))
                                                {
                                                    if (string.IsNullOrEmpty(cc.Range.Tables[1].Cell(cc.Range.Cells[1].RowIndex + i, cc.Range.Cells[1].ColumnIndex).Range.Text) || cc.Range.Tables[1].Cell(cc.Range.Cells[1].RowIndex + i, cc.Range.Cells[1].ColumnIndex).Range.Text == "\r\a")
                                                    {
                                                        cc.Range.Tables[1].Cell(cc.Range.Cells[1].RowIndex + i, cc.Range.Cells[1].ColumnIndex).Range.Text = text;
                                                    }
                                                    else
                                                    {
                                                        var paragraph = cc.Range.Tables[1].Cell(cc.Range.Cells[1].RowIndex + i, cc.Range.Cells[1].ColumnIndex).Range.Paragraphs.Add();
                                                        paragraph.Range.Text = text;
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (cc.Title == "To")
                                            {
                                                cc.Range.Text = letterDTO.Tos[0].DispayName;
                                            }
                                            else if (cc.Title == "To (Company)")
                                            {
                                                cc.Range.Text = GetCompanyNameOfRecipient(letterDTO.Tos[0]);
                                            }
                                            else if (cc.Title == "To (Fax Number)")
                                            {
                                                cc.Range.Text = GetFaxOfRecipient(letterDTO.Tos[0]);
                                            }
                                            else if (cc.Title == "To (Phone Number)")
                                            {
                                                cc.Range.Text = GetPhoneOfRecipient(letterDTO.Tos[0]);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        var updateList = new List<string>();
                                        foreach (var to in letterDTO.Tos)
                                        {
                                            if (cc.Title == "To")
                                            {

                                                updateList.Add(to.DispayName);

                                            }
                                            else if (cc.Title == "To (Company)")
                                            {
                                                var companyName = GetCompanyNameOfRecipient(to);
                                                if (!string.IsNullOrEmpty(companyName))
                                                {
                                                    updateList.Add(companyName);
                                                }
                                            }
                                            else if (cc.Title == "To (Fax Number)")
                                            {
                                                var faxNumber = GetFaxOfRecipient(to);
                                                if (!string.IsNullOrEmpty(faxNumber))
                                                {
                                                    updateList.Add(faxNumber);
                                                }
                                            }
                                            else if (cc.Title == "To (Phone Number)")
                                            {
                                                var phoneNumber = GetPhoneOfRecipient(to);
                                                if (!string.IsNullOrEmpty(phoneNumber))
                                                {
                                                    updateList.Add(phoneNumber);
                                                }
                                            }
                                        }

                                        if (updateList.Count > 0)
                                        {
                                            cc.Range.Text = string.Join("\v", updateList);
                                        }
                                    }
                                }
                                else
                                {
                                    if (cc.Title == "To")
                                    {
                                        cc.Range.Text = ReplaceParagraphBreaksWithLineBreaks(letterDTO.Recipients)?.SpaceIfEmpty() ?? " ";
                                    }
                                    else if (cc.Title == "To (Company)")
                                    {
                                        cc.Range.Text = letterDTO.Firm?.SpaceIfEmpty() ?? " ";
                                    }
                                    else if (cc.Title == "To (Fax Number)")
                                    {
                                        cc.Range.Text = letterDTO.FaxNumber?.SpaceIfEmpty() ?? " ";
                                    }
                                    else if (cc.Title == "To (Phone Number)")
                                    {
                                        cc.Range.Text = letterDTO.PhoneNumber?.SpaceIfEmpty() ?? " ";
                                    }
                                }
                            }
                            break;
                        case "Assistant's Name":
                            cc.Range.Text = letterDTO.Assistant?.FirstName + " " + letterDTO.Assistant?.LastName;
                            break;
                        case "Assistant's Phone Number":
                            cc.Range.Text = BaseUtils.GetContactFirstInformation(letterDTO.Assistant?.ContactInformations, "Phone").SpaceIfEmpty();
                            break;
                        case "Attention":
                            if (attentions?.Length == 1)
                            {
                                FillAndHide(cc, attentions[0]);
                            }
                            else
                            {
                                FillAndHide(cc, " ", true);
                            }
                            break;
                        case "Author's Initials":
                            if (!letterDTO.Initials.IsEmpty())
                                cc.Range.Text = letterDTO.Initials;
                            else
                                cc.Range.Text = GetAuthorInitials_Letter(letterDTO.Author).SpaceIfEmpty();
                            break;
                        case "Law Society Number Label":
                            var lawSocietyNrLabel = CustomConstants.LAW_SOC_NR_LABEL;
                            if (!string.IsNullOrEmpty(letterDTO.LawSocietyNumberLabel))
                            {
                                lawSocietyNrLabel = letterDTO.LawSocietyNumberLabel;
                            }
                            cc.Range.Text = lawSocietyNrLabel;
                            break;
                        default:
                            string unknownTitle = cc.Title;
                            break;
                    }
                }
            }

            RefreshContentContols(document);
            HideAllContentControlsToHideWhenLineEmpty();
            BaseUtils.UnhideRow(contentControls, FillAndHideGeneralContentControlTitles);
            BaseUtils.HideUnhideSeparator(contentControls);
            if (letterDTO.IncludeAssistantBlock == false)
            {
                HideAssistantBlock(contentControls);
            }
            document.ActiveWindow.View.ShowHiddenText = showHiddenText;
        }

        private static void ConvertTablesInCCToTextAndDeleteText(Document document, ContentControl cc)
        {
            Range start = cc.Range;
            start.Collapse(WdCollapseDirection.wdCollapseStart);
            start.MoveStart(WdUnits.wdCharacterFormatting, -1);
            start.MoveEnd(WdUnits.wdCharacterFormatting, -1);

            Range end = cc.Range;
            end.Collapse(WdCollapseDirection.wdCollapseEnd);
            end.MoveEnd(WdUnits.wdCharacterFormatting, 1);
            end.MoveStart(WdUnits.wdCharacterFormatting, 1);

            ConvertTablesInRangeToText(cc.Range);

            Range newStart = cc.Range;
            newStart.Collapse(WdCollapseDirection.wdCollapseStart);
            newStart.MoveStart(WdUnits.wdCharacterFormatting, -1);
            newStart.MoveEnd(WdUnits.wdCharacterFormatting, -1);

            Range newEnd = cc.Range;
            newEnd.Collapse(WdCollapseDirection.wdCollapseEnd);
            newEnd.MoveEnd(WdUnits.wdCharacterFormatting, 1);
            newEnd.MoveStart(WdUnits.wdCharacterFormatting, 1);

            try
            {
                if (start.Start < newStart.Start)
                {
                    document.Range(start.Start, newStart.Start).Delete();
                }
            }
            catch (Exception ex)
            {
            }

            try
            {
                if (newEnd.End < end.End)
                {
                    document.Range(newEnd.End, end.End).Delete();
                }
            }
            catch (Exception ex)
            {
            }
        }

        public static void InsertLetterRecipients(Document document, PaneDTO letterDTO, PaneSettings paneSettings, bool hasAttentionLineCC, string[] attentions, ContentControl cc)
        {
            string[] recipients = null;

            try
            {
                ConvertTablesInCCToTextAndDeleteText(document, cc);

                cc.Range.Text = "";
                cc.Range.ParagraphFormat.LineSpacingRule = WdLineSpacing.wdLineSpaceSingle;
                cc.Range.ParagraphFormat.SpaceAfter = 8;

                try
                {
                    cc.Range.Paragraphs.First.set_Style("Address");
                }
                catch (Exception ex)
                {
                    cc.Range.Paragraphs.First.set_Style(WdBuiltinStyle.wdStyleNormal);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }

            if (letterDTO.RecipientsArr == null || letterDTO.RecipientsArr.Length == 0)
            {
                recipients = letterDTO.Recipients.Trim().Split(new string[] { "\r\n\r\n", "\r\r", "\n\n", "\v\v" }, StringSplitOptions.RemoveEmptyEntries);
            }
            else
            {
                recipients = letterDTO.RecipientsArr;
            }

            if (recipients.Length >= paneSettings.MinRecipientCountForHorizontal)
            {
                try
                {
                    int rows = (recipients.Length - 1) / paneSettings.HorizontalRecipientsColumnCount + 1;
                    ContentControl contentControl = cc;
                    Word.Table table = null;
                    try
                    {
                        table = cc.Range.Tables.Add(cc.Range, rows, paneSettings.HorizontalRecipientsColumnCount);
                    }
                    catch
                    {
                        contentControl = DeleteCCAndReAdd(cc);
                        table = contentControl.Range.Tables.Add(contentControl.Range, rows, paneSettings.HorizontalRecipientsColumnCount);
                    }

                    table.Borders.Enable = 0;
                    table.Rows.LeftIndent = -1 * table.LeftPadding;

                    List<float?> tableColumnsWidths = GetRecipTableWidths(document);
                    if (tableColumnsWidths != null)
                    {
                        for (int k = 0; k < tableColumnsWidths.Count; k++)
                        {
                            if (tableColumnsWidths[k] != null && table.Columns.Count > k)
                            {
                                table.Columns[k + 1].PreferredWidthType = WdPreferredWidthType.wdPreferredWidthPoints;
                                table.Columns[k + 1].PreferredWidth = tableColumnsWidths[k].Value;
                            }
                        }
                    }

                    int i = 0;
                    int nonAttention = 0;
                    foreach (string recipient in recipients)
                    {
                        int row = i / paneSettings.HorizontalRecipientsColumnCount + 1;
                        int column = i % paneSettings.HorizontalRecipientsColumnCount + 1;
                        Word.Cell cell = table.Cell(row, column);
                        if (row > 1)
                        {
                            cell.TopPadding = 12;
                        }
                        cell.Range.Paragraphs.Alignment = WdParagraphAlignment.wdAlignParagraphLeft;
                        cell.Range.Text = recipient;
                        List<SelectedContact> selectedContacts = new List<SelectedContact>(letterDTO.AddedRecipients);
                        BaseUtils.RemoveRecipientsNotInTextBox(selectedContacts, recipient);
                        if (selectedContacts.Count > 0)
                        {
                            nonAttention++;
                        }
                        else if ((attentions.Length != 1 || !hasAttentionLineCC) && i < attentions.Length + nonAttention)
                        {
                            AddAttentionLineParagraphToEndOfRange(cell.Range, attentions[i - nonAttention], true, letterDTO.AttentionTranslation);
                        }

                        cell.TopPadding = 0;
                        i++;
                    }

                    TrimEmptyParagraphs(cc, table);
                }
                catch { };
            }
            else
            {
                try
                {
                    if (letterDTO.RecipientsArr == null || letterDTO.RecipientsArr.Length == 0)
                    {
                        cc.Range.Text = letterDTO.Recipients.Trim().SpaceIfEmpty();
                        AddAttentionLineToNonTableRecipients(letterDTO, attentions, cc, hasAttentionLineCC);
                    }
                    else
                    {
                        cc.Range.Text = string.Join("\f", letterDTO.RecipientsArr).Trim();
                    }

                    cc.Range.Paragraphs.Alignment = WdParagraphAlignment.wdAlignParagraphLeft;
                }
                catch
                {
                    ContentControl newcc = DeleteCCAndReAdd(cc);

                    if (letterDTO.RecipientsArr == null || letterDTO.RecipientsArr.Length == 0)
                    {
                        newcc.Range.Text = letterDTO.Recipients.Trim().SpaceIfEmpty();
                        AddAttentionLineToNonTableRecipients(letterDTO, attentions, newcc, hasAttentionLineCC);
                    }
                    else
                    {
                        newcc.Range.Text = string.Join("\f", letterDTO.RecipientsArr).Trim();
                    }

                    newcc.Range.Paragraphs.Alignment = WdParagraphAlignment.wdAlignParagraphLeft;
                }
            }
        }

        private static void FillFaxCustomRecipientBlock(Range range, PaneDTO letterDTO)
        {
            var list = range.ContentControls.Cast<ContentControl>().ToList();
            StringCounter stringCounter = new StringCounter();

            if (letterDTO.Tos.Length > 0)
            {
                foreach (ContentControl cc in list)
                {
                    switch (cc.Title)
                    {
                        case "To":
                            try
                            {
                                FillAndHideCCOnly(cc, letterDTO.Tos[stringCounter.GetStringCount("To")].DispayName.SpaceIfEmpty());
                                stringCounter.AddString("To");
                            }
                            catch (Exception ex)
                            {
                            }
                            break;
                        case "To (Company)":
                            try
                            {
                                FillAndHideCCOnly(cc, GetCompanyNameOfRecipient(letterDTO.Tos[stringCounter.GetStringCount("To (Company)")]).SpaceIfEmpty());
                                stringCounter.AddString("To (Company)");
                            }
                            catch (Exception ex)
                            {
                            }
                            break;
                        case "To (Fax Number)":
                            try
                            {
                                FillAndHideCCOnly(cc, GetFaxOfRecipient(letterDTO.Tos[stringCounter.GetStringCount("To (Fax Number)")]).SpaceIfEmpty());
                                stringCounter.AddString("To (Fax Number)");
                            }
                            catch (Exception ex)
                            {
                            }
                            break;
                        case "To (Phone Number)":
                            try
                            {
                                FillAndHideCCOnly(cc, GetPhoneOfRecipient(letterDTO.Tos[stringCounter.GetStringCount("To (Phone Number)")]).SpaceIfEmpty());
                                stringCounter.AddString("To (Phone Number)");
                            }
                            catch (Exception ex)
                            {
                            }
                            break;
                    }
                }
            }
            else
            {
                foreach (ContentControl cc in list)
                {
                    switch (cc.Title)
                    {
                        case "To":
                            FillAndHideCCOnly(cc, ReplaceParagraphBreaksWithLineBreaks(letterDTO.Recipients)?.SpaceIfEmpty());
                            break;
                        case "To (Company)":
                            FillAndHideCCOnly(cc, letterDTO.Firm?.SpaceIfEmpty());
                            break;
                        case "To (Fax Number)":
                            FillAndHideCCOnly(cc, letterDTO.FaxNumber?.SpaceIfEmpty());
                            break;
                        case "To (Phone Number)":
                            FillAndHideCCOnly(cc, letterDTO.PhoneNumber?.SpaceIfEmpty());
                            break;
                    }
                }
            }
        }

        public static Range CopyCustomRecipientBlock(Document doc, ContentControl cc, int repeats)
        {
            Range range = cc.Range;
            range.MoveStart(WdUnits.wdCharacterFormatting, -1);
            range.MoveEnd(WdUnits.wdCharacterFormatting, 1);
            Range startRange = range.Duplicate;
            startRange.Collapse(WdCollapseDirection.wdCollapseStart);
            Range endRange = range.Duplicate;
            endRange.Collapse(WdCollapseDirection.wdCollapseEnd);
            range.Copy();

            Range nextRange = endRange.Duplicate;
            nextRange.MoveEnd(1);
            if (range.Text.EndsWith("\r") || range.Text.EndsWith("\n") || nextRange.Text == "\r" || nextRange.Text == "\r\n")
            {
                if (range.Paragraphs.Last.Next() == null)
                {
                    endRange.Text = "\r";
                    endRange.Collapse(WdCollapseDirection.wdCollapseEnd);
                }
                else
                {
                    endRange.Move(WdUnits.wdCharacter, 1);
                }
            }

            for (int i = 0; i < repeats; i++)
            {
                endRange.Paste();
                RenameContentControls(endRange, CustomConstants.CUSTOM_RECIPIENT_BLOCK, CustomConstants.CUSTOM_RECIPIENT_BLOCK_COPY);
                endRange.Collapse(WdCollapseDirection.wdCollapseEnd);
            }

            return doc.Range(startRange.Start, endRange.End);
        }

        private static void RenameContentControls(Range range, string from, string to)
        {
            var list = range.ContentControls.Cast<ContentControl>().Where(x => x.Title == from).ToList();

            foreach (ContentControl cc in list)
            {
                cc.Title = to;
            }
        }

        private static void CopyEnvelopePage(Document document, string recipients)
        {
            foreach (Bookmark bookmark in document.Bookmarks)
            {
                if (bookmark.Name.StartsWith(ENVELOPE_COPY_PAGE))
                {
                    bookmark.Range.Delete();
                }
            }

            int nrOfRecipients = recipients?.Trim()?.Split(new string[] { "\r\n\r\n", "\r\r", "\n\n", "\v\v" }, StringSplitOptions.RemoveEmptyEntries)?.Length ?? 0;

            Range rangeToCopy = document.Content;
            rangeToCopy.MoveEnd(WdUnits.wdCharacter, -1);
            rangeToCopy.Copy();
            for (int i = 1; i < nrOfRecipients; i++)
            {
                Range range = document.Content;
                rangeToCopy.MoveEnd(WdUnits.wdCharacter, -1);
                range.Collapse(WdCollapseDirection.wdCollapseEnd);
                int start = range.Start;
                range.InsertBreak(WdBreakType.wdPageBreak);

                range.Collapse(WdCollapseDirection.wdCollapseEnd);
                range.Paste();
                Range bookmarkRange = document.Range(start, range.End);
                document.Bookmarks.Add(ENVELOPE_COPY_PAGE + i, bookmarkRange);
            }
        }

        private static void DeleteExtraLabelsPages(Document document)
        {
            foreach (Bookmark bookmark in document.Bookmarks)
            {
                if (bookmark.Name.StartsWith(LABELS_COPY_PAGE))
                {
                    bookmark.Range.Delete();
                }
            }
        }

        private static void CopyLabelsPage(Document document, string recipients)
        {
            int nrOfRecipients = recipients?.Trim()?.Split(new string[] { "\r\n\r\n", "\r\r", "\n\n", "\v\v" }, StringSplitOptions.RemoveEmptyEntries)?.Length ?? 0;

            Range rangeToCopy = document.Content;
            rangeToCopy.MoveEnd(WdUnits.wdCharacter, -1);
            rangeToCopy.Copy();
            for (int i = 1; i < nrOfRecipients; i++)
            {
                Range range = document.Content;
                rangeToCopy.MoveEnd(WdUnits.wdCharacter, -1);
                range.Collapse(WdCollapseDirection.wdCollapseEnd);
                int start = range.Start;
                range.InsertBreak(WdBreakType.wdPageBreak);

                range.Collapse(WdCollapseDirection.wdCollapseEnd);
                range.Paste();
                Range bookmarkRange = document.Range(start, range.End);
                document.Bookmarks.Add(LABELS_COPY_PAGE + i, bookmarkRange);
            }
        }


        private static void ShowAssistantBlock(List<ContentControl> contentControls)
        {
            foreach (var cc in contentControls.Where(x => x.Title == "Assistant Block"))
            {
                foreach (Paragraph paragraph in cc.Range.Paragraphs)
                {
                    paragraph.Range.Font.Hidden = 0;
                }
            }
        }

        private static void HideAssistantBlock(List<ContentControl> contentControls)
        {
            foreach (var cc in contentControls.Where(x => x.Title == "Assistant Block"))
            {
                foreach (ContentControl nestedCC in cc.Range.ContentControls)
                {
                    try
                    {
                        nestedCC.Range.Text = " ";
                    }
                    catch { }
                }
                foreach (Paragraph paragraph in cc.Range.Paragraphs)
                {
                    paragraph.Range.Font.Hidden = -1;
                }
            }
        }

        private static void AddAttentionLineToNonTableRecipients(PaneDTO letterDTO, string[] attentions, ContentControl cc, bool hasAttentionCC, string separator = "\v\v", string moveUntil = "\v")
        {
            int i = 0;
            int nonAttention = 0;
            foreach (Range recipientRange in GetRecipientRanges(cc.Range, separator, moveUntil))
            {
                List<SelectedContact> selectedContacts = new List<SelectedContact>(letterDTO.AddedRecipients);
                BaseUtils.RemoveRecipientsNotInTextBox(selectedContacts, recipientRange.Text);
                if (selectedContacts.Count > 0)
                {
                    nonAttention++;
                }
                else if (attentions != null && (attentions.Length != 1 || !hasAttentionCC) && i < attentions.Length + nonAttention)
                {
                    AddAttentionLineParagraphToEndOfRange(recipientRange, attentions[i - nonAttention], false, letterDTO.AttentionTranslation);
                }
                i++;
            }
        }

        private static bool ContainsContentControl(Range range, string title)
        {
            if (range != null)
            {
                foreach (ContentControl cc in range.ContentControls)
                {
                    if (cc.Tag == CustomConstants.TemplateTags && cc.Title == title)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        private static bool ContainsContentControl(Range range, List<string> titles)
        {
            if (titles != null)
            {
                foreach (string title in titles)
                {
                    if (ContainsContentControl(range, title))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        private static Range GetPreviousLine(Range range)
        {
            try
            {
                range.MoveStartUntil("\v\r\n\a", WdConstants.wdBackward);
                range = range.Previous(WdUnits.wdWord);
                int moved = range.MoveStartUntil("\v\r\n\a", WdConstants.wdBackward);
                if (moved == 0 && range.Paragraphs.First.Previous() == null)
                {
                    range.Start = 0;
                }
                return range;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private static Range GetAssistantIntialsSlashRange(ContentControl cc, Document document)
        {
            Range range = BaseUtils.GetLineOfRange(cc.Range);
            foreach (ContentControl acc in range.ContentControls)
            {
                if ((acc.Title == "Author's Initials" || acc.Title == "Initials") && acc.Tag == CustomConstants.TemplateTags)
                {
                    int end = acc.Range.End;
                    if (end < cc.Range.Start)
                    {
                        Range result = acc.Range.Duplicate;
                        result.Collapse(WdCollapseDirection.wdCollapseEnd);

                        result.MoveEnd(WdUnits.wdCharacterFormatting, cc.Range.Start - end - 1);
                        result.MoveStart(WdUnits.wdCharacterFormatting, 1);
                        if (result.Text.Trim() == "\\" || result.Text.Trim() == "/")
                        {
                            return result;
                        }
                    }
                }
            }

            return null;
        }

        private static void TrimEmptyParagraphs(ContentControl cc, Word.Table table)
        {
            if (cc != null && table != null)
            {
                if (table.Range.Paragraphs.First.Previous() != null)
                {
                    Paragraph previous = table.Range.Paragraphs.First.Previous();

                    if (previous.Range.End > cc.Range.Start && previous.Range.Text.IsEmpty())
                    {

                        if (PreviousParagraphIsTable(cc.Range.Paragraphs.First))
                        {
                            Paragraph first = cc.Range.Paragraphs.First;
                            first.SpaceAfter = 0;
                            first.LineSpacingRule = WdLineSpacing.wdLineSpaceExactly;
                            first.LineSpacing = 1;
                        }
                        else
                        {
                            Range range = previous.Range;
                            range.MoveStart(WdUnits.wdCharacterFormatting, 1);
                            range.Text = "";
                        }
                    }
                }

                if (table.Range.Paragraphs.Last.Next() != null)
                {
                    Paragraph next = table.Range.Paragraphs.Last.Next();
                    if (next.Range.Start <= cc.Range.End && next.Range.Text.IsEmpty())
                    {
                        Range range = next.Range;
                        range.MoveEnd(WdUnits.wdCharacterFormatting, -1);

                        if (range.Start < range.End)
                        {
                            try
                            {
                                range.Delete();
                            }
                            catch { }
                        }
                    }
                }
            }
        }

        private static bool PreviousParagraphIsTable(Paragraph first)
        {
            if (first != null && first.Previous() != null && first.Range.Tables.Count == 0 && first.Previous().Range.Tables.Count > 0)
            {
                return true;
            }

            return false;
        }

        private static List<float?> GetRecipTableWidths(Document document)
        {
            List<float?> result = new List<float?>();
            string prop = BaseUtils.ReadProperty(document, RECIP_TABLE_WIDTHS_PROP);
            if (prop != null)
            {
                string[] strArr = prop.Split(',');
                foreach (string str in strArr)
                {
                    if (float.TryParse(str, out float fres))
                    {
                        result.Add(fres);
                    }
                    else
                    {
                        result.Add(null);
                    }
                }
            }
            return result;
        }

        private static void DeleteInlineShapes(Range range)
        {
            for (int i = range.InlineShapes.Count; i >= 1; i--)
            {
                range.InlineShapes[i].Delete();
            }
        }

        private static AuthorWithPathsDTO MergeContactInformation(AuthorWithPathsDTO author, int locationId)
        {
            if (author != null)
            {
                List<AContactInformationDTO> officeSpecificCIs = AdminPanelWebApi.GetAuthorOfficeSpecificContactInformation(author.Id, locationId);
                if (officeSpecificCIs != null)
                {
                    foreach (AContactInformationDTO info in officeSpecificCIs)
                    {
                        var tempInfo = author.ContactInformations?.FirstOrDefault(x => x.Id == info.Id);
                        if (tempInfo != null)
                        {
                            tempInfo.Details = info.Details;
                        }
                    }
                }
            }

            return author;
        }

        public static ContentControl DeleteCCAndReAdd(ContentControl cc)
        {
            ContentControl contentControl;
            Word.Paragraph paragraph = cc.Range.Paragraphs.First;
            string title = cc.Title;
            string tag = cc.Tag;
            cc.Delete(true);
            contentControl = paragraph.Range.ContentControls.Add();
            paragraph.Range.Font.Hidden = 0;
            contentControl.Title = title;
            contentControl.Tag = tag;
            return contentControl;
        }

        private static void ConvertTablesInRangeToText(Range range)
        {
            var toDelete = new List<Word.Table>();

            foreach (Word.Table tbl in range.Tables)
            {
                if (tbl.Range.Start + 1 >= range.Start)
                {
                    toDelete.Add(tbl);
                }
            }

            foreach (Word.Table tbl in toDelete)
            {
                tbl.ConvertToText();
            }
        }

        private static void HideAllContentControlsToHideWhenLineEmpty()
        {
            if (ContentControlsToHideWhenLineEmpty != null)
            {
                foreach (ContentControl cc in ContentControlsToHideWhenLineEmpty)
                {
                    var row = BaseUtils.GetLineOfRange(cc.Range);

                    if (!string.IsNullOrWhiteSpace(row.Text))
                    {
                        row.Font.Hidden = 0;
                    }
                    else
                    {
                        row.Font.Hidden = -1;
                    }
                }
            }

            foreach (ContentControl cc in ContentControlsToHideWhenLineEmpty)
            {
                if (string.IsNullOrWhiteSpace(cc.Range.Text))
                {
                    cc.Range.Font.Hidden = -1;
                }
                else
                {
                    cc.Range.Font.Hidden = 0;
                }
            }
        }

        private static void FillAndHideWhenLineEmpty(ContentControl cc, string value)
        {
            cc.Range.Text = value?.SpaceIfEmpty() ?? " ";

            ContentControlsToHideWhenLineEmpty?.Add(cc);
        }

        private static void FillAndHide(ContentControl cc, string value, bool alwaysHide = false, bool includetableRow = false)
        {
            value = value?.Trim();
            try
            {
                cc.Range.Text = value?.SpaceIfEmpty() ?? " ";
            }
            catch (Exception ex)
            {
                if (value.Contains("\r") || value.Contains("\n"))
                {
                    value = ReplaceParagraphBreaksWithLineBreaks(value);
                    try
                    {
                        cc.Range.Text = value?.SpaceIfEmpty() ?? " ";
                    }
                    catch (Exception ex2)
                    {
                        return;
                    }
                }
            }
            var row = BaseUtils.GetLineOfRange(cc.Range);
            bool isIsTable = cc.Range.Tables.Count > 0;

            if (!includetableRow || !isIsTable)
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    row.Font.Hidden = alwaysHide ? -1 : 0;
                }
                else
                {
                    row.Font.Hidden = -1;
                }
            }
            else
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    cc.Range.Rows[1].Range.Font.Hidden = alwaysHide ? -1 : 0;
                }
                else
                {
                    cc.Range.Rows[1].Range.Font.Hidden = -1;
                }
            }
        }
        private static void FillAndHideCCOnly(ContentControl cc, string value, bool alwaysHide = false)
        {
            value = value?.Trim();
            try
            {
                cc.Range.Text = value?.SpaceIfEmpty();
            }
            catch (Exception ex)
            {
                if (value.Contains("\r") || value.Contains("\n"))
                {
                    value = ReplaceParagraphBreaksWithLineBreaks(value);
                    try
                    {
                        cc.Range.Text = value?.SpaceIfEmpty() ?? " ";
                    }
                    catch (Exception ex2)
                    {
                        return;
                    }
                }
            }

            if (!string.IsNullOrWhiteSpace(value))
            {
                cc.Range.Font.Hidden = alwaysHide ? -1 : 0;
            }
            else
            {
                cc.Range.Font.Hidden = -1;
            }
        }

        public static PaneDTO ReadAnswersFromDocument(Document document, PaneDTO input = null)
        {
            PaneDTO result = input ?? new PaneDTO();
            var contentControls = DocumentUtils.GetAllContentControls(document, CustomConstants.TemplateTags)?.ToList();
            var matterMatterControls = DocumentUtils.GetAllContentControls(document, CustomConstants.MATTER_MASTER_TAG)?.ToList();

            foreach (Word.ContentControl cc in matterMatterControls)
            {
                switch (cc.Title)
                {
                    case "FileNo":
                        result.FileNumber = GetContentControlValue(cc) ?? result.FileNumber;
                        break;
                }
            }

            bool recipientsCCFound = false;
            if (contentControls != null && contentControls.Count > 0)
            {
                recipientsCCFound = contentControls.Where(x => x.Title == "Address(es)" || x.Title == "Recipient(s)" || x.Title == "Recipient").Count() > 0;

                StringCounter stringCounter = new StringCounter();
                foreach (Word.ContentControl cc in contentControls)
                {
                    if (cc.Tag == CustomConstants.CONTENT_CONTROL_TAG)
                    {
                        if (stringCounter.GetStringCount(cc.Title) == 0 && !cc.ShowingPlaceholderText && !string.IsNullOrWhiteSpace(cc.Range.Text))
                        {
                            stringCounter.AddString(cc.Title);
                            switch (cc.Title)
                            {
                                case "Our File Number Tag":
                                case "File No. Tag":
                                case "File Number Tag":
                                    result.FileNumberTagSetting = GetContentControlValue(cc) ?? result.FileNumberTagSetting;
                                    break;
                                case "Our File Number":
                                case "File Number":
                                    result.FileNumber = GetContentControlValue(cc) ?? result.FileNumber;
                                    break;
                                case "Their File Number":
                                case "Client File Number":
                                    result.ClientFileNumber = GetContentControlValue(cc) ?? result.ClientFileNumber;
                                    break;
                                case "No. of Pages":
                                    result.NoOfPages = GetContentControlValue(cc) ?? result.NoOfPages;
                                    break;
                                case "Delivery":
                                    var delivery = GetContentControlValue(cc);

                                    if (!string.IsNullOrEmpty(result.IncludeContactInformationDeliverySetting) && !string.IsNullOrWhiteSpace(delivery) && result.AddedRecipients.Count > 0)
                                    {
                                        var firstRecipient = result.AddedRecipients.FirstOrDefault();
                                        if (firstRecipient != null)
                                        {
                                            switch (result.IncludeContactInformationDeliverySetting)
                                            {
                                                case "E":
                                                    var email = BaseUtils.GetContactFirstInformation(firstRecipient.ContactInformations, "Email");
                                                    if (email != null)
                                                    {
                                                        if (delivery.Length > email.Length + 1)
                                                        {
                                                            result.Delivery = delivery.Substring(0, delivery.Length - email.Length - 1);
                                                        }
                                                    }
                                                    break;
                                                case "F":
                                                    var fax = BaseUtils.GetContactFirstInformation(firstRecipient.ContactInformations, "Fax");
                                                    if (fax != null && !string.IsNullOrWhiteSpace(result.Delivery))
                                                    {
                                                        if (delivery.Length > fax.Length + 1)
                                                        {
                                                            result.Delivery = delivery.Substring(0, delivery.Length - fax.Length - 1);
                                                        }
                                                    }
                                                    break;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        result.Delivery = GetContentControlValue(cc) ?? result.Delivery;
                                    }
                                    break;
                                case "Address(es)":
                                case "Recipient(s)":
                                    foreach (Bookmark bm in cc.Range.Bookmarks)
                                    {
                                        if (bm.Name.StartsWith(CustomConstants.ERB_BLANK_LINE))
                                        {
                                            bm.Range.Text = "\r";
                                        }
                                    }


                                    if (cc.Range.Text.IndexOf('\a') >= 0)
                                    {
                                        result.Recipients = ReplaceRAWithRR(cc.Range.Text).Trim('\r');
                                    }
                                    else
                                    {
                                        result.Recipients = cc.Range.Text;
                                    }

                                    string[] strArr = result.Recipients.Split(new string[] { "\r\n", "\r", "\n", "\v" }, StringSplitOptions.None);
                                    result.Recipients = string.Join("\r\n", strArr.Where(x => AttentionNotFound(x, result.Attention)).ToArray());

                                    foreach (Bookmark bm in cc.Range.Bookmarks)
                                    {
                                        if (bm.Name.StartsWith(CustomConstants.ERB_BLANK_LINE))
                                        {
                                            Range tempRange = bm.Range;
                                            tempRange.MoveEnd(WdUnits.wdCharacter, 1);
                                            tempRange.Text = "";
                                        }
                                    }
                                    break;
                                case "Handling":
                                    result.Handling = GetContentControlValue(cc) ?? result.Handling;
                                    break;
                                case "Salutation":
                                    result.Salutation = GetContentControlValue(cc) ?? result.Salutation;
                                    break;
                                case "Re Line":
                                    result.ReLine = GetContentControlValue(cc) ?? result.ReLine;
                                    break;
                                case "Closing":
                                    result.Closing = (GetContentControlValue(cc) ?? result.Closing)?.TrimEnd(new char[] { ',' });
                                    break;
                                case "Courtesy Copy":
                                    result.CC = GetContentControlValue(cc) ?? result.CC;
                                    break;
                                case "Blind Copy":
                                    result.BCC = GetContentControlValue(cc) ?? result.BCC;
                                    break;
                                case "Office Location":
                                    result.OfficeLocation = GetContentControlValue(cc) ?? result.OfficeLocation;
                                    break;
                                case "Attention":
                                    result.Attention = GetContentControlValue(cc) ?? result.Attention;
                                    break;
                                case "Enclosure":
                                    string enclosureMixed = GetContentControlValue(cc);
                                    if (enclosureMixed != null)
                                    {
                                        string enclosuretext = Regex.Replace(enclosureMixed, @"\(\d+\)", "").Trim();
                                        result.Enclosures = enclosuretext;
                                        Regex regex = new Regex(@"\(\d+\)", RegexOptions.IgnoreCase);

                                        Match m = regex.Match(enclosureMixed);   // m is the first match
                                        if (m.Success && int.TryParse(m.Value.Trim('(', ')'), out int enclosureNr))
                                        {
                                            result.Number = enclosureNr;
                                        }
                                    }
                                    break;
                                case "Date":
                                    var ccValue = GetContentControlValue(cc);
                                    if (!string.IsNullOrWhiteSpace(ccValue))
                                    {
                                        if (DateTime.TryParse(ccValue, out DateTime validDate))
                                        {
                                            result.Date = validDate;
                                        }
                                    }
                                    break;
                                case "Initials":
                                case "Author's Initials":
                                    result.Initials = GetContentControlValue(cc) ?? result.Initials;
                                    break;
                                default:
                                    string unknownTitle = cc.Title;
                                    break;
                            }

                        }
                    }
                }
            }

            if (!recipientsCCFound)
            {
                Paragraph firstAddressPar = null;
                foreach (Paragraph par in document.Paragraphs)
                {
                    Style style = par.get_Style();

                    if (style != null && style.NameLocal == "Address")
                    {
                        firstAddressPar = par;
                        break;
                    }
                }

                if (firstAddressPar != null)
                {
                    List<string> texts = new List<string>();
                    Paragraph nextAddressParagraph = firstAddressPar;
                    while (nextAddressParagraph != null)
                    {
                        texts.Add(nextAddressParagraph.Range.Text.Trim());
                        nextAddressParagraph = nextAddressParagraph.Next();
                        if (nextAddressParagraph != null)
                        {
                            Style style = nextAddressParagraph.get_Style();

                            if (style == null || style.NameLocal != "Address")
                            {
                                nextAddressParagraph = null;
                            }
                        }
                    }

                    result.Recipients = string.Join("\v\v", texts);
                }
            }

            return result;
        }

        private static bool AttentionNotFound(string line, string attention)
        {
            if (string.IsNullOrWhiteSpace(line) || string.IsNullOrWhiteSpace(attention) || !line.Contains("\t"))
            {
                return true;
            }

            attention.Split(new string[] { "\r\n", "\r", "\n", "\v" }, StringSplitOptions.None);

            return !attention.Where(x => line.Contains(x)).Any();
        }

        private static string ReplaceRAWithRR(string text)
        {
            var regex = new Regex("(\r\a)\\1+");
            text = regex.Replace(text, "$1");
            return text.Replace("\r\a", "\r\r");
        }

        private static string GetContentControlValue(ContentControl cc)
        {
            if (cc != null && !string.IsNullOrWhiteSpace(cc.Range.Text))
            {
                return cc.Range.Text;
            }

            return null;
        }

        private static ACompanySimpleDTO GetCompoundCompany(AuthorWithPathsDTO author, int? companyId, MsoLanguageID languageID = MsoLanguageID.msoLanguageIDNone, int? officeLocationId = null)
        {
            if (author == null)
            {
                author = AdminPanelWebApi.GetCurrentAuthor();
            }
            List<FavouriteAuthorDTO> favAuthors = AdminPanelWebApi.GetFavouriteAuthors();

            companyId = favAuthors?.FirstOrDefault(x => x.Id == author.Id)?.CurrentACompanyId ?? companyId;

            //get company details
            ACompanySimpleDTO currentCompany;
            if (companyId != null && companyId > 0)
            {
                ACompanyWithPathsDTO fullCompany = AdminPanelWebApi.GetCompanyDetails((int)companyId, languageID);

                currentCompany = new ACompanySimpleDTO() { Id = fullCompany.Id, CompanyName = fullCompany.Name, Description = fullCompany.Description, CompanyWebsite = fullCompany.Website };

                var defaultOfficeLocation = fullCompany.Addresses.FirstOrDefault(x => x.IsDefault) ?? fullCompany.Addresses.FirstOrDefault();

                if (defaultOfficeLocation != null)
                {
                    currentCompany.DefaultLocationId = defaultOfficeLocation.Id;
                    currentCompany.Address = defaultOfficeLocation.Address1;
                    currentCompany.Address2 = defaultOfficeLocation.Address2;
                    currentCompany.City = defaultOfficeLocation.City;
                    currentCompany.Country = defaultOfficeLocation.Country;
                    currentCompany.PostalCode = defaultOfficeLocation.PostalCode;
                    currentCompany.Province = defaultOfficeLocation.Province;
                    currentCompany.CompanyPhone = defaultOfficeLocation.Phone;
                    currentCompany.CompanyFax = defaultOfficeLocation.Fax;
                }
            }
            else
            {
                currentCompany = AdminPanelWebApi.GetCurrentCompanySimpleDTO();
            }

            if (currentCompany == null)
            {
                var companies = AdminPanelWebApi.GetCompanies();
                if (companies?.Count == 1)
                {
                    ACompanyWithPathsDTO fullCompany = AdminPanelWebApi.GetCompanyDetails(companies.First().Id, languageID);
                    currentCompany = new ACompanySimpleDTO() { Id = fullCompany.Id, CompanyName = fullCompany.Name, Description = fullCompany.Description, CompanyWebsite = fullCompany.Website };
                }
            }

            //get office location
            int? location = officeLocationId ?? favAuthors?.FirstOrDefault(x => x.Id == author.Id)?.OfficeLocationId;
            if (currentCompany != null && location != null)
            {
                AuthorAddressDTO addressDTO = AdminPanelWebApi.GetAddress(location.Value, languageID);
                if (addressDTO != null)
                {
                    currentCompany.DefaultLocationId = addressDTO.Id;
                    currentCompany.Address = addressDTO.Address1;
                    currentCompany.Address2 = addressDTO.Address2;
                    currentCompany.City = addressDTO.City;
                    currentCompany.Province = addressDTO.Province;
                    currentCompany.PostalCode = addressDTO.PostalCode;
                    if (!string.IsNullOrEmpty(addressDTO.Fax))
                    {
                        currentCompany.CompanyFax = addressDTO.Fax;
                    }
                    if (!string.IsNullOrEmpty(addressDTO.Phone))
                    { 
                        currentCompany.CompanyPhone = addressDTO.Phone;
                    }
                }
            }

            return currentCompany;
        }

        public static void InsertLabelTemplateDTO(Word.Document document, PaneDTO letterDTO)
        {
            bool showHiddenText = document.ActiveWindow.View.ShowHiddenText;
            document.ActiveWindow.View.ShowHiddenText = true;
            ACompanySimpleDTO currentCompany = GetCompoundCompany(letterDTO.Author, letterDTO.CompanyId, DocumentUtils.GetDocumentLanguage(document), letterDTO.OfficeLocationId);
            if (currentCompany != null && currentCompany.DefaultLocationId.HasValue)
            {
                if (letterDTO.Author != null)
                {
                    letterDTO.Author = MergeContactInformation(letterDTO.Author, currentCompany.DefaultLocationId.Value);
                }

                if (letterDTO.Assistant != null)
                {
                    letterDTO.Assistant = MergeContactInformation(letterDTO.Assistant, currentCompany.DefaultLocationId.Value);
                }
                letterDTO.OfficeLocation = currentCompany.City;
            }
            DeleteExtraLabelsPages(document);
            var contentControls = DocumentUtils.GetAllContentControls(document, CustomConstants.TemplateTags)?.ToList();
            int recipientBlocksCount = contentControls.Where(x => x.Title == "Recipient").Count();

            int counter = -1;
            int startIndex = letterDTO.StartAtLabelNumber - 1;
            StringCounter stringCounter = new StringCounter();
            var recipArr = letterDTO.Recipients.Trim().Split(new string[] { "\r\n\r\n", "\v\v" }, StringSplitOptions.RemoveEmptyEntries);

            if (recipArr.Length > 1 && recipientBlocksCount == 1)
            {
                CopyLabelsPage(document, letterDTO.Recipients);
                contentControls = DocumentUtils.GetAllContentControls(document, CustomConstants.TemplateTags)?.ToList();
            }

            string[] attentions = letterDTO?.Attention?.Trim()?.Split(new string[] { "\r\n", "\r", "\n"}, StringSplitOptions.RemoveEmptyEntries);
            int nonAttentionCount = 0;
            if (contentControls != null && contentControls.Count > 0)
            {
                foreach (Word.ContentControl cc in contentControls)
                {
                    /*if (cc.Title == "Company Name")
                        counter++;
                    if (startIndex > counter || (!letterDTO.UseFirstRecipient && (counter - startIndex) >= letterDTO.RecipientsArr.Length))
                    {
                        cc.Range.Text = " ";
                        continue;
                    }*/

                    counter = stringCounter.GetStringCount(cc.Title);

                    if (!letterDTO.UseFirstRecipient && (counter < startIndex || (counter >= startIndex + recipArr.Length && recipArr.Length > 0)))
                    { 
                        if (stringCounter.IsAbsoluteMax(cc.Title))
                        {
                            HideShowCell(cc, true);
                        }

                        if (cc.Title == "Attention")
                        {
                            FillAndHide(cc, string.Empty);
                        }

                        cc.Range.Text = " ";
                        stringCounter.AddString(cc.Title);
                        cc.Range.Text = " ";

                        continue;
                    }

                    if (stringCounter.IsAbsoluteMax(cc.Title))
                    {
                        HideShowCell(cc, false);
                    }

                    switch (cc.Title)
                    {
                        case "Recipient":
                            if (letterDTO.UseFirstRecipient)
                            {
                                if (recipArr.Length > 0)
                                {
                                    cc.Range.Text = recipArr[0].SpaceIfEmpty();
                                }
                                else
                                {
                                    cc.Range.Text = " ";
                                }
                            }
                            else
                            {
                                if (recipArr == null || recipArr.Length <= counter - startIndex)
                                    cc.Range.Text = " ";
                                else
                                    cc.Range.Text = recipArr[counter - startIndex];
                            }
                            break;
                        case "Company Name":
                            cc.Range.Text = currentCompany?.CompanyName?.SpaceIfEmpty() ?? " ";
                            break;
                        case "Company Address":
                            cc.Range.Text = currentCompany?.Address?.SpaceIfEmpty() ?? " ";
                            break;
                        case "Company Address 2":
                            FillAndHide(cc, currentCompany?.Address2);
                            break;
                        case "Company Address (Graphic)":
                            if (cc.Range.InlineShapes.Count == 0 && currentCompany.DefaultLocationId.HasValue)
                                try
                                {
                                    cc.Range.Text = " ";

                                    bool isSmall = letterDTO.Type == TemplateType.Envelope;
                                    var addressGraphicUrl = AdminPanelWebApi.GetCompanyLocationAddressGraphicUrl(currentCompany.DefaultLocationId.Value, currentCompany.Id);
                                    if (!addressGraphicUrl.IsEmpty())
                                    {
                                        var pic = cc.Range.InlineShapes.AddPicture(addressGraphicUrl);
                                        try
                                        {
                                            File.Delete(addressGraphicUrl);
                                        }
                                        catch { }
                                    }
                                }
                                catch { }//do nothing, probably no logo uploaded yet
                            break;
                        case "Company City":
                            cc.Range.Text = currentCompany?.City?.SpaceIfEmpty() ?? " ";
                            break;
                        case "Company Province":
                            cc.Range.Text = currentCompany?.Province?.SpaceIfEmpty() ?? " ";
                            break;
                        case "Company Postal Code":
                            cc.Range.Text = currentCompany?.PostalCode?.SpaceIfEmpty() ?? " ";
                            break;
                        case "Our File Number":
                        case "File Number":
                            cc.Range.Text = letterDTO.FileNumber?.SpaceIfEmpty() ?? " ";
                            break;
                        case "Their File Number":
                        case "Client File Number":
                            cc.Range.Text = letterDTO.ClientFileNumber?.SpaceIfEmpty() ?? " ";
                            break;
                        case "Author's Initials":
                            if (letterDTO.Initials?.IsEmpty() == false)
                                cc.Range.Text = letterDTO.Initials;
                            else
                                cc.Range.Text = GetAuthorInitials_Letter(letterDTO.Author).SpaceIfEmpty();
                            break;
                        case "Assistant's Initials":
                            string assistantInitials = GetAuthorInitials_Letter(letterDTO.Assistant)?.ToLower();
                            Range slashRange = GetAssistantIntialsSlashRange(cc, document);
                            if (!string.IsNullOrEmpty(assistantInitials))
                            {
                                cc.Range.Text = assistantInitials;
                                if (slashRange != null)
                                {
                                    slashRange.Font.Hidden = 0;
                                }
                            }
                            else
                            {
                                cc.Range.Text = " ";
                                if (slashRange != null)
                                {
                                    slashRange.Font.Hidden = -1;
                                }
                            }
                            break;
                        case "Delivery":
                            var delivery = letterDTO.Delivery;
                            if (!string.IsNullOrEmpty(letterDTO.IncludeContactInformationDeliverySetting) && !string.IsNullOrWhiteSpace(delivery) && letterDTO.AddedRecipients.Count > 0)
                            {
                                var firstRecipient = letterDTO.AddedRecipients.FirstOrDefault();
                                if (firstRecipient != null)
                                {
                                    switch (letterDTO.IncludeContactInformationDeliverySetting)
                                    {
                                        case "E":
                                            var email = BaseUtils.GetContactFirstInformation(firstRecipient.ContactInformations, "Email");
                                            if (email != null && !string.IsNullOrWhiteSpace(letterDTO.Delivery))
                                            {
                                                delivery += " " + email;
                                            }
                                            break;
                                        case "F":
                                            var fax = BaseUtils.GetContactFirstInformation(firstRecipient.ContactInformations, "Fax");
                                            if (fax != null && !string.IsNullOrWhiteSpace(letterDTO.Delivery))
                                            {
                                                delivery += " " + fax;
                                            }
                                            break;
                                    }
                                }
                            }
                            cc.Range.Text = delivery.SpaceIfEmpty();
                            break;
                        case "Handling":
                            cc.Range.Text = letterDTO.Handling.SpaceIfEmpty();
                            break;
                        case "Attention":
                            bool nonAttention = false;

                            int attentionCounter = letterDTO.UseFirstRecipient ? 0 : counter;

                            if (recipArr != null && recipArr.Length > attentionCounter - startIndex)
                            {
                                string recipientStr = recipArr[attentionCounter - startIndex];
                                List<SelectedContact> selectedContacts = new List<SelectedContact>(letterDTO.AddedRecipients);
                                BaseUtils.RemoveRecipientsNotInTextBox(selectedContacts, recipientStr);
                                nonAttention = selectedContacts.Count > 0;
                            }

                            if (!nonAttention)
                            {
                                if (attentionCounter - startIndex - nonAttentionCount < attentions.Length)
                                {
                                    FillAndHide(cc, attentions[attentionCounter - startIndex - nonAttentionCount]);
                                }
                                else
                                {
                                    FillAndHide(cc, " ");
                                }
                            }
                            else
                            {
                                nonAttentionCount++;
                                FillAndHide(cc, " ");
                            }

                            break;

                        case "To (Full Name)":
                            string valueToInsert = string.Empty;
                            {
                                string firstLineRecipients = letterDTO.Recipients.Trim().Split(new string[] { "\r\n", "\r", "\n", "\v" }, StringSplitOptions.RemoveEmptyEntries).FirstOrDefault();
                                List<SelectedContact> selectedContacts = new List<SelectedContact>(letterDTO.AddedRecipients);
                                BaseUtils.RemoveRecipientsNotInTextBox(selectedContacts, firstLineRecipients);
                                if (selectedContacts.Count == 0 && attentions?.FirstOrDefault() != null)
                                {
                                    firstLineRecipients = attentions.FirstOrDefault() ?? firstLineRecipients;
                                }
                                valueToInsert = firstLineRecipients?.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).FirstOrDefault();
                            }

                            FillAndHide(cc, valueToInsert);

                            break;
                        case "Company Description":
                            cc.Range.Text = currentCompany?.Description?.SpaceIfEmpty() ?? " ";
                            break;
                        case "Company Phone":
                            cc.Range.Text = currentCompany?.CompanyPhone?.SpaceIfEmpty() ?? " ";
                            break;
                        case "Company Fax":
                            cc.Range.Text = currentCompany?.CompanyFax?.SpaceIfEmpty() ?? " ";
                            break;
                        case "Company Website":
                            cc.Range.Text = currentCompany?.CompanyWebsite?.SpaceIfEmpty() ?? " ";
                            break;
                        case "Full Name":

                            cc.Range.Text = (string.IsNullOrEmpty(letterDTO.Author.Prefix) ? "" : (letterDTO.Author.Prefix + " ")) + letterDTO.Author.FirstName + " "
                                + letterDTO.Author.MiddleName + (string.IsNullOrEmpty(letterDTO.Author.MiddleName) ? "" : " ")
                                + letterDTO.Author.LastName + (string.IsNullOrEmpty(letterDTO.Author.Suffix) ? "" : (" " + letterDTO.Author.Suffix));

                            break;
                        case "Title":
                            FillAndHideWhenLineEmpty(cc, letterDTO.Author.JobTitle.SpaceIfEmpty());
                            break;
                        case "Assistant Name":
                            if (letterDTO.Assistant != null)
                            {
                                cc.Range.Text = letterDTO.Assistant.FirstName + " "
                                    + letterDTO.Assistant.MiddleName + (string.IsNullOrEmpty(letterDTO.Assistant.MiddleName) ? "" : " ")
                                    + letterDTO.Assistant.LastName;
                            }
                            else
                            {
                                cc.Range.Text = "";
                            }
                            break;
                        case "Degrees":
                            FillAndHideWhenLineEmpty(cc, ReplaceParagraphBreaksWithLineBreaks(letterDTO.Author.Credentials.SpaceIfEmpty()));
                            break;
                        case "Certifications":
                            FillAndHideWhenLineEmpty(cc, ReplaceParagraphBreaksWithLineBreaks(letterDTO.Author.Certification.SpaceIfEmpty()));
                            break;
                        case "User1":
                            FillAndHideWhenLineEmpty(cc, ReplaceParagraphBreaksWithLineBreaks(letterDTO.Author.User1.SpaceIfEmpty()));
                            break;
                        case "User2":
                            FillAndHideWhenLineEmpty(cc, ReplaceParagraphBreaksWithLineBreaks(letterDTO.Author.User2.SpaceIfEmpty()));
                            break;
                        case "User3":
                            FillAndHideWhenLineEmpty(cc, ReplaceParagraphBreaksWithLineBreaks(letterDTO.Author.User3.SpaceIfEmpty()));
                            break;
                        case "Direct Telephone":
                            cc.Range.Text = BaseUtils.GetContactFirstInformation(letterDTO.Author.ContactInformations, "Phone").SpaceIfEmpty();
                            break;
                        case "Mobile Phone":
                            cc.Range.Text = BaseUtils.GetContactFirstInformation(letterDTO.Author.ContactInformations, "Mobile").SpaceIfEmpty();
                            break;
                        case "Direct Fax":
                            string directFax = BaseUtils.GetContactFirstInformation(letterDTO.Author.ContactInformations, "Fax").SpaceIfEmpty();
                            if (string.IsNullOrWhiteSpace(directFax))
                            {
                                cc.Range.Text = currentCompany?.CompanyFax?.SpaceIfEmpty() ?? " ";
                            }
                            else
                            {
                                cc.Range.Text = directFax?.SpaceIfEmpty() ?? " ";
                            }
                            break;
                        case "Email":
                            cc.Range.Text = BaseUtils.GetContactFirstInformation(letterDTO.Author.ContactInformations, "Email").SpaceIfEmpty();
                            if (letterDTO.InformOptions?.IncludeEmailAddress == false)
                            {
                                FillAndHide(cc, "");
                            }
                            break;
                        case "Lawyer ID":
                            cc.Range.Text = letterDTO.Author?.LawyerNumber?.SpaceIfEmpty();
                            break;
                        case "Assistant Phone":
                            cc.Range.Text = BaseUtils.GetContactFirstInformation(letterDTO.Assistant?.ContactInformations, "Phone").SpaceIfEmpty();
                            break;
                        case "Assistant Email":
                            cc.Range.Text = BaseUtils.GetContactFirstInformation(letterDTO.Assistant?.ContactInformations, "Email").SpaceIfEmpty();
                            break;
                        case "Assistant Title":
                            FillAndHideWhenLineEmpty(cc, letterDTO.Assistant?.JobTitle.SpaceIfEmpty());
                            break;
                        case "Office Location":
                            cc.Range.Text = letterDTO.OfficeLocation.SpaceIfEmpty();
                            break;
                        case "Date":
                            string dateFormat = string.IsNullOrWhiteSpace(letterDTO.DateFormat) ? "MMMM d, yyyy" : letterDTO.DateFormat;
                            if (letterDTO.CultureId.IsEmpty())
                            {
                                cc.Range.Text = letterDTO.Date.ToString(dateFormat);
                            }
                            else
                            {
                                cc.Range.Text = letterDTO.Date.ToString(dateFormat, CultureInfo.CreateSpecificCulture(letterDTO.CultureId));
                            }

                            break;
                        case "Law Society Number Label":
                            var lawSocietyNrLabel = CustomConstants.LAW_SOC_NR_LABEL;
                            if (!string.IsNullOrEmpty(letterDTO.LawSocietyNumberLabel))
                            {
                                lawSocietyNrLabel = letterDTO.LawSocietyNumberLabel;
                            }
                            cc.Range.Text = lawSocietyNrLabel;
                            break;
                        case "Company Name Rich Text":
                            FillAndHide(cc, "CN");
                            if (currentCompany != null)
                            {
                                string xml = AdminPanelWebApi.GetCompanyNameRichText(currentCompany.Id, DocumentUtils.GetDocumentLanguage(document));
                                cc.Range.Text = "";
                                if (!string.IsNullOrWhiteSpace(xml))
                                {
                                    DocumentUtils.InsertXMLWithoutExtraParagraph(cc, xml);
                                }
                                else
                                {
                                    cc.Range.Text = currentCompany.CompanyName;
                                }
                            }
                            else
                            {
                                cc.Range.Text = " ";
                            }

                            break;
                        case "Re Line":
                            cc.Range.Text = ReplaceParagraphBreaksWithLineBreaks(letterDTO.ReLine?.SpaceIfEmpty()) ?? " ";
                            break;
                        case "Our File Number Tag":
                        case "File No. Tag":
                        case "File Number Tag":
                            var fileNumberTag = CustomConstants.FILE_NUMBER_TAG;
                            if (!string.IsNullOrEmpty(letterDTO.FileNumberTagSetting))
                            {
                                fileNumberTag = letterDTO.FileNumberTagSetting;
                            }
                            cc.Range.Text = fileNumberTag;
                            break;
                        case "Salutation":
                            cc.Range.Text = letterDTO.Salutation.SpaceIfEmpty();
                            break;
                        case "Closing":
                            var closing = letterDTO.Closing;
                            if (!string.IsNullOrWhiteSpace(letterDTO.Closing))
                            {
                                closing += letterDTO.ClosingEndChar;
                            }
                            cc.Range.Text = closing.SpaceIfEmpty();
                            break;
                        case "Enclosure":
                            var enclosure = "";
                            if (!string.IsNullOrEmpty(letterDTO.Enclosures))
                            {
                                if (letterDTO.Number > 0)
                                {
                                    enclosure = string.Format("{0} ({1})", letterDTO.Enclosures, letterDTO.Number);
                                }
                                else
                                {
                                    enclosure = letterDTO.Enclosures;
                                }
                            }

                            FillAndHide(cc, enclosure);
                            break;
                    }
                    stringCounter.AddString(cc.Title);
                }
            }

            RefreshContentContols(document);
            BaseUtils.UnhideRow(contentControls, FillAndHideLabelContentControlTitles);
            BaseUtils.HideUnhideSeparator(contentControls);
            document.ActiveWindow.View.ShowHiddenText = showHiddenText;
        }

        private static void HideShowCell(ContentControl cc, bool hide)
        {
            // only do if inside a table
            try
            {
                if (cc.Range.Cells.Count > 0)
                {
                    foreach (Cell cell in cc.Range.Cells)
                    {
                        if (cell.NestingLevel == 1)
                        {
                            cell.Range.Font.Hidden = hide ? -1 : 0;
                        }
                        else
                        {
                            HideShowCellFromNestedCell(cell, hide);
                        }
                    }
                }
            }
            catch(Exception ex)
            {
            }
        }

        private static void HideShowCellFromNestedCell(Cell cell, bool hide)
        {
            try
            {
                Table table = cell.Range.Tables.Cast<Table>().Where(x => x.NestingLevel == cell.NestingLevel).FirstOrDefault();

                if (table != null)
                {
                    Range range = table.Range;
                    range.Collapse(WdCollapseDirection.wdCollapseEnd);
                    if (range.Cells.Count > 0)
                    {
                        foreach (Cell cell2 in range.Cells)
                        {
                            if (cell2.NestingLevel < cell.NestingLevel)
                            {
                                if (cell2.NestingLevel == 1)
                                {
                                    cell2.Range.Font.Hidden = hide ? -1 : 0;
                                }
                                else
                                {
                                    HideShowCellFromNestedCell(cell2, hide);
                                }
                            }
                        }
                    }
                }
            }
            catch(Exception ex) 
            { 
            }
        }

        private static void RefreshContentContols(Document document)
        {
            var range = document.Content;
            var find = range.Find;
            find.ClearFormatting();

            find.Execute("a", Replace: WdReplace.wdReplaceAll, ReplaceWith: "a", MatchCase: true);
        }

        public static string GetCompanyNameOfRecipient(SelectedContact selectedContact)
        {
            return selectedContact.Companies.FirstOrDefault(x => x.IsDefault = true)?.Name ?? selectedContact.Companies.FirstOrDefault()?.Name;
        }

        public static string GetPhoneOfRecipient(SelectedContact selectedContact)
        {
            return selectedContact.ContactInformations.FirstOrDefault(x => (x.Type == "Phone") && (x.IsDefault = true))?.Details ?? selectedContact.ContactInformations.FirstOrDefault(x => x.Type == "Phone")?.Details;
        }

        public static string GetFaxOfRecipient(SelectedContact selectedContact)
        {
            return selectedContact.ContactInformations.FirstOrDefault(x => (x.Type == "Fax") && (x.IsDefault = true))?.Details ?? selectedContact.ContactInformations.FirstOrDefault(x => x.Type == "Fax")?.Details;
        }

        public static string GetAuthorInitials_Letter(AuthorWithPathsDTO author)
        {
            if (author != null)
            {
                if (!string.IsNullOrWhiteSpace(author.Initials))
                {
                    return author.Initials;
                }
                else
                {
                    var firstName = author.FirstName;
                    var lastName = author.LastName;

                    var initials = (firstName ?? "") + " " + (author.MiddleName ?? "") + " " + (lastName ?? "");

                    var initialsArr = initials.Split(' ');
                    var result = "";
                    for (var i = 0; i < initialsArr.Length; i++)
                    {
                        initialsArr[i] = initialsArr[i]?.Trim();
                        if (initialsArr[i].Length > 0)
                            result = result + initialsArr[i][0];
                    }

                    return result;
                }
            }

            return null;
        }

        private static string ReplaceParagraphBreaksWithLineBreaks(string input)
        {
            if (input == null)
            {
                return null;
            }

            return input.Replace("\r\n", "\v").Replace("\r", "\v").Replace("\n", "\v");
        }

        public static void PopulateLetterheadContentControls(Document document)
        {
            if (document != null)
            {
                ACompanySimpleDTO currentCompany = GetCompoundCompany(null, null, DocumentUtils.GetDocumentLanguage(document), null);
                var contentControls = DocumentUtils.GetAllContentControls(document, CustomConstants.TemplateTags)?.ToList();

                if (contentControls != null && contentControls.Count > 0)
                {
                    foreach (Word.ContentControl cc in contentControls)
                    {
                        switch (cc.Title)
                        {
                            case "Company Address (Graphic)":
                                try
                                {
                                    if (cc.Range.InlineShapes.Count > 0)
                                    {
                                        DeleteInlineShapes(cc.Range);
                                    }

                                    if (cc.Range.InlineShapes.Count == 0 && currentCompany.DefaultLocationId.HasValue)
                                    {
                                        cc.Range.Text = " ";

                                        var addressGraphicUrl = AdminPanelWebApi.GetCompanyLocationAddressGraphicUrl(currentCompany.DefaultLocationId.Value, currentCompany.Id);
                                        if (!addressGraphicUrl.IsEmpty())
                                        {
                                            var pic = cc.Range.InlineShapes.AddPicture(addressGraphicUrl);
                                            try
                                            {
                                                File.Delete(addressGraphicUrl);
                                            }
                                            catch { }
                                        }
                                    }

                                    cc.Tag = CustomConstants.LETTERHEAD_TAG;
                                }
                                catch { }//do nothing, probably no logo uploaded yet
                                break;
                        }
                    }
                }
            }
        }

        private static void AddAttentionLineParagraphToEndOfRange(Range range, string attention, bool inCell = false, string attentionTranslation = "Attention")
        {
            if (range != null && !string.IsNullOrEmpty(attention))
            {
                attention = attention.Trim('\r', '\v', '\n');

                var addedParagraph = range.Paragraphs.Add();

                if (inCell)
                {
                    addedParagraph.Range.Text = attentionTranslation + ":\t" + attention;

                    try
                    {
                        addedParagraph.set_Style("AttnLine");
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex);
                    }
                }
                else
                {
                    range.Collapse(WdCollapseDirection.wdCollapseEnd);
                    Range endParagraphRange = addedParagraph.Range;
                    endParagraphRange.Collapse(WdCollapseDirection.wdCollapseEnd);
                    endParagraphRange.Text = attentionTranslation + ":\t" + attention;

                    Range nextChar = endParagraphRange.Duplicate;
                    nextChar.Collapse(WdCollapseDirection.wdCollapseEnd);
                    nextChar.MoveEnd(WdUnits.wdCharacter, 1);
                    if (nextChar.Text == "\v")
                    {
                        nextChar.Text = "\r\n";
                    }

                    try
                    {
                        endParagraphRange.Paragraphs.First.set_Style("AttnLine");
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex);
                    }
                }
            }
        }

        private static List<Range> GetRecipientRanges(Range input, string separator, string moveUntil)
        {
            List<Range> result = new List<Range>();
            int end = input.End;

            Range range = input.Duplicate;
            range.Collapse(WdCollapseDirection.wdCollapseStart);
            int moved = 1;
            while (moved > 0 && range.End <= end)
            {
                moved = range.MoveEndUntil(moveUntil);
                if (moved > 0 && range.End <= end)
                {
                    Range tempRange = range.Duplicate;
                    tempRange.Collapse(WdCollapseDirection.wdCollapseEnd);
                    tempRange.MoveEnd(WdUnits.wdCharacter, separator.Length);
                    if (tempRange.Text == separator)
                    {
                        result.Add(range.Duplicate);
                        range.MoveEnd(WdUnits.wdCharacter, separator.Length);
                        range.Collapse(WdCollapseDirection.wdCollapseEnd);
                    }
                    else
                    {
                        range.MoveEnd(WdUnits.wdCharacter, 1);
                    }
                }
            }
            range.End = end;
            result.Add(range);

            return result;
        }
    }
}
