﻿using DocumentFormat.OpenXml.CustomProperties;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.VariantTypes;
using InfowareVSTO.Common.DTOs;
using Microsoft.Office.Core;
using Microsoft.Office.Interop.Word;
using OpenXmlPowerTools;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Media;
using System.Xml.Linq;
using Word = Microsoft.Office.Interop.Word;

namespace InfowareVSTO.Common.General
{
	public static class DocumentUtils
	{
        public static MsoLanguageID GetDocumentLanguage(Document document)
        {
            if (document != null)
            {
                try
                {
                    var languageFarEast = (MsoLanguageID)document.Styles[WdBuiltinStyle.wdStyleNormal].LanguageIDFarEast;

                    if (languageFarEast != MsoLanguageID.msoLanguageIDNone && languageFarEast != MsoLanguageID.msoLanguageIDEnglishUS)
                    {
                        return languageFarEast;
                    }
                }
                catch { }

                try
                {
                    return (MsoLanguageID)document.Styles[WdBuiltinStyle.wdStyleNormal].LanguageID;
                }
                catch
                {
                    return MsoLanguageID.msoLanguageIDNone;
                }
            }

            return MsoLanguageID.msoLanguageIDNone;
        }

        public static IEnumerable<Word.ContentControl> GetAllContentControls(Word.Document oDoc, string onlyWithTagName = null, string secondTagName = null)
		{
			List<Word.ContentControl> ccList = new List<Word.ContentControl>();
			// The code below search content controls in all
			// word document stories see http://word.mvps.org/faqs/customization/ReplaceAnywhere.htm
			Word.Range rangeStory;
			foreach (Word.Range range1 in oDoc.StoryRanges)
			{
				rangeStory = range1;
				do
				{
					try
					{
						foreach (Word.ContentControl cc in rangeStory.ContentControls)
						{
							ccList.Add(cc);
						}
						foreach (Word.Shape shapeRange in rangeStory.ShapeRange)
						{
							foreach (Word.ContentControl cc in shapeRange.TextFrame.TextRange.ContentControls)
							{
								ccList.Add(cc);
							}
						}
					}
					catch (COMException) { }
					rangeStory = rangeStory.NextStoryRange;

				}
				while (rangeStory != null);
			}


            if(!string.IsNullOrEmpty(onlyWithTagName) && !string.IsNullOrEmpty(secondTagName))
            {
                ccList = ccList.Where(x => x.Tag == onlyWithTagName || x.Tag == secondTagName).ToList();
            }
            else if (!string.IsNullOrEmpty(onlyWithTagName))
				ccList = ccList.Where(x => x.Tag == onlyWithTagName).ToList();

			return ccList;
		}

		public static KeyValuePair<int, int> GetLabelTemplatePositions(Word.Document oDoc)
		{
            var controls = GetAllContentControls(oDoc, CustomConstants.TemplateTags)?.ToList();
			List<int> columns = new List<int>();
			List<int> rows = new List<int>();
            if(controls != null)
            {
                foreach (var control in controls.Where(x => x.Title == "Recipient"))
                {
                    int column = control.Range.get_Information(Word.WdInformation.wdEndOfRangeColumnNumber);
                    int row = control.Range.get_Information(Word.WdInformation.wdEndOfRangeRowNumber);
                    columns.Add(column);
                    rows.Add(row);
                }
            }
			return new KeyValuePair<int, int>(rows.Distinct().Count(), columns.Distinct().Count());
		}

        public static void InsertXMLWithoutExtraParagraph(Word.ContentControl cc, string xml)
        {
            if (cc == null)
            {
                return;
            }

            try
            {
                Word.Paragraph next = cc.Range.Paragraphs.Last.Next();

                Range range = cc.Range.Paragraphs.First.Range;
                range.Collapse(WdCollapseDirection.wdCollapseStart);
                range.Text = " \r";
                cc.Range.InsertXML(xml);

                Range tempRange = cc.Range;
                tempRange.Collapse(WdCollapseDirection.wdCollapseEnd);
                tempRange.MoveEndUntil("\r\n");
                tempRange.MoveStart(WdUnits.wdCharacterFormatting, 1);
                tempRange.Text = "";
                Word.Paragraph newNext = cc.Range.Paragraphs.Last.Next();

                if (next?.Range?.Start != newNext?.Range?.Start)
                {
                    Word.Range last = cc.Range.Paragraphs.Last.Range;
                    last.Collapse(Word.WdCollapseDirection.wdCollapseEnd);
                    last.MoveStart(Word.WdUnits.wdCharacter, -1);

                    try
                    {
                        last.Delete();
                    }
                    catch
                    {
                    }
                }

                var par = cc.Range.Paragraphs.First.Previous();
                range = par.Range;
                range.Collapse(WdCollapseDirection.wdCollapseEnd);
                range.MoveStart(WdUnits.wdCharacter, -1);
                range.Delete();
                range.MoveStart(WdUnits.wdCharacter, -1);
                range.Delete();
            }
            catch(Exception ex)
            {
                Debug.WriteLine(ex);
            }
        }

        public static void InsertXMLWithoutExtraParagraphOriginal(Word.ContentControl cc, string xml)
        {
            if (cc == null)
            {
                return;
            }

            Word.Paragraph next = cc.Range.Paragraphs.Last.Next();

            cc.Range.InsertXML(xml);

            Word.Paragraph newNext = cc.Range.Paragraphs.Last.Next();

            if (next?.Range?.Start != newNext?.Range?.Start)
            {
                Word.Range last = cc.Range.Paragraphs.Last.Range;
                last.Collapse(Word.WdCollapseDirection.wdCollapseEnd);
                last.MoveStart(Word.WdUnits.wdCharacter, -1);
                try
                {
                    last.Delete();
                }
                catch
                {
                }
            }
        }

        public static string ConvertWordXmlDocumentToHtml(string xml)
        {
            var convertedHtml = string.Empty;
            if (!string.IsNullOrEmpty(xml))
            {
                var flatOpcString = HttpUtility.HtmlDecode(SecurityElement.Escape(xml));
                if (!string.IsNullOrEmpty(flatOpcString))
                {
                    WordprocessingDocument wdoc = WordprocessingDocument.FromFlatOpcString(flatOpcString);
                    XElement element = WmlToHtmlConverter.ConvertToHtml(wdoc, new WmlToHtmlConverterSettings());

                    convertedHtml = element.ToString().Replace("\r\n", "");
                }
            }
            return convertedHtml;
        }

        public static childItem FindVisualChild<childItem>(DependencyObject obj) where childItem : DependencyObject
        {
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
            {
                DependencyObject child = VisualTreeHelper.GetChild(obj, i);
                if (child != null && child is childItem)
                {
                    return (childItem)child;
                }
                else
                {
                    childItem childOfChild = FindVisualChild<childItem>(child);
                    if (childOfChild != null)
                    {
                        return childOfChild;
                    }
                }
            }

            return null;
        }

        public static IEnumerable<T> Descendants<T>(DependencyObject dependencyItem) where T : DependencyObject
        {
            if (dependencyItem != null)
            {
                for (var index = 0; index < VisualTreeHelper.GetChildrenCount(dependencyItem); index++)
                {
                    var child = VisualTreeHelper.GetChild(dependencyItem, index);
                    if (child is T dependencyObject)
                    {
                        yield return dependencyObject;
                    }

                    foreach (var childOfChild in Descendants<T>(child))
                    {
                        yield return childOfChild;
                    }
                }
            }
        }

        public static void DoSave(Document document)
        {
            if (document != null)
            {
                try
                {
                    CommandBar commandBar = document.CommandBars.Add("InfoFileSaveTemp");
                    CommandBarControl commandBarControl = commandBar.Controls.Add(MsoControlType.msoControlButton, 3);

                    commandBarControl.Execute();
                    commandBar.Delete();

                    document.Saved = true;
                    commandBarControl = null;
                    commandBar = null;
                }
                catch(Exception ex)
                {
                    Debug.WriteLine(ex);
                }
            }
        }
    }
}
