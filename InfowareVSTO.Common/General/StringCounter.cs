﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfowareVSTO.Common.General
{
    public class StringCounter
    {
        private Dictionary<string, int> count = new Dictionary<string, int>();

        public void AddString(string str)
        {
            if (!string.IsNullOrEmpty(str))
            {
                count[str] = GetStringCount(str) + 1;
            }
        }

        public int GetStringCount(string str)
        {
            int result = 0;
            if (!string.IsNullOrEmpty(str) && count.TryGetValue(str, out result))
            {
                return result;
            }

            return 0;
        }

        internal bool IsAbsoluteMax(string title)
        {
            int thisCount = GetStringCount(title) + 1;
            int max = count.Values.Count() > 0 ? count.Values.Max() : 0;

            return thisCount == max + 1;
        }
    }
}
