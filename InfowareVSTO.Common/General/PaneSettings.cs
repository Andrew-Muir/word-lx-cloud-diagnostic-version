﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfowareVSTO.Common.General
{
    public class PaneSettings
    {
        public int MinRecipientCountForHorizontal { get; set; }
        public int HorizontalRecipientsColumnCount { get; set; }

        public PaneSettings()
        {
            MinRecipientCountForHorizontal = 3;
            HorizontalRecipientsColumnCount = 3;
        }
    }
}
