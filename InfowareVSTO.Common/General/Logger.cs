﻿using InfowareVSTO.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfowareVSTO.Common
{
    public class Logger
    {
        private static string folder;
        private static string FileName
        {
            get
            {
                if (folder == null)
                {
                    folder = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\Infoware\\Logs";
                }

                if (!Directory.Exists(folder))
                {
                    try
                    {
                        Directory.CreateDirectory(folder);
                    }
                    catch
                    {
                        return null;
                    }
                }

                return folder + "\\" + DateTime.Now.ToString("yyyy.MM.dd") + ".txt";
            }
        }

        private const bool LoggerEnabled = true;

        public static void Log (string message)
        {
            string filename = FileName;
            if (LoggerEnabled && filename != null)
            {
                File.AppendAllText(filename, DateTime.Now.ToLongTimeString() + "." + DateTime.Now.Millisecond + ": " + message + '\n');
            }
        }

        public static void LogException(Exception ex)
        {
            Logger.Log("Exception occurred: " + ex.GetType().ToString() + ", Message: " + ex.Message + " at " + ex.StackTrace);
        }
    }
}
