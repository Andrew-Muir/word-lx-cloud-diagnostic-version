﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfowareVSTO.Common.Language
{
    public enum LanguageEnum
    {
        English = 0,
        French = 1
    }
}
