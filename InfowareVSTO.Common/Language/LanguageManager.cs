﻿using InfowareVSTO.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfowareVSTO.Common.Language
{
    public class LanguageManager
    {
        private const string ENGLISH_DECIMAL_POINT = ".";
        private static Dictionary<int, Dictionary<int, string>> Translations;

        public static LanguageEnum? currentLanguage;

        static LanguageManager()
        {
            Translations = AdminPanelWebApi.DownLoadTranslations();
        }

        public static void SetLanguage(LanguageEnum language)
        {
            currentLanguage = language;
            Microsoft.Win32.Registry.SetValue(ConfigSettings.InfowareKeyFolder, "Language", (int)language);
        }

        public static string GetDecimalPoint()
        {
            LanguageEnum language = GetLanguage();
            switch (language)
            {
                case LanguageEnum.French:
                    return ",";
                case LanguageEnum.English:
                    return ".";
                default:
                    return ".";
            }
        }

        public static string TranslateNumber(string input)
        {
            if (input != null)
            {
                return input.Replace(ENGLISH_DECIMAL_POINT, GetDecimalPoint());
            }

            return null;
        }

        public static LanguageEnum GetLanguage()
        {
            if (currentLanguage == null)
            {
                object languageObj = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "Language", (int)LanguageEnum.English);
                try
                {
                    currentLanguage = (LanguageEnum)((int)languageObj);
                }
                catch
                {
                    currentLanguage = LanguageEnum.English;
                }
            }

            return currentLanguage.Value;
        }

        public static string GetTranslation(int id, string defaultValue)
        {
            if (Translations != null)
            {
                LanguageEnum currentLanguage = GetLanguage();

                if (Translations.TryGetValue((int)currentLanguage, out Dictionary<int, string> currentLanguateTranslations))
                {
                    if (currentLanguateTranslations.TryGetValue(id, out string result) && !string.IsNullOrWhiteSpace(result))
                    {
                        return result;
                    }
                }
            }

            return defaultValue;
        }
    }
}
