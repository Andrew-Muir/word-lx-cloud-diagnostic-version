﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfowareVSTO.Common
{
	public class ConfigSettings
	{
		public static string WebApiUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["WebApiUrl"];
            }
        }

		public static string InfowareKeyFolder
        {
            get
            {
                return ConfigurationManager.AppSettings["InfowareKeyFolder"];
            }
        }
	}
}
