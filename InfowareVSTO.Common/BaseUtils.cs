﻿using InfowareVSTO.Common.DTOs;
using InfowareVSTO.Common.Language;
using InfowareVSTO.Common.Models;
using Microsoft.Office.Core;
using Microsoft.Office.Interop.Outlook;
using Microsoft.Office.Interop.Word;
using Newtonsoft.Json;
using OpenXmlPowerTools;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Management;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Table = Microsoft.Office.Interop.Outlook.Table;

namespace InfowareVSTO.Common
{
    public class BaseUtils
	{
        private const string OUTLOOK_NOT_INSTALLED = "You do not have Outlook installed.";
        private const string AN_ERROR_HAS_OCCURRED = "An error has occured: ";
        private const string ESCAPED_COMMA_CHARACTER = "[WORDLX-ESCAPED-COMMA-CHARACTER]";

        public const string PHONE = "Phone";
        public const string FAX = "Fax";
		public static string[] VALID_TITLES = new string[] { "Ms", "Ms.", "Mrs", "Mrs.", "Miss", "Mr/Ms", "Mr/Ms.", "Mr./Ms.", "Mr/Mrs", "Mr/Mrs.", "Mr./Mrs.", "Mr/Miss", "Mr./Miss", "Mx", "Madame", "Mesdames", "Mr", "Mr.", "Mr/Ms", "Mr./Ms.", "Mr/Mrs", "Mr./Mrs.", "Mr/Miss", "Mr./Miss", "Mx." };

		public static string GetProcessorId()
		{
            string id = "";
            try
            {
                var mbs = new ManagementObjectSearcher("Select ProcessorId From Win32_processor");
                ManagementObjectCollection mbsList = mbs.Get();

                foreach (ManagementObject mo in mbsList)
                {
                    id = mo["ProcessorId"].ToString();
                    break;
                }
            }
            catch(NullReferenceException ex)
            {
                id = new HardwareId().GetHardwareID();
            }

			return id;
		}

		public static List<AuthorWithPathsDTO> GetOutlookContacts()
		{
            var contacts = new List<AuthorWithPathsDTO>();
            try
            {
                var outlook = new Microsoft.Office.Interop.Outlook.Application();
                NameSpace mapiNamespace = outlook.Application.GetNamespace("MAPI");

                foreach (Store store in mapiNamespace.Stores)
                {
                    try
                    {
                        var folder = store.GetDefaultFolder(OlDefaultFolders.olFolderContacts);
                        AddContacts(folder, ref contacts);
                        List<MAPIFolder> subFolders = GetAllSubFolders(folder).Where(x => x.DefaultItemType == OlItemType.olContactItem).ToList();
                        foreach (MAPIFolder subfolder in subFolders)
                        {
                            AddContacts(subfolder, ref contacts);
                        }
                    }
                    catch { continue; }
                }

                return contacts;
            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(AN_ERROR_HAS_OCCURRED + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                LogOutlookError(ex);
            }

            return null;
		}

        private static List<MAPIFolder> GetAllSubFolders(MAPIFolder input)
        {
            List<MAPIFolder> result = new List<MAPIFolder>();
            foreach (MAPIFolder folder in input.Folders)
            {
                result.Add(folder);
                result.AddRange(GetAllSubFolders(folder));
            }

            return result;
        }

        private static void AddContacts(MAPIFolder folder, ref List<AuthorWithPathsDTO> contacts)
        {
            Table tb = null;
            tb = folder.GetTable(null, OlItemType.olContactItem);

            tb.Columns.RemoveAll();
            tb.Columns.Add("FirstName");
            tb.Columns.Add("LastName");
            tb.Columns.Add("JobTitle");
            tb.Columns.Add("Email1Address");
            tb.Columns.Add("CompanyName");
            tb.Columns.Add("BusinessTelephoneNumber");
            tb.Columns.Add("HomeTelephoneNumber");
            tb.Columns.Add("OtherTelephoneNumber");
            tb.Columns.Add("BusinessFaxNumber");
            tb.Columns.Add("HomeFaxNumber");
            tb.Columns.Add("OtherFaxNumber");
            tb.Columns.Add("MobileTelephoneNumber");
            tb.Columns.Add("BusinessAddressCountry");
            tb.Columns.Add("BusinessAddressState");
            tb.Columns.Add("BusinessAddressCity");
            tb.Columns.Add("BusinessAddressStreet");
            tb.Columns.Add("BusinessAddressPostalCode");
            tb.Columns.Add("HomeAddressCountry");
            tb.Columns.Add("HomeAddressState");
            tb.Columns.Add("HomeAddressCity");
            tb.Columns.Add("HomeAddressStreet");
            tb.Columns.Add("HomeAddressPostalCode");
            tb.Columns.Add("OtherAddressCountry");
            tb.Columns.Add("OtherAddressState");
            tb.Columns.Add("OtherAddressCity");
            tb.Columns.Add("OtherAddressStreet");
            tb.Columns.Add("OtherAddressPostalCode");
			tb.Columns.Add("FullName");
			tb.Columns.Add("Title");
            tb.Columns.Add("Suffix");
            tb.Columns.Add("SelectedMailingAddress");
            tb.Columns.Add("Department");

            object[,] otb = tb.GetArray(100000) as object[,];
            int len = otb.GetUpperBound(0);

            for (int i = 0; i <= len; i++)
            {
                OlMailingAddress mailingAddress = OlMailingAddress.olNone;
                if (otb[i, 30] != null)
                {
                    mailingAddress = (OlMailingAddress)otb[i, 30];
                }
            
                List<AuthorAddressDTO> addresses = new List<AuthorAddressDTO>();
                List<AuthorAddressDTO> alternateAddresses = new List<AuthorAddressDTO>();
                AuthorAddressDTO business = new AuthorAddressDTO() {Name = "Business", Address1 = otb[i, 15]?.ToString(), City = otb[i, 14]?.ToString(), Country = otb[i, 12]?.ToString(), PostalCode = otb[i, 16]?.ToString(), Province = otb[i, 13]?.ToString() };
                AuthorAddressDTO home = new AuthorAddressDTO() { Name = "Home", Address1 = otb[i, 20]?.ToString(), City = otb[i, 19]?.ToString(), Country = otb[i, 17]?.ToString(), PostalCode = otb[i, 21]?.ToString(), Province = otb[i, 18]?.ToString() };
                AuthorAddressDTO other = new AuthorAddressDTO() { Name = "Other", Address1 = otb[i, 25]?.ToString(), City = otb[i, 24]?.ToString(), Country = otb[i, 22]?.ToString(), PostalCode = otb[i, 26]?.ToString(), Province = otb[i, 23]?.ToString() };


                if (mailingAddress == OlMailingAddress.olNone)
                {
                    if(!string.IsNullOrWhiteSpace(business.Address1) || !string.IsNullOrWhiteSpace(business.City))
                    {
                        mailingAddress = OlMailingAddress.olBusiness;
                    }
                    else if (!string.IsNullOrWhiteSpace(home.Address1) || !string.IsNullOrWhiteSpace(home.City))
                    {
                        mailingAddress = OlMailingAddress.olHome;
                    }
                    else if (!string.IsNullOrWhiteSpace(other.Address1) || !string.IsNullOrWhiteSpace(other.City))
                    {
                        mailingAddress = OlMailingAddress.olOther;
                    }
                    else
                    {
                        mailingAddress = OlMailingAddress.olBusiness;
                    }
                }


                switch (mailingAddress)
                {
                    case OlMailingAddress.olBusiness:
                        addresses = new List<AuthorAddressDTO>()
                        {
                            business
                        };
                        alternateAddresses = new List<AuthorAddressDTO>()
                        {
                            home,
                            other
                        };
                        break;
                    case OlMailingAddress.olHome:
                        addresses = new List<AuthorAddressDTO>()
                        {
                            home
                        };
                        alternateAddresses = new List<AuthorAddressDTO>()
                        {
                            business,
                            other
                        };
                        break;
                    case OlMailingAddress.olOther:
                        addresses = new List<AuthorAddressDTO>()
                        {
                            other
                        };
                        alternateAddresses = new List<AuthorAddressDTO>()
                        {
                            business,
                            home
                        };
                        break;
                }

                var contact = new AuthorWithPathsDTO()
                {
                    CompanyName = otb[i, 4]?.ToString(),
                    FirstName = otb[i, 0]?.ToString(),
                    LastName = otb[i, 1]?.ToString(),
                    JobTitle = otb[i, 2]?.ToString(),
                    Prefix = otb[i, 28]?.ToString(),
                    Suffix = otb[i, 29]?.ToString(),
                    Department = otb[i,31]?.ToString(),
                    Companies = new List<ACompanyDTO>() { new ACompanyDTO() { Name = otb[i, 4]?.ToString() } },
                    ContactInformations = new List<AContactInformationDTO>() { new AContactInformationDTO() { Type = PHONE, Details =  (otb[i, 5] ?? otb[i, 6] ?? otb[i, 7])?.ToString()},
                                                new AContactInformationDTO(){ Type = FAX, Details =  (otb[i, 8] ??  otb[i, 9]?? otb[i, 10])?.ToString()} },
                    Addresses = addresses,
                    AlternateAddresses = alternateAddresses };

                if (!string.IsNullOrWhiteSpace(contact.FirstName) || !string.IsNullOrWhiteSpace(contact.LastName) || !string.IsNullOrWhiteSpace(contact.CompanyName))
                {
                    contacts.Add(contact);
                }
            }
        }

        private static void LogOutlookError(System.Exception ex)
        {
            string log = "Outlook Error: " + ex.GetType().ToString() + " " + ex.Message + "\n";
            log += "Main StackTrace:" + ex.StackTrace + "\n";
            System.Exception inner = ex.InnerException;

            while (inner != null)
            {
                log += "Inner Exception:" + ex.GetType().ToString() + " " + ex.Message + "\n";
                log += "Inner Exception StackTrace:" + ex.StackTrace + "\n";
                inner = inner.InnerException;
            }

            Logger.Log(log);
        }

        public static List<AuthorWithPathsDTO> GetOutlookContactsByType(OutlookSourceType sourceType, string currentAuthorEmail, string currentAuthorFirstName, string currentAuthorLastName)
        {
            if (sourceType == OutlookSourceType.Shared)
            {
                return GetSharedOutlookContacts();
            }
            if (sourceType == OutlookSourceType.CurrentAuthor)
            {
                if (currentAuthorEmail == null)
                {
                    return new List<AuthorWithPathsDTO>();
                }
                else
                {
                    currentAuthorEmail = currentAuthorEmail.Trim();
                }
            }

            var contacts = new List<AuthorWithPathsDTO>();
            try
            {
                var outlook = new Microsoft.Office.Interop.Outlook.Application();
                NameSpace mapiNamespace = outlook.Application.GetNamespace("MAPI");

                Store defaultStore = mapiNamespace.DefaultStore;
                List<Store> stores = new List<Store>();
                if (sourceType == OutlookSourceType.Personal)
                {
                    stores.Add(defaultStore);
                }
                else if (sourceType == OutlookSourceType.CurrentAuthor)
                {
                    foreach (Store store in mapiNamespace.Stores)
                    {
                        if (store.DisplayName == currentAuthorEmail)
                        {
                            stores.Add(store);
                        }
                    }
                }
                else
                {
                    foreach (Store store in mapiNamespace.Stores)
                    {
                        if (store.DisplayName != currentAuthorEmail && store.DisplayName != defaultStore.DisplayName)
                        {
                            stores.Add(store);
                        }
                    }
                }

                foreach (Store store in stores)
                {
                    try
                    {
                        var folder = store.GetDefaultFolder(OlDefaultFolders.olFolderContacts);

                        AddContacts(folder, ref contacts);

                        List<MAPIFolder> subFolders = GetAllSubFolders(folder).Where(x => x.DefaultItemType == OlItemType.olContactItem).ToList();
                        foreach (MAPIFolder subfolder in subFolders)
                        {
                            AddContacts(subfolder, ref contacts);
                        }

                    }
                    catch { continue; }
                }

                return contacts;
            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(AN_ERROR_HAS_OCCURRED + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                LogOutlookError(ex);
            }

            return null;
        }

        public static List<AuthorWithPathsDTO> GetSharedOutlookContacts()
        {
            var contacts = new List<AuthorWithPathsDTO>();

            try
            {
                Microsoft.Office.Interop.Outlook.Application outlookObj = null;
                Process process = null;
                try
                {
                    outlookObj = GetActiveOutlookObject(ref process);
                }
                catch (System.Exception ex)
                {
                    KillOutlookProcess();
                    Thread.Sleep(300);
                    outlookObj = GetActiveOutlookObject(ref process);
                }

                int retries = 0;

                while(outlookObj.ActiveExplorer() == null && retries < 5)
                {
                    KillOutlookProcess();
                    Thread.Sleep(200);
                    outlookObj = GetActiveOutlookObject(ref process);
                    retries++;
                }

                ContactsModule module = (ContactsModule)outlookObj.ActiveExplorer().NavigationPane.Modules.GetNavigationModule(OlNavigationModuleType.olModuleContacts);
                foreach (NavigationGroup navigationGroup in module.NavigationGroups)
                {
                    if (navigationGroup.GroupType == OlGroupType.olPeopleFoldersGroup || navigationGroup.GroupType == OlGroupType.olOtherFoldersGroup)
                    {
                        foreach (NavigationFolder navigationFolder in navigationGroup.NavigationFolders)
                        {
                            try
                            {
                                var folderName = navigationFolder.DisplayName;
                                AddContacts(navigationFolder.Folder, ref contacts);
                            }
                            catch (System.Exception ex)
                            {
                            }
                        }
                    }
                }
                if (process != null)
                {
                    KillOutlookProcess();
                }

            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(AN_ERROR_HAS_OCCURRED + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                LogOutlookError(ex);
            }

            return contacts;
        }

        private static void KillOutlookProcess()
        {
            var processes = Process.GetProcessesByName("OUTLOOK");

            for (int i = processes.Length - 1; i >= 0; i--)
            {
                processes[i].Kill();
            }
        }

        private static Microsoft.Office.Interop.Outlook.Application GetActiveOutlookObject(ref Process process)
        {
            Microsoft.Office.Interop.Outlook.Application outlookObj;
            try
            {
                outlookObj = (Microsoft.Office.Interop.Outlook.Application)Marshal.GetActiveObject("Outlook.Application");
            }
            catch (System.Exception ex)
            {
                var startInfo = new ProcessStartInfo
                {
                    FileName = "outlook.exe",
                    UseShellExecute = true,
                    WindowStyle = ProcessWindowStyle.Minimized
                };

                process = Process.Start(startInfo);

                process.WaitForInputIdle();
                BringCurrentApplicationIntoFront();
                outlookObj = new Microsoft.Office.Interop.Outlook.Application();
            }

            return outlookObj;
        }

        [DllImport("USER32.DLL")]
        public static extern bool SetForegroundWindow(IntPtr hWnd);

        private static void BringCurrentApplicationIntoFront()
        {
            Process current = Process.GetCurrentProcess();
            IntPtr s = current.MainWindowHandle;
            SetForegroundWindow(s);
        }

        private static bool IncludeName(string senderName, string currentAuthorLastName, string currentAuthorFirstName, bool include)
        {
            if (!currentAuthorLastName.IsEmpty() && !currentAuthorFirstName.IsEmpty())
            {
                var senderArr = senderName.ToLower().Split(' ');
                if (senderArr.Contains(currentAuthorFirstName.ToLower().Trim()) && senderArr.Contains(currentAuthorLastName.ToLower().Trim()))
                {
                    return include;
                }
            }

            return !include;
        }

        public static string GetContactFirstInformation(List<AContactInformationDTO> contactInformations, string type)
        {
            if (contactInformations == null)
                return null;
            var ci = contactInformations.Where(x => x.Type == type).FirstOrDefault();
            if (ci != null)
                return ci.Details;
            else
                return null;
        }

        /// <summary>
        /// This is only used for Outlook import conversion. Other objects should not have these values
        /// </summary>
        /// <param name="authorWithPathsDTO"></param>
        /// <returns></returns>
        public static AuthorWithPathsDTO ParseAuthorDTOToWithPaths(AuthorDTO authorDTO)
        {
            AuthorWithPathsDTO authorWithPathsDTO = new AuthorWithPathsDTO();
            authorWithPathsDTO.FirstName = authorDTO.FirstName;
            authorWithPathsDTO.LastName = authorDTO.LastName;
            authorWithPathsDTO.JobTitle = authorDTO.JobTitle;
            if (!authorDTO.Company.IsEmpty())
            {
                var aCompanyDTOs = new List<ACompanyDTO>();
                aCompanyDTOs.Add(new ACompanyDTO() { Name = authorDTO.Company });
                authorWithPathsDTO.Companies = aCompanyDTOs;
            }
            authorWithPathsDTO.ContactInformations = new List<AContactInformationDTO>();
            if (!authorDTO.Phone.IsEmpty())
            {
                authorWithPathsDTO.ContactInformations.Add(new AContactInformationDTO() { Type = "Phone", Details = authorDTO.Phone });
            }
            if (!authorDTO.Fax.IsEmpty())
            {
                authorWithPathsDTO.ContactInformations.Add(new AContactInformationDTO() { Type = "Fax", Details = authorDTO.Fax });
            }
            return authorWithPathsDTO;
        }

        public static AuthorAddressDTO GetAuthorAddress(AuthorWithPathsDTO author)
        {
            if (author.Addresses.Any(x => x.IsDefault))
            {
                return author.Addresses.FirstOrDefault(x => x.IsDefault);
            }
            else
            {
                return author.Addresses.FirstOrDefault();
            }
        }

        public static AuthorAddressDTO GetAuthorAddress(SelectedContact contact)
        {
            if (contact.Addresses != null)
            {
                if (contact.Addresses.Any(x => x.IsDefault))
                {
                    return contact.Addresses.FirstOrDefault(x => x.IsDefault);
                }
                else
                {
                    return contact.Addresses.FirstOrDefault();
                }
            }

            return null;
        }

        public static string GetAttentionLine(SelectedContact recipient, JobTitleVisibility jobTitleVisibility, bool departmentVisibility = false)
        {
            var attention = new List<string>();
            string fullName = GetFullName(recipient);

            if (!string.IsNullOrWhiteSpace(fullName))
            {
                attention.Add(fullName);
            }

            if (((int)jobTitleVisibility) > 0 && !string.IsNullOrEmpty(recipient.JobTitle))
            {
                attention.Add(recipient.JobTitle);
            }

            string result = string.Join(", ", attention);

            if(departmentVisibility && !string.IsNullOrWhiteSpace(recipient.Department))
            {
                result += "\v" + recipient.Department;
            }

            return result;
        }

        public static string GetFullName(SelectedContact recipient)
        {
            var fullName = new List<string>();
            if (!recipient.Prefix.IsEmpty())
            {
                fullName.Add(recipient.Prefix);
            }
            if (!recipient.FirstName.IsEmpty())
            {
                fullName.Add(recipient.FirstName);
            }
            if (!recipient.LastName.IsEmpty())
            {
                fullName.Add(recipient.LastName);
            }
            if (!recipient.Suffix.IsEmpty())
            {
                fullName.Add(recipient.Suffix);
            }
            return string.Join(" ", fullName.ToArray());
        }

        public static ImageSource GetStandardWordNotificationIconsSource(Icon icon)
        {
            if(icon != null)
            {
                return Imaging.CreateBitmapSourceFromHIcon(icon.Handle, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
            }
            return null;
        }

        public static string GetRecipientTextFirstRow(string text)
        {
            if (!string.IsNullOrWhiteSpace(text))
            {
                return text.Split('\v').FirstOrDefault().Trim();
            }
            return null;
        }

        public static List<Enclosure> ParseEnclosureSettings(string enclosureSettings)
        {
            if (!string.IsNullOrEmpty(enclosureSettings))
            {
                var enclosures = enclosureSettings.Split(';').ToList();
                if (enclosures.Count > 0)
                {
                    var enclosureSettingOptions = new List<Enclosure>();
                    foreach (var enclosure in enclosures)
                    {
                        var enclosureRow = UnescapeCommaCharacter(enclosure.Split(',')).ToList();
                        if (enclosureRow.Count > 0)
                        {
                            var enclosurePluralized = new Enclosure
                            {
                                SingularValue = enclosureRow.FirstOrDefault() ?? null,
                                PluralValue = enclosureRow.LastOrDefault() ?? null
                            };
                            enclosureSettingOptions.Add(enclosurePluralized);
                        }
                    }
                    return enclosureSettingOptions;
                }
            }
            return null;
        }

        public static string UnescapeCommaCharacter(string input)
        {
            if (input != null)
            {
                return input.Replace(ESCAPED_COMMA_CHARACTER, ",");
            }

            return input;
        }

        public static string[] UnescapeCommaCharacter(string[] input)
        {
            if (input != null)
            {
                for (int i = 0; i < input.Length; i++)
                {
                    input[i] = UnescapeCommaCharacter(input[i]);
                }
            }

            return input;
        }

        public static List<KeyValuePair<string, string>> GetEnclosureOptions(List<Enclosure> enclosures)
        {
            List<KeyValuePair<string, string>> result = new List<KeyValuePair<string, string>>();
            result.Add(new KeyValuePair<string, string>(LanguageManager.GetTranslation(LanguageConstants.None, "None"), ""));
            if (enclosures != null && enclosures.Count > 0)
            {
                var enclosureOptions = new List<string>();
         
                foreach (var enclosure in enclosures)
                {
                    if (!string.IsNullOrEmpty(enclosure.SingularValue))
                    {
                        enclosureOptions.Add(enclosure.SingularValue);
                    }
                    if (!string.IsNullOrEmpty(enclosure.PluralValue))
                    {
                        enclosureOptions.Add(enclosure.PluralValue);
                    }
                }

                result.AddRange( enclosureOptions.Select(x => new KeyValuePair<string, string>(x,x)).ToList());
                return result;
            }
            return null;
        }

        public static bool CheckIfTemplateAlreadyExists(string name, TemplateType? templateType)
        {
            var authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null);
            if (authKey != null && !string.IsNullOrEmpty(name) && templateType != null)
            {
                var relativeUrl = $"api/WebApi/GetTemplateNameExists?authKey={authKey}&templateName={name}&type={(int)templateType.Value}";
                var request = new RestRequest(relativeUrl, Method.GET);

                try
                {
                    var client = new RestClient(ConfigSettings.WebApiUrl);
                    var response = client.Execute(request);
                    var content = JsonConvert.DeserializeObject<ApiResponseDTO>(response.Content);
                    if ((content.StatusCode == 200 || content.StatusCode == 300) && content.Data.ToString() == "1")
                    {
                        return true;
                    }
                }
                catch (JsonReaderException)
                {
                    return false;
                }
            }
            return false;
        }

        public static string AddDocxToFilename(string fileName)
        {
            if (!fileName.EndsWith(".docx"))
            {
                fileName += ".docx";
            }
            return fileName;
        }

        public static UserFullName GetFirstRecipientFirstAndLastName(string firstRecipientsRow)
        {
            if (!string.IsNullOrEmpty(firstRecipientsRow))
            {
                int commaIndex = firstRecipientsRow.IndexOf(",");
                if(commaIndex > 0)
                {
                    firstRecipientsRow = firstRecipientsRow.Substring(0, commaIndex);
                }

                var fullName = firstRecipientsRow.Split(' ').ToList();
                if (fullName != null && fullName.Count > 0)
                {
					var fullUserName = new UserFullName();
					if (VALID_TITLES.Contains(fullName[0].Trim()))
					{
						fullUserName.Title = fullName[0].Trim();
						fullName.RemoveAt(0);
					}

					fullUserName.FirstName = fullName.FirstOrDefault() ?? "";

                    if (fullName.Count > 1)
                    {
                        fullUserName.LastName = string.Join(" ", fullName.Skip(1)) ?? "";
                    }
                    return fullUserName;
                }
            }
            return null;
        }

        public static void CreateDiaryDateOutlookTask(PaneDTO letterDTO, DateTime? date)
        {
            if (date != null)
            {
                var outlookObj = new Microsoft.Office.Interop.Outlook.Application();
                if (outlookObj != null)
                {
                    TaskItem taskItem = outlookObj.CreateItem(OlItemType.olTaskItem);
                    taskItem.Subject = letterDTO.ReLine;
                    taskItem.ReminderSet = true;
                    taskItem.StartDate = date.Value;
                    taskItem.ContactNames = letterDTO.Recipients.Split(new string[] { "\v" }, StringSplitOptions.RemoveEmptyEntries).FirstOrDefault();
                    taskItem.Save();
                }
            }
        }

        public static T FindChild<T>(DependencyObject parent, string childName) where T : DependencyObject
        {
            if (parent == null) return null;

            T foundChild = null;

            int childrenCount = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < childrenCount; i++)
            {
                var child = VisualTreeHelper.GetChild(parent, i);
                T childType = child as T;
                if (childType == null)
                {
                    foundChild = FindChild<T>(child, childName);

                    if (foundChild != null) break;
                }
                else if (!string.IsNullOrEmpty(childName))
                {
                    var frameworkElement = child as FrameworkElement;
                    if (frameworkElement != null && frameworkElement.Name == childName)
                    {
                        foundChild = (T)child;
                        break;
                    }
                }
                else
                {
                    foundChild = (T)child;
                    break;
                }
            }
            return foundChild;
        }

        public static string ImplodeNameList(List<string> nameList)
        {
            var nameListString = string.Empty;

            if(nameList != null && nameList.Count > 0)
            {
                nameListString = string.Join(" ", nameList.Where(x => !x.IsEmpty()));
            }

            return nameListString;
        }

        public static void HideUnhideSeparator(List<Microsoft.Office.Interop.Word.ContentControl> contentControls)
        {
            if (contentControls != null)
            {
                var separatorContentControls = contentControls.Where(x => x.Title == "Separator");
                if (separatorContentControls != null && separatorContentControls.Count() > 0)
                {
                    foreach (var separatorContentControl in separatorContentControls)
                    {
                        if (separatorContentControl != null && separatorContentControl.Range.Paragraphs.Count > 0)
                        {
                            var separatorParagraph = separatorContentControl.Range.Paragraphs[1];
                            if (separatorParagraph?.Range?.ContentControls != null)
                            {
                                var paragraphContentControls = new List<Microsoft.Office.Interop.Word.ContentControl>();

                                foreach (Microsoft.Office.Interop.Word.ContentControl contentControl in separatorParagraph.Range.ContentControls)
                                {
                                    if (contentControl.Tag == CustomConstants.CONTENT_CONTROL_TAG)
                                    {
                                        paragraphContentControls.Add(contentControl);
                                    }
                                }
                                                                
                                var separatorPosition = paragraphContentControls.FindIndex(x => x.ID == separatorContentControl.ID);
                                if (separatorPosition != -1)
                                {
                                    var previousContentControl = paragraphContentControls.ElementAtOrDefault(separatorPosition - 1);
                                    var nextContentControl = paragraphContentControls.ElementAtOrDefault(separatorPosition + 1);

                                    if (string.IsNullOrWhiteSpace(previousContentControl?.Range?.Text) || string.IsNullOrWhiteSpace(nextContentControl?.Range?.Text))
                                    {
                                        separatorContentControl.Range.Font.Hidden = -1;
                                    }
                                    else
                                    {
                                        separatorContentControl.Range.Font.Hidden = 0;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        public static void UnhideRow(List<Microsoft.Office.Interop.Word.ContentControl> contentControls, List<string> fillAndHideContentControlTitles)
        {
            if (contentControls != null && contentControls.Count() > 0)
            {
                foreach (var contentControl in contentControls)
                {
                    if(fillAndHideContentControlTitles.Contains(contentControl.Title))
                    {
                        var row = GetLineOfRange(contentControl.Range);
                       
                        if (CheckToUnHideRow(contentControl.Range))
                        {
                            row.Font.Hidden = 0;
                        }                        
                    }
                }
            }
        }

        public static bool CheckToUnHideRow(Range range)
        {
            bool unhideRow = false;

            if(range != null && range.Paragraphs.Count > 0)
            {
                if(range.Paragraphs[1]?.Range?.ContentControls != null && range.Paragraphs[1].Range.ContentControls.Count > 0)
                {
                    var totalContentControlsCount = 0;
                    var emptyContentControlsCount = 0;
                    var separatorCount = 0;

                    foreach (Microsoft.Office.Interop.Word.ContentControl contentControl in range.Paragraphs[1].Range.ContentControls)
                    {
                        if(contentControl.Tag == CustomConstants.CONTENT_CONTROL_TAG)
                        {
                            if(contentControl.Title == "Separator")
                            {
                                ++separatorCount;
                            }
                            else
                            {
                                if(string.IsNullOrWhiteSpace(contentControl.Range.Text) || contentControl.Range.Text == contentControl.Title)
                                {
                                    ++emptyContentControlsCount;
                                }

                                ++totalContentControlsCount;
                            }
                        }
                    }

                    if (separatorCount > 0 && totalContentControlsCount != emptyContentControlsCount)
                    {
                        unhideRow = true;
                    }                    
                }
            }

            return unhideRow;
        }

        public static Range GetLineOfRange(Range input)
        {
            var result = input.Duplicate;
            result.MoveEnd(WdUnits.wdCharacter, -1);
            result.MoveStartUntil("\v\r\n\a", WdConstants.wdBackward);

            try
            {
                if (input.Paragraphs.First.Range.Start == 0)
                {
                    result.Start = 0;
                }
            }
            catch(System.Exception ex)
            {
            }


            result.MoveEndUntil("\v\r\n\a", WdConstants.wdForward);

            Range previous, next;
            previous = result.Previous(WdUnits.wdCharacter);
            next = result.Next(WdUnits.wdCharacter);
            bool? moveDirection = GetMoveDirection(previous, next, result.Font.Hidden == -1 ? 0 : -1);

            if (moveDirection == false)
            {
                result.MoveStart(WdUnits.wdCharacter, -1);
            }

            if (moveDirection == true)
            {
                result.MoveEnd(WdUnits.wdCharacter, 1);
            }

            if (result == null)
            {
                return input.Paragraphs[1].Range;
            }

            return result;
        }

        public static IEnumerable<TSource> DistinctBy<TSource, TKey>(IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
        {
            HashSet<TKey> seenKeys = new HashSet<TKey>();
            foreach (TSource element in source)
            {
                if (seenKeys.Add(keySelector(element)))
                {
                    yield return element;
                }
            }
        }

        //true = forward
        //false = backwards
        //null = none
        private static bool? GetMoveDirection(Range previous, Range next, int hidden)
        {
            string previousText = previous?.Text;
            string nextText = next?.Text;

            if ((string.IsNullOrEmpty(previousText) || previousText == "\a" || previousText == "\r\a") && (string.IsNullOrEmpty(nextText) || nextText == "\a" || nextText == "\r\a"))
            {
                return null;
            }

            if (string.IsNullOrEmpty(previousText) || previousText == "\a" || previousText == "\r\a")
            {
                return true;
            }

            if (string.IsNullOrEmpty(nextText) || nextText == "\a" || nextText == "\r\a")
            {
                return false;
            }

            if (previous.Font.Hidden == -1 && next.Font.Hidden == -1 && hidden == 0)
            {
                return true;
            }

            if (previous.Font.Hidden == hidden)
            {
                return true;
            }
            if (next.Font.Hidden == hidden)
            {
                return false;
            }

            return !((nextText == "\r" || nextText == "\n" || nextText == "\r\n") && (previousText == "\v" || previousText == "\v\n"));
        } 

        public static string PrepareJSONForDeserialization(string json)
        {
            if (json!= null && json.Length > 0 && json[0] == (char)65279)
            {
                return json.Substring(1);
            }

            return json;
        }
        public static string ReadProperty(Microsoft.Office.Interop.Word.Document document, string propertyName)
        {
            DocumentProperties properties = document.CustomDocumentProperties;

            if (properties != null)
            {
                foreach (DocumentProperty property in properties)
                {
                    if (property.Name.Equals(propertyName))
                    {
                        return property.Value.ToString();
                    }
                }
            }

            return null;
        }

        public static void RemoveRecipientsNotInTextBox(List<SelectedContact> recipients, string textBoxText, string attentionText = "")
        {
            if (recipients != null)
            {
                List<SelectedContact> toDelete = new List<SelectedContact>();

                foreach (var recip in recipients)
                {
                    string searchCriteria = string.Empty;
                    if (!recip.LastName.IsEmpty() || !recip.FirstName.IsEmpty())
                    {
                        if (!recip.FirstName.IsEmpty())
                        {
                            searchCriteria += recip.FirstName;
                        }

                        if (!recip.LastName.IsEmpty())
                        {
                            searchCriteria += searchCriteria.IsEmpty() ? recip.LastName : (" " + recip.LastName);
                        }
                    }
                    else
                    {
                        searchCriteria = recip.CompanyName;
                    }

                    if (!searchCriteria.IsEmpty() && !textBoxText.ToLower().Contains(searchCriteria.ToLower()) && !(attentionText?.ToLower()?.Contains(searchCriteria.ToLower()) ?? false))
                    {
                        toDelete.Add(recip);
                    }
                }

                foreach (var recip in toDelete)
                {
                    recipients.Remove(recip);
                }
            }
        }

        //Trims only from the start.
        public static string TrimNonAlphaNumeric(string input)
        {
            if (input == null)
            {
                return null;
            }

            return Regex.Replace(input, "^[^a-zA-Z0-9\\s]+", string.Empty);
        }

        public static void RemoveContactsFromTextBoxes(List<SelectedContact> contactsToRemove, System.Windows.Controls.TextBox recipientTextBox, System.Windows.Controls.TextBox attentionTextBox = null, List<SelectedContact> addedRecipients = null, bool includesAddress = true)
        {
            if (contactsToRemove != null && contactsToRemove.Count > 0 && recipientTextBox != null)
            {
                foreach (SelectedContact contactToRemove in contactsToRemove)
                {
                    if (includesAddress)
                    {
                        if (attentionTextBox != null && addedRecipients != null && RecipientFound(attentionTextBox.Text, contactToRemove))
                        {
                            RemoveMatchingRecipientBlock(recipientTextBox, attentionTextBox, contactToRemove, addedRecipients);
                            RemoveAttention(attentionTextBox, contactToRemove);
                        }
                        else
                        {
                            RemoveRecipientBlock(recipientTextBox,contactToRemove);
                        }
                    }
                    else
                    {
                        RemoveRecipientLines(recipientTextBox, contactToRemove);
                    }
                }
            }
        }

        private static void RemoveRecipientLines(System.Windows.Controls.TextBox recipientTextBox, SelectedContact contactToRemove)
        {
            List<string> strArr =  recipientTextBox.Text.Split(new string[] { "\v", "\r\n", "\r", "\n" }, StringSplitOptions.None).ToList();

            string text = strArr.Where(x => x.Contains(GetName(contactToRemove))).FirstOrDefault();

            if (text != null)
            {
                if (strArr.IndexOf(text) < strArr.Count - 1 && strArr[strArr.IndexOf(text) + 1].Contains(contactToRemove.CompanyName))
                {
                    strArr.RemoveAt(strArr.IndexOf(text) + 1);
                }

                strArr.Remove(text);

                recipientTextBox.Text = string.Join("\r\n", strArr.ToArray());
            }
        }

        private static void RemoveRecipientBlock(System.Windows.Controls.TextBox recipientTextBox, SelectedContact contactToRemove)
        {
            List<string> strArr =  recipientTextBox.Text.Split(new string[] { "\r\r", "\v\v", "\n\n", "\r\n\r\n" }, StringSplitOptions.None).ToList();
            
            string text = strArr.Where(x => x.Contains(GetName(contactToRemove))).FirstOrDefault();
            if (text != null)
            {
                strArr.Remove(text);

                recipientTextBox.Text = string.Join("\r\n\r\n", strArr.ToArray());
            }
        }

        private static void RemoveAttention(System.Windows.Controls.TextBox attentionTextBox, SelectedContact contactToRemove)
        {
            List<string> strArr = attentionTextBox.Text.Split(new string[] { "\r\n", "\n", "\r" }, StringSplitOptions.None).ToList();

            string text = strArr.Where(x => x.Contains(GetName(contactToRemove))).FirstOrDefault();
            if (text != null)
            {
                strArr.Remove(text);

                attentionTextBox.Text = string.Join("\r\n", strArr.ToArray());
            }
        }

        private static void RemoveMatchingRecipientBlock(System.Windows.Controls.TextBox recipientTextBox, System.Windows.Controls.TextBox attentionTextBox, SelectedContact contactToRemove, List<SelectedContact> addedRecipients)
        {
            List<string> strArr = attentionTextBox.Text.Split(new string[] { "\r\n", "\v", "\n", "\r" }, StringSplitOptions.None).ToList();

            string text = strArr.Where(x => x.Contains(GetName(contactToRemove))).FirstOrDefault();
            if (text != null)
            {
                int position = strArr.IndexOf(text);

                List<string> recipList = recipientTextBox.Text.Split(new string[] { "\r\r", "\v\v", "\n\n", "\r\n\r\n" }, StringSplitOptions.None).ToList();
                int i = 0;
                foreach (string recip in recipList)
                {
                    foreach(SelectedContact contact in addedRecipients)
                    {
                        if (recip.Contains(GetName(contact)))
                        {
                            i--;
                            break;
                        }
                    }

                    if (i == position)
                    {
                        recipList.Remove(recip);
                        recipientTextBox.Text = string.Join("\r\n\r\n", recipList.ToArray());

                        return;
                    }
                    i++;
                }

            }
        }

        private static bool RecipientFound(string text, SelectedContact contactToRemove)
        {
            return text?.Contains(GetName(contactToRemove)) == true;
        }

        private static string GetName(SelectedContact contact)
        {
            return string.Join(" ", new List<string>() { contact.FirstName, contact.LastName }.Where(x => !string.IsNullOrWhiteSpace(x)).ToArray());
        }
    }
}
