﻿using Microsoft.Office.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfowareVSTO.Common.MultiLanguage
{
    public class MLanguage
    {
        public MsoLanguageID Id { get; set; }

        public string Name { get; set; }

        public string DisplayName { get; set; }
    }
}
