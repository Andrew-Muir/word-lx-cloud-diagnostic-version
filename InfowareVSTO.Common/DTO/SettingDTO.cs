﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfowareVSTO.Common.DTO
{
    public class SettingDTO
    {
        public List<Section> Sections { get; set; }
    }

    public class Section
    {
        public string SectionName { get; set; }
        public Dictionary<string, string> KeyValues { get; set; }
    }
}
