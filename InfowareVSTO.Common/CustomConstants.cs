﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfowareVSTO.Common
{
	public class CustomConstants
	{
		public const string ProgramDataTemplatesPath = @"C:\ProgramData\Infoware\Templates\4105\";

		public const string DefaultPrinter = "Default Printer";

		public const string DefaultPrinterBin = "Printer Default Bin";

		public const string LETTERHEAD_TAG = "LX-LETTERHEAD";

        public const string LETTERHEAD_ON = "LX-LETTERHEAD-ON";
        
        public const string TemplateTags = "LX-TEMPLATE";

        public const string TemplateTypeTags = "LX-TEMPLATE-TYPE";
    
        public const int LIST_LEVEL_MAX_POSITIONS_VALUE_IN_INCHES = 22;

        public const int LIST_LEVEL_MAX_POSITIONS_VALUE_IN_DP = 1584;

        public const string ADDENDA_STYLE = "Addenda";

        public const string ADDENDA_CONTENT_CONTROL_TITLE = "Addenda title";

        public const string ADDENDA_BOOKMARK_PREFIX = "Addenda";

        public const string ADDENDA_TYPE_APPENDIX_TITLE = "Appendix";

        public const string ADDENDA_TYPE_EXHIBIT_TITLE = "Exhibit";

        public const string ADDENDA_TYPE_SCHEDULE_TITLE = "Schedule";

        public const string ADDENDA_TYPE_NUMBERING_SCHEDULE_TITLE = "Schedule numbering";

        public const string ADDENDA_CHARACTER_STYLE = "Addenda character";

        public const string ADDENDA_HYPERLINK_CHARACTER_STYLE = "Hyperlink";
        public const string ADDENDA_BOOKMARK_SOURCE_PARAGRAPH = "Source";

        public const string ADDENDA_NUMBER_IN_PAGE_NUMBER_TAG = "AddendaNumberInPageNumber";

        public const string PAGE_NUMBERING_TAG = "LX-PAGENUMBERING";

        public const string TOC_TITLE_STYLE_NAME = "TocTitleStyle";

        public const int MARGIN_MAX_IN_DP = 1584;

        public const string FILE_NUMBER_TAG = "File No.";

        public const string CONTENT_CONTROL_TAG = "LX-TEMPLATE";

        public const bool DELETE_FILE_NO_ROW = false;

        public const int UNDEFINED_VALUE = 9999999;

        public const string USE_ATTENTION_LINE = "USE_ATTENTION_LINE";

        public const string LAW_SOC_NR_LABEL = "LS #:";

        public const string FROM_INFORM_PROPERTY = "FromInFORM";

        public const string FIXED_CLAUSE_TAG = "LX-FIXEDCLAUSE";
        public const string FIXED_CLAUSE_GROUP_TAG = "LX-FIXEDCLAUSEGROUP";
        public const string FIXED_CLAUSE_LIBRARY = "FCLibrary~";
        public const string FIXED_CLAUSE_GROUP = "FCGroup~";
        public const string FIXED_CLAUSE_CLAUSE = "FCClause~";
        public const string FIXED_CLAUSE_UNFORMATTED = "FCUnformatted~";
        public const string CUSTOM_RECIPIENT_BLOCK = "Custom Recipient Block";
        public const string CUSTOM_RECIPIENT_BLOCK_COPY = "Custom Recipient Block - Copy";
        public const string MATTER_MASTER_TAG = "LX-MATTERMASTER";

        public const string ROW_EXPANDED = "LX-INFORM-ROWEXPANDED";
        public const string THIRD_PARTY_ADDED = "LX-INFORM-THIRDPARTYADDED";
        public const string ERB_NAME = "MM_ERB";
        public const string ERB_BLANK_LINE = "MM_BlankLine";

        public const string SPECIAL_HANDLING_BOOKMARK = "LX_SPCHND_";
        public const string SPECIAL_HANDLING_LETTERHEAD = "_LX_LTRH_";
    }
}
