﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfowareVSTO.Common.OfflineMode
{
    public class PreCacheManager
    {
        private const string PRE_CACHE_REG_KEY = "LastPreCache";
        private const string PRE_CACHE_AUTHKEY_REG_KEY = "LastPreCacheAuthKey";
       
        private const int INTERVAL_DAYS = 7;
        

        private List<string> PreCacheWebMethods { get; set; } = new List<string>()
        {
            "DownloadTemplate",
            "GetTemplates",
            "DownloadCompanyNameTemplateAsXml",
            "GetACompany", 
            "ConvertTOOOXMLAndDownloadTemplate",
            "GetCompanyLogo", 
            "GetAuthor",
            "GetAuthors",
            "GetAddress",
            "GetAddresses", 
            "GetAuthorLocations", 
            "GetTemplateACompany",
            "GetSigningLinePreviews",
            "GetSigningLineHash"
        };

        private List<string> SimplePreCacheWebMethods { get; set; } = new List<string>()
        {
            "GetCompanySettings",
            "DownloadCompanyAddendaExhibitTemplateAsXml",
            "DownloadCoverPageTemplateAsXml",
            "GetCurrentACompany",
            "DownloadCompanyFirmListStructure",
            "GetIsValidAuthKeyWithTest",
            "DownloadSigningLineTemplateAsXml",
            "GetACompanies",
            "GetCurrentACompany",
            "GetCurrentLocation",
            "GetCurrentACompany",
            "GetCurrentAuthor",
            "GetCurrentAssistantId",
            "GetFavouriteAuthors",
            "GetAllTemplates",
            "GetFavoriteTemplates",
            "DownloadTopDraftDateStampAsXml",
            "DownloadLeftDraftDateStampAsXml",
            "DownloadRightDraftDateStampAsXml",
            "GetCompanyMultiPrintSettings"
        };
        private List<string> SimplePreCacheWebMethodsByteArr { get; set; } = new List<string>()
        {
            "DownloadCompanyFirmListDocument"
        };
        private List<string> SimplePreCacheWebMethodsApiResponse { get; set; } = new List<string>()
        {
            "GetParagraphNumberingSchemes"
        };
        private List<string> NoParamterPreCacheWebMethods { get; set; } = new List<string>()
        {
           "DownloadTranslations"
        };

        private static PreCacheManager instance;
        public static PreCacheManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new PreCacheManager();
                }

                return instance;
            }
        }

        public void UpdateIfNeeded()
        {
            DateTime? lastPreCache = GetUpdateTimeStamp();

            if (OfflineModeManager.DeleteCacheIfNeeded())
            {
                lastPreCache = null;
            }

            LastPreCacheAuthKey authKey = GetLastPreCacheAuthKey();

            if (authKey != LastPreCacheAuthKey.NoAuthKey && (lastPreCache == null || lastPreCache.Value.AddDays(INTERVAL_DAYS) < DateTime.Now))
            {
                if (authKey == LastPreCacheAuthKey.DifferentAuthKey)
                {
                    lastPreCache = null;
                }

                PreCacheAll(lastPreCache);

                SetUpdateTimeStamp(DateTime.Now);
            }
        }

        private void PreCacheAll(DateTime? lastUpdate = null)
        {
            foreach (string webMethod in PreCacheWebMethods)
            {
                string responseListStr = AdminPanelWebApi.PreCacheWebMethod(webMethod, lastUpdate);
                if (responseListStr != null)
                {
                    try
                    {
                        MethodWebserviceResponses responses = JsonConvert.DeserializeObject<MethodWebserviceResponses>(responseListStr);
                        OfflineModeManager.AddAndOverWriteResponses(responses);
                    }
                    catch { }
                }
            }

            foreach (string webMethod in SimplePreCacheWebMethods)
            {
                AdminPanelWebApi.PreCacheSimpleWebMethod(webMethod);
            }

            foreach (string webMethod in SimplePreCacheWebMethodsByteArr)
            {
                AdminPanelWebApi.PreCacheSimpleWebMethod(webMethod, true);
            }

            foreach (string webMethod in SimplePreCacheWebMethodsApiResponse)
            {
                AdminPanelWebApi.PreCacheSimpleWebMethod(webMethod, useApiResponse: true);
            }

            foreach (string webMethod in NoParamterPreCacheWebMethods)
            {
                AdminPanelWebApi.PreCacheSimpleWebMethod(webMethod, noAuth: true);
            }
        }

        private LastPreCacheAuthKey GetLastPreCacheAuthKey()
        {
            string authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null) as string;
            if (authKey != null)
            {
                string lastPreCacheAuthKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, PRE_CACHE_AUTHKEY_REG_KEY, null) as string;
                if (lastPreCacheAuthKey != null && authKey == lastPreCacheAuthKey)
                {
                    return LastPreCacheAuthKey.SameAuthKey;
                }

                return LastPreCacheAuthKey.DifferentAuthKey;
            }

            return LastPreCacheAuthKey.NoAuthKey;
        }

        private void SetUpdateTimeStamp(DateTime timeStamp)
        {
            Microsoft.Win32.Registry.SetValue(ConfigSettings.InfowareKeyFolder, PRE_CACHE_REG_KEY, timeStamp.ToString());
            string authKey = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, "AuthKey", null) as string;

            if (authKey != null)
            {
                Microsoft.Win32.Registry.SetValue(ConfigSettings.InfowareKeyFolder, PRE_CACHE_AUTHKEY_REG_KEY, authKey);
            }
        }

        private DateTime? GetUpdateTimeStamp()
        {
            object dateTimeStr = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, PRE_CACHE_REG_KEY, null);

            if (dateTimeStr is string)
            {
                DateTime dateTime = DateTime.Now;
                if (DateTime.TryParse(dateTimeStr as string, out dateTime))
                {
                    return dateTime;
                }
            }

            return null;
        }
    }

    public enum LastPreCacheAuthKey
    {
        NoAuthKey = 0,
        SameAuthKey,
        DifferentAuthKey
    }
}
