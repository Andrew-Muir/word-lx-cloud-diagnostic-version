﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfowareVSTO.Common.OfflineMode
{
    public class OfflineModeManager
    {
        public static string OFFLINE_MODE_FOLDER = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\Infoware\\OfflineMode\\";
        private const string CACHE_DELETEDATE_REG_KEY = "LastCacheDeleteDate";
        private const int DELETE_INTERVAL_DAYS = 47;
        public static bool? IsOffline { get; set; }

        public static List<MethodWebserviceResponses> Responses { get; set; } = new List<MethodWebserviceResponses>();

        public static void SaveWebserviceResponse(WebserviceResponse response)
        {
            if (response != null)
            {
                MethodWebserviceResponses methodResponses = LoadMethodWebserviceResponses(response.MethodName);

                // add / replace webserviceResonse
                WebserviceResponse oldWebserviceResponse = methodResponses.Responses.FirstOrDefault(x => x.HasSameParameters(response.Parameters));

                if (oldWebserviceResponse != null)
                {
                    methodResponses.Responses.Remove(oldWebserviceResponse);
                }

                methodResponses.Responses.Add(response);

                SaveMethodWebserviceResponses(methodResponses);
            }
        }

        public static WebserviceResponse GetWebserviceResponse(string methodName, List<WSParameter> parameters)
        {
            MethodWebserviceResponses responses = LoadMethodWebserviceResponses(methodName);
            return responses.Responses.FirstOrDefault(x => x.HasSameParameters(parameters));
        }

        public static void AddAndOverWriteResponses(MethodWebserviceResponses preCachedResponsedList)
        {
            if (preCachedResponsedList != null)
            {
                MethodWebserviceResponses currentMethodResponses = LoadMethodWebserviceResponses(preCachedResponsedList.MethodName);

                foreach (WebserviceResponse response in preCachedResponsedList.Responses)
                {
                    WebserviceResponse oldResponse = currentMethodResponses.Responses.Where(x => x.HasSameParameters(response.Parameters)).FirstOrDefault();
                    if (oldResponse != null)
                    {
                        currentMethodResponses.Responses.Remove(oldResponse);
                    }

                    currentMethodResponses.Responses.Add(response);
                }

                SaveMethodWebserviceResponses(currentMethodResponses);
            }
        }

        public static bool DeleteCacheIfNeeded()
        {
            if (AdminPanelWebApi.TestConnection())
            {
                string lastCacheDeleteDate = Microsoft.Win32.Registry.GetValue(ConfigSettings.InfowareKeyFolder, CACHE_DELETEDATE_REG_KEY, null) as string;
                if (lastCacheDeleteDate != null && DateTime.TryParse(lastCacheDeleteDate, out DateTime result))
                {
                    if (result.AddDays(DELETE_INTERVAL_DAYS) < DateTime.Now)
                    {
                        DeleteAllCache();
                        Microsoft.Win32.Registry.SetValue(ConfigSettings.InfowareKeyFolder, CACHE_DELETEDATE_REG_KEY, DateTime.Now.ToString());
                        return true;
                    }
                }
                else
                {
                    Microsoft.Win32.Registry.SetValue(ConfigSettings.InfowareKeyFolder, CACHE_DELETEDATE_REG_KEY, DateTime.Now.ToString());
                }

                try
                {
                    if (!File.Exists(OFFLINE_MODE_FOLDER + "GetAuthor2.json"))
                    {
                        File.Create(OFFLINE_MODE_FOLDER + "GetAuthor2.json").Close();

                        return true;
                    }
                }
                catch { }
            }

            return false;
        }

        private static void DeleteAllCache()
        {
            try
            {
                foreach (string path in Directory.GetFiles(OFFLINE_MODE_FOLDER))
                {
                    File.Delete(path);
                }
            }
            catch { }
        }
     

        private static MethodWebserviceResponses LoadMethodWebserviceResponses(string methodName)
        {
            if (methodName != null)
            {
                // get MethodWebserviceResponse
                var methodResponses = Responses.FirstOrDefault(x => x.MethodName == methodName);

                //see if exists in memory
                if (methodResponses == null)
                {
                    //if exists in file read it 
                    try
                    {
                        string filename = OFFLINE_MODE_FOLDER + methodName + ".json";
                        if (File.Exists(filename))
                        {
                            string jsonStr = File.ReadAllText(filename);
                            if (!string.IsNullOrWhiteSpace(jsonStr))
                            {
                                methodResponses = JsonConvert.DeserializeObject<MethodWebserviceResponses>(jsonStr);
                            }
                        }
                    }
                    catch { }

                    if (methodResponses == null)
                    { 
                        methodResponses = new MethodWebserviceResponses(methodName);
                    }
               
                    Responses.Add(methodResponses);
                  
                }
                return methodResponses;
            }

            return null;
        }

        private static void SaveMethodWebserviceResponses(MethodWebserviceResponses responses)
        {
            if (responses != null)
            {
                string jsonStr = JsonConvert.SerializeObject(responses);

                if (!Directory.Exists(OFFLINE_MODE_FOLDER))
                {
                    Directory.CreateDirectory(OFFLINE_MODE_FOLDER);
                }

                File.WriteAllText(OFFLINE_MODE_FOLDER + responses.MethodName + ".json", jsonStr);
            }
        }
    }
}
