﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfowareVSTO.Common.OfflineMode
{
    public class MethodWebserviceResponses
    {
        public string MethodName { get; set; }

        public List<WebserviceResponse> Responses { get; set; }

        public MethodWebserviceResponses(string methodName)
        {
            MethodName = methodName;
            Responses = new List<WebserviceResponse>();
        }
    }
}
