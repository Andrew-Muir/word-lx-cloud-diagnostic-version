﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfowareVSTO.Common.OfflineMode
{
    public class WSParameter
    {
        public string Key { get; set; }
        public string Value { get; set; }

        public WSParameter(string key, string value)
        {
            Key = key;
            Value = value;
        }
    }
}
