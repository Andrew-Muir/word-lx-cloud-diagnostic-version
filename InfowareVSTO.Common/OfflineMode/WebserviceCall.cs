﻿using DocumentFormat.OpenXml.Bibliography;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InfowareVSTO.Common.OfflineMode
{
    public class WebserviceCall
    {
        public static bool ThrowWhenForbidden { get; set; } = false;
        public string MethodName { get; set; }

        public List<WSParameter> Parameters { get; set; }

        private List<WSParameter> cacheParameters;

        public List<WSParameter> CacheParameters
        {
            get
            {
                return cacheParameters ?? Parameters;
            }
            set
            {
                cacheParameters = value;
            }
        }

        public WebserviceResponse Response { get; set; }

        public WebserviceCall(string methodName, List<WSParameter> parameters)
        {
            MethodName = methodName;
            Parameters = parameters;
        }

        public IRestResponse Execute(bool useByteArr = false, bool useApiResponse = false)
        {
            RestClient client = new RestClient(ConfigSettings.WebApiUrl);
            RestRequest request;
            IRestResponse response;

            request = new RestRequest("api/WebApi/" + MethodName + GetParameterString(), Method.GET);
            response = client.Execute(request);
            if (response.StatusCode == HttpStatusCode.OK || response.StatusCode == HttpStatusCode.Accepted || response.StatusCode == HttpStatusCode.NoContent)
            {
                if (useApiResponse)
                {
                    try
                    {
                        Response = new WebserviceResponse(MethodName, CacheParameters, JsonConvert.DeserializeObject<ApiResponseDTO>(response.Content));
                    }
                    catch { }
                }
                else
                {
                    Response = new WebserviceResponse(MethodName, CacheParameters, useByteArr ? Convert.ToBase64String(response.RawBytes) : response.Content);
                }
                OfflineModeManager.SaveWebserviceResponse(Response);
                OfflineModeManager.IsOffline = false;
            }
            else if (ThrowWhenForbidden && response.StatusCode == HttpStatusCode.Forbidden)
            {
                throw new UnauthorizedAccessException();
            }
            else
            {
                Response = OfflineModeManager.GetWebserviceResponse(MethodName, CacheParameters);
                if (useApiResponse)
                {
                    try
                    {
                        Response.Content = JsonConvert.DeserializeObject<ApiResponseDTO>(Response.Content.ToString());
                    }
                    catch { }
                }
                OfflineModeManager.IsOffline = true;
            }

            if (Response == null)
            {
                //  MessageBox.Show("Http Status is " + (int)response.StatusCode, "Bad HTTP Status", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            return response;
        }

        private string GetParameterString()
        {
            string result = string.Empty;
            if (Parameters != null)
            {
                List<string> parameters = new List<string>();

                foreach (WSParameter param in Parameters)
                {
                    parameters.Add(param.Key + "=" + param.Value);
                }

                if (parameters.Count > 0)
                {
                    return "?" + string.Join("&", parameters);
                }
            }

            return string.Empty;
        }
    }
}
