﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace InfowareVSTO.Common.OfflineMode
{
    public class WebserviceResponse
    {
        public string MethodName { get; set; }

        public List<WSParameter> Parameters { get; set; }

        public object Content { get; set; }
        [IgnoreDataMember] [JsonIgnore]
        public byte[] RawBytes
        {
            get
            {
                if (Content is byte[])
                {
                    return Content as byte[];
                }
                else if (Content is string)
                {
                    try
                    {
                        return Convert.FromBase64String(Content.ToString());
                    }
                    catch { };
                }

                return null;
            }
        }

        public WebserviceResponse(string methodName,List<WSParameter> parameters, object content)
        {
            MethodName = methodName;
            Parameters = parameters;
            Content = content;
        }

        public bool HasSameParameters(List<WSParameter> parametersToCompare)
        {
            if (parametersToCompare != null && Parameters!= null)
            {
                if (parametersToCompare.Count != Parameters.Count)
                {
                    return false;
                }

                foreach (WSParameter param  in parametersToCompare)
                {
                    WSParameter own = Parameters.FirstOrDefault(x => x.Key == param.Key);
                    if (own == null || own.Value != param.Value)
                    {
                        return false;
                    }
                }
            }
            else if (parametersToCompare != null || Parameters != null)
            {
                return false;
            }

            return true;
        }
    }
}
